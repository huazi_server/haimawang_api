<!DOCTYPE html>
<!--[if IE 8]> <html class="ie8"> <![endif]-->
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html> <!--<![endif]-->
<head>

    <title>登录界面</title>
    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <c:set var="ctx" value="${pageContext.request.contextPath}" />


</head>
<body>
<jsp:include page="/WEB-INF/views/main/frame/title.jsp"/>


<div id="loginbox">


    <form id="loginform" name="form" action="#" class="form-vertical" method="post">
        <div class="control-group normal_text"> <h3><!-- <img src="img/logo.png" alt="Logo" /> -->登录界面</h3></div>

        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_lg"><i class="icon-user"></i></span><input type="text" id="uname" name="uname" value="${msp['uname']}" placeholder="用户名" />
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_lb"><i class="icon-lock"></i></span><input type="password" id="pwd" name="pwd" value="${msp['pwd']}" placeholder="密码" />
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box" name="">
                    <span class="add-on bg_ly"><i class="icon-barcode"></i></span>
                    <input style="width: 50%; margin-left: -1%;" maxlength="4" type="text" id="validateCode" name="validateCode" value="" placeholder="验证码" />
                    <img src="${ctx}/main/validateCode" style="width:25%; height: 2%;" name="code" id="code" style="cursor:hand;vertical-align: middle; " title="点击可更换图片" onclick="changeCode()"/>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <span class="pull-right">
                <button class="btn btn-success" onclick="login()" type="button">登录</button>
            </span>
        </div>

    </form>
</div>


<jsp:include page="/WEB-INF/views/main/frame/bottom.jsp"/>
<jsp:include page="/WEB-INF/views/main/frame/footer.jsp"/>

<script src="${ctx}/pluginExt/js/main/login.js"></script>

</body>
</html>