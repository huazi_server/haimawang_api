<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />



<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="首页" class="tip-bottom"><i class="icon-home"></i>首页</a></div>
    </div>
    <!--End-breadcrumbs-->

    <!-- 首页显示内容 -->
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">

<!-- 测试Code可以在此编写 Start -->

            <!-- 测试JS日期控件生成 -->
            <label class="control-label">日历：</label>
            <div class="text-center">
                <input type="text" onClick="WdatePicker()" onfocus="this.blur()"/>
            </div>

            <hr>

            <!-- 测试JS文件上传控件 -->
            <label class="control-label">图片：</label>
            <div class="text-center">
                <input type="hidden" id="action" value="${ctx}/mav/test/fileUpload">
                <input id="file-1" name="file" class="file" type="file" multiple>
            </div>

            <hr>


            <!-- 测试JS信息框弹出 -->
            <label class="control-label">信息框：</label>
            <div class="text-center">
                <button type="button" class="btn btn-success" onclick="hintShot()">弹出</button>
            </div>

            <hr>


            <!-- 测试JS确认框弹出 -->
            <label class="control-label">确认框：</label>
            <div class="text-center">
                <button type="button" class="btn btn-success" onclick="confirmShot()">弹出</button>
            </div>

            <hr>


            <!-- 测试JS分页插件构建 -->
            <label class="control-label">分页插件：</label>
            <div class="text-center" id="pagination-div">
                <ul id="pagination" class="pagination-sm"></ul>
            </div>

            <hr>







<!-- 测试Code可以在此编写 End -->

        </div>
    </div>

</div>
<!--Footer-part-->


<!-- my JS Test(测试用) -->
<%--<script src="${ctx}/pluginExt/js/main/test.js"></script>--%>