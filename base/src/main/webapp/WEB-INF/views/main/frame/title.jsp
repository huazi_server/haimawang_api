<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<script src="${ctx}/js/jquery.min.js"></script>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<%--<link rel="stylesheet" href="${ctx}/css/bootstrap.min.css" />--%>
<%--<link rel="stylesheet" href="${ctx}/css/bootstrap3.3.4.min.css" />--%>
<link rel="stylesheet" href="${ctx}/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="${ctx}/css/bootstrap-wysihtml5.css" />

<link rel="stylesheet" href="${ctx}/css/fullcalendar.css" />
<link rel="stylesheet" href="${ctx}/css/select2.css" />
<link rel="stylesheet" href="${ctx}/css/datepicker.css" />
<link rel="stylesheet" href="${ctx}/css/uniform.css" />
<link rel="stylesheet" href="${ctx}/css/colorpicker.css" />

<link rel="stylesheet" href="${ctx}/css/jquery.gritter.css" />

<link rel="stylesheet" href="${ctx}/css/matrix-login.css" />
<link rel="stylesheet" href="${ctx}/css/matrix-style.css" />
<link rel="stylesheet" href="${ctx}/css/matrix-media.css" />
<link rel="stylesheet" href="${ctx}/css/fullcalendar.css" />
<link rel="stylesheet" href="${ctx}/font/css/font-awesome.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="${ctx}/plugin/bootstrap-toastr/toastr.css" />
<%--<link href="${ctx}/plugin/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />--%>
<link href="//cdn.bootcss.com/bootstrap-fileinput/4.3.3/css/fileinput.css" rel="stylesheet">


<link rel="stylesheet" href="${ctx}/cssExt/selfCss.css" />


