<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!--sidebar-menu-->


<!-- 菜单选中为父li加open样式、子li加active样式 -->

<div id="sidebar" style="position: fixed;"><a href="#" class="visible-phone"><i class="icon icon-home"></i> 首页</a>
	<ul>
		<li class="menu" name="${ctx}" id="${ctx}/main/home"><a href="${ctx}/mav/main/home"><i class="icon icon-home"></i> <span>首页</span></a> </li>

		<li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>测试管理</span> <span class="label label-important"><b class="icon-chevron-down"></b></span></a>
			<ul>
				<li class="menu" name="${ctx}" id="${ctx}/xxx/xxx"><a href="${ctx}/xxx/xxx">测试列表</a></li>
			</ul>
		</li>

	</ul>
</div>

<!--close-sidebar-menu-->
