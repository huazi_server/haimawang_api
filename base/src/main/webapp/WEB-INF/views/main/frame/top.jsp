<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div id="header">
    <h1><a href="xxx.html">欢迎进入XXX系统</a></h1>
</div>

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome   ${sessionScope.user.uname}用户</span><!-- <b class="caret"></b> --></a></li>
        <li class=""><a title="" href="javascript: showEdit()"><i class="icon icon-cog"></i> <span class="text">修改密码</span></a></li>
        <li class=""><a title="" href="${ctx}/main/logout"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
    </ul>
</div>
<!--close-top-Header-menu-->

<!-- 修改密码 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">XXX</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" action="#" name="form" id="form">
                    <div class="control-group">
                        <div class="controls">新&nbsp;&nbsp;密&nbsp;&nbsp;码
                            <input type="password" name="pwd" id="pwd" required/>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">确认密码
                            <input type="password" name="pwd2" id="pwd2" required/>
                        </div>
                    </div>
                    <div class="form-actions" style="margin-top: 10px;">
                        <input type="button" onclick="updatePwd()" class="btn btn-success" value="提交" style="margin-top: 1%;">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



