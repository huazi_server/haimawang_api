<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<!-- jQuery JS -->
<script src="${ctx}/js/jquery-1.10.2.min.js"></script>
<script src="${ctx}/js/jquery.ui.custom.js"></script>
<script src="${ctx}/js/jquery.flot.min.js"></script>
<script src="${ctx}/js/jquery.flot.resize.min.js"></script>
<script src="${ctx}/js/jquery.peity.min.js"></script>
<script src="${ctx}/js/jquery.validate.js"></script>
<script src="${ctx}/js/jquery.wizard.js"></script>
<script src="${ctx}/js/jquery.form.js"></script>
<script src="${ctx}/js/jquery.uniform.js"></script>
<script src="${ctx}/js/jquery.dataTables.min.js"></script>
<script src="${ctx}/js/jquery.gritter.min.js"></script>
<script src="${ctx}/js/jquery.toggle.buttons.html"></script>

<!-- Bootstrap JS -->
<script src="${ctx}/js/bootstrap.min.js"></script>
<script src="${ctx}/js/bootstrap-scrollspy.js"></script>
<script src="${ctx}/js/bootstrap-dropdown.js"></script>
<script src="${ctx}/js/bootstrap-colorpicker.js"></script>
<script src="${ctx}/js/bootstrap-datepicker.js"></script>
<script src="${ctx}/js/bootstrap-wysihtml5.js"></script>

<!-- Template JS -->
<script src="${ctx}/js/matrix.calendar.js"></script>
<script src="${ctx}/js/matrix.js"></script>
<script src="${ctx}/js/matrix.dashboard.js"></script>
<script src="${ctx}/js/matrix.interface.js"></script>
<script src="${ctx}/js/matrix.chat.js"></script>
<script src="${ctx}/js/matrix.form_validation.js"></script>
<script src="${ctx}/js/matrix.popover.js"></script>
<script src="${ctx}/js/matrix.tables.js"></script>
<script src="${ctx}/js/matrix.calendar.js"></script>
<script src="${ctx}/js/matrix.form_common.js"></script>
<script src="${ctx}/js/matrix.login.js"></script>

<!-- Other JS -->
<script src="${ctx}/js/wysihtml5-0.3.0.js"></script>
<script src="${ctx}/js/masked.js"></script>
<script src="${ctx}/js/excanvas.min.js"></script>
<script src="${ctx}/js/select2.min.js"></script>
<script src="${ctx}/js/fullcalendar.min.js"></script>


<!-- plugin JS -->
<script src="${ctx}/plugin/bootstrap-toastr/toastr.js"></script>
<script src="${ctx}/plugin/bootbox-master/bootbox.js"></script>
<script src="${ctx}/plugin/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/plugin/bootstrap-fileinput/js/fileinput.js"></script>
<script src="${ctx}/plugin/bootstrap-fileinput/js/locales/zh.js"></script>
<script src="${ctx}/plugin/jquery-twbsPagination/js/jquery.twbsPagination.js"></script>

<!-- my pluginExt JS -->
<script src="${ctx}/pluginExt/js/jquery-validate-expand.js"></script>
<script src="${ctx}/pluginExt/js/hint-box.js"></script>
<script src="${ctx}/pluginExt/js/confirm-box.js"></script>
<script src="${ctx}/pluginExt/js/uploadFile.js"></script>
<script src="${ctx}/pluginExt/js/pagination-page.js"></script>

<!-- my JS -->
<script src="${ctx}/pluginExt/js/menuCode.js"></script>
<script src="${ctx}/pluginExt/js/widget-collapse.js"></script>
<script src="${ctx}/pluginExt/js/getProjectPath.js"></script>
<script src="${ctx}/pluginExt/js/main/home.js"></script>







<script>

</script>
