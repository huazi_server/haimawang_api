<!DOCTYPE html>
<!--[if IE 8]> <html class="ie8"> <![endif]-->
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html> <!--<![endif]-->
<head>

    <title> xxx</title>
    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <c:set var="ctx" value="${pageContext.request.contextPath}" />
    <jsp:include page="/WEB-INF/views/main/frame/title.jsp"/>

</head>
<body>
<jsp:include page="/WEB-INF/views/main/frame/top.jsp"/>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="首页" class="tip-bottom"><i class="icon-home"></i> 首页</a> <a href="#" class="current">Error</a> </div>
        <h1>Error 405</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Error 405</h5>
                    </div>
                    <div class="widget-content">
                        <div class="error_ex">
                            <h1>405</h1>
                            <h3>亲, 宝贝.</h3>
                            <p>请勿非法访问哟。</p>
                            <p>${requestScope.errorMsg}</p>
                            <a class="btn btn-warning btn-big"  href="${ctx}/main/home">去首页</a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="/WEB-INF/views/main/frame/bottom.jsp"/>
</body>
</html>
