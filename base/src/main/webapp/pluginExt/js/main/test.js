
/***********************************    文件上传    ****************************************/

//定义可以上传的文件类型
var arr=new Array();
arr[0]="jpg";
arr[1]="png";
arr[2]="gif";

//定义回调函数
var func=function (data){
    alert("data====="+JSON.stringify(data));
}
//初始化上传控件
uploadFile("file-1", $("#action").val(), arr);
//异步上传返回结果处理
$("#file-1").on("fileuploaded", function (event, data, previewId, index) {
    //上传成功后操作,多文件上传时,每次上传后都会执行
    func(data.response);
});


/***********************************    信息框    ****************************************/

//信息框操作
function hintShot(){
    toastr.success("操作成功","提示");
}




/***********************************    确认框    ****************************************/

//确认对话框操作
function confirmShot(){
    var m = "提示信息: 您确定要进行此操作吗？";
    //传入两个回调函数
    confirmBox(m, function(){
        alert('点击确认按钮了');
    }, function(){
        alert('点击取消按钮了');
    });
}


/***********************************    分页(ajax异步)    ****************************************/


//构建分页插件,并执行回调
var totalPages = 10;
var callbackFunc = function(event, page){

    alert("event="+event+"&page="+page);
}
//构建分页(加载页面后就会构建分页插件,所以加载页面前需要将总页数的值设置好)
createPagination(totalPages,callbackFunc);



