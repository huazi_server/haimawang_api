//弹出修改密码框
function showEdit(){
    $("#myModalLabel").text("修改密码");
    $('#myModal').modal();
}
//验证字段,提交表单
function pwd_verify(){

    var result = $("#form").validate({
        debug: false,
        rules: {
            pwd: {
                required: true,
                minlength: 6
            },
            pwd2: {
                required: true,
                minlength: 6,
                equalTo: "#pwd"
            }
        },
        messages: {
            pwd: {
                required: "请输入密码",
                minlength: jQuery.format("密码不能小于{0}个字符")
            },
            pwd2: {
                required: "请输入确认密码",
                minlength: "确认密码不能小于{0}个字符",
                equalTo: "两次输入密码不一致"
            }
        }
    });
    return result.form();
}

function updatePwd(){
    if(pwd_verify()){
        $.ajax({
            type:"post",  //提交方式
            dataType:"json", //数据类型
            data: $('#form').serialize(),
            url:rootPath+"/main/updatePwd", //请求url
            success:function(data){ //调用成功的回调函数
                if(data.state=='SUCCESS'){
                    toastr.success(data.message,"提示");
                    //隐藏弹出表单
                    setTimeout(function(){
                        $(".close").click();
                    }, 1000);
                }else{
                    toastr.error(data.message,"提示");
                    return false;
                }
            }
        });
    }
}
