//登录验证
function login_verify (){
    var result = $("#loginform").validate({
        debug: false,
        rules: {
            uname: {
                required: true,
                minlength: 5
            },
            pwd: {
                required: true,
                minlength: 6
            },
            validateCode: {
                required: true,
                maxlength: 4
            }
        },
        messages: {
            uname: {
                required: "请输入用户名",
                minlength: "用户名不能小于{0}个字符"
            },
            pwd: {
                required: "请输入密码",
                minlength: jQuery.format("密码不能小于{0}个字符")
            },
            validateCode: {
                required: "请输入验证码",
                maxlength: jQuery.format("验证码是{0}个字符")
            }
        }
    });
    return result.form();
}
//获取验证码
function changeCode(){
    //document.getElementById("code").src=document.getElementById("code").src+"?nocache="+Math.random();
    $("#code").attr("src",$("#code").attr("src")+"?nocache="+new Date().getTime());
}

function login(){
    if(login_verify()){

        var uname = $("#uname").val();
        var pwd = $("#pwd").val();
        var validateCode = $("#validateCode").val();
        $.ajax({
            type:"post",  //提交方式
            dataType:"json", //数据类型
            data: {"uname": uname, "pwd": pwd ,"validateCode": validateCode},
            url:rootPath+"/main/login", //请求url
            success:function(data){ //调用成功的回调函数
                if(data.state=='SUCCESS'){
                    toastr.success(data.message,"提示");
                    window.location.href=rootPath+'/main/home';
                }else{
                    toastr.error(data.message,"提示");
                    return false;
                }
            }
        });
    }
}
