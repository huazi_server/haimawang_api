//弹出提示框
var type = "";
var title = "";
var message = "";
var fun = null;

function hintBox(){

    //上中
    var positionClass = "toast-top-center";
    //上中(整行)
    //var positionClass = "toast-top-full-width";

    if(type!='' && message!=''){

        var func = null;
        if(typeof fun == 'function'){
            func = fun;
        }
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "positionClass": positionClass,
            "onclick": func,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000"
        };

        if(type.toLowerCase()=='success'){
            toastr.success(title,message);
        }
        if(type.toLowerCase()=='warning'){
            toastr.warning(title,message);
        }
        if(type.toLowerCase()=='error'){
            toastr.error(title,message);
        }
        if(type.toLowerCase()=='clear'){
            toastr.clear();
        }
    }

}

/*
 HintType       提示的类型(必传)
 HintMessage    提示的信息(必传)
 */
function createHintBox(HintType,HintTitle,HintMessage,HintFun){
    type = HintType;
    title = HintTitle;
    message = HintMessage;
    fun = HintFun;
    hintBox();
}



