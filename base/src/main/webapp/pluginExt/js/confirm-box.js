//确认操作提示框
//message: 提示信息
//func1: 确定执行的操作(回调)
//func2: 取消执行的操作(回调)
function confirmBox(message, func1, func2){
    bootbox.confirm({
        buttons: {
            confirm: {
                label: '确认',
                className: 'btn-primary'
            },
            cancel: {
                label: '取消',
                className: 'btn-default'
            }
        },
        message: message,
        callback: function(result) {
            if(result) {
                func1();
            } else {
                func2();
            }
        },
    });
}
