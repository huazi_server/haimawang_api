
//由于采用ajax方式查询,当总页数发生改变时页面上页码不能发生改变,所以先清除插件,再重新构建
/*$("#pagination").remove();
$("#pagination-div").html('<ul id="pagination" class="pagination-sm"></ul>');*/
/**
 * 创建分页插件
 * @param message
 * @param func1
 * @param func2
 */
function createPagination(totalPages, callbackFunc){
    $("#pagination").remove();
    $("#pagination-div").html('<ul id="pagination" class="pagination-sm"></ul>');

    $('#pagination').twbsPagination({
        first: '首页',
        prev: '前一页',
        next: '下一页',
        last: '尾页',
        totalPages: totalPages,//总页数
        visiblePages: totalPages<5?totalPages:5,//显示的页码
        //version: '1.0',//当页数多时,分页插件的页码不会自动左右移动,需要手动点击切换
        version: '1.1',//当页数多时,分页插件的页码自动左右移动
        onPageClick: function (event, page) {
            //执行回调
            callbackFunc(event, page);
        }
    });
}

