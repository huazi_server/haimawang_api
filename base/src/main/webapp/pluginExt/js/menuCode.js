//父菜单绑定事件
$('.submenu').bind('click', function() {
    if($(this).hasClass("open")){
        $(this).removeClass("open");
    }else{
        $(this).addClass("open");
    }
});
//子菜单绑定事件(保存菜单id,用于页面加载完后设置样式)
$("li[class='menu']").bind('click', function(){
    $.ajax({
        type: "get",
        url: $(this).attr("name")+'/main/saveMenuId?menuId='+$(this).attr("id"),
        data: {},
        success: function (data) {
            //保存成功
            //alert("保存成功");
        }
    });
});
//子菜单设置样式
var menuId = $("#menuId").val();
$("li[class='menu']").each(function(m,obj){
    if($(this).attr("id")==menuId){
        console.log("设置菜单样式---->开始");
        $(this).attr("class","menu active");
        $(this).parent().parent().addClass("open");
        console.log("设置菜单样式---->结束");
    }
});