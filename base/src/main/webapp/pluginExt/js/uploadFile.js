
/**
 *
 * @param fileInputId   file的控件的元素id
 * @param uploadUrl     上传调用的方法
 * @param fileType      可以上传的文件类型(形式：'jpg', 'png','gif')
 */
function uploadFile(fileInputId, uploadUrl, fileType){

    //构建上传文件时的组件
    $("#"+fileInputId).fileinput({
        uploadUrl: uploadUrl, //提交地址
        enctype: 'multipart/form-data',
        uploadAsync: true, //默认异步上传
        allowedFileExtensions : fileType,
        overwriteInitial: false,
        language : 'zh',
        maxFileSize: 1000,
        maxFilesNum: 10,
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        showCaption: false,
        browseClass: "btn btn-primary",
        dropZoneEnabled: false,
        /*uploadExtraData: {
         "excelType": document.getElementByID('id')
         },*/
        uploadExtraData: function() {
            //这里只能通过原生js取值,此参数用来向后台传递附加参数
            return {"excelType": "extraValue"};
        },
        //allowedFileTypes: ['image']
    });

}

