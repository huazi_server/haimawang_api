package cn.com.mys.project_kernel.base.util;

import java.lang.reflect.ParameterizedType;


/*
 * 把传入的泛型转换为实际的类--对象
 * 范类转换工具类
 */
public class GenericSuperClassUtils {
	
	
	//范类转换
	@SuppressWarnings("rawtypes")
	public static Class getGenericSuperClass(Class entity) {
		//获取对象所属的类型
		ParameterizedType type = (ParameterizedType) entity.getGenericSuperclass();
		//获取实际类型
		Class entityClass = (Class) type.getActualTypeArguments()[0];
		return entityClass;
		
		
	}
	
	

}
