package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ChannelUser;
import cn.com.mys.project_kernel.base.entity.ext.ChannelUserExt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ChannelUserService {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ChannelUserExt> findListByCondition(Map<String, Object> map4Param);

    Integer insert(ChannelUser channelUser);
    Integer update(ChannelUser channelUser);

    ChannelUserExt findObjectByCondition(Map<String, Object> map4Param);

    Integer findDayUserCountByCondition(Map<String, Object> map4Param);
}
