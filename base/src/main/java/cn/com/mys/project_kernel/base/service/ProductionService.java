package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.Production;
import cn.com.mys.project_kernel.base.entity.ext.ProductionExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ProductionService {

    Integer findCountByCondition4All(Map<String, Object> map4Param);
    List<ProductionExt> findListByCondition4All(Map<String, Object> map4Param);

    ProductionExt findObjectByCondition(Map<String, Object> map4Param);

    Integer insert(ProductionExt productionExt);
    Integer update(Production production);

    Production findById(long id);

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<ProductionExt> findListByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition4Collect(Map<String, Object> map4Param);
    List<ProductionExt> findListByCondition4Collect(Map<String, Object> map4Param);

    Integer findCountByCondition4Like(Map<String, Object> map4Param);
    List<ProductionExt> findListByCondition4Like(Map<String, Object> map4Param);
}
