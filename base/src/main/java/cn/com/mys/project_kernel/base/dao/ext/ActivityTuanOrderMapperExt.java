package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ActivityTuanOrderExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ActivityTuanOrderMapperExt {

    ActivityTuanOrderExt findObjectByCondition(Map<String, Object> map);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ActivityTuanOrderExt> findListByCondition(Map<String, Object> map4Param);

    ActivityTuanOrderExt findObjectByActivityId(Map<String, Object> map);

    ActivityTuanOrderExt findObjectByActivityId4Join(Map<String, Object> map);
}