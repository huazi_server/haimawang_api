package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ContestUser;

public class ContestUserExt extends ContestUser {

    private Integer voteFlag;//指定作品投票标识(1未投、2已投)
    public Integer getVoteFlag() {
        return voteFlag;
    }
    public void setVoteFlag(Integer voteFlag) {
        this.voteFlag = voteFlag;
    }
}