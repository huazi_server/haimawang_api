package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ConfigDictMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ConfigDictMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ConfigDictExt;
import cn.com.mys.project_kernel.base.service.ConfigDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ConfigDictServiceImpl implements ConfigDictService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ConfigDictMapper configDictMapper;

	@Autowired
	public ConfigDictMapperExt configDictMapperExt;


	@Override
	public List<ConfigDictExt> findListByCondition(Map<String, Object> map4Param) {
		return configDictMapperExt.findListByCondition(map4Param);
	}
}
