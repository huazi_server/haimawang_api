package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ResourceFileMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ResourceFileMapperExt;
import cn.com.mys.project_kernel.base.entity.ResourceFile;
import cn.com.mys.project_kernel.base.entity.ext.ResourceFileExt;
import cn.com.mys.project_kernel.base.service.ResourceFileService;
import cn.com.mys.project_kernel.base.util.FileUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ResourceFileServiceImpl implements ResourceFileService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ResourceFileMapper resourceFileMapper;

	@Autowired
	public ResourceFileMapperExt resourceFileMapperExt;



	@Override
	public int insert() {

		String url4BG = "https://huazi-api-test.oss-cn-beijing.aliyuncs.com/storage/4038/282842/XOO2KUZRxAnwNmPXuD9xzsApXa88Vb9r.json";//背景
		String url4ZX = "https://huazi-api-test.oss-cn-beijing.aliyuncs.com/storage/4038/282842/XOO2UkZRxAnwNmPYhu2g3uuES4P2zPk.json";//造型
		String url4SY = "https://huazi-api-test.oss-cn-beijing.aliyuncs.com/storage/4038/282842/XOO2dUZRxAnwNmPZWCLXIgpzpr0svNs.json";//声音


		String result = FileUtils.openFile("");
		Integer type = 1;//类型(1背景、2造型、3声音)

		System.out.println("");
		System.out.println("");
		System.out.println("");

		System.out.println("----------------------");

		JSONArray array = JSONArray.fromObject(result);
		System.out.println("array.size()="+array.size());

		for(Object o : array){
			System.out.println("o="+o);

			JSONObject jo = JSONObject.fromObject(o);
			String name = jo.getString("name");
			String md5 = jo.getString("md5");

			ResourceFile resourceFile = new ResourceFile();
			resourceFile.setName(name);
			resourceFile.setFileName(md5);
			resourceFile.setType(type);

			resourceFile.setState(1);//状态(1未完成、2已完成)
			resourceFile.setRemark("");
			resourceFile.setCreatedAt(new Date());
			resourceFile.setUpdatedAt(new Date());
			resourceFile.setDelFlag(0);

			resourceFileMapper.insertSelective(resourceFile);


			//上传文件
			String fileName = md5;
			String fileType = md5.substring(md5.lastIndexOf(".")+1);
			String fileNetUrl = "https://cdn.assets.scratch.mit.edu/internalapi/asset/"+fileName+"/get/";
			System.out.println("fileNetUrl="+fileNetUrl);

			String fileUrl = upload(fileNetUrl, fileName, fileType);
			System.out.println("fileUrl="+fileUrl);

			if(StringUtils.isNotEmpty(fileUrl)){
				String fileName4New = fileUrl.substring(fileUrl.lastIndexOf("/")+1);
				resourceFile.setFileName4New(fileName4New);

				String filePath4New = fileUrl.substring("https://haimacode.oss-cn-beijing.aliyuncs.com".length());
				resourceFile.setFilePath4New(filePath4New);

				resourceFile.setFileUrl(fileUrl);

				resourceFile.setState(2);//状态(1未完成、2已完成)
				resourceFile.setUpdatedAt(new Date());
				resourceFile.setDelFlag(0);
				resourceFileMapper.updateByPrimaryKeySelective(resourceFile);
			}


			System.out.println("");
			System.out.println("");
			System.out.println("");

			System.out.println("----------------------");

		}

		return 0;
	}


	@Override
	public int update() {

		String filePath = "";
		//filePath = "F://backdrops.json";
		//filePath = "F://costumes.json";
		//filePath = "F://sounds.json";
		//filePath = "F://sprites.json";
		String content = FileUtils.readFile(filePath);
		System.out.println(content);

		String content4New = content;

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("type", "");//类型(1背景、2造型、3声音)
		List<ResourceFileExt> list = resourceFileMapperExt.findListByCondition(map);
		for(ResourceFileExt rf : list){
			String fileName = rf.getFileName();
			String fileName4New = rf.getFileName4New();
			String filePath4New = rf.getFilePath4New();

			//content4New = content4New.replace(fileName, fileName4New);
			content4New = content4New.replace(fileName, filePath4New);
		}

		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("------------------------");
		System.out.println(content4New);



		String writename = "";
		//writename = "F://backdrops_new.json";
		//writename = "F://costumes_new.json";
		//writename = "F://sounds_new.json";
		//writename = "F://sprites_new.json";
		File writeFile = new File(writename);
		try {
			writeFile.createNewFile(); // 创建新文件
			BufferedWriter out = new BufferedWriter(new FileWriter(writeFile));
			out.write(content4New);
			out.flush(); // 把缓存区内容压入文件
			out.close(); // 最后记得关闭文件
		} catch (IOException e) {
			e.printStackTrace();
		}

		return 0;
	}





	public String upload(String fileUrl, String fileName, String fileType) {
		String uploadFileUrl="https://test.huazilive.com/api/service/account/privacy/product/folder/file/upload";
		/*try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}*/


		String filename = fileName.substring(0,fileName.lastIndexOf("."));
		//获取后缀(扩展名)
		String contentType = fileType;

		// 下载网络文件
		int bytesum = 0;
		int byteread = 0;

		String fileUrl4Local = "";
		System.setProperty("https.protocols", "TLSv1.2");

		try {
			URL url = new URL(fileUrl);
			URLConnection conn = url.openConnection();
			InputStream inStream = conn.getInputStream();
			FileOutputStream fs = new FileOutputStream("F://www/"+filename+"."+contentType);

			byte[] buffer = new byte[1204];
			int length;
			while ((byteread = inStream.read(buffer)) != -1) {
				bytesum += byteread;
				//System.out.println(bytesum);
				fs.write(buffer, 0, byteread);
			}
			fs.flush();


			File f = new File("F://www/"+filename+"."+contentType);

			Map<String, Object> map4Param = new HashMap<String, Object>();
			map4Param.put("loginToken", "23b7834d6e978120b1b6cacc1bbaa4a8");
			map4Param.put("folderPath", "/scratch/asset");//文件保存的路径
			map4Param.put("fileName", filename+"."+contentType);//(包含扩展名)
			map4Param.put("contentType", contentType);//扩展名
			System.out.println("p=="+map4Param);
			String r = FileUtils.post4Caller(uploadFileUrl,"upload",f,map4Param);
			System.out.println("r=="+r);


			JSONObject rr = JSONObject.fromObject(r);
			fileUrl4Local = rr.getJSONObject("data").getString("url");


			fs.close();
		} catch (Exception e) {
			fileUrl4Local = "";
			e.printStackTrace();
		}

		return fileUrl4Local;
	}




}
