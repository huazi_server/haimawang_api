package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ChannelStatisticsExt;
import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public interface ChannelStatisticsMapperExt {

    ChannelStatisticsExt findObjectByCondition(Map<String, Object> mm);
}