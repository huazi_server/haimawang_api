package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ChannelExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ChannelMapperExt {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ChannelExt> findListByCondition(Map<String, Object> map4Param);

    ChannelExt findByAccountId(String accountId);

    ChannelExt findByPhone(String phone);
}