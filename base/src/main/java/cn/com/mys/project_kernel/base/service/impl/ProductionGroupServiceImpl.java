package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ProductionGroupMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ProductionGroupMapperExt;
import cn.com.mys.project_kernel.base.service.ProductionGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service的实现类
 *
 */

@Service
public class ProductionGroupServiceImpl implements ProductionGroupService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ProductionGroupMapper productionGroupMapper;

	@Autowired
	public ProductionGroupMapperExt productionGroupMapperExt;


}
