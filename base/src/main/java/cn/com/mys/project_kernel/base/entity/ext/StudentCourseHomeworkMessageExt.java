package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.StudentCourseHomeworkMessage;

public class StudentCourseHomeworkMessageExt extends StudentCourseHomeworkMessage {

    private String studentName;//学生名称
    private String studentAvatarUrl;//学生头像

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentAvatarUrl() {
        return studentAvatarUrl;
    }

    public void setStudentAvatarUrl(String studentAvatarUrl) {
        this.studentAvatarUrl = studentAvatarUrl;
    }
}