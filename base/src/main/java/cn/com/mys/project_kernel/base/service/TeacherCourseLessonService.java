package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.TeacherCourseLessonExt;

import java.util.Map;

/**
 * Service的接口
 *
 */
public interface TeacherCourseLessonService {


    TeacherCourseLessonExt findObjectByCondition(Map<String, Object> m);

    Integer update(TeacherCourseLessonExt teacherCourseLessonExt);

    Integer insert(TeacherCourseLessonExt tcl);

    Integer updateByTeacherCourseId(Map<String, Object> m);
}
