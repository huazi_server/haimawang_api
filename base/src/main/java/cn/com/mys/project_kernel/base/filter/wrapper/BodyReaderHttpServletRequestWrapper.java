package cn.com.mys.project_kernel.base.filter.wrapper;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class BodyReaderHttpServletRequestWrapper extends HttpServletRequestWrapper {
	private final byte[] body;

    private MultipartHttpServletRequest multipartRequest;
    public MultipartHttpServletRequest getMultipartRequest() {
        return multipartRequest;
    }
    public void setMultipartRequest(MultipartHttpServletRequest multipartRequest) {
        this.multipartRequest = multipartRequest;
    }



    public BodyReaderHttpServletRequestWrapper(HttpServletRequest request) throws IOException {
        super(request);
        body = HttpHelper.getBodyString(request).getBytes(Charset.forName("UTF-8"));
    }


    public BodyReaderHttpServletRequestWrapper(HttpServletRequest request, String param) throws IOException {
        super(request);
        body = param.getBytes(Charset.forName("UTF-8"));
    }


    public byte[] getBody() {
        return body;
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {

        final ByteArrayInputStream bais = new ByteArrayInputStream(body);

        return new ServletInputStream() {

            @Override
            public int read() throws IOException {
                return bais.read();
            }

            //@Override
            public boolean isFinished() {
                return false;
            }

            //@Override
            public boolean isReady() {
                return false;
            }

            //@Override
            public void setReadListener(ReadListener readListener) {

            }
        };
    }
}
