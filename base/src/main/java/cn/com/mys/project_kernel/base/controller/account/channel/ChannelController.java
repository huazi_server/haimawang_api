package cn.com.mys.project_kernel.base.controller.account.channel;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.*;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.filter.wrapper.BodyReaderHttpServletRequestWrapper;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.AdminPage;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 渠道
 *
 */
@Controller
@RequestMapping(value = "/account/channel")
public class ChannelController {

	Log log = LogFactory.getLog(ChannelController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentCourseService studentCourseService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private StudentCourseHomeworkService studentCourseHomeworkService;
	@Autowired
	private TeacherCourseService teacherCourseService;
	@Autowired
	private CourseLessonService courseLessonService;

	@Autowired
	private CoursePackageService coursePackageService;
	@Autowired
	private CourseGroupService courseGroupService;
	@Autowired
	private ChannelCourseService channelCourseService;
	@Autowired
	private DiscountService discountService;
	@Autowired
	private StudentDiscountService studentDiscountService;

	@Autowired
	private StudentCourseOrderService studentCourseOrderService;

	@Autowired
	private AdminService adminService;
	@Autowired
	private ChannelService channelService;

	@Autowired
	private ActivityFreeService activityFreeService;
	@Autowired
	private ActivityTuanService activityTuanService;
	@Autowired
	private ChannelActivityService channelActivityService;

    @Autowired
    private ChannelUserService channelUserService;


	@RequestMapping(value = "info")
	public ResponseEntity<ResponseJson> info(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		Map<String, Object> m = VerifyLoginToken.verifyLoginToken4Map(loginToken);

		Integer accountId = (Integer) m.get("id");
		String phone = (String) m.get("phone");

		//请求体内的参数

		if(accountId==null || accountId==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		/*ChannelExt channelExt = channelService.findByAccountId(accountId.toString());
		if(channelExt==null || channelExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		ChannelExt channelExt = channelService.findByPhone(phone);
		if(channelExt==null || channelExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		channelExt.setAccountId(accountId.longValue());
		channelExt.setPhone(phone);

		channelExt.setUpdatedAt(new Date());
		channelService.update(channelExt);

		channelExt.setPassword("");//不返回密码
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),channelExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//成功购课的学生列表
	@RequestMapping(value = "student/list")
	public ResponseEntity<ResponseJson> list4Student(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		ChannelExt channelExt = channelService.findByAccountId(accountId);
		String channelId = channelExt!=null?String.valueOf(channelExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(channelId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//请求体内的参数
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)

		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		if(StringUtils.isEmpty(courseType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("courseType", courseType);//课程类型(1单课程、2系列课)
		map4Param.put("state", "3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		map4Param.put("channelId", channelId);
		map4Param.put("page", page);

		Integer count = studentCourseOrderService.findCountByCondition4Channel(map4Param);
		List<StudentCourseOrderExt> list = studentCourseOrderService.findListByCondition4Channel(map4Param);

		List<Map> _list = new ArrayList<>();
		for(StudentCourseOrderExt sco : list){
			Long studentId = sco.getStudentId();
			Student student = studentService.findById(studentId);

			String courseName = "";
			if("1".equals(courseType)){
				Course c = courseService.findById(Long.parseLong(sco.getCourseIds()));
				if(c!=null && c.getId()>0) courseName = c.getName();
			}
			if("2".equals(courseType)){
				CourseGroup cg = courseGroupService.findById(sco.getCourseGroupId());
				if(cg!=null && cg.getId()>0) courseName = cg.getName();
			}

			if(student!=null && student.getId()>0){
				Map m = new HashMap();
				m.put("studentId", studentId);
				m.put("studentName", student.getName());
				m.put("studentPhone", student.getPhone());
				m.put("courseName", courseName);
				m.put("payAt", sco.getPayAt());
				_list.add(m);
			}
		}

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}







}
