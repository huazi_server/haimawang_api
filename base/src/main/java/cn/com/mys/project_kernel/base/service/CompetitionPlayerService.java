package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CompetitionPlayerExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface CompetitionPlayerService {

    CompetitionPlayerExt findObjectByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<CompetitionPlayerExt> findListByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition4Vote(Map<String, Object> map4Param);
    List<CompetitionPlayerExt> findListByCondition4Vote(Map<String, Object> map4Param);

    CompetitionPlayerExt findById(Long id);

    Integer insert(CompetitionPlayerExt playerExt);

    Integer update(CompetitionPlayerExt playerExt);

    CompetitionPlayerExt findLastObjectByCondition(Map<String, Object> m);
}
