package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.UserInfoGather;
import cn.com.mys.project_kernel.base.entity.ext.UserInfoGatherExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface UserInfoGatherService {

    Integer insert(UserInfoGather userInfoGather);

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<UserInfoGatherExt> findListByCondition(Map<String, Object> map4Param);
}
