package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.StudentCourseHomeworkMessageMapper;
import cn.com.mys.project_kernel.base.dao.StudentMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.StudentCourseHomeworkMessageMapperExt;
import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.StudentCourseHomeworkMessage;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseHomeworkExt;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseHomeworkMessageExt;
import cn.com.mys.project_kernel.base.service.StudentCourseHomeworkMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class StudentCourseHomeworkMessageServiceImpl implements StudentCourseHomeworkMessageService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public StudentCourseHomeworkMessageMapper studentCourseHomeworkMessageMapper;

	@Autowired
	public StudentCourseHomeworkMessageMapperExt studentCourseHomeworkMessageMapperExt;

	@Autowired
	public StudentMapper studentMapper;



	//设置消息的学生的名称、头像
	private void setNameAndAvatarUrl(StudentCourseHomeworkMessageExt studentCourseHomeworkMessageExt){
		Long studentId = studentCourseHomeworkMessageExt.getStudentId();//学生id
		Student student = studentMapper.selectByPrimaryKey(studentId);
		studentCourseHomeworkMessageExt.setStudentName(student.getName());
		studentCourseHomeworkMessageExt.setStudentAvatarUrl(student.getAvatarUrl());
	}


	@Override
	public Integer insert(StudentCourseHomeworkMessageExt studentCourseHomeworkMessageExt) {
		return studentCourseHomeworkMessageMapper.insertSelective(studentCourseHomeworkMessageExt);
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return studentCourseHomeworkMessageMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<StudentCourseHomeworkMessageExt> findListByCondition(Map<String, Object> map4Param) {
		List<StudentCourseHomeworkMessageExt> list = studentCourseHomeworkMessageMapperExt.findListByCondition(map4Param);
		for(StudentCourseHomeworkMessageExt schm : list){
			setNameAndAvatarUrl(schm);
		}
		return list;
	}

	@Override
	public StudentCourseHomeworkMessage findById(long id) {
		return studentCourseHomeworkMessageMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer update(StudentCourseHomeworkMessage studentCourseHomeworkMessage) {
		return studentCourseHomeworkMessageMapper.updateByPrimaryKeySelective(studentCourseHomeworkMessage);
	}
}
