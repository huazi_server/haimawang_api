package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.StudentCourseHomeworkExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface StudentCourseHomeworkMapperExt {

    StudentCourseHomeworkExt findObjectByCondition(Map<String, Object> m);

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<StudentCourseHomeworkExt> findListByCondition(Map<String, Object> map4Param);

    List<StudentCourseHomeworkExt> findListByCondition4CourseLesson(Map<String, Object> map4Param);

    Integer findAllCountByCondition(Map<String, Object> m);
    Integer findCommitCountByCondition(Map<String, Object> m);
    Integer findOnlineCountByCondition(Map<String, Object> m);
    Integer findUnCheckCountByCondition(Map<String, Object> m);

    Integer update4OnlineFlag(String onlineFlag);

    StudentCourseHomeworkExt findById(long id);



}