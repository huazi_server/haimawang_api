package cn.com.mys.project_kernel.base.entity;

import cn.com.mys.project_kernel.base.entity.base.BaseEntity;
import java.util.Date;

public class Notify extends BaseEntity {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.id
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.ownerType
     *
     * @mbggenerated
     */
    private Integer ownerType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.personId
     *
     * @mbggenerated
     */
    private Long personId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.type
     *
     * @mbggenerated
     */
    private Integer type;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.fromType
     *
     * @mbggenerated
     */
    private Integer fromType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.fromId
     *
     * @mbggenerated
     */
    private Long fromId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.fromName
     *
     * @mbggenerated
     */
    private String fromName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.fromAvatarUrl
     *
     * @mbggenerated
     */
    private String fromAvatarUrl;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.toType
     *
     * @mbggenerated
     */
    private Integer toType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.toId
     *
     * @mbggenerated
     */
    private Long toId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.toName
     *
     * @mbggenerated
     */
    private String toName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.toAvatarUrl
     *
     * @mbggenerated
     */
    private String toAvatarUrl;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.content
     *
     * @mbggenerated
     */
    private String content;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.productionId
     *
     * @mbggenerated
     */
    private Long productionId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.productionName
     *
     * @mbggenerated
     */
    private String productionName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.remark
     *
     * @mbggenerated
     */
    private String remark;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.state
     *
     * @mbggenerated
     */
    private Integer state;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.createdAt
     *
     * @mbggenerated
     */
    private Date createdAt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.updatedAt
     *
     * @mbggenerated
     */
    private Date updatedAt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column notify.delFlag
     *
     * @mbggenerated
     */
    private Integer delFlag;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.id
     *
     * @return the value of notify.id
     *
     * @mbggenerated
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.id
     *
     * @param id the value for notify.id
     *
     * @mbggenerated
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.ownerType
     *
     * @return the value of notify.ownerType
     *
     * @mbggenerated
     */
    public Integer getOwnerType() {
        return ownerType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.ownerType
     *
     * @param ownerType the value for notify.ownerType
     *
     * @mbggenerated
     */
    public void setOwnerType(Integer ownerType) {
        this.ownerType = ownerType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.personId
     *
     * @return the value of notify.personId
     *
     * @mbggenerated
     */
    public Long getPersonId() {
        return personId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.personId
     *
     * @param personId the value for notify.personId
     *
     * @mbggenerated
     */
    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.type
     *
     * @return the value of notify.type
     *
     * @mbggenerated
     */
    public Integer getType() {
        return type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.type
     *
     * @param type the value for notify.type
     *
     * @mbggenerated
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.fromType
     *
     * @return the value of notify.fromType
     *
     * @mbggenerated
     */
    public Integer getFromType() {
        return fromType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.fromType
     *
     * @param fromType the value for notify.fromType
     *
     * @mbggenerated
     */
    public void setFromType(Integer fromType) {
        this.fromType = fromType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.fromId
     *
     * @return the value of notify.fromId
     *
     * @mbggenerated
     */
    public Long getFromId() {
        return fromId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.fromId
     *
     * @param fromId the value for notify.fromId
     *
     * @mbggenerated
     */
    public void setFromId(Long fromId) {
        this.fromId = fromId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.fromName
     *
     * @return the value of notify.fromName
     *
     * @mbggenerated
     */
    public String getFromName() {
        return fromName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.fromName
     *
     * @param fromName the value for notify.fromName
     *
     * @mbggenerated
     */
    public void setFromName(String fromName) {
        this.fromName = fromName == null ? null : fromName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.fromAvatarUrl
     *
     * @return the value of notify.fromAvatarUrl
     *
     * @mbggenerated
     */
    public String getFromAvatarUrl() {
        return fromAvatarUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.fromAvatarUrl
     *
     * @param fromAvatarUrl the value for notify.fromAvatarUrl
     *
     * @mbggenerated
     */
    public void setFromAvatarUrl(String fromAvatarUrl) {
        this.fromAvatarUrl = fromAvatarUrl == null ? null : fromAvatarUrl.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.toType
     *
     * @return the value of notify.toType
     *
     * @mbggenerated
     */
    public Integer getToType() {
        return toType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.toType
     *
     * @param toType the value for notify.toType
     *
     * @mbggenerated
     */
    public void setToType(Integer toType) {
        this.toType = toType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.toId
     *
     * @return the value of notify.toId
     *
     * @mbggenerated
     */
    public Long getToId() {
        return toId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.toId
     *
     * @param toId the value for notify.toId
     *
     * @mbggenerated
     */
    public void setToId(Long toId) {
        this.toId = toId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.toName
     *
     * @return the value of notify.toName
     *
     * @mbggenerated
     */
    public String getToName() {
        return toName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.toName
     *
     * @param toName the value for notify.toName
     *
     * @mbggenerated
     */
    public void setToName(String toName) {
        this.toName = toName == null ? null : toName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.toAvatarUrl
     *
     * @return the value of notify.toAvatarUrl
     *
     * @mbggenerated
     */
    public String getToAvatarUrl() {
        return toAvatarUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.toAvatarUrl
     *
     * @param toAvatarUrl the value for notify.toAvatarUrl
     *
     * @mbggenerated
     */
    public void setToAvatarUrl(String toAvatarUrl) {
        this.toAvatarUrl = toAvatarUrl == null ? null : toAvatarUrl.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.content
     *
     * @return the value of notify.content
     *
     * @mbggenerated
     */
    public String getContent() {
        return content;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.content
     *
     * @param content the value for notify.content
     *
     * @mbggenerated
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.productionId
     *
     * @return the value of notify.productionId
     *
     * @mbggenerated
     */
    public Long getProductionId() {
        return productionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.productionId
     *
     * @param productionId the value for notify.productionId
     *
     * @mbggenerated
     */
    public void setProductionId(Long productionId) {
        this.productionId = productionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.productionName
     *
     * @return the value of notify.productionName
     *
     * @mbggenerated
     */
    public String getProductionName() {
        return productionName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.productionName
     *
     * @param productionName the value for notify.productionName
     *
     * @mbggenerated
     */
    public void setProductionName(String productionName) {
        this.productionName = productionName == null ? null : productionName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.remark
     *
     * @return the value of notify.remark
     *
     * @mbggenerated
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.remark
     *
     * @param remark the value for notify.remark
     *
     * @mbggenerated
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.state
     *
     * @return the value of notify.state
     *
     * @mbggenerated
     */
    public Integer getState() {
        return state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.state
     *
     * @param state the value for notify.state
     *
     * @mbggenerated
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.createdAt
     *
     * @return the value of notify.createdAt
     *
     * @mbggenerated
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.createdAt
     *
     * @param createdAt the value for notify.createdAt
     *
     * @mbggenerated
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.updatedAt
     *
     * @return the value of notify.updatedAt
     *
     * @mbggenerated
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.updatedAt
     *
     * @param updatedAt the value for notify.updatedAt
     *
     * @mbggenerated
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column notify.delFlag
     *
     * @return the value of notify.delFlag
     *
     * @mbggenerated
     */
    public Integer getDelFlag() {
        return delFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column notify.delFlag
     *
     * @param delFlag the value for notify.delFlag
     *
     * @mbggenerated
     */
    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }
}