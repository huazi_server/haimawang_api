package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ProductionTypeMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ProductionTypeMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ProductionTypeExt;
import cn.com.mys.project_kernel.base.service.ProductionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ProductionTypeServiceImpl implements ProductionTypeService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ProductionTypeMapper productionTypeMapper;

	@Autowired
	public ProductionTypeMapperExt productionTypeMapperExt;


	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return productionTypeMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ProductionTypeExt> findListByCondition(Map<String, Object> map4Param) {
		return productionTypeMapperExt.findListByCondition(map4Param);
	}
}
