package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ContestProduction;

import java.util.List;

public class ContestProductionExt extends ContestProduction {
    private ContestPlayerExt contestPlayerInfo;
    public ContestPlayerExt getContestPlayerInfo() {
        return contestPlayerInfo;
    }
    public void setContestPlayerInfo(ContestPlayerExt contestPlayerInfo) {
        this.contestPlayerInfo = contestPlayerInfo;
    }


    List<ContestProductionScoreExt> contestProductionScoreInfoList;
    public List<ContestProductionScoreExt> getContestProductionScoreInfoList() {
        return contestProductionScoreInfoList;
    }
    public void setContestProductionScoreInfoList(List<ContestProductionScoreExt> contestProductionScoreInfoList) {
        this.contestProductionScoreInfoList = contestProductionScoreInfoList;
    }

    String reviewProgress;//评审进度
    public String getReviewProgress() {
        return reviewProgress;
    }
    public void setReviewProgress(String reviewProgress) {
        this.reviewProgress = reviewProgress;
    }
}