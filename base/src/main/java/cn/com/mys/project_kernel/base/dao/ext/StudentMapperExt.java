package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.ext.StudentExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface StudentMapperExt {

    StudentExt findByAccountId(String accountId);

    Integer updateById(StudentExt studentExt);

    StudentExt findObjectById(Long id);

    StudentExt findByPhone(String phone);

    Integer insert4Ignore(StudentExt studentExt);

    Integer findCountByCondition4Homework(Map<String, Object> map4Param);
    List<StudentExt> findListByCondition4Homework(Map<String, Object> map4Param);

    Integer findCountByCondition4Course(Map<String, Object> map4Param);
    List<StudentExt> findListByCondition4Course(Map<String, Object> map4Param);
}