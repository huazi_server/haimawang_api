package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ReportMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ReportMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ReportExt;
import cn.com.mys.project_kernel.base.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service的实现类
 *
 */

@Service
public class ReportServiceImpl implements ReportService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ReportMapper reportMapper;

	@Autowired
	public ReportMapperExt reportMapperExt;


	@Override
	public Integer insert(ReportExt reportExt) {
		return reportMapper.insertSelective(reportExt);
	}
}
