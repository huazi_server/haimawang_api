package cn.com.mys.project_kernel.base.util;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Map工具类
 * 排序
 * 
 */

public class MapUtils {


	//按键排序
	public Map<String, String> sortMapByKey(Map<String, String> oriMap) {
		if (oriMap == null || oriMap.isEmpty()) {
			return null;
		}
		Map<String, String> sortedMap = new TreeMap<String, String>(new Comparator<String>() {
			public int compare(String key1, String key2) {
				int intKey1 = 0, intKey2 = 0;
				try {
					intKey1 = getInt(key1);
					intKey2 = getInt(key2);
				} catch (Exception e) {
					intKey1 = 0;
					intKey2 = 0;
				}
				return intKey1 - intKey2;
			}});
		sortedMap.putAll(oriMap);
		return sortedMap;
	}

	private int getInt(String str) {
		int i = 0;
		try {
			Pattern p = Pattern.compile("^\\d+");
			Matcher m = p.matcher(str);
			if (m.find()) {
				i = Integer.valueOf(m.group());
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return i;
	}



	//按值排序
	public Map<String, String> sortMapByValue(Map<String, String> oriMap) {
		Map<String, String> sortedMap = new LinkedHashMap<String, String>();
		if (oriMap != null && !oriMap.isEmpty()) {
			List<Map.Entry<String, String>> entryList = new ArrayList<Map.Entry<String, String>>(oriMap.entrySet());
			Collections.sort(entryList,
					new Comparator<Map.Entry<String, String>>() {
						public int compare(Map.Entry<String, String> entry1,
										   Map.Entry<String, String> entry2) {
							int value1 = 0, value2 = 0;
							try {
								value1 = getInt(entry1.getValue());
								value2 = getInt(entry2.getValue());
							} catch (NumberFormatException e) {
								value1 = 0;
								value2 = 0;
							}
							return value2 - value1;
						}
					});
			Iterator<Map.Entry<String, String>> iter = entryList.iterator();
			Map.Entry<String, String> tmpEntry = null;
			while (iter.hasNext()) {
				tmpEntry = iter.next();
				sortedMap.put(tmpEntry.getKey(), tmpEntry.getValue());
			}
		}
		return sortedMap;
	}




	/**
	 * 将map键值都转为字符串
	 * @param
	 * @return
	 */
	public static Map parameter2Map(Map parameter) {
		//定义返回map
		Map<String, Object> map = new HashMap<String, Object>();
		//获取迭代器
		Iterator entries = parameter.entrySet().iterator();
		Map.Entry entry;

		//根据迭代器遍历
		while (entries.hasNext()) {
			String name = "";
			String value = "";
			entry = (Map.Entry) entries.next();
			//获取key
			name = (String) entry.getKey();
			//获取value
			Object valueObj = entry.getValue();
			/*
			 * 处理value
			 */
			if(valueObj == null){
				value = "";
			}else if(valueObj instanceof String[]){
				String[] values = (String[])valueObj;
				for(int i=0;i<values.length;i++){
					value += values[i] + ",";
				}
				//如果有同名参数,就将数组转成字符串,多个","分隔
				value = value.substring(0, value.length()-1);
			}else{
				value = valueObj.toString();
			}
			//设置map
			map.put(name, value);
		}
		return map;
	}





}
