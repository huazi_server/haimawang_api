package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.StudentCourseOrderFlowMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.StudentCourseOrderFlowMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseOrderFlowExt;
import cn.com.mys.project_kernel.base.service.StudentCourseOrderFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service的实现类
 *
 */

@Service
public class StudentCourseOrderFlowServiceImpl implements StudentCourseOrderFlowService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public StudentCourseOrderFlowMapper studentCourseOrderFlowMapper;

	@Autowired
	public StudentCourseOrderFlowMapperExt studentCourseOrderFlowMapperExt;


	@Override
	public Integer insert(StudentCourseOrderFlowExt studentCourseOrderFlowExt) {
		return studentCourseOrderFlowMapper.insertSelective(studentCourseOrderFlowExt);
	}
}
