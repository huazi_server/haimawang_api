package cn.com.mys.project_kernel.base.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import cn.com.mys.project_kernel.base.service.TbTestService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;


@PersistJobDataAfterExecution	//保存在JobDataMap传递的参数(静态属性:如当你要一个计数器的时候)
@DisallowConcurrentExecution	//保证多个任务间不会同时执行.不允许调度框架在同一时刻调用Job类
public class ClusterQuartzJob4Test extends QuartzJobBean {

	Logger logger = LoggerFactory.getLogger(ClusterQuartzJob4Test.class);
	
	//这个属性如不是static,那么每次都要实例这个任务类,始终打印为: 1  
    //private static int _counter = 1;
    
    
    @Autowired
	public TbTestService tbTestService;
	
    
	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss"); 

		/*logger.info("---------TaskOneJob----------->>>>sleep:"+df.format(new Date()));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
		
		
		logger.info("---------TaskOneJob----------->>>>time:"+df.format(new Date()));
		
		/*_counter++;
		logger.info("---------TaskOneJob----------->>>>_counter:"+_counter);*/
		
		
		/*Teacher teacher = teacherService.findById(20111265);
		System.out.println("--"+teacher.getId()+"--"+teacher.getName());*/
		
		
		logger.info("-------------------------------------------");
	    
	}
	
	
}
