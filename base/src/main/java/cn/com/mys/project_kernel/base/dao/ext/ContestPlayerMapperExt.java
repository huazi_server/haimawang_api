package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CompetitionPlayerExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestPlayerExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ContestPlayerMapperExt {

    ContestPlayerExt findObjectByCondition(Map<String, Object> map4Param);

    CompetitionPlayerExt findLastObjectByCondition(Map<String, Object> m);

    Integer findCountByCondition4Production(Map<String, Object> map4Param);

    List<ContestPlayerExt> findListByCondition4Production(Map<String, Object> map4Param);

    ContestPlayerExt findById(Long playerId);

    Integer findCountByCondition4Vote(Map<String, Object> map4Param);

    List<ContestPlayerExt> findListByCondition4Vote(Map<String, Object> map4Param);

    Integer findCountByCondition4Score(Map<String, Object> map4Param);

    List<ContestPlayerExt> findListByCondition4Score(Map<String, Object> map4Param);

    Integer findSignupCountByContestId(Long contestId);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ContestPlayerExt> findListByCondition(Map<String, Object> map4Param);
}