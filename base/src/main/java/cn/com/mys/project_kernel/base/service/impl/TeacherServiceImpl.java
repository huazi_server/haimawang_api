package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.TeacherMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.TeacherMapperExt;
import cn.com.mys.project_kernel.base.entity.Teacher;
import cn.com.mys.project_kernel.base.entity.ext.TeacherExt;
import cn.com.mys.project_kernel.base.service.TeacherService;
import cn.com.mys.project_kernel.base.util.BeanConverterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class TeacherServiceImpl implements TeacherService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public TeacherMapper teacherMapper;

	@Autowired
	public TeacherMapperExt teacherMapperExt;


	@Override
	public TeacherExt findByAccountId(String accountId) {
		return teacherMapperExt.findByAccountId(accountId);
	}

	@Override
	public Integer updateById(TeacherExt teacherExt) {
		return teacherMapperExt.updateById(teacherExt);
	}

	@Override
	public Teacher findById(long id) {
		return teacherMapper.selectByPrimaryKey(id);
	}

	@Override
	public TeacherExt findObjectById(Long id) {
		return teacherMapperExt.findObjectById(id);
	}

	@Override
	public Integer insert(TeacherExt teacherExt) {
		return teacherMapper.insertSelective(teacherExt);
	}

	@Override
	public List<TeacherExt> findListByType(Map<String, Object> map4Param) {
		return teacherMapperExt.findListByType(map4Param);
	}

	@Override
	public TeacherExt findByPhone(String phone) {
		return teacherMapperExt.findByPhone(phone);
	}

	@Override
	public Integer updateOtherByAccountId(Map<String, Object> mm) {
		return teacherMapperExt.updateOtherByAccountId(mm);
	}
}
