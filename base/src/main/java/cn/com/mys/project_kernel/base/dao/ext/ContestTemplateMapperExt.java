package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestReviewTeacherExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestTemplateExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ContestTemplateMapperExt {

    ContestTemplateExt findById(Long id);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ContestReviewTeacherExt> findListByCondition(Map<String, Object> map4Param);
}