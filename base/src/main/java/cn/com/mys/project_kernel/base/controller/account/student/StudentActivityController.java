package cn.com.mys.project_kernel.base.controller.account.student;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.*;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 学生活动
 *
 */
@Controller
@RequestMapping(value = "/account/student/activity")
public class StudentActivityController {

	Log log = LogFactory.getLog(StudentActivityController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private CourseLessonService courseLessonService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private ActivityFreeService activityFreeService;
	@Autowired
	private StudentCourseService studentCourseService;
	@Autowired
	private StudentCourseOrderService studentCourseOrderService;
	@Autowired
	private CourseGroupService courseGroupService;

	@Autowired
	private ChannelService channelService;
	@Autowired
	private ChannelStatisticsService channelStatisticsService;
	@Autowired
	private ChannelCourseService channelCourseService;

	@Autowired
	private ActivityTuanService activityTuanService;
	@Autowired
	private ActivityTuanOrderService activityTuanOrderService;

	@Autowired
	private TeacherService teacherService;


	@Autowired
	private RandomUserService randomUserService;

	@Autowired
	private StudentDiscountService studentDiscountService;

	@Autowired
	private ChannelActivityService channelActivityService;




	//领取
	@RequestMapping(value = "free/gain")
	public ResponseEntity<ResponseJson> gain4Free(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";


		String activityId = (String) paramMap.get("activityId");//活动id
		String channelId = (String) paramMap.get("channelId");//渠道id

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);

		ActivityFreeExt activityFreeExt = activityFreeService.findObjectByCondition(map4Param);
		if(activityFreeExt==null){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(activityFreeExt.getState()!=null && activityFreeExt.getState()!=2){//状态(1下线、2上线)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动未上线",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if((activityFreeExt.getCourseId()==null || activityFreeExt.getCourseId()==0) && (activityFreeExt.getCourseGroupId()==null || activityFreeExt.getCourseGroupId()==0)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动关联的课程或年课不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> _m = new LinkedHashMap<>();
		_m.put("qrCodeUrl4Official", Constant.qrCodeUrl4Official);
		_m.put("type", 1);//成功1、活动已结束2、已有此课程3、人数到上限4


		String coursePackageIds = "";
		String courseIds = "";

		Integer courseType = 0;//课程类型(1单课程、2系列课)
		Long courseId = activityFreeExt.getCourseId();
		Long courseGroupId = activityFreeExt.getCourseGroupId();

		String _courseId = "";//课程id或年课id

		if(courseId!=null && courseId>0){
			Map<String, Object> cm = new HashMap<String, Object>();
			cm.put("delFlag", "0");
			cm.put("courseId", courseId);
			CourseExt courseExt = courseService.findObjectByCondition(cm);
			activityFreeExt.setCourseInfo(courseExt);
			if(courseExt==null || courseExt.getId()==0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动关联的课程不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}


			courseType = 1;
			coursePackageIds = courseExt.getCoursePackageId().toString();
			courseIds = courseId.toString();
			_courseId = courseId.toString();


			if(courseExt.getState()!=3){
				channelStatisticsService.generateChannelStatistics(channelId, studentId, "3", activityId, courseType.toString(), _courseId, "1");
				_m.put("type", 2);
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"活动报名未开始或已结束",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_m);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			map4Param = new HashMap<String, Object>();
			map4Param.put("studentId",studentId);
			map4Param.put("delFlag","0");
			//map4Param.put("courseId",courseId);
			map4Param.put("coursePackageId",courseExt.getCoursePackageId());
			StudentCourseExt sc = studentCourseService.findObjectByCondition(map4Param);
			if(sc!=null && sc.getId()>0){
				channelStatisticsService.generateChannelStatistics(channelId, studentId, "3", activityId, courseType.toString(), _courseId, "1");
				_m.put("type", 3);
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"您已拥有此课程，无需重复领取。",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_m);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

		}
		if(courseGroupId!=null && courseGroupId>0){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseGroupId", courseGroupId);
			CourseGroupExt courseGroup = courseGroupService.findObjectByCondition(mm);
			if(courseGroup==null || courseGroup.getId()==0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动关联的年课不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}


			courseType = 2;
			coursePackageIds = courseGroup.getCoursePackageIds();
			courseIds = courseGroup.getCourseIds();
			_courseId = courseGroup.getId().toString();


			if(courseGroup.getState()!=2){
				channelStatisticsService.generateChannelStatistics(channelId, studentId, "3", activityId, courseType.toString(), _courseId, "1");
				_m.put("type", 2);
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"活动报名已结束",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_m);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			Map<String, Object> m = new HashMap<>();
			m.put("studentId", studentId);
			m.put("delFlag","0");
			List<StudentCourseExt> studentCourseInfoList = studentCourseService.findListByCondition4CourseGroup(m);
			if(studentCourseInfoList!=null && studentCourseInfoList.size()>0){
				channelStatisticsService.generateChannelStatistics(channelId, studentId, "3", activityId, courseType.toString(), _courseId, "1");
				_m.put("type", 3);
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"您已拥有此课程，无需重复领取。",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_m);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

		}


		Integer quotaCount = activityFreeExt.getQuotaCount()==null?0:activityFreeExt.getQuotaCount();
		Integer gainCount = activityFreeExt.getGainCount()==null?0:activityFreeExt.getGainCount();
		if(quotaCount<=gainCount){
			channelStatisticsService.generateChannelStatistics(channelId, studentId, "3", activityId, courseType.toString(), _courseId, "1");
			_m.put("type", 4);
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"领取人数达到上限("+quotaCount+")",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_m);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}



		String[] cpIds = coursePackageIds.split(",");
		String[] cIds = courseIds.split(",");
		for(int i=0;i<cIds.length;i++){
			String cId = cIds[i];
			String coursePackageId = cpIds[i];
			Course c = courseService.findById(Long.parseLong(cId));
			coursePackageId = (c.getCoursePackageId()!=null&&c.getCoursePackageId()>0)?c.getCoursePackageId().toString():coursePackageId;

			//生成学生课程数据
			Map<String, Object> m = new HashMap<>();
			m.put("courseId", cId);
			m.put("coursePackageId", coursePackageId);
			m.put("studentId", studentId);
			m.put("delFlag", "0");
			StudentCourseExt studentCourseExt = studentCourseService.findObjectByCondition(m);
			synchronized (StudentCourseExt.class) {//加锁,防止出现重复数据
				studentCourseExt = studentCourseService.findObjectByCondition(m);
				if (studentCourseExt == null || studentCourseExt.getId() == 0) {
					//生成学生课程信息
					studentCourseExt = new StudentCourseExt();
					studentCourseExt.setStudentId(Long.parseLong(studentId));
					studentCourseExt.setCourseId(Long.parseLong(cId));
					studentCourseExt.setCoursePackageId(Long.parseLong(coursePackageId));
					studentCourseExt.setCourseGroupId(courseGroupId);
					studentCourseExt.setSourceType(2);//来源类型(正常购买1、免费领2、拼团购3、直接生成4)
					studentCourseExt.setSourceId(Long.parseLong(activityId));//来源id(订单id或活动id)
					studentCourseExt.setCourseStartHintFlag(1);//课程开始提醒标识(1未提醒、2已提醒)
					studentCourseExt.setFullStudyFlag(1);//是否全部学习标识(1否、2是)
					studentCourseExt.setFullFinishFlag(1);//是否全部完成标识(1否、2是)

					studentCourseExt.setRemark("");
					studentCourseExt.setState(1);//状态(1未分配教师、2已分配教师)
					studentCourseExt.setCreatedAt(new Date());
					studentCourseExt.setUpdatedAt(new Date());
					studentCourseExt.setDelFlag(0);

					studentCourseService.insert(studentCourseExt);
				}
			}


			//do nothing
			log.info("generateTeacherCourseAndStudentCourseData");
			if(Constant.autoChooseTeacherFlag.intValue()==2) {//支付完自动分班标识(1否、2是)
				//生成教师课程、学生课程信息
				log.info("[generateTeacherCourseAndStudentCourseData]:"+"	"+"studentId="+studentCourseExt.getStudentId()+",coursePackageId="+studentCourseExt.getCoursePackageId()+",courseId="+studentCourseExt.getCourseId());
				studentCourseOrderService.generateTeacherCourseAndStudentCourseData(studentCourseExt.getStudentId().toString(), studentCourseExt.getCoursePackageId().toString(), studentCourseExt.getCourseId().toString());
			}
		}



		activityFreeExt.setGainCount(gainCount+1);
		activityFreeService.update(activityFreeExt);

		channelStatisticsService.generateChannelStatistics(channelId, studentId, "4", activityId, courseType.toString(), _courseId, "2");

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	/************************************************************************************************************/
	/************************************************************************************************************/
	/************************************************************************************************************/



	//查询可以加入的拼团订单列表
	@RequestMapping(value = "tuan/order/list")
	public ResponseEntity<ResponseJson> list4TuanOrder(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String activityId = (String) paramMap.get("activityId");//拼团活动id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"拼团活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);
		ActivityTuanExt activityTuanExt = activityTuanService.findObjectByCondition(map4Param);


		Integer count = 0;
		List<ActivityTuanOrderExt> list = new ArrayList<>();
		if(activityTuanExt!=null && activityTuanExt.getId()>0){
			Integer restrictionCount = activityTuanExt.getRestrictionCount();//成团限制人数

			Map<String, Object> m = new HashMap<String, Object>();
			m.put("delFlag", "0");
			m.put("activityTuanId", activityId);
			m.put("page", page);
			m.put("state", "1");//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
			m.put("restrictionCount", restrictionCount.toString());//已有人数小于成团限制人数
			m.put("currentDate", new Date());//当前时间

			count = activityTuanOrderService.findCountByCondition(m);
			list = activityTuanOrderService.findListByCondition(m);
		}


		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询拼团订单详情
	@RequestMapping(value = "tuan/order/detail")
	public ResponseEntity<ResponseJson> detail4TuanOrder(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String activityTuanOrderId = (String) paramMap.get("activityTuanOrderId");//拼团订单id
		String channelId = (String) paramMap.get("channelId");//渠道id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		if(StringUtils.isEmpty(activityTuanOrderId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"拼团订单id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> map4Param = new HashMap<>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityTuanOrderId", activityTuanOrderId);
		ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderService.findObjectByCondition(map4Param);
		if(activityTuanOrderExt==null || activityTuanOrderExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此拼团订单不存在或已过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(activityTuanOrderExt.getState()==0){//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此拼团订单不存在或已过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(activityTuanOrderExt.getActivityTuanId()==null || activityTuanOrderExt.getActivityTuanId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> m = new HashMap<String, Object>();
		m.put("delFlag", "0");
		m.put("activityId", activityTuanOrderExt.getActivityTuanId().toString());
		ActivityTuanExt activityTuanExt = activityTuanService.findObjectByCondition(m);
		if(activityTuanExt==null || activityTuanExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		String activityId = activityTuanExt.getId().toString();
		ChannelActivityExt channelActivityExt = null;
		if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(activityId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("state", "1");//状态(0初始化、1正常)
			mm.put("activityType", "1");//活动类型(0免费领、1拼团)
			mm.put("channelId", channelId);
			mm.put("activityId", activityId);
			channelActivityExt = channelActivityService.findObjectByCondition(mm);
			if(channelActivityExt==null){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道活动不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(channelActivityExt.getPrice()==null || channelActivityExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelActivityExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格信息不完整",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}



		Integer stopFlag = 1;//结束标识(1未结束、2已结束)
		if(activityTuanExt.getEndAt()!=null && activityTuanExt.getEndAt().getTime()<new Date().getTime()){
			stopFlag = 2;
		}
		activityTuanExt.setStopFlag(stopFlag);


		Integer joinFlag = 1;//参加标识(1未参加、已参加)
		if(StringUtils.isNotEmpty(studentId)){
			Map<String, Object> m4PinTuan = new HashMap<>();
			m4PinTuan.put("delFlag", "0");
			m4PinTuan.put("activityTuanId", activityTuanExt.getId().toString());
			m4PinTuan.put("studentId", studentId);
			m4PinTuan.put("state", "1,2");//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
			ActivityTuanOrderExt ato = activityTuanOrderService.findObjectByActivityId4Join(m4PinTuan);
			if(ato!=null && ato.getId()>0){
				joinFlag = 2;
			}
		}
		activityTuanExt.setJoinFlag(joinFlag);


		if(Constant.createValidFlag4PinTuan==2){
			Integer createPinTuanOrderFlag = 1;//能否新建拼团订单标识(1是、2否)
			if(activityTuanExt.getEndAt()!=null && DateUtils.addHours(activityTuanExt.getEndAt(), 0-Constant.createEndDate4PinTuan).getTime()<new Date().getTime()){
				createPinTuanOrderFlag = 2;//开团有效时间已过
			}
			activityTuanExt.setCreatePinTuanOrderFlag(createPinTuanOrderFlag);
		}


		activityTuanExt.setChannelActivityInfo(channelActivityExt);

		Map<String, Object> _m = new HashMap<String, Object>();
		_m.put("activityTuanInfo", activityTuanExt);
		_m.put("activityTuanOrderInfo", activityTuanOrderExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}





	//查询拼团的个人订单列表
	@RequestMapping(value = "tuan/order/oneself/list")
	public ResponseEntity<ResponseJson> list4TuanOrderOneself(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("page", page);

		map4Param.put("state", "");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)

		map4Param.put("caseValue", "2");//所有订单1、拼团的个人订单2

		Integer count = studentCourseOrderService.findCountByCondition(map4Param);
		List<StudentCourseOrderExt> list = studentCourseOrderService.findListByCondition(map4Param);



		for(StudentCourseOrderExt order : list){

			//设置教师的二维码图片地址
			CourseExt c = order.getCourseInfo();
			if(c!=null && c.getId()>0){
				Map<String, Object> m = new HashMap<>();
				m.put("courseId", c.getId().toString());
				m.put("studentId", studentId);
				m.put("delFlag", "0");
				CourseExt courseExt = courseService.findObjectByCondition(m);

				Map<String, Object> mm = new HashMap<>();
				mm.put("courseId", c.getId().toString());
				mm.put("studentId", studentId);
				mm.put("delFlag", "0");
				StudentCourseExt studentCourseExt = studentCourseService.findObjectByCondition(mm);

				String qrCodeUrl4Teacher = "";
				if(studentCourseExt!=null && studentCourseExt.getId()>0 && studentCourseExt.getTeacherId()!=null && studentCourseExt.getTeacherId()>0){
					Teacher teacher = teacherService.findById(studentCourseExt.getTeacherId());
					if(teacher!=null && teacher.getId()>0){
						qrCodeUrl4Teacher = teacher.getQrCodeUrl();
					}
				}
				c.setQrCodeUrl4Teacher(qrCodeUrl4Teacher);
			}
		}


		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询拼团的个人订单详情
	@RequestMapping(value = "tuan/order/oneself/detail")
	public ResponseEntity<ResponseJson> detail4TuanOrderOneself(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String orderNo = (String) paramMap.get("orderNo");//订单编号

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(orderNo)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"订单编号不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//查询订单信息
		JSONObject obj = new JSONObject();
		obj.put("loginToken", loginToken);
		obj.put("tradeNo", orderNo);
		obj.put("loginProductId", Constant.loginProductId);
		obj.put("loginProductKey", Constant.loginProductKey);

		JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("detailUrl"), obj);
		log.info("--"+jo.toString());

		/**
		 {
		 "result": true,
		 "duration": 391,
		 "type": "Detail execute",
		 "code": 200,
		 "message": "Success",
		 "data": {
		 "payAmount": 50,
		 "detail": "详情",
		 "accountId": 20111345,
		 "tradeNo": "114542864360275968",
		 "refundTotalAmount": 0,
		 "state": 0,
		 "productId": 20111105,
		 "updatedAt": 1557973135000,
		 "id": 9,
		 "payTotalAmount": 0,
		 "title": "课程",
		 "refundAmount": 0,
		 "orderAmount": 100,
		 "createdAt": 1557916908000,
		 "payType": "weixin-web",
		 "payExpireSecond": 60,
		 "delFlag": 0,
		 "payInfoListInfo": [{
		 "id": 25,
		 "tradeId": 9,
		 "accountId": 20111345,
		 "type": "weixin-web",
		 "outTradeNo": "91557973134828",
		 "body": "课程",
		 "detail": "详情",
		 "infos": null,
		 "feeType": null,
		 "feeAmount": 50,
		 "feeSuccessAmount": 0,
		 "refundTotalAmount": 0,
		 "refundAmount": 0,
		 "spbillCreateIp": null,
		 "timeStartAt": null,
		 "timeExpireSecond": null,
		 "prepayId": null,
		 "request": null,
		 "result": null,
		 "state": 0,
		 "updatedAt": 1557973135000,
		 "createdAt": 1557973135000,
		 "delFlag": 0
		 },{
		 "id": 21,
		 "tradeId": 9,
		 "accountId": 20111345,
		 "type": "weixin-web",
		 "outTradeNo": "91557916908422",
		 "body": "课程2",
		 "detail": "课程2",
		 "infos": null,
		 "feeType": null,
		 "feeAmount": 1000,
		 "feeSuccessAmount": 0,
		 "refundTotalAmount": 0,
		 "refundAmount": 0,
		 "spbillCreateIp": null,
		 "timeStartAt": null,
		 "timeExpireSecond": null,
		 "prepayId": null,
		 "request": null,
		 "result": null,
		 "state": 0,
		 "updatedAt": 1557918176000,
		 "createdAt": 1557916908000,
		 "delFlag": 0
		 }],
		 "refundInfoListInfo": []
		 },
		 "error": null,
		 "version": null,
		 "extParam": null
		 }
		 */



		String type = "1";//类型(1支付、2退款)

		//查询订单信息
		StudentCourseOrderExt order = studentCourseOrderService.findByOrderNo(orderNo);
		Long atoId = 0L;
		if(order!=null && order.getId()>0){
			atoId = order.getActivityTuanOrderId();//拼团订单id
		}


		//类型(1支付、2退款)
		if(StringUtils.isNotEmpty(type) && "1".equals(type)){//支付

			if(order!=null && order.getState()==1){//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
				if(jo!=null && jo.getInt("code")>0 && jo.getInt("code")==200){
					JSONObject jo_data = jo.getJSONObject("data");
					int orderAmount = jo_data.getInt("orderAmount");//订单金额
					int payTotalAmount = jo_data.getInt("payTotalAmount");//支付总金额
					int refundTotalAmount = jo_data.getInt("refundTotalAmount");//退款总金额

					Double d = order.getOrderMoney().doubleValue()*100;//订单总金额,单位:分
					Double p = d-(order.getDiscountMoney()==null?0:order.getDiscountMoney().doubleValue()*100);//支付总金额,单位:分
					if(d.intValue()==orderAmount && p.intValue()==payTotalAmount){//判断订单是否支付成功

						if(order.getState()==1){
							order.setPayMoney(BigDecimal.valueOf(p/100.0));
							order.setPayAt(new Date());
							order.setState(3);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
							order.setUpdatedAt(new Date());
							order.setDelFlag(0);
							studentCourseOrderService.update(order);
						}
						//删除学生优惠券数据
						Map<String, Object> mm = new HashMap<>();
						mm.put("discountId", order.getDiscountId().toString());//优惠券id
						mm.put("studentId", order.getStudentId().toString());
						mm.put("state", "1");//状态(0未使用、1已使用)
						mm.put("delFlag", "1");
						studentDiscountService.update4Map(mm);



						if(order.getActivityTuanOrderId()==null || order.getActivityTuanOrderId()==0){
							//生成学生课程数据
							activityTuanOrderService.generateStudentCourseData(order);



							String channelId = order.getChannelId()==null?"":order.getChannelId().toString();
							studentId = order.getStudentId().toString();
							String courseType = order.getCourseType().toString();//课程类型(1单课程、2系列课)
							Long courseGroupId = order.getCourseGroupId();
							String courseIds = order.getCourseIds();
							String cId = "";
							String cgId = "";
							if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
								cId = courseIds;
							}
							if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
								cgId = courseGroupId.toString();
							}
							//根据渠道课程的优惠券的关系生成学生优惠券信息
							if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(courseType) && StringUtils.isNotEmpty(studentId)){
								studentDiscountService.insert(channelId, courseType, studentId, cId, cgId, order.getId().toString());
							}



							//更新购课人数限制
							courseService.updateCourseEnrollCount(cId);
						}



						Integer _sourceType = 0;//来源类型(购买单课程1、购买系列课2、活动领取失败3、活动领取成功4)
						Long _courseId = 0L;//单课程id或系列课id
						if(order.getCourseType()==1){
							_sourceType = 1;
							_courseId = StringUtils.isNotEmpty(order.getCourseIds())?Long.parseLong(order.getCourseIds()):_courseId;
						}
						if(order.getCourseType()==2){
							_sourceType = 2;
							_courseId = order.getCourseGroupId();
						}
						channelStatisticsService.generateChannelStatistics(order.getChannelId()!=null?order.getChannelId().toString():"", order.getStudentId().toString(), _sourceType.toString(), order.getId().toString(), order.getCourseType().toString(), _courseId.toString(), "2");

					}

				}

			}


			/*order.setUpdatedAt(new Date());
			order.setDelFlag(0);
			studentCourseOrderService.update(order);*/

		}

		if(StringUtils.isNotEmpty(type) && "2".equals(type)){//退款
			//do nothing
		}


		//查询订单信息
		Map<String, Object> m = new HashMap<>();
		StudentCourseOrderExt orderInfo = studentCourseOrderService.findByOrderNo(orderNo);
		if(orderInfo!=null && orderInfo.getId()>0){
			Map<String, Object> mm = new HashMap<>();
			mm.put("sourceId", orderInfo.getId().toString());
			mm.put("studentId", studentId);
			mm.put("delFlag", "0");
			StudentCourseExt studentCourseExt = studentCourseService.findObjectByCondition(mm);
			String qrCodeUrl4Teacher = "";
			if(studentCourseExt!=null && studentCourseExt.getId()>0 && studentCourseExt.getTeacherId()!=null && studentCourseExt.getTeacherId()>0){
				Teacher teacher = teacherService.findById(studentCourseExt.getTeacherId());
				if(teacher!=null && teacher.getId()>0){
					qrCodeUrl4Teacher = teacher.getQrCodeUrl();
				}
			}


			Integer pinTuanChangeFlag = 1;//拼团订单改变标识(1未变、2已变)
			if(atoId!=null && atoId>0){
				Long activityTuanOrderId = orderInfo.getActivityTuanOrderId();
				if(activityTuanOrderId!=null && activityTuanOrderId>0 && atoId.longValue()!=activityTuanOrderId.longValue()){
					pinTuanChangeFlag = 2;
				}
			}

			Integer pinTuanOrderUpdateFlag = 1;//个人订单支付完，拼团订单是否更新标识(1否、2是)
			Integer restrictionCount = 0;
			Long activityTuanOrderId = orderInfo.getActivityTuanOrderId();
			if(activityTuanOrderId!=null && activityTuanOrderId>0){
				ActivityTuanOrder activityTuanOrder = activityTuanOrderService.findById(activityTuanOrderId);
				if(activityTuanOrder!=null && activityTuanOrder.getId()>0){
					String studentOrderIds = activityTuanOrder.getStudentOrderIds();
					if(StringUtils.isNotEmpty(studentOrderIds)){
						if(Arrays.asList(studentOrderIds.split(",")).contains(orderInfo.getId().toString())){
							pinTuanOrderUpdateFlag = 2;
						}
					}
				}
				if(activityTuanOrder!=null && activityTuanOrder.getId()>0 && activityTuanOrder.getActivityTuanId()!=null && activityTuanOrder.getActivityTuanId()>0){
					ActivityTuan activityTuan = activityTuanService.findById(activityTuanOrder.getActivityTuanId());
					if(activityTuan!=null && activityTuan.getId()>0){
						restrictionCount = activityTuan.getRestrictionCount();
					}
				}
			}
			orderInfo.setPinTuanOrderUpdateFlag(pinTuanOrderUpdateFlag);

			m.put("orderInfo", orderInfo);
			m.put("qrCodeUrl4Teacher", qrCodeUrl4Teacher);
			m.put("pinTuanChangeFlag", pinTuanChangeFlag);
			m.put("restrictionCount", restrictionCount);
		}




		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}






	//创建订单
	@RequestMapping(value = "tuan/order/create")
	public ResponseEntity<ResponseJson> create4TuanOrder(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");//课程id
		String courseGroupId = (String) paramMap.get("courseGroupId");//课程分组id
		String payChannel = (String) paramMap.get("payChannel");//支付渠道(1微信、2支付宝)
		String payType = (String) paramMap.get("payType");//支付方式(weixin-web|weixin-app|weixin-jsapi)

		String channelId = (String) paramMap.get("channelId");//渠道id

		String discountId = (String) paramMap.get("discountId");//优惠券id

		String activityId = (String) paramMap.get("activityId");//活动id
		String activityTuanOrderId = (String) paramMap.get("activityTuanOrderId");//拼团订单id

		String appId = (String) paramMap.get("appId");
		String appKey = (String) paramMap.get("appKey");

		int sourceType = 0;//来源类型(1WEB端、2小程序、3公众号)
		if(StringUtils.isNotEmpty(appId) && Constant.appId_web.equals(appId)){
			sourceType = 1;
		}
		if(StringUtils.isNotEmpty(appId) && Constant.appId_xiaochengxu.equals(appId)){
			sourceType = 2;
		}
		if(StringUtils.isNotEmpty(appId) && Constant.appId_gongzhonghao.equals(appId)){
			sourceType = 3;
		}



		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) && StringUtils.isEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id或年课id不能同时为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseId) && StringUtils.isNotEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id或年课id不能同时存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(payChannel) || StringUtils.isEmpty(payType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"支付渠道、支付方式不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//验证支付方式
		if("1".equals(payChannel) && !"weixin-web".equalsIgnoreCase(payType) && !"weixin-app".equalsIgnoreCase(payType) && !"weixin-jsapi".equalsIgnoreCase(payType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"支付方式有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);

		ActivityTuanExt activityTuanExt = activityTuanService.findObjectByCondition(map4Param);
		if(activityTuanExt==null || activityTuanExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ChannelActivityExt channelActivityExt = null;
		if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(activityId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("state", "1");//状态(0初始化、1正常)
			mm.put("activityType", "1");//活动类型(0免费领、1拼团)
			mm.put("channelId", channelId);
			mm.put("activityId", activityId);
			channelActivityExt = channelActivityService.findObjectByCondition(mm);
			if(channelActivityExt==null){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道活动不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(channelActivityExt.getPrice()==null || channelActivityExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelActivityExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格信息不完整",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(activityTuanExt!=null && activityTuanExt.getState()!=null && activityTuanExt.getState()!=1){//状态(0未上线、1已上线)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动未上线",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		Date startAt = activityTuanExt.getStartAt();
		Date endAt = activityTuanExt.getEndAt();
		if(startAt!=null && startAt.getTime()>new Date().getTime()){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动未开始",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(endAt!=null && endAt.getTime()<new Date().getTime()){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动已结束",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(Constant.createValidFlag4PinTuan==2){
			if(StringUtils.isEmpty(activityTuanOrderId) || "0".equals(activityTuanOrderId)){
				if(activityTuanExt.getEndAt()!=null && DateUtils.addHours(activityTuanExt.getEndAt(), 0-Constant.createEndDate4PinTuan).getTime()<new Date().getTime()){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"开团有效时间已过",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
		}



		Map<String, Object> mm4PinTuan = new HashMap<>();
		mm4PinTuan.put("delFlag", "0");
		mm4PinTuan.put("activityTuanId", activityId);
		mm4PinTuan.put("studentId", studentId);
		mm4PinTuan.put("state", "1,2");//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
		ActivityTuanOrderExt ato = activityTuanOrderService.findObjectByActivityId4Join(mm4PinTuan);
		if(ato!=null && ato.getId()>0){
			//查询个人订单
			Map<String, Object> om = new HashMap<>();
			om.put("delFlag", "0");
			om.put("activityTuanId", activityId);
			om.put("studentId", studentId);
			om.put("state", "3,4");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			List<StudentCourseOrderExt> scoList = studentCourseOrderService.findListByActivityTuanOrderId(om);
			if(scoList!=null && scoList.size()>0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复参与此活动",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isNotEmpty(activityTuanOrderId) && Long.parseLong(activityTuanOrderId)>0){
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("activityTuanOrderId", activityTuanOrderId);
			ato = activityTuanOrderService.findObjectByCondition(m);
			if(ato==null || ato.getId()==0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此拼团订单不存在或已过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(ato.getState()!=1){//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"拼团中的订单才能加入",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}



		Integer courseType = 0;//课程类型(1单课程、2系列课)
		String courseGroupIdStr = "0";
		String coursePackageIds = "";
		String courseIds = "";
		BigDecimal realPrice = new BigDecimal(0);

		CourseExt courseInfo = null;
		CourseGroupExt courseGroupInfo = null;

		if(StringUtils.isNotEmpty(courseId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseId", courseId);
			CourseExt course = courseService.findObjectById(mm);
			if(course.getState()!=3){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"报名中的课程才能购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			courseType = 1;
			courseGroupIdStr = "0";
			coursePackageIds = course.getCoursePackageId().toString();
			courseIds = course.getId().toString();
			realPrice = course.getRealPrice();
			courseInfo = course;


			Map<String, Object> m = new HashMap<>();
			m.put("coursePackageId", coursePackageIds);
			m.put("studentId", studentId);
			m.put("delFlag","0");
			StudentCourseExt studentCourseExt = studentCourseService.findObjectByCondition(m);
			Integer buyFlag;//购买标识(1未购买、2已购买)
			if(studentCourseExt!=null && studentCourseExt.getId()>0){
				buyFlag = 2;
			} else{
				buyFlag = 1;
			}
			if(buyFlag == 2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isNotEmpty(courseGroupId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseGroupId", courseGroupId);
			CourseGroupExt courseGroup = courseGroupService.findObjectByCondition(mm);
			if(courseGroup.getState()!=2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"上线中的年课才能购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			courseType = 2;
			courseGroupIdStr = courseGroupId;
			coursePackageIds = courseGroup.getCoursePackageIds();
			courseIds = courseGroup.getCourseIds();
			realPrice = courseGroup.getRealPrice();
			courseGroupInfo = courseGroup;


			Map<String, Object> m = new HashMap<>();
			m.put("studentId", studentId);
			m.put("delFlag","0");
			List<StudentCourseExt> studentCourseInfoList = studentCourseService.findListByCondition4CourseGroup(m);
			Integer buyFlag;//购买标识(1未购买、2已购买)
			if(studentCourseInfoList!=null && studentCourseInfoList.size()>0){
				buyFlag = 2;
			} else{
				buyFlag = 1;
			}
			if(buyFlag == 2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}


		BigDecimal tuanPrice = activityTuanExt.getPrice();
		if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(activityId)){
			if(channelActivityExt!=null){
				if(channelActivityExt.getPrice()!=null){
					tuanPrice = channelActivityExt.getPrice();
				}
			}
		}


		//验证优惠券
		BigDecimal discountMoney = new BigDecimal(0);
		if(StringUtils.isNotEmpty(discountId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("studentId", studentId);
			mm.put("discountId", discountId);
			StudentDiscountExt studentDiscountExt = studentDiscountService.findObjectByCondition(mm);
			if(studentDiscountExt==null || studentDiscountExt.getId()==0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券不属于你或已失效",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(studentDiscountExt.getValidityStart().getTime()>new Date().getTime() || studentDiscountExt.getValidityEnd().getTime()<new Date().getTime()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券不在使用期限内",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(courseType!=studentDiscountExt.getCourseType()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券的课程类型不匹配",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(tuanPrice.compareTo(studentDiscountExt.getMoney()) == -1){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"未达到满减条件",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			Integer type = studentDiscountExt.getType();//类型(0抵用券、1折扣券)
			BigDecimal money = studentDiscountExt.getMoney();
			BigDecimal amount = studentDiscountExt.getAmount();
			if(type!=null && type==0){
				discountMoney = amount;
			}
			if(type!=null && type==1){
				discountMoney = tuanPrice.multiply(amount).setScale(2, BigDecimal.ROUND_HALF_UP);//相乘(保留2位小数)
			}
			if(tuanPrice.subtract(discountMoney).compareTo(BigDecimal.ZERO)==-1 || tuanPrice.subtract(discountMoney).compareTo(BigDecimal.ZERO)==0){//当订单金额-优惠金额大于0时才可以
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券优惠价格要小于订单价格",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}else{
			discountId = "0";
		}



		//查询待付款的订单信息
		Map<String, Object> m = new HashMap<>();
		m.put("coursePackageIds", coursePackageIds);
		m.put("studentId", studentId);
		m.put("delFlag","0");
		m.put("state","1");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		m.put("courseType", courseType);
		StudentCourseOrderExt order = studentCourseOrderService.findObjectByCondition(m);
		Integer useExistFlag = Constant.useExistFlag;//是否使用已存在待付款订单的标识(1使用、2不使用)
		if(useExistFlag==1 && order!=null && order.getId()>0){

			order.setCourseType(courseType);
			order.setCourseGroupId(Long.parseLong(courseGroupIdStr));
			order.setCoursePackageIds(coursePackageIds);
			order.setCourseIds(courseIds);
			order.setStudentId(Long.parseLong(studentId));
			order.setPhone(studentExt.getPhone());
			if(StringUtils.isNotEmpty(channelId)){
				order.setChannelId(Long.parseLong(channelId));
			}
			//order.setOrderNo(OrderUtils.getUniqueCode().toString());//订单编号
			order.setSourceType(sourceType);//来源类型(1WEB端、2小程序、3公众号)
			order.setOrderMoney(tuanPrice);
			order.setDiscountId(Long.parseLong(discountId));
			order.setDiscountMoney(discountMoney);
			order.setPayChannel(Integer.parseInt(StringUtils.isEmpty(payChannel)?"0":payChannel));//支付渠道(1微信、2支付宝)
			order.setPayType(payType);
			order.setAppId(paramMap.get("appId")==null?"":(String)paramMap.get("appId"));
			order.setAppKey(paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));

			order.setRemark("");
			order.setState(1);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			order.setCreatedAt(new Date());
			order.setUpdatedAt(new Date());
			order.setDelFlag(0);

			studentCourseOrderService.update(order);
		}else{
			order = new StudentCourseOrderExt();

			order.setCourseType(courseType);
			order.setCourseGroupId(Long.parseLong(courseGroupIdStr));
			order.setCoursePackageIds(coursePackageIds);
			order.setCourseIds(courseIds);
			order.setStudentId(Long.parseLong(studentId));
			order.setPhone(studentExt.getPhone());
			if(StringUtils.isNotEmpty(channelId)){
				order.setChannelId(Long.parseLong(channelId));
			}
			order.setOrderNo(OrderUtils.getUniqueCode().toString());//订单编号
			order.setSourceType(sourceType);//来源类型(1WEB端、2小程序、3公众号)
			order.setOrderMoney(tuanPrice);
			order.setDiscountId(Long.parseLong(discountId));
			order.setDiscountMoney(discountMoney);
			order.setPayChannel(Integer.parseInt(StringUtils.isEmpty(payChannel)?"0":payChannel));//支付渠道(1微信、2支付宝)
			order.setPayType(payType);
			order.setAppId(paramMap.get("appId")==null?"":(String)paramMap.get("appId"));
			order.setAppKey(paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));

			order.setRemark("");
			order.setState(1);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			order.setCreatedAt(new Date());
			order.setUpdatedAt(new Date());
			order.setDelFlag(0);

			studentCourseOrderService.insert(order);
		}


		//处理拼团订单相关逻辑
		if(StringUtils.isEmpty(activityTuanOrderId) || "0".equals(activityTuanOrderId)){
			//创建新的拼团
			Map<String, Object> m4PinTuan = new HashMap<>();
			m4PinTuan.put("delFlag", "0");
			m4PinTuan.put("activityTuanId", activityId);
			m4PinTuan.put("studentId", studentId);
			m4PinTuan.put("state", "0");//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
			ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderService.findObjectByActivityId(m4PinTuan);
			if(activityTuanOrderExt!=null && activityTuanOrderExt.getId()>0){
				activityTuanOrderExt.setStudentId(Long.parseLong(studentId));
				activityTuanOrderExt.setActivityTuanId(Long.parseLong(activityId));
				activityTuanOrderExt.setPeopleCount(0);//初始化为0
				activityTuanOrderExt.setStartAt(new Date());
				activityTuanOrderExt.setEndAt(DateUtils.addHours(new Date(), Constant.createEndDate4PinTuan));
				activityTuanOrderExt.setRemark("");
				activityTuanOrderExt.setState(0);
				activityTuanOrderExt.setCreatedAt(new Date());
				activityTuanOrderExt.setUpdatedAt(new Date());
				activityTuanOrderExt.setDelFlag(0);
				activityTuanOrderService.update(activityTuanOrderExt);
			}else{
				activityTuanOrderExt = new ActivityTuanOrderExt();
				activityTuanOrderExt.setStudentId(Long.parseLong(studentId));
				activityTuanOrderExt.setActivityTuanId(Long.parseLong(activityId));
				activityTuanOrderExt.setPeopleCount(0);//初始化为0
				activityTuanOrderExt.setStartAt(new Date());
				activityTuanOrderExt.setEndAt(DateUtils.addHours(new Date(), Constant.createEndDate4PinTuan));
				activityTuanOrderExt.setRemark("");
				activityTuanOrderExt.setState(0);
				activityTuanOrderExt.setCreatedAt(new Date());
				activityTuanOrderExt.setUpdatedAt(new Date());
				activityTuanOrderExt.setDelFlag(0);
				activityTuanOrderService.insert(activityTuanOrderExt);
			}


			order.setActivityTuanOrderId(activityTuanOrderExt.getId());
			studentCourseOrderService.update(order);
		}else{
			//加入其它拼团
			Map<String, Object> m4PinTuan = new HashMap<>();
			m4PinTuan.put("delFlag", "0");
			m4PinTuan.put("activityTuanOrderId", activityTuanOrderId);
			ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderService.findObjectByCondition(m4PinTuan);
			if(activityTuanOrderExt!=null && activityTuanOrderExt.getId()>0 && activityTuanOrderExt.getState()==1){//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)


				order.setActivityTuanOrderId(activityTuanOrderExt.getId());
				studentCourseOrderService.update(order);
			}
		}




		String title = "";
		String detail = "";


		if(courseType==1){
			order.setCourseInfo(courseInfo);
			title = courseInfo.getName();
			detail = courseInfo.getName();
		}
		if(courseType==2){
			order.setCourseGroupInfo(courseGroupInfo);
			title = courseGroupInfo.getName();
			detail = courseGroupInfo.getName();
		}



		//调用支付接口
		if("1".equals(payChannel)){
			//微信

			JSONObject jo = studentCourseOrderService.invokePay4Weixin(order, title, detail, loginToken, payType, paramMap.get("appId")==null?"":(String)paramMap.get("appId"), paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));
			String state = jo.getString("state");
			JSONObject result = jo.getJSONObject("result");
			JSONObject payResult = result.getJSONObject("payResult");

			if(StringUtils.isNotEmpty(state) && "SUCCESS".equals(state)){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),result);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}else{
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"调用支付失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),result);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}else{
			//支付宝

			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此方式暂未开放",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),null);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}



		/*ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),order);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);*/

	}



	//订单支付(继续支付)
	@RequestMapping(value = "tuan/order/pay")
	public ResponseEntity<ResponseJson> pay4TuanOrder(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");//课程id
		String courseGroupId = (String) paramMap.get("courseGroupId");//课程分组id
		String payChannel = (String) paramMap.get("payChannel");//支付渠道(1微信、2支付宝)
		String payType = (String) paramMap.get("payType");//支付方式(weixin-web|weixin-app|weixin-jsapi)
		String orderNo = (String) paramMap.get("orderNo");//订单编号

		String channelId = (String) paramMap.get("channelId");//渠道id

		String discountId = (String) paramMap.get("discountId");//优惠券id

		String appId = (String) paramMap.get("appId");
		String appKey = (String) paramMap.get("appKey");

		int sourceType = 0;//来源类型(1WEB端、2小程序、3公众号)
		if(StringUtils.isNotEmpty(appId) && Constant.appId_web.equals(appId)){
			sourceType = 1;
		}
		if(StringUtils.isNotEmpty(appId) && Constant.appId_xiaochengxu.equals(appId)){
			sourceType = 2;
		}
		if(StringUtils.isNotEmpty(appId) && Constant.appId_gongzhonghao.equals(appId)){
			sourceType = 3;
		}



		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) && StringUtils.isEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id或年课id不能同时为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseId) && StringUtils.isNotEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id或年课id不能同时存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(payChannel) || StringUtils.isEmpty(payType) || StringUtils.isEmpty(orderNo)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"支付渠道、支付方式、订单编号不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//验证支付方式
		if("1".equals(payChannel) && !"weixin-web".equalsIgnoreCase(payType) && !"weixin-app".equalsIgnoreCase(payType) && !"weixin-jsapi".equalsIgnoreCase(payType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"支付方式有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		StudentCourseOrderExt order = studentCourseOrderService.findByOrderNo(orderNo);
		String activityTuanOrderId = (order.getActivityTuanOrderId()!=null&&order.getActivityTuanOrderId()>0)?order.getActivityTuanOrderId().toString():"";
		String activityId = "";


		if(StringUtils.isNotEmpty(activityTuanOrderId) && Long.parseLong(activityTuanOrderId)>0){
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("activityTuanOrderId", activityTuanOrderId);
			ActivityTuanOrderExt ato = activityTuanOrderService.findObjectByCondition(m);
			activityId = ato.getActivityTuanId().toString();
			if(ato==null || ato.getId()==0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此拼团订单不存在或已过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(ato.getState()!=1){//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"拼团中的订单才能加入",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		Map<String, Object> mm4PinTuan = new HashMap<>();
		mm4PinTuan.put("delFlag", "0");
		mm4PinTuan.put("activityTuanId", activityId);
		mm4PinTuan.put("studentId", studentId);
		mm4PinTuan.put("state", "1,2");//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
		ActivityTuanOrderExt ato = activityTuanOrderService.findObjectByActivityId4Join(mm4PinTuan);
		if(ato!=null && ato.getId()>0){
			//查询个人订单
			Map<String, Object> om = new HashMap<>();
			om.put("delFlag", "0");
			om.put("activityTuanId", activityId);
			om.put("studentId", studentId);
			om.put("state", "3,4");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			List<StudentCourseOrderExt> scoList = studentCourseOrderService.findListByActivityTuanOrderId(om);
			if(scoList!=null && scoList.size()>0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复参与此活动",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}


		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);

		ActivityTuanExt activityTuanExt = activityTuanService.findObjectByCondition(map4Param);
		if(activityTuanExt==null || activityTuanExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ChannelActivityExt channelActivityExt = null;
		if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(activityId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("state", "1");//状态(0初始化、1正常)
			mm.put("activityType", "1");//活动类型(0免费领、1拼团)
			mm.put("channelId", channelId);
			mm.put("activityId", activityId);
			channelActivityExt = channelActivityService.findObjectByCondition(mm);
			if(channelActivityExt==null){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道活动不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(channelActivityExt.getPrice()==null || channelActivityExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelActivityExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格信息不完整",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(activityTuanExt!=null && activityTuanExt.getState()!=null && activityTuanExt.getState()!=1){//状态(0未上线、1已上线)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动未上线",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		Date startAt = activityTuanExt.getStartAt();
		Date endAt = activityTuanExt.getEndAt();
		if(startAt!=null && startAt.getTime()>new Date().getTime()){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动未开始",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(endAt!=null && endAt.getTime()<new Date().getTime()){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动已结束",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(Constant.createValidFlag4PinTuan==2){
			if(StringUtils.isEmpty(activityTuanOrderId) || "0".equals(activityTuanOrderId)){
				if(activityTuanExt.getEndAt()!=null && DateUtils.addHours(activityTuanExt.getEndAt(), 0-Constant.createEndDate4PinTuan).getTime()<new Date().getTime()){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"开团有效时间已过",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
		}


		Integer courseType = 0;//课程类型(1单课程、2系列课)
		String courseGroupIdStr = "0";
		String coursePackageIds = "";
		String courseIds = "";
		BigDecimal realPrice = new BigDecimal(0);

		CourseExt courseInfo = null;
		CourseGroupExt courseGroupInfo = null;

		if(StringUtils.isNotEmpty(courseId)){
			Course c = courseService.findById(Long.parseLong(order.getCourseIds()));

			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseId", courseId);
			CourseExt course = courseService.findObjectById(mm);

			if(course==null){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(course.getCoursePackageId()!=c.getCoursePackageId()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"与原订单的课程包不一致，不能继续支付",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(course.getState()!=3){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"报名中的课程才能购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}


			courseType = 1;
			courseGroupIdStr = "0";
			coursePackageIds = course.getCoursePackageId().toString();
			courseIds = course.getId().toString();
			realPrice = course.getRealPrice();
			courseInfo = course;


			Map<String, Object> m = new HashMap<>();
			m.put("coursePackageId", coursePackageIds);
			m.put("studentId", studentId);
			m.put("delFlag","0");
			StudentCourseExt studentCourseExt = studentCourseService.findObjectByCondition(m);
			Integer buyFlag;//购买标识(1未购买、2已购买)
			if(studentCourseExt!=null && studentCourseExt.getId()>0){
				buyFlag = 2;
			} else{
				buyFlag = 1;
			}
			if(buyFlag == 2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

		}

		if(StringUtils.isNotEmpty(courseGroupId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseGroupId", courseGroupId);
			CourseGroupExt courseGroup = courseGroupService.findObjectByCondition(mm);
			if(courseGroup.getState()!=2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"上线中的年课才能购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			courseType = 2;
			courseGroupIdStr = courseGroupId;
			coursePackageIds = courseGroup.getCoursePackageIds();
			courseIds = courseGroup.getCourseIds();
			realPrice = courseGroup.getRealPrice();
			courseGroupInfo = courseGroup;


			Map<String, Object> m = new HashMap<>();
			m.put("studentId", studentId);
			m.put("delFlag","0");
			List<StudentCourseExt> studentCourseInfoList = studentCourseService.findListByCondition4CourseGroup(m);
			Integer buyFlag;//购买标识(1未购买、2已购买)
			if(studentCourseInfoList!=null && studentCourseInfoList.size()>0){
				buyFlag = 2;
			} else{
				buyFlag = 1;
			}
			if(buyFlag == 2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

		}


		if(order.getState()!=1){//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此订单不能继续支付",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		BigDecimal tuanPrice = activityTuanExt.getPrice();
		if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(activityId)){
			if(channelActivityExt!=null){
				if(channelActivityExt.getPrice()!=null){
					tuanPrice = channelActivityExt.getPrice();
				}
			}
		}


		//验证优惠券
		BigDecimal discountMoney = new BigDecimal(0);
		if(StringUtils.isNotEmpty(discountId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("studentId", studentId);
			mm.put("discountId", discountId);
			StudentDiscountExt studentDiscountExt = studentDiscountService.findObjectByCondition(mm);
			if(studentDiscountExt==null || studentDiscountExt.getId()==0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券不属于你或已失效",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(studentDiscountExt.getValidityStart().getTime()>new Date().getTime() || studentDiscountExt.getValidityEnd().getTime()<new Date().getTime()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券不在使用期限内",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(courseType!=studentDiscountExt.getCourseType()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券的课程类型不匹配",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(tuanPrice.compareTo(studentDiscountExt.getMoney()) == -1){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"未达到满减条件",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			Integer type = studentDiscountExt.getType();//类型(0抵用券、1折扣券)
			BigDecimal money = studentDiscountExt.getMoney();
			BigDecimal amount = studentDiscountExt.getAmount();
			if(type!=null && type==0){
				discountMoney = amount;
			}
			if(type!=null && type==1){
				discountMoney = tuanPrice.multiply(amount).setScale(2, BigDecimal.ROUND_HALF_UP);//相乘(保留2位小数)
			}
			if(tuanPrice.subtract(discountMoney).compareTo(BigDecimal.ZERO)==-1 || tuanPrice.subtract(discountMoney).compareTo(BigDecimal.ZERO)==0){//当订单金额-优惠金额大于0时才可以
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券优惠价格要小于订单价格",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}else{
			discountId = "0";
		}



		Integer useExistFlag = Constant.useExistFlag;//是否使用已存在待付款订单的标识(1使用、2不使用)
		if(useExistFlag==1 && order!=null && order.getId()>0){

			order.setCourseType(courseType);
			order.setCourseGroupId(Long.parseLong(courseGroupIdStr));
			order.setCoursePackageIds(coursePackageIds);
			order.setCourseIds(courseIds);
			order.setStudentId(Long.parseLong(studentId));
			order.setPhone(studentExt.getPhone());
			if(StringUtils.isNotEmpty(channelId)){
				order.setChannelId(Long.parseLong(channelId));
			}
			//order.setOrderNo(OrderUtils.getUniqueCode().toString());//订单编号
			order.setSourceType(sourceType);//来源类型(1WEB端、2小程序、3公众号)
			order.setOrderMoney(tuanPrice);
			order.setDiscountId(Long.parseLong(discountId));
			order.setDiscountMoney(discountMoney);
			order.setPayChannel(Integer.parseInt(StringUtils.isEmpty(payChannel)?"0":payChannel));//支付渠道(1微信、2支付宝)
			order.setPayType(payType);
			order.setAppId(paramMap.get("appId")==null?"":(String)paramMap.get("appId"));
			order.setAppKey(paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));

			order.setRemark("");
			order.setState(1);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			order.setCreatedAt(new Date());
			order.setUpdatedAt(new Date());
			order.setDelFlag(0);

			studentCourseOrderService.update(order);
		}else{
			order = new StudentCourseOrderExt();

			order.setCourseType(courseType);
			order.setCourseGroupId(Long.parseLong(courseGroupIdStr));
			order.setCoursePackageIds(coursePackageIds);
			order.setCourseIds(courseIds);
			order.setStudentId(Long.parseLong(studentId));
			order.setPhone(studentExt.getPhone());
			if(StringUtils.isNotEmpty(channelId)){
				order.setChannelId(Long.parseLong(channelId));
			}
			order.setOrderNo(OrderUtils.getUniqueCode().toString());//订单编号
			order.setSourceType(sourceType);//来源类型(1WEB端、2小程序、3公众号)
			order.setOrderMoney(tuanPrice);
			order.setDiscountId(Long.parseLong(discountId));
			order.setDiscountMoney(discountMoney);
			order.setPayChannel(Integer.parseInt(StringUtils.isEmpty(payChannel)?"0":payChannel));//支付渠道(1微信、2支付宝)
			order.setPayType(payType);
			order.setAppId(paramMap.get("appId")==null?"":(String)paramMap.get("appId"));
			order.setAppKey(paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));

			order.setRemark("");
			order.setState(1);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			order.setCreatedAt(new Date());
			order.setUpdatedAt(new Date());
			order.setDelFlag(0);

			studentCourseOrderService.insert(order);
		}



		//处理拼团订单相关逻辑
		if(StringUtils.isEmpty(activityTuanOrderId) || "0".equals(activityTuanOrderId)){
			//创建新的拼团
			Map<String, Object> m4PinTuan = new HashMap<>();
			m4PinTuan.put("delFlag", "0");
			m4PinTuan.put("activityTuanId", activityId);
			m4PinTuan.put("studentId", studentId);
			m4PinTuan.put("state", "0");//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
			ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderService.findObjectByActivityId(m4PinTuan);
			if(activityTuanOrderExt!=null && activityTuanOrderExt.getId()>0){
				activityTuanOrderExt.setStudentId(Long.parseLong(studentId));
				activityTuanOrderExt.setActivityTuanId(Long.parseLong(activityId));
				activityTuanOrderExt.setPeopleCount(0);//初始化为0
				activityTuanOrderExt.setStartAt(new Date());
				activityTuanOrderExt.setEndAt(DateUtils.addHours(new Date(), Constant.createEndDate4PinTuan));
				activityTuanOrderExt.setRemark("");
				activityTuanOrderExt.setState(0);
				activityTuanOrderExt.setCreatedAt(new Date());
				activityTuanOrderExt.setUpdatedAt(new Date());
				activityTuanOrderExt.setDelFlag(0);
				activityTuanOrderService.update(activityTuanOrderExt);
			}else{
				activityTuanOrderExt = new ActivityTuanOrderExt();
				activityTuanOrderExt.setStudentId(Long.parseLong(studentId));
				activityTuanOrderExt.setActivityTuanId(Long.parseLong(activityId));
				activityTuanOrderExt.setPeopleCount(0);//初始化为0
				activityTuanOrderExt.setStartAt(new Date());
				activityTuanOrderExt.setEndAt(DateUtils.addHours(new Date(), Constant.createEndDate4PinTuan));
				activityTuanOrderExt.setRemark("");
				activityTuanOrderExt.setState(0);
				activityTuanOrderExt.setCreatedAt(new Date());
				activityTuanOrderExt.setUpdatedAt(new Date());
				activityTuanOrderExt.setDelFlag(0);
				activityTuanOrderService.insert(activityTuanOrderExt);
			}


			order.setActivityTuanOrderId(activityTuanOrderExt.getId());
			studentCourseOrderService.update(order);
		}else{
			//加入其它拼团
			Map<String, Object> m4PinTuan = new HashMap<>();
			m4PinTuan.put("delFlag", "0");
			m4PinTuan.put("activityTuanOrderId", activityTuanOrderId);
			ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderService.findObjectByCondition(m4PinTuan);
			if(activityTuanOrderExt!=null && activityTuanOrderExt.getId()>0 && activityTuanOrderExt.getState()==1){//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)


				order.setActivityTuanOrderId(activityTuanOrderExt.getId());
				studentCourseOrderService.update(order);
			}
		}





		String title = "";
		String detail = "";


		if(courseType==1){
			order.setCourseInfo(courseInfo);
			title = courseInfo.getName();
			detail = courseInfo.getName();
		}
		if(courseType==2){
			order.setCourseGroupInfo(courseGroupInfo);
			title = courseGroupInfo.getName();
			detail = courseGroupInfo.getName();
		}



		//调用支付接口
		if("1".equals(payChannel)){
			//微信

			JSONObject jo = studentCourseOrderService.invokePay4Weixin(order, title, detail, loginToken, payType, paramMap.get("appId")==null?"":(String)paramMap.get("appId"), paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));
			String state = jo.getString("state");
			JSONObject result = jo.getJSONObject("result");
			JSONObject payResult = result.getJSONObject("payResult");

			if(StringUtils.isNotEmpty(state) && "SUCCESS".equals(state)){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),result);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}else{
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"调用支付失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),result);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}else{
			//支付宝

			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此方式暂未开放",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),null);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}



	}













}
