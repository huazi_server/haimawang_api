package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestProductionMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestPlayerMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.ContestProductionMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestPlayerExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestProductionExt;
import cn.com.mys.project_kernel.base.service.ContestProductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ContestProductionServiceImpl implements ContestProductionService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestProductionMapper contestProductionMapper;

	@Autowired
	public ContestProductionMapperExt contestProductionMapperExt;

	@Autowired
	public ContestPlayerMapperExt contestPlayerMapperExt;

	@Override
	public ContestProductionExt findObjectByCondition(Map<String, Object> m) {
		return contestProductionMapperExt.findObjectByCondition(m);
	}

	@Override
	public ContestProductionExt findById(long id) {
		ContestProductionExt contestProductionExt = contestProductionMapperExt.findById(id);
		if(contestProductionExt!=null && contestProductionExt.getId()>0 && contestProductionExt.getPlayerId()!=null && contestProductionExt.getPlayerId()>0){
			ContestPlayerExt contestPlayerExt = contestPlayerMapperExt.findById(contestProductionExt.getPlayerId());
			contestProductionExt.setContestPlayerInfo(contestPlayerExt);
		}
		return contestProductionExt;
	}

	@Override
	public Integer update(ContestProductionExt contestProductionExt) {
		return contestProductionMapper.updateByPrimaryKeySelective(contestProductionExt);
	}

	@Override
	public Integer insert(ContestProductionExt contestProductionExt) {
		return contestProductionMapper.insertSelective(contestProductionExt);
	}

	@Override
	public ContestProductionExt findLastObjectByCondition(Map<String, Object> m) {
		return contestProductionMapperExt.findLastObjectByCondition(m);
	}

	@Override
	public List<ContestProductionExt> findListByContestId(Long contestId) {
		return contestProductionMapperExt.findListByContestId(contestId);
	}
}
