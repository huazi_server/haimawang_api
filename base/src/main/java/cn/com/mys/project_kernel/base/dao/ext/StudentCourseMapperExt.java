package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.StudentCourseExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface StudentCourseMapperExt {

    StudentCourseExt findObjectByCondition(Map<String, Object> m);

    List<StudentCourseExt> findListByCondition(Map<String, Object> m);

    Integer findStudentCountByTeacherCourseId(Map<String, Object> map4Param);

    Integer findMaxCourseStageByStudentId(Map<String, Object> mmm);

    List<Map<String,Object>> findMapByCondition(Map<String, Object> m);

    List<StudentCourseExt> findListByCourseId(Map<String, Object> m);

    List<StudentCourseExt> findListByCondition4CourseGroup(Map<String, Object> m);

    Integer update4Del(Map<String, Object> m);

}