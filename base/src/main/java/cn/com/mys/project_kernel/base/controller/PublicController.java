package cn.com.mys.project_kernel.base.controller;


import cn.com.mys.project_kernel.base.util.DateUtils;
import cn.com.mys.project_kernel.base.util.PropertiesUtils;
import cn.com.mys.project_kernel.base.util.UUIDUtils;
import cn.com.mys.project_kernel.base.util.UploadFileUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author mingjun_T
 * 公共方法
 * 
 */
@Controller
@RequestMapping(value = "/public")
public class PublicController {

	/**
	 * 文件上传(不跳转页面,返回文件保存的路径)
	 * 返回数据到调用页面
	 */
	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
	@ResponseBody
	public String fileUpload(HttpServletRequest request, Model model, HttpServletResponse response){

		JSONArray ja = new JSONArray();

		//转型为MultipartHttpRequest
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> files = multipartRequest.getFiles("file");//页面中要有名称一致的属性(获取多个)
		for (MultipartFile file : files) {
			JSONObject jo = new JSONObject();
			if (file != null && file.getOriginalFilename() != null && !"".equals(file.getOriginalFilename())) {
				String savePath = PropertiesUtils.getKeyValue("config.properties", "savePath");
				String originalFilename = file.getOriginalFilename();
				String fileType = originalFilename.substring(originalFilename.lastIndexOf(".") + 1).toLowerCase();
				//重命名上传的文件(形式:	jpg_20160101-010101_UUID.jpg)
				String renameFileName = fileType + "_" + DateUtils.getDate_Time() + "_" + UUIDUtils.generateUUID() + "." + fileType;
				String fileName = UploadFileUtils.saveFileByLocal(file, response, savePath, renameFileName);

				jo.put("fileName", "/"+renameFileName);
				ja.add(jo);
			}
		}
		return ja.toString();
	}

	/**
	 * 文件上传(不跳转页面,返回文件保存的路径)
	 * 返回数据到调用方法
     */
	@RequestMapping(value = "/fileUpload2Path")
	public static List<String> fileUpload2Path(HttpServletRequest request, Model model, HttpServletResponse response){

		List<String> paths = new ArrayList<String>();

		//转型为MultipartHttpRequest
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> files = multipartRequest.getFiles("file");//页面中要有名称一致的属性(获取多个)
		for (MultipartFile file : files) {
			String path = "";
			if (file != null && file.getOriginalFilename() != null && !"".equals(file.getOriginalFilename())) {
				String savePath = PropertiesUtils.getKeyValue("config.properties", "savePath");
				String originalFilename = file.getOriginalFilename();
				String fileType = originalFilename.substring(originalFilename.lastIndexOf(".") + 1).toLowerCase();
				//重命名上传的文件(形式:	jpg_20160101-010101_UUID.jpg)
				String renameFileName = fileType + "_" + DateUtils.getDate_Time() + "_" + UUIDUtils.generateUUID() + "." + fileType;
				String fileName = UploadFileUtils.saveFileByLocal(file, response, savePath, renameFileName);

				path = "/"+renameFileName;
				paths.add(path);
			}
		}
		return paths;
	}





}
