package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.StudentProductionLikeMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.StudentProductionLikeMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.StudentProductionLikeExt;
import cn.com.mys.project_kernel.base.service.StudentProductionLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class StudentProductionLikeServiceImpl implements StudentProductionLikeService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public StudentProductionLikeMapper studentProductionLikeMapper;

	@Autowired
	public StudentProductionLikeMapperExt studentProductionLikeMapperExt;


	@Override
	public StudentProductionLikeExt findObjectByCondition(Map<String, Object> map4Param) {
		return studentProductionLikeMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer insert(StudentProductionLikeExt studentProductionLikeExt) {
		return studentProductionLikeMapper.insertSelective(studentProductionLikeExt);
	}

	@Override
	public Integer delete(StudentProductionLikeExt studentProductionLikeExt) {
		return studentProductionLikeMapper.deleteByPrimaryKey(studentProductionLikeExt.getId());
	}
}
