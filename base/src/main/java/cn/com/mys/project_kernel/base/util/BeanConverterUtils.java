package cn.com.mys.project_kernel.base.util;

/**
 * Created by mingjun_T on 2016-07-25.
 */

import net.sf.json.JSONObject;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.DateConverter;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 转换器
 * 1:将JavaBean 转换成Map、JSONObject
 * 2:将JSONObject 转换成Map
 *
 * @author xxx
 */
public class BeanConverterUtils {

    /**
     * 将javaBean对象转换成Map
     *
     * @param obj javaBean对象
     * @return Map对象
     */
    public static Map<String, Object> javaBean2Map(Object obj) {
        Map<String, Object> params = new HashMap<String, Object>(0);
        try {
            PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
            PropertyDescriptor[] descriptors = propertyUtilsBean.getPropertyDescriptors(obj);
            for (int i = 0; i < descriptors.length; i++) {
                String name = descriptors[i].getName();
                if ("class".equals(name)) {
                    params.put(name, propertyUtilsBean.getNestedProperty(obj, name));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }


    /**
     * 将json对象转换成Map
     *
     * @param jsonObject json对象
     * @return Map对象
     */
    public static Map<String, String> jsonBean2Map(JSONObject jsonObject) {
        Map<String, String> result = new HashMap<String, String>();
        Iterator<String> iterator = jsonObject.keys();
        String key = null;
        String value = null;
        while (iterator.hasNext()) {
            key = iterator.next();
            value = jsonObject.getString(key);
            result.put(key, value);
        }
        return result;
    }

    /**
     * map转javaBean
     * @param map
     * @param beanClass
     * @return
     * @throws Exception
     */
    public static Object map2JavaBean(Map<String, Object> map, Class<?> beanClass) {
        if (map == null)
            return null;
        Object obj = null;
        try {
            obj = beanClass.newInstance();
            //DateConverter converter = new DateConverter();
            //converter.setPattern("yyyy-MM-dd");
            BeanUtils.populate(obj, map);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj;
    }


}
