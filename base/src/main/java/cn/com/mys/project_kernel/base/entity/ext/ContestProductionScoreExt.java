package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ContestProductionScore;

public class ContestProductionScoreExt extends ContestProductionScore {

    private String reviewTeacherName;//评审老师名称
    public String getReviewTeacherName() {
        return reviewTeacherName;
    }
    public void setReviewTeacherName(String reviewTeacherName) {
        this.reviewTeacherName = reviewTeacherName;
    }
}