package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.Production;

import java.util.List;

public class ProductionExt extends Production {

    private Integer collectFlag;//收藏标识(1未收藏、2已收藏)
    private Integer likeFlag;//点赞标识(1未点赞、2已点赞)
    private Integer selfFlag;//所属标识(1不属于自己、2属于自己)
    public Integer getCollectFlag() {
        return collectFlag;
    }

    public void setCollectFlag(Integer collectFlag) {
        this.collectFlag = collectFlag;
    }

    public Integer getLikeFlag() {
        return likeFlag;
    }

    public void setLikeFlag(Integer likeFlag) {
        this.likeFlag = likeFlag;
    }

    public Integer getSelfFlag() {
        return selfFlag;
    }

    public void setSelfFlag(Integer selfFlag) {
        this.selfFlag = selfFlag;
    }

    /*private String authorName;//作者名称
    private String authorAvatarUrl;//作者头像
    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    public String getAuthorAvatarUrl() {
        return authorAvatarUrl;
    }
    public void setAuthorAvatarUrl(String authorAvatarUrl) {
        this.authorAvatarUrl = authorAvatarUrl;
    }*/


    private List<ProductionTagExt> productionTagInfoList;//作品标签列表
    public List<ProductionTagExt> getProductionTagInfoList() {
        return productionTagInfoList;
    }
    public void setProductionTagInfoList(List<ProductionTagExt> productionTagInfoList) {
        this.productionTagInfoList = productionTagInfoList;
    }

    private Integer reportFlag;//举报标识(1未被举报、2已被举报)
    public Integer getReportFlag() {
        return reportFlag;
    }
    public void setReportFlag(Integer reportFlag) {
        this.reportFlag = reportFlag;
    }
}