package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ContestWinExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ContestWinService {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ContestWinExt> findListByCondition(Map<String, Object> map4Param);

    Integer insert(ContestWinExt contestWinExt);
}
