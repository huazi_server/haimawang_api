package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestMapper;
import cn.com.mys.project_kernel.base.dao.ContestProductionMapper;
import cn.com.mys.project_kernel.base.dao.ContestTemplateMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.*;
import cn.com.mys.project_kernel.base.entity.ContestPlayer;
import cn.com.mys.project_kernel.base.entity.ContestProduction;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.ContestService;
import cn.com.mys.project_kernel.base.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ContestServiceImpl implements ContestService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestMapper contestMapper;

	@Autowired
	public ContestMapperExt contestMapperExt;


	@Autowired
	public ContestSignupConfigMapperExt contestSignupConfigMapperExt;
	@Autowired
	public ContestReviewConfigMapperExt contestReviewConfigMapperExt;
	@Autowired
	public ContestReviewTeacherMapperExt contestReviewTeacherMapperExt;
	@Autowired
	public ContestAwardMapperExt contestAwardMapperExt;
	@Autowired
	public ContestGroupMapperExt contestGroupMapperExt;
	@Autowired
	public ConfigGradeMapperExt configGradeMapperExt;

	@Autowired
	public ContestTemplateMapperExt contestTemplateMapperExt;


	@Autowired
	public ContestPlayerMapperExt contestPlayerMapperExt;
	@Autowired
	public ContestProductionMapperExt contestProductionMapperExt;
	@Autowired
	public ContestStatMapperExt contestStatMapperExt;


	//设置配置
	private void setConfig(ContestExt contestExt){
		if(contestExt!=null && contestExt.getId()>0){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("delFlag", "0");
			map.put("contestId", contestExt.getId().toString());
			ContestSignupConfigExt contestSignupConfigExt = contestSignupConfigMapperExt.findObjectByCondition(map);
			ContestReviewConfigExt contestReviewConfigExt = contestReviewConfigMapperExt.findObjectByCondition(map);
			List<ContestReviewTeacherExt> contestReviewTeacherInfoList = contestReviewTeacherMapperExt.findListByContestId(map);
			List<ContestAwardExt> contestAwardInfoList = contestAwardMapperExt.findListByContestId(map);

			//设置组别
			if(contestSignupConfigExt!=null && contestSignupConfigExt.getId()>0 && StringUtils.isNotEmpty(contestSignupConfigExt.getGroupIds())){
				List<ContestGroupExt> contestGroupInfoList = contestGroupMapperExt.findListByIds(contestSignupConfigExt.getGroupIds());
				for(ContestGroupExt contestGroupExt : contestGroupInfoList){
					if(contestGroupExt!=null && contestGroupExt.getId()>0 && StringUtils.isNotEmpty(contestGroupExt.getGradeIds())){
						List<ConfigGradeMapperExt> configGradeInfoList = configGradeMapperExt.findListByIds(contestGroupExt.getGradeIds());
						contestGroupExt.setConfigGradeInfoList(configGradeInfoList);
					}
				}
				contestSignupConfigExt.setContestGroupInfoList(contestGroupInfoList);
			}

			contestExt.setContestSignupConfigInfo(contestSignupConfigExt);
			contestExt.setContestReviewConfigInfo(contestReviewConfigExt);
			contestExt.setContestReviewTeacherInfoList(contestReviewTeacherInfoList);
			contestExt.setContestAwardInfoList(contestAwardInfoList);

			if(contestExt.getTemplateId()!=null && contestExt.getTemplateId()>0){
				ContestTemplateExt contestTemplateExt = contestTemplateMapperExt.findById(contestExt.getTemplateId());
				contestExt.setContestTemplateInfo(contestTemplateExt);
			}

			Integer signupCount = 0;//报名人数
			Integer commitCount = 0;//提交人数
			signupCount = contestPlayerMapperExt.findSignupCountByContestId(contestExt.getId());
			commitCount = contestProductionMapperExt.findCommitCountByContestId(contestExt.getId());
			contestExt.setSignupCount(signupCount);
			contestExt.setCommitCount(commitCount);

			//查询统计数据
			ContestStatExt contestStatExt = contestStatMapperExt.findByContestId(contestExt.getId());
			contestExt.setContestStatInfo(contestStatExt);
		}
	}

	@Override
	public ContestExt findById(long id) {
		ContestExt contestExt = contestMapperExt.findById(id);
		if(contestExt!=null && contestExt.getId()>0){
			setConfig(contestExt);
		}
		return contestExt;
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return contestMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ContestExt> findListByCondition(Map<String, Object> map4Param) {
		List<ContestExt> list = contestMapperExt.findListByCondition(map4Param);
		for(ContestExt contestExt : list){
			setConfig(contestExt);
		}
		return list;
	}

	@Override
	public Integer findCountByCondition4Join(Map<String, Object> map4Param) {
		return contestMapperExt.findCountByCondition4Join(map4Param);
	}

	@Override
	public List<ContestExt> findListByCondition4Join(Map<String, Object> map4Param) {
		List<ContestExt> list = contestMapperExt.findListByCondition4Join(map4Param);
		for(ContestExt contestExt : list){
			setConfig(contestExt);
		}
		return list;
	}

	@Override
	public Integer findCountByCondition4Create(Map<String, Object> map4Param) {
		return contestMapperExt.findCountByCondition4Create(map4Param);
	}

	@Override
	public List<ContestExt> findListByCondition4Create(Map<String, Object> map4Param) {
		List<ContestExt> list = contestMapperExt.findListByCondition4Create(map4Param);
		for(ContestExt contestExt : list){
			setConfig(contestExt);
		}
		return list;
	}

	@Override
	public Integer insert(ContestExt contestExt) {
		return contestMapper.insertSelective(contestExt);
	}

	@Override
	public Integer update(ContestExt contestExt) {
		return contestMapper.updateByPrimaryKeySelective(contestExt);
	}

	@Override
	public List<ContestExt> findAllListByCondition(Map<String, Object> map) {
		List<ContestExt> list = contestMapperExt.findAllListByCondition(map);
		for(ContestExt contestExt : list){
			setConfig(contestExt);
		}
		return list;
	}
}
