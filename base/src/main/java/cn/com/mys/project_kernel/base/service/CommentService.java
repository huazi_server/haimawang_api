package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CommentExt;

/**
 * Service的接口
 *
 */
public interface CommentService {


    Integer insert(CommentExt commentExt);
}
