package cn.com.mys.project_kernel.base.controller.account.channel;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 渠道用户
 *
 */
@Controller
@RequestMapping(value = "/account/channel/user")
public class ChannelUserController {

	Log log = LogFactory.getLog(ChannelUserController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private ChannelService channelService;
    @Autowired
    private ChannelUserService channelUserService;



    //列表
    @RequestMapping(value = "list")
    public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

        //参数转换
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);

        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }

        log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

        //url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		ChannelExt channelExt = channelService.findByAccountId(accountId);
		String channelId = channelExt!=null?String.valueOf(channelExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(channelId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

        //请求体内的参数

        //String channelId = (String) paramMap.get("channelId");
        String name = (String) paramMap.get("name");
        String phone = (String) paramMap.get("phone");
        String gender = (String) paramMap.get("gender");//性别(0未知、1男、2女)

        String pageNumber = (String) paramMap.get("pageNumber");//页数
        String pageSize = (String) paramMap.get("pageSize");//查询条目数

        Page page = new Page();

        if(StringUtils.isNotEmpty(pageNumber)){
            page.setPageNumber(Integer.parseInt(pageNumber));
        }
        if(StringUtils.isNotEmpty(pageSize)){
            page.setPageSize(Integer.parseInt(pageSize));
        }

        Map<String, Object> map4Param = new HashMap<String, Object>();
        map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
        map4Param.put("channelId", channelId);
        map4Param.put("name", name);
        map4Param.put("phone", phone);
        map4Param.put("gender", gender);
        map4Param.put("page", page);

        Integer count = channelUserService.findCountByCondition(map4Param);
        List<ChannelUserExt> list = channelUserService.findListByCondition(map4Param);

        page.setTotalSize(count);
        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
    }

	//查询当天提交人数
	@RequestMapping(value = "day")
	public ResponseEntity<ResponseJson> day(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		String channelId = (String) paramMap.get("channelId");

		if(StringUtils.isEmpty(channelId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//验证每天参与人数
		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("channelId", channelId);
		Integer dayUserCount = channelUserService.findDayUserCountByCondition(map4Param);
		dayUserCount = dayUserCount==null?0:dayUserCount;

		Map<String, Object> m = new LinkedHashMap<>();
		if(dayUserCount.intValue()<Constant.dayMaxUserCount4ChannelUser.intValue()){
			m.put("type", "1");//1今天参与人数未满、2今天参与人数已满
			m.put("msg", "今天参与人数未满");
			m.put("maxCount", Constant.dayMaxUserCount4ChannelUser);
			m.put("count", dayUserCount);
		}else {
			m.put("type", "2");//1今天参与人数未满、2今天参与人数已满
			m.put("msg", "今天参与人数已满");
			m.put("maxCount", Constant.dayMaxUserCount4ChannelUser);
			m.put("count", dayUserCount);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//验证
	@RequestMapping(value = "verify")
	public ResponseEntity<ResponseJson> verify(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		String channelId = (String) paramMap.get("channelId");
		String phone = (String) paramMap.get("phone");

		if(StringUtils.isEmpty(channelId) || StringUtils.isEmpty(phone)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id、手机号不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(!ValidatorUtils.checkPhone(phone)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"手机号不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//验证每天参与人数
		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("channelId", channelId);
		Integer dayUserCount = channelUserService.findDayUserCountByCondition(map4Param);
		dayUserCount = dayUserCount==null?0:dayUserCount;
		if(dayUserCount.intValue()>=Constant.dayMaxUserCount4ChannelUser.intValue()){
			Map<String, Object> m = new HashMap<>();
			m.put("type", "1");//1今天参与人数已满、2已参加活动、3验证码有误或已失效
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"今天参与人数已满",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//验证手机号是否提交过
		map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("channelId", channelId);
		map4Param.put("phone", phone);
		ChannelUserExt cu = channelUserService.findObjectByCondition(map4Param);
		if(cu!=null && cu.getId()>0){
			Map<String, Object> m = new HashMap<>();
			m.put("type", "2");//1今天参与人数已满、2已参加活动、3验证码有误或已失效
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"此手机号已参与过活动，请勿重复提交",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//发送验证码
		String lastAuthCode = "";
		try{
			JSONObject obj = new JSONObject();
			obj.put("productId", Constant.loginProductId);
			obj.put("productKey", Constant.loginProductKey);
			obj.put("verifyType", "HuodongAction");
			obj.put("verifyPhone", phone);
			JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("verifyRequestUrl"), obj);
			log.debug("--"+jo.toString());
			if(jo!=null && jo.getBoolean("result") && jo.getJSONObject("data")!=null){
				String code = jo.getJSONObject("data").getString("code");//验证码
				Integer validity = jo.getJSONObject("data").getInt("validity");//有效时间(秒)
				if(StringUtils.isEmpty(code)){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"验证码获取有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
				lastAuthCode = code;
			}else{
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"验证码获取有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}catch (Exception e){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"验证码获取有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		map4Param.put("state", "0");//状态(0未完成、1已完成)
		ChannelUserExt channelUser = channelUserService.findObjectByCondition(map4Param);
		if(channelUser==null || channelUser.getId()==0){
			channelUser = new ChannelUserExt();
			channelUser.setChannelId(Long.parseLong(channelId));
			channelUser.setPhone(phone);
			channelUser.setLastAuthCode(lastAuthCode);
			channelUser.setLastAuthTime(new Date());

			channelUser.setState(0);
			channelUser.setCreatedAt(new Date());
			channelUser.setUpdatedAt(new Date());
			channelUser.setDelFlag(0);
			channelUserService.insert(channelUser);
		}else{
			channelUser.setChannelId(Long.parseLong(channelId));
			channelUser.setPhone(phone);
			channelUser.setLastAuthCode(lastAuthCode);
			channelUser.setLastAuthTime(new Date());

			channelUser.setState(0);
			channelUser.setCreatedAt(new Date());
			channelUser.setUpdatedAt(new Date());
			channelUser.setDelFlag(0);
			channelUserService.update(channelUser);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),channelUser);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

    //保存
    @RequestMapping(value = "save")
    public ResponseEntity<ResponseJson> save(HttpServletRequest request, HttpServletResponse response) {

        //参数转换
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);

        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }

        log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

        //url地址后的参数处理

        //请求体内的参数

        String channelId = (String) paramMap.get("channelId");
		String phone = (String) paramMap.get("phone");
		String authCode = (String) paramMap.get("authCode");//验证码
        String name = (String) paramMap.get("name");
        String age = (String) paramMap.get("age");
        String gender = (String) paramMap.get("gender");//性别(0未知、1男、2女)

		if(StringUtils.isEmpty(channelId) || StringUtils.isEmpty(phone) || StringUtils.isEmpty(authCode)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id、手机号、验证码不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(!ValidatorUtils.checkPhone(phone)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"手机号不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

        if(StringUtils.isEmpty(name) || StringUtils.isEmpty(age) || StringUtils.isEmpty(gender)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"姓名、年龄、性别不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

		//验证每天参与人数
		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("channelId", channelId);
		Integer dayUserCount = channelUserService.findDayUserCountByCondition(map4Param);
		dayUserCount = dayUserCount==null?0:dayUserCount;
		if(dayUserCount.intValue()>=Constant.dayMaxUserCount4ChannelUser.intValue()){
			Map<String, Object> m = new HashMap<>();
			m.put("type", "1");//1今天参与人数已满、2已参加活动、3验证码有误或已失效
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"今天参与人数已满",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//验证手机号是否提交过
		map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("channelId", channelId);
		map4Param.put("phone", phone);
		ChannelUserExt cu = channelUserService.findObjectByCondition(map4Param);
		if(cu!=null && cu.getId()>0){
			Map<String, Object> m = new HashMap<>();
			m.put("type", "2");//1今天参与人数已满、2已参加活动、3验证码有误或已失效
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"此手机号已参与过活动，请勿重复提交",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		map4Param.put("state", "0");//状态(0未完成、1已完成)
		ChannelUserExt channelUser = channelUserService.findObjectByCondition(map4Param);
		if(channelUser==null || channelUser.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此手机号未曾获取验证码",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//验证验证码
		String lastAuthCode = channelUser.getLastAuthCode();
		Date lastAuthTime = channelUser.getLastAuthTime();
		if(StringUtils.isEmpty(lastAuthCode) || DateUtils.getInterval(new Date(), lastAuthTime)>=Constant.authCodeValidity4ChannelUser.intValue() || !authCode.equals(lastAuthCode)){
			Map<String, Object> m = new HashMap<>();
			m.put("type", "3");//1今天参与人数已满、2已参加活动、3验证码有误或已失效
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"验证码有误或失效",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

        channelUser.setName(name);
        channelUser.setAge(age);
        channelUser.setGender(StringUtils.isNotEmpty(gender)?Integer.parseInt(gender):0);
		channelUser.setCommitTime(new Date());
        channelUser.setState(1);
        channelUser.setCreatedAt(new Date());
        channelUser.setUpdatedAt(new Date());
        channelUser.setDelFlag(0);
        channelUserService.update(channelUser);

        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),channelUser);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
    }








}
