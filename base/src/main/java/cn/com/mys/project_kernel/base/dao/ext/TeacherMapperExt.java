package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.TeacherExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface TeacherMapperExt {

    TeacherExt findByAccountId(String accountId);

    Integer updateById(TeacherExt teacherExt);

    TeacherExt findObjectById(Long id);

    List<TeacherExt> findListByType(Map<String, Object> map4Param);

    TeacherExt findByPhone(String phone);

    Integer updateOtherByAccountId(Map<String, Object> mm);
}