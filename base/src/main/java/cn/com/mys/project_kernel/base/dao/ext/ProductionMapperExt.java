package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ProductionExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ProductionMapperExt {

    Integer findCountByCondition4All(Map<String, Object> map4Param);
    List<ProductionExt> findListByCondition4All(Map<String, Object> map4Param);

    ProductionExt findObjectByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<ProductionExt> findListByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition4Collect(Map<String, Object> map4Param);
    List<ProductionExt> findListByCondition4Collect(Map<String, Object> map4Param);
    Integer findCountByCondition4Like(Map<String, Object> map4Param);
    List<ProductionExt> findListByCondition4Like(Map<String, Object> map4Param);
}