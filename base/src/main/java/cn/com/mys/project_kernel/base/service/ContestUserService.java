package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ContestUserExt;

/**
 * Service的接口
 *
 */
public interface ContestUserService {

    ContestUserExt findByAccountId(String accountId);

    Integer insert(ContestUserExt contestUserExt);

    Integer update(ContestUserExt contestUserExt);
}
