package cn.com.mys.project_kernel.base.dao.base;

import org.springframework.stereotype.Repository;


/**
 * BaseMapper<T>   基础Mapper
 * 定义公共方法
 * 
 */
@Repository
public interface BaseMapper<T>{

    int deleteByPrimaryKey(String id);

    int insert(T t);

    int insertSelective(T t);

    T selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(T t);

    int updateByPrimaryKey(T t);
}