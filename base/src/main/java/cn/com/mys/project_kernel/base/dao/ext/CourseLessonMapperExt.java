package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CourseLessonExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface CourseLessonMapperExt {

    List<CourseLessonExt> findListByCondition(Map<String, Object> m);

    CourseLessonExt findObjectByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition4CurrentDate(Map<String, Object> map4Param);
    List<CourseLessonExt> findListByCondition4CurrentDate(Map<String, Object> map4Param);
}