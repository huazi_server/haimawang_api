package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ProductionTagExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ProductionTagService {


    List<ProductionTagExt> findListByCondition(Map<String, Object> map);
}
