package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CompetitionAwardMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CompetitionAwardMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CompetitionAwardExt;
import cn.com.mys.project_kernel.base.service.CompetitionAwardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class CompetitionAwardServiceImpl implements CompetitionAwardService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CompetitionAwardMapper competitionAwardMapper;

	@Autowired
	public CompetitionAwardMapperExt competitionAwardMapperExt;


	@Override
	public List<CompetitionAwardExt> findListByCondition(Map<String, Object> map4Param) {
		return competitionAwardMapperExt.findListByCondition(map4Param);
	}
}
