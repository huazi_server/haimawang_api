package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.dao.*;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.*;
import cn.com.mys.project_kernel.base.entity.ActivityTuan;
import cn.com.mys.project_kernel.base.entity.ActivityTuanOrder;
import cn.com.mys.project_kernel.base.entity.Course;
import cn.com.mys.project_kernel.base.entity.StudentCourseOrder;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.StudentCourseOrderService;
import cn.com.mys.project_kernel.base.util.DateUtils;
import cn.com.mys.project_kernel.base.util.HttpUtils;
import cn.com.mys.project_kernel.base.util.PropertiesUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * Service的实现类
 *
 */

@Service
public class StudentCourseOrderServiceImpl implements StudentCourseOrderService {

	Log log = LogFactory.getLog(StudentCourseOrderServiceImpl.class);


	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public StudentCourseOrderMapper studentCourseOrderMapper;

	@Autowired
	public StudentCourseOrderMapperExt studentCourseOrderMapperExt;
	@Autowired
	public CourseMapper courseMapper;
	@Autowired
	public CourseMapperExt courseMapperExt;

	@Autowired
	public StudentCourseMapper studentCourseMapper;
	@Autowired
	public StudentCourseMapperExt studentCourseMapperExt;

	@Autowired
	public TeacherCourseMapper teacherCourseMapper;
	@Autowired
	public TeacherCourseMapperExt teacherCourseMapperExt;
	@Autowired
	public CourseGroupMapperExt courseGroupMapperExt;


	@Autowired
	public ActivityTuanOrderMapper activityTuanOrderMapper;
	@Autowired
	public ActivityTuanOrderMapperExt activityTuanOrderMapperExt;
	@Autowired
	public ActivityTuanMapper activityTuanMapper;




	//支付接口
	//payType:weixin-web|weixin-app|weixin-jsapi
	@Override
	public JSONObject invokePay4Weixin(StudentCourseOrderExt order, String title, String detail, String loginToken, String payType, String appId, String appKey){
		JSONObject resultJo = new JSONObject();
		Map<String, Object> m = new HashMap<>();
		m.put("orderNo", order.getOrderNo());

		JSONObject obj = new JSONObject();
		obj.put("loginToken", loginToken);
		obj.put("loginProductId", Constant.loginProductId);
		obj.put("loginProductKey", Constant.loginProductKey);
		String tradeNo = order.getOrderNo().toString();//订单编号
		Double d = order.getOrderMoney().doubleValue()*100;//订单总金额,单位:分
		Double p = d-(order.getDiscountMoney()==null?0:order.getDiscountMoney().doubleValue()*100);//支付总金额,单位:分
		int orderAmount = d.intValue();
		int payAmount = p.intValue();
		int payExpireSecond = Constant.payExpireSecond;//过期时间,单位:秒

		obj.put("tradeNo", tradeNo);
		obj.put("orderAmount", orderAmount);
		obj.put("payAmount", payAmount);
		obj.put("payType", payType);
		obj.put("payExpireSecond", payExpireSecond);
		obj.put("title", title);//非必传
		obj.put("detail", detail);//非必传
		obj.put("appId", appId);
		obj.put("appKey", appKey);


		log.info("--"+obj.toString());
		JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("payUrl"), obj);
		log.info("--"+jo.toString());

		/**
		 {
		 "result": true,
		 "duration": 625,
		 "type": "Pay execute",
		 "code": 200,
		 "message": "Success",
		 "data": {
		 "payAmount": 50,
		 "detail": "课程详情",
		 "accountId": 20111345,
		 "tradeNo": "114496577057325056",
		 "refundTotalAmount": 0,
		 "state": 0,
		 "productId": 20111105,
		 "updatedAt": 1557910055016,
		 "id": 8,
		 "payTotalAmount": 0,
		 "title": "课程",
		 "refundAmount": 0,
		 "orderAmount": 50,
		 "createdAt": 1557909558000,
		 "payType": "weixin-web",
		 "payExpireSecond": 60,
		 "delFlag": 0,
		 "payResult": {
		 "id": 20,
		 "tradeId": 8,
		 "accountId": 20111345,
		 "type": "weixin-web",
		 "outTradeNo": "81557910055031",
		 "body": "课程",
		 "detail": "课程详情",
		 "infos": null,
		 "feeType": null,
		 "feeAmount": 50,
		 "feeSuccessAmount": 0,
		 "refundTotalAmount": 0,
		 "refundAmount": 0,
		 "spbillCreateIp": null,
		 "timeStartAt": null,
		 "timeExpireSecond": null,
		 "prepayId": null,
		 "request": null,
		 "result": "{\"sign\":\"62E0176635FEADBB254405CC24A3D516\",\"timestamp\":\"1557910055\",\"noncestr\":\"ilSEphqfz1vQ6IrNtg67FcTIFR7LH18m\",\"partnerid\":\"1488708052\",\"requestXml\":\"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\r\\n<xml>\\r\\n<sign>AB59FFAA9E16B89256A0455DA740584A</sign>\\r\\n<mch_id>1488708052</mch_id>\\r\\n<appid>wxd5126531c0485699</appid>\\r\\n<nonce_str>1557910055</nonce_str>\\r\\n<out_trade_no>81557910055031</out_trade_no>\\r\\n</xml>\\r\\n\",\"prepayid\":\"wx1516473519347821f107f0793150069395\",\"package\":\"Sign=WXPay\",\"appid\":\"wxd5126531c0485699\",\"codeurl\":\"weixin://wxpay/bizpayurl?pr=aiYDG9P\",\"requestUrl\":\"https://api.mch.weixin.qq.com/pay/orderquery\"}",
		 "state": 0,
		 "updatedAt": 1557910055297,
		 "createdAt": 1557910055031,
		 "delFlag": 0
		 }
		 },
		 "error": null,
		 "version": null,
		 "extParam": null
		 }
		 */



		if(jo!=null && jo.getInt("code")>0 && jo.getInt("code")==200){
			JSONObject jo_data = jo.getJSONObject("data");
			JSONObject pr = jo_data.getJSONObject("payResult");
			if(pr!=null && pr.size()>0){
				JSONObject payResult = pr.getJSONObject("result");
				if(payResult!=null && payResult.size()>0){
					resultJo.put("state", "SUCCESS");
					m.put("payResult", payResult);
					resultJo.put("result", m);
					return resultJo;
				}
			}
		}

		resultJo.put("state", "FAIL");
		m.put("payResult", new JSONObject());
		resultJo.put("result", m);
		return resultJo;
	}




	//设置课程信息
	private void setCourse(StudentCourseOrderExt studentCourseOrderExt){
		Long courseId = Long.parseLong(studentCourseOrderExt.getCourseIds());
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("courseId", courseId.toString());
		CourseExt courseInfo = courseMapperExt.findObjectById(m);
		studentCourseOrderExt.setCourseInfo(courseInfo);
	}
	//设置课程分组信息
	private void setCourseGroup(StudentCourseOrderExt studentCourseOrderExt){
		Long courseGroupId = studentCourseOrderExt.getCourseGroupId();
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("courseGroupId", courseGroupId);
		CourseGroupExt courseGroupExt = courseGroupMapperExt.findObjectByCondition(m);
		m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("courseType", "2");//课程类型(1单课程、2系列课)
		m.put("courseGroupId", courseGroupId);
		List<CourseExt> courseInfoList = courseMapperExt.findListByCondition4CourseGroup(m);

		String coursePackageIds = "";
		String courseIds = "";
		for(CourseExt c : courseInfoList){
			coursePackageIds = coursePackageIds + (c.getCoursePackageId()==null?"":","+c.getCoursePackageId());
			courseIds = courseIds + "," + c.getId();
		}

		if(StringUtils.isNotEmpty(coursePackageIds)){
			coursePackageIds = coursePackageIds.substring(0,coursePackageIds.length()-1);
		}
		if(StringUtils.isNotEmpty(courseIds)){
			courseIds = courseIds.substring(0,courseIds.length()-1);
		}

		courseGroupExt.setCoursePackageIds(coursePackageIds);
		courseGroupExt.setCourseIds(courseIds);
		courseGroupExt.setCourseInfoList(courseInfoList);

		studentCourseOrderExt.setCourseGroupInfo(courseGroupExt);
	}


	//设置拼团订单信息
	private void setActivityTuanOrder(StudentCourseOrderExt studentCourseOrderExt){
		Long activityTuanOrderId = studentCourseOrderExt.getActivityTuanOrderId();
		if(activityTuanOrderId!=null && activityTuanOrderId>0){
			Map<String, Object> map = new HashMap<>();
			map.put("delFlag", "0");
			map.put("activityTuanOrderId", activityTuanOrderId.toString());
			ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderMapperExt.findObjectByCondition(map);
			if(activityTuanOrderExt!=null && activityTuanOrderExt.getId()>0){
				String studentIds = activityTuanOrderExt.getStudentIds();
				String studentNames = activityTuanOrderExt.getStudentNames();
				String studentAvatarUrls = activityTuanOrderExt.getStudentAvatarUrls();
				String studentOrderIds = activityTuanOrderExt.getStudentOrderIds();
				if(StringUtils.isNotEmpty(studentIds) && StringUtils.isNotEmpty(studentNames) && StringUtils.isNotEmpty(studentAvatarUrls) && StringUtils.isNotEmpty(studentOrderIds)) {
					String[] sIds = studentIds.split(",");
					String[] sNames = studentNames.split(",");
					String[] sAvatarUrls = studentAvatarUrls.split(",");
					String[] sOrderIds = studentOrderIds.split(",");
					List<Map<String, Object>> studentInfoList = new ArrayList<>();
					if(sIds.length==sNames.length && sNames.length==sAvatarUrls.length && sAvatarUrls.length==sOrderIds.length){
						for(int i=0;i<sIds.length;i++){
							Map<String, Object> m = new HashMap<>();
							m.put("studentId", sIds[i]);
							m.put("studentName", sNames[i]);
							m.put("studentAvatarUrl", sAvatarUrls[i]);
							m.put("studentOrderId", sOrderIds[i]);
							studentInfoList.add(m);
						}
					}
					activityTuanOrderExt.setStudentInfoList(studentInfoList);
				}

				if(activityTuanOrderExt.getEndAt()!=null){
					if(activityTuanOrderExt.getEndAt().getTime()>=new Date().getTime()){
						activityTuanOrderExt.setRemnantTime(DateUtils.getInterval(activityTuanOrderExt.getEndAt(), new Date()));
					}else{
						activityTuanOrderExt.setRemnantTime(0L);
					}
				}
				if(activityTuanOrderExt.getActivityTuanId()!=null && activityTuanOrderExt.getActivityTuanId()>0){
					ActivityTuan activityTuan = activityTuanMapper.selectByPrimaryKey(activityTuanOrderExt.getActivityTuanId());
					if(activityTuan!=null && activityTuan.getId()>0){
						activityTuanOrderExt.setRestrictionCount(activityTuan.getRestrictionCount());
					}
				}
			}
			studentCourseOrderExt.setActivityTuanOrderInfo(activityTuanOrderExt);
		}
	}



	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return studentCourseOrderMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<StudentCourseOrderExt> findListByCondition(Map<String, Object> map4Param) {
		List<StudentCourseOrderExt> list = studentCourseOrderMapperExt.findListByCondition(map4Param);
		for(StudentCourseOrderExt studentCourseOrderExt : list){
			if(studentCourseOrderExt!=null && studentCourseOrderExt.getId()>0){
				if(studentCourseOrderExt.getCourseType()!=null && studentCourseOrderExt.getCourseType()==1){
					setCourse(studentCourseOrderExt);
				}
				if(studentCourseOrderExt.getCourseType()!=null && studentCourseOrderExt.getCourseType()==2){
					setCourseGroup(studentCourseOrderExt);
				}
				setActivityTuanOrder(studentCourseOrderExt);
			}
		}
		return list;
	}

	@Override
	public StudentCourseOrder findById(long id) {
		return studentCourseOrderMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer update(StudentCourseOrder studentCourseOrder) {
		return studentCourseOrderMapper.updateByPrimaryKeySelective(studentCourseOrder);
	}

	@Override
	public StudentCourseOrderExt findObjectByCondition(Map<String, Object> m) {
		StudentCourseOrderExt studentCourseOrderExt = studentCourseOrderMapperExt.findObjectByCondition(m);
		if(studentCourseOrderExt!=null && studentCourseOrderExt.getId()>0){
			if(studentCourseOrderExt.getCourseType()!=null && studentCourseOrderExt.getCourseType()==1){
				setCourse(studentCourseOrderExt);
			}
			if(studentCourseOrderExt.getCourseType()!=null && studentCourseOrderExt.getCourseType()==2){
				setCourseGroup(studentCourseOrderExt);
			}
			setActivityTuanOrder(studentCourseOrderExt);
		}
		return studentCourseOrderExt;
	}

	@Override
	public Integer insert(StudentCourseOrderExt order) {
		return studentCourseOrderMapper.insertSelective(order);
	}

	@Override
	public StudentCourseOrderExt findByOrderNo(String orderNo) {
		StudentCourseOrderExt studentCourseOrderExt = studentCourseOrderMapperExt.findByOrderNo(orderNo);
		if(studentCourseOrderExt!=null && studentCourseOrderExt.getId()>0){
			if(studentCourseOrderExt.getCourseType()!=null && studentCourseOrderExt.getCourseType()==1){
				setCourse(studentCourseOrderExt);
			}
			if(studentCourseOrderExt.getCourseType()!=null && studentCourseOrderExt.getCourseType()==2){
				setCourseGroup(studentCourseOrderExt);
			}
			setActivityTuanOrder(studentCourseOrderExt);
		}
		return studentCourseOrderExt;
	}

	@Override
	public List<StudentCourseOrderExt> findAllListByCondition(Map<String, Object> map) {
		List<StudentCourseOrderExt> list = studentCourseOrderMapperExt.findAllListByCondition(map);
		for(StudentCourseOrderExt studentCourseOrderExt : list){
			if(studentCourseOrderExt!=null && studentCourseOrderExt.getId()>0){
				if(studentCourseOrderExt.getCourseType()!=null && studentCourseOrderExt.getCourseType()==1){
					setCourse(studentCourseOrderExt);
				}
				if(studentCourseOrderExt.getCourseType()!=null && studentCourseOrderExt.getCourseType()==2){
					setCourseGroup(studentCourseOrderExt);
				}
				setActivityTuanOrder(studentCourseOrderExt);
			}
		}
		return list;
	}

	@Override
	public Integer updateByCondition(Map<String, Object> map) {
		return studentCourseOrderMapperExt.updateByCondition(map);
	}


	@Override
	public List<StudentCourseOrderExt> findListByActivityTuanOrderId(Map<String, Object> mm) {
		return studentCourseOrderMapperExt.findListByActivityTuanOrderId(mm);
	}

	@Override
	public List<StudentCourseOrderExt> findListByCondition4PinTuanOrder(Map<String, Object> pinTuanMap) {
		return studentCourseOrderMapperExt.findListByCondition4PinTuanOrder(pinTuanMap);
	}

	@Override
	public List<StudentCourseOrderExt> findListByCondition4ActivityTuanOrderId(Map<String, Object> map) {
		return studentCourseOrderMapperExt.findListByCondition4ActivityTuanOrderId(map);
	}

	@Override
	public Integer findCountByCondition4Channel(Map<String, Object> map4Param) {
		return studentCourseOrderMapperExt.findCountByCondition4Channel(map4Param);
	}

	@Override
	public List<StudentCourseOrderExt> findListByCondition4Channel(Map<String, Object> map4Param) {
		return studentCourseOrderMapperExt.findListByCondition4Channel(map4Param);
	}


	@Override
	public void generateTeacherCourseAndStudentCourseData(String studentId, String coursePackageId, String courseId) {
		Map<String, Object> map = new HashMap<>();
		map.put("courseId", courseId);
		map.put("delFlag", "0");
		CourseExt courseExt = courseMapperExt.findObjectByCondition(map);

		map.put("coursePackageId", coursePackageId);
		map.put("studentId", studentId);
		StudentCourseExt studentCourseExt = studentCourseMapperExt.findObjectByCondition(map);

		if(courseExt!=null && courseExt.getId()>0 && StringUtils.isNotEmpty(courseExt.getPlanTeacherIds())){
			String planTeacherIds = courseExt.getPlanTeacherIds();//计划授课教师id(多个","分隔)
			System.out.println("planTeacherIds="+planTeacherIds);
			String[] teacherIds = planTeacherIds.split(",");
			//初始化数据
			/*List<Map<String, Object>> l = new ArrayList<>();
			for(String tId : teacherIds){
				Map<String, Object> mm = new HashMap<>();
				mm.put("courseId", courseId);
				mm.put("teacherId", tId);
				mm.put("teacherCourseId", 0);
				mm.put("studentCount", 0);
				l.add(mm);
			}*/
			Map<String, Integer> mmm = new HashMap<>();
			for(String tId : teacherIds){
				mmm.put(tId, 0);
			}

			//查询课程关联的教师已分配的学生数量
			Map<String, Object> m = new HashMap<>();
			m.put("courseId", courseId);
			m.put("state", "2");//状态(1未分配教师、2已分配教师)
			m.put("delFlag", "0");
			List<Map<String, Object>> ll = studentCourseMapperExt.findMapByCondition(m);
			for(Map<String, Object> mmmm : ll){
				Long teacherId = (Long) mmmm.get("teacherId");
				Long studentCount = (Long) mmmm.get("studentCount");
				mmm.put(teacherId.toString(), studentCount.intValue());
			}

			//将Map转化为List集合,List采用ArrayList
			List<Map.Entry<String, Integer>> listData = new ArrayList<Map.Entry<String,Integer>>(mmm.entrySet());
			//通过Collections.sort(List I,Comparator c)方法进行排序
			Collections.sort(listData,new Comparator<Map.Entry<String, Integer>>() {
				@Override
				public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
					return (o1.getValue() - o2.getValue());//从小到大
				}
			});
			System.out.println(listData);
			Map.Entry<String, Integer> me = listData.get(0);
			String teacherId = me.getKey();//选择教师

			//分配教师
			if(studentCourseExt!=null && studentCourseExt.getState()==1){//状态(1未分配教师、2已分配教师)
				Map<String, Object> _m = new HashMap<>();

				//插入教师课程
				TeacherCourseExt teacherCourseExt = new TeacherCourseExt();
				teacherCourseExt.setCourseId(Long.parseLong(courseId));
				teacherCourseExt.setTeacherId(Long.parseLong(teacherId));
				teacherCourseExt.setStudentCount(0);
				teacherCourseExt.setScore(0);
				teacherCourseExt.setRemark("");
				teacherCourseExt.setState(0);
				teacherCourseExt.setCreatedAt(new Date());
				teacherCourseExt.setUpdatedAt(new Date());
				teacherCourseExt.setDelFlag(0);

				_m = new HashMap<>();
				_m.put("delFlag", "0");
				_m.put("courseId", courseId);
				_m.put("teacherId", teacherId);
				TeacherCourseExt tc = teacherCourseMapperExt.findObjectByCondition(_m);
				if(tc==null || tc.getId()==0){
					teacherCourseMapper.insertSelective(teacherCourseExt);
				}else{
					teacherCourseExt = tc;
				}

				//更新学生课程
				studentCourseExt.setTeacherId(Long.parseLong(teacherId));
				studentCourseExt.setTeacherCourseId(teacherCourseExt.getId());
				studentCourseExt.setState(2);//状态(1未分配教师、2已分配教师)
				studentCourseExt.setUpdatedAt(new Date());
				studentCourseMapper.updateByPrimaryKeySelective(studentCourseExt);

				//更新教师课程
				Integer studentCount = studentCourseMapperExt.findStudentCountByTeacherCourseId(_m);
				teacherCourseExt.setStudentCount(studentCount);
				teacherCourseExt.setUpdatedAt(new Date());
				teacherCourseMapper.updateByPrimaryKeySelective(teacherCourseExt);

			}

		}

	}





}
