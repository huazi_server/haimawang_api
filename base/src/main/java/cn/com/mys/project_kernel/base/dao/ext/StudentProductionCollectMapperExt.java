package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.StudentProductionCollectExt;
import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public interface StudentProductionCollectMapperExt {

    StudentProductionCollectExt findObjectByCondition(Map<String, Object> m);
}