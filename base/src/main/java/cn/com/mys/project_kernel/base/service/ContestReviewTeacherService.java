package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ContestReviewTeacher;
import cn.com.mys.project_kernel.base.entity.ext.ContestReviewTeacherExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ContestReviewTeacherService {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ContestReviewTeacherExt> findListByCondition(Map<String, Object> map4Param);

    ContestReviewTeacher findById(long id);

    Integer insert(ContestReviewTeacher contestReviewTeacher);

    Integer update(ContestReviewTeacher contestReviewTeacher);


    ContestReviewTeacherExt findByPhone(String phone);
}
