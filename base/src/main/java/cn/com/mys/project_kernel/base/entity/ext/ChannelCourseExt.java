package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ChannelCourse;

import java.util.List;

public class ChannelCourseExt extends ChannelCourse {

    private String channelName;//渠道名称
    private String courseName;//单课程或系列课名称
    private String courseIssue;//单课程或系列课期次

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseIssue() {
        return courseIssue;
    }

    public void setCourseIssue(String courseIssue) {
        this.courseIssue = courseIssue;
    }

    private List<DiscountExt> discountInfoList;//优惠券列表

    public List<DiscountExt> getDiscountInfoList() {
        return discountInfoList;
    }

    public void setDiscountInfoList(List<DiscountExt> discountInfoList) {
        this.discountInfoList = discountInfoList;
    }
}