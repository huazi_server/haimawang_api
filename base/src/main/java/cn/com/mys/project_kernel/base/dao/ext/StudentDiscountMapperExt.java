package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.StudentDiscount;
import cn.com.mys.project_kernel.base.entity.ext.StudentDiscountExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface StudentDiscountMapperExt {

    StudentDiscountExt findObjectByCondition(Map<String, Object> mm);

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<StudentDiscountExt> findListByCondition(Map<String, Object> map4Param);

    Integer delete4Map(Map<String, Object> mm);

    Integer update4Map(Map<String, Object> mm);
}