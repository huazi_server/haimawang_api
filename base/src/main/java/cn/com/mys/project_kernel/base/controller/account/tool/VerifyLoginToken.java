package cn.com.mys.project_kernel.base.controller.account.tool;

import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.util.HttpUtils;
import cn.com.mys.project_kernel.base.util.PropertiesUtils;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mingjun_T on 2016-07-08.
 */
public class VerifyLoginToken {


    static Log log = LogFactory.getLog(VerifyLoginToken.class);

    //loginToken验证接口地址
    private static String account4LoginTokenUrl = PropertiesUtils.getKeyValue("account4LoginTokenUrl");

    //验证loginToken,返回用户id
    public static String verifyLoginToken(String loginToken) {

        String accountId = "";

        JSONObject obj = new JSONObject();
        obj.put("loginToken", loginToken);
        obj.put("loginProductId", Constant.loginProductId);
        obj.put("loginProductKey", Constant.loginProductKey);
        JSONObject jo = HttpUtils.sendPost4JSON(account4LoginTokenUrl, obj);
        log.info("--"+jo.toString());


        if(jo!=null && jo.has("code") && jo.getInt("code")>0 && jo.getInt("code")==200){
            JSONObject jo_data = jo.getJSONObject("data");
            String userId = jo_data.getString("id");//accountId
            Map<String, Object> m = jo_data;
            //查询相应的教师或学生的实际id
            accountId = userId;
        }else{
            accountId = "";
        }
        return accountId;
    }



    //验证loginToken,返回用户信息
    public static Map<String, Object> verifyLoginToken4Map(String loginToken) {

        JSONObject jo_data = new JSONObject();

        JSONObject obj = new JSONObject();
        obj.put("loginToken", loginToken);
        obj.put("loginProductId", Constant.loginProductId);
        obj.put("loginProductKey", Constant.loginProductKey);
        JSONObject jo = HttpUtils.sendPost4JSON(account4LoginTokenUrl, obj);
        log.info("--"+jo.toString());

        if(jo!=null && jo.has("code") && jo.getInt("code")>0 && jo.getInt("code")==200){
            jo_data = jo.getJSONObject("data");
        }else{
            jo_data = new JSONObject();
        }
        return jo_data;
    }


}
