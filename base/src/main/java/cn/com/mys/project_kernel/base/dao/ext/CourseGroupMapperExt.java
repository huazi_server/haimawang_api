package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CourseGroupExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface CourseGroupMapperExt {

    CourseGroupExt findObjectByCondition(Map<String, Object> m);

    List<CourseGroupExt> findListByCondition4All(Map<String, Object> map4Param);

    List<CourseGroupExt> findListByCondition4Admin(Map<String, Object> map4Param);
}