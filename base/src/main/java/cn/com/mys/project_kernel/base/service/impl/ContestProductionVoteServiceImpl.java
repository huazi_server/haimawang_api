package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestProductionVoteMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestProductionVoteMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CompetitionProductionVoteExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestProductionVoteExt;
import cn.com.mys.project_kernel.base.service.ContestProductionVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ContestProductionVoteServiceImpl implements ContestProductionVoteService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestProductionVoteMapper contestProductionVoteMapper;

	@Autowired
	public ContestProductionVoteMapperExt contestProductionVoteMapperExt;

	@Override
	public List<CompetitionProductionVoteExt> findListByCondition4Production(Map<String, Object> map4Param) {
		return contestProductionVoteMapperExt.findListByCondition4Production(map4Param);
	}

	@Override
	public Integer insert(ContestProductionVoteExt contestProductionVoteExt) {
		return contestProductionVoteMapper.insertSelective(contestProductionVoteExt);
	}
}
