package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.StudentProductionCollectMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.StudentProductionCollectMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.StudentProductionCollectExt;
import cn.com.mys.project_kernel.base.service.StudentProductionCollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class StudentProductionCollectServiceImpl implements StudentProductionCollectService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public StudentProductionCollectMapper studentProductionCollectMapper;

	@Autowired
	public StudentProductionCollectMapperExt studentProductionCollectMapperExt;


	@Override
	public StudentProductionCollectExt findObjectByCondition(Map<String, Object> map4Param) {
		return studentProductionCollectMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer insert(StudentProductionCollectExt studentProductionCollectExt) {
		return studentProductionCollectMapper.insertSelective(studentProductionCollectExt);
	}

	@Override
	public Integer delete(StudentProductionCollectExt studentProductionCollectExt) {
		return studentProductionCollectMapper.deleteByPrimaryKey(studentProductionCollectExt.getId());
	}
}
