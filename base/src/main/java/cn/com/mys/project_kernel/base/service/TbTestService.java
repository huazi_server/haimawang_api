package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.TbTest;

import java.util.List;
import java.util.Map;

/**
 * Tb1TestService的接口
 *
 */
public interface TbTestService {


    List<TbTest> findTbTestListByCondition(Map<String, Object> map);

    int findTbTestListCountByCondition(Map<String, Object> map);

    List<TbTest> findTbTestListPageByCondition(Map<String, Object> map);

    List<TbTest> findTbTestListAll();

    int insetTbTest(TbTest tbTest);

    int deleteTbTestById(String id);

    int updateTbTest(TbTest tbTest);
    
    int updateTbTest4Map(Map<String, Object> map);

    TbTest findTbTestById(String id);
    
    TbTest findTbTestByCondition(Map<String, Object> map);

}
