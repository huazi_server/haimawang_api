package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ContestSignupConfigExt;

import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ContestSignupConfigService {

    ContestSignupConfigExt findObjectByCondition(Map<String, Object> map4Param);

    Integer insert(ContestSignupConfigExt contestSignupConfigExt);

    Integer update(ContestSignupConfigExt contestSignupConfigExt);
}
