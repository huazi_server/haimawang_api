package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.StudentCourseHomework;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseHomeworkExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface StudentCourseHomeworkService {

    StudentCourseHomeworkExt findObjectByCondition(Map<String, Object> map4Param);

    Integer update(StudentCourseHomeworkExt studentCourseHomeworkExt);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<StudentCourseHomeworkExt> findListByCondition(Map<String, Object> map4Param);

    List<StudentCourseHomeworkExt> findListByCondition4CourseLesson(Map<String, Object> map4Param);

    Integer findAllCountByCondition(Map<String, Object> m);
    Integer findCommitCountByCondition(Map<String, Object> m);
    Integer findOnlineCountByCondition(Map<String, Object> m);

    Integer update4OnlineFlag(String onlineFlag);

    StudentCourseHomeworkExt findById(long id);


    Integer insert(StudentCourseHomeworkExt sch);

}
