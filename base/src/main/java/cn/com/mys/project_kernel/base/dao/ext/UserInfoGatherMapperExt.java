package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.UserInfoGather;
import cn.com.mys.project_kernel.base.entity.ext.UserInfoGatherExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface UserInfoGatherMapperExt {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<UserInfoGatherExt> findListByCondition(Map<String, Object> map4Param);
}