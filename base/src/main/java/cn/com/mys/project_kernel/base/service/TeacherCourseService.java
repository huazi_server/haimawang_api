package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.TeacherCourse;
import cn.com.mys.project_kernel.base.entity.ext.TeacherCourseExt;

import java.util.Map;

/**
 * Service的接口
 *
 */
public interface TeacherCourseService {


    TeacherCourse findById(Long id);

    Integer update(TeacherCourse teacherCourse);

    Integer insert(TeacherCourseExt teacherCourseExt);

    TeacherCourseExt findObjectByCondition(Map<String, Object> map4Param);
}
