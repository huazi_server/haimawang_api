package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.StudentCourseHomeworkMessage;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseHomeworkMessageExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface StudentCourseHomeworkMessageService {


    Integer insert(StudentCourseHomeworkMessageExt studentCourseHomeworkMessageExt);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<StudentCourseHomeworkMessageExt> findListByCondition(Map<String, Object> map4Param);

    StudentCourseHomeworkMessage findById(long id);
    Integer update(StudentCourseHomeworkMessage studentCourseHomeworkMessage);
}
