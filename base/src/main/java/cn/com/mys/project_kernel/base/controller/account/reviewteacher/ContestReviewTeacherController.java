package cn.com.mys.project_kernel.base.controller.account.reviewteacher;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.ParamUtils;
import cn.com.mys.project_kernel.base.util.RequestUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author mingjun_T
 * 比赛
 *
 */
@Controller
@RequestMapping(value = "/account/contest/reviewteacher")
public class ContestReviewTeacherController {

	Log log = LogFactory.getLog(ContestReviewTeacherController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;
	@Autowired
	private StudentService studentService;
    @Autowired
    private ConfigGradeService configGradeService;

    @Autowired
	private ContestService contestService;
	@Autowired
	private ContestGroupService contestGroupService;
	@Autowired
	private ContestAwardService contestAwardService;
	@Autowired
	private ContestUserService contestUserService;
	@Autowired
	private ContestPlayerService contestPlayerService;
	@Autowired
	private ContestProductionService contestProductionService;
	@Autowired
	private ContestProductionVoteService contestProductionVoteService;
	@Autowired
	private ContestProductionScoreService contestProductionScoreService;
	@Autowired
	private ContestSignupConfigService contestSignupConfigService;
	@Autowired
	private ContestReviewConfigService contestReviewConfigService;
	@Autowired
	private ContestReviewTeacherService contestReviewTeacherService;
	@Autowired
	private ContestWinService contestWinService;
	@Autowired
	private ContestStatService contestStatService;



	//获取个人信息
	@RequestMapping(value = "info")
	public ResponseEntity<ResponseJson> info(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		Map<String, Object> m = VerifyLoginToken.verifyLoginToken4Map(loginToken);

		Integer accountId = (Integer) m.get("id");
		String phone = (String) m.get("phone");

		//请求体内的参数

		/*if(accountId==null || accountId==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/
		if(StringUtils.isEmpty(phone)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		ContestReviewTeacherExt contestReviewTeacherExt = contestReviewTeacherService.findByPhone(phone);
		if(contestReviewTeacherExt==null || contestReviewTeacherExt.getId()==0){
			//不存在直接返回
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		contestReviewTeacherExt.setAccountId((accountId==null || accountId==0)?0L:accountId.longValue());
		contestReviewTeacherExt.setPhone(phone);
		contestReviewTeacherExt.setUpdatedAt(new Date());
		contestReviewTeacherService.update(contestReviewTeacherExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestReviewTeacherExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//获取比赛列表
	@RequestMapping(value = "contest/list")
	public ResponseEntity<ResponseJson> list4Contest(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		Map<String, Object> m = VerifyLoginToken.verifyLoginToken4Map(loginToken);

		Integer accountId = (Integer) m.get("id");
		String phone = (String) m.get("phone");

		//请求体内的参数

		/*if(accountId==null || accountId==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/
		if(StringUtils.isEmpty(phone)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		ContestReviewTeacherExt contestReviewTeacherExt = contestReviewTeacherService.findByPhone(phone);
		if(contestReviewTeacherExt==null || contestReviewTeacherExt.getId()==0){
			//不存在直接返回
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("reviewTeacherId", contestReviewTeacherExt.getId());
		map4Param.put("page", page);

		map4Param.put("auditFlag", "3");//审核标识(1待审核、2审核不通过、3审核通过)
		map4Param.put("state", "3,4,5,6");//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)
		Integer count = contestService.findCountByCondition4Create(map4Param);
		List<ContestExt> list = contestService.findListByCondition4Create(map4Param);
		for(ContestExt contestExt : list){
			Map<String, Object> mm = new HashMap<String, Object>();
			mm.put("delFlag", "0");
			mm.put("state", "1");//状态(0未完成、1已完成)
			mm.put("reviewTeacherId", contestReviewTeacherExt.getId());
			mm.put("contestId", contestExt.getId().toString());
			ContestPlayerExt contestPlayerExt = contestPlayerService.findObjectByCondition(mm);
			Integer joinFlag = 1;//指定比赛参加标识(1未参加、2已参加)
			if(contestPlayerExt!=null && contestPlayerExt.getId()>0){
				joinFlag = 2;
			}
			contestExt.setJoinFlag(joinFlag);
		}


		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询参赛者列表
	@RequestMapping(value = "player/list")
	public ResponseEntity<ResponseJson> list4Player(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("contestId", contestId);
		map4Param.put("page", page);
		Integer count = contestPlayerService.findCountByCondition(map4Param);
		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}
		List<ContestPlayerExt> list = contestPlayerService.findListByCondition(map4Param);
		for(ContestPlayerExt contestPlayerExt : list){
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("contestId", contestId);
			m.put("playerId", contestPlayerExt.getId().toString());
			ContestProductionExt contestProductionExt = contestProductionService.findObjectByCondition(m);
			if(contestProductionExt!=null && contestProductionExt.getId()>0){
				m.put("productionId", contestProductionExt.getId().toString());
				//查询评分
				List<ContestProductionScoreExt> contestProductionScoreList = contestProductionScoreService.findListByCondition(m);
				Integer score = 0;
				if(contestProductionScoreList.size()>0){
					for(ContestProductionScoreExt ps : contestProductionScoreList){
						if(ps.getScore()!=null && ps.getScore()>0){
							score += ps.getScore();
						}
					}
					score = score/contestProductionScoreList.size();
				}

				contestProductionExt.setScore(score);
				contestProductionExt.setContestProductionScoreInfoList(contestProductionScoreList);

				String reviewProgress = "";//评审进度
				Integer productionReviewCount = 0;
				if(contestExt.getReviewWay()!=null && contestExt.getReviewWay()==2 && contestExt.getContestReviewConfigInfo()!=null && contestExt.getContestReviewConfigInfo().getId()>0 && contestExt.getContestReviewConfigInfo().getProductionReviewCount()!=null && contestExt.getContestReviewConfigInfo().getProductionReviewCount()>0){
					productionReviewCount = contestExt.getContestReviewConfigInfo().getProductionReviewCount();
				}
				reviewProgress = contestProductionScoreList.size()+"/"+productionReviewCount;
				contestProductionExt.setReviewProgress(reviewProgress);
			}
			contestPlayerExt.setContestProductionInfo(contestProductionExt);
		}

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//获取比赛的作品列表
	@RequestMapping(value = "production/list")
	public ResponseEntity<ResponseJson> list4Production(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String type = (String) paramMap.get("type");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		type = StringUtils.isEmpty(type)?"1":type;//1时间排序、2投票排序、3评分排序

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("contestId", contestId);
		map4Param.put("page", page);

		map4Param.put("type", type);

		Integer count = contestPlayerService.findCountByCondition4Production(map4Param);
		List<ContestPlayerExt> list = contestPlayerService.findListByCondition4Production(map4Param);

		for(ContestPlayerExt contestPlayerExt : list){
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("contestId", contestId);
			m.put("playerId", contestPlayerExt.getId().toString());
			ContestProductionExt contestProductionExt = contestProductionService.findObjectByCondition(m);
			if(contestExt.getReviewWay()!=null && contestExt.getReviewWay()==2 && contestProductionExt!=null && contestProductionExt.getId()>0){
				m.put("productionId", contestProductionExt.getId().toString());
				//查询评分
				List<ContestProductionScoreExt> contestProductionScoreList = contestProductionScoreService.findListByCondition(m);
				Integer score = 0;
				if(contestProductionScoreList.size()>0){
					for(ContestProductionScoreExt ps : contestProductionScoreList){
						if(ps.getScore()!=null && ps.getScore()>0){
							score += ps.getScore();
						}
					}
					score = score/contestProductionScoreList.size();
				}

				contestProductionExt.setScore(score);
				contestProductionExt.setContestProductionScoreInfoList(contestProductionScoreList);
			}
			contestPlayerExt.setContestProductionInfo(contestProductionExt);
		}


		//扩展数据
		ContestStatExt contestStatExt = contestStatService.findByContestId(contestExt.getId());
		Map<String, Object> m = new HashMap<>();
		m.put("contestReviewTeacherInfoList", contestExt.getContestReviewTeacherInfoList());
		m.put("contestStatInfo", contestStatExt);


		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,m,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}







}
