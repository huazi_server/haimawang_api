package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestReviewTeacherExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ContestReviewTeacherMapperExt {

    List<ContestReviewTeacherExt> findListByContestId(Map<String, Object> map);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ContestReviewTeacherExt> findListByCondition(Map<String, Object> map4Param);

    ContestReviewTeacherExt findByPhone(String phone);
}