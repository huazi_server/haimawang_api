package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CourseExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface CourseMapperExt {

    List<CourseExt> findListByCondition4All(Map<String, Object> map4Param);

    List<CourseExt> findListByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition4Teacher(Map<String, Object> map4Param);
    List<CourseExt> findListByCondition4Teacher(Map<String, Object> map4Param);

    CourseExt findObjectByCondition(Map<String, Object> map4Param);

    List<CourseExt> findAllListByCondition(Map<String, Object> map);

    CourseExt findObjectByCourseId(Map<String, Object> map4Param);

    List<CourseExt> findListByCondition4CourseGroup(Map<String, Object> m);


    CourseExt findObjectByCondition4CoursePackageId(Map<String, Object> m);
    CourseExt findObjectByCoursePackageId(Map<String, Object> m);

    CourseExt findObjectById(Map<String, Object> m);

    Integer findCountByCondition4TeacherCP(Map<String, Object> map4Param);
    List<CourseExt> findListByCondition4TeacherCP(Map<String, Object> map4Param);

    CourseExt findObjectByCoursePackageIdAndStudentId(Map<String, Object> mm);

    List<CourseExt> findListByCondition4Admin(Map<String, Object> map4Param);
}