package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.WeixinNotifyExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface WeixinNotifyMapperExt {

    WeixinNotifyExt findObjectByCondition(Map<String, Object> mm);
}