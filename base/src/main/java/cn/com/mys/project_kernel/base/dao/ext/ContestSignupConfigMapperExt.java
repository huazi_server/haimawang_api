package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestSignupConfigExt;
import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public interface ContestSignupConfigMapperExt {

    ContestSignupConfigExt findObjectByCondition(Map<String, Object> map4Param);
}