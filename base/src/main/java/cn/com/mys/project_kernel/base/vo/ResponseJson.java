package cn.com.mys.project_kernel.base.vo;

import cn.com.mys.project_kernel.base.util.ParamUtils;
import cn.com.mys.project_kernel.base.util.RequestUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import com.sun.org.apache.bcel.internal.generic.ARRAYLENGTH;
import net.sf.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * JSON 数据模型
 * 
 * @status 请求是否成功
 * @message 请求提示信息
 * @object 请求结果数据
 */
@SuppressWarnings("serial")
public class ResponseJson implements Serializable {


    //获取controller执行时间(毫秒值)
    public static Integer getExecuteTime(HttpServletRequest request){
        int duration = 0;
        try {
            Long duration4Long = (Long)request.getAttribute("duration");
            duration4Long = System.currentTimeMillis() - duration4Long;
            duration = new Long(duration4Long).intValue();
        }catch (Exception e){

        }
        return duration;
    }


    //获取controller扩展参数
    public static Object getExtParam(HttpServletRequest request){
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);
        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }
        return paramMap.get("extParam");
    }




    public final static boolean TRUE = true;
    public final static boolean FALSE = false;

    public final static String SUCCESS = "Success";
    public final static String FAIL = "Fail";


    private boolean result;		//状态:成功true、失败false
    private Integer code;	    //状态码(200成功、100参数错误、401不存在、402没有登陆)
    private String message;     //Success、Fail
    private String error;       //错误信息
    private Integer duration;   //调用时间
    private Object data;        //
    private Object extParam;    //扩展参数

    public ResponseJson() {
        super();
    }

    public ResponseJson(boolean result, Integer code, String message, String error, Integer duration, Object extParam) {
        super();
        this.result = result;
        this.code = code;
        this.message = message;
        this.error = error;
        this.duration = duration;
        this.extParam = extParam;
    }


    public ResponseJson(boolean result, String code, String message, String error, Integer duration, Object extParam) {
        super();
        this.result = result;
        this.code = StringUtils.isNotEmpty(code)?Integer.parseInt(code):0;
        this.message = message;
        this.error = error;
        this.duration = duration;
        this.extParam = extParam;
    }

    public ResponseJson(boolean result, Integer code, String message, String error, Integer duration, Object extParam, Object obj) {
        super();
        this.result = result;
        this.code = code;
        this.message = message;
        this.error = error;
        this.duration = duration;
        this.extParam = extParam;
        this.data =  obj;
    }

    public ResponseJson(boolean result, String code, String message, String error, Integer duration, Object extParam, Object obj) {
        super();
        this.result = result;
        this.code = StringUtils.isNotEmpty(code)?Integer.parseInt(code):0;
        this.message = message;
        this.error = error;
        this.duration = duration;
        this.extParam = extParam;
        this.data =  obj;
    }

    public ResponseJson(boolean result, Integer code, String message, String error, Integer duration, Object extParam, Object list, Page page) {
        super();
        this.result = result;
        this.code = code;
        this.message = message;
        this.error = error;
        this.duration = duration;
        this.extParam = extParam;
        Map<String, Object> map = new HashMap<String, Object>();
        if(list==null){
            list = new ArrayList<>();
        }
        map.put("content", list);
        map.put("pageable",page);
        this.data = map;
    }


    public ResponseJson(boolean result, String code, String message, String error, Integer duration, Object extParam, Object list, Page page) {
        super();
        this.result = result;
        this.code = StringUtils.isNotEmpty(code)?Integer.parseInt(code):0;
        this.message = message;
        this.error = error;
        this.duration = duration;
        this.extParam = extParam;
        Map<String, Object> map = new HashMap<String, Object>();
        if(list==null){
            list = new ArrayList<>();
        }
        map.put("content", list);
        map.put("pageable",page);
        this.data = map;
    }


    public ResponseJson(boolean result, String code, String message, String error, Integer duration, Object extParam, Object list, Object obj, Page page) {
        super();
        this.result = result;
        this.code = StringUtils.isNotEmpty(code)?Integer.parseInt(code):0;
        this.message = message;
        this.error = error;
        this.duration = duration;
        this.extParam = extParam;
        Map<String, Object> map = new HashMap<String, Object>();
        if(list==null){
            list = new ArrayList<>();
        }
        map.put("content", list);
        map.put("pageable",page);
        map.put("extendParam", obj);
        this.data = map;
    }




    /*public boolean isResult() {
        return result;
    }*/

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getExtParam() {
        return extParam;
    }

    public void setExtParam(Object extParam) {
        this.extParam = extParam;
    }

    @Override
    public String toString() {
        return "{" +
                "result=" + result +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", error='" + error + '\'' +
                ", duration=" + duration +
                //", data=" + data +
                //", extParam=" + extParam +
                '}';
    }


}
