package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.Discount;
import cn.com.mys.project_kernel.base.entity.ext.DiscountExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface DiscountService {


    Integer findCountByCondition(Map<String, Object> map4Param);

    List<DiscountExt> findListByCondition(Map<String, Object> map4Param);

    Integer insert(Discount discount);

    Discount findById(long id);

    Integer update(Discount d);

    Integer update4Map(Map<String, Object> map4Param);
}
