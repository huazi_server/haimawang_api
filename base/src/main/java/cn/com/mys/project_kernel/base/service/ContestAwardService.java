package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ContestAward;
import cn.com.mys.project_kernel.base.entity.ext.ContestAwardExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ContestAwardService {

    ContestAward findById(long id);

    Integer insert(ContestAwardExt contestAwardExt);

    Integer update(ContestAward contestAward);

    List<ContestAwardExt> findListByContestId(Map<String, Object> mm);

    Integer deleteByContestId(Long contestId);
}
