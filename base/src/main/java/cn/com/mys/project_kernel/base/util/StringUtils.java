package cn.com.mys.project_kernel.base.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;


public class StringUtils {

	public static String substring(String str, int toCount, String more) {
		int reInt = 0;
		String reStr = "";
		if (str == null)
			return "";
		char[] tempChar = str.toCharArray();
		for (int kk = 0; (kk < tempChar.length && toCount > reInt); kk++) {
			String s1 = str.valueOf(tempChar[kk]);
			byte[] b = s1.getBytes();
			reInt += b.length;
			reStr += tempChar[kk];
		}
		if (toCount == reInt || (toCount == reInt - 1))
			reStr += more;
		return reStr;
	}	
	
	/**
	 * 
	 * @param sourceString
	 *            eg:&#28023;&#27915;&#28065;&#26059;&#31995;&#21015;&#35762;&#
	 *            24231;11-12&#26149;&#23395;
	 * @param maxLength
	 *            最大长度，一个中文占一个单位长度
	 * @param more
	 *            如果超过所截取的长度，字符串最后要加上的省略符。
	 * @return
	 */
	public static String limitForHtml(String sourceString, int maxLength, String more) {
		String str = org.apache.commons.lang.StringEscapeUtils.unescapeHtml(sourceString);
		return limit(str, maxLength, more);
	}

	/**
	 * 
	 * @param sourceString
	 *            eg:测试字符串
	 * @param maxLength
	 *            最大长度，一个中文占一个单位长度
	 * @param more
	 *            如果超过所截取的长度，字符串最后要加上的省略符。
	 * @return
	 */
	public static String limit(String sourceString, int maxLength, String more) {
		String resultString = "";
		if (sourceString == null || sourceString.equals("") || maxLength < 1) {
			return resultString;
		} else if (sourceString.length() <= maxLength) {
			return sourceString;
		} else if (sourceString.length() > 2 * maxLength) {
			// sourceString = sourceString.substring(0, 2 * maxLength);
		}

		if (sourceString.length() > maxLength) {
			char[] chr = sourceString.toCharArray();
			int strNum = 0;
			int strGBKNum = 0;
			boolean isHaveDot = false;

			for (int i = 0; i < sourceString.length(); i++) {
				if (chr[i] >= 0xa1) // 0xa1汉字最小位开始
				{
					strNum = strNum + 2;
					strGBKNum++;
				} else {
					strNum++;
				}

				if (strNum == 2 * maxLength || strNum == 2 * maxLength + 1) {
					if (i + 1 < sourceString.length()) {
						isHaveDot = true;
					}

					break;
				}
			}

			resultString = sourceString.substring(0, strNum - strGBKNum);
			if (isHaveDot) {
				resultString = resultString + more;
			}
		}
		return resultString;
	}

	/**
	 * 格式化时间
	 * 
	 * @param milliSecond
	 *            毫秒
	 * @return
	 */
	public static String timeToString(long milliSecond) {
		int ss = 1000;
		int mi = ss * 60;
		int hh = mi * 60;
		long day = hh * 24;
		long month = day * 30;
		long year = month * 12;

		long years = (milliSecond) / year;
		long months = (long) ((milliSecond - years * year) / month);
		long days = (milliSecond - years * year - months * month) / day;
		long hour = (long) ((milliSecond - years * year - months * month - days * day) / hh);
		long minute = (int) ((milliSecond - years * year - months * month - days * day - hour * hh) / mi);
		long second = (int) ((milliSecond - years * year - months * month - days * day - hour * hh - minute * mi) / ss);
		String result = "";
		if (years != 0) {
			result += years + "年前";
		} else if (months != 0) {
			result += months + "月前";
		} else if (days != 0) {
			result += days + "天前";
		} else if (hour != 0) {
			result += hour + "小时前";
		} else if (minute != 0) {
			result += minute + "分钟前";
		} else if (second != 0) {
			result += second + "秒前";
		}
		return result;
	}

	/**
	 * 格式化时间
	 * 
	 * @param milliSecond
	 *            毫秒
	 * @return
	 */
	public static String timeToString2(long milliSecond) {
		int ss = 1000;
		int mi = ss * 60;
		int hh = mi * 60;
		long day = hh * 24;
		long month = day * 30;
		long year = month * 12;

		long years = (milliSecond) / year;
		long months = (long) ((milliSecond - years * year) / month);
		long days = (milliSecond - years * year - months * month) / day;
		long hour = (long) ((milliSecond - years * year - months * month - days * day) / hh);
		long minute = (int) ((milliSecond - years * year - months * month - days * day - hour * hh) / mi);
		long second = (long) ((milliSecond - years * year - months * month - days * day - hour * hh - minute * mi) / ss);
		long ms = milliSecond - years * year - months * month - days * day - hour * hh - minute * mi - second * ss;
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(2);
		String result = "";
		if (years != 0) {
			result += years + "年";
		} else if (months != 0) {
			result += months + "月";
		} else if (days != 0) {
			result += days + "天";
		} else if (hour != 0) {
			result += hour + "小时";
		} else if (minute != 0) {
			result += minute + "分钟";
		} else if (second != 0) {
			// result += second + "秒";
			result += nf.format(second + (ms / 1000.0)) + "秒";
		}
		return result;
	}

	/**
	 * 格式化日期
	 * 
	 * @param date
	 * @return
	 */
	public static String timeToString3(Date date) {
		if (date == null)
			return "无";
		long milliSecond = System.currentTimeMillis() - date.getTime();
		int ss = 1000;
		int mi = ss * 60;
		int hh = mi * 60;
		long day = hh * 24;
		long month = day * 30;
		long year = month * 12;

		long years = (milliSecond) / year;
		long months = (long) ((milliSecond - years * year) / month);
		long days = (milliSecond - years * year - months * month) / day;
		long hour = (long) ((milliSecond - years * year - months * month - days * day) / hh);
		long minute = (int) ((milliSecond - years * year - months * month - days * day - hour * hh) / mi);
		long second = (int) ((milliSecond - years * year - months * month - days * day - hour * hh - minute * mi) / ss);
		String result = "";
		if (years != 0) {
			result += years + "年前";
		} else if (months != 0) {
			result += months + "月前";
		} else if (days != 0) {
			result += days + "天前";
		} else if (hour != 0) {
			result += hour + "小时前";
		} else if (minute != 0) {
			result += minute + "分钟前";
		} else if (second != 0) {
			result += second + "秒前";
		}
		return result;
	}

	/**
	 * 判断字符串是否为NULL或空字符串
	 * 
	 * @param obj
	 *            被判断的字符串
	 * @return 为空返回true，否则返回 false
	 */
	public static boolean isNullorEmpty(String obj) {
		if (obj == null)
			return true;
		if ("".equals(obj))
			return true;
		return false;
	}

	/**
	 * 判断字符串是否不为NULL或空字符串
	 *
	 * @param obj
	 *            被判断的字符串
	 * @return 不为空返回true，否则返回 false
	 */
	public static boolean isNotEmpty(String obj) {
		if (obj == null)
			return false;
		if ("".equals(obj))
			return false;
		return true;
	}

	//判断是否为空或空字符串
	public static boolean isEmpty(String obj) {
		if (obj == null)
			return true;
		if ("".equals(obj))
			return true;
		return false;
	}


	/**
	 * 将字符串转化为日期
	 * 
	 * @param dateStr
	 *            源字符串
	 * @param style
	 *            格式化串
	 * @return 返回Date
	 * @throws ParseException
	 */
	public static Date parseDate(String dateStr, String style) {
		if (isNullorEmpty(dateStr)) {
			return null;
		}
		SimpleDateFormat sf = new SimpleDateFormat(style);
		Date date;
		try {
			date = sf.parse(dateStr);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 将日期格式化为字符串
	 * 
	 * @param date
	 *            日期
	 * @param style
	 *            格式化形式
	 * @return
	 */
	public static String formatDate(Date date, String style) {
		SimpleDateFormat sf = new SimpleDateFormat(style);
		String str = sf.format(date);
		return str;
	}

	public static Long toLong(String data) {
		Long _data = null;
		if (data != null && !"".equals(data) && !"null".equals(data)) {
			_data = Long.parseLong(data);
		}
		return _data;
	}

	public static Integer toInteger(String data) {
		Integer _data = null;
		if (data != null && !"".equals(data) && !"null".equals(data)) {
			_data = Integer.parseInt(data);
		}
		return _data;
	}

	public static String replaceContent(HttpServletRequest request, String src) {
		String result = src;
		if (isNullorEmpty(result)) {
			return result;
		}
		Pattern pattern = Pattern.compile("<img.*?src *= *['\"]?([^>]+)['\"]?.*?>", Pattern.CASE_INSENSITIVE);
		Pattern pattern2 = Pattern.compile("src *= *['\"]?([^>]+)['\"]?", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(src);

		String domain = request.getScheme() + "://" + request.getHeader("host") + request.getContextPath();

		while (matcher.find()) {
			String tt = matcher.group();
			Matcher matcher2 = pattern2.matcher(tt);
			if (matcher2.find()) {
				String image = matcher2.group();
				String image2 = image.replace(domain, "\\${pageContext.request.contextPath}");
				String tt2 = matcher2.replaceAll(image2);
				result = result.replace(tt, tt2);
			}

		}
		return result;
	}

	public static int getCurrentYear() {
		Calendar now = Calendar.getInstance();
		return now.get(Calendar.YEAR);
	}

	public static String getFileSizeString(Long _size) {
		String size = "";
		long size_long = Math.abs(_size == null ? 0 : _size.longValue());
		NumberFormat formatter = NumberFormat.getInstance();
		formatter.setMaximumFractionDigits(2);
		if (size_long > 700000000L) {
			String[] args = { formatter.format(1.0 * size_long / (1024L * 1024L * 1024L)) };
			for (int _a = 0; _a < args.length; _a++) {
				size += args[_a];
			}
			size += " GB";
		} else if (size_long > 700000L) {
			String[] args = { formatter.format(1.0 * size_long / (1024L * 1024L)) };
			for (int _a = 0; _a < args.length; _a++) {
				size += args[_a];
			}
			size += " MB";
		} else if (size_long > 700L) {
			String[] args = { formatter.format(1.0 * size_long / 1024L) };
			for (int _a = 0; _a < args.length; _a++) {
				size += args[_a];
			}
			size += " KB";
		} else {
			String[] args = { formatter.format(size_long) };
			for (int _a = 0; _a < args.length; _a++) {
				size += args[_a];
			}
			size += " Bytes";
		}
		if (_size < 0) {
			size = "-" + size;
		}
		return size;
	}

	public static String convertToCNNumber(Integer num) {
		if (num == null)
			return null;

		String[] cn_names = new String[] { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" };

		int v = num.intValue();
		StringBuilder str = new StringBuilder();

		String tmp = "" + v;
		for (int i = 0; i < tmp.length(); i++) {
			char c = tmp.charAt(i);
			int tt = Integer.parseInt("" + c);
			str.append(cn_names[tt]);

		}

		if (str.length() > 0) {
			return str.toString();
		} else {
			return null;
		}
	}

	public static void main(String[] args) {
		int num = 1234321;
		String tt = convertToCNNumber(num);
		System.out.println(tt);
	}
	
	
	//正则替换@
	public static String replaceStr(String str){
		//String layers = "@@@@55@@6@4@@9@@@";
		
		String regEx="@+";	//表示一个或多个@,等同于"@{1,}"
		Pattern pat=Pattern.compile(regEx);
		Matcher mat=pat.matcher(str);
		String s=mat.replaceAll(" ");
		
		//String regEx2=" "; //表示一个或多个@,等同于"@{1,}"
		Pattern pat2=Pattern.compile(" ");
		Matcher mat2=pat2.matcher(s.trim());
		String s2=mat2.replaceAll("@");
		System.out.println(s2);
		
		return s2;
	}

	
	//判断是否是数字
	public static boolean isNumeric(String str){
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if( !isNum.matches() ){
			return false;
		}
		return true;
		   
	}

	//生成随机字符串(指定长度)
	public static String getRandomString(int length){
		//定义一个字符串（A-Z，a-z，0-9）即62位
		String str="zxcvbnmlkjhgfdsaqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
		//由Random生成随机数
		Random random=new Random();
		StringBuffer sb=new StringBuffer();
		//长度为几就循环几次
		for(int i=0; i<length; ++i){
			//产生0-61的数字
			int number=random.nextInt(62);
			//将产生的数字通过length次承载到sb中
			sb.append(str.charAt(number));
		}
		//将承载的字符转换成字符串
		return sb.toString();
	}


	//字符串转list
	public static List<String> string2List(String str){
		List<String> list = null;
		if(StringUtils.isNotEmpty(str)){
			list = new ArrayList<String>();
			String[] array = str.split(",");
			list = Arrays.asList(array);
		}
		return list;
	}



	//list去重
	public static List<String> unique(List<String> list){
		return new ArrayList<>(new LinkedHashSet(new ArrayList<>(list)));
	}

	//数组去重
	public static Object[] unique(Object [] arr){
		//实例化一个set集合
		Set set = new HashSet();
		//遍历数组并存入集合,如果元素已存在则不会重复存入
		for (int i = 0; i < arr.length; i++) {
			set.add(arr[i]);
		}
		//返回Set集合的数组形式
		return set.toArray();
	}

	//字符串去重(str形式:"xxx,xxx,xxx")
	public static String unique(String str){
		if(StringUtils.isNotEmpty(str)){
			List<String> data = new ArrayList<String>();
			String[] ids = str.split(",");
			for (int i = 0; i < ids.length; i++) {
				if (!data.contains(ids[i])) {
					data.add(ids[i]);
				}
			}
			str = "";
			for (String id : data) {
				str += id+",";
			}
			str = str.length()>0?str.substring(0,str.length()-1):str;
		}
		return str;
	}

	//字符串连接(connector:分隔符)
	public static String join(String str1, String str2, String separator){
		String defaultSeparator = ",";//默认分隔符
		if(StringUtils.isEmpty(separator)){
			separator = defaultSeparator;
		}
		return str1.concat(separator).concat(str2);
	}



	/**
     * 对数组进行升序排序的方法
	 * string 数字字符串
	 * regex 按指定字符切割
     */
	public static String sortNumberString(String string, String regex, String join) {
		//1.切分字符串
		String[] stringArray=string.split(regex);
		//2.字符串数组转化成数字数组
		double[] doubleArray= new double[stringArray.length];
		for (int i = 0; i < stringArray.length; i++) {
			doubleArray[i]=Double.parseDouble(stringArray[i]);
		}
		//3.整数数组排序,Arrays工具类自带快排
		Arrays.sort(doubleArray);
		//4.排完顺序转化成字符串,单线程使用StringBuider好一点
		StringBuilder returnString=new StringBuilder();
		for (int i = 0; i < doubleArray.length; i++) {
			DecimalFormat decimalFormat = new DecimalFormat("###################.###########");//去掉小数末尾多余的0
			returnString.append(decimalFormat.format(doubleArray[i]));
			//returnString.append(doubleArray[i]);
			if(i!=doubleArray.length-1)
				returnString.append(join);
		}
		return returnString.toString();
	}





	
}
