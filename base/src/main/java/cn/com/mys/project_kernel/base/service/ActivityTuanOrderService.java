package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ActivityTuanOrder;
import cn.com.mys.project_kernel.base.entity.ext.ActivityTuanOrderExt;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseOrderExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ActivityTuanOrderService {


    ActivityTuanOrder findById(Long tuanOrderId);

    ActivityTuanOrderExt findObjectByCondition(Map<String, Object> map);

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<ActivityTuanOrderExt> findListByCondition(Map<String, Object> map4Param);

    ActivityTuanOrderExt findObjectByActivityId(Map<String, Object> map);
    ActivityTuanOrderExt findObjectByActivityId4Join(Map<String, Object> map);

    Integer insert(ActivityTuanOrderExt activityTuanOrderExt);

    Integer update(ActivityTuanOrderExt activityTuanOrderExt);


    void mockRandomUser(ActivityTuanOrderExt ato, Integer restrictionCount);
    void generateStudentCourseData(StudentCourseOrderExt order);


}