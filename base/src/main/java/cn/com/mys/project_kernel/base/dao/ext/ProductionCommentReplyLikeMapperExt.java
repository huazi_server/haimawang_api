package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentReplyLikeExt;
import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public interface ProductionCommentReplyLikeMapperExt {

    ProductionCommentReplyLikeExt findObjectByCondition(Map<String, Object> m);
}