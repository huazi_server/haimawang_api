package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.AdminMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.AdminMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.AdminExt;
import cn.com.mys.project_kernel.base.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public AdminMapper adminMapper;

	@Autowired
	public AdminMapperExt adminMapperExt;


	@Override
	public AdminExt findByAccountId(String accountId) {
		return adminMapperExt.findByAccountId(accountId);
	}
}
