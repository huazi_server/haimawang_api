package cn.com.mys.project_kernel.base.util;

import cn.com.mys.project_kernel.base.constant.Constant;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2019/3/6.
 */
public class ExcelImportUtils {

    public static Log log = LogFactory.getLog(ExcelImportUtils.class);


    public static List<String[]> getExcelData(MultipartFile file) {
        List<String[]> list = new ArrayList<String[]>();
        try {
            POIFSFileSystem pois = new POIFSFileSystem(file.getInputStream());
            // 新建WorkBook
            HSSFWorkbook wb = new HSSFWorkbook(pois);
            // 获取Sheet（工作薄）总个数
            int sheetNumber = wb.getNumberOfSheets();
            for (int i = 0; i < sheetNumber; i++) {
                // 获取Sheet（工作薄）
                HSSFSheet sheet = wb.getSheetAt(i);
                // 开始行数
                int firstRow = sheet.getFirstRowNum();
                // 结束行数
                int lastRow = sheet.getLastRowNum();
                // 判断该Sheet（工作薄)是否为空
                boolean isEmpty = false;
                if (firstRow == lastRow) {
                    isEmpty = true;
                }

                if (!isEmpty) {
                    for (int j = firstRow + 1; j <= lastRow; j++) {
                        // 获取一行
                        HSSFRow row = sheet.getRow(j);
                        // 开始列数
                        int firstCell = row.getFirstCellNum();
                        // 结束列数
                        int lastCell = row.getLastCellNum();
                        // 判断该行是否为空
                        String[] value = new String[lastCell];
                        if (firstCell != lastCell) {
                            for (int k = firstCell; k < lastCell; k++) {

                                // 获取一个单元格
                                HSSFCell cell = row.getCell(k);
                                Object str = null;
                                // 获取单元格，值的类型
                                int cellType = cell.getCellType();

                                if (cellType == 0) {
                                    DecimalFormat df = new DecimalFormat("########");
                                    str = df.format(cell.getNumericCellValue());
                                } else if (cellType == 1) {
                                    str = cell.getStringCellValue();
                                } else if (cellType == 2) {
                                } else if (cellType == 4) {
                                    str = cell.getBooleanCellValue();
                                }
                                value[k] = (String) str;
                            }

                        }
                        // 每一行循环完对应的就是一个用户故事的所有属性全部拿到
                        list.add(value);
                    }

                }

            }
        } catch (IOException e) {

            e.printStackTrace();
        }
        return list;
    }

    /**
     *
     * @param file
     * @param startRow 开始行 0开始
     * @param endRow 结束行 0开始 不包含
     * @param startCell 开始列 0开始
     * @param endCell 结束列 0开始 不包含
     * @param sheetAt 第一个sheet 0开始
     * @return
     */
    public static List<String[]> getExcelData(MultipartFile file, Integer startRow, Integer endRow, Integer startCell, Integer endCell, int sheetAt) {
        List<String[]> list = new ArrayList<String[]>();
        try {
            Workbook wb = WorkbookFactory.create(file.getInputStream());//适配针对excel 2003 和 excel 2007
            // 获取Sheet（工作薄）
            Sheet sheet = wb.getSheetAt(sheetAt);
            // 开始行数
            int firstRow = sheet.getFirstRowNum();
            if(startRow != null){
                firstRow = startRow;
            }
            // 结束行数
            int lastRow = sheet.getLastRowNum()+1;
            if(endRow != null){
                lastRow = endRow;
            }
            // 判断该Sheet（工作薄)是否为空
            boolean isEmpty = false;
            if (firstRow == lastRow) {
                isEmpty = true;
            }

            if (!isEmpty) {
                for (int j = firstRow; j < lastRow; j++) {
                    // 获取一行
//                XSSFRow row = sheet.getRow(j);
                    Row row=sheet.getRow(j);
                    if(row == null) {
                        throw new Exception("第" + (j + 1) + "行为空");
                    }
                    // 开始列数
                    int firstCell = row.getFirstCellNum();
                    if(startCell != null){
                        firstCell = startCell;
                    }
                    // 结束列数
                    int coloumNum=sheet.getRow(0).getPhysicalNumberOfCells();
                    if(endCell != null){
                        coloumNum = endCell;
                    }
                    int lastCell = row.getLastCellNum();
                    // 判断该行是否为空
                    String[] value = new String[coloumNum];
                    if (firstCell != lastCell) {
                        for (int k = firstCell; k < coloumNum; k++) {
                            // 获取一个单元格
                            Cell cell = row.getCell(k);
                            if(cell==null){
                                value[k] = "";
                                continue;
                            }
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                            value[k] = cell.getStringCellValue();
                        }

                        // 每一行循环完对应的就是一个用户故事的所有属性全部拿到
                        list.add(value);
                    }
                    // 每一行循环完对应的就是一个用户故事的所有属性全部拿到
                    // list.add(value);

                }

            }
        } catch (IOException e) {
            try {
                throw new Exception("非法模板{表头和模板表头不一致}")  ;
            } catch (Exception e1) {
                //e1.printStackTrace();
                log.error("非法模板{表头和模板表头不一致");
            }
        }catch (Exception e){
            try {
                throw new Exception("模板不符合要求，请下载模板");
            } catch (Exception e1) {
                //e1.printStackTrace();
                log.error("模板不符合要求，请下载模板");
            }
        }
        return list;
    }


    private static boolean isValidTitle(Row row) {
        // 开始列数
        int firstCell = row.getFirstCellNum();
        int lastCell = row.getLastCellNum();
        // 判断该行是否为空
        if (firstCell != lastCell) {
            for (int i = firstCell; i < lastCell; i++) {

                // 获取一个单元格
                Cell cell = row.getCell(i);
                if(cell == null){
                    return false;
                }
                Object str = null;
                // 获取单元格，值的类型
                int cellType = cell.getCellType();
                if (cellType == 1) {
                    str = cell.getStringCellValue();
                }


                if(!Constant.TITLES[i].equalsIgnoreCase(str.toString().trim())){
                    return false;
                }

            }
            return true;
        } else {
            return false;
        }

    }

}
