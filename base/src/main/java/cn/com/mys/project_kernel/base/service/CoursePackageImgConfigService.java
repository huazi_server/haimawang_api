package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CoursePackageImgConfigExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface CoursePackageImgConfigService {


    List<CoursePackageImgConfigExt> findListByCondition(Map<String, Object> map4Param);

}
