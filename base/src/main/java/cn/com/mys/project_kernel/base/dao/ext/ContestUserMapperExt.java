package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestUserExt;
import org.springframework.stereotype.Repository;


@Repository
public interface ContestUserMapperExt {

    ContestUserExt findByAccountId(String accountId);
}