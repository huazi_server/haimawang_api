package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.TeacherCourseLessonMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.TeacherCourseLessonMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.TeacherCourseLessonExt;
import cn.com.mys.project_kernel.base.service.TeacherCourseLessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class TeacherCourseLessonServiceImpl implements TeacherCourseLessonService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public TeacherCourseLessonMapper teacherCourseLessonMapper;

	@Autowired
	public TeacherCourseLessonMapperExt teacherCourseLessonMapperExt;


	@Override
	public TeacherCourseLessonExt findObjectByCondition(Map<String, Object> m) {
		return teacherCourseLessonMapperExt.findObjectByCondition(m);
	}

	@Override
	public Integer update(TeacherCourseLessonExt teacherCourseLessonExt) {
		return teacherCourseLessonMapper.updateByPrimaryKeySelective(teacherCourseLessonExt);
	}

	@Override
	public Integer insert(TeacherCourseLessonExt tcl) {
		return teacherCourseLessonMapper.insertSelective(tcl);
	}

	@Override
	public Integer updateByTeacherCourseId(Map<String, Object> m) {
		return teacherCourseLessonMapperExt.updateByTeacherCourseId(m);
	}
}
