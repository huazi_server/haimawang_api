package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ProductionCommentReplyMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ProductionCommentReplyLikeMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.ProductionCommentReplyMapperExt;
import cn.com.mys.project_kernel.base.entity.ProductionCommentReply;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentReplyExt;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentReplyLikeExt;
import cn.com.mys.project_kernel.base.service.ProductionCommentReplyService;
import cn.com.mys.project_kernel.base.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ProductionCommentReplyServiceImpl implements ProductionCommentReplyService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ProductionCommentReplyMapper productionCommentReplyMapper;

	@Autowired
	public ProductionCommentReplyMapperExt productionCommentReplyMapperExt;

	@Autowired
	public ProductionCommentReplyLikeMapperExt productionCommentReplyLikeMapperExt;


	//设置所属标识(1不属于自己、2属于自己)
	private void setSelfFlag(ProductionCommentReplyExt productionCommentReplyExt, String studentId){
		if(StringUtils.isNotEmpty(studentId) && Long.parseLong(studentId)>0 && productionCommentReplyExt.getStudentId()!=null && productionCommentReplyExt.getStudentId()==Long.parseLong(studentId)){
			productionCommentReplyExt.setSelfFlag(2);
		}else{
			productionCommentReplyExt.setSelfFlag(1);
		}
	}

	//设置点赞标识(1未点赞、2已点赞)
	private void setLikeFlag(ProductionCommentReplyExt productionCommentReplyExt, String studentId){
		Map<String, Object> map = new HashMap<>();
		map.put("studentId",studentId);
		map.put("commentId",productionCommentReplyExt.getId().toString());
		map.put("delFlag","0");
		ProductionCommentReplyLikeExt productionCommentReplyLikeExt = productionCommentReplyLikeMapperExt.findObjectByCondition(map);
		if(productionCommentReplyLikeExt==null || productionCommentReplyLikeExt.getId()==0){
			productionCommentReplyExt.setLikeFlag(1);
		}else{
			productionCommentReplyExt.setLikeFlag(2);
		}
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return productionCommentReplyMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ProductionCommentReplyExt> findListByCondition(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<ProductionCommentReplyExt> list = productionCommentReplyMapperExt.findListByCondition(map4Param);
		for(ProductionCommentReplyExt pcr : list){
			setSelfFlag(pcr, studentId);
			setLikeFlag(pcr, studentId);
		}
		return list;
	}

	@Override
	public ProductionCommentReply findById(long id) {
		return productionCommentReplyMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer update(ProductionCommentReply productionCommentReply) {
		return productionCommentReplyMapper.updateByPrimaryKeySelective(productionCommentReply);
	}

	@Override
	public Integer insert(ProductionCommentReplyExt productionCommentReplyExt) {
		return productionCommentReplyMapper.insertSelective(productionCommentReplyExt);
	}
}
