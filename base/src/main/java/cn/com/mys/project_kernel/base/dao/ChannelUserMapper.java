package cn.com.mys.project_kernel.base.dao;

import cn.com.mys.project_kernel.base.entity.ChannelUser;
import org.springframework.stereotype.Repository;

@Repository
public interface ChannelUserMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table channel_user
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table channel_user
     *
     * @mbggenerated
     */
    int insert(ChannelUser record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table channel_user
     *
     * @mbggenerated
     */
    int insertSelective(ChannelUser record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table channel_user
     *
     * @mbggenerated
     */
    ChannelUser selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table channel_user
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(ChannelUser record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table channel_user
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(ChannelUser record);
}