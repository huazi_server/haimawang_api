package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.StudentCourseOrderExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface StudentCourseOrderMapperExt {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<StudentCourseOrderExt> findListByCondition(Map<String, Object> map4Param);

    StudentCourseOrderExt findObjectByCondition(Map<String, Object> m);

    StudentCourseOrderExt findByOrderNo(String orderNo);

    List<StudentCourseOrderExt> findAllListByCondition(Map<String, Object> map);

    Integer updateByCondition(Map<String, Object> map);

    List<StudentCourseOrderExt> findListByActivityTuanOrderId(Map<String, Object> mm);

    List<StudentCourseOrderExt> findListByCondition4PinTuanOrder(Map<String, Object> pinTuanMap);

    List<StudentCourseOrderExt> findListByCondition4ActivityTuanOrderId(Map<String, Object> map);

    Integer findCountByCondition4Channel(Map<String, Object> map4Param);

    List<StudentCourseOrderExt> findListByCondition4Channel(Map<String, Object> map4Param);
}