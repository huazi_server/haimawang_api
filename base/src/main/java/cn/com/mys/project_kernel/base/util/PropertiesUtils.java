package cn.com.mys.project_kernel.base.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;

public class PropertiesUtils {

	
	
	
	//获取配置文件中的相关keyValue
	public static String getKeyValue(String fileName, String keyName) {
		
		//keyValue
		String keyValue = "";
		/***/
		//读取配置文件
		InputStream in;
		try {
			in = new ClassPathResource(fileName).getInputStream();
			Properties properties = new Properties();
			//properties.load(in);
			properties.load(new InputStreamReader(in, "utf-8"));
			//获取keyValue
			keyValue = properties.getProperty(keyName);
			
		} catch (IOException e) {
			
			keyValue = "";
		}
		
		return keyValue;
	}
	
	
	//获取配置文件中的相关keyValue(指定key)
	public static String getKeyValue(String keyName) {
		
		String fileName = "config.properties";
		//keyValue
		String keyValue = "";
		/***/
		//读取配置文件
		InputStream in;
		try {
			in = new ClassPathResource(fileName).getInputStream();
			Properties properties = new Properties();
			//properties.load(in);
			properties.load(new InputStreamReader(in, "utf-8"));
			//获取keyValue
			keyValue = properties.getProperty(keyName);
			
		} catch (IOException e) {
			
			keyValue = "";
		}
		
		return keyValue;
	}
	
	
	
	
	
	
	
	
	
	
}
