package cn.com.mys.project_kernel.base.service;



import cn.com.mys.project_kernel.base.entity.ext.ResourceConfigExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ResourceConfigService {


    List<ResourceConfigExt> findListByCondition(Map<String, Object> map4Param);
}
