package cn.com.mys.project_kernel.base.dao;

import cn.com.mys.project_kernel.base.entity.ProductionType;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductionTypeMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_type
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_type
     *
     * @mbggenerated
     */
    int insert(ProductionType record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_type
     *
     * @mbggenerated
     */
    int insertSelective(ProductionType record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_type
     *
     * @mbggenerated
     */
    ProductionType selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_type
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(ProductionType record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_type
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(ProductionType record);
}