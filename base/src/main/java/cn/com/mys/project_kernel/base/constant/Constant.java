package cn.com.mys.project_kernel.base.constant;

import cn.com.mys.project_kernel.base.util.PropertiesUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;

/**
 * Created by admin on 2019/1/7.
 */
public class Constant {

    public static String avatarUrl4Default = "https://huazi-api-test.oss-cn-beijing.aliyuncs.com/img/42314339997391789.png";//默认头像地址

    public static String studentName4Default = "海码学生";//默认学生名称
    public static String studentAvatarUrl4Default = "https://huazi-api-test.oss-cn-beijing.aliyuncs.com/storage/4038/3821220/6f272cdb6e04f24559f439dfe4783c76.jpg";//默认学生头像地址

    public static Integer apiPageSize = 10;//接口分页查询的每页条目数

    public static String coverUrl4Default = "https://huazi-api-test.oss-cn-beijing.aliyuncs.com/storage/4038/288848/ceec6570cb27ce5deccd09c09a66a33d.png";//默认作品封面地址

    //类型(1系统、2收到的点赞、3收到的评论、4发出的评论)
    public static String notifyType_2 = "点赞了你的作品";//收到的点赞
    public static String notifyType_3_1 = "评论了你的作品";//收到的评论
    public static String notifyType_3_2 = "回复了你的评论";//收到的回复
    public static String notifyType_4_1 = "你评论了TA的作品";//发出的评论
    public static String notifyType_4_2 = "你回复了TA的评论";//发出的回复


    public static Integer useExistFlag = 1;//是否使用已存在待付款订单的标识(1使用、2不使用)

    public static Integer payExpireSecond = 86400;//过期时间,单位:秒(24小时)

    public static Integer teacherCourseMaxCount = 12;//教师课程列表查询最大数量
    public static Integer maxCourseStage = 6;//最大课程阶段

    public static Integer loginProductId = 20111107;//访问第三方接口必须携带参数
    public static String loginProductKey = "4c2d32ab731a584eb3848cf1sdf6ee65";//访问第三方接口必须携带参数


    public static String authorName4Production = "佚名";//作品默认作者名称
    public static String authorAvatarUrl4Production = "https://huazi-api-test.oss-cn-beijing.aliyuncs.com/img/42314339997391789.png";//作品默认作者头像

    public static Integer autoChooseTeacherFlag = 2;//支付完自动分班标识(1否、2是)


    public static String qrCodeUrl4Official = "https://haimacode.oss-cn-beijing.aliyuncs.com/storage/4038/5241638/4f4a8a8056682d2a80312bff355e9c06.jpg";//公众号二维码图片地址
    public static Integer msgSendFlag4Official = 2;//是否进行公众号消息发送(1不发送、2发送)

    public static Integer appId4Official = 20111121;//访问第三方接口必须携带参数(公众号)
    public static String appKey4Official = "1s25584382df4ec2d3b6cfab731a6ee6";//访问第三方接口必须携带参数(公众号)
    public static Integer messageId2HomeworkCheck4Official = 20111105;//公众号消息模板id(作业批改完成提醒)
    public static Integer messageId2CourseStart4Official = 20111106;//公众号消息模板id(开课提醒)
    public static Integer messageId2HomeworkCommit4Official = 20111107;//公众号消息模板id(作业完成提醒)

    public static String wordData2HomeworkCheck4Official = "{\"keyword1\":\"KEYWORD1\",\"keyword2\":\"KEYWORD2\",\"keyword3\":\"KEYWORD3\"}";//公众号消息内容(作业批改完成提醒)——作业名称、所属课程、辅导老师
    public static String wordData2CourseStart4Official = "{\"keyword1\":\"KEYWORD1\",\"keyword2\":\"KEYWORD2\"}";//公众号消息内容(开课提醒)——课程名称、时间
    //public static String wordData2CourseStart4Official = "{\"keyword1\":\"KEYWORD1\",\"keyword2\":\"KEYWORD2\",\"keyword3\":\"KEYWORD3\"}";//公众号消息内容(开课提醒)——课程名称、课时标题、时间
    public static String wordData2HomeworkCommit4Official = "{\"keyword1\":\"KEYWORD1\",\"keyword2\":\"KEYWORD2\",\"keyword3\":\"KEYWORD3\"}";//公众号消息内容(作业完成提醒)——学生姓名、作业名称、完成时间


    public static Integer restrictionCount4PinTuan = 5;//拼团限制人数上限
    public static Integer createValidFlag4PinTuan = 2;//验证创建拼团有效时间标识(1不验证、2验证)
    public static Integer createEndDate4PinTuan = 12;//创建拼团订单的最后时间相对结束时间提前多久(小时)
    public static Integer mockFlag4PinTuan = 2;//模拟标识(1不模拟、2模拟) - 拼团订单在截止时间时用户人数不足的情况，是否模拟用户，使拼团订单能够成功
    public static String smsSendMsg4PinTuan = "{\"course\":\"【COURSENAME】\",\"page\":\"【我的团购】\"}";//拼团成功，发送短信消息——课程名称、查看页面




    public static Integer closeOrderFlag4CourseState = 1;//是否根据课程状态，检查关闭订单(1否、2是)



    //WEB端
    public static String appId_web = "20111113";
    public static String appKey_web = "ab731a584eb3826cf1sdf6ee654c2d32 ";
    //小程序
    public static String appId_xiaochengxu = "20111114";
    public static String appKey_xiaochengxu = "b3826cf1sdf6ee6584ec2d3254ab731a";
    //公众号
    public static String appId_gongzhonghao = "20111115";
    public static String appKey_gongzhonghao = "31a584eb3e654c2d32826cf1ab7sdf6e";





    /**
     * Excel模板的表头
     */
    public static String[] TITLES = {"序号","姓名","手机号"};


    public static String defaultPassword4Channel = "123456";//渠道默认密码
    public static Integer dayMaxUserCount4ChannelUser = 70;//当天参与人数上限
    public static Integer authCodeValidity4ChannelUser = 300;//验证码有效时间(秒)


    //competition的相关配置
    public static Integer dayVoteCount4Competition = 1;//当天投票次数限制
    public static Integer authCodeValidity4Competition = 300;//验证码有效时间(秒)


    //购课人数限制
    public static Integer buyCount = 50;
    public static Integer buyCountRefreshFlag = 1;//达到购课人数限制是否重新设置(1否、2是)





}
