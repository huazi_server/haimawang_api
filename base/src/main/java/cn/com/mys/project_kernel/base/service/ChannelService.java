package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.Channel;
import cn.com.mys.project_kernel.base.entity.ext.ChannelExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ChannelService {

    Channel findById(long id);

    Integer update(Channel channel);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ChannelExt> findListByCondition(Map<String, Object> map4Param);

    Integer insert(ChannelExt channelExt);

    ChannelExt findByAccountId(String accountId);

    ChannelExt findByPhone(String phone);
}
