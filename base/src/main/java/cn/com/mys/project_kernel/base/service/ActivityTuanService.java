package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ActivityTuan;
import cn.com.mys.project_kernel.base.entity.ext.ActivityTuanExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ActivityTuanService {


    ActivityTuan findById(Long activityTuanId);

    ActivityTuanExt findObjectByCondition(Map<String, Object> map4Param);

    Integer update(ActivityTuanExt activityTuanExt);


    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ActivityTuanExt> findListByCondition(Map<String, Object> map4Param);

    Integer insert(ActivityTuanExt activityTuanExt);
}