package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.CourseLesson;

import java.util.List;

public class CourseLessonExt extends CourseLesson {

    private StudentCourseHomeworkExt studentCourseHomeworkInfo;//学生作业
    public StudentCourseHomeworkExt getStudentCourseHomeworkInfo() {
        return studentCourseHomeworkInfo;
    }

    public void setStudentCourseHomeworkInfo(StudentCourseHomeworkExt studentCourseHomeworkInfo) {
        this.studentCourseHomeworkInfo = studentCourseHomeworkInfo;
    }


    private List<StudentCourseHomeworkExt> studentCourseHomeworkInfoList;//学生作业列表
    public List<StudentCourseHomeworkExt> getStudentCourseHomeworkInfoList() {
        return studentCourseHomeworkInfoList;
    }
    public void setStudentCourseHomeworkInfoList(List<StudentCourseHomeworkExt> studentCourseHomeworkInfoList) {
        this.studentCourseHomeworkInfoList = studentCourseHomeworkInfoList;
    }


    private Integer lockFlag;//锁标识(1关闭、2打开)
    public Integer getLockFlag() {
        return lockFlag;
    }

    public void setLockFlag(Integer lockFlag) {
        this.lockFlag = lockFlag;
    }

    private Integer allCount;//总人数
    private Integer commitCount;//提交人数
    private Integer onlineCount;//上线人数
    private Integer unCheckCount;//未批改人数
    public Integer getAllCount() {
        return allCount;
    }
    public void setAllCount(Integer allCount) {
        this.allCount = allCount;
    }
    public Integer getCommitCount() {
        return commitCount;
    }
    public void setCommitCount(Integer commitCount) {
        this.commitCount = commitCount;
    }
    public Integer getOnlineCount() {
        return onlineCount;
    }
    public void setOnlineCount(Integer onlineCount) {
        this.onlineCount = onlineCount;
    }
    public Integer getUnCheckCount() {
        return unCheckCount;
    }
    public void setUnCheckCount(Integer unCheckCount) {
        this.unCheckCount = unCheckCount;
    }

    private Integer messageCount;//学生留言数量
    public Integer getMessageCount() {
        return messageCount;
    }
    public void setMessageCount(Integer messageCount) {
        this.messageCount = messageCount;
    }

    private Long studentCourseId;//学生课程id
    public Long getStudentCourseId() {
        return studentCourseId;
    }
    public void setStudentCourseId(Long studentCourseId) {
        this.studentCourseId = studentCourseId;
    }


    private Long teacherCourseId;//教师的课程id
    public Long getTeacherCourseId() {
        return teacherCourseId;
    }
    public void setTeacherCourseId(Long teacherCourseId) {
        this.teacherCourseId = teacherCourseId;
    }


    private List courseLessonVideoPointInfoList;//课时的视频片段列表
    public List getCourseLessonVideoPointInfoList() {
        return courseLessonVideoPointInfoList;
    }
    public void setCourseLessonVideoPointInfoList(List courseLessonVideoPointInfoList) {
        this.courseLessonVideoPointInfoList = courseLessonVideoPointInfoList;
    }

    private Integer processFlag;//进行标识(1待开始、2进行中、3已完结)
    public Integer getProcessFlag() {
        return processFlag;
    }
    public void setProcessFlag(Integer processFlag) {
        this.processFlag = processFlag;
    }
}