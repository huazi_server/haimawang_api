package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.Teacher;
import cn.com.mys.project_kernel.base.entity.ext.TeacherExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface TeacherService {

    TeacherExt findByAccountId(String accountId);

    Integer updateById(TeacherExt teacherExt);

    Teacher findById(long id);

    TeacherExt findObjectById(Long id);

    Integer insert(TeacherExt teacherExt);

    List<TeacherExt> findListByType(Map<String, Object> map4Param);

    TeacherExt findByPhone(String phone);

    Integer updateOtherByAccountId(Map<String, Object> mm);
}
