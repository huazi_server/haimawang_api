package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.WeixinNotifyMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.WeixinNotifyMapperExt;
import cn.com.mys.project_kernel.base.entity.WeixinNotify;
import cn.com.mys.project_kernel.base.entity.ext.WeixinNotifyExt;
import cn.com.mys.project_kernel.base.service.WeixinNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class WeixinNotifyServiceImpl implements WeixinNotifyService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public WeixinNotifyMapper weixinNotifyMapper;

	@Autowired
	public WeixinNotifyMapperExt weixinNotifyMapperExt;


	@Override
	public Integer insert(WeixinNotifyExt weixinNotifyExt) {
		return weixinNotifyMapper.insertSelective(weixinNotifyExt);
	}

	@Override
	public WeixinNotifyExt findObjectByCondition(Map<String, Object> mm) {
		return weixinNotifyMapperExt.findObjectByCondition(mm);
	}
}
