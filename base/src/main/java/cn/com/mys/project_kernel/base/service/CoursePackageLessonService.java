package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CoursePackageLessonExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface CoursePackageLessonService {


    List<CoursePackageLessonExt> findListByCondition(Map<String, Object> map4Param);
}
