package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestGroupExt;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ContestGroupMapperExt {

    List<ContestGroupExt> findListByIds(String groupIds);
}