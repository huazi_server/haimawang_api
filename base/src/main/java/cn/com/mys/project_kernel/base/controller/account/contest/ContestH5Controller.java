package cn.com.mys.project_kernel.base.controller.account.contest;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.ConfigGrade;
import cn.com.mys.project_kernel.base.entity.ContestGroup;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.ParamUtils;
import cn.com.mys.project_kernel.base.util.RequestUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 比赛
 *
 */
@Controller
@RequestMapping(value = "/account/contest/h5")
public class ContestH5Controller {

	Log log = LogFactory.getLog(ContestH5Controller.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;
	@Autowired
	private StudentService studentService;
    @Autowired
    private ConfigGradeService configGradeService;

    @Autowired
	private ContestService contestService;
	@Autowired
	private ContestGroupService contestGroupService;
	@Autowired
	private ContestAwardService contestAwardService;
	@Autowired
	private ContestUserService contestUserService;
	@Autowired
	private ContestPlayerService contestPlayerService;
	@Autowired
	private ContestProductionService contestProductionService;
	@Autowired
	private ContestProductionVoteService contestProductionVoteService;
	@Autowired
	private ContestProductionScoreService contestProductionScoreService;
	@Autowired
	private ContestSignupConfigService contestSignupConfigService;
	@Autowired
	private ContestReviewConfigService contestReviewConfigService;
	@Autowired
	private ContestReviewTeacherService contestReviewTeacherService;
	@Autowired
	private ContestWinService contestWinService;


	//获取用户信息
	@RequestMapping(value = "user/info")
	public ResponseEntity<ResponseJson> info4User(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id
		String productionId = (String) paramMap.get("productionId");//作品id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		Map<String, Object> m = VerifyLoginToken.verifyLoginToken4Map(loginToken);

		Integer accountId = (Integer) m.get("id");
		String phone = (String) m.get("phone");
		String nickname = (String) m.get("nickname");
		String avatarUrl = (String) m.get("avatarUrl");

		if(accountId==null || accountId==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestUserExt contestUserExt = contestUserService.findByAccountId(accountId.toString());
		if(contestUserExt==null || contestUserExt.getId()==0){
			contestUserExt = new ContestUserExt();

			contestUserExt.setAccountId(accountId.longValue());
			contestUserExt.setPhone(phone);
			contestUserExt.setName(nickname);
			contestUserExt.setAvatarUrl(avatarUrl);

			contestUserExt.setState(0);
			contestUserExt.setCreatedAt(new Date());
			contestUserExt.setUpdatedAt(new Date());
			contestUserExt.setDelFlag(0);
			contestUserService.insert(contestUserExt);
		}
		contestUserExt.setAccountId(accountId.longValue());
		contestUserExt.setPhone(phone);
		contestUserExt.setName(nickname);
		contestUserExt.setAvatarUrl(avatarUrl);

		contestUserExt.setUpdatedAt(new Date());
		contestUserService.update(contestUserExt);


		Integer voteFlag = 1;//指定作品投票标识(1未投、2已投)
		if(StringUtils.isNotEmpty(contestId) && StringUtils.isNotEmpty(productionId)){
			Map<String, Object> map4Param = new HashMap<String, Object>();
			map4Param.put("delFlag", "0");
			map4Param.put("userId", contestUserExt.getId().toString());
			map4Param.put("contestId", contestId);
			map4Param.put("productionId", productionId);
			List<CompetitionProductionVoteExt> list = contestProductionVoteService.findListByCondition4Production(map4Param);//查询投票列表
			if(list!=null && list.size()>0){
				voteFlag = 2;
			}
		}
		contestUserExt.setVoteFlag(voteFlag);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestUserExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//获取比赛的作品详情
	@RequestMapping(value = "production/info")
	public ResponseEntity<ResponseJson> info4Production(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id
		String productionId = (String) paramMap.get("productionId");//作品id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		ContestUserExt contestUserExt = contestUserService.findByAccountId(accountId);
		String userId = contestUserExt!=null?String.valueOf(contestUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(userId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId) || StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		ContestProductionExt contestProductionExt = contestProductionService.findById(Long.parseLong(productionId));
		if(contestProductionExt==null || contestProductionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Integer voteFlag = 1;//指定作品投票标识(1未投、2已投)
		if(StringUtils.isNotEmpty(contestId) && StringUtils.isNotEmpty(productionId)){
			Map<String, Object> map4Param = new HashMap<String, Object>();
			map4Param.put("delFlag", "0");
			map4Param.put("userId", contestUserExt.getId().toString());
			map4Param.put("contestId", contestId);
			map4Param.put("productionId", productionId);
			List<CompetitionProductionVoteExt> list = contestProductionVoteService.findListByCondition4Production(map4Param);//查询投票列表
			if(list!=null && list.size()>0){
				voteFlag = 2;
			}
		}
		contestUserExt.setVoteFlag(voteFlag);

		Map<String, Object> m = new HashMap<>();
		m.put("contestUserInfo", contestUserExt);
		m.put("contestProductionInfo", contestProductionExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//给作品投票
	@RequestMapping(value = "production/vote")
	public ResponseEntity<ResponseJson> vote4Production(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id
		String productionId = (String) paramMap.get("productionId");//作品id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		ContestUserExt contestUserExt = contestUserService.findByAccountId(accountId);
		String userId = contestUserExt!=null?String.valueOf(contestUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(userId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId) || StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		ContestProductionExt contestProductionExt = contestProductionService.findById(Long.parseLong(productionId));
		if(contestProductionExt==null || contestProductionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}



		Integer voteFlag = 1;//指定作品投票标识(1未投、2已投)
		if(StringUtils.isNotEmpty(contestId) && StringUtils.isNotEmpty(productionId)){
			Map<String, Object> map4Param = new HashMap<String, Object>();
			map4Param.put("delFlag", "0");
			map4Param.put("userId", contestUserExt.getId().toString());
			map4Param.put("contestId", contestId);
			map4Param.put("productionId", productionId);
			List<CompetitionProductionVoteExt> list = contestProductionVoteService.findListByCondition4Production(map4Param);//查询投票列表
			if(list!=null && list.size()>0){
				voteFlag = 2;
			}
		}
		contestUserExt.setVoteFlag(voteFlag);

		if(voteFlag==2){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"您已经投票",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestProductionVoteExt contestProductionVoteExt = new ContestProductionVoteExt();
		contestProductionVoteExt.setContestId(Long.parseLong(contestId));
		contestProductionVoteExt.setProductionId(Long.parseLong(productionId));
		contestProductionVoteExt.setPlayerId(contestProductionExt.getPlayerId());
		contestProductionVoteExt.setUserId(contestUserExt.getId());

		contestProductionVoteExt.setState(0);
		contestProductionVoteExt.setCreatedAt(new Date());
		contestProductionVoteExt.setUpdatedAt(new Date());
		contestProductionVoteExt.setDelFlag(0);
		contestProductionVoteService.insert(contestProductionVoteExt);

		Integer vote = (contestProductionExt.getVote()==null||contestProductionExt.getVote()<=0)?0:contestProductionExt.getVote();
		contestProductionExt.setVote(vote+1);
		contestProductionService.update(contestProductionExt);



		//重新查询
		voteFlag = 1;//指定作品投票标识(1未投、2已投)
		if(StringUtils.isNotEmpty(contestId) && StringUtils.isNotEmpty(productionId)){
			Map<String, Object> map4Param = new HashMap<String, Object>();
			map4Param.put("delFlag", "0");
			map4Param.put("userId", contestUserExt.getId().toString());
			map4Param.put("contestId", contestId);
			map4Param.put("productionId", productionId);
			List<CompetitionProductionVoteExt> list = contestProductionVoteService.findListByCondition4Production(map4Param);//查询投票列表
			if(list!=null && list.size()>0){
				voteFlag = 2;
			}
		}
		contestUserExt.setVoteFlag(voteFlag);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestUserExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}









}
