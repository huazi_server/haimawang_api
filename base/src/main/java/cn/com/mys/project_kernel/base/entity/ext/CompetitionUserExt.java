package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.CompetitionUser;

public class CompetitionUserExt extends CompetitionUser {


    private Integer joinFlag;//指定比赛参加标识(1未参加、2已参加)
    private Integer voteFlag;//指定比赛投票标识(1可以、2不可以)
    public Integer getJoinFlag() {
        return joinFlag;
    }
    public void setJoinFlag(Integer joinFlag) {
        this.joinFlag = joinFlag;
    }
    public Integer getVoteFlag() {
        return voteFlag;
    }
    public void setVoteFlag(Integer voteFlag) {
        this.voteFlag = voteFlag;
    }

    private CompetitionPlayerExt competitionPlayerInfo;
    public CompetitionPlayerExt getCompetitionPlayerInfo() {
        return competitionPlayerInfo;
    }
    public void setCompetitionPlayerInfo(CompetitionPlayerExt competitionPlayerInfo) {
        this.competitionPlayerInfo = competitionPlayerInfo;
    }


}