package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.ext.StudentExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface StudentService {

    StudentExt findByAccountId(String accountId);

    Integer updateById(StudentExt studentExt);

    Student findById(long id);

    StudentExt findObjectById(Long id);

    Integer insert(StudentExt studentExt);

    StudentExt findByPhone(String phone);

    Integer insert4Ignore(StudentExt studentExt);

    Integer update(Student student);


    Integer findCountByCondition4Homework(Map<String, Object> map4Param);
    List<StudentExt> findListByCondition4Homework(Map<String, Object> map4Param);

    Integer findCountByCondition4Course(Map<String, Object> map4Param);
    List<StudentExt> findListByCondition4Course(Map<String, Object> map4Param);
}
