package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ProductionCommentReply;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentReplyExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ProductionCommentReplyService {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ProductionCommentReplyExt> findListByCondition(Map<String, Object> map4Param);

    ProductionCommentReply findById(long id);
    Integer update(ProductionCommentReply productionCommentReply);
    Integer insert(ProductionCommentReplyExt productionCommentReplyExt);
}
