package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestTemplateMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestTemplateMapperExt;
import cn.com.mys.project_kernel.base.entity.ContestTemplate;
import cn.com.mys.project_kernel.base.entity.ext.ContestReviewTeacherExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestTemplateExt;
import cn.com.mys.project_kernel.base.service.ContestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ContestTemplateServiceImpl implements ContestTemplateService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestTemplateMapper contestTemplateMapper;

	@Autowired
	public ContestTemplateMapperExt contestTemplateMapperExt;


	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return contestTemplateMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ContestReviewTeacherExt> findListByCondition(Map<String, Object> map4Param) {
		return contestTemplateMapperExt.findListByCondition(map4Param);
	}

	@Override
	public ContestTemplateExt findById(long id) {
		return contestTemplateMapperExt.findById(id);
	}

	@Override
	public Integer insert(ContestTemplateExt contestTemplateExt) {
		return contestTemplateMapper.insertSelective(contestTemplateExt);
	}

	@Override
	public Integer update(ContestTemplateExt contestTemplateExt) {
		return contestTemplateMapper.updateByPrimaryKeySelective(contestTemplateExt);
	}
}
