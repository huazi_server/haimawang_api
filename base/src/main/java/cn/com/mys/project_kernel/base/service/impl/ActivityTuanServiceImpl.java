package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ActivityTuanMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ActivityTuanMapperExt;
import cn.com.mys.project_kernel.base.entity.ActivityTuan;
import cn.com.mys.project_kernel.base.entity.ext.ActivityTuanExt;
import cn.com.mys.project_kernel.base.service.ActivityTuanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ActivityTuanServiceImpl implements ActivityTuanService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ActivityTuanMapper activityTuanMapper;

	@Autowired
	public ActivityTuanMapperExt activityTuanMapperExt;


	@Override
	public ActivityTuan findById(Long activityTuanId) {
		return activityTuanMapper.selectByPrimaryKey(activityTuanId);
	}

	@Override
	public ActivityTuanExt findObjectByCondition(Map<String, Object> map4Param) {
		return activityTuanMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer update(ActivityTuanExt activityTuanExt) {
		return activityTuanMapper.updateByPrimaryKeySelective(activityTuanExt);
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return activityTuanMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ActivityTuanExt> findListByCondition(Map<String, Object> map4Param) {
		return activityTuanMapperExt.findListByCondition(map4Param);
	}

	@Override
	public Integer insert(ActivityTuanExt activityTuanExt) {
		return activityTuanMapper.insertSelective(activityTuanExt);
	}
}
