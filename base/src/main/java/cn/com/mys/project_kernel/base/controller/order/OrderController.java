package cn.com.mys.project_kernel.base.controller.order;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.Course;
import cn.com.mys.project_kernel.base.entity.StudentCourse;
import cn.com.mys.project_kernel.base.entity.StudentCourseOrder;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author mingjun_T
 * 订单
 *
 */
@Controller
@RequestMapping(value = "/order")
public class OrderController {

	Log log = LogFactory.getLog(OrderController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentCourseOrderService studentCourseOrderService;
	@Autowired
	private StudentCourseOrderFlowService studentCourseOrderFlowService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private CourseLessonService courseLessonService;
	@Autowired
	private StudentCourseService studentCourseService;

	@Autowired
	private ActivityTuanService activityTuanService;
	@Autowired
	private ActivityTuanOrderService activityTuanOrderService;



	//支付或退款的回调
	@RequestMapping(value = "callback")
	public ResponseEntity<ResponseJson> callback(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String orderNo = (String) paramMap.get("orderNo");//订单编号
		String type = (String) paramMap.get("type");//类型(1支付、2退款)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		if(StringUtils.isEmpty(orderNo)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"订单编号不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(type) || (!"1".equals(type) && !"2".equals(type))){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"类型不能为空，并且取值为1或2",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//查询订单信息
		JSONObject obj = new JSONObject();
		obj.put("loginProductId", Constant.loginProductId);
		obj.put("loginProductKey", Constant.loginProductKey);
		String tradeNo = orderNo;//订单编号
		obj.put("tradeNo", tradeNo);

		JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("detailUrl"), obj);
		log.info("--"+jo.toString());

		/**
		{
			"result": true,
			"duration": 391,
			"type": "Detail execute",
			"code": 200,
			"message": "Success",
			"data": {
				"payAmount": 50,
				"detail": "详情",
				"accountId": 20111345,
				"tradeNo": "114542864360275968",
				"refundTotalAmount": 0,
				"state": 0,
				"productId": 20111105,
				"updatedAt": 1557973135000,
				"id": 9,
				"payTotalAmount": 0,
				"title": "课程",
				"refundAmount": 0,
				"orderAmount": 100,
				"createdAt": 1557916908000,
				"payType": "weixin-web",
				"payExpireSecond": 60,
				"delFlag": 0,
				"payInfoListInfo": [{
					"id": 25,
					"tradeId": 9,
					"accountId": 20111345,
					"type": "weixin-web",
					"outTradeNo": "91557973134828",
					"body": "课程",
					"detail": "详情",
					"infos": null,
					"feeType": null,
					"feeAmount": 50,
					"feeSuccessAmount": 0,
					"refundTotalAmount": 0,
					"refundAmount": 0,
					"spbillCreateIp": null,
					"timeStartAt": null,
					"timeExpireSecond": null,
					"prepayId": null,
					"request": null,
					"result": null,
					"state": 0,
					"updatedAt": 1557973135000,
					"createdAt": 1557973135000,
					"delFlag": 0
		 		},{
					"id": 21,
					"tradeId": 9,
					"accountId": 20111345,
					"type": "weixin-web",
					"outTradeNo": "91557916908422",
					"body": "课程2",
					"detail": "课程2",
					"infos": null,
					"feeType": null,
					"feeAmount": 1000,
					"feeSuccessAmount": 0,
					"refundTotalAmount": 0,
					"refundAmount": 0,
					"spbillCreateIp": null,
					"timeStartAt": null,
					"timeExpireSecond": null,
					"prepayId": null,
					"request": null,
					"result": null,
					"state": 0,
					"updatedAt": 1557918176000,
					"createdAt": 1557916908000,
					"delFlag": 0
				}],
				"refundInfoListInfo": []
			},
			"error": null,
			"version": null,
			"extParam": null
		}
		*/



		//查询订单信息
		StudentCourseOrderExt order = studentCourseOrderService.findByOrderNo(orderNo);

		//类型(1支付、2退款)
		if(StringUtils.isNotEmpty(type) && "1".equals(type)){//支付

			if(order!=null && order.getState()==1){//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
				if(jo!=null && jo.getInt("code")>0 && jo.getInt("code")==200){
					JSONObject jo_data = jo.getJSONObject("data");
					int orderAmount = jo_data.getInt("orderAmount");//订单金额
					int payTotalAmount = jo_data.getInt("payTotalAmount");//支付总金额
					int refundTotalAmount = jo_data.getInt("refundTotalAmount");//退款总金额

					Double d = order.getOrderMoney().doubleValue()*100;//订单总金额,单位:分
					Double p = d-(order.getDiscountMoney()==null?0:order.getDiscountMoney().doubleValue()*100);//支付总金额,单位:分
					if(d.intValue()==orderAmount && p.intValue()==payTotalAmount){//判断订单是否支付成功

						if(order.getState()==1){
							order.setPayMoney(BigDecimal.valueOf(p/100.0));
							order.setPayAt(new Date());
							order.setState(3);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
							order.setUpdatedAt(new Date());
							order.setDelFlag(0);
							studentCourseOrderService.update(order);
						}

						if(order.getActivityTuanOrderId()==null || order.getActivityTuanOrderId()==0){
							//生成学生课程数据
							activityTuanOrderService.generateStudentCourseData(order);
						}



					}

				}

			}


			/*order.setUpdatedAt(new Date());
			order.setDelFlag(0);
			studentCourseOrderService.update(order);*/

		}

		if(StringUtils.isNotEmpty(type) && "2".equals(type)){//退款
			//do nothing
		}



		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),order);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}











}
