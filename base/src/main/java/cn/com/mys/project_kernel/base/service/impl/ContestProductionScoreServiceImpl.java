package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestProductionScoreMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestProductionScoreMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestProductionScoreExt;
import cn.com.mys.project_kernel.base.service.ContestProductionScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ContestProductionScoreServiceImpl implements ContestProductionScoreService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestProductionScoreMapper contestProductionScoreMapper;

	@Autowired
	public ContestProductionScoreMapperExt contestProductionScoreMapperExt;

	@Override
	public List<ContestProductionScoreExt> findListByCondition(Map<String, Object> m) {
		return contestProductionScoreMapperExt.findListByCondition(m);
	}
}
