package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestAwardExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ContestAwardMapperExt {

    List<ContestAwardExt> findListByContestId(Map<String, Object> map);

    Integer deleteByContestId(Long contestId);
}