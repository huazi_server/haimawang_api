package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.Student;

import java.util.List;

public class StudentExt extends Student {

    private Integer notifyCount;//通知数量
    public Integer getNotifyCount() {
        return notifyCount;
    }

    public void setNotifyCount(Integer notifyCount) {
        this.notifyCount = notifyCount;
    }


    private Integer currentCourseStage;//当前的课程阶段
    private Integer maxCourseStage;//最大的课程阶段
    public Integer getCurrentCourseStage() {
        return currentCourseStage;
    }
    public void setCurrentCourseStage(Integer currentCourseStage) {
        this.currentCourseStage = currentCourseStage;
    }
    public Integer getMaxCourseStage() {
        return maxCourseStage;
    }
    public void setMaxCourseStage(Integer maxCourseStage) {
        this.maxCourseStage = maxCourseStage;
    }


    private StudentCourseHomeworkExt studentCourseHomeworkInfo;//学生作业
    public StudentCourseHomeworkExt getStudentCourseHomeworkInfo() {
        return studentCourseHomeworkInfo;
    }
    public void setStudentCourseHomeworkInfo(StudentCourseHomeworkExt studentCourseHomeworkInfo) {
        this.studentCourseHomeworkInfo = studentCourseHomeworkInfo;
    }


    private List<StudentCourseHomeworkExt> studentCourseHomeworkInfoList;//学生作业列表
    public List<StudentCourseHomeworkExt> getStudentCourseHomeworkInfoList() {
        return studentCourseHomeworkInfoList;
    }
    public void setStudentCourseHomeworkInfoList(List<StudentCourseHomeworkExt> studentCourseHomeworkInfoList) {
        this.studentCourseHomeworkInfoList = studentCourseHomeworkInfoList;
    }


}