package cn.com.mys.project_kernel.base.controller.account.teacher;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.CourseLesson;
import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.StudentCourseHomework;
import cn.com.mys.project_kernel.base.entity.StudentCourseHomeworkMessage;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 教师课程
 *
 */
@Controller
@RequestMapping(value = "/account/teacher/course")
public class TeacherCourseController {

	Log log = LogFactory.getLog(TeacherCourseController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private TeacherService teacherService;
	@Autowired
	private TeacherCourseService teacherCourseService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private CourseLessonService courseLessonService;
	@Autowired
	private StudentCourseHomeworkService studentCourseHomeworkService;
	@Autowired
	private StudentCourseHomeworkMessageService studentCourseHomeworkMessageService;
	@Autowired
	private TeacherCourseLessonService teacherCourseLessonService;
	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentCourseService studentCourseService;



	//查询课程列表
	@RequestMapping(value = "list")
	public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String type = (String) paramMap.get("type");//类型(1待开始、2进行中、3已结束)
		String issue = (String) paramMap.get("issue");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		/*if(StringUtils.isEmpty(type)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("teacherId", teacherId);
		map4Param.put("page", page);

		map4Param.put("issue", issue);

		String state = "";//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
		state = "";

		if(StringUtils.isEmpty(type) && StringUtils.isEmpty(issue)){
			page.setPageSize(Constant.teacherCourseMaxCount);
			map4Param.put("page", page);

			Map<String, Object> m = new LinkedHashMap<>();
			List<Map<String, Object>> list = new ArrayList<>();

			state = "3,4";
			map4Param.put("state", state);
			Integer count1 = courseService.findCountByCondition4Teacher(map4Param);
			List<CourseExt> list1 = courseService.findListByCondition4Teacher(map4Param);
			m = new LinkedHashMap<>();
			m.put("type", "1");
			m.put("title", "待开始");
			m.put("count", count1);
			m.put("list", list1);
			list.add(m);

			state = "5";
			map4Param.put("state", state);
			Integer count2 = courseService.findCountByCondition4Teacher(map4Param);
			List<CourseExt> list2 = courseService.findListByCondition4Teacher(map4Param);
			m = new LinkedHashMap<>();
			m.put("type", "2");
			m.put("title", "进行中");
			m.put("count", count2);
			m.put("list", list2);
			list.add(m);

			state = "6";
			map4Param.put("state", state);
			Integer count3 = courseService.findCountByCondition4Teacher(map4Param);
			List<CourseExt> list3 = courseService.findListByCondition4Teacher(map4Param);
			m = new LinkedHashMap<>();
			m.put("type", "3");
			m.put("title", "已结束");
			m.put("count", count3);
			m.put("list", list3);
			list.add(m);

			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		state = "3,4,5,6";
		if(StringUtils.isNotEmpty(type)){
			if("1".equals(type)){
				state = "3,4";
			}
			if("2".equals(type)){
				state = "5";
			}
			if("3".equals(type)){
				state = "6";
			}
		}
		map4Param.put("state", state);

		Integer count = courseService.findCountByCondition4Teacher(map4Param);
		List<CourseExt> list = courseService.findListByCondition4Teacher(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课程详情
	@RequestMapping(value = "detail")
	public ResponseEntity<ResponseJson> detail(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(teacherCourseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、教师课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("teacherId", teacherId);
		map4Param.put("courseId", courseId);
		map4Param.put("teacherCourseId", teacherCourseId);

		CourseExt courseExt = courseService.findObjectByCondition(map4Param);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),courseExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课时详情(包含学生作业列表)
	@RequestMapping(value = "lesson/detail")
	public ResponseEntity<ResponseJson> detail4Lesson(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(teacherCourseId) || StringUtils.isEmpty(courseLessonId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、教师课程id、课时id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("teacherId", teacherId);
		map4Param.put("courseId", courseId);
		map4Param.put("courseLessonId", courseLessonId);
		map4Param.put("teacherCourseId", teacherCourseId);

		CourseExt courseExt = courseService.findObjectByCondition(map4Param);
		List<StudentExt> studentList = courseExt.getStudentInfoList();//学生列表

		CourseLessonExt courseLessonExt = courseLessonService.findObjectByCondition(map4Param);
		//查询学生作业列表
		List<StudentCourseHomeworkExt> studentCourseHomeworkInfoList = studentCourseHomeworkService.findListByCondition4CourseLesson(map4Param);

		//获取学生的差集
		List<StudentExt> sList = new ArrayList<>();
		for(StudentExt s : studentList){
			Long studentId = s.getId();
			Integer f = 0;//无0、有1
			for(StudentCourseHomeworkExt sch : studentCourseHomeworkInfoList){
				Long sId = sch.getStudentId();
				if(studentId.longValue()==sId.longValue()){
					f = 1;
				}
			}

			if(f==0){
				sList.add(s);
			}
		}

		//构造空作业数据
		for(StudentExt s : sList){
			StudentCourseHomeworkExt studentCourseHomeworkExt = new StudentCourseHomeworkExt();
			studentCourseHomeworkExt.setStudentId(s.getId());
			studentCourseHomeworkExt.setCourseId(Long.parseLong(courseId));
			studentCourseHomeworkExt.setCourseLessonId(Long.parseLong(courseLessonId));
			studentCourseHomeworkExt.setCoverUrl(courseLessonExt.getCoverUrl());
			studentCourseHomeworkExt.setStudyFlag(1);//学习标识(1未学习、2已学习)
			studentCourseHomeworkExt.setOnlineFlag(1);//在线标识(1离线、2在线)
			studentCourseHomeworkExt.setFinishFlag(0);//完成标识(0初始化、1待修改、2已完成)
			studentCourseHomeworkExt.setRemark("");
			studentCourseHomeworkExt.setState(1);//状态(1未保存、2未提交、3已提交)
			studentCourseHomeworkExt.setCreatedAt(new Date());
			studentCourseHomeworkExt.setUpdatedAt(new Date());
			studentCourseHomeworkExt.setDelFlag(0);

			studentCourseHomeworkExt.setStudentName(StringUtils.isNotEmpty(s.getName())?s.getName():Constant.studentName4Default);
			studentCourseHomeworkExt.setStudentAvatarUrl(StringUtils.isNotEmpty(s.getAvatarUrl())?s.getAvatarUrl():Constant.studentAvatarUrl4Default);

			studentCourseHomeworkExt.setCourseLessonTitle(courseLessonExt.getTitle());
			studentCourseHomeworkExt.setCourseLessonName(courseLessonExt.getName());
			studentCourseHomeworkExt.setCourseLessonNumber(courseLessonExt.getNumber());
			String coverUrl = studentCourseHomeworkExt.getCoverUrl();
			studentCourseHomeworkExt.setCoverUrl(StringUtils.isEmpty(coverUrl)?courseLessonExt.getCoverUrl():coverUrl);
			studentCourseHomeworkExt.setCourseName((courseExt!=null&&courseExt.getId()>0&&StringUtils.isNotEmpty(courseExt.getName()))?courseExt.getName():"");

			studentCourseHomeworkInfoList.add(studentCourseHomeworkExt);
		}

		//设置学生作业列表
		courseLessonExt.setStudentCourseHomeworkInfoList(studentCourseHomeworkInfoList);

		TeacherCourseLessonExt teacherCourseLessonExt = teacherCourseLessonService.findObjectByCondition(map4Param);
		if(teacherCourseLessonExt!=null){
			courseLessonExt.setAllCount(teacherCourseLessonExt.getAllCount()==null?0:teacherCourseLessonExt.getAllCount());
			courseLessonExt.setCommitCount(teacherCourseLessonExt.getCommitCount()==null?0:teacherCourseLessonExt.getCommitCount());
			courseLessonExt.setOnlineCount(teacherCourseLessonExt.getOnlineCount()==null?0:teacherCourseLessonExt.getOnlineCount());


			Map<String, Object> m = new HashMap<String, Object>();
			//重新设置总人数
			m = new HashMap<String, Object>();
			m.put("delFlag","0");
			m.put("courseId",courseId);
			m.put("courseLessonId",courseLessonId);
			m.put("teacherCourseId",teacherCourseId);
			Integer allCount = studentCourseHomeworkService.findAllCountByCondition(m);
			courseLessonExt.setAllCount(allCount==null?0:allCount);
			//重新设置上线人数
			m = new HashMap<String, Object>();
			m.put("delFlag","0");
			m.put("courseId",courseId);
			m.put("courseLessonId",courseLessonId);
			m.put("teacherCourseId",teacherCourseId);
			m.put("onlineFlag","2");//在线标识(1离线、2在线)
			Integer onlineCount = studentCourseHomeworkService.findOnlineCountByCondition(m);
			courseLessonExt.setOnlineCount(onlineCount==null?0:onlineCount);


			//重新设置总人数
			allCount = studentList.size();
			courseLessonExt.setAllCount(allCount);

			//更新教师课时(相关人数信息)
			teacherCourseLessonExt.setAllCount(allCount);
			teacherCourseLessonExt.setOnlineCount(onlineCount);
			teacherCourseLessonService.update(teacherCourseLessonExt);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),courseLessonExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询学生作业详情
	@RequestMapping(value = "student/homework/detail")
	public ResponseEntity<ResponseJson> detail4StudentHomework(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String studentCourseHomeworkId = (String) paramMap.get("studentCourseHomeworkId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(studentCourseHomeworkId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生作业id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findById(Long.parseLong(studentCourseHomeworkId));

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseHomeworkExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//更新学生作业评语
	@RequestMapping(value = "student/homework/comment")
	public ResponseEntity<ResponseJson> comment4StudentHomework(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String studentCourseHomeworkId = (String) paramMap.get("studentCourseHomeworkId");
		String comment = (String) paramMap.get("comment");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(studentCourseHomeworkId) || StringUtils.isEmpty(comment)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生作业id、评语不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findById(Long.parseLong(studentCourseHomeworkId));
		studentCourseHomeworkExt.setComment(comment);
		studentCourseHomeworkExt.setCommentAt(new Date());
		studentCourseHomeworkExt.setCheckFlag(2);//批改标识(1未批改、2已批改)
		studentCourseHomeworkService.update(studentCourseHomeworkExt);




		//公众号消息发送
		if(Constant.msgSendFlag4Official==2){//是否进行公众号消息发送(1不发送、2发送)
			JSONObject obj = new JSONObject();
			obj.put("loginProductId", Constant.loginProductId);
			obj.put("loginProductKey", Constant.loginProductKey);
			obj.put("appId", Constant.appId4Official);
			obj.put("appKey", Constant.appKey4Official);
			obj.put("messageId", Constant.messageId2HomeworkCheck4Official);
			String wordData = Constant.wordData2HomeworkCheck4Official;
			wordData = wordData.replace("KEYWORD1",studentCourseHomeworkExt.getCourseLessonTitle());//作业名称
			wordData = wordData.replace("KEYWORD2",studentCourseHomeworkExt.getCourseName());//所属课程
			wordData = wordData.replace("KEYWORD3",teacherExt.getName());//辅导老师
			obj.put("wordData", wordData);
			Long studentId = studentCourseHomeworkExt.getStudentId();
			Student s = studentService.findById(studentId);
			obj.put("accountId", s.getAccountId());
			obj.put("webUrl", PropertiesUtils.getKeyValue("homeworkUrl4Official")+""+studentCourseHomeworkId);
			obj.put("xcxAppId", "");
			obj.put("xcxPagepath", "");

			try{
				log.info("[HomeworkCheck send msg]--"+obj.toString());
				JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("msgSend4OfficialUrl"), obj);
				log.info("[HomeworkCheck send msg]--"+jo.toString());
			}catch (Exception e){
				log.info("[HomeworkCheck send msg fail]");
			}

		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseHomeworkExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//更新学生作业完成度标识
	@RequestMapping(value = "student/homework/mark")
	public ResponseEntity<ResponseJson> mark4StudentHomework(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String studentCourseHomeworkId = (String) paramMap.get("studentCourseHomeworkId");
		String finishFlag = (String) paramMap.get("finishFlag");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(studentCourseHomeworkId) || StringUtils.isEmpty(finishFlag)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生作业id、完成度标识不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findById(Long.parseLong(studentCourseHomeworkId));
		if(studentCourseHomeworkExt==null || studentCourseHomeworkExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生作业不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(studentCourseHomeworkExt!=null && studentCourseHomeworkExt.getId()>0 && studentCourseHomeworkExt.getState()!=3){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"已提交作业才能标记",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		studentCourseHomeworkExt.setFinishFlag(Integer.parseInt(finishFlag));
		studentCourseHomeworkService.update(studentCourseHomeworkExt);

		//检查当前课程下所有作业是否全部完成
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("courseId", studentCourseHomeworkExt.getCourseId().toString());
		m.put("studentId", studentCourseHomeworkExt.getStudentId().toString());
		List<StudentCourseHomeworkExt> homeworkList = studentCourseHomeworkService.findListByCondition(m);
		int fullFinishFlag = 1;//是否全部完成标识(1否、2是)
		int finishCount = 0;
		for(StudentCourseHomeworkExt h : homeworkList){
			int _finishFlag = h.getFinishFlag();//完成标识(0初始化、1待修改、2已完成)
			if(_finishFlag==2) finishCount++;
		}
		if(homeworkList.size()>0 && homeworkList.size()==finishCount){
			fullFinishFlag = 2;
		}

		m.put("teacherCourseId", studentCourseHomeworkExt.getTeacherCourseId().toString());
		StudentCourseExt sc = studentCourseService.findObjectByCondition(m);
		if(sc!=null && sc.getId()>0) {
			sc.setFullFinishFlag(fullFinishFlag);
			studentCourseService.update(sc);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseHomeworkExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}




	//查询课程的课时作业的留言列表
	@RequestMapping(value = "lesson/homework/message/list")
	public ResponseEntity<ResponseJson> list4LessonHomeworkMessage(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");
		String sort = (String) paramMap.get("sort");//排序(1时间正序、2时间倒序)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(courseLessonId) || StringUtils.isEmpty(teacherCourseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、课时id、教师课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("teacherId", teacherId);
		map4Param.put("page", page);

		map4Param.put("courseId", courseId);
		map4Param.put("courseLessonId", courseLessonId);
		map4Param.put("teacherCourseId", teacherCourseId);
		map4Param.put("sort", sort);

		Integer count = studentCourseHomeworkMessageService.findCountByCondition(map4Param);
		List<StudentCourseHomeworkMessageExt> list = studentCourseHomeworkMessageService.findListByCondition(map4Param);

		//查询不同状态(1未解决、2已解决)的数量
		map4Param.put("state", "1");
		Integer c1 = studentCourseHomeworkMessageService.findCountByCondition(map4Param);
		map4Param.put("state", "2");
		Integer c2 = studentCourseHomeworkMessageService.findCountByCondition(map4Param);

		Map<String, Object> m1 = new HashMap<String, Object>();
		m1.put("t", "A");
		m1.put("n", "未解决");
		Map<String, Object> m2 = new HashMap<String, Object>();
		m2.put("t", "B");
		m2.put("n", "已解决");

		m1.put("c", c1);//未解决
		m2.put("c", c2);//已解决

		List<Object> l = new ArrayList<>();
		l.add(m1);l.add(m2);//t:A"未解决"、B"已解决"

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,l,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//更新留言
	@RequestMapping(value = "lesson/homework/message/update")
	public ResponseEntity<ResponseJson> update4LessonHomeworkMessage(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String studentCourseHomeworkMessageIds = (String) paramMap.get("studentCourseHomeworkMessageIds");//留言id(多个","分隔)
		String state = (String) paramMap.get("state");//状态(1未解决、2已解决)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(studentCourseHomeworkMessageIds) || StringUtils.isEmpty(state)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"留言id、状态不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		String[] ids = studentCourseHomeworkMessageIds.split(",");
		for(String id : ids){
			StudentCourseHomeworkMessage studentCourseHomeworkMessage = studentCourseHomeworkMessageService.findById(Long.parseLong(id));
			studentCourseHomeworkMessage.setState(Integer.parseInt(state));
			studentCourseHomeworkMessageService.update(studentCourseHomeworkMessage);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),null);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}








}
