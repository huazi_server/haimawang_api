package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentLikeExt;
import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public interface ProductionCommentLikeMapperExt {

    ProductionCommentLikeExt findObjectByCondition(Map<String, Object> map);
}