package cn.com.mys.project_kernel.base.controller.account.admin;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.*;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.filter.wrapper.BodyReaderHttpServletRequestWrapper;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.AdminPage;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 课程
 *
 */
@Controller
@RequestMapping(value = "/account/admin")
public class AdminController {

	Log log = LogFactory.getLog(AdminController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentCourseService studentCourseService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private StudentCourseHomeworkService studentCourseHomeworkService;
	@Autowired
	private TeacherCourseService teacherCourseService;
	@Autowired
	private CourseLessonService courseLessonService;

	@Autowired
	private CoursePackageService coursePackageService;
	@Autowired
	private CourseGroupService courseGroupService;
	@Autowired
	private ChannelCourseService channelCourseService;
	@Autowired
	private DiscountService discountService;
	@Autowired
	private StudentDiscountService studentDiscountService;

	@Autowired
	private StudentCourseOrderService studentCourseOrderService;

	@Autowired
	private AdminService adminService;
	@Autowired
	private ChannelService channelService;

	@Autowired
	private ActivityFreeService activityFreeService;
	@Autowired
	private ActivityTuanService activityTuanService;
	@Autowired
	private ChannelActivityService channelActivityService;

    @Autowired
    private ChannelUserService channelUserService;


	@Autowired
	private ProductionService productionService;

	@Autowired
	private CompetitionService competitionService;
	@Autowired
	private CompetitionAwardService competitionAwardService;
	@Autowired
	private CompetitionUserService competitionUserService;
	@Autowired
	private CompetitionPlayerService competitionPlayerService;
	@Autowired
	private CompetitionProductionService competitionProductionService;
	@Autowired
	private CompetitionProductionVoteService competitionProductionVoteService;



	//添加学生
	@RequestMapping(value = "/student/add")
	public ResponseEntity<ResponseJson> add4Student(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//请求体内的参数
		String nickName = (String) paramMap.get("nickName");//昵称

		String name = (String) paramMap.get("name");//姓名
		String phone = (String) paramMap.get("phone");//手机号
		String channelId = (String) paramMap.get("channelId");//渠道id - 非必传

		if(StringUtils.isEmpty(name) || StringUtils.isEmpty(phone)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"姓名、手机号、渠道id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

        if(StringUtils.isNotEmpty(phone) && phone.length()!=11){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"手机号位数不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }


		Integer resultType = 0;//0成功、1已存在、2失败
		String resultMsgs = "创建成功,已存在,创建失败";//返回的信息

		StudentExt studentExt = studentService.findByPhone(phone);

		try{
			if(studentExt==null || studentExt.getId()==0){
				//不存在则创建
				studentExt = new StudentExt();
				studentExt.setAccountId(null);
				studentExt.setType(0);//类型(0默认)
				studentExt.setPhone(phone);
				studentExt.setName(name);
				studentExt.setSourceType(1);//来源类型(1、WEB端)
				studentExt.setEnableFlag(1);//启用标记(1正常、2禁用)
				studentExt.setRegisterAt(new Date());
				studentExt.setRemark("");
				studentExt.setState(0);
				studentExt.setCreatedAt(new Date());
				studentExt.setUpdatedAt(new Date());
				studentExt.setDelFlag(0);
				studentService.insert4Ignore(studentExt);


				//插入操作可能由于并发而失败，所以重新查询数据
				studentExt = studentService.findByPhone(phone);


				resultType = 0;
			}else{
				resultType = 1;
			}


			if(StringUtils.isNotEmpty(channelId)){
				String channelIds = StringUtils.isEmpty(studentExt.getChannelIds())?channelId:studentExt.getChannelIds()+","+channelId;

				if(StringUtils.isNotEmpty(channelIds)){
					channelIds = StringUtils.unique(channelIds);
				}
				studentExt.setChannelIds(channelIds);
				studentService.updateById(studentExt);
			}

		}catch (Exception e){
			resultType = 2;
		}


		LinkedHashMap<String, Object> m = new LinkedHashMap<>();
		m.put("number", "");
		m.put("name", name);
		m.put("phone", phone);
		m.put("id", studentExt.getId());
		m.put("resultType", resultType);
		m.put("resultMsg", resultMsgs.split(",")[resultType]);


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//下载文件
	@RequestMapping("/student/download")
	public void download4Student(HttpServletRequest request, HttpServletResponse response) {
		DataInputStream in = null;
		OutputStream out = null;
		try{
			response.reset();// 清空输出流

			String resultFileName = System.currentTimeMillis() + ".xlsx";
			resultFileName = URLEncoder.encode(resultFileName,"UTF-8");
			request.setCharacterEncoding("utf-8");
			response.setCharacterEncoding("UTF-8");
			response.setHeader("Content-disposition", "attachment; filename=" + resultFileName);// 设定输出文件头
			response.setContentType("application/msexcel");// 定义输出类型
			//输入流：本地文件路径
			in = new DataInputStream(
					new FileInputStream(new File("[FILE LOCAL PATH]")));
			//输出流
			out = response.getOutputStream();
			//输出文件
			int bytes = 0;
			byte[] bufferOut = new byte[1024];
			while ((bytes = in.read(bufferOut)) != -1) {
				out.write(bufferOut, 0, bytes);
			}
		} catch(Exception e){
			e.printStackTrace();
			response.reset();
			try {
				OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream(), "UTF-8");
				String data = "操作异常！";
				writer.write(data);
				writer.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}finally {
			if(null != in) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(null != out) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}



	//excel文件导入
	@RequestMapping(value = "/student/import",produces = "application/json;charset=utf-8")
	@ResponseBody
	public String import4Student(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",402);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","loginToken不存在或过期");

			return jo.toString();
		}
		if(StringUtils.isEmpty(adminId)){
			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",100);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","用户不存在");

			return jo.toString();
		}

		String channelId = (String) paramMap.get("channelId");//渠道id - 非必传
		/*if(StringUtils.isEmpty(channelId)){
			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",100);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","渠道id不能为空");

			return jo.toString();
		}*/



		try {
			BodyReaderHttpServletRequestWrapper bodyReaderRequest = (BodyReaderHttpServletRequestWrapper) request;
			MultipartHttpServletRequest multipartRequest = bodyReaderRequest.getMultipartRequest();
			String fileParamName = "upload";
			List<MultipartFile> files = multipartRequest.getFiles(fileParamName);//获取文件

			MultipartFile file = files.get(0);

			//获取上传的文件的名称(包含后缀)
			String filename = file.getOriginalFilename();
			//获取名称(不含后缀)
			String fileName = filename.substring(0,filename.lastIndexOf("."));
			//获取后缀(扩展名)
			String contentType = filename.substring(filename.lastIndexOf(".")+1);
			if(!"xlsx".equals(contentType.toLowerCase()) && !"xls".equals(contentType.toLowerCase())){
				JSONObject jo = new JSONObject();
				jo.put("result",ResponseJson.FALSE);
				jo.put("code",100);
				jo.put("message",ResponseJson.FAIL);
				jo.put("error","文件格式不正确");

				return jo.toString();
			}


			//List<String[]> list = ExcelImport.getExcelData(file,1,3,0,4,0);
			List<String[]> list = ExcelImportUtils.getExcelData(file,null,null,null,null,0);
			if(list==null || list.size()==0 || !Arrays.toString(Constant.TITLES).equals(Arrays.toString(list.get(0)))){
				JSONObject jo = new JSONObject();
				jo.put("result",ResponseJson.FALSE);
				jo.put("code",100);
				jo.put("message",ResponseJson.FAIL);
				jo.put("error","文件有误或表头不正确");

				return jo.toString();
			}

			list = list.subList(1, list.size());//截取list(去掉表头)


			for(String[] s : list){
				System.out.println(Arrays.toString(s));

				String number = StringUtils.isEmpty(s[0])?"":s[0];
				if(StringUtils.isEmpty(number) || Integer.parseInt(number)<=0){
					JSONObject jo = new JSONObject();
					jo.put("result",ResponseJson.FALSE);
					jo.put("code",100);
					jo.put("message",ResponseJson.FAIL);
					jo.put("error","序号不能为空，并要大于0");

					return jo.toString();
				}

				String name = StringUtils.isEmpty(s[1])?"":s[1];
				if(StringUtils.isEmpty(name)){
					JSONObject jo = new JSONObject();
					jo.put("result",ResponseJson.FALSE);
					jo.put("code",100);
					jo.put("message",ResponseJson.FAIL);
					jo.put("error","姓名不能为空");

					return jo.toString();
				}

                String phone = StringUtils.isEmpty(s[2])?"":s[2];
                if(StringUtils.isEmpty(phone)){
                    JSONObject jo = new JSONObject();
                    jo.put("result",ResponseJson.FALSE);
                    jo.put("code",100);
                    jo.put("message",ResponseJson.FAIL);
                    jo.put("error","手机号不能为空");

                    return jo.toString();
                }
                if(StringUtils.isNotEmpty(phone) && phone.length()!=11){
                    JSONObject jo = new JSONObject();
                    jo.put("result",ResponseJson.FALSE);
                    jo.put("code",100);
                    jo.put("message",ResponseJson.FAIL);
                    jo.put("error","手机号位数不正确");

                    return jo.toString();
                }
			}


			List<LinkedHashMap<String, Object>> studentMapList = new ArrayList<>();

			for(String[] s : list){
				System.out.println(Arrays.toString(s));

				String number = StringUtils.isEmpty(s[0])?"0":s[0];
				String name = StringUtils.isEmpty(s[1])?"":s[1];
				String phone = StringUtils.isEmpty(s[2])?"":s[2];

				LinkedHashMap<String, Object> m = new LinkedHashMap<>();
				m.put("number", number);
				m.put("name", name);
				m.put("phone", phone);

				Long id = 0L;//学生id
				Integer resultType = 0;//0成功、1已存在、2失败
				String resultMsgs = "创建成功,已存在,创建失败";//返回的信息

				try{

					StudentExt studentExt = studentService.findByPhone(phone);
					if(studentExt==null || studentExt.getId()==0){
						//不存在则创建
						studentExt = new StudentExt();
						studentExt.setAccountId(null);
						studentExt.setType(0);//类型(0默认)
						studentExt.setPhone(phone);
						studentExt.setName(name);
						studentExt.setSourceType(1);//来源类型(1、WEB端)
						studentExt.setEnableFlag(1);//启用标记(1正常、2禁用)
						studentExt.setRegisterAt(new Date());
						studentExt.setRemark("");
						studentExt.setState(0);
						studentExt.setCreatedAt(new Date());
						studentExt.setUpdatedAt(new Date());
						studentExt.setDelFlag(0);
						studentService.insert4Ignore(studentExt);


						//插入操作可能由于并发而失败，所以重新查询数据
						studentExt = studentService.findByPhone(phone);


						resultType = 0;
					}else{
						resultType = 1;
					}
					id = studentExt.getId();


					if(StringUtils.isNotEmpty(channelId)){
						String channelIds = StringUtils.isEmpty(studentExt.getChannelIds())?channelId:studentExt.getChannelIds()+","+channelId;

						if(StringUtils.isNotEmpty(channelIds)){
							channelIds = StringUtils.unique(channelIds);
						}
						studentExt.setChannelIds(channelIds);
						studentService.updateById(studentExt);
					}

				}catch (Exception e){
					resultType = 2;
				}

				m.put("id", id);
				m.put("resultType", resultType);
				m.put("resultMsg", resultMsgs.split(",")[resultType]);
				studentMapList.add(m);
			}


			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.TRUE);
			jo.put("code",200);
			jo.put("message",ResponseJson.SUCCESS);
			jo.put("error","");
			jo.put("duration",ResponseJson.getExecuteTime(request));
			jo.put("extParam",ResponseJson.getExtParam(request));
			jo.put("data",studentMapList);

			return jo.toString();
		} catch (Exception e) {

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code","100");
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","导入失败");

			return jo.toString();

		}
	}





	//给学生添加课程相关数据
	@RequestMapping(value = "/student/course/create")
	public ResponseEntity<ResponseJson> createStudentCourseData(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String phones = (String) paramMap.get("phones");//手机号(多个","分隔)
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)
		String cId = (String) paramMap.get("courseId");
		String cgId = (String) paramMap.get("courseGroupId");

		String channelId = (String) paramMap.get("channelId");

		if(StringUtils.isEmpty(phones) || StringUtils.isEmpty(courseType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生手机号、课程类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType) && StringUtils.isEmpty(cId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"单课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType) && StringUtils.isEmpty(cgId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"系列课id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		String coursePackageIds = "";
		String courseIds = "";

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
			cgId = "";
			Course course = courseService.findById(Long.parseLong(cId));
			coursePackageIds += course.getCoursePackageId();
			courseIds += course.getId();
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
			cId = "";
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("courseGroupId", cgId);
			CourseGroupExt courseGroupExt = courseGroupService.findObjectByCondition(m);
			coursePackageIds = courseGroupExt.getCoursePackageIds();
			courseIds = courseGroupExt.getCourseIds();
		}

		String[] cpIds = coursePackageIds.split(",");
		String[] cIds = courseIds.split(",");


		String[] array_phone = phones.split(",");
		for(String phone : array_phone){
			StudentExt studentExt = studentService.findByPhone(phone);
			if(studentExt==null || studentExt.getId()==0){
				//不存在则创建
				studentExt = new StudentExt();
				studentExt.setAccountId(null);
				studentExt.setType(0);//类型(0默认)
				studentExt.setPhone(phone);
				studentExt.setSourceType(1);//来源类型(1、WEB端)
				studentExt.setEnableFlag(1);//启用标记(1正常、2禁用)
				studentExt.setRegisterAt(new Date());
				studentExt.setRemark("");
				studentExt.setState(0);
				studentExt.setCreatedAt(new Date());
				studentExt.setUpdatedAt(new Date());
				studentExt.setDelFlag(0);
				studentService.insert4Ignore(studentExt);


				//插入操作可能由于并发而失败，所以重新查询数据
				studentExt = studentService.findByPhone(phone);
			}

			if(StringUtils.isNotEmpty(channelId)){
				String channelIds = StringUtils.isEmpty(studentExt.getChannelIds())?channelId:studentExt.getChannelIds()+","+channelId;

				if(StringUtils.isNotEmpty(channelIds)){
					channelIds = StringUtils.unique(channelIds);
				}
				studentExt.setChannelIds(channelIds);
				studentService.updateById(studentExt);
			}



			String studentId = studentExt.getId().toString();


			for(int i=0;i<cIds.length;i++){
				String courseId = cIds[i];
				String coursePackageId = cpIds[i];
				Course c = courseService.findById(Long.parseLong(courseId));
				coursePackageId = (c.getCoursePackageId()!=null&&c.getCoursePackageId()>0)?c.getCoursePackageId().toString():coursePackageId;

				StudentCourseExt studentCourseExt = new StudentCourseExt();
				studentCourseExt.setStudentId(Long.parseLong(studentId));
				studentCourseExt.setCourseId(Long.parseLong(courseId));
				studentCourseExt.setCoursePackageId(Long.parseLong(coursePackageId));
				studentCourseExt.setCourseGroupId(c.getCourseGroupId());
				studentCourseExt.setSourceType(4);//来源类型(正常购买1、免费领2、拼团购3、直接生成4)
				studentCourseExt.setSourceId(0L);//来源id(订单id或活动id)
				studentCourseExt.setCourseStartHintFlag(1);//课程开始提醒标识(1未提醒、2已提醒)
				studentCourseExt.setFullStudyFlag(1);//是否全部学习标识(1否、2是)
				studentCourseExt.setFullFinishFlag(1);//是否全部完成标识(1否、2是)

				studentCourseExt.setRemark("");
				studentCourseExt.setState(1);//状态(1未分配教师、2已分配教师)
				studentCourseExt.setCreatedAt(new Date());
				studentCourseExt.setUpdatedAt(new Date());
				studentCourseExt.setDelFlag(0);


				Map<String, Object> m = new HashMap<>();
				//m.put("courseId", courseId);
				m.put("coursePackageId", coursePackageId);
				m.put("studentId", studentId);
				m.put("delFlag", "0");
				StudentCourseExt sc = studentCourseService.findObjectByCondition(m);
				if (sc == null || sc.getId() == 0) {
					//生成学生课程信息
					studentCourseService.insert(studentCourseExt);
				}else{
					studentCourseExt = sc;
				}


				//do nothing
				log.info("generateTeacherCourseAndStudentCourseData");
				if(Constant.autoChooseTeacherFlag.intValue()==2) {//支付完自动分班标识(1否、2是)
					//生成教师课程、学生课程信息
					log.info("[generateTeacherCourseAndStudentCourseData]:"+"	"+"studentId="+studentCourseExt.getStudentId()+",coursePackageId="+studentCourseExt.getCoursePackageId()+",courseId="+studentCourseExt.getCourseId());
					studentCourseOrderService.generateTeacherCourseAndStudentCourseData(studentCourseExt.getStudentId().toString(), studentCourseExt.getCoursePackageId().toString(), studentCourseExt.getCourseId().toString());
				}
			}



			//根据渠道课程的优惠券的关系生成学生优惠券信息
			if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(courseType) && StringUtils.isNotEmpty(studentId)){
				studentDiscountService.insert(channelId, courseType, studentId, cId, cgId, "0");
			}

		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}




	//查询渠道列表
	@RequestMapping(value = "/channel/list")
	public ResponseEntity<ResponseJson> list4Channel(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String state = (String) paramMap.get("state");//状态(1启用、2禁用)
		String name = (String) paramMap.get("name");
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", state);
		map4Param.put("name", name);
		map4Param.put("page", page);

		Integer count = channelService.findCountByCondition(map4Param);

		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}

		List<ChannelExt> list = channelService.findListByCondition(map4Param);
		for(ChannelExt c : list){
			c.setPassword("");//不返回密码
		}

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//获取渠道信息
	@RequestMapping(value = "/channel/info")
	public ResponseEntity<ResponseJson> info4Channel(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String channelId = (String) paramMap.get("channelId");

		if(StringUtils.isEmpty(channelId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Channel c = channelService.findById(Long.parseLong(channelId));

		c.setPassword("");//不返回密码
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),c);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//添加渠道
	@RequestMapping(value = "/channel/add")
	public ResponseEntity<ResponseJson> add4Channel(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String info = (String) paramMap.get("info");
		String address = (String) paramMap.get("address");
		String number = (String) paramMap.get("number");

		String phone = (String) paramMap.get("phone");
		String password = (String) paramMap.get("password");

		if(StringUtils.isEmpty(name)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"名称不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(phone)){
			ChannelExt channelExt = channelService.findByPhone(phone);
			if(channelExt!=null && channelExt.getId()>0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"手机号已存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		ChannelExt c = new ChannelExt();
		c.setName(name);
		c.setInfo(info);
		c.setAddress(address);
		c.setNumber(number);

		c.setState(1);//状态(1启用、2禁用)
		c.setCreatedAt(new Date());
		c.setUpdatedAt(new Date());
		c.setDelFlag(0);
		channelService.insert(c);


		//accountId
		Long id = 0L;
		//加密后的密码
		String password4Md5 = "";
		if(StringUtils.isNotEmpty(phone) && StringUtils.isNotEmpty(password)){
			try{
				JSONObject obj = new JSONObject();
				obj.put("loginToken", loginToken);
				obj.put("productId", Constant.loginProductId);
				obj.put("productKey", Constant.loginProductKey);
				obj.put("phone", phone);
				obj.put("password", password);
				obj.put("accountType", "channel");//账户类型
				JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("accountCreateUrl"), obj);
				log.debug("--"+jo.toString());
				if(jo!=null && jo.getBoolean("result") && jo.getJSONObject("data")!=null){
					id = jo.getJSONObject("data").getLong("id");//accountId
					password4Md5 = jo.getJSONObject("data").getString("password");//加密后的密码
				}
			}catch (Exception e){
				//do nothing
			}
		}

		c.setPhone(phone);
		c.setAccountId(id);
		c.setPassword(password4Md5);
		c.setUpdatedAt(new Date());
		channelService.update(c);

		c.setPassword("");//不返回密码
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),c);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//修改渠道
	@RequestMapping(value = "/channel/update")
	public ResponseEntity<ResponseJson> update4Channel(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String channelId = (String) paramMap.get("channelId");
		String name = (String) paramMap.get("name");
		String info = (String) paramMap.get("info");
		String address = (String) paramMap.get("address");
		String number = (String) paramMap.get("number");

		String state = (String) paramMap.get("state");//状态(1启用、2禁用)

		if(StringUtils.isEmpty(channelId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Channel c = channelService.findById(Long.parseLong(channelId));
		c.setName(name);
		c.setInfo(info);
		c.setAddress(address);
		c.setNumber(number);

		c.setState(StringUtils.isEmpty(state)?null:Integer.parseInt(state));

		c.setUpdatedAt(new Date());
		channelService.update(c);

		c.setPassword("");//不返回密码
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),c);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//修改渠道登录信息
	@RequestMapping(value = "/channel/updateLogin")
	public ResponseEntity<ResponseJson> updateLogin4Channel(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String channelId = (String) paramMap.get("channelId");
		String phone = (String) paramMap.get("phone");
		String password = (String) paramMap.get("password");

		if(StringUtils.isEmpty(channelId) || StringUtils.isEmpty(phone) || StringUtils.isEmpty(password)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id、手机号、密码不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(phone)){
			ChannelExt channelExt = channelService.findByPhone(phone);
			if(channelExt!=null && channelExt.getId()>0 && Long.parseLong(channelId)!=channelExt.getId().longValue()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"手机号已存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		Channel c = channelService.findById(Long.parseLong(channelId));
		//accountId
		Long id = 0L;
		//加密后的密码
		String password4Md5 = "";
		try{
			JSONObject obj = new JSONObject();
			obj.put("loginToken", loginToken);
			obj.put("productId", Constant.loginProductId);
			obj.put("productKey", Constant.loginProductKey);
			obj.put("phone", phone);
			obj.put("password", password);
			obj.put("accountType", "channel");//账户类型
			JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("accountCreateUrl"), obj);
			log.debug("--"+jo.toString());
			if(jo!=null && jo.getBoolean("result") && jo.getJSONObject("data")!=null){
				id = jo.getJSONObject("data").getLong("id");//accountId
				password4Md5 = jo.getJSONObject("data").getString("password");//加密后的密码
			}
		}catch (Exception e){
			//do nothing
		}

		if(StringUtils.isEmpty(password4Md5)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"修改失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		c.setPhone(phone);
		c.setAccountId(id);
		c.setPassword(password4Md5);
		c.setUpdatedAt(new Date());
		channelService.update(c);

		c.setPassword("");//不返回密码
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),c);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//删除渠道
	@RequestMapping(value = "/channel/delete")
	public ResponseEntity<ResponseJson> delete4Channel(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String channelId = (String) paramMap.get("channelId");

		if(StringUtils.isEmpty(channelId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Channel c = channelService.findById(Long.parseLong(channelId));
		c.setDelFlag(1);

		channelService.update(c);

		c.setPassword("");//不返回密码
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),c);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询优惠券列表
	@RequestMapping(value = "/discount/list")
	public ResponseEntity<ResponseJson> list4Discount(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String category = (String) paramMap.get("category");//种类(1scratch)
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)
		String type = (String) paramMap.get("type");//类型(0抵用券、1折扣券)
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("name", name);
		map4Param.put("category", category);
		map4Param.put("courseType", courseType);
		map4Param.put("type", type);
		map4Param.put("page", page);

		Integer count = discountService.findCountByCondition(map4Param);

		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}

		List<DiscountExt> list = discountService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//添加优惠券
	@RequestMapping(value = "/discount/add")
	public ResponseEntity<ResponseJson> add4Discount(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String category = (String) paramMap.get("category");//种类(1scratch)
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)
		String type = (String) paramMap.get("type");//类型(0抵用券、1折扣券)

		String validityDays = (String) paramMap.get("validityDays");//得到多久有效(天)
		String money = (String) paramMap.get("money");//满减金额(小数位保留2位)
		String amount = (String) paramMap.get("amount");//优惠额度-优惠金额或折减百分率(小数位保留2位)

		if(StringUtils.isEmpty(name) || StringUtils.isEmpty(category) || StringUtils.isEmpty(courseType) || StringUtils.isEmpty(type) || StringUtils.isEmpty(validityDays) || StringUtils.isEmpty(money) || StringUtils.isEmpty(amount)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"缺少必传参数",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(validityDays) && (!(validityDays.matches("[0-9]+")) || (Integer.parseInt(validityDays)<=0))) {//有效期的值为大于0的正整数
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"有效期的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(money)) {
			try {
				double dd = Double.parseDouble(money);
				if(dd<0){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"满减金额的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}catch (Exception e){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"满减金额的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}
		if(StringUtils.isNotEmpty(amount)) {
			try {
				double dd = Double.parseDouble(amount);
				if(dd<=0){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠额度的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}catch (Exception e){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠额度的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isNotEmpty(type) && "0".equals(type)){
			if(StringUtils.isNotEmpty(money) && StringUtils.isNotEmpty(amount)){
				if(new BigDecimal(amount).compareTo(new BigDecimal(money))==1 || new BigDecimal(amount).compareTo(new BigDecimal(money))==0){//大于money、等于money
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"抵用券优惠额度的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
		}
		if(StringUtils.isNotEmpty(type) && "1".equals(type)){
			if(StringUtils.isNotEmpty(money) && StringUtils.isNotEmpty(amount)){
				if(new BigDecimal(amount).compareTo(new BigDecimal("1"))==1 || new BigDecimal(amount).compareTo(new BigDecimal("1"))==0){//大于1、等于1
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"折扣券优惠额度的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
		}

		Discount d = new Discount();
		d.setName(name);
		d.setCategory(Integer.parseInt(category));
		d.setCourseType(Integer.parseInt(courseType));
		d.setType(Integer.parseInt(type));
		d.setValidityDays(Integer.parseInt(validityDays));
		d.setMoney(new BigDecimal(money));
		d.setAmount(new BigDecimal(amount));

		d.setState(0);
		d.setCreatedAt(new Date());
		d.setUpdatedAt(new Date());
		d.setDelFlag(0);

		discountService.insert(d);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),d);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//修改优惠券
	@RequestMapping(value = "/discount/update")
	public ResponseEntity<ResponseJson> update4Discount(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String discountId = (String) paramMap.get("discountId");
		String name = (String) paramMap.get("name");
		String category = (String) paramMap.get("category");//种类(1scratch)
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)
		String type = (String) paramMap.get("type");//类型(0抵用券、1折扣券)

		String validityDays = (String) paramMap.get("validityDays");//得到多久有效(天)
		String money = (String) paramMap.get("money");//满减金额(小数位保留2位)
		String amount = (String) paramMap.get("amount");//优惠额度-优惠金额或折减百分率(小数位保留2位)

		if(StringUtils.isEmpty(discountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Discount d = discountService.findById(Long.parseLong(discountId));
		if(d!=null && d.getId()>0 && d.getState()==1){//状态(0未发放过、1已发放过)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"已发放过的优惠券无法编辑",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(validityDays) && (!(validityDays.matches("[0-9]+")) || (Integer.parseInt(validityDays)<=0))) {//有效期的值为大于0的正整数
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"有效期的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(money)) {
			try {
				double dd = Double.parseDouble(money);
				if(dd<0){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"满减金额的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}catch (Exception e){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"满减金额的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}
		if(StringUtils.isNotEmpty(amount)) {
			try {
				double dd = Double.parseDouble(amount);
				if(dd<=0){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠额度的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}catch (Exception e){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠额度的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isNotEmpty(type) && "0".equals(type)){
			if(StringUtils.isNotEmpty(money) && StringUtils.isNotEmpty(amount)){
				if(new BigDecimal(amount).compareTo(new BigDecimal(money))==1 || new BigDecimal(amount).compareTo(new BigDecimal(money))==0){//大于money、等于money
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"抵用券优惠额度的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
		}
		if(StringUtils.isNotEmpty(type) && "1".equals(type)){
			if(StringUtils.isNotEmpty(money) && StringUtils.isNotEmpty(amount)){
				if(new BigDecimal(amount).compareTo(new BigDecimal("1"))==1 || new BigDecimal(amount).compareTo(new BigDecimal("1"))==0){//大于1、等于1
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"折扣券优惠额度的值不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
		}

		Map<String, Object> map4Param = new HashMap<>();
		map4Param.put("name", name);
		map4Param.put("category", StringUtils.isEmpty(category)?"":category);
		map4Param.put("courseType", StringUtils.isEmpty(courseType)?"":courseType);
		map4Param.put("type", StringUtils.isEmpty(type)?"":type);
		map4Param.put("validityDays", StringUtils.isEmpty(validityDays)?"":validityDays);
		map4Param.put("money", StringUtils.isEmpty(money)?"":money);
		map4Param.put("amount", StringUtils.isEmpty(amount)?"":amount);
		map4Param.put("updatedAt", new Date());

		map4Param.put("discountId", discountId);

		discountService.update4Map(map4Param);

		d = discountService.findById(Long.parseLong(discountId));

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),d);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//删除优惠券
	@RequestMapping(value = "/discount/delete")
	public ResponseEntity<ResponseJson> delete4Discount(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String discountId = (String) paramMap.get("discountId");

		if(StringUtils.isEmpty(discountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Discount d = discountService.findById(Long.parseLong(discountId));
		if(d!=null && d.getId()>0 && d.getState()==1){//状态(0未发放过、1已发放过)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"已发放过的优惠券无法编辑",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		d.setDelFlag(1);
		discountService.update(d);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),d);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询渠道下课程列表
	@RequestMapping(value = "/channel/course/list")
	public ResponseEntity<ResponseJson> list4ChannelCourse(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String channelId = (String) paramMap.get("channelId");
		String courseType = (String) paramMap.get("courseType");
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数


		if(StringUtils.isEmpty(channelId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("channelId", channelId);
		map4Param.put("courseType", courseType);
		map4Param.put("page", page);

		Integer count = channelCourseService.findCountByCondition(map4Param);

		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}

		List<ChannelCourseExt> list = channelCourseService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//添加渠道课程
	@RequestMapping(value = "/channel/course/add")
	public ResponseEntity<ResponseJson> add4ChannelCourse(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String channelId = (String) paramMap.get("channelId");
		String courseType = (String) paramMap.get("courseType");
		String courseId = (String) paramMap.get("courseId");
		String courseGroupId = (String) paramMap.get("courseGroupId");
		String price = (String) paramMap.get("price");

		String discountIds = (String) paramMap.get("discountIds");

		if(StringUtils.isEmpty(channelId) || StringUtils.isEmpty(courseType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id、课程类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType) && StringUtils.isEmpty(courseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"单课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType) && StringUtils.isEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"系列课id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(price)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}else{
			try {
				BigDecimal p = new BigDecimal(price);
				if(p.compareTo(new BigDecimal(0))==-1) {//前者小于后者
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}catch (Exception e){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
			courseGroupId = "0";
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
			courseId = "0";
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("channelId", channelId);
		map4Param.put("courseType", courseType);
		map4Param.put("courseId", courseId);
		map4Param.put("courseGroupId", courseGroupId);

		ChannelCourseExt channelCourseExt = channelCourseService.findObjectByCondition(map4Param);
		if(channelCourseExt!=null && channelCourseExt.getId()>0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复关联",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ChannelCourse cc = new ChannelCourse();
		cc.setChannelId(Long.parseLong(channelId));
		cc.setCourseType(Integer.parseInt(courseType));
		cc.setCourseId(Long.parseLong(courseId));
		cc.setCourseGroupId(Long.parseLong(courseGroupId));
		cc.setPrice(new BigDecimal(price));
		cc.setDiscountIds(discountIds);

		cc.setState(0);
		cc.setCreatedAt(new Date());
		cc.setUpdatedAt(new Date());
		cc.setDelFlag(0);

		channelCourseService.insert(cc);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),cc);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//修改渠道课程
	@RequestMapping(value = "/channel/course/update")
	public ResponseEntity<ResponseJson> update4ChannelCourse(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String channelCourseId = (String) paramMap.get("channelCourseId");
		String channelId = (String) paramMap.get("channelId");
		String courseType = (String) paramMap.get("courseType");
		String courseId = (String) paramMap.get("courseId");
		String courseGroupId = (String) paramMap.get("courseGroupId");
		String price = (String) paramMap.get("price");

		String discountIds = (String) paramMap.get("discountIds");

		if(StringUtils.isEmpty(channelCourseId) || StringUtils.isEmpty(channelId) || StringUtils.isEmpty(courseType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道课程id、渠道id、课程类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType) && StringUtils.isEmpty(courseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"单课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType) && StringUtils.isEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"系列课id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(price)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}else{
			try {
				BigDecimal p = new BigDecimal(price);
				if(p.compareTo(new BigDecimal(0))==-1) {//前者小于后者
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}catch (Exception e){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}


		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
			courseGroupId = "0";
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
			courseId = "0";
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("channelId", channelId);
		map4Param.put("courseType", courseType);
		map4Param.put("courseId", courseId);
		map4Param.put("courseGroupId", courseGroupId);

		ChannelCourseExt channelCourseExt = channelCourseService.findObjectByCondition(map4Param);
		if(channelCourseExt!=null && channelCourseExt.getId()>0 && Long.parseLong(channelCourseId)!=channelCourseExt.getId().longValue()){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复关联",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ChannelCourse cc = channelCourseService.findById(Long.parseLong(channelCourseId));
		cc.setChannelId(Long.parseLong(channelId));
		cc.setCourseType(Integer.parseInt(courseType));
		cc.setCourseId(Long.parseLong(courseId));
		cc.setCourseGroupId(Long.parseLong(courseGroupId));
		cc.setPrice(new BigDecimal(price));
		cc.setDiscountIds(discountIds);

		cc.setUpdatedAt(new Date());
		channelCourseService.update(cc);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),cc);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//删除渠道课程
	@RequestMapping(value = "/channel/course/delete")
	public ResponseEntity<ResponseJson> delete4ChannelCourse(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String channelCourseId = (String) paramMap.get("channelCourseId");

		if(StringUtils.isEmpty(channelCourseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ChannelCourse cc = channelCourseService.findById(Long.parseLong(channelCourseId));

		cc.setDelFlag(1);
		channelCourseService.update(cc);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),cc);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//保存渠道课程的优惠券信息
	@RequestMapping(value = "/channel/course/discount/save")
	public ResponseEntity<ResponseJson> save4ChannelCourseDiscount(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String channelCourseId = (String) paramMap.get("channelCourseId");
		String discountIds = (String) paramMap.get("discountIds");//优惠券id(多个","分隔)

		if(StringUtils.isEmpty(channelCourseId) || StringUtils.isEmpty(discountIds)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道课程id、优惠券id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ChannelCourse cc = channelCourseService.findById(Long.parseLong(channelCourseId));
		if(cc!=null && cc.getId()>0){
			/*String ds = cc.getDiscountIds();
			discountIds = StringUtils.isEmpty(ds)?discountIds:ds+","+discountIds;*/
			discountIds = StringUtils.unique(discountIds);//去重
			cc.setDiscountIds(discountIds);
			cc.setUpdatedAt(new Date());
			channelCourseService.update(cc);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),cc);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}





	/***************************************************************************************************************/
	/***************************************************************************************************************/
	/***************************************************************************************************************/



	//活动-免费领
	@RequestMapping(value = "/activity/free/list")
	public ResponseEntity<ResponseJson> list4ActivityFree(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("name", name);
		map4Param.put("page", page);

		Integer count = activityFreeService.findCountByCondition(map4Param);

		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}

		List<ActivityFreeExt> list = activityFreeService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	@RequestMapping(value = "/activity/free/add")
	public ResponseEntity<ResponseJson> add4ActivityFree(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String dest = (String) paramMap.get("dest");
		String intro = (String) paramMap.get("intro");
		String quotaCount = (String) paramMap.get("quotaCount");//名额人数
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)

		String courseGroupId = (String) paramMap.get("courseGroupId");//课程分组id
		String courseId = (String) paramMap.get("courseId");//课程id
		String imgUrl = (String) paramMap.get("imgUrl");//图片地址(多个","分隔)
		String remark = (String) paramMap.get("remark");//备注

		if(StringUtils.isEmpty(name) || StringUtils.isEmpty(quotaCount)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"缺少必传参数",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType) && StringUtils.isEmpty(courseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"单课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType) && StringUtils.isEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"系列课id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
			courseGroupId = "0";
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
			courseId = "0";
		}


		ActivityFreeExt activityFreeExt = new ActivityFreeExt();
		activityFreeExt.setName(name);
		activityFreeExt.setDest(dest);
		activityFreeExt.setIntro(intro);
		activityFreeExt.setQuotaCount(Integer.parseInt(quotaCount));
		activityFreeExt.setCourseType(Integer.parseInt(courseType));
		activityFreeExt.setCourseId(Long.parseLong(courseId));
		activityFreeExt.setCourseGroupId(Long.parseLong(courseGroupId));
		activityFreeExt.setImgUrl(imgUrl);

		activityFreeExt.setRemark(remark);
		activityFreeExt.setState(0);//初始化
		activityFreeExt.setCreatedAt(new Date());
		activityFreeExt.setUpdatedAt(new Date());
		activityFreeExt.setDelFlag(0);

		activityFreeService.insert(activityFreeExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityFreeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	@RequestMapping(value = "/activity/free/update")
	public ResponseEntity<ResponseJson> update4ActivityFree(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String activityId = (String) paramMap.get("activityId");
		String name = (String) paramMap.get("name");
		String dest = (String) paramMap.get("dest");
		String intro = (String) paramMap.get("intro");
		String quotaCount = (String) paramMap.get("quotaCount");//名额人数
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)

		String courseGroupId = (String) paramMap.get("courseGroupId");//课程分组id
		String courseId = (String) paramMap.get("courseId");//课程id
		String imgUrl = (String) paramMap.get("imgUrl");//图片地址(多个","分隔)
		String remark = (String) paramMap.get("remark");//备注

		String state = (String) paramMap.get("state");//状态(1下线、2上线)

		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		/*if(StringUtils.isEmpty(name) || StringUtils.isEmpty(quotaCount)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"缺少必传参数",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType) && StringUtils.isEmpty(courseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"单课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType) && StringUtils.isEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"系列课id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
			courseGroupId = "0";
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
			courseId = "0";
		}*/


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);
		ActivityFreeExt activityFreeExt = activityFreeService.findObjectByCondition(map4Param);
		/*if(activityFreeExt!=null && activityFreeExt.getId()>0 && activityFreeExt.getState()==2){//状态(1下线、2上线)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"已上线的活动不能编辑",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		activityFreeExt.setName(name);
		activityFreeExt.setDest(dest);
		activityFreeExt.setIntro(intro);
		if(StringUtils.isNotEmpty(quotaCount)){
			activityFreeExt.setQuotaCount(Integer.parseInt(quotaCount));
		}
		activityFreeExt.setImgUrl(imgUrl);

		if(StringUtils.isNotEmpty(state)){
			activityFreeExt.setState(Integer.parseInt(state));
		}

		activityFreeExt.setRemark(remark);
		activityFreeExt.setUpdatedAt(new Date());
		activityFreeExt.setDelFlag(0);

		activityFreeService.update(activityFreeExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityFreeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	@RequestMapping(value = "/activity/free/updateState")
	public ResponseEntity<ResponseJson> updateState4ActivityFree(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String activityId = (String) paramMap.get("activityId");
		String state = (String) paramMap.get("state");//状态(1下线、2上线)

		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(state)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"状态不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);
		ActivityFreeExt activityFreeExt = activityFreeService.findObjectByCondition(map4Param);
		if(StringUtils.isNotEmpty(state)){
			activityFreeExt.setState(Integer.parseInt(state));
		}
		activityFreeExt.setUpdatedAt(new Date());
		activityFreeExt.setDelFlag(0);

		activityFreeService.update(activityFreeExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityFreeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	@RequestMapping(value = "/activity/free/delete")
	public ResponseEntity<ResponseJson> delete4ActivityFree(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String activityId = (String) paramMap.get("activityId");

		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);
		ActivityFreeExt activityFreeExt = activityFreeService.findObjectByCondition(map4Param);
		if(activityFreeExt!=null && activityFreeExt.getId()>0 && activityFreeExt.getState()==2){//状态(1下线、2上线)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"已上线的活动不能删除",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		activityFreeExt.setDelFlag(1);
		activityFreeService.update(activityFreeExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityFreeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//活动-拼团领
	@RequestMapping(value = "/activity/tuan/list")
	public ResponseEntity<ResponseJson> list4ActivityTuan(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("name", name);
		map4Param.put("page", page);

		Integer count = activityTuanService.findCountByCondition(map4Param);

		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}

		List<ActivityTuanExt> list = activityTuanService.findListByCondition(map4Param);

		for(ActivityTuanExt activityTuanExt : list){
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("delFlag", "0");
			m.put("state", "1");//状态(0初始化、1正常)
			m.put("activityType", "1");//活动类型(0免费领、1拼团)
			m.put("activityId", activityTuanExt.getId().toString());
			List<ChannelActivityExt> channelActivityInfoList = channelActivityService.findListByCondition(m);
			activityTuanExt.setChannelActivityInfoList(channelActivityInfoList);
		}


		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	@RequestMapping(value = "/activity/tuan/add")
	public ResponseEntity<ResponseJson> add4ActivityTuan(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String dest = (String) paramMap.get("dest");
		String intro = (String) paramMap.get("intro");
		String restrictionCount = (String) paramMap.get("restrictionCount");//名额人数
		String price = (String) paramMap.get("price");//价格
		String startAt = (String) paramMap.get("startAt");//开始时间,形式:yyyy-MM-dd HH:mm:ss
		String endAt = (String) paramMap.get("endAt");//结束时间,形式:yyyy-MM-dd HH:mm:ss
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)
		String courseGroupIds = (String) paramMap.get("courseGroupIds");//课程分组id(多个","分隔)
		String courseIds = (String) paramMap.get("courseIds");//课程id(多个","分隔)
		String imgUrl = (String) paramMap.get("imgUrl");//图片地址(多个","分隔)
		String remark = (String) paramMap.get("remark");//备注

		if(StringUtils.isEmpty(name) || StringUtils.isEmpty(restrictionCount)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"缺少必传参数",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(DateUtils.string2Date(startAt)==null || DateUtils.string2Date(endAt)==null){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"开始时间或结束时间格式有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(price)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(price)){
			try {
				BigDecimal p = new BigDecimal(price);
				if(p.compareTo(new BigDecimal(0))==-1) {//前者小于后者
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}catch (Exception e){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isNotEmpty(restrictionCount)){
			try {
				if(Integer.parseInt(restrictionCount)>Constant.restrictionCount4PinTuan.intValue()) {
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"人数有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}catch (Exception e){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"人数有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isEmpty(courseType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType) && StringUtils.isEmpty(courseIds)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"单课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType) && StringUtils.isEmpty(courseGroupIds)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"系列课id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
			courseGroupIds = "0";
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
			courseIds = "0";
		}


		ActivityTuanExt activityTuanExt = new ActivityTuanExt();
		activityTuanExt.setName(name);
		activityTuanExt.setDest(dest);
		activityTuanExt.setIntro(intro);
		activityTuanExt.setStartAt(DateUtils.string2Date(startAt));
		activityTuanExt.setEndAt(DateUtils.string2Date(endAt));
		activityTuanExt.setPrice(new BigDecimal(price));
		activityTuanExt.setRestrictionCount(Integer.parseInt(restrictionCount));
		activityTuanExt.setCourseType(Integer.parseInt(courseType));
		activityTuanExt.setCourseIds(courseIds);
		activityTuanExt.setCourseGroupIds(courseGroupIds);
		activityTuanExt.setImgUrl(imgUrl);

		activityTuanExt.setRemark(remark);
		activityTuanExt.setState(0);//初始化
		activityTuanExt.setCreatedAt(new Date());
		activityTuanExt.setUpdatedAt(new Date());
		activityTuanExt.setDelFlag(0);

		activityTuanService.insert(activityTuanExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityTuanExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	@RequestMapping(value = "/activity/tuan/update")
	public ResponseEntity<ResponseJson> update4ActivityTuan(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String activityId = (String) paramMap.get("activityId");
		String name = (String) paramMap.get("name");
		String dest = (String) paramMap.get("dest");
		String intro = (String) paramMap.get("intro");
		String restrictionCount = (String) paramMap.get("restrictionCount");//名额人数
		String price = (String) paramMap.get("price");//价格
		String startAt = (String) paramMap.get("startAt");//开始时间,形式:yyyy-MM-dd HH:mm:ss
		String endAt = (String) paramMap.get("endAt");//结束时间,形式:yyyy-MM-dd HH:mm:ss
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)
		String courseGroupIds = (String) paramMap.get("courseGroupIds");//课程分组id(多个","分隔)
		String courseIds = (String) paramMap.get("courseIds");//课程id(多个","分隔)
		String imgUrl = (String) paramMap.get("imgUrl");//图片地址(多个","分隔)
		String remark = (String) paramMap.get("remark");//备注

		String state = (String) paramMap.get("state");//状态(0未上线、1已上线)

		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		/*if(StringUtils.isEmpty(name) || StringUtils.isEmpty(restrictionCount)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"缺少必传参数",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(DateUtils.string2Date(startAt)==null || DateUtils.string2Date(endAt)==null){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"开始时间或结束时间格式有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(price)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		if(StringUtils.isNotEmpty(price)){
			try {
				BigDecimal p = new BigDecimal(price);
				if(p.compareTo(new BigDecimal(0))==-1) {//前者小于后者
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}catch (Exception e){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isNotEmpty(restrictionCount)){
			try {
				if(Integer.parseInt(restrictionCount)>Constant.restrictionCount4PinTuan.intValue()) {
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"人数有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}catch (Exception e){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"人数有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isEmpty(courseType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType) && StringUtils.isEmpty(courseIds)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"单课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType) && StringUtils.isEmpty(courseGroupIds)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"系列课id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
			courseGroupIds = "0";
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
			courseIds = "0";
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);
		ActivityTuanExt activityTuanExt = activityTuanService.findObjectByCondition(map4Param);
		if(activityTuanExt!=null && activityTuanExt.getId()>0 && activityTuanExt.getState()==0){//状态(0未上线、1已上线)
			activityTuanExt.setStartAt(DateUtils.string2Date(startAt));
			activityTuanExt.setRestrictionCount(Integer.parseInt(restrictionCount));
		}

		activityTuanExt.setName(name);
		activityTuanExt.setDest(dest);
		activityTuanExt.setIntro(intro);

		//courseGroupIds = StringUtils.isEmpty(activityTuanExt.getCourseGroupIds())?courseGroupIds:activityTuanExt.getCourseGroupIds()+","+courseGroupIds;
		courseGroupIds = StringUtils.unique(courseGroupIds);
		//courseIds = StringUtils.isEmpty(activityTuanExt.getCourseIds())?courseIds:activityTuanExt.getCourseIds()+","+courseIds;
		courseIds = StringUtils.unique(courseIds);

		activityTuanExt.setCourseType(Integer.parseInt(courseType));
		activityTuanExt.setCourseIds(courseIds);
		activityTuanExt.setCourseGroupIds(courseGroupIds);

		activityTuanExt.setEndAt(DateUtils.string2Date(endAt));
		activityTuanExt.setPrice(new BigDecimal(price));
		activityTuanExt.setImgUrl(imgUrl);

		if(StringUtils.isNotEmpty(state)){
			activityTuanExt.setState(Integer.parseInt(state));
		}
		activityTuanExt.setRemark(remark);
		activityTuanExt.setUpdatedAt(new Date());
		activityTuanExt.setDelFlag(0);

		activityTuanService.update(activityTuanExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityTuanExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	@RequestMapping(value = "/activity/tuan/updateState")
	public ResponseEntity<ResponseJson> updateState4ActivityTuan(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String activityId = (String) paramMap.get("activityId");
		String state = (String) paramMap.get("state");//状态(0未上线、1已上线)

		if(StringUtils.isEmpty(activityId)) {
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE, "100", ResponseJson.FAIL, "活动id不能为空", ResponseJson.getExecuteTime(request), ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:" + responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(state)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"状态不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);
		ActivityTuanExt activityTuanExt = activityTuanService.findObjectByCondition(map4Param);
		if(StringUtils.isNotEmpty(state)){
			activityTuanExt.setState(Integer.parseInt(state));
		}
		activityTuanExt.setUpdatedAt(new Date());
		activityTuanExt.setDelFlag(0);

		activityTuanService.update(activityTuanExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityTuanExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	@RequestMapping(value = "/activity/tuan/delete")
	public ResponseEntity<ResponseJson> delete4ActivityTuan(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String activityId = (String) paramMap.get("activityId");

		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);
		ActivityTuanExt activityTuanExt = activityTuanService.findObjectByCondition(map4Param);
		if(activityTuanExt!=null && activityTuanExt.getId()>0 && activityTuanExt.getState()==1){//状态(0未上线、1已上线)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"已上线的活动不能删除",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		activityTuanExt.setDelFlag(1);
		activityTuanService.update(activityTuanExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityTuanExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//查询活动关联渠道
	@RequestMapping(value = "/channel/activity/list")
	public ResponseEntity<ResponseJson> list4ChannelActivity(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String activityType = (String) paramMap.get("activityType");//活动类型(0免费领、1拼团)
		String activityId = (String) paramMap.get("activityId");
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数


		if(StringUtils.isEmpty(activityType) || StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动类型、活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityType", activityType);
		map4Param.put("activityId", activityId);
		map4Param.put("page", page);

		Integer count = channelActivityService.findCountByCondition(map4Param);

		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}

		List<ChannelActivityExt> list = channelActivityService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//保存活动关联渠道
	@RequestMapping(value = "/channel/activity/save")
	public ResponseEntity<ResponseJson> save4ChannelActivity(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String activityType = (String) paramMap.get("activityType");//活动类型(0免费领、1拼团)
		String activityId = (String) paramMap.get("activityId");
		String channelIds = (String) paramMap.get("channelIds");//渠道id(多个","分隔)
		String prices = (String) paramMap.get("prices");//价格(多个","分隔)


		if(StringUtils.isEmpty(activityType) || StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动类型、活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(channelIds) || StringUtils.isEmpty(prices)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id、价格不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//保存
		if(StringUtils.isNotEmpty(channelIds) && StringUtils.isNotEmpty(prices)){
			String[] channelIdsArray = channelIds.split(",");
			String[] pricesArray = prices.split(",");
			if(channelIdsArray.length!=pricesArray.length){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id与价格长度不一致",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			for(String channelId : channelIdsArray){
				try{
					Long cId = Long.parseLong(channelId);
					if(cId<=0){
						ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
						log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
						return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
					}
				}catch (Exception e){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道id有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
			for(String price : pricesArray){
				try {
					BigDecimal p = new BigDecimal(price);
					if(p.compareTo(new BigDecimal(0))==-1) {//前者小于后者
						ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
						log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
						return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
					}
				}catch (Exception e){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}

			//先删除，后添加
			Map<String, Object> map4Param = new HashMap<String, Object>();
			map4Param.put("delFlag", "0");
			map4Param.put("activityType", activityType);
			map4Param.put("activityId", activityId);
			channelActivityService.deleteByCondition(map4Param);

			for(int i=0;i<channelIdsArray.length;i++){
				String channelId = channelIdsArray[i];
				String price = pricesArray[i];

				ChannelActivityExt channelActivityExt = new ChannelActivityExt();
				channelActivityExt.setActivityType(Integer.parseInt(activityType));
				channelActivityExt.setActivityId(Long.parseLong(activityId));
				channelActivityExt.setChannelId(Long.parseLong(channelId));
				channelActivityExt.setPrice(new BigDecimal(price));

				channelActivityExt.setState(1);//状态(0初始化、1正常)
				channelActivityExt.setCreatedAt(new Date());
				channelActivityExt.setUpdatedAt(new Date());
				channelActivityExt.setDelFlag(0);
				channelActivityService.insert(channelActivityExt);
			}
		}


		//查询结果返回
		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityType", activityType);
		map4Param.put("activityId", activityId);
		List<ChannelActivityExt> list = channelActivityService.findListByCondition(map4Param);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	/********************************************************************************/
	/********************************************************************************/
	/********************************************************************************/



	//查询作品详情
	@RequestMapping(value = "production/info")
	public ResponseEntity<ResponseJson> info4Production(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Production production = productionService.findById(Long.parseLong(productionId));
		if(production==null || production.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),production);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//上传文件
	@RequestMapping(value = "competition/uploadFile",produces = "application/json;charset=utf-8")
	@ResponseBody
	public String uploadFile4Competition(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",402);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","loginToken不存在或过期");

			return jo.toString();
		}
		if(StringUtils.isEmpty(adminId)){
			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",100);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","用户不存在");
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("loginToken", loginToken);
		map4Param.put("folderPath", "/competition/user/file");//比赛用户相关的文件保存的路径

		map4Param.put("loginProductId", Constant.loginProductId);
		map4Param.put("loginProductKey", Constant.loginProductKey);

		try {
			String result = "";

			BodyReaderHttpServletRequestWrapper bodyReaderRequest = (BodyReaderHttpServletRequestWrapper) request;
			MultipartHttpServletRequest multipartRequest = bodyReaderRequest.getMultipartRequest();
			String fileParamName = "upload";
			List<MultipartFile> files = multipartRequest.getFiles(fileParamName);//获取文件

			MultipartFile file = files.get(0);

			//获取上传的文件的名称(包含后缀)
			String filename = file.getOriginalFilename();
			//获取名称(不含后缀)
			String fileName = filename.substring(0,filename.lastIndexOf("."));
			//获取后缀(扩展名)
			String contentType = filename.substring(filename.lastIndexOf(".")+1);
			map4Param.put("fileName", filename);
			map4Param.put("contentType", contentType);

			CommonsMultipartFile cf= (CommonsMultipartFile)file;
			DiskFileItem fi = (DiskFileItem)cf.getFileItem();

			//MultipartFile转换成File
			File f = fi.getStoreLocation();

			String uploadFileUrl = PropertiesUtils.getKeyValue("uploadFileUrl");

			result = FileUtils.post4Caller(uploadFileUrl,"upload",f,map4Param);

			//System.out.println(result);

			JSONObject jo = JSONObject.fromObject(result);

			//ResponseJson responseJsonResult = new ResponseJson(_result,_code,_message,_error,ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_data);
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			return jo.toString();
		} catch (IOException e) {

			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"上传失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code","100");
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","上传失败");

			return jo.toString();

		}
	}


	//查询比赛列表
	@RequestMapping(value = "/competition/list")
	public ResponseEntity<ResponseJson> list4Competition(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数


		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("page", page);
		Integer count = competitionService.findCountByCondition(map4Param);
		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}
		List<CompetitionExt> list = competitionService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询参赛者列表
	@RequestMapping(value = "/competition/player/list")
	public ResponseEntity<ResponseJson> list4CompetitionPlayer(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		if(StringUtils.isEmpty(competitionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("competitionId", competitionId);
		map4Param.put("page", page);
		Integer count = competitionPlayerService.findCountByCondition(map4Param);
		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}
		List<CompetitionPlayerExt> list = competitionPlayerService.findListByCondition(map4Param);
		for(CompetitionPlayerExt competitionPlayerExt : list){
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("competitionId", competitionId);
			m.put("playerId", competitionPlayerExt.getId().toString());
			CompetitionProductionExt competitionProductionExt = competitionProductionService.findObjectByCondition(m);
			competitionPlayerExt.setCompetitionProductionInfo(competitionProductionExt);
		}

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//获取比赛的作品详情
	@RequestMapping(value = "competition/production/info")
	public ResponseEntity<ResponseJson> info4CompetitionProduction(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");//比赛id
		String playerId = (String) paramMap.get("playerId");//参赛者id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(competitionId) || StringUtils.isEmpty(playerId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、参赛者id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		CompetitionPlayerExt competitionPlayerExt = competitionPlayerService.findById(Long.parseLong(playerId));
		if(competitionPlayerExt==null || competitionPlayerExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"参赛者不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("playerId", playerId);
		map4Param.put("competitionId", competitionId);
		CompetitionProductionExt competitionProductionExt = competitionProductionService.findObjectByCondition(map4Param);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),competitionProductionExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//保存比赛的作品详情
	@RequestMapping(value = "competition/production/save")
	public ResponseEntity<ResponseJson> save4CompetitionProduction(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");//比赛id
		String playerId = (String) paramMap.get("playerId");//参赛者id
		String originProductionId = (String) paramMap.get("originProductionId");
		String originProductionLink = (String) paramMap.get("originProductionLink");
		String videoUrl = (String) paramMap.get("videoUrl");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(competitionId) || StringUtils.isEmpty(playerId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、参赛者id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(originProductionId) || StringUtils.isEmpty(originProductionLink) || StringUtils.isEmpty(videoUrl)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"原始作品id、原始作品链接、视频地址不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		CompetitionPlayerExt competitionPlayerExt = competitionPlayerService.findById(Long.parseLong(playerId));
		if(competitionPlayerExt==null || competitionPlayerExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"参赛者不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Production production = productionService.findById(Long.parseLong(originProductionId));
		if(production==null || production.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("playerId", playerId);
		map4Param.put("competitionId", competitionId);
		CompetitionProductionExt competitionProductionExt = competitionProductionService.findObjectByCondition(map4Param);
		if(competitionProductionExt==null || competitionProductionExt.getId()==0){
			competitionProductionExt = new CompetitionProductionExt();
			competitionProductionExt.setState(0);
			competitionProductionExt.setCreatedAt(new Date());
			competitionProductionExt.setUpdatedAt(new Date());
			competitionProductionExt.setDelFlag(0);
			competitionProductionService.insert(competitionProductionExt);
		}

		competitionProductionExt.setPlayerId(Long.parseLong(playerId));
		competitionProductionExt.setCompetitionId(Long.parseLong(competitionId));
		competitionProductionExt.setOriginProductionId(Long.parseLong(originProductionId));
		competitionProductionExt.setOriginProductionLink(originProductionLink);

		competitionProductionExt.setName(production.getName());
		competitionProductionExt.setContent(production.getContent());
		competitionProductionExt.setCoverUrl(production.getCoverUrl());
		competitionProductionExt.setOperateExplain(production.getOperateExplain());
		competitionProductionExt.setIntro(production.getIntro());

		competitionProductionExt.setVoteCount(0);
		competitionProductionExt.setVideoUrl(videoUrl);

		competitionProductionExt.setUpdatedAt(new Date());
		competitionProductionService.update(competitionProductionExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),competitionProductionExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}











}
