package cn.com.mys.project_kernel.base.controller.account.student;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.*;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.filter.wrapper.BodyReaderHttpServletRequestWrapper;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 *
 * @author mingjun_T
 * 学生课程
 *
 */
@Controller
@RequestMapping(value = "/account/student/course")
public class StudentCourseController {

	Log log = LogFactory.getLog(StudentCourseController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentCourseService studentCourseService;
	@Autowired
	private StudentCourseOrderService studentCourseOrderService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private StudentCourseHomeworkService studentCourseHomeworkService;
	@Autowired
	private TeacherCourseService teacherCourseService;
	@Autowired
	private CourseLessonService courseLessonService;
	@Autowired
	private TeacherCourseLessonService teacherCourseLessonService;
	@Autowired
	private CoursePackageService coursePackageService;



	//查询课程列表
	@RequestMapping(value = "list4Old")
	public ResponseEntity<ResponseJson> list4Old(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String issue = (String) paramMap.get("issue");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		//map4Param.put("state","3");//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束

		map4Param.put("name",name);
		map4Param.put("issue",issue);

		List<CourseExt> list = courseService.findListByCondition(map4Param);
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("studentId", studentId);
		m.put("state","3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		for(CourseExt c : list){
			m.put("courseId", c.getId().toString());
			StudentCourseOrderExt studentCourseOrderInfo = studentCourseOrderService.findObjectByCondition(m);//学生课程订单信息
			c.setStudentCourseOrderInfo(studentCourseOrderInfo);
		}


		//只保留正常数据
		CopyOnWriteArrayList<CourseExt> cowList = new CopyOnWriteArrayList<CourseExt>(list);
		for (CourseExt item : cowList) {
			StudentCourseExt sc = item.getStudentCourseInfo();
			StudentCourseOrderExt sco = item.getStudentCourseOrderInfo();
			//来源类型(正常购买1、免费领2、拼团购3、直接生成4)
			if (sc!=null && sc.getSourceType()!=null && (sc.getSourceType()==1 || sc.getSourceType()==3) && sco!=null && sco.getState()!=null && sco.getState()==3) {
				//do nothing
			}else if(sc!=null && sc.getSourceType()!=null && (sc.getSourceType()==2 || sc.getSourceType()==4)){
				//do nothing
			}
			else{
				cowList.remove(item);
			}
		}



		/***初始化教师课时列表、学生作业列表***/
		generateTeacherCourseLessonAndStudentCourseHomeworkData(studentId, cowList);



		list = courseService.findListByCondition(map4Param);
		m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("studentId", studentId);
		m.put("state","3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		for(CourseExt c : list){
			m.put("courseId", c.getId().toString());
			StudentCourseOrderExt studentCourseOrderInfo = studentCourseOrderService.findObjectByCondition(m);//学生课程订单信息
			c.setStudentCourseOrderInfo(studentCourseOrderInfo);
		}


		//只保留正常数据
		cowList = new CopyOnWriteArrayList<CourseExt>(list);
		for (CourseExt item : cowList) {
			StudentCourseExt sc = item.getStudentCourseInfo();
			StudentCourseOrderExt sco = item.getStudentCourseOrderInfo();
			//来源类型(正常购买1、免费领2、拼团购3、直接生成4)
			if (sc!=null && sc.getSourceType()!=null && (sc.getSourceType()==1 || sc.getSourceType()==3) && sco!=null && sco.getState()!=null && sco.getState()==3) {
				//do nothing
			}else if(sc!=null && sc.getSourceType()!=null && (sc.getSourceType()==2 || sc.getSourceType()==4)){
				//do nothing
			}
			else{
				cowList.remove(item);
			}
		}



		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),cowList);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课程列表
	@RequestMapping(value = "list")
	public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String issue = (String) paramMap.get("issue");

		String source = (String) paramMap.get("source");//有值并为"1"表示来自我的课程列表，将阶段为0的课程设置为5

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		List<CoursePackageExt> cpList = coursePackageService.findListByCondition(map4Param);
		List<CourseExt> cList = new ArrayList<>();

		for(CoursePackageExt cp : cpList){
			Long cpId = cp.getId();

			Map<String, Object> m = new HashMap<String, Object>();
			m.put("studentId",studentId);
			m.put("delFlag","0");
			m.put("enableFlag","1");//启用标记(1正常、2禁用)
			m.put("coursePackageId",cpId.toString());
			CourseExt c = courseService.findObjectByCondition4CoursePackageId(m);
			/*if(c==null || c.getId()==0){
				Map<String, Object> mm = new HashMap<String, Object>();
				mm.put("studentId",studentId);
				mm.put("delFlag","0");
				mm.put("coursePackageId",cpId.toString());
				c = courseService.findObjectByCoursePackageId(mm);
			}*/


			if(c!=null && c.getId()>0){
				StudentCourseOrderExt sco = null;
				Integer courseType = c.getCourseType();//课程类型(1单课程、2系列课)
				if(courseType!=null && courseType==1){
					Map<String, Object> mm = new HashMap<String, Object>();
					mm.put("delFlag", "0");
					mm.put("studentId", studentId);
					mm.put("state","3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
					mm.put("courseId", c.getId().toString());
					sco = studentCourseOrderService.findObjectByCondition(mm);//学生课程订单信息
				}
				if(courseType!=null && courseType==2){
					Map<String, Object> mm = new HashMap<String, Object>();
					mm.put("delFlag", "0");
					mm.put("studentId", studentId);
					mm.put("state","3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
					mm.put("courseType", courseType);
					sco = studentCourseOrderService.findObjectByCondition(mm);//学生课程订单信息
				}
				c.setStudentCourseOrderInfo(sco);

				StudentCourseExt sc = c.getStudentCourseInfo();


				//将阶段为0的课程设置为5
				if(StringUtils.isNotEmpty(source) && "1".equals(source) && c.getStage()!=null && c.getStage()==0 && c.getState()<=5){
					c.setState(5);//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
				}


				//只保留正常数据
				//来源类型(正常购买1、免费领2、拼团购3、直接生成4)
				if (sc!=null && sc.getSourceType()!=null && (sc.getSourceType()==1 || sc.getSourceType()==3) && sco!=null && sco.getState()!=null && sco.getState()==3) {
					cList.add(c);
				}else if(sc!=null && sc.getSourceType()!=null && (sc.getSourceType()==2 || sc.getSourceType()==4)){
					cList.add(c);
				}
				else{
					//do nothing
				}
				cp.setCourseInfo(c);
				cp.setBuyFlag(c.getBuyFlag());
			}else{
				cp.setBuyFlag(1);//购买标识(1未购买、2已购买)
			}
		}



		/***初始化教师课时列表、学生作业列表***/
		generateTeacherCourseLessonAndStudentCourseHomeworkData(studentId, cList);



		map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		cpList = coursePackageService.findListByCondition(map4Param);
		cList = new ArrayList<>();

		for(CoursePackageExt cp : cpList){
			Long cpId = cp.getId();

			Map<String, Object> m = new HashMap<String, Object>();
			m.put("studentId",studentId);
			m.put("delFlag","0");
			m.put("enableFlag","1");//启用标记(1正常、2禁用)
			m.put("coursePackageId",cpId.toString());
			CourseExt c = courseService.findObjectByCondition4CoursePackageId(m);
			/*if(c==null || c.getId()==0){
				Map<String, Object> mm = new HashMap<String, Object>();
				mm.put("studentId",studentId);
				mm.put("delFlag","0");
				mm.put("coursePackageId",cpId.toString());
				c = courseService.findObjectByCoursePackageId(mm);
			}*/


			if(c!=null && c.getId()>0){
				StudentCourseOrderExt sco = null;
				Integer courseType = c.getCourseType();//课程类型(1单课程、2系列课)
				if(courseType!=null && courseType==1){
					Map<String, Object> mm = new HashMap<String, Object>();
					mm.put("delFlag", "0");
					mm.put("studentId", studentId);
					mm.put("state","3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
					mm.put("courseId", c.getId().toString());
					sco = studentCourseOrderService.findObjectByCondition(mm);//学生课程订单信息
				}
				if(courseType!=null && courseType==2){
					Map<String, Object> mm = new HashMap<String, Object>();
					mm.put("delFlag", "0");
					mm.put("studentId", studentId);
					mm.put("state","3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
					mm.put("courseType", courseType);
					sco = studentCourseOrderService.findObjectByCondition(mm);//学生课程订单信息
				}
				c.setStudentCourseOrderInfo(sco);

				StudentCourseExt sc = c.getStudentCourseInfo();


				//将阶段为0的课程设置为5
				if(StringUtils.isNotEmpty(source) && "1".equals(source) && c.getStage()!=null && c.getStage()==0 && c.getState()<=5){
					c.setState(5);//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
				}


				//只保留正常数据
				//来源类型(正常购买1、免费领2、拼团购3、直接生成4)
				if (sc!=null && sc.getSourceType()!=null && (sc.getSourceType()==1 || sc.getSourceType()==3) && sco!=null && sco.getState()!=null && sco.getState()==3) {
					cList.add(c);
				}else if(sc!=null && sc.getSourceType()!=null && (sc.getSourceType()==2 || sc.getSourceType()==4)){
					cList.add(c);
				}
				else{
					//do nothing
				}
				cp.setCourseInfo(c);
				cp.setBuyFlag(c.getBuyFlag());
			}else{
				cp.setBuyFlag(1);//购买标识(1未购买、2已购买)
			}
		}


		Map<String, Object> m = new HashMap<>();
		m.put("coursePackageInfoList", cpList);
		m.put("courseInfoList", cList);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询课程详情
	@RequestMapping(value = "detail")
	public ResponseEntity<ResponseJson> detail(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		CourseExt courseExt = courseService.findObjectByCourseId(map4Param);



		/***初始化教师课时列表、学生作业列表***/
		generateTeacherCourseLessonAndStudentCourseHomeworkData(studentId, Arrays.asList(courseExt));



		map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		courseExt = courseService.findObjectByCourseId(map4Param);

		if(courseExt!=null && courseExt.getId()>0){
			List<CourseLessonExt> l = courseExt.getCourseLessonInfoList();

			CourseLessonExt courseLessonInfo4StayOpen = null;//待开放的第一个课时信息
			for(CourseLessonExt cl : l){
				if(cl.getLockFlag()==1){//锁标识(1关闭、2打开)
					courseLessonInfo4StayOpen = cl;
					break;
				}
			}

			courseExt.setCourseLessonInfo4StayOpen(courseLessonInfo4StayOpen);
		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),courseExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//初始化教师课时列表、学生作业列表
	public void generateTeacherCourseLessonAndStudentCourseHomeworkData(String studentId, List<CourseExt> list){
		Map<String, Object> mm = new HashMap<>();
		mm.put("delFlag", "0");
		mm.put("studentId", studentId);
		for(CourseExt c : list){
			mm.put("courseId", c.getId().toString());
			CourseExt courseExt = courseService.findObjectByCourseId(mm);
			List<CourseLessonExt> lessonList = courseExt.getCourseLessonInfoList();
			//mm.put("courseId", "");//置空，通过课程包id查询
			//mm.put("coursePackageId", c.getCoursePackageId().toString());
			StudentCourseExt sc = studentCourseService.findObjectByCondition(mm);

			//课程	状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
			//学生课程	状态(1未分配教师、2已分配教师)
			if(courseExt!=null && courseExt.getState()!=null && courseExt.getState()>=5 && sc!=null && sc.getState()==2 && sc.getTeacherCourseId()!=null && sc.getTeacherCourseId()>0 && sc.getTeacherId()!=null && sc.getTeacherId()>0){
				TeacherCourse tc = teacherCourseService.findById(sc.getTeacherCourseId());
				if(tc!=null && tc.getId()>0){
					Map<String, Object> mmm = new HashMap<>();
					for(CourseLessonExt cl : lessonList){
						mmm = new HashMap<>();
						mmm.put("delFlag", "0");
						mmm.put("courseId", c.getId().toString());
						mmm.put("courseLessonId", cl.getId().toString());
						mmm.put("teacherCourseId", tc.getId().toString());
						TeacherCourseLessonExt teacherCourseLessonExt = teacherCourseLessonService.findObjectByCondition(mmm);
						if(teacherCourseLessonExt==null || teacherCourseLessonExt.getId()==0){
							teacherCourseLessonExt = new TeacherCourseLessonExt();
							teacherCourseLessonExt.setCourseId(c.getId());
							teacherCourseLessonExt.setCourseLessonId(cl.getId());
							teacherCourseLessonExt.setTeacherCourseId(tc.getId());
							teacherCourseLessonExt.setTeacherId(tc.getTeacherId());
							teacherCourseLessonExt.setRemark("");
							teacherCourseLessonExt.setState(0);
							teacherCourseLessonExt.setCreatedAt(new Date());
							teacherCourseLessonExt.setUpdatedAt(new Date());
							teacherCourseLessonExt.setDelFlag(0);
							teacherCourseLessonService.insert(teacherCourseLessonExt);
						}


						mmm = new HashMap<>();
						mmm.put("delFlag", "0");
						mmm.put("studentId", studentId);
						mmm.put("courseId", c.getId().toString());
						mmm.put("courseLessonId", cl.getId().toString());
						mmm.put("teacherCourseId", tc.getId().toString());
						StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findObjectByCondition(mmm);
						if(studentCourseHomeworkExt==null || studentCourseHomeworkExt.getId()==0){
							studentCourseHomeworkExt = new StudentCourseHomeworkExt();
							studentCourseHomeworkExt.setStudentId(Long.parseLong(studentId));
							studentCourseHomeworkExt.setCourseId(c.getId());
							studentCourseHomeworkExt.setCourseLessonId(cl.getId());
							studentCourseHomeworkExt.setTeacherCourseId(tc.getId());
							studentCourseHomeworkExt.setTeacherCourseLessonId(teacherCourseLessonExt.getId());
							studentCourseHomeworkExt.setTeacherId(tc.getTeacherId());
							studentCourseHomeworkExt.setStudentCourseId(sc.getId());

							//studentCourseHomeworkExt.setDraft(cl.getSb3Url());//默认草稿

							studentCourseHomeworkExt.setCoverUrl(cl.getCoverUrl());
							studentCourseHomeworkExt.setOnlineFlag(1);//在线标识(1离线、2在线)
							studentCourseHomeworkExt.setStudyFlag(1);//学习标识(1未学习、2已学习)
							studentCourseHomeworkExt.setFinishFlag(0);//完成标识(0初始化、1待修改、2已完成)
							studentCourseHomeworkExt.setCheckFlag(1);//批改标识(1未批改、2已批改)

							studentCourseHomeworkExt.setRemark("");
							studentCourseHomeworkExt.setState(1);//状态(1未保存、2未提交、3已提交)
							studentCourseHomeworkExt.setCreatedAt(new Date());
							studentCourseHomeworkExt.setUpdatedAt(new Date());
							studentCourseHomeworkExt.setDelFlag(0);
							studentCourseHomeworkService.insert(studentCourseHomeworkExt);
						}

					}

				}
			}
		}


	}





	//给老师的课程打分
	@RequestMapping(value = "grade")
	public ResponseEntity<ResponseJson> grade(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");
		String score = (String) paramMap.get("score");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(teacherCourseId) || StringUtils.isEmpty(score)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、教师课程id、分值不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();

		map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		CourseExt courseExt = courseService.findObjectByCourseId(map4Param);
		if(courseExt==null || courseExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生课程不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		map4Param.put("coursePackageId",courseExt.getCoursePackageId());
		map4Param.put("teacherCourseId",teacherCourseId);
		StudentCourseExt studentCourseExt = studentCourseService.findObjectByCondition(map4Param);
		if(studentCourseExt==null || studentCourseExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生课程不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		studentCourseExt.setScore(Integer.parseInt(score));
		studentCourseService.update(studentCourseExt);

		//更新教师关联的课程评分
		TeacherCourse teacherCourse  = teacherCourseService.findById(Long.parseLong(teacherCourseId));
		if(teacherCourse==null || teacherCourse.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"教师课程不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> m = new HashMap<String, Object>();
		m.put("teacherCourseId",teacherCourseId);
		m.put("delFlag","0");
		List<StudentCourseExt> studentCourseList  = studentCourseService.findListByCondition(m);
		Integer score4Teacher = 0;
		for(StudentCourseExt sce : studentCourseList){
			score4Teacher = score4Teacher+(sce.getScore()==null?0:sce.getScore());
		}
		teacherCourse.setScore(score4Teacher/studentCourseList.size());
		teacherCourseService.update(teacherCourse);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课时列表
	@RequestMapping(value = "lesson/list")
	public ResponseEntity<ResponseJson> list4Lesson(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(teacherCourseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、教师课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		CourseExt courseExt = courseService.findObjectByCourseId(map4Param);

		List<CourseLessonExt> list = (courseExt!=null && courseExt.getCourseLessonInfoList()!=null)?courseExt.getCourseLessonInfoList():new ArrayList<CourseLessonExt>();

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课时详情(包含作业信息)
	@RequestMapping(value = "lesson/detail")
	public ResponseEntity<ResponseJson> detail4Lesson(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(courseLessonId) || StringUtils.isEmpty(teacherCourseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、课时id、教师课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		map4Param.put("courseLessonId",courseLessonId);
		map4Param.put("teacherCourseId",teacherCourseId);
		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findObjectByCondition(map4Param);

		Map<String, Object> m = new HashMap<String, Object>();
		m.put("courseLessonId",courseLessonId);
		CourseLessonExt courseLessonExt = courseLessonService.findObjectByCondition(m);
		courseLessonExt.setStudentCourseHomeworkInfo(studentCourseHomeworkExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),courseLessonExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}







}
