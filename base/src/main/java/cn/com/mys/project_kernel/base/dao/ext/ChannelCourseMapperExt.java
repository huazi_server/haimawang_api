package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ChannelCourseExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ChannelCourseMapperExt {

    List<ChannelCourseExt> findListByCondition(Map<String, Object> m);

    Integer findCountByCondition(Map<String, Object> map4Param);

    ChannelCourseExt findObjectByCondition(Map<String, Object> map4Param);
}