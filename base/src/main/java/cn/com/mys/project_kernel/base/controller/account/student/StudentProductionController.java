package cn.com.mys.project_kernel.base.controller.account.student;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.*;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.filter.wrapper.BodyReaderHttpServletRequestWrapper;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 学生作品
 *
 */
@Controller
@RequestMapping(value = "/account/student/production")
public class StudentProductionController {

	Log log = LogFactory.getLog(StudentProductionController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private ProductionService productionService;

	@Autowired
	private StudentProductionCollectService studentProductionCollectService;
	@Autowired
	private StudentProductionLikeService studentProductionLikeService;

	@Autowired
	private ReportService reportService;


	@Autowired
	private ProductionCommentService productionCommentService;
	@Autowired
	private ProductionCommentReplyService productionCommentReplyService;
	@Autowired
	private ProductionCommentLikeService productionCommentLikeService;
	@Autowired
	private ProductionCommentReplyLikeService productionCommentReplyLikeService;

	@Autowired
	private NotifyService notifyService;

	@Autowired
	private TeacherService teacherService;


	//查询作品列表(作品首页)
	@RequestMapping(value = "home/list")
	public ResponseEntity<ResponseJson> list4Home(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String type = (String) paramMap.get("type");//作品类型(1精选、2推荐、3优秀)
		String state = (String) paramMap.get("state");//状态(1未发布、2已发布、3已下线)
		String name = (String) paramMap.get("name");


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		/*if(StringUtils.isEmpty(type)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("page", page);
		map4Param.put("state", "2");
		map4Param.put("name", name);

		//来源类型(1系统、2教师、3学生)
		String sourceType = "";
		if(StringUtils.isEmpty(type)){
			sourceType = "2,3";
		}
		map4Param.put("sourceType", sourceType);
		map4Param.put("type", type);

		Integer count = productionService.findCountByCondition4All(map4Param);
		List<ProductionExt> list = productionService.findListByCondition4All(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询作品详情
	@RequestMapping(value = "detail")
	public ResponseEntity<ResponseJson> detail(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");
		String lookCount4AddFlag = (String) paramMap.get("lookCount4AddFlag");//浏览量是否增加的标识(1不增加、2增加)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("productionId", productionId);

		ProductionExt productionExt = productionService.findObjectByCondition(map4Param);

		/*if(productionExt!=null && StringUtils.isNotEmpty(lookCount4AddFlag) && "2".equals(lookCount4AddFlag)){
			productionExt.setLookCount((productionExt.getLookCount()==null||productionExt.getLookCount()==0)?0+1:productionExt.getLookCount()+1);
			productionService.update(productionExt);
		}*/


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//保存作品
	@RequestMapping(value = "save")
	public ResponseEntity<ResponseJson> save(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String sourceType = (String) paramMap.get("sourceType");//来源类型(1系统、2教师、3学生)
		String name = (String) paramMap.get("name");//名称
		String content = (String) paramMap.get("content");//内容地址
		String coverUrl = (String) paramMap.get("coverUrl");//封面图片url
		String intro = (String) paramMap.get("intro");//介绍
		String operateExplain = (String) paramMap.get("operateExplain");//操作说明
		String tagIds = (String) paramMap.get("tagIds");//标签id(多个","分隔)
		String openFlag = (String) paramMap.get("openFlag");//开放标记(1开放、2不开放)
		String category = (String) paramMap.get("category");//种类(1scratch)


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(sourceType) || StringUtils.isEmpty(category)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"来源类型、种类不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ProductionExt productionExt = new ProductionExt();
		productionExt.setPersonId(Long.parseLong(studentId));
		productionExt.setSourceType(StringUtils.isEmpty(sourceType)?null:Integer.parseInt(sourceType));
		productionExt.setName(name);
		productionExt.setContent(content);
		productionExt.setCoverUrl(StringUtils.isEmpty(coverUrl)?Constant.coverUrl4Default:coverUrl);
		productionExt.setIntro(intro);
		productionExt.setOperateExplain(operateExplain);
		productionExt.setTagIds(tagIds);
		productionExt.setOpenFlag(StringUtils.isEmpty(openFlag)?null:Integer.parseInt(openFlag));
		productionExt.setCategory(StringUtils.isEmpty(category)?null:Integer.parseInt(category));

		productionExt.setLikeCount(0);
		productionExt.setLookCount(0);
		productionExt.setCommentCount(0);
		productionExt.setScore(0);

		productionExt.setState(1);//状态(1未发布、2已发布、3已下线)
		productionExt.setCreatedAt(new Date());
		productionExt.setUpdatedAt(new Date());
		productionExt.setDelFlag(0);
		productionService.insert(productionExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//更新作品
	@RequestMapping(value = "update")
	public ResponseEntity<ResponseJson> update(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		String sourceType = (String) paramMap.get("sourceType");//来源类型(1系统、2教师、3学生)
		String name = (String) paramMap.get("name");//名称
		String content = (String) paramMap.get("content");//内容地址
		String coverUrl = (String) paramMap.get("coverUrl");//封面图片url
		String intro = (String) paramMap.get("intro");//介绍
		String operateExplain = (String) paramMap.get("operateExplain");//操作说明
		String tagIds = (String) paramMap.get("tagIds");//标签id(多个","分隔)
		String openFlag = (String) paramMap.get("openFlag");//开放标记(1开放、2不开放)
		String category = (String) paramMap.get("category");//种类(1scratch)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Production production = productionService.findById(Long.parseLong(productionId));
		production.setPersonId(Long.parseLong(studentId));
		production.setSourceType(StringUtils.isEmpty(sourceType)?null:Integer.parseInt(sourceType));
		production.setName(name);
		production.setContent(content);
		production.setCoverUrl(StringUtils.isEmpty(coverUrl)?Constant.coverUrl4Default:coverUrl);
		production.setIntro(intro);
		production.setOperateExplain(operateExplain);
		production.setTagIds(tagIds);
		production.setOpenFlag(StringUtils.isEmpty(openFlag)?null:Integer.parseInt(openFlag));
		production.setCategory(StringUtils.isEmpty(category)?null:Integer.parseInt(category));

		//production.setLikeCount(0);
		//production.setLookCount(0);
		//production.setCommentCount(0);
		//production.setScore(0);

		//production.setState(1);//状态(1未发布、2已发布、3已下线)
		production.setCreatedAt(new Date());
		production.setUpdatedAt(new Date());
		production.setDelFlag(0);
		productionService.update(production);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),production);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//删除作品
	@RequestMapping(value = "delete")
	public ResponseEntity<ResponseJson> delete(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Production production = productionService.findById(Long.parseLong(productionId));

		/*if(production!=null && production.getState()==2){//状态(1未发布、2已发布、3已下线)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"已发布作品不能直接删除",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		production.setDelFlag(1);
		productionService.update(production);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),production);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//发布作品
	@RequestMapping(value = "publish")
	public ResponseEntity<ResponseJson> publish(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		String sourceType = (String) paramMap.get("sourceType");//来源类型(1系统、2教师、3学生)
		String name = (String) paramMap.get("name");//名称
		String content = (String) paramMap.get("content");//内容地址
		String coverUrl = (String) paramMap.get("coverUrl");//封面图片url
		String intro = (String) paramMap.get("intro");//介绍
		String operateExplain = (String) paramMap.get("operateExplain");//操作说明
		String tagIds = (String) paramMap.get("tagIds");//标签id(多个","分隔)
		String openFlag = (String) paramMap.get("openFlag");//开放标记(1开放、2不开放)
		String category = (String) paramMap.get("category");//种类(1scratch)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(sourceType) || StringUtils.isEmpty(category)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品来源类型、种类不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(content) || StringUtils.isEmpty(openFlag)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"内容、开放标识不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(name) || StringUtils.isEmpty(intro) || StringUtils.isEmpty(operateExplain)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"名称、介绍、操作说明不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Production production = productionService.findById(Long.parseLong(productionId));
		if(production==null || production.getId()==0){
			ProductionExt productionExt = new ProductionExt();
			//新增
			productionExt.setPersonId(Long.parseLong(studentId));
			productionExt.setSourceType(StringUtils.isEmpty(sourceType)?null:Integer.parseInt(sourceType));
			productionExt.setName(name);
			productionExt.setContent(content);
			productionExt.setCoverUrl(StringUtils.isEmpty(coverUrl)?Constant.coverUrl4Default:coverUrl);
			productionExt.setIntro(intro);
			productionExt.setOperateExplain(operateExplain);
			productionExt.setTagIds(tagIds);
			productionExt.setOpenFlag(StringUtils.isEmpty(openFlag)?null:Integer.parseInt(openFlag));
			productionExt.setCategory(StringUtils.isEmpty(category)?null:Integer.parseInt(category));

			productionExt.setLikeCount(0);
			productionExt.setLookCount(0);
			productionExt.setCommentCount(0);
			productionExt.setScore(0);

			productionExt.setState(2);//状态(1未发布、2已发布、3已下线)
			productionExt.setCreatedAt(new Date());
			productionExt.setUpdatedAt(new Date());
			productionExt.setDelFlag(0);
			productionService.insert(productionExt);

			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionExt);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//更新
		production.setPersonId(Long.parseLong(studentId));
		production.setSourceType(StringUtils.isEmpty(sourceType)?null:Integer.parseInt(sourceType));
		production.setName(name);
		production.setContent(content);
		production.setCoverUrl(StringUtils.isEmpty(coverUrl)?Constant.coverUrl4Default:coverUrl);
		production.setIntro(intro);
		production.setOperateExplain(operateExplain);
		production.setTagIds(tagIds);
		production.setOpenFlag(StringUtils.isEmpty(openFlag)?null:Integer.parseInt(openFlag));
		production.setCategory(StringUtils.isEmpty(category)?null:Integer.parseInt(category));

		//production.setLikeCount(0);
		//production.setLookCount(0);
		//production.setCommentCount(0);
		//production.setScore(0);

		production.setState(2);//状态(1未发布、2已发布、3已下线)
		production.setCreatedAt(new Date());
		production.setUpdatedAt(new Date());
		production.setDelFlag(0);
		productionService.update(production);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),production);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询作品列表(个人)
	@RequestMapping(value = "list")
	public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String sourceType = (String) paramMap.get("sourceType");//来源类型(1系统、2教师、3学生)
		String state = (String) paramMap.get("state");//状态(1未发布、2已发布、3已下线)
		String name = (String) paramMap.get("name");


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("personId", studentId);
		map4Param.put("studentId", studentId);
		map4Param.put("page", page);

		map4Param.put("sourceType", "3");
		map4Param.put("state", state);
		map4Param.put("name", name);


		Integer count = productionService.findCountByCondition(map4Param);
		List<ProductionExt> list = productionService.findListByCondition(map4Param);

		//查询不同状态(1未发布、2已发布、3已下线)的数量
		map4Param.put("state", "1");
		Integer c1 = productionService.findCountByCondition(map4Param);
		map4Param.put("state", "2");
		Integer c2 = productionService.findCountByCondition(map4Param);
		map4Param.put("state", "3");
		Integer c3 = productionService.findCountByCondition(map4Param);

		Map<String, Object> m1 = new HashMap<String, Object>();
		m1.put("t", "A");
		m1.put("n", "未发布");
		Map<String, Object> m2 = new HashMap<String, Object>();
		m2.put("t", "B");
		m2.put("n", "已发布");
		Map<String, Object> m3 = new HashMap<String, Object>();
		m3.put("t", "C");
		m3.put("n", "已下线");

		m1.put("c", c1);//未发布
		m2.put("c", c2);//已发布
		m3.put("c", c3);//已下线

		List<Object> l = new ArrayList<>();
		l.add(m2);l.add(m1);//t:A"未发布"、B"已发布"

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,l,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}









	/********************************************************************************************************/



	//查询收藏作品列表
	@RequestMapping(value = "collect/list")
	public ResponseEntity<ResponseJson> list4Collect(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String state = (String) paramMap.get("state");//状态(1未发布、2已发布、3已下线)
		String name = (String) paramMap.get("name");


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("page", page);

		map4Param.put("state", "2");
		map4Param.put("name", name);


		Integer count = productionService.findCountByCondition4Collect(map4Param);
		List<ProductionExt> list = productionService.findListByCondition4Collect(map4Param);

		//查询点赞数量
		Integer c = productionService.findCountByCondition4Like(map4Param);
		Map<String, Object> m1 = new HashMap<String, Object>();
		m1.put("t", "A");
		m1.put("n", "收藏");
		Map<String, Object> m2 = new HashMap<String, Object>();
		m2.put("t", "B");
		m2.put("n", "点赞");

		m1.put("c", count);//收藏
		m2.put("c", c);//点赞

		List<Object> l = new ArrayList<>();
		l.add(m1);l.add(m2);//t:A"收藏"、B"点赞"

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,l,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询点赞作品列表
	@RequestMapping(value = "like/list")
	public ResponseEntity<ResponseJson> list4Like(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String state = (String) paramMap.get("state");//状态(1未发布、2已发布、3已下线)
		String name = (String) paramMap.get("name");


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("page", page);

		map4Param.put("state", "2");
		map4Param.put("name", name);


		Integer count = productionService.findCountByCondition4Like(map4Param);
		List<ProductionExt> list = productionService.findListByCondition4Like(map4Param);

		//查询收藏数量
		Integer c = productionService.findCountByCondition4Collect(map4Param);
		Map<String, Object> m1 = new HashMap<String, Object>();
		m1.put("t", "A");
		m1.put("n", "收藏");
		Map<String, Object> m2 = new HashMap<String, Object>();
		m2.put("t", "B");
		m2.put("n", "点赞");

		m1.put("c", c);//收藏
		m2.put("c", count);//点赞

		List<Object> l = new ArrayList<>();
		l.add(m1);l.add(m2);//t:A"收藏"、B"点赞"

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,l,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//作品的收藏
	@RequestMapping(value = "collect/add")
	public ResponseEntity<ResponseJson> add4Collect(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("productionId", productionId);

		StudentProductionCollectExt studentProductionCollectExt = studentProductionCollectService.findObjectByCondition(map4Param);
		if(studentProductionCollectExt!=null && studentProductionCollectExt.getId()>0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"该作品已被收藏",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(studentProductionCollectExt==null){
			studentProductionCollectExt = new StudentProductionCollectExt();
			studentProductionCollectExt.setProductionId(Long.parseLong(productionId));
			studentProductionCollectExt.setStudentId(Long.parseLong(studentId));
			studentProductionCollectExt.setState(0);
			studentProductionCollectExt.setCreatedAt(new Date());
			studentProductionCollectExt.setUpdatedAt(new Date());
			studentProductionCollectExt.setDelFlag(0);
			studentProductionCollectService.insert(studentProductionCollectExt);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentProductionCollectExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//作品的取消收藏
	@RequestMapping(value = "collect/delete")
	public ResponseEntity<ResponseJson> delete4Collect(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("productionId", productionId);

		StudentProductionCollectExt studentProductionCollectExt = studentProductionCollectService.findObjectByCondition(map4Param);
		if(studentProductionCollectExt==null || studentProductionCollectExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"该作品未被收藏",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		studentProductionCollectExt.setDelFlag(1);
		studentProductionCollectService.delete(studentProductionCollectExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentProductionCollectExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//作品的点赞
	@RequestMapping(value = "like/add")
	public ResponseEntity<ResponseJson> add4Like(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("productionId", productionId);

		StudentProductionLikeExt studentProductionLikeExt = studentProductionLikeService.findObjectByCondition(map4Param);
		if(studentProductionLikeExt!=null && studentProductionLikeExt.getId()>0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"该作品已被点赞",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(studentProductionLikeExt==null){
			studentProductionLikeExt = new StudentProductionLikeExt();
			studentProductionLikeExt.setProductionId(Long.parseLong(productionId));
			studentProductionLikeExt.setStudentId(Long.parseLong(studentId));
			studentProductionLikeExt.setState(0);
			studentProductionLikeExt.setCreatedAt(new Date());
			studentProductionLikeExt.setUpdatedAt(new Date());
			studentProductionLikeExt.setDelFlag(0);
			studentProductionLikeService.insert(studentProductionLikeExt);

			Production production = productionService.findById(Long.parseLong(productionId));
			production.setLikeCount((production.getLikeCount()==null||production.getLikeCount()==0)?0+1:production.getLikeCount()+1);
			productionService.update(production);


			//添加学生通知消息数据
			Integer sourceType = production.getSourceType();//来源类型(1系统、2教师、3学生)
			if(sourceType==1){
				//do nothing
			}
			if(sourceType==2){
				Long id = production.getPersonId();
				Teacher teacher = teacherService.findById(id);//查询教师
				//do nothing
			}
			if(sourceType==3){
				Long id = production.getPersonId();
				Student student = studentService.findById(id);//查询学生

				NotifyExt notify = new NotifyExt();
				notify.setOwnerType(2);//所属类型(1教师、2学生)
				notify.setPersonId(id);
				notify.setType(2);//类型(1系统、2收到的点赞、3收到的评论、4发出的评论)
				notify.setFromType(2);//来自类型(1教师、2学生)
				notify.setFromId(Long.parseLong(studentId));
				notify.setFromName(studentExt.getName());
				notify.setFromAvatarUrl(studentExt.getAvatarUrl());
				notify.setToType(2);//接收类型(1教师、2学生)
				notify.setToId(student.getId());
				notify.setToName(student.getName());
				notify.setToAvatarUrl(student.getAvatarUrl());
				notify.setContent(Constant.notifyType_2);
				notify.setProductionId(production.getId());
				notify.setProductionName(production.getName());
				notify.setRemark("");
				notify.setState(1);//状态(1未读、2已读)
				notify.setCreatedAt(new Date());
				notify.setUpdatedAt(new Date());
				notify.setDelFlag(0);
				notifyService.insert(notify);
			}
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentProductionLikeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//作品的取消点赞
	@RequestMapping(value = "like/delete")
	public ResponseEntity<ResponseJson> delete4Like(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("productionId", productionId);

		StudentProductionLikeExt studentProductionLikeExt = studentProductionLikeService.findObjectByCondition(map4Param);
		if(studentProductionLikeExt==null || studentProductionLikeExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"该作品未被点赞",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		studentProductionLikeExt.setDelFlag(1);
		studentProductionLikeService.delete(studentProductionLikeExt);

		Production production = productionService.findById(Long.parseLong(productionId));
		production.setLikeCount((production.getLikeCount()==null||production.getLikeCount()==0)?0:production.getLikeCount()-1);
		productionService.update(production);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentProductionLikeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//举报作品、评论或回复
	@RequestMapping(value = "report")
	public ResponseEntity<ResponseJson> report(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String type = (String) paramMap.get("type");//类型(1作品、2评论、3回复)
		String defendantId = (String) paramMap.get("defendantId");//作品、评论、回复id
		String reportConfigId = (String) paramMap.get("reportConfigId");//配置id
		String title = (String) paramMap.get("title");//标题(关键词)
		String content = (String) paramMap.get("content");//内容
		String imgUrl = (String) paramMap.get("imgUrl");//截图地址(多个","分隔)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(type) || StringUtils.isEmpty(defendantId) || StringUtils.isEmpty(reportConfigId) || StringUtils.isEmpty(title)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"类型、作品或评论回复id、关键词不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		ReportExt reportExt = new ReportExt();
		reportExt.setType(Integer.parseInt(type));
		reportExt.setDefendantId(Long.parseLong(defendantId));
		reportExt.setReportConfigId(Long.parseLong(reportConfigId));
		reportExt.setTitle(title);
		reportExt.setContent(content);
		reportExt.setImgUrl(imgUrl);
		reportExt.setPlaintiffId(Long.parseLong(studentId));
		reportExt.setState(0);//状态(0待处理、1处理中 2已处理)
		reportExt.setCreatedAt(new Date());
		reportExt.setUpdatedAt(new Date());
		reportExt.setDelFlag(0);
		reportService.insert(reportExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),reportExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	/********************************************************************************************************/



	//查询作品的评论列表
	@RequestMapping(value = "comment/list")
	public ResponseEntity<ResponseJson> list4Comment(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String productionId = (String) paramMap.get("productionId");
		String replyCount = (String) paramMap.get("replyCount");//初次查询回复的数量(默认10) - 非必传

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("page", page);

		map4Param.put("productionId", productionId);
		map4Param.put("replyCount", replyCount);

		Integer count = productionCommentService.findCountByCondition(map4Param);
		List<ProductionCommentExt> list = productionCommentService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询作品的评论的回复列表
	@RequestMapping(value = "commentReply/list")
	public ResponseEntity<ResponseJson> list4CommentReply(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String commentId = (String) paramMap.get("commentId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		if(StringUtils.isEmpty(commentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"评论id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("page", page);

		map4Param.put("commentId", commentId);

		Integer count = productionCommentReplyService.findCountByCondition(map4Param);
		List<ProductionCommentReplyExt> list = productionCommentReplyService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//作品的评论点赞
	@RequestMapping(value = "comment/like/add")
	public ResponseEntity<ResponseJson> add4CommentLike(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String commentId = (String) paramMap.get("commentId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(commentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"评论id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("commentId", commentId);

		ProductionCommentLikeExt productionCommentLikeExt = productionCommentLikeService.findObjectByCondition(map4Param);
		if(productionCommentLikeExt!=null && productionCommentLikeExt.getId()>0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"该评论已被点赞",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(productionCommentLikeExt==null){
			productionCommentLikeExt = new ProductionCommentLikeExt();
			productionCommentLikeExt.setCommentId(Long.parseLong(commentId));
			productionCommentLikeExt.setStudentId(Long.parseLong(studentId));
			productionCommentLikeExt.setState(0);
			productionCommentLikeExt.setCreatedAt(new Date());
			productionCommentLikeExt.setUpdatedAt(new Date());
			productionCommentLikeExt.setDelFlag(0);
			productionCommentLikeService.insert(productionCommentLikeExt);

			ProductionComment productionComment = productionCommentService.findById(Long.parseLong(commentId));
			productionComment.setLikeCount((productionComment.getLikeCount()==null||productionComment.getLikeCount()==0)?0+1:productionComment.getLikeCount()+1);
			productionCommentService.update(productionComment);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionCommentLikeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//作品的评论取消点赞
	@RequestMapping(value = "comment/like/delete")
	public ResponseEntity<ResponseJson> delete4CommentLike(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String commentId = (String) paramMap.get("commentId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(commentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"评论id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("commentId", commentId);

		ProductionCommentLikeExt productionCommentLikeExt = productionCommentLikeService.findObjectByCondition(map4Param);
		if(productionCommentLikeExt==null || productionCommentLikeExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"该评论未被点赞",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		productionCommentLikeExt.setDelFlag(1);
		productionCommentLikeService.delete(productionCommentLikeExt);

		ProductionComment productionComment = productionCommentService.findById(Long.parseLong(commentId));
		productionComment.setLikeCount((productionComment.getLikeCount()==null||productionComment.getLikeCount()==0)?0:productionComment.getLikeCount()-1);
		productionCommentService.update(productionComment);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionCommentLikeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//作品的评论的回复点赞
	@RequestMapping(value = "commentReply/like/add")
	public ResponseEntity<ResponseJson> add4CommentReplyLike(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String replyId = (String) paramMap.get("replyId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(replyId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"回复id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("replyId", replyId);

		ProductionCommentReplyLikeExt productionCommentReplyLikeExt = productionCommentReplyLikeService.findObjectByCondition(map4Param);
		if(productionCommentReplyLikeExt!=null && productionCommentReplyLikeExt.getId()>0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"该回复已被点赞",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(productionCommentReplyLikeExt==null){
			productionCommentReplyLikeExt = new ProductionCommentReplyLikeExt();
			productionCommentReplyLikeExt.setReplyId(Long.parseLong(replyId));
			productionCommentReplyLikeExt.setStudentId(Long.parseLong(studentId));
			productionCommentReplyLikeExt.setState(0);
			productionCommentReplyLikeExt.setCreatedAt(new Date());
			productionCommentReplyLikeExt.setUpdatedAt(new Date());
			productionCommentReplyLikeExt.setDelFlag(0);
			productionCommentReplyLikeService.insert(productionCommentReplyLikeExt);

			ProductionCommentReply productionCommentReply = productionCommentReplyService.findById(Long.parseLong(replyId));
			productionCommentReply.setLikeCount((productionCommentReply.getLikeCount()==null||productionCommentReply.getLikeCount()==0)?0+1:productionCommentReply.getLikeCount()+1);
			productionCommentReplyService.update(productionCommentReply);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionCommentReplyLikeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//作品的评论的回复取消点赞
	@RequestMapping(value = "commentReply/like/delete")
	public ResponseEntity<ResponseJson> delete4CommentReplyLike(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String replyId = (String) paramMap.get("replyId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(replyId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"回复id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("replyId", replyId);

		ProductionCommentReplyLikeExt productionCommentReplyLikeExt = productionCommentReplyLikeService.findObjectByCondition(map4Param);
		if(productionCommentReplyLikeExt==null || productionCommentReplyLikeExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"该回复未被点赞",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		productionCommentReplyLikeExt.setDelFlag(1);
		productionCommentReplyLikeService.delete(productionCommentReplyLikeExt);

		ProductionCommentReply productionCommentReply = productionCommentReplyService.findById(Long.parseLong(replyId));
		productionCommentReply.setLikeCount((productionCommentReply.getLikeCount()==null||productionCommentReply.getLikeCount()==0)?0:productionCommentReply.getLikeCount()-1);
		productionCommentReplyService.update(productionCommentReply);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionCommentReplyLikeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}




	//作品的评论删除
	@RequestMapping(value = "comment/delete")
	public ResponseEntity<ResponseJson> delete4Comment(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String commentId = (String) paramMap.get("commentId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(commentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"评论id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ProductionComment productionComment = productionCommentService.findById(Long.parseLong(commentId));

		long id = productionComment.getFromId();


		productionComment.setDelFlag(1);
		productionCommentService.update(productionComment);

		Production production = productionService.findById(productionComment.getProductionId());

		//更新评论数量
		production.setCommentCount((production.getCommentCount()==null||production.getCommentCount()==0)?0:production.getCommentCount()-1);
		productionService.update(production);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionComment);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//作品的评论的回复删除
	@RequestMapping(value = "commentReply/delete")
	public ResponseEntity<ResponseJson> delete4CommentReply(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String replyId = (String) paramMap.get("replyId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(replyId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"回复id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ProductionCommentReply productionCommentReply = productionCommentReplyService.findById(Long.parseLong(replyId));
		productionCommentReply.setDelFlag(1);
		productionCommentReplyService.update(productionCommentReply);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionCommentReply);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//作品的评论添加
	@RequestMapping(value = "comment/add")
	public ResponseEntity<ResponseJson> add4Comment(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");
		String content = (String) paramMap.get("content");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId) || StringUtils.isEmpty(content)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id、评论内容不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ProductionCommentExt productionCommentExt = new ProductionCommentExt();
		productionCommentExt.setStudentId(Long.parseLong(studentId));
		productionCommentExt.setType(0);//类型(0:默认)
		productionCommentExt.setProductionId(Long.parseLong(productionId));
		productionCommentExt.setFromId(Long.parseLong(studentId));
		productionCommentExt.setFromName(studentExt.getName());
		productionCommentExt.setFromAvatarUrl(studentExt.getAvatarUrl());
		productionCommentExt.setContent(content);
		productionCommentExt.setLikeCount(0);
		productionCommentExt.setReplyCount(0);
		productionCommentExt.setRemark("");
		productionCommentExt.setState(0);
		productionCommentExt.setCreatedAt(new Date());
		productionCommentExt.setUpdatedAt(new Date());
		productionCommentExt.setDelFlag(0);
		productionCommentService.insert(productionCommentExt);


		Production production = productionService.findById(Long.parseLong(productionId));
		Integer sourceType = production.getSourceType();//来源类型(1系统、2教师、3学生)



		if(sourceType==1){

			NotifyExt notify = new NotifyExt();
			/*notify.setOwnerType(1);//所属类型(1教师、2学生)
			notify.setPersonId(id);
			notify.setFromType(2);//来自类型(1教师、2学生)
			notify.setFromId(Long.parseLong(studentId));
			notify.setFromName(studentExt.getName());
			notify.setFromAvatarUrl(studentExt.getAvatarUrl());
			notify.setToType(1);//接收类型(1教师、2学生)
			notify.setToId(teacher==null?0:teacher.getId());
			notify.setToName(teacher==null?"":teacher.getName());
			notify.setToAvatarUrl(teacher==null?"":teacher.getAvatarUrl());
			notify.setContent(Constant.notifyType_3_1);
			notify.setProductionId(production.getId());
			notify.setProductionName(production.getName());
			notify.setType(3);//类型(1系统、2收到的点赞、3收到的评论、4发出的评论)
			notify.setRemark("");
			notify.setState(1);//状态(1未读、2已读)
			notify.setCreatedAt(new Date());
			notify.setUpdatedAt(new Date());
			notify.setDelFlag(0);
			notifyService.insert(notify);*/

			notify = new NotifyExt();
			notify.setOwnerType(2);//所属类型(1教师、2学生)
			notify.setPersonId(Long.parseLong(studentId));
			notify.setFromType(2);//来自类型(1教师、2学生)
			notify.setFromId(Long.parseLong(studentId));
			notify.setFromName(studentExt.getName());
			notify.setFromAvatarUrl(studentExt.getAvatarUrl());
			notify.setToType(0);//接收类型(1教师、2学生)
			notify.setToId(0L);
			notify.setToName(production.getAuthorName());
			notify.setToAvatarUrl(production.getAuthorAvatarUrl());
			notify.setContent(Constant.notifyType_4_1);
			notify.setProductionId(production.getId());
			notify.setProductionName(production.getName());
			notify.setType(4);//类型(1系统、2收到的点赞、3收到的评论、4发出的评论)
			notify.setRemark("");
			notify.setState(2);//状态(1未读、2已读)
			notify.setCreatedAt(new Date());
			notify.setUpdatedAt(new Date());
			notify.setDelFlag(0);
			notifyService.insert(notify);
		}

		if(sourceType==2){
			Long id = production.getPersonId();
			Teacher teacher = teacherService.findById(id);//查询教师

			NotifyExt notify = new NotifyExt();
			/*notify.setOwnerType(1);//所属类型(1教师、2学生)
			notify.setPersonId(id);
			notify.setFromType(2);//来自类型(1教师、2学生)
			notify.setFromId(Long.parseLong(studentId));
			notify.setFromName(studentExt.getName());
			notify.setFromAvatarUrl(studentExt.getAvatarUrl());
			notify.setToType(1);//接收类型(1教师、2学生)
			notify.setToId(teacher==null?0:teacher.getId());
			notify.setToName(teacher==null?"":teacher.getName());
			notify.setToAvatarUrl(teacher==null?"":teacher.getAvatarUrl());
			notify.setContent(Constant.notifyType_3_1);
			notify.setProductionId(production.getId());
			notify.setProductionName(production.getName());
			notify.setType(3);//类型(1系统、2收到的点赞、3收到的评论、4发出的评论)
			notify.setRemark("");
			notify.setState(1);//状态(1未读、2已读)
			notify.setCreatedAt(new Date());
			notify.setUpdatedAt(new Date());
			notify.setDelFlag(0);
			notifyService.insert(notify);*/

			notify = new NotifyExt();
			notify.setOwnerType(2);//所属类型(1教师、2学生)
			notify.setPersonId(Long.parseLong(studentId));
			notify.setFromType(2);//来自类型(1教师、2学生)
			notify.setFromId(Long.parseLong(studentId));
			notify.setFromName(studentExt.getName());
			notify.setFromAvatarUrl(studentExt.getAvatarUrl());
			notify.setToType(1);//接收类型(1教师、2学生)
			notify.setToId(teacher==null?0:teacher.getId());
			notify.setToName(teacher==null?"":teacher.getName());
			notify.setToAvatarUrl(teacher==null?"":teacher.getAvatarUrl());
			notify.setContent(Constant.notifyType_4_1);
			notify.setProductionId(production.getId());
			notify.setProductionName(production.getName());
			notify.setType(4);//类型(1系统、2收到的点赞、3收到的评论、4发出的评论)
			notify.setRemark("");
			notify.setState(2);//状态(1未读、2已读)
			notify.setCreatedAt(new Date());
			notify.setUpdatedAt(new Date());
			notify.setDelFlag(0);
			notifyService.insert(notify);
		}

		if(sourceType==3){
			Long id = production.getPersonId();
			Student student = studentService.findById(id);//查询学生

			NotifyExt notify = new NotifyExt();
			notify.setOwnerType(2);//所属类型(1教师、2学生)
			notify.setPersonId(id);
			notify.setFromType(2);//来自类型(1教师、2学生)
			notify.setFromId(Long.parseLong(studentId));
			notify.setFromName(studentExt.getName());
			notify.setFromAvatarUrl(studentExt.getAvatarUrl());
			notify.setToType(2);//接收类型(1教师、2学生)
			notify.setToId(student==null?0:student.getId());
			notify.setToName(student==null?"":student.getName());
			notify.setToAvatarUrl(student==null?"":student.getAvatarUrl());
			notify.setContent(Constant.notifyType_3_1);
			notify.setProductionId(production.getId());
			notify.setProductionName(production.getName());
			notify.setType(3);//类型(1系统、2收到的点赞、3收到的评论、4发出的评论)
			notify.setRemark("");
			notify.setState(1);//状态(1未读、2已读)
			notify.setCreatedAt(new Date());
			notify.setUpdatedAt(new Date());
			notify.setDelFlag(0);
			notifyService.insert(notify);

			notify = new NotifyExt();
			notify.setOwnerType(2);//所属类型(1教师、2学生)
			notify.setPersonId(Long.parseLong(studentId));
			notify.setFromType(2);//来自类型(1教师、2学生)
			notify.setFromId(Long.parseLong(studentId));
			notify.setFromName(studentExt.getName());
			notify.setFromAvatarUrl(studentExt.getAvatarUrl());
			notify.setToType(2);//接收类型(1教师、2学生)
			notify.setToId(student==null?0:student.getId());
			notify.setToName(student==null?"":student.getName());
			notify.setToAvatarUrl(student==null?"":student.getAvatarUrl());
			notify.setContent(Constant.notifyType_4_1);
			notify.setProductionId(production.getId());
			notify.setProductionName(production.getName());
			notify.setType(4);//类型(1系统、2收到的点赞、3收到的评论、4发出的评论)
			notify.setRemark("");
			notify.setState(2);//状态(1未读、2已读)
			notify.setCreatedAt(new Date());
			notify.setUpdatedAt(new Date());
			notify.setDelFlag(0);
			notifyService.insert(notify);
		}


		//更新评论数量
		production.setCommentCount((production.getCommentCount()==null||production.getCommentCount()==0)?0+1:production.getCommentCount()+1);
		productionService.update(production);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionCommentExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//作品的评论的回复添加
	@RequestMapping(value = "commentReply/add")
	public ResponseEntity<ResponseJson> add4CommentReply(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String type = (String) paramMap.get("type");//类型(0:默认,1:针对评论的回复,2:针对回复的回复)
		String commentId = (String) paramMap.get("commentId");//所属评论id
		String targetId = (String) paramMap.get("targetId");//目标id(评论id或回复id)
		String content = (String) paramMap.get("content");//内容

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(type) || StringUtils.isEmpty(commentId) || StringUtils.isEmpty(targetId) || StringUtils.isEmpty(content)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"类型、评论id、目标id、内容不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Long productionId = 0L;

		Long toId = 0L;
		String toName = "";
		String toAvatarUrl = "";

		if("1".equals(type)){//针对评论的回复
			//查询评论
			ProductionComment pc = productionCommentService.findById(Long.parseLong(targetId));
			toId = pc.getFromId();
			toName = pc.getFromName();
			toAvatarUrl = pc.getFromAvatarUrl();

			productionId = pc.getProductionId();
		}
		if("2".equals(type)){//针对回复的回复
			//查询回复
			ProductionCommentReply pcr = productionCommentReplyService.findById(Long.parseLong(targetId));
			toId = pcr.getFromId();
			toName = pcr.getFromName();
			toAvatarUrl = pcr.getFromAvatarUrl();

			productionId = pcr.getProductionId();
		}

		ProductionCommentReplyExt productionCommentReplyExt = new ProductionCommentReplyExt();
		productionCommentReplyExt.setStudentId(Long.parseLong(studentId));
		productionCommentReplyExt.setType(Integer.parseInt(type));//类型(0:默认,1:针对评论的回复,2:针对回复的回复)
		productionCommentReplyExt.setProductionId(productionId);
		productionCommentReplyExt.setCommentId(Long.parseLong(commentId));
		productionCommentReplyExt.setTargetId(Long.parseLong(targetId));
		productionCommentReplyExt.setFromId(Long.parseLong(studentId));
		productionCommentReplyExt.setFromName(studentExt.getName());
		productionCommentReplyExt.setFromAvatarUrl(studentExt.getAvatarUrl());
		productionCommentReplyExt.setToId(toId);
		productionCommentReplyExt.setToName(toName);
		productionCommentReplyExt.setToAvatarUrl(toAvatarUrl);
		productionCommentReplyExt.setContent(content);
		productionCommentReplyExt.setLikeCount(0);
		productionCommentReplyExt.setReplyCount(0);
		productionCommentReplyExt.setRemark("");
		productionCommentReplyExt.setState(0);
		productionCommentReplyExt.setCreatedAt(new Date());
		productionCommentReplyExt.setUpdatedAt(new Date());
		productionCommentReplyExt.setDelFlag(0);
		productionCommentReplyService.insert(productionCommentReplyExt);


		Production production = productionService.findById(productionId);

		NotifyExt notify = new NotifyExt();
		notify.setOwnerType(2);//所属类型(1教师、2学生)
		notify.setPersonId(toId);
		notify.setFromType(2);//来自类型(1教师、2学生)
		notify.setFromId(Long.parseLong(studentId));
		notify.setFromName(studentExt.getName());
		notify.setFromAvatarUrl(studentExt.getAvatarUrl());
		notify.setToType(2);//接收类型(1教师、2学生)
		notify.setToId(toId);
		notify.setToName(toName);
		notify.setToAvatarUrl(toAvatarUrl);
		notify.setContent(Constant.notifyType_3_2);
		notify.setProductionId(production.getId());
		notify.setProductionName(production.getName());
		notify.setType(3);//类型(1系统、2收到的点赞、3收到的评论、4发出的评论)
		notify.setRemark("");
		notify.setState(1);//状态(1未读、2已读)
		notify.setCreatedAt(new Date());
		notify.setUpdatedAt(new Date());
		notify.setDelFlag(0);
		notifyService.insert(notify);

		notify = new NotifyExt();
		notify.setOwnerType(2);//所属类型(1教师、2学生)
		notify.setPersonId(Long.parseLong(studentId));
		notify.setFromType(2);//来自类型(1教师、2学生)
		notify.setFromId(Long.parseLong(studentId));
		notify.setFromName(studentExt.getName());
		notify.setFromAvatarUrl(studentExt.getAvatarUrl());
		notify.setToType(2);//接收类型(1教师、2学生)
		notify.setToId(toId);
		notify.setToName(toName);
		notify.setToAvatarUrl(toAvatarUrl);
		notify.setContent(Constant.notifyType_4_2);
		notify.setProductionId(production.getId());
		notify.setProductionName(production.getName());
		notify.setType(4);//类型(1系统、2收到的点赞、3收到的评论、4发出的评论)
		notify.setRemark("");
		notify.setState(2);//状态(1未读、2已读)
		notify.setCreatedAt(new Date());
		notify.setUpdatedAt(new Date());
		notify.setDelFlag(0);
		notifyService.insert(notify);


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionCommentReplyExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}













}
