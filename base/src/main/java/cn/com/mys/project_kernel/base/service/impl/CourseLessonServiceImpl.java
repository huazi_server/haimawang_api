package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CourseLessonMapper;
import cn.com.mys.project_kernel.base.dao.StudentMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.*;
import cn.com.mys.project_kernel.base.entity.CourseLesson;
import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.StudentCourseHomework;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.CourseLessonService;
import cn.com.mys.project_kernel.base.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class CourseLessonServiceImpl implements CourseLessonService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CourseLessonMapper courseLessonMapper;

	@Autowired
	public CourseLessonMapperExt courseLessonMapperExt;

	@Autowired
	public StudentCourseHomeworkMapperExt studentCourseHomeworkMapperExt;

	@Autowired
	public StudentCourseHomeworkMessageMapperExt studentCourseHomeworkMessageMapperExt;

	@Autowired
	public StudentMapper studentMapper;

	@Autowired
	public StudentCourseMapperExt studentCourseMapperExt;
	@Autowired
	public CourseLessonVideoPointMapperExt courseLessonVideoPointMapperExt;
	@Autowired
	public CoursePackageLessonVideoPointMapperExt coursePackageLessonVideoPointMapperExt;


	//设置课时的留言数量
	private void setMessageCount(CourseLessonExt courseLessonExt, String teacherCourseId){
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag","0");
		m.put("courseId", courseLessonExt.getCourseId().toString());
		m.put("courseLessonId",courseLessonExt.getId().toString());
		m.put("teacherCourseId", StringUtils.isNotEmpty(teacherCourseId)?teacherCourseId:"0");
		m.put("state","1");//状态(1未解决、2已解决)
		Integer messageCount = studentCourseHomeworkMessageMapperExt.findCountByCondition(m);//学生留言数量
		courseLessonExt.setMessageCount(messageCount);
	}
	//设置课时的未批改人数
	private void setUnCheckCount(CourseLessonExt courseLessonExt, String teacherCourseId){
		Integer unCheckCount = 0;//未批改人数
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag","0");
		m.put("courseId", courseLessonExt.getCourseId().toString());
		m.put("courseLessonId",courseLessonExt.getId().toString());
		m.put("state","3");//状态(1未保存、2未提交、3已提交)
		m.put("checkFlag","1");//批改标识(1未批改、2已批改)
		m.put("teacherCourseId",teacherCourseId);
		unCheckCount = studentCourseHomeworkMapperExt.findUnCheckCountByCondition(m);
		courseLessonExt.setUnCheckCount(unCheckCount);
	}


	//设置课时的视频片段列表
	private void setVideoPointList(CourseLessonExt courseLessonExt){
		//查询课程的课时的视频片段列表
		Map<String, Object> map4Point = new HashMap<>();
		map4Point.put("delFlag", "0");
		map4Point.put("courseId", courseLessonExt.getCourseId().toString());
		map4Point.put("courseLessonId",courseLessonExt.getId().toString());
		map4Point.put("state", "1");//状态(0禁用、1启用)
		List<CourseLessonVideoPointExt> courseLessonVideoPointInfoList = courseLessonVideoPointMapperExt.findListByCondition(map4Point);
		//查询课程包的课时的视频片段列表
		map4Point = new HashMap<>();
		map4Point.put("delFlag", "0");
		map4Point.put("coursePackageId", courseLessonExt.getCoursePackageId().toString());
		map4Point.put("coursePackageLessonId",courseLessonExt.getCoursePackageLessonId().toString());
		map4Point.put("state", "1");//状态(0禁用、1启用)
		List<CoursePackageLessonVideoPointExt> coursePackageLessonVideoPointInfoList = coursePackageLessonVideoPointMapperExt.findListByCondition(map4Point);
		//设置课时的视频片段列表
		courseLessonExt.setCourseLessonVideoPointInfoList((courseLessonVideoPointInfoList!=null&&courseLessonVideoPointInfoList.size()>0)?courseLessonVideoPointInfoList:coursePackageLessonVideoPointInfoList);
	}


	//设置锁标识(1关闭、2打开)
	private void setLockFlag(CourseLessonExt courseLessonExt){
		Integer lockFlag = 1;//锁标识(1关闭、2打开)
		Date startDate = courseLessonExt.getStartAt();
		Date endDate = courseLessonExt.getEndAt();
				/*if(startDate.getTime()<=new Date().getTime() && new Date().getTime()<=endDate.getTime()){
					lockFlag = 2;
				}*/
		if(startDate.getTime()<=new Date().getTime()){
			lockFlag = 2;
		}
		courseLessonExt.setLockFlag(lockFlag);
	}


	//设置课时的进行标识
	private void setProcessFlag(CourseLessonExt courseLessonExt){
		Date startDate = courseLessonExt.getStartAt();
		Date endDate = courseLessonExt.getEndAt();
		//设置进行标识(1待开始、2进行中、3已完结)
		if(new Date().getTime()<=startDate.getTime()){
			courseLessonExt.setProcessFlag(1);
		}else if(startDate.getTime()<new Date().getTime() && new Date().getTime()<endDate.getTime()){
			courseLessonExt.setProcessFlag(2);
		}else if(endDate.getTime()<=new Date().getTime()){
			courseLessonExt.setProcessFlag(3);
		}
	}


	//设置作业的学生的名称、头像
	private void setNameAndAvatarUrl(StudentCourseHomeworkExt studentCourseHomeworkExt){
		Long studentId = studentCourseHomeworkExt.getStudentId();//学生id
		Student student = studentMapper.selectByPrimaryKey(studentId);
		studentCourseHomeworkExt.setStudentName(student.getName());
		studentCourseHomeworkExt.setStudentAvatarUrl(student.getAvatarUrl());
	}



	@Override
	public CourseLessonExt findObjectByCondition(Map<String, Object> map4Param) {
		CourseLessonExt courseLessonExt = courseLessonMapperExt.findObjectByCondition(map4Param);
		String teacherCourseId = (String) map4Param.get("teacherCourseId");
		setMessageCount(courseLessonExt,teacherCourseId);
		setUnCheckCount(courseLessonExt,teacherCourseId);
		setVideoPointList(courseLessonExt);
		setLockFlag(courseLessonExt);
		setProcessFlag(courseLessonExt);
		return courseLessonExt;
	}

	@Override
	public Integer update(CourseLessonExt courseLessonExt) {
		return courseLessonMapper.updateByPrimaryKeySelective(courseLessonExt);
	}

	@Override
	public Integer findCountByCondition4CurrentDate(Map<String, Object> map4Param) {
		return courseLessonMapperExt.findCountByCondition4CurrentDate(map4Param);
	}

	@Override
	public List<CourseLessonExt> findListByCondition4CurrentDate(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<CourseLessonExt> list = courseLessonMapperExt.findListByCondition4CurrentDate(map4Param);
		for(CourseLessonExt courseLessonExt : list){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag","0");
			mm.put("courseId", courseLessonExt.getCourseId().toString());
			mm.put("coursePackageId", courseLessonExt.getCoursePackageId());
			mm.put("studentId",studentId);
			StudentCourseExt sc = studentCourseMapperExt.findObjectByCondition(mm);//查询学生课程信息
			Integer lockFlag = 1;//锁标识(1关闭、2打开)
			Date startDate = courseLessonExt.getStartAt();
			Date endDate = courseLessonExt.getEndAt();
			/*if(startDate.getTime()<=new Date().getTime() && new Date().getTime()<=endDate.getTime()){
				lockFlag = 2;
			}*/
			if(sc!=null && sc.getState()==2 && startDate.getTime()<=new Date().getTime()){//状态(1未分配教师、2已分配教师)
				lockFlag = 2;
			}
			courseLessonExt.setLockFlag(lockFlag);

			//设置锁标识(1关闭、2打开)
			setLockFlag(courseLessonExt);
			//设置课时的进行标识
			setProcessFlag(courseLessonExt);
			//设置课时的视频片段列表
			setVideoPointList(courseLessonExt);

			//查询作业信息
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("studentId", studentId);
			m.put("courseId", courseLessonExt.getCourseId().toString());
			m.put("courseLessonId", courseLessonExt.getId().toString());
			m.put("studentCourseId", courseLessonExt.getStudentCourseId().toString());
			StudentCourseHomeworkExt h = studentCourseHomeworkMapperExt.findObjectByCondition(m);
			if(h!=null){
				setNameAndAvatarUrl(h);
				courseLessonExt.setStudentCourseHomeworkInfo(h);
			}

		}
		return list;
	}

	@Override
	public List<CourseLessonExt> findListByCondition(Map<String, Object> m) {
		List<CourseLessonExt> list = courseLessonMapperExt.findListByCondition(m);
		return list;
	}


}
