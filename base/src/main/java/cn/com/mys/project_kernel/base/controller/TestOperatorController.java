package cn.com.mys.project_kernel.base.controller;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.com.mys.project_kernel.base.entity.TbTest;
import cn.com.mys.project_kernel.base.service.TbTestService;
import cn.com.mys.project_kernel.base.util.DateUtils;
import cn.com.mys.project_kernel.base.util.ParamUtils;
import cn.com.mys.project_kernel.base.util.PropertiesUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import cn.com.mys.project_kernel.base.util.UUIDUtils;
import cn.com.mys.project_kernel.base.util.UploadFileUtils;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ParamMap;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;



/**
 * 
 * @author mingjun_T
 * 测试
 *
 */
@Controller
@RequestMapping(value = "/test")
public class TestOperatorController {

	@Autowired
	private TbTestService tbTestService;


	/****************************************************************************************************/
	/****************************************************************************************************/
	/****************************************************************************************************/
	/*************************************	返回常规数据,跳转页面	*****************************************/

	
	//通过条件查询list
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, Model model) {

		ModelAndView mav = new ModelAndView();

		//设置查询条件,查询时统一放入map中
		ParamMap<String, Object> paramMap = new ParamMap<String, Object>();
		paramMap.put("column1", "");
		paramMap.put("startIndex", 0);
		paramMap.put("numIndex", 10);

		//(1)查询所有
		//List<TbTest> tbTestList = tbTestService.findTbTestListAll();

		//(2)条件查询
		//List<TbTest> tbTestList = tbTestService.findTbTestListByCondition(paramMap);

		//(3)分页查询
		int tbTestCount = tbTestService.findTbTestListCountByCondition(paramMap);
		List<TbTest> tbTestList = tbTestService.findTbTestListPageByCondition(paramMap);

		//重新处理分页属性
		paramMap.put("startIndex", 0);
		paramMap.put("numIndex", 10);

		model.addAttribute("tbTestList", tbTestList);
		model.addAttribute("paramMap", paramMap);

		mav.setViewName("test/test-list");
		return mav;
	}

	//跳转到添加页面
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView add(HttpServletRequest request, Model model) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("test/test-add");
		return mav;
	}

	//保存
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public ModelAndView insert(HttpServletRequest request, Model model, TbTest tbTest) {
		ModelAndView mav = new ModelAndView();

		tbTest.setTestId(UUIDUtils.generateUUID());
		int result = tbTestService.insetTbTest(tbTest);

		ParamMap paramMap = new ParamMap();
		paramMap.put("result", result);

		model.addAttribute("paramMap", paramMap);
		mav.setViewName("test/test-msg");
		return mav;
	}

	//跳转到编辑页面
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(HttpServletRequest request, Model model) {
		ModelAndView mav = new ModelAndView();

		String testId = request.getParameter("testId");
		TbTest tbTest = tbTestService.findTbTestById(testId);
		model.addAttribute("tbTest", tbTest);
		mav.setViewName("test/test-edit");
		return mav;
	}

	//更改
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView update(HttpServletRequest request,  Model model, TbTest tbTest) {
		ModelAndView mav = new ModelAndView();

		int result = tbTestService.updateTbTest(tbTest);

		ParamMap paramMap = new ParamMap();
		paramMap.put("result", result);

		model.addAttribute("paramMap", paramMap);
		mav.setViewName("test/test-msg");
		return mav;
	}

	//删除
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(HttpServletRequest request, Model model) {

		ModelAndView mav = new ModelAndView();

		String testId = request.getParameter("testId");
		int result = tbTestService.deleteTbTestById(testId);

		ParamMap paramMap = new ParamMap();
		paramMap.put("result", result);

		model.addAttribute("paramMap", paramMap);
		mav.setViewName("test/test-msg");
		return mav;
	}


	/********************************************************************************************************/
	/********************************************************************************************************/
	/********************************************************************************************************/
	/*************************************	返回JSON数据,不跳转页面	*****************************************/


	@RequestMapping(value = "/edit2Json_1", method = RequestMethod.GET)
	@ResponseBody
	public TbTest edit2Json_1(HttpServletRequest request, Model model){
		String testId = request.getParameter("testId");
		TbTest tbTest = tbTestService.findTbTestById(testId);
		return tbTest;
	}


	@RequestMapping(value = "/edit2Json_2", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ResponseJson> edit2Json_2(HttpServletRequest request, Model model) {
		String testId = request.getParameter("testId");
		TbTest tbTest = tbTestService.findTbTestById(testId);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),tbTest);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//测试文件上传
	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
	@ResponseBody
	public String fileUpload(HttpServletRequest request, Model model, HttpServletResponse response){

		JSONObject jo = new JSONObject();
		//转型为MultipartHttpRequest
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> files = multipartRequest.getFiles("file");//页面中要有名称一致的属性(获取多个)
		for (MultipartFile file : files) {
			if (file != null && file.getOriginalFilename() != null && !"".equals(file.getOriginalFilename())) {
				String savePath = PropertiesUtils.getKeyValue("config.properties", "savePath_test");
				String originalFilename = file.getOriginalFilename();
				String fileType = originalFilename.substring(originalFilename.lastIndexOf(".") + 1).toLowerCase();
				//重命名上传的文件(形式:	jpg_20160101-010101_UUID.jpg)
				String renameFileName = fileType + "_" + DateUtils.getDate_Time() + "_" + UUIDUtils.generateUUID() + "." + fileType;
				String fileName = UploadFileUtils.saveFileByLocal(file, response, savePath, renameFileName);

				jo.put("fileName", "/"+renameFileName);
			}
		}
		return jo.toString();
	}

	
	
	
	
	/********************************************************************************************************/
	
	
	//分页查询(测试)
	@RequestMapping(value = "/list4Page", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public ResponseEntity<ResponseJson> list4Page(HttpServletRequest request, HttpServletResponse response, Model model) {
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);

		int pageNowInt = 1;//初始页数值
		String pageNow = (String)map.get("pageNow");
		if(!StringUtils.isNullorEmpty(pageNow)){
			pageNowInt = Integer.parseInt(pageNow);
		}
		Page page = new Page();

		map.put("startIndex", (pageNowInt-1)*page.getPageSize());
		map.put("numIndex", page.getPageSize());


		//分页查询
		int count = tbTestService.findTbTestListCountByCondition(map);
		List<TbTest> list = tbTestService.findTbTestListPageByCondition(map);

		
		//重新处理分页属性
		page.setPageNumber(pageNowInt);
		page.setTotalSize(count);
		map.put("pageTotal", page.getPageTotal());
		map.put("list", list);
		
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}





}
