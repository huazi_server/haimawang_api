package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ConfigDictExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ConfigDictService {

    List<ConfigDictExt> findListByCondition(Map<String, Object> map4Param);
}
