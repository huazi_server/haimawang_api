package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ReportConfigExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ReportConfigMapperExt {

    List<ReportConfigExt> findListByCondition(Map<String, Object> map);
}