package cn.com.mys.project_kernel.base.controller.event;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.ActivityTuan;
import cn.com.mys.project_kernel.base.entity.Course;
import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import com.alibaba.fastjson.JSON;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author mingjun_T
 * 事件
 *
 */
@Controller
@RequestMapping(value = "/event")
public class EventController {

	Log log = LogFactory.getLog(EventController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentCourseOrderService studentCourseOrderService;
	@Autowired
	private StudentCourseOrderFlowService studentCourseOrderFlowService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private StudentCourseService studentCourseService;
	@Autowired
	private CourseLessonService courseLessonService;

	@Autowired
	private ChannelService channelService;
	@Autowired
	private ChannelStatisticsService channelStatisticsService;

	@Autowired
	private ActivityTuanService activityTuanService;
	@Autowired
	private ActivityTuanOrderService activityTuanOrderService;
	@Autowired
	private StudentDiscountService studentDiscountService;



	/**
	 * 事件访问
	 * @param request
	 * @param response
	 * @return
	 *
	 * eventGroup:	事件分组:	ORDER、USER、COURSE、GZH
	 * eventType:	事件类型:	ORDER[CALLBACK(支付或退款回调)]、USER[UPDATE(更新)]、COURSE[]、GZH[SUBSCRIBE(关注)/UNSUBSCRIBE(取关)/BUTTON(按钮)/MESSAGE(消息)]
	 *
	 * ORDER[CALLBACK(支付或退款回调)]
	 * USER[UPDATE(更新)]
	 * COURSE[]
	 * GZH[SUBSCRIBE(关注)/UNSUBSCRIBE(取关)/BUTTON(按钮)/MESSAGE(消息)]
	 *
	 *
     */
	@RequestMapping(value = "call")
	public ResponseEntity<ResponseJson> call(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if (paramMap == null || paramMap.size() == 0) {
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:" + paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String eventGroup = (String) paramMap.get("eventGroup");//事件分组
		String eventType = (String) paramMap.get("eventType");//事件类型
		String eventData = (String) paramMap.get("eventData");//事件数据(json字符串结构)

		if(StringUtils.isEmpty(eventGroup)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"事件分组不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(eventType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"事件类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if (StringUtils.isEmpty(eventData)) {
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE, "100", ResponseJson.FAIL, "事件数据不能为空", ResponseJson.getExecuteTime(request), ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:" + responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//json字符串转换为map
		Map<String, Object> eventData4Map = MapUtils.parameter2Map(JSON.parseObject(eventData,Map.class));


		if("ORDER".equalsIgnoreCase(eventGroup) && "CALLBACK".equalsIgnoreCase(eventType)){
			String orderNo = (String) eventData4Map.get("orderNo");//订单编号
			String type = (String) eventData4Map.get("type");//类型(1支付、2退款)

			if(StringUtils.isEmpty(orderNo)){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"订单编号不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(StringUtils.isEmpty(type) || (!"1".equals(type) && !"2".equals(type))){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"类型不能为空，并且取值为1或2",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			StudentCourseOrderExt order = studentCourseOrderService.findByOrderNo(orderNo);
			if(order==null || order.getId()==0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"订单不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			Map<String, Object> result4Map = call4Order(eventData4Map);
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),result4Map);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if("USER".equalsIgnoreCase(eventGroup) && "UPDATE".equalsIgnoreCase(eventType)){
			String accountId = (String) eventData4Map.get("accountId");
			String openId = (String) eventData4Map.get("openId");
			if(StringUtils.isEmpty(accountId)){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"accountId不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(StringUtils.isEmpty(openId)){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"openId不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			StudentExt student = studentService.findByAccountId(accountId);
			if(student==null || student.getId()==0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			student.setOpenId(openId);
			studentService.updateById(student);
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),student);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if("GZH".equalsIgnoreCase(eventGroup) && "SUBSCRIBE".equalsIgnoreCase(eventType)){
			String accountId = (String) eventData4Map.get("accountId");
			String openId = (String) eventData4Map.get("openId");



		}
		if("GZH".equalsIgnoreCase(eventGroup) && "UNSUBSCRIBE".equalsIgnoreCase(eventType)){
			String accountId = (String) eventData4Map.get("accountId");
			String openId = (String) eventData4Map.get("openId");



		}
		if("GZH".equalsIgnoreCase(eventGroup) && "BUTTON".equalsIgnoreCase(eventType)){
			String accountId = (String) eventData4Map.get("accountId");
			String openId = (String) eventData4Map.get("openId");
			String button = (String) eventData4Map.get("button");

			Map<String, Object> result4Map = call4Gzh_BUTTON(eventData4Map);
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),result4Map);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if("GZH".equalsIgnoreCase(eventGroup) && "MESSAGE".equalsIgnoreCase(eventType)){
			String accountId = (String) eventData4Map.get("accountId");
			String openId = (String) eventData4Map.get("openId");
			String message = (String) eventData4Map.get("message");



		}



		System.out.println("---------------------");


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),eventData4Map);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

	}






	//支付或退款的回调
	public Map<String, Object> call4Order(Map<String, Object> map) {


		Map<String, Object> paramMap = map;
		log.info("--params:"+paramMap);
		//请求体内的参数
		String orderNo = (String) paramMap.get("orderNo");//订单编号
		String type = (String) paramMap.get("type");//类型(1支付、2退款)


		//查询订单信息
		JSONObject obj = new JSONObject();
		obj.put("loginProductId", Constant.loginProductId);
		obj.put("loginProductKey", Constant.loginProductKey);
		String tradeNo = orderNo;//订单编号
		obj.put("tradeNo", tradeNo);

		JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("detailUrl"), obj);
		log.info("--"+jo.toString());

		/**
		{
			"result": true,
			"duration": 391,
			"type": "Detail execute",
			"code": 200,
			"message": "Success",
			"data": {
				"payAmount": 50,
				"detail": "详情",
				"accountId": 20111345,
				"tradeNo": "114542864360275968",
				"refundTotalAmount": 0,
				"state": 0,
				"productId": 20111105,
				"updatedAt": 1557973135000,
				"id": 9,
				"payTotalAmount": 0,
				"title": "课程",
				"refundAmount": 0,
				"orderAmount": 100,
				"createdAt": 1557916908000,
				"payType": "weixin-web",
				"payExpireSecond": 60,
				"delFlag": 0,
				"payInfoListInfo": [{
					"id": 25,
					"tradeId": 9,
					"accountId": 20111345,
					"type": "weixin-web",
					"outTradeNo": "91557973134828",
					"body": "课程",
					"detail": "详情",
					"infos": null,
					"feeType": null,
					"feeAmount": 50,
					"feeSuccessAmount": 0,
					"refundTotalAmount": 0,
					"refundAmount": 0,
					"spbillCreateIp": null,
					"timeStartAt": null,
					"timeExpireSecond": null,
					"prepayId": null,
					"request": null,
					"result": null,
					"state": 0,
					"updatedAt": 1557973135000,
					"createdAt": 1557973135000,
					"delFlag": 0
		 		},{
					"id": 21,
					"tradeId": 9,
					"accountId": 20111345,
					"type": "weixin-web",
					"outTradeNo": "91557916908422",
					"body": "课程2",
					"detail": "课程2",
					"infos": null,
					"feeType": null,
					"feeAmount": 1000,
					"feeSuccessAmount": 0,
					"refundTotalAmount": 0,
					"refundAmount": 0,
					"spbillCreateIp": null,
					"timeStartAt": null,
					"timeExpireSecond": null,
					"prepayId": null,
					"request": null,
					"result": null,
					"state": 0,
					"updatedAt": 1557918176000,
					"createdAt": 1557916908000,
					"delFlag": 0
				}],
				"refundInfoListInfo": []
			},
			"error": null,
			"version": null,
			"extParam": null
		}
		*/



		//查询订单信息
		StudentCourseOrderExt order = studentCourseOrderService.findByOrderNo(orderNo);

		//类型(1支付、2退款)
		if(StringUtils.isNotEmpty(type) && "1".equals(type)){//支付

			if(order!=null && order.getState()==1){//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
				if(jo!=null && jo.getInt("code")>0 && jo.getInt("code")==200){
					JSONObject jo_data = jo.getJSONObject("data");
					int orderAmount = jo_data.getInt("orderAmount");//订单金额
					int payTotalAmount = jo_data.getInt("payTotalAmount");//支付总金额
					int refundTotalAmount = jo_data.getInt("refundTotalAmount");//退款总金额

					Double d = order.getOrderMoney().doubleValue()*100;//订单总金额,单位:分
					Double p = d-(order.getDiscountMoney()==null?0:order.getDiscountMoney().doubleValue()*100);//支付总金额,单位:分
					if(d.intValue()==orderAmount && p.intValue()==payTotalAmount){//判断订单是否支付成功

						if(order.getState()==1){
							order.setPayMoney(BigDecimal.valueOf(p/100.0));
							order.setPayAt(new Date());
							order.setState(3);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
							order.setUpdatedAt(new Date());
							order.setDelFlag(0);
							studentCourseOrderService.update(order);
						}
						//删除学生优惠券数据
						Map<String, Object> mm = new HashMap<>();
						mm.put("discountId", order.getDiscountId().toString());//优惠券id
						mm.put("studentId", order.getStudentId().toString());
						mm.put("state", "1");//状态(0未使用、1已使用)
						mm.put("delFlag", "1");
						studentDiscountService.update4Map(mm);



						if(order.getActivityTuanOrderId()==null || order.getActivityTuanOrderId()==0){
							//生成学生课程数据
							activityTuanOrderService.generateStudentCourseData(order);



							String channelId = order.getChannelId()==null?"":order.getChannelId().toString();
							String studentId = order.getStudentId().toString();
							String courseType = order.getCourseType().toString();//课程类型(1单课程、2系列课)
							Long courseGroupId = order.getCourseGroupId();
							String courseIds = order.getCourseIds();
							String cId = "";
							String cgId = "";
							if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
								cId = courseIds;
							}
							if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
								cgId = courseGroupId.toString();
							}
							//根据渠道课程的优惠券的关系生成学生优惠券信息
							if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(courseType) && StringUtils.isNotEmpty(studentId)){
								studentDiscountService.insert(channelId, courseType, studentId, cId, cgId, order.getId().toString());
							}



							//更新购课人数限制
							courseService.updateCourseEnrollCount(cId);
						}



					}

				}

			}


			/*order.setUpdatedAt(new Date());
			order.setDelFlag(0);
			studentCourseOrderService.update(order);*/

		}


		if(StringUtils.isNotEmpty(type) && "2".equals(type)){//退款
			log.info("[REFUND START]"+"--orderNo:"+orderNo);
			if(jo!=null && jo.getInt("code")>0 && jo.getInt("code")==200){
				JSONObject jo_data = jo.getJSONObject("data");
				int orderAmount = jo_data.getInt("orderAmount");//订单金额
				int payTotalAmount = jo_data.getInt("payTotalAmount");//支付总金额
				int refundTotalAmount = jo_data.getInt("refundTotalAmount");//退款总金额

				Double d = order.getOrderMoney().doubleValue()*100;//订单总金额,单位:分
				Double p = d-(order.getDiscountMoney()==null?0:order.getDiscountMoney().doubleValue()*100);//支付总金额,单位:分
				if(d.intValue()==orderAmount && p.intValue()==payTotalAmount && payTotalAmount==refundTotalAmount) {//判断订单是否退款成功
					order.setRefundMoney(new BigDecimal(refundTotalAmount/100.0));
					order.setPayAt(new Date());
					order.setState(6);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
					order.setUpdatedAt(new Date());
					order.setDelFlag(0);
					studentCourseOrderService.update(order);

					//删除学生课程数据
					Map<String, Object> m = new HashMap<>();
					m.put("sourceType", "1,3");//来源类型(正常购买1、免费领2、拼团购3、直接生成4)
					m.put("sourceId", order.getId().toString());//来源id(订单id或活动id)
					m.put("studentId", order.getStudentId().toString());
					m.put("updatedAt", new Date());
					m.put("delFlag", "1");
					studentCourseService.update4Del(m);

					//删除学生的订单关联的优惠券数据
					Map<String, Object> mm = new HashMap<>();
					mm.put("orderId", order.getId().toString());//订单id
					mm.put("studentId", order.getStudentId().toString());
					studentDiscountService.delete4Map(mm);
				}
			}
			log.info("[REFUND SUCCESS]"+"--orderNo:"+orderNo);
		}


		StudentCourseOrderFlowExt studentCourseOrderFlowExt = new StudentCourseOrderFlowExt();

		studentCourseOrderFlowExt.setOrderNo(order.getOrderNo());
		studentCourseOrderFlowExt.setRemark(paramMap.toString());

		studentCourseOrderFlowExt.setState(0);
		studentCourseOrderFlowExt.setCreatedAt(new Date());
		studentCourseOrderFlowExt.setUpdatedAt(new Date());
		studentCourseOrderFlowExt.setDelFlag(0);
		studentCourseOrderFlowService.insert(studentCourseOrderFlowExt);



		Map<String, Object> m = new HashMap<>();
		m.put("orderInfo", order);
		return m;
	}





	//公众号的回调-BUTTON
	public Map<String, Object> call4Gzh_BUTTON(Map<String, Object> map) {


		Map<String, Object> paramMap = map;
		log.info("--params:" + paramMap);
		//请求体内的参数
		String accountId = (String) paramMap.get("accountId");
		String openId = (String) paramMap.get("openId");
		String button = (String) paramMap.get("button");


		//TODO
		if(StringUtils.isNotEmpty(button) && "V1001_YOUHUIFULI".equalsIgnoreCase(button)){//活动(免费领)



		}


		return null;
	}









}
