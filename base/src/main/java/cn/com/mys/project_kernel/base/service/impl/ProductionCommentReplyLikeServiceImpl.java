package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ProductionCommentReplyLikeMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ProductionCommentReplyLikeMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentReplyLikeExt;
import cn.com.mys.project_kernel.base.service.ProductionCommentReplyLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ProductionCommentReplyLikeServiceImpl implements ProductionCommentReplyLikeService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ProductionCommentReplyLikeMapper productionCommentReplyLikeMapper;

	@Autowired
	public ProductionCommentReplyLikeMapperExt productionCommentReplyLikeMapperExt;


	@Override
	public ProductionCommentReplyLikeExt findObjectByCondition(Map<String, Object> map4Param) {
		return productionCommentReplyLikeMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer insert(ProductionCommentReplyLikeExt productionCommentReplyLikeExt) {
		return productionCommentReplyLikeMapper.insertSelective(productionCommentReplyLikeExt);
	}

	@Override
	public Integer delete(ProductionCommentReplyLikeExt productionCommentReplyLikeExt) {
		return productionCommentReplyLikeMapper.deleteByPrimaryKey(productionCommentReplyLikeExt.getId());
	}
}
