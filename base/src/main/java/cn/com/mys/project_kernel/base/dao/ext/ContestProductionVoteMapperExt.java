package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CompetitionProductionVoteExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ContestProductionVoteMapperExt {

    List<CompetitionProductionVoteExt> findListByCondition4Production(Map<String, Object> map4Param);
}