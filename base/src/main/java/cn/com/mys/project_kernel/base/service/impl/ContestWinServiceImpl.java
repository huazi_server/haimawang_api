package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestWinMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestWinMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestWinExt;
import cn.com.mys.project_kernel.base.service.ContestWinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ContestWinServiceImpl implements ContestWinService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestWinMapper contestWinMapper;

	@Autowired
	public ContestWinMapperExt contestWinMapperExt;

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return contestWinMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ContestWinExt> findListByCondition(Map<String, Object> map4Param) {
		return contestWinMapperExt.findListByCondition(map4Param);
	}

	@Override
	public Integer insert(ContestWinExt contestWinExt) {
		return contestWinMapper.insertSelective(contestWinExt);
	}
}
