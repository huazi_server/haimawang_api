package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.Discount;
import cn.com.mys.project_kernel.base.entity.ext.DiscountExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface DiscountMapperExt {

    List<DiscountExt> findByIds(String discountIds);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<DiscountExt> findListByCondition(Map<String, Object> map4Param);

    Integer update4Map(Map<String, Object> map4Param);
}