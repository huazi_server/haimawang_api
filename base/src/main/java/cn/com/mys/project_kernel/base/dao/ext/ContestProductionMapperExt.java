package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestProductionExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ContestProductionMapperExt {

    ContestProductionExt findObjectByCondition(Map<String, Object> m);

    ContestProductionExt findById(long id);

    ContestProductionExt findLastObjectByCondition(Map<String, Object> m);

    Integer findCommitCountByContestId(Long contestId);

    List<ContestProductionExt> findListByContestId(Long contestId);
}