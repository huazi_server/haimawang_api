package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestUserMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestUserMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestUserExt;
import cn.com.mys.project_kernel.base.service.ContestUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service的实现类
 *
 */

@Service
public class ContestUserServiceImpl implements ContestUserService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestUserMapper contestUserMapper;

	@Autowired
	public ContestUserMapperExt contestUserMapperExt;


	@Override
	public ContestUserExt findByAccountId(String accountId) {
		return contestUserMapperExt.findByAccountId(accountId);
	}

	@Override
	public Integer insert(ContestUserExt contestUserExt) {
		return contestUserMapper.insertSelective(contestUserExt);
	}

	@Override
	public Integer update(ContestUserExt contestUserExt) {
		return contestUserMapper.updateByPrimaryKeySelective(contestUserExt);
	}

}
