package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ChannelActivityMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ChannelActivityMapperExt;
import cn.com.mys.project_kernel.base.entity.ChannelActivity;
import cn.com.mys.project_kernel.base.entity.ext.ChannelActivityExt;
import cn.com.mys.project_kernel.base.service.ChannelActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ChannelActivityServiceImpl implements ChannelActivityService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ChannelActivityMapper channelActivityMapper;

	@Autowired
	public ChannelActivityMapperExt channelActivityMapperExt;


	@Override
	public ChannelActivityExt findObjectByCondition(Map<String, Object> m) {
		return channelActivityMapperExt.findObjectByCondition(m);
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return channelActivityMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ChannelActivityExt> findListByCondition(Map<String, Object> map4Param) {
		return channelActivityMapperExt.findListByCondition(map4Param);
	}

	@Override
	public Integer deleteByCondition(Map<String, Object> map4Param) {
		return channelActivityMapperExt.deleteByCondition(map4Param);
	}

	@Override
	public Integer insert(ChannelActivityExt channelActivityExt) {
		return channelActivityMapper.insertSelective(channelActivityExt);
	}
}
