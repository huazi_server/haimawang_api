package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ResourceFileExt;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public interface ResourceFileMapperExt {

    List<ResourceFileExt> findListByCondition(Map<String, Object> map);
}