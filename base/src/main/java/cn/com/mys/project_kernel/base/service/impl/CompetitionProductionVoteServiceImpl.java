package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CompetitionProductionVoteMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CompetitionProductionVoteMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CompetitionProductionVoteExt;
import cn.com.mys.project_kernel.base.service.CompetitionProductionVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class CompetitionProductionVoteServiceImpl implements CompetitionProductionVoteService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CompetitionProductionVoteMapper competitionProductionVoteMapper;

	@Autowired
	public CompetitionProductionVoteMapperExt competitionProductionVoteMapperExt;


	@Override
	public List<CompetitionProductionVoteExt> findListByCondition4Today(Map<String, Object> map4Param) {
		return competitionProductionVoteMapperExt.findListByCondition4Today(map4Param);
	}

	@Override
	public Integer insert(CompetitionProductionVoteExt competitionProductionVoteExt) {
		return competitionProductionVoteMapper.insertSelective(competitionProductionVoteExt);
	}
}
