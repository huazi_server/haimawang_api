package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ActivityFreeExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ActivityFreeService {


    ActivityFreeExt findObjectByCondition(Map<String, Object> map4Param);

    Integer update(ActivityFreeExt activityFreeExt);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ActivityFreeExt> findListByCondition(Map<String, Object> map4Param);

    Integer insert(ActivityFreeExt activityFreeExt);
}
