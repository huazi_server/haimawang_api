package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ContestReviewConfigExt;

/**
 * Service的接口
 *
 */
public interface ContestReviewConfigService {

    Integer insert(ContestReviewConfigExt contestReviewConfigExt);

    Integer update(ContestReviewConfigExt contestReviewConfigExt);
}
