package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.StudentCourseMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.StudentCourseMapperExt;
import cn.com.mys.project_kernel.base.entity.StudentCourse;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseExt;
import cn.com.mys.project_kernel.base.service.StudentCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class StudentCourseServiceImpl implements StudentCourseService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public StudentCourseMapper studentCourseMapper;

	@Autowired
	public StudentCourseMapperExt studentCourseMapperExt;


	@Override
	public StudentCourseExt findObjectByCondition(Map<String, Object> map4Param) {
		return studentCourseMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer update(StudentCourseExt studentCourseExt) {
		return studentCourseMapper.updateByPrimaryKeySelective(studentCourseExt);
	}

	@Override
	public List<StudentCourseExt> findListByCondition(Map<String, Object> m) {
		return studentCourseMapperExt.findListByCondition(m);
	}

	@Override
	public Integer insert(StudentCourseExt studentCourseExt) {
		return studentCourseMapper.insertSelective(studentCourseExt);
	}

	@Override
	public Integer delete(StudentCourseExt studentCourseExt) {
		return studentCourseMapper.deleteByPrimaryKey(studentCourseExt.getId());
	}

	@Override
	public Integer findStudentCountByTeacherCourseId(Map<String, Object> map4Param) {
		return studentCourseMapperExt.findStudentCountByTeacherCourseId(map4Param);
	}

	@Override
	public Integer findMaxCourseStageByStudentId(Map<String, Object> mmm) {
		return studentCourseMapperExt.findMaxCourseStageByStudentId(mmm);
	}

	@Override
	public List<StudentCourseExt> findListByCourseId(Map<String, Object> m) {
		return studentCourseMapperExt.findListByCourseId(m);
	}

	@Override
	public List<StudentCourseExt> findListByCondition4CourseGroup(Map<String, Object> m) {
		return studentCourseMapperExt.findListByCondition4CourseGroup(m);
	}

	@Override
	public Integer update4Del(Map<String, Object> m) {
		return studentCourseMapperExt.update4Del(m);
	}


}
