package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.TeacherCourseMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.TeacherCourseMapperExt;
import cn.com.mys.project_kernel.base.entity.TeacherCourse;
import cn.com.mys.project_kernel.base.entity.ext.TeacherCourseExt;
import cn.com.mys.project_kernel.base.service.TeacherCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class TeacherCourseServiceImpl implements TeacherCourseService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public TeacherCourseMapper teacherCourseMapper;

	@Autowired
	public TeacherCourseMapperExt teacherCourseMapperExt;


	@Override
	public TeacherCourse findById(Long id) {
		return teacherCourseMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer update(TeacherCourse teacherCourse) {
		return teacherCourseMapper.updateByPrimaryKeySelective(teacherCourse);
	}

	@Override
	public Integer insert(TeacherCourseExt teacherCourseExt) {
		return teacherCourseMapper.insertSelective(teacherCourseExt);
	}

	@Override
	public TeacherCourseExt findObjectByCondition(Map<String, Object> map4Param) {
		return teacherCourseMapperExt.findObjectByCondition(map4Param);
	}

}
