package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.CourseGroup;
import cn.com.mys.project_kernel.base.entity.ext.CourseGroupExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface CourseGroupService {

    CourseGroupExt findObjectByCondition(Map<String, Object> m);

    List<CourseGroupExt> findListByCondition4All(Map<String, Object> map4Param);

    CourseGroup findById(long id);

    List<CourseGroupExt> findListByCondition4Admin(Map<String, Object> map4Param);
}
