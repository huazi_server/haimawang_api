package cn.com.mys.project_kernel.base.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;



public class HtmlUtils {

	
	//动态构建jsp页面的内容
	public static void buildContent(String pageRealPath, String content) throws IOException{
		//定义头部信息,解决中文乱码
		String head = "<%@ page language='java' contentType='text/html; charset=UTF-8' pageEncoding='UTF-8'%>\n";
		
		File f = new File(pageRealPath);
		PrintWriter out = new PrintWriter(new FileWriter(f));
		//写入数据
		out.print(head+content);
		out.close();
		
	}

	
}
