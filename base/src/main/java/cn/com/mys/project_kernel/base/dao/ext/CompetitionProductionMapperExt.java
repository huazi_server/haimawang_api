package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CompetitionProductionExt;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface CompetitionProductionMapperExt {

    CompetitionProductionExt findObjectByCondition(Map<String, Object> m);

    CompetitionProductionExt findById(long id);
}