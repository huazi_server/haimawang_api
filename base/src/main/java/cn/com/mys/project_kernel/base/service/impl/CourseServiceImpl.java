package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.dao.CourseMapper;
import cn.com.mys.project_kernel.base.dao.CoursePackageMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.*;
import cn.com.mys.project_kernel.base.entity.Course;
import cn.com.mys.project_kernel.base.entity.CoursePackage;
import cn.com.mys.project_kernel.base.entity.StudentCourse;
import cn.com.mys.project_kernel.base.entity.StudentCourseHomework;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.CourseService;
import cn.com.mys.project_kernel.base.util.StringUtils;
import cn.com.mys.project_kernel.base.vo.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.print.attribute.HashAttributeSet;
import java.util.*;


/**
 * Service的实现类
 *
 */

@Service
public class CourseServiceImpl implements CourseService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CourseMapper courseMapper;

	@Autowired
	public CourseMapperExt courseMapperExt;
	@Autowired
	public StudentCourseMapperExt studentCourseMapperExt;
	@Autowired
	public CourseLessonMapperExt courseLessonMapperExt;
	@Autowired
	public StudentCourseHomeworkMapperExt studentCourseHomeworkMapperExt;
	@Autowired
	public CoursePackageMapper coursePackageMapper;
	@Autowired
	public StudentCourseHomeworkMessageMapperExt studentCourseHomeworkMessageMapperExt;
	@Autowired
	public StudentCourseOrderMapperExt studentCourseOrderMapperExt;
	@Autowired
	public StudentMapperExt studentMapperExt;
	@Autowired
	public CourseLessonVideoPointMapperExt courseLessonVideoPointMapperExt;
	@Autowired
	public CoursePackageLessonVideoPointMapperExt coursePackageLessonVideoPointMapperExt;


	//设置购买标识(1未购买、2已购买)
	private void setBuyFlag(CourseExt courseExt, String studentId){
		Map<String, Object> m = new HashMap<>();
		m.put("coursePackageId", courseExt.getCoursePackageId());
		m.put("studentId", studentId);
		m.put("delFlag","0");
		StudentCourseExt studentCourseExt = studentCourseMapperExt.findObjectByCondition(m);
		if(studentCourseExt!=null && studentCourseExt.getId()>0){
			courseExt.setBuyFlag(2);
		}else{
			courseExt.setBuyFlag(1);
		}
	}

	//设置课时列表和学生评分
	private void setLessonAndStudentScore(CourseExt courseExt, String studentId){
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag","0");
		m.put("enableFlag","1");//启用标记(1正常、2禁用)
		m.put("courseId", courseExt.getId().toString());
		List<CourseLessonExt> courseLessonInfoList = courseLessonMapperExt.findListByCondition(m);

		Map<String, Object> mm = new HashMap<>();
		mm.put("delFlag","0");
		mm.put("courseId", courseExt.getId().toString());
		mm.put("coursePackageId", courseExt.getCoursePackageId());
		mm.put("studentId",studentId);
		StudentCourseExt sc = studentCourseMapperExt.findObjectByCondition(mm);//查询学生课程信息
		if(StringUtils.isNotEmpty(studentId)){
			courseExt.setStudentScore((sc==null||sc.getScore()==null)?0:sc.getScore());
			courseExt.setStudentCourseInfo(sc);
		}


		Integer unCommitCount = 0;//未提交的课时的作业数量
		//查询课时的作业情况
		for(CourseLessonExt courseLessonExt : courseLessonInfoList){
			Integer lockFlag = 1;//锁标识(1关闭、2打开)
			Date startDate = courseLessonExt.getStartAt();
			Date endDate = courseLessonExt.getEndAt();
				/*if(startDate.getTime()<=new Date().getTime() && new Date().getTime()<=endDate.getTime()){
					lockFlag = 2;
				}*/
			if(sc!=null && sc.getState()==2 && startDate.getTime()<=new Date().getTime()){//状态(1未分配教师、2已分配教师)
				lockFlag = 2;
			}
			courseLessonExt.setLockFlag(lockFlag);


			//设置进行标识(1待开始、2进行中、3已完结)
			if(new Date().getTime()<=startDate.getTime()){
				courseLessonExt.setProcessFlag(1);
			}else if(startDate.getTime()<new Date().getTime() && new Date().getTime()<endDate.getTime()){
				courseLessonExt.setProcessFlag(2);
			}else if(endDate.getTime()<=new Date().getTime()){
				courseLessonExt.setProcessFlag(3);
			}


			m.put("courseLessonId",courseLessonExt.getId().toString());
			m.put("teacherCourseId",courseExt.getTeacherCourseId()!=null?courseExt.getTeacherCourseId().toString():"0");
			StudentCourseHomeworkExt studentCourseHomeworkInfo = null;
			if(StringUtils.isNotEmpty(studentId)){
				m.put("studentId",studentId);
				studentCourseHomeworkInfo = studentCourseHomeworkMapperExt.findObjectByCondition(m);
			}
			courseLessonExt.setStudentCourseHomeworkInfo(studentCourseHomeworkInfo);
			if(studentCourseHomeworkInfo==null || (studentCourseHomeworkInfo!=null && (studentCourseHomeworkInfo.getState()==1||studentCourseHomeworkInfo.getState()==2))){//状态(1未保存、2未提交、3已提交)
				unCommitCount++;
			}

			m.put("state","1");//状态(1未解决、2已解决)
			Integer messageCount = studentCourseHomeworkMessageMapperExt.findCountByCondition(m);//学生留言数量
			courseLessonExt.setMessageCount(messageCount);

			Integer unCheckCount = 0;//未批改人数
			m = new HashMap<>();
			m.put("delFlag","0");
			m.put("courseId", courseLessonExt.getCourseId().toString());
			m.put("courseLessonId",courseLessonExt.getId().toString());
			m.put("state","3");//状态(1未保存、2未提交、3已提交)
			m.put("checkFlag","1");//批改标识(1未批改、2已批改)
			unCheckCount = studentCourseHomeworkMapperExt.findUnCheckCountByCondition(m);
			courseLessonExt.setUnCheckCount(unCheckCount);

			//设置教师课程id
			courseLessonExt.setTeacherCourseId(courseExt.getTeacherCourseId());


			//查询课程的课时的视频片段列表
			Map<String, Object> map4Point = new HashMap<>();
			map4Point.put("delFlag", "0");
			map4Point.put("courseId", courseLessonExt.getCourseId().toString());
			map4Point.put("courseLessonId",courseLessonExt.getId().toString());
			map4Point.put("state", "1");//状态(0禁用、1启用)
			List<CourseLessonVideoPointExt> courseLessonVideoPointInfoList = courseLessonVideoPointMapperExt.findListByCondition(map4Point);
			//查询课程包的课时的视频片段列表
			map4Point = new HashMap<>();
			map4Point.put("delFlag", "0");
			map4Point.put("coursePackageId", courseLessonExt.getCoursePackageId().toString());
			map4Point.put("coursePackageLessonId",courseLessonExt.getCoursePackageLessonId().toString());
			map4Point.put("state", "1");//状态(0禁用、1启用)
			List<CoursePackageLessonVideoPointExt> coursePackageLessonVideoPointInfoList = coursePackageLessonVideoPointMapperExt.findListByCondition(map4Point);
			//设置课时的视频片段列表
			courseLessonExt.setCourseLessonVideoPointInfoList((courseLessonVideoPointInfoList!=null&&courseLessonVideoPointInfoList.size()>0)?courseLessonVideoPointInfoList:coursePackageLessonVideoPointInfoList);
		}

		courseExt.setCourseLessonCount(courseLessonInfoList.size());
		courseExt.setCourseLessonInfoList(courseLessonInfoList);

		courseExt.setUnCommitCount(unCommitCount);
	}

	//设置课程包名称、最大课程阶段
	private void setCoursePackageNameAndMaxCourseStage(CourseExt courseExt){
		Long coursePackageId = courseExt.getCoursePackageId();
		CoursePackage coursePackage = coursePackageMapper.selectByPrimaryKey(coursePackageId);
		courseExt.setCoursePackageName(coursePackage.getName());
		courseExt.setMaxCourseStage(Constant.maxCourseStage);
	}


	//设置结束标识(1未结束、2已结束)
	private void setStopFlag(CourseExt courseExt){
		if(courseExt!=null && courseExt.getId()>0 && courseExt.getTeachEndAt().getTime()<=new Date().getTime()){
			courseExt.setStopFlag(2);
		}else{
			courseExt.setStopFlag(1);
		}
	}


	//设置学生列表
	private void setStudentList(CourseExt courseExt, Long teacherCourseId){
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("teacherCourseId", teacherCourseId);
		List<StudentCourseExt> studentCourseList = studentCourseMapperExt.findListByCondition(m);
		List<StudentExt> studentInfoList = new ArrayList<>();
		for(StudentCourseExt sc : studentCourseList){
			StudentExt s = studentMapperExt.findObjectById(sc.getStudentId());

			//设置默认名称和头像
			s.setName(StringUtils.isNotEmpty(s.getName())?s.getName():Constant.studentName4Default);
			s.setAvatarUrl(StringUtils.isNotEmpty(s.getAvatarUrl())?s.getAvatarUrl():Constant.studentAvatarUrl4Default);

			studentInfoList.add(s);
		}
		courseExt.setStudentInfoList(studentInfoList);
	}




	@Override
	public List<CourseExt> findListByCondition4All(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<CourseExt> list = courseMapperExt.findListByCondition4All(map4Param);
		for(CourseExt courseExt : list){
			setBuyFlag(courseExt,studentId);
			setLessonAndStudentScore(courseExt,studentId);
			setCoursePackageNameAndMaxCourseStage(courseExt);
			setStopFlag(courseExt);
		}
		return list;
	}

	@Override
	public List<CourseExt> findListByCondition(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<CourseExt> list = courseMapperExt.findListByCondition(map4Param);
		for(CourseExt courseExt : list){
			setBuyFlag(courseExt,studentId);
			setLessonAndStudentScore(courseExt,studentId);
			setCoursePackageNameAndMaxCourseStage(courseExt);
			setStopFlag(courseExt);
		}
		return list;
	}

	@Override
	public Course findById(Long courseId) {
		return courseMapper.selectByPrimaryKey(courseId);
	}


	@Override
	public CourseExt findObjectByCourseId(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		CourseExt courseExt = courseMapperExt.findObjectByCourseId(map4Param);
		if(courseExt!=null) {
			setBuyFlag(courseExt, studentId);
			setLessonAndStudentScore(courseExt, studentId);
			setCoursePackageNameAndMaxCourseStage(courseExt);
			setStopFlag(courseExt);
		}
		return courseExt;
	}

	@Override
	public CourseExt findObjectByCondition4CoursePackageId(Map<String, Object> m) {
		String studentId = (String) m.get("studentId");
		CourseExt courseExt = courseMapperExt.findObjectByCondition4CoursePackageId(m);
		if(courseExt!=null) {
			setBuyFlag(courseExt, studentId);
			setLessonAndStudentScore(courseExt, studentId);
			setCoursePackageNameAndMaxCourseStage(courseExt);
			setStopFlag(courseExt);
		}
		return courseExt;
	}

	@Override
	public CourseExt findObjectByCoursePackageId(Map<String, Object> m) {
		String studentId = (String) m.get("studentId");
		CourseExt courseExt = courseMapperExt.findObjectByCoursePackageId(m);
		if(courseExt!=null) {
			setBuyFlag(courseExt, studentId);
			setLessonAndStudentScore(courseExt, studentId);
			setCoursePackageNameAndMaxCourseStage(courseExt);
			setStopFlag(courseExt);
		}
		return courseExt;
	}

	@Override
	public CourseExt findObjectById(Map<String, Object> mm) {
		return courseMapperExt.findObjectById(mm);
	}


	@Override
	public Integer findCountByCondition4TeacherCP(Map<String, Object> map4Param) {
		return courseMapperExt.findCountByCondition4TeacherCP(map4Param);
	}

	@Override
	public List<CourseExt> findListByCondition4TeacherCP(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<CourseExt> list = courseMapperExt.findListByCondition4TeacherCP(map4Param);
		for(CourseExt courseExt : list){
			setBuyFlag(courseExt,studentId);
			setLessonAndStudentScore(courseExt,studentId);
			setCoursePackageNameAndMaxCourseStage(courseExt);
			setStopFlag(courseExt);

			setStudentList(courseExt, courseExt.getTeacherCourseId());
		}
		return list;
	}

	@Override
	public CourseExt findObjectByCoursePackageIdAndStudentId(Map<String, Object> mm) {
		String studentId = (String) mm.get("studentId");
		CourseExt courseExt = courseMapperExt.findObjectByCoursePackageIdAndStudentId(mm);
		if(courseExt!=null) {
			setBuyFlag(courseExt, studentId);
			setLessonAndStudentScore(courseExt, studentId);
			setCoursePackageNameAndMaxCourseStage(courseExt);
			setStopFlag(courseExt);
		}
		return courseExt;
	}


	@Override
	public Integer findCountByCondition4Teacher(Map<String, Object> map4Param) {
		return courseMapperExt.findCountByCondition4Teacher(map4Param);
	}

	@Override
	public List<CourseExt> findListByCondition4Teacher(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<CourseExt> list = courseMapperExt.findListByCondition4Teacher(map4Param);
		for(CourseExt courseExt : list){
			setBuyFlag(courseExt,studentId);
			setLessonAndStudentScore(courseExt,studentId);
			setCoursePackageNameAndMaxCourseStage(courseExt);
			setStopFlag(courseExt);

			setStudentList(courseExt, courseExt.getTeacherCourseId());
		}
		return list;
	}

	@Override
	public CourseExt findObjectByCondition(Map<String, Object> map4Param) {
		String teacherCourseId = (String) map4Param.get("teacherCourseId");
		CourseExt courseExt = courseMapperExt.findObjectByCondition(map4Param);
		if(courseExt!=null){
			if(StringUtils.isNotEmpty(teacherCourseId)){
				courseExt.setTeacherCourseId(Long.parseLong(teacherCourseId));
			}
			setLessonAndStudentScore(courseExt,"");
			setCoursePackageNameAndMaxCourseStage(courseExt);
			setStopFlag(courseExt);

			setStudentList(courseExt, courseExt.getTeacherCourseId());
		}
		return courseExt;
	}




	@Override
	public Integer update(CourseExt courseExt) {
		return courseMapper.updateByPrimaryKeySelective(courseExt);
	}

	@Override
	public List<CourseExt> findAllListByCondition(Map<String, Object> map) {
		return courseMapperExt.findAllListByCondition(map);
	}



	/****************************************************/


	@Override
	public List<CourseExt> findListByCondition4Admin(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<CourseExt> list = courseMapperExt.findListByCondition4Admin(map4Param);
		for(CourseExt courseExt : list){
			setBuyFlag(courseExt,studentId);
			setLessonAndStudentScore(courseExt,studentId);
			setCoursePackageNameAndMaxCourseStage(courseExt);
			setStopFlag(courseExt);
		}
		return list;
	}


	@Override
	public Integer updateCourseEnrollCount(String courseId) {
		if(StringUtils.isNotEmpty(courseId)){
			Course course = courseMapper.selectByPrimaryKey(Long.parseLong(courseId));
			if(course!=null && course.getId()>0){
				Integer enrollCount = course.getEnrollCount();//限制人数
				if(enrollCount!=null && enrollCount>0){
					enrollCount = enrollCount - 1;
				}

				if(Constant.buyCountRefreshFlag==2){//达到购课人数限制是否重新设置(1否、2是)
					if(enrollCount==null || enrollCount<=0){
						enrollCount = Constant.buyCount;
					}
				}

				course.setEnrollCount(enrollCount);
				courseMapper.updateByPrimaryKeySelective(course);
			}
		}

		return 0;
	}


}
