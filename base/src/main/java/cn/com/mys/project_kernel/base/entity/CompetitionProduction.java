package cn.com.mys.project_kernel.base.entity;

import cn.com.mys.project_kernel.base.entity.base.BaseEntity;
import java.util.Date;

public class CompetitionProduction extends BaseEntity {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.id
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.competitionId
     *
     * @mbggenerated
     */
    private Long competitionId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.playerId
     *
     * @mbggenerated
     */
    private Long playerId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.name
     *
     * @mbggenerated
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.content
     *
     * @mbggenerated
     */
    private String content;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.coverUrl
     *
     * @mbggenerated
     */
    private String coverUrl;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.intro
     *
     * @mbggenerated
     */
    private String intro;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.operateExplain
     *
     * @mbggenerated
     */
    private String operateExplain;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.videoUrl
     *
     * @mbggenerated
     */
    private String videoUrl;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.voteCount
     *
     * @mbggenerated
     */
    private Integer voteCount;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.originProductionId
     *
     * @mbggenerated
     */
    private Long originProductionId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.originProductionLink
     *
     * @mbggenerated
     */
    private String originProductionLink;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.awardId
     *
     * @mbggenerated
     */
    private Long awardId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.remark
     *
     * @mbggenerated
     */
    private String remark;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.state
     *
     * @mbggenerated
     */
    private Integer state;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.createdAt
     *
     * @mbggenerated
     */
    private Date createdAt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.updatedAt
     *
     * @mbggenerated
     */
    private Date updatedAt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column competition_production.delFlag
     *
     * @mbggenerated
     */
    private Integer delFlag;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.id
     *
     * @return the value of competition_production.id
     *
     * @mbggenerated
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.id
     *
     * @param id the value for competition_production.id
     *
     * @mbggenerated
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.competitionId
     *
     * @return the value of competition_production.competitionId
     *
     * @mbggenerated
     */
    public Long getCompetitionId() {
        return competitionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.competitionId
     *
     * @param competitionId the value for competition_production.competitionId
     *
     * @mbggenerated
     */
    public void setCompetitionId(Long competitionId) {
        this.competitionId = competitionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.playerId
     *
     * @return the value of competition_production.playerId
     *
     * @mbggenerated
     */
    public Long getPlayerId() {
        return playerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.playerId
     *
     * @param playerId the value for competition_production.playerId
     *
     * @mbggenerated
     */
    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.name
     *
     * @return the value of competition_production.name
     *
     * @mbggenerated
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.name
     *
     * @param name the value for competition_production.name
     *
     * @mbggenerated
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.content
     *
     * @return the value of competition_production.content
     *
     * @mbggenerated
     */
    public String getContent() {
        return content;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.content
     *
     * @param content the value for competition_production.content
     *
     * @mbggenerated
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.coverUrl
     *
     * @return the value of competition_production.coverUrl
     *
     * @mbggenerated
     */
    public String getCoverUrl() {
        return coverUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.coverUrl
     *
     * @param coverUrl the value for competition_production.coverUrl
     *
     * @mbggenerated
     */
    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl == null ? null : coverUrl.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.intro
     *
     * @return the value of competition_production.intro
     *
     * @mbggenerated
     */
    public String getIntro() {
        return intro;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.intro
     *
     * @param intro the value for competition_production.intro
     *
     * @mbggenerated
     */
    public void setIntro(String intro) {
        this.intro = intro == null ? null : intro.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.operateExplain
     *
     * @return the value of competition_production.operateExplain
     *
     * @mbggenerated
     */
    public String getOperateExplain() {
        return operateExplain;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.operateExplain
     *
     * @param operateExplain the value for competition_production.operateExplain
     *
     * @mbggenerated
     */
    public void setOperateExplain(String operateExplain) {
        this.operateExplain = operateExplain == null ? null : operateExplain.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.videoUrl
     *
     * @return the value of competition_production.videoUrl
     *
     * @mbggenerated
     */
    public String getVideoUrl() {
        return videoUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.videoUrl
     *
     * @param videoUrl the value for competition_production.videoUrl
     *
     * @mbggenerated
     */
    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl == null ? null : videoUrl.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.voteCount
     *
     * @return the value of competition_production.voteCount
     *
     * @mbggenerated
     */
    public Integer getVoteCount() {
        return voteCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.voteCount
     *
     * @param voteCount the value for competition_production.voteCount
     *
     * @mbggenerated
     */
    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.originProductionId
     *
     * @return the value of competition_production.originProductionId
     *
     * @mbggenerated
     */
    public Long getOriginProductionId() {
        return originProductionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.originProductionId
     *
     * @param originProductionId the value for competition_production.originProductionId
     *
     * @mbggenerated
     */
    public void setOriginProductionId(Long originProductionId) {
        this.originProductionId = originProductionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.originProductionLink
     *
     * @return the value of competition_production.originProductionLink
     *
     * @mbggenerated
     */
    public String getOriginProductionLink() {
        return originProductionLink;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.originProductionLink
     *
     * @param originProductionLink the value for competition_production.originProductionLink
     *
     * @mbggenerated
     */
    public void setOriginProductionLink(String originProductionLink) {
        this.originProductionLink = originProductionLink == null ? null : originProductionLink.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.awardId
     *
     * @return the value of competition_production.awardId
     *
     * @mbggenerated
     */
    public Long getAwardId() {
        return awardId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.awardId
     *
     * @param awardId the value for competition_production.awardId
     *
     * @mbggenerated
     */
    public void setAwardId(Long awardId) {
        this.awardId = awardId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.remark
     *
     * @return the value of competition_production.remark
     *
     * @mbggenerated
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.remark
     *
     * @param remark the value for competition_production.remark
     *
     * @mbggenerated
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.state
     *
     * @return the value of competition_production.state
     *
     * @mbggenerated
     */
    public Integer getState() {
        return state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.state
     *
     * @param state the value for competition_production.state
     *
     * @mbggenerated
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.createdAt
     *
     * @return the value of competition_production.createdAt
     *
     * @mbggenerated
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.createdAt
     *
     * @param createdAt the value for competition_production.createdAt
     *
     * @mbggenerated
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.updatedAt
     *
     * @return the value of competition_production.updatedAt
     *
     * @mbggenerated
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.updatedAt
     *
     * @param updatedAt the value for competition_production.updatedAt
     *
     * @mbggenerated
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column competition_production.delFlag
     *
     * @return the value of competition_production.delFlag
     *
     * @mbggenerated
     */
    public Integer getDelFlag() {
        return delFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column competition_production.delFlag
     *
     * @param delFlag the value for competition_production.delFlag
     *
     * @mbggenerated
     */
    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }
}