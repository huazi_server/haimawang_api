package cn.com.mys.project_kernel.base.util;

import java.util.UUID;



/**
 * UUID工具类
 * 随机生成32位UUID--字符串
 * 
 */

public class UUIDUtils {
	
	public static String generateUUID(){
		//设置UUID用于user的保存id（去掉短横 -） 32位
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	
	public static String generateTOKEN(){
		//设置TOKEN用于user的保存token（去掉短横 -） 64位
		return UUID.randomUUID().toString().replace("-", "")+UUID.randomUUID().toString().replace("-", "");
	}
	
}
