package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ActivityTuan;

import java.util.List;

public class ActivityTuanExt extends ActivityTuan {

    List<CourseExt> courseInfoList;//单课程列表
    List<CourseGroupExt> courseGroupInfoList;//系列课列表
    public List<CourseExt> getCourseInfoList() {
        return courseInfoList;
    }
    public void setCourseInfoList(List<CourseExt> courseInfoList) {
        this.courseInfoList = courseInfoList;
    }
    public List<CourseGroupExt> getCourseGroupInfoList() {
        return courseGroupInfoList;
    }
    public void setCourseGroupInfoList(List<CourseGroupExt> courseGroupInfoList) {
        this.courseGroupInfoList = courseGroupInfoList;
    }

    Integer stopFlag;//结束标识(1未结束、2已结束)
    public Integer getStopFlag() {
        return stopFlag;
    }
    public void setStopFlag(Integer stopFlag) {
        this.stopFlag = stopFlag;
    }

    Integer joinFlag;//参加标识(1未参加、已参加)
    public Integer getJoinFlag() {
        return joinFlag;
    }
    public void setJoinFlag(Integer joinFlag) {
        this.joinFlag = joinFlag;
    }


    Integer createPinTuanOrderFlag;//能否新建拼团订单标识(1是、2否)
    public Integer getCreatePinTuanOrderFlag() {
        return createPinTuanOrderFlag;
    }
    public void setCreatePinTuanOrderFlag(Integer createPinTuanOrderFlag) {
        this.createPinTuanOrderFlag = createPinTuanOrderFlag;
    }

    //渠道活动
    ChannelActivityExt channelActivityInfo;
    public ChannelActivityExt getChannelActivityInfo() {
        return channelActivityInfo;
    }
    public void setChannelActivityInfo(ChannelActivityExt channelActivityInfo) {
        this.channelActivityInfo = channelActivityInfo;
    }

    List<ChannelActivityExt> channelActivityInfoList;//渠道活动list
    public List<ChannelActivityExt> getChannelActivityInfoList() {
        return channelActivityInfoList;
    }
    public void setChannelActivityInfoList(List<ChannelActivityExt> channelActivityInfoList) {
        this.channelActivityInfoList = channelActivityInfoList;
    }
}