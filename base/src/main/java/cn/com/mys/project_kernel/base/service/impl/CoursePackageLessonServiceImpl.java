package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CoursePackageLessonMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CoursePackageLessonMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CoursePackageLessonExt;
import cn.com.mys.project_kernel.base.service.CoursePackageLessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class CoursePackageLessonServiceImpl implements CoursePackageLessonService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CoursePackageLessonMapper coursePackageLessonMapper;

	@Autowired
	public CoursePackageLessonMapperExt coursePackageLessonMapperExt;


	@Override
	public List<CoursePackageLessonExt> findListByCondition(Map<String, Object> map4Param) {
		return coursePackageLessonMapperExt.findListByCondition(map4Param);
	}
}
