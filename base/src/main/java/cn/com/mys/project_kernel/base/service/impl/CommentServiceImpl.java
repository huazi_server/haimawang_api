package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CommentMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CommentMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CommentExt;
import cn.com.mys.project_kernel.base.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service的实现类
 *
 */

@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CommentMapper commentMapper;

	@Autowired
	public CommentMapperExt commentMapperExt;


	@Override
	public Integer insert(CommentExt commentExt) {
		return commentMapper.insertSelective(commentExt);
	}
}
