package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ProductionCommentReply;

public class ProductionCommentReplyExt extends ProductionCommentReply {

    private Integer selfFlag;//所属标识(1不属于自己、2属于自己)
    private Integer likeFlag;//点赞标识(1未点赞、2已点赞)
    public Integer getLikeFlag() {
        return likeFlag;
    }
    public void setLikeFlag(Integer likeFlag) {
        this.likeFlag = likeFlag;
    }

    public Integer getSelfFlag() {
        return selfFlag;
    }

    public void setSelfFlag(Integer selfFlag) {
        this.selfFlag = selfFlag;
    }
}