package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ChannelMapper;
import cn.com.mys.project_kernel.base.dao.ChannelStatisticsMapper;
import cn.com.mys.project_kernel.base.dao.StudentMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ChannelStatisticsMapperExt;
import cn.com.mys.project_kernel.base.entity.Channel;
import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.ext.ChannelStatisticsExt;
import cn.com.mys.project_kernel.base.service.ChannelStatisticsService;
import cn.com.mys.project_kernel.base.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * Service的实现类
 *
 */

@Service
public class ChannelStatisticsServiceImpl implements ChannelStatisticsService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ChannelStatisticsMapper channelStatisticsMapper;

	@Autowired
	public ChannelStatisticsMapperExt channelStatisticsMapperExt;


	@Autowired
	public ChannelMapper channelMapper;
	@Autowired
	public StudentMapper studentMapper;


	@Override
	public Integer insert(ChannelStatisticsExt cs) {
		return channelStatisticsMapper.insertSelective(cs);
	}

	@Override
	public Integer update(ChannelStatisticsExt cs) {
		return channelStatisticsMapper.updateByPrimaryKeySelective(cs);
	}

	@Override
	public ChannelStatisticsExt findObjectByCondition(Map<String, Object> mm) {
		return channelStatisticsMapperExt.findObjectByCondition(mm);
	}

	@Override
	public void generateChannelStatistics(String channelId, String studentId, String sourceType, String sourceId, String courseType, String courseId, String type) {
		//channelId		渠道id
		//studentId		学生id
		//sourceType	来源类型(购买单课程1、购买系列课2、活动领取失败3、活动领取成功4)
		//sourceId		来源id(订单id或活动id)
		//courseType	课程类型(1单课程、2系列课)
		//courseId		课程id或年课id
		//type			1不更新用户信息、2更新用户信息

		//生成渠道数据
		if(StringUtils.isNotEmpty(channelId)){
			Student student = studentMapper.selectByPrimaryKey(Long.parseLong(studentId));
			Channel channel = channelMapper.selectByPrimaryKey(Long.parseLong(channelId));
			if(channel!=null && channel.getId()>0){

				if(StringUtils.isNotEmpty(sourceType) && ("1".equals(sourceType) || "2".equals(sourceType))){

					Map<String, Object> mm = new HashMap<>();
					mm.put("delFlag", "0");
					//mm.put("channelId", channel.getId().toString());
					mm.put("studentId", student.getId().toString());
					mm.put("sourceType", sourceType);
					mm.put("sourceId", sourceId);
					ChannelStatisticsExt cs = findObjectByCondition(mm);
					if(cs==null || cs.getId()==0){
						cs = new ChannelStatisticsExt();
						cs.setChannelId(channel.getId());
						cs.setStudentId(Long.parseLong(studentId));
						cs.setPhone(student.getPhone());
						cs.setSourceType(Integer.parseInt(sourceType));
						cs.setSourceId(Long.parseLong(sourceId));
						cs.setCourseType(Integer.parseInt(courseType));
						cs.setCourseId(Long.parseLong(courseId));
						cs.setRemark("");
						cs.setState(0);
						cs.setCreatedAt(new Date());
						cs.setUpdatedAt(new Date());
						cs.setDelFlag(0);
						insert(cs);
					}else{
						cs.setChannelId(channel.getId());
						cs.setStudentId(student.getId());
						cs.setPhone(student.getPhone());
						cs.setSourceType(Integer.parseInt(sourceType));
						cs.setSourceId(Long.parseLong(sourceId));
						cs.setCourseType(Integer.parseInt(courseType));
						cs.setCourseId(Long.parseLong(courseId));
						cs.setRemark("");
						cs.setState(0);
						cs.setCreatedAt(new Date());
						cs.setUpdatedAt(new Date());
						cs.setDelFlag(0);
						update(cs);
					}
				}

				if(StringUtils.isNotEmpty(sourceType) && ("3".equals(sourceType) || "4".equals(sourceType))){

					ChannelStatisticsExt cs = new ChannelStatisticsExt();
					cs.setChannelId(channel.getId());
					cs.setStudentId(student.getId());
					cs.setPhone(student.getPhone());
					cs.setSourceType(Integer.parseInt(sourceType));//来源类型(购买单课程1、购买系列课2、活动领取失败3、活动领取成功4)
					cs.setSourceId(Long.parseLong(sourceId));//来源id(订单id或活动id)
					cs.setCourseType(Integer.parseInt(courseType));
					cs.setCourseId(Long.parseLong(courseId));//课程id或年课id
					cs.setRemark("");
					cs.setState(0);
					cs.setCreatedAt(new Date());
					cs.setUpdatedAt(new Date());
					cs.setDelFlag(0);
					insert(cs);
				}


				if(StringUtils.isNotEmpty(type) && "2".equals(type)){
					String channelIds = StringUtils.isEmpty(student.getChannelIds())?channelId:student.getChannelIds()+","+channelId;
					//去重
					if(StringUtils.isNotEmpty(channelIds)){
						List<String> data = new ArrayList<String>();
						String[] ids = channelIds.split(",");
						for (int i = 0; i < ids.length; i++) {
							if (!data.contains(ids[i])) {
								data.add(ids[i]);
							}
						}
						channelIds = "";
						for (String id : data) {
							channelIds += id+",";
						}
						channelIds = channelIds.length()>0?channelIds.substring(0,channelIds.length()-1):channelIds;
					}
					student.setChannelIds(channelIds);
					studentMapper.updateByPrimaryKeySelective(student);

				}

			}
		}

	}


}
