package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ChannelUserExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ChannelUserMapperExt {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ChannelUserExt> findListByCondition(Map<String, Object> map4Param);

    ChannelUserExt findObjectByCondition(Map<String, Object> map4Param);

    Integer findDayUserCountByCondition(Map<String, Object> map4Param);
}