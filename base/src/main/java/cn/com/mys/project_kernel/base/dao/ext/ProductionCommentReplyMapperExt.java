package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentReplyExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ProductionCommentReplyMapperExt {

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<ProductionCommentReplyExt> findListByCondition(Map<String, Object> map);

}