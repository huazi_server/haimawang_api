package cn.com.mys.project_kernel.base.controller.config;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.CoursePackage;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * 
 * @author mingjun_T
 * 配置
 *
 */
@Controller
@RequestMapping(value = "/config")
public class ConfigController {

	Log log = LogFactory.getLog(ConfigController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private ProductionCoverService productionCoverService;
	@Autowired
	private ProductionTagService productionTagService;
	@Autowired
	private ReportConfigService reportConfigService;
	@Autowired
	private TeacherService teacherService;

	@Autowired
	private CoursePackageImgConfigService coursePackageImgConfigService;

	@Autowired
	private CourseService courseService;
	@Autowired
	private CoursePackageService coursePackageService;
	@Autowired
	private StudentService studentService;

	@Autowired
	private ResourceFileService resourceFileService;
	@Autowired
	private ResourceConfigService resourceConfigService;
	@Autowired
	private CourseGroupService courseGroupService;

	@Autowired
	private ChannelCourseService channelCourseService;

	@Autowired
	private ConfigDictService configDictService;



	//作品封面
	@RequestMapping(value = "productionCover")
	public ResponseEntity<ResponseJson> productionCover(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		List<ProductionCoverExt> list = productionCoverService.findListByCondition(map4Param);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//作品标签
	@RequestMapping(value = "productionTag")
	public ResponseEntity<ResponseJson> productionTag(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		List<ProductionTagExt> list = productionTagService.findListByCondition(map4Param);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//举报
	@RequestMapping(value = "reportConfig")
	public ResponseEntity<ResponseJson> reportConfig(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String type = (String) paramMap.get("type");//类型(1作品、2评论、3回复)

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");

		map4Param.put("type","1");
		List<ReportConfigExt> list4P = reportConfigService.findListByCondition(map4Param);

		map4Param.put("type","2");
		List<ReportConfigExt> list4C = reportConfigService.findListByCondition(map4Param);

		map4Param.put("type","3");
		List<ReportConfigExt> list4R = reportConfigService.findListByCondition(map4Param);

		Map<String, Object> m = new HashMap<>();
		m.put("productionKeyword", list4P);
		m.put("commentKeyword", list4C);
		m.put("replyKeyword", list4R);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//首页配置
	@RequestMapping(value = "homeConfig")
	public ResponseEntity<ResponseJson> homeConfig(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理
		String channelId = (String) paramMap.get("channelId");//渠道id

		//请求体内的参数
		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		Map<String, Object> map4Param = new HashMap<String, Object>();

		map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		List<CoursePackageImgConfigExt> coursePackageImgInfoList = coursePackageImgConfigService.findListByCondition(map4Param);

		map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		map4Param.put("type","1");//类型(1名师、2普通)
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		Page page = new Page();
		page.setPageNumber(Integer.parseInt("0"));
		page.setPageSize(Integer.parseInt("10"));
		map4Param.put("page", page);
		List<TeacherExt> teacherInfoList = teacherService.findListByType(map4Param);

		map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		map4Param.put("state","3");//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
		map4Param.put("courseType", "1");//课程类型(1单课程、2系列课)
		map4Param.put("name","");
		List<CourseExt> courseInfoList = courseService.findListByCondition4All(map4Param);


		//只保留正常数据
		CopyOnWriteArrayList<CourseExt> cowList = new CopyOnWriteArrayList<CourseExt>(courseInfoList);
		for (CourseExt item : cowList) {
			if(item!=null && item.getCourseFlag()!=null && item.getCourseFlag()==2){//课程标识(1需购买课程、2非购买课程)
				cowList.remove(item);
			}
		}
		courseInfoList = cowList;

		String courseType = "1";//课程类型(1单课程、2系列课)
		if(StringUtils.isNotEmpty(channelId)){
			cowList = new CopyOnWriteArrayList<CourseExt>(courseInfoList);
			for (CourseExt item : cowList) {
				ChannelCourseExt channelCourseExt = null;
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("delFlag", "0");
				_map.put("channelId", channelId);
				_map.put("courseType", courseType);
				_map.put("courseId", item.getId().toString());
				_map.put("courseGroupId", "0");
				channelCourseExt = channelCourseService.findObjectByCondition(_map);
				item.setChannelCourseInfo(channelCourseExt);
				if(channelCourseExt==null){
					cowList.remove(item);
					continue;
				}
				if(channelCourseExt.getPrice()==null || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
					cowList.remove(item);
					continue;
				}
			}
			courseInfoList = cowList;
		}

		//按课程包分组
		Map<String, List> m = new HashMap<String, List>();
		for(CourseExt c : courseInfoList){
			if(m.get(c.getCoursePackageId().toString())==null){
				List<CourseExt> cl = new ArrayList();
				cl.add(c);
				m.put(c.getCoursePackageId().toString(), cl);
			}else{
				m.get(c.getCoursePackageId().toString()).add(c);

			}
		}

		List<CoursePackageExt> _l = new ArrayList<>();
		for (Map.Entry<String, List> entry : m.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

			Map<String, Object> mm = new HashMap<String, Object>();
			mm.put("coursePackageId", entry.getKey());
			mm.put("delFlag","0");
			CoursePackageExt coursePackageExt = coursePackageService.findObjectByCoursePackageId(mm);
			coursePackageExt.setCourseInfoList(entry.getValue());

			_l.add(coursePackageExt);
		}


		/**********************************************/


		map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		map4Param.put("state","2");//状态,1待上线、2已上线

		map4Param.put("name","");

		List<CourseGroupExt> list = courseGroupService.findListByCondition4All(map4Param);


		//只保留正常数据
		CopyOnWriteArrayList<CourseGroupExt> cowList4Group = new CopyOnWriteArrayList<CourseGroupExt>(list);
		for (CourseGroupExt item : cowList4Group) {
			if(item!=null && item.getCourseGroupFlag()!=null && item.getCourseGroupFlag()==2){//课程分组标识(1正常课程分组、2活动课程分组)
				cowList.remove(item);
			}
		}
		list = cowList4Group;

		courseType = "2";//课程类型(1单课程、2系列课)
		if(StringUtils.isNotEmpty(channelId)){
			cowList4Group = new CopyOnWriteArrayList<CourseGroupExt>(list);
			for (CourseGroupExt item : cowList4Group) {
				ChannelCourseExt channelCourseExt = null;
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("delFlag", "0");
				_map.put("channelId", channelId);
				_map.put("courseType", courseType);
				_map.put("courseId", "0");
				_map.put("courseGroupId", item.getId().toString());
				channelCourseExt = channelCourseService.findObjectByCondition(_map);
				item.setChannelCourseInfo(channelCourseExt);
				if(channelCourseExt==null){
					cowList4Group.remove(item);
					continue;
				}
				if(channelCourseExt.getPrice()==null || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
					cowList4Group.remove(item);
					continue;
				}
			}
			list = cowList4Group;
		}


		Map<String, Object> _m = new HashMap<>();
		_m.put("coursePackageImgInfoList", coursePackageImgInfoList);
		_m.put("teacherInfoList", teacherInfoList);
		//_m.put("courseInfoList", courseInfoList);
		_m.put("coursePackageInfoList", _l);
		_m.put("courseGroupInfoList", list);
		_m.put("qrCodeUrl4Official", Constant.qrCodeUrl4Official);


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	/********************************************************************************************/
	/********************************************************************************************/
	/********************************************************************************************/


	//资源
	@RequestMapping(value = "resource")
	public ResponseEntity<ResponseJson> resource(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String type = (String) paramMap.get("type");//类型(1背景、2造型、3声音、4角色)


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		map4Param.put("type",type);
		List<ResourceConfigExt> list = resourceConfigService.findListByCondition(map4Param);

		Map<String, Object> m = new LinkedHashMap<>();
		for(ResourceConfigExt resourceConfigExt : list){
			Integer _type = resourceConfigExt.getType();//类型(1背景、2造型、3声音、4角色)
			String _title = resourceConfigExt.getTitle();//backdrops、costumes、sounds、sprites
			if(StringUtils.isNotEmpty(_title)){
				m.put(_title, resourceConfigExt.getFileUrl());
			}
		}


		//处理资源文件
		//resourceFileService.insert();
		//resourceFileService.update();
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//获取字典
	@RequestMapping(value = "dict")
	public ResponseEntity<ResponseJson> dict(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String type = (String) paramMap.get("type");//类型

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		map4Param.put("state","1");//状态(0不可用、1可用)
		map4Param.put("type",StringUtils.isNotEmpty(type)?type.toUpperCase():"");
		List<ConfigDictExt> list = configDictService.findListByCondition(map4Param);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}







}
