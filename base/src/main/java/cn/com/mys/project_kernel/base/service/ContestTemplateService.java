package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ContestTemplate;
import cn.com.mys.project_kernel.base.entity.ext.ContestReviewTeacherExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestTemplateExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ContestTemplateService {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ContestReviewTeacherExt> findListByCondition(Map<String, Object> map4Param);

    ContestTemplateExt findById(long id);

    Integer insert(ContestTemplateExt contestTemplateExt);

    Integer update(ContestTemplateExt contestTemplateExt);
}
