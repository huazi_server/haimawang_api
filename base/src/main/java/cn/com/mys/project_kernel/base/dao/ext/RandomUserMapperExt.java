package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.RandomUserExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface RandomUserMapperExt {

    List<RandomUserExt> findListByCondition(Map<String, Object> map4Param);
}