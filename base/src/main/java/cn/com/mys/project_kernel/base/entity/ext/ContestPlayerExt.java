package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ContestPlayer;

public class ContestPlayerExt extends ContestPlayer {

    private ContestProductionExt contestProductionInfo;

    public ContestProductionExt getContestProductionInfo() {
        return contestProductionInfo;
    }

    public void setContestProductionInfo(ContestProductionExt contestProductionInfo) {
        this.contestProductionInfo = contestProductionInfo;
    }
}