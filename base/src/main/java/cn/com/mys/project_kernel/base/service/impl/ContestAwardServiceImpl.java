package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestAwardMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestAwardMapperExt;
import cn.com.mys.project_kernel.base.entity.ContestAward;
import cn.com.mys.project_kernel.base.entity.ext.ContestAwardExt;
import cn.com.mys.project_kernel.base.service.ContestAwardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ContestAwardServiceImpl implements ContestAwardService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestAwardMapper contestAwardMapper;

	@Autowired
	public ContestAwardMapperExt contestAwardMapperExt;

	@Override
	public ContestAward findById(long id) {
		return contestAwardMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer insert(ContestAwardExt contestAwardExt) {
		return contestAwardMapper.insertSelective(contestAwardExt);
	}

	@Override
	public Integer update(ContestAward contestAward) {
		return contestAwardMapper.updateByPrimaryKeySelective(contestAward);
	}

	@Override
	public List<ContestAwardExt> findListByContestId(Map<String, Object> mm) {
		return contestAwardMapperExt.findListByContestId(mm);
	}

	@Override
	public Integer deleteByContestId(Long contestId) {
		return contestAwardMapperExt.deleteByContestId(contestId);
	}
}
