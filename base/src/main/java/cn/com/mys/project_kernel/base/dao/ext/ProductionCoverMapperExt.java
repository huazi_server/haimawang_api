package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ProductionCoverExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ProductionCoverMapperExt {

    List<ProductionCoverExt> findListByCondition(Map<String, Object> map);
}