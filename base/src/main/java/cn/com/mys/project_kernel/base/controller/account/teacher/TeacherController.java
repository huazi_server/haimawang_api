package cn.com.mys.project_kernel.base.controller.account.teacher;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.entity.Teacher;
import cn.com.mys.project_kernel.base.entity.ext.TeacherExt;
import cn.com.mys.project_kernel.base.filter.wrapper.BodyReaderHttpServletRequestWrapper;
import cn.com.mys.project_kernel.base.service.TbTestService;
import cn.com.mys.project_kernel.base.service.TeacherService;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.util.FileUtils;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 * 
 * @author mingjun_T
 * 教师
 *
 */
@Controller
@RequestMapping(value = "/account/teacher")
public class TeacherController {

	Log log = LogFactory.getLog(TeacherController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private TeacherService teacherService;



	//获取教师信息
	@RequestMapping(value = "info")
	public ResponseEntity<ResponseJson> info(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		Map<String, Object> m = VerifyLoginToken.verifyLoginToken4Map(loginToken);

		Integer accountId = (Integer) m.get("id");
		String phone = (String) m.get("phone");

		//请求体内的参数
		String useFlag = (String) paramMap.get("useFlag");//使用传递的名称和头像(1不使用、2使用)
		String name = (String) paramMap.get("name");
		String avatarUrl = (String) paramMap.get("avatarUrl");

		if(accountId==null || accountId==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		/*TeacherExt teacherExt = teacherService.findByAccountId(accountId.toString());
		if(teacherExt==null || teacherExt.getId()==0){
			//不存在则创建
			*//*teacherExt = new TeacherExt();
			teacherExt.setAccountId(accountId.longValue());
			teacherExt.setType(2);//类型(1名师、2普通)
			teacherExt.setPhone(phone);
			teacherExt.setSourceType(1);//来源类型(1、WEB端)
			teacherExt.setEnableFlag(1);//启用标记(1正常、2禁用)
			teacherExt.setRegisterAt(new Date());
			teacherExt.setRemark("");
			teacherExt.setState(0);
			teacherExt.setCreatedAt(new Date());
			teacherExt.setUpdatedAt(new Date());
			teacherExt.setDelFlag(0);
			teacherService.insert(teacherExt);*//*
			//不存在直接返回
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"教师不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/


		TeacherExt teacherExt = teacherService.findByPhone(phone);
		if(teacherExt==null || teacherExt.getId()==0){
			//不存在则创建
			/*teacherExt = new TeacherExt();
			teacherExt.setAccountId(accountId.longValue());
			teacherExt.setType(2);//类型(1名师、2普通)
			teacherExt.setPhone(phone);
			teacherExt.setSourceType(1);//来源类型(1、WEB端)
			teacherExt.setEnableFlag(1);//启用标记(1正常、2禁用)
			teacherExt.setRegisterAt(new Date());
			teacherExt.setRemark("");
			teacherExt.setState(0);
			teacherExt.setCreatedAt(new Date());
			teacherExt.setUpdatedAt(new Date());
			teacherExt.setDelFlag(0);
			teacherService.insert(teacherExt);*/
			//不存在直接返回
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"教师不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(accountId!=null){
			teacherExt.setAccountId(accountId.longValue());
			teacherService.updateById(teacherExt);
		}

		//更新手机号
		String _phone = teacherExt.getPhone();
		if(StringUtils.isNotEmpty(phone) || (StringUtils.isNotEmpty(phone) && StringUtils.isNotEmpty(_phone) && !phone.equals(_phone))){
			teacherExt.setPhone(phone);
			teacherService.updateById(teacherExt);
		}


		if(StringUtils.isNotEmpty(useFlag) && "2".equals(useFlag)){
			teacherExt.setName(name);
			teacherExt.setAvatarUrl(avatarUrl);
			teacherService.updateById(teacherExt);
		}else{
			//do nothing
		}


		//更新名称和头像
		Object _nickname = m.get("nickname");
		Object _avatarUrl = m.get("avatarUrl");
		if(_nickname!=null && !"null".equalsIgnoreCase(_nickname.toString()) && StringUtils.isEmpty(teacherExt.getName())){
			teacherExt.setName(_nickname.toString());
		}
		if(_avatarUrl!=null && !"null".equalsIgnoreCase(_avatarUrl.toString()) && StringUtils.isEmpty(teacherExt.getAvatarUrl())){
			teacherExt.setAvatarUrl(_avatarUrl.toString());
		}
		teacherService.updateById(teacherExt);


		TeacherExt teacher = teacherService.findObjectById(teacherExt.getId());
		Integer notifyCount = 0;
		teacher.setNotifyCount(notifyCount);


		//清空其他数据的accountId
		Map<String, Object> mm = new HashMap<>();
		mm.put("delFlag","0");
		mm.put("accountId",accountId);
		mm.put("teacherId",teacherExt.getId().toString());
		teacherService.updateOtherByAccountId(mm);

		if(teacher.getState()!=null && teacher.getState()==2){
			teacher.setState(0);//正常0、禁用1、未激活2
			teacherService.updateById(teacher);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),teacher);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//更新教师信息
	@RequestMapping(value = "update")
	public ResponseEntity<ResponseJson> update(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		String name = (String) paramMap.get("name");
		String nickName = (String) paramMap.get("nickName");
		String wxNickName = (String) paramMap.get("wxNickName");
		//String phone = (String) paramMap.get("phone");
		String avatarUrl = (String) paramMap.get("avatarUrl");//头像
		String email = (String) paramMap.get("email");
		String gender = (String) paramMap.get("gender");//性别(0未知、1男、2女)

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		TeacherExt te = new TeacherExt();
		te.setId(Long.parseLong(teacherId));

		te.setName(name);
		te.setNickName(nickName);
		te.setWxNickName(wxNickName);
		//te.setPhone(phone);
		te.setAvatarUrl(avatarUrl);
		te.setEmail(email);
		te.setGender(StringUtils.isNotEmpty(gender)?Integer.parseInt(gender):null);

		te.setUpdatedAt(new Date());
		teacherService.updateById(te);

		Teacher t = teacherService.findById(Long.parseLong(teacherId));


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),t);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//上传文件
	@RequestMapping(value = "uploadFile",produces = "application/json;charset=utf-8")
	@ResponseBody
	public String uploadFile(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",402);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","loginToken不存在或过期");

			return jo.toString();
		}
		if(StringUtils.isEmpty(teacherId)){
			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",100);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","用户不存在");

			return jo.toString();
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("loginToken", loginToken);
		map4Param.put("folderPath", "/teacher/file");//教师相关的文件保存的路径

		map4Param.put("loginProductId", Constant.loginProductId);
		map4Param.put("loginProductKey", Constant.loginProductKey);

		try {
			String result = "";

			BodyReaderHttpServletRequestWrapper bodyReaderRequest = (BodyReaderHttpServletRequestWrapper) request;
			MultipartHttpServletRequest multipartRequest = bodyReaderRequest.getMultipartRequest();
			String fileParamName = "upload";
			List<MultipartFile> files = multipartRequest.getFiles(fileParamName);//获取文件

			MultipartFile file = files.get(0);

			//获取上传的文件的名称(包含后缀)
			String filename = file.getOriginalFilename();
			//获取名称(不含后缀)
			String fileName = filename.substring(0,filename.lastIndexOf("."));
			//获取后缀(扩展名)
			String contentType = filename.substring(filename.lastIndexOf(".")+1);
			map4Param.put("fileName", filename);
			map4Param.put("contentType", contentType);

			CommonsMultipartFile cf= (CommonsMultipartFile)file;
			DiskFileItem fi = (DiskFileItem)cf.getFileItem();

			//MultipartFile转换成File
			File f = fi.getStoreLocation();

			String uploadFileUrl = PropertiesUtils.getKeyValue("uploadFileUrl");

			result = FileUtils.post4Caller(uploadFileUrl,"upload",f,map4Param);

			//System.out.println(result);

			JSONObject jo = JSONObject.fromObject(result);

			//ResponseJson responseJsonResult = new ResponseJson(_result,_code,_message,_error,ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_data);
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			return jo.toString();
		} catch (IOException e) {

			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"上传失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code","100");
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","上传失败");

			return jo.toString();

		}
	}












}
