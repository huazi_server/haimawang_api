package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ContestStatExt;

/**
 * Service的接口
 *
 */
public interface ContestStatService {

    ContestStatExt findByContestId(Long contestId);

    Integer insert(ContestStatExt contestStatExt);

    Integer update(ContestStatExt contestStatExt);
}
