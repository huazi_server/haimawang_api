package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CompetitionProductionMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CompetitionPlayerMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.CompetitionProductionMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.CompetitionProductionVoteMapperExt;
import cn.com.mys.project_kernel.base.entity.CompetitionPlayer;
import cn.com.mys.project_kernel.base.entity.ext.CompetitionPlayerExt;
import cn.com.mys.project_kernel.base.entity.ext.CompetitionProductionExt;
import cn.com.mys.project_kernel.base.service.CompetitionProductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class CompetitionProductionServiceImpl implements CompetitionProductionService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CompetitionProductionMapper competitionProductionMapper;

	@Autowired
	public CompetitionProductionMapperExt competitionProductionMapperExt;

	@Autowired
	public CompetitionProductionVoteMapperExt competitionProductionVoteMapperExt;

	@Autowired
	public CompetitionPlayerMapperExt competitionPlayerMapperExt;



	@Override
	public CompetitionProductionExt findObjectByCondition(Map<String, Object> m) {
		CompetitionProductionExt competitionProductionExt = competitionProductionMapperExt.findObjectByCondition(m);
		if(competitionProductionExt!=null && competitionProductionExt.getId()>0 && competitionProductionExt.getCompetitionId()!=null && competitionProductionExt.getCompetitionId()>0){
			//获取比赛最高票
			Integer maxVoteCount = competitionProductionVoteMapperExt.findMaxVoteCountByCompetitionId(competitionProductionExt.getCompetitionId());
			competitionProductionExt.setMaxVoteCount(maxVoteCount);
		}
		return competitionProductionExt;
	}

	@Override
	public CompetitionProductionExt findById(long id) {
		CompetitionProductionExt competitionProductionExt = competitionProductionMapperExt.findById(id);
		if(competitionProductionExt!=null && competitionProductionExt.getId()>0 && competitionProductionExt.getPlayerId()!=null && competitionProductionExt.getPlayerId()>0){
			CompetitionPlayerExt competitionPlayerExt = competitionPlayerMapperExt.findById(competitionProductionExt.getPlayerId());
			competitionProductionExt.setCompetitionPlayerInfo(competitionPlayerExt);
		}
		return competitionProductionExt;
	}

	@Override
	public Integer update(CompetitionProductionExt competitionProductionExt) {
		return competitionProductionMapper.updateByPrimaryKeySelective(competitionProductionExt);
	}

	@Override
	public Integer insert(CompetitionProductionExt competitionProductionExt) {
		return competitionProductionMapper.insertSelective(competitionProductionExt);
	}
}
