package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CompetitionProductionVoteExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface CompetitionProductionVoteService {

    List<CompetitionProductionVoteExt> findListByCondition4Today(Map<String, Object> map4Param);

    Integer insert(CompetitionProductionVoteExt competitionProductionVoteExt);
}
