package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CompetitionUserExt;

/**
 * Service的接口
 *
 */
public interface CompetitionUserService {

    CompetitionUserExt findByAccountId(String accountId);
    CompetitionUserExt findByOpenId(String openId);

    Integer insert(CompetitionUserExt competitionUserExt);
    Integer update(CompetitionUserExt competitionUserExt);
}
