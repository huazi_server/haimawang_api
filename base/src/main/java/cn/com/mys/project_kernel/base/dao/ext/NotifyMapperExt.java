package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.NotifyExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface NotifyMapperExt {

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<NotifyExt> findListByCondition(Map<String, Object> map4Param);

    Integer updateByCondition(Map<String, Object> map4Param);
    Integer deleteByCondition(Map<String, Object> map4Param);
}