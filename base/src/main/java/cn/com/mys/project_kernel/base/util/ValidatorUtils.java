package cn.com.mys.project_kernel.base.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字段验证工具
 * 表单常规验证
 * @author mignjun_T
 *
 */
public class ValidatorUtils {

	//禁止实例化
	private ValidatorUtils(){}

	/**
	 * 判断是否为浮点数或者整数
	 * @param str
	 * @return true Or false
	 */
	public static boolean isNumeric(String str){
          Pattern pattern = Pattern.compile("^(-?\\d+)(\\.\\d+)?$");
          Matcher isNum = pattern.matcher(str);
          if( !isNum.matches() ){
                return false;
          }
          return true;
    }
	
	/**
	 * 判断是否为正确的邮件格式
	 * @param str
	 * @return boolean
	 */
	public static boolean isEmail(String str){
		if(isEmpty(str))
			return false;
		return str.matches("^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$");
	}
	
	/**
	 * 判断字符串是否为合法手机号 11位 13 14 15 18开头
	 * @param str
	 * @return boolean
	 */
	public static boolean isMobile(String str){
		if(isEmpty(str))
			return false;
		return str.matches("^(13|14|15|18)\\d{9}$");
	}
	
	/**
	 * 判断是否为数字
	 * @param str
	 * @return
	 */
	public static boolean isNumber(String str) {
		try{
			Integer.parseInt(str);
			return true;
		}catch(Exception ex){
			return false;
		}
	}
	
		
	/**
	 * 判断字符串是否为非空(包含null与"")
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str){
		if(str == null || "".equals(str))
			return false;
		return true;
	}
	
	/**
	 * 判断字符串是否为非空(包含null与"","    ")
	 * @param str
	 * @return
	 */
	public static boolean isNotEmptyIgnoreBlank(String str){
		if(str == null || "".equals(str) || "".equals(str.trim()))
			return false;
		return true;
	}
	
	/**
	 * 判断字符串是否为空(包含null与"")
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str){
		if(str == null || "".equals(str))
			return true;
		return false;
	}
	
	/**
	 * 判断字符串是否为空(包含null与"","    ")
	 * @param str
	 * @return
	 */
	public static boolean isEmptyIgnoreBlank(String str){
		if(str == null || "".equals(str) || "".equals(str.trim()))
			return true;
		return false;
	}
	
	
	
	 /**
	  * 
	  * @param idcard
	  * @return
	  * @author mignjun  
	  * @description 身份证号
	  */
	 public static boolean checkIdCard(String idcard){
	  Pattern p = null;
	  switch(idcard.length()){
	      case 15:
	         if((Integer.parseInt(idcard.substring(6,2+6))+1900) % 4 == 0||((Integer.parseInt(idcard.substring(6,2+6))+1900) % 100 == 0 && (Integer.parseInt(idcard.substring(6,2+6))+1900) % 4 == 0 )){
	          p=Pattern.compile("^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$");
	         }else{
	          p=Pattern.compile("^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$");
	         }
	         Matcher m=p.matcher(idcard);
	         if(m.matches()){
	          return true;
	         }else{
	          return false;
	         }
	      case 18:
	       if(Integer.parseInt(idcard.substring(6,4+6)) % 4 == 0 || (Integer.parseInt(idcard.substring(6,4+6)) % 100 == 0 && Integer.parseInt(idcard.substring(6,4+6))%4 == 0 )){
	        p= Pattern.compile("^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$");
	        }else{
	        p= Pattern.compile("^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$");
	        }
	        Matcher m1=p.matcher(idcard);
	        String[] idcard_array=idcard.split("");
	        if(m1.matches()){
	        int S = (Integer.parseInt(idcard_array[1]) + Integer.parseInt(idcard_array[11])) * 7 + (Integer.parseInt(idcard_array[2]) + Integer.parseInt(idcard_array[12])) * 9 + (Integer.parseInt(idcard_array[3]) + Integer.parseInt(idcard_array[13])) * 10 + (Integer.parseInt(idcard_array[4]) + Integer.parseInt(idcard_array[14])) * 5 + (Integer.parseInt(idcard_array[5]) + Integer.parseInt(idcard_array[15])) * 8 + (Integer.parseInt(idcard_array[6]) + Integer.parseInt(idcard_array[16])) * 4 + (Integer.parseInt(idcard_array[7]) + Integer.parseInt(idcard_array[17])) * 2 + Integer.parseInt(idcard_array[8]) * 1 + Integer.parseInt(idcard_array[8]) * 6 + Integer.parseInt(idcard_array[10]) * 3 ; 
	      int Y = S % 11; 
	      String M = "F"; 
	      String JYM = "10X98765432"; 
	      M = JYM.substring(Y,1+Y); 
	      if(M.equals(idcard_array[18])) {
	      return true;
	     }else{ 
	      return false;
	     }
	        }else{
	         return false;
	        }
	      default:break;
	            
	  }
	   
	  return false;
	 }


	//验证手机号
	public static boolean checkPhone(String phone) {
		Pattern p = Pattern.compile("^1[0-9]{10}$");
		Matcher m = p.matcher(phone);
		boolean b = m.matches();
		return b;
	}







}

