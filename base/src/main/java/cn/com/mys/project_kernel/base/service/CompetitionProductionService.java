package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CompetitionProductionExt;

import java.util.Map;

/**
 * Service的接口
 *
 */
public interface CompetitionProductionService {

    CompetitionProductionExt findObjectByCondition(Map<String, Object> m);

    CompetitionProductionExt findById(long id);

    Integer update(CompetitionProductionExt competitionProductionExt);

    Integer insert(CompetitionProductionExt competitionProductionExt);
}
