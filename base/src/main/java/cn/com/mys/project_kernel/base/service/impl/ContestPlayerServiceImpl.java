package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestPlayerMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestPlayerMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CompetitionPlayerExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestPlayerExt;
import cn.com.mys.project_kernel.base.service.ContestPlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ContestPlayerServiceImpl implements ContestPlayerService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestPlayerMapper contestPlayerMapper;

	@Autowired
	public ContestPlayerMapperExt contestPlayerMapperExt;

	@Override
	public ContestPlayerExt findObjectByCondition(Map<String, Object> map4Param) {
		return contestPlayerMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer insert(ContestPlayerExt playerExt) {
		return contestPlayerMapper.insertSelective(playerExt);
	}

	@Override
	public Integer update(ContestPlayerExt playerExt) {
		return contestPlayerMapper.updateByPrimaryKeySelective(playerExt);
	}

	@Override
	public CompetitionPlayerExt findLastObjectByCondition(Map<String, Object> m) {
		return contestPlayerMapperExt.findLastObjectByCondition(m);
	}

	@Override
	public Integer findCountByCondition4Production(Map<String, Object> map4Param) {
		return contestPlayerMapperExt.findCountByCondition4Production(map4Param);
	}

	@Override
	public List<ContestPlayerExt> findListByCondition4Production(Map<String, Object> map4Param) {
		return contestPlayerMapperExt.findListByCondition4Production(map4Param);
	}

	@Override
	public Integer findCountByCondition4Vote(Map<String, Object> map4Param) {
		return contestPlayerMapperExt.findCountByCondition4Vote(map4Param);
	}

	@Override
	public List<ContestPlayerExt> findListByCondition4Vote(Map<String, Object> map4Param) {
		return contestPlayerMapperExt.findListByCondition4Vote(map4Param);
	}

	@Override
	public Integer findCountByCondition4Score(Map<String, Object> map4Param) {
		return contestPlayerMapperExt.findCountByCondition4Score(map4Param);
	}

	@Override
	public List<ContestPlayerExt> findListByCondition4Score(Map<String, Object> map4Param) {
		return contestPlayerMapperExt.findListByCondition4Score(map4Param);
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return contestPlayerMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ContestPlayerExt> findListByCondition(Map<String, Object> map4Param) {
		return contestPlayerMapperExt.findListByCondition(map4Param);
	}
}
