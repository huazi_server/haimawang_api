package cn.com.mys.project_kernel.base.entity;

import cn.com.mys.project_kernel.base.entity.base.BaseEntity;
import java.util.Date;

public class ProductionCommentReply extends BaseEntity {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.id
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.studentId
     *
     * @mbggenerated
     */
    private Long studentId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.type
     *
     * @mbggenerated
     */
    private Integer type;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.productionId
     *
     * @mbggenerated
     */
    private Long productionId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.commentId
     *
     * @mbggenerated
     */
    private Long commentId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.targetId
     *
     * @mbggenerated
     */
    private Long targetId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.fromId
     *
     * @mbggenerated
     */
    private Long fromId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.fromName
     *
     * @mbggenerated
     */
    private String fromName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.fromAvatarUrl
     *
     * @mbggenerated
     */
    private String fromAvatarUrl;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.toId
     *
     * @mbggenerated
     */
    private Long toId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.toName
     *
     * @mbggenerated
     */
    private String toName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.toAvatarUrl
     *
     * @mbggenerated
     */
    private String toAvatarUrl;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.content
     *
     * @mbggenerated
     */
    private String content;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.likeCount
     *
     * @mbggenerated
     */
    private Integer likeCount;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.replyCount
     *
     * @mbggenerated
     */
    private Integer replyCount;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.remark
     *
     * @mbggenerated
     */
    private String remark;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.state
     *
     * @mbggenerated
     */
    private Integer state;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.createdAt
     *
     * @mbggenerated
     */
    private Date createdAt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.updatedAt
     *
     * @mbggenerated
     */
    private Date updatedAt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column production_comment_reply.delFlag
     *
     * @mbggenerated
     */
    private Integer delFlag;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.id
     *
     * @return the value of production_comment_reply.id
     *
     * @mbggenerated
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.id
     *
     * @param id the value for production_comment_reply.id
     *
     * @mbggenerated
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.studentId
     *
     * @return the value of production_comment_reply.studentId
     *
     * @mbggenerated
     */
    public Long getStudentId() {
        return studentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.studentId
     *
     * @param studentId the value for production_comment_reply.studentId
     *
     * @mbggenerated
     */
    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.type
     *
     * @return the value of production_comment_reply.type
     *
     * @mbggenerated
     */
    public Integer getType() {
        return type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.type
     *
     * @param type the value for production_comment_reply.type
     *
     * @mbggenerated
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.productionId
     *
     * @return the value of production_comment_reply.productionId
     *
     * @mbggenerated
     */
    public Long getProductionId() {
        return productionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.productionId
     *
     * @param productionId the value for production_comment_reply.productionId
     *
     * @mbggenerated
     */
    public void setProductionId(Long productionId) {
        this.productionId = productionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.commentId
     *
     * @return the value of production_comment_reply.commentId
     *
     * @mbggenerated
     */
    public Long getCommentId() {
        return commentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.commentId
     *
     * @param commentId the value for production_comment_reply.commentId
     *
     * @mbggenerated
     */
    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.targetId
     *
     * @return the value of production_comment_reply.targetId
     *
     * @mbggenerated
     */
    public Long getTargetId() {
        return targetId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.targetId
     *
     * @param targetId the value for production_comment_reply.targetId
     *
     * @mbggenerated
     */
    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.fromId
     *
     * @return the value of production_comment_reply.fromId
     *
     * @mbggenerated
     */
    public Long getFromId() {
        return fromId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.fromId
     *
     * @param fromId the value for production_comment_reply.fromId
     *
     * @mbggenerated
     */
    public void setFromId(Long fromId) {
        this.fromId = fromId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.fromName
     *
     * @return the value of production_comment_reply.fromName
     *
     * @mbggenerated
     */
    public String getFromName() {
        return fromName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.fromName
     *
     * @param fromName the value for production_comment_reply.fromName
     *
     * @mbggenerated
     */
    public void setFromName(String fromName) {
        this.fromName = fromName == null ? null : fromName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.fromAvatarUrl
     *
     * @return the value of production_comment_reply.fromAvatarUrl
     *
     * @mbggenerated
     */
    public String getFromAvatarUrl() {
        return fromAvatarUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.fromAvatarUrl
     *
     * @param fromAvatarUrl the value for production_comment_reply.fromAvatarUrl
     *
     * @mbggenerated
     */
    public void setFromAvatarUrl(String fromAvatarUrl) {
        this.fromAvatarUrl = fromAvatarUrl == null ? null : fromAvatarUrl.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.toId
     *
     * @return the value of production_comment_reply.toId
     *
     * @mbggenerated
     */
    public Long getToId() {
        return toId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.toId
     *
     * @param toId the value for production_comment_reply.toId
     *
     * @mbggenerated
     */
    public void setToId(Long toId) {
        this.toId = toId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.toName
     *
     * @return the value of production_comment_reply.toName
     *
     * @mbggenerated
     */
    public String getToName() {
        return toName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.toName
     *
     * @param toName the value for production_comment_reply.toName
     *
     * @mbggenerated
     */
    public void setToName(String toName) {
        this.toName = toName == null ? null : toName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.toAvatarUrl
     *
     * @return the value of production_comment_reply.toAvatarUrl
     *
     * @mbggenerated
     */
    public String getToAvatarUrl() {
        return toAvatarUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.toAvatarUrl
     *
     * @param toAvatarUrl the value for production_comment_reply.toAvatarUrl
     *
     * @mbggenerated
     */
    public void setToAvatarUrl(String toAvatarUrl) {
        this.toAvatarUrl = toAvatarUrl == null ? null : toAvatarUrl.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.content
     *
     * @return the value of production_comment_reply.content
     *
     * @mbggenerated
     */
    public String getContent() {
        return content;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.content
     *
     * @param content the value for production_comment_reply.content
     *
     * @mbggenerated
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.likeCount
     *
     * @return the value of production_comment_reply.likeCount
     *
     * @mbggenerated
     */
    public Integer getLikeCount() {
        return likeCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.likeCount
     *
     * @param likeCount the value for production_comment_reply.likeCount
     *
     * @mbggenerated
     */
    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.replyCount
     *
     * @return the value of production_comment_reply.replyCount
     *
     * @mbggenerated
     */
    public Integer getReplyCount() {
        return replyCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.replyCount
     *
     * @param replyCount the value for production_comment_reply.replyCount
     *
     * @mbggenerated
     */
    public void setReplyCount(Integer replyCount) {
        this.replyCount = replyCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.remark
     *
     * @return the value of production_comment_reply.remark
     *
     * @mbggenerated
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.remark
     *
     * @param remark the value for production_comment_reply.remark
     *
     * @mbggenerated
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.state
     *
     * @return the value of production_comment_reply.state
     *
     * @mbggenerated
     */
    public Integer getState() {
        return state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.state
     *
     * @param state the value for production_comment_reply.state
     *
     * @mbggenerated
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.createdAt
     *
     * @return the value of production_comment_reply.createdAt
     *
     * @mbggenerated
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.createdAt
     *
     * @param createdAt the value for production_comment_reply.createdAt
     *
     * @mbggenerated
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.updatedAt
     *
     * @return the value of production_comment_reply.updatedAt
     *
     * @mbggenerated
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.updatedAt
     *
     * @param updatedAt the value for production_comment_reply.updatedAt
     *
     * @mbggenerated
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column production_comment_reply.delFlag
     *
     * @return the value of production_comment_reply.delFlag
     *
     * @mbggenerated
     */
    public Integer getDelFlag() {
        return delFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column production_comment_reply.delFlag
     *
     * @param delFlag the value for production_comment_reply.delFlag
     *
     * @mbggenerated
     */
    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }
}