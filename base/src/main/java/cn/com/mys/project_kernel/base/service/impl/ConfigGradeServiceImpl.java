package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ConfigGradeMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ConfigGradeMapperExt;
import cn.com.mys.project_kernel.base.entity.ConfigGrade;
import cn.com.mys.project_kernel.base.entity.ext.ConfigGradeExt;
import cn.com.mys.project_kernel.base.service.ConfigGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ConfigGradeServiceImpl implements ConfigGradeService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ConfigGradeMapper configGradeMapper;

	@Autowired
	public ConfigGradeMapperExt configGradeMapperExt;


	@Override
	public ConfigGradeExt findObjectByCondition(Map<String, Object> map4Param) {
		return configGradeMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public ConfigGrade findById(long id) {
		return configGradeMapper.selectByPrimaryKey(id);
	}
}
