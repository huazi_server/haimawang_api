package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CompetitionProductionVoteExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestProductionVoteExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ContestProductionVoteService {

    List<CompetitionProductionVoteExt> findListByCondition4Production(Map<String, Object> map4Param);

    Integer insert(ContestProductionVoteExt contestProductionVoteExt);
}
