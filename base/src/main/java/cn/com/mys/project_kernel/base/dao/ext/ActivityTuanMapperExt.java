package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ActivityTuanExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ActivityTuanMapperExt {

    ActivityTuanExt findObjectByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ActivityTuanExt> findListByCondition(Map<String, Object> map4Param);
}