package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CompetitionExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface CompetitionService {

    CompetitionExt findById(long id);

    List<CompetitionExt> findAllListByCondition(Map<String, Object> map);

    Integer update(CompetitionExt competition);

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<CompetitionExt> findListByCondition(Map<String, Object> map4Param);
}
