package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CompetitionUserExt;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetitionUserMapperExt {

    CompetitionUserExt findByAccountId(String accountId);
    CompetitionUserExt findByOpenId(String openId);
}