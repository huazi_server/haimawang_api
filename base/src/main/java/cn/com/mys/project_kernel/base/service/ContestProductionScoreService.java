package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ContestProductionScoreExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ContestProductionScoreService {

    List<ContestProductionScoreExt> findListByCondition(Map<String, Object> m);
}
