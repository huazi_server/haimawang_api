package cn.com.mys.project_kernel.base.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;




/** 
 * 解析POST提交的json数据工具类 
 */
public class RequestUtils {
	
	private static Logger log = Logger.getLogger(RequestUtils.class);
	
	/** 
     * 解析request的输入流 
     * @param request 
     * @return 请求的json字符串 
     */  
    public static synchronized String getRequestBody(HttpServletRequest request) {  
        String str = null;  
        try {  
            str = IOUtils.toString(request.getInputStream(),"UTF-8");
        } catch (IOException e) {  
            log.error(e.getMessage());
            e.printStackTrace();  
        }  
        return str;  
    }  
      
      
  
    /** 
     * 解析request的json数据 
     * @param request 
     * @return Map 
     */  
    public static synchronized Map<String, Object> parseRequest(  
            HttpServletRequest request) {  
        Map<String, Object> map = new HashMap<String, Object>() ;  
        try {  
            String req = getRequestBody(request);  
            if(StringUtils.isNotEmpty(req)){
                JSONObject jsonObject = JSONObject.fromObject(req);  
                for (Iterator iter = jsonObject.keys(); iter.hasNext();) {  
                    String key = (String) iter.next();  
                    map.put(key, jsonObject.get(key).toString());
                }  
            }  
            log.debug(map);
  
        } catch (Exception e) {  
        	log.error(e.getMessage());  
            e.printStackTrace();  
            
        }  
        return map;  
    }  
      
      
      
  
    /** 
     * 获取map的value 
     * @param map 
     * @param key 
     * @return  
     */  
    public static Object getRequestBodyValue(  
            Map<String, Object> map, String key) {  
        if (map != null && StringUtils.isNotEmpty(key) && map.containsKey(key)) {
            return map.get(key);  
        }  
        return null;  
    }


    //获取日志的前缀信息
    public static String getLog4Prefix(HttpServletRequest request){
        String remoteAddr = request.getRemoteAddr();
        String path = request.getServletPath();
        String prefix = "[ "+remoteAddr + " # " + path + " ]"+" - ";
        //System.out.println(prefix);
        return prefix;
    }



}
