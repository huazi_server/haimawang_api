package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CompetitionPlayerExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestPlayerExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ContestPlayerService {

    ContestPlayerExt findObjectByCondition(Map<String, Object> map4Param);

    Integer insert(ContestPlayerExt playerExt);

    Integer update(ContestPlayerExt playerExt);

    CompetitionPlayerExt findLastObjectByCondition(Map<String, Object> m);

    Integer findCountByCondition4Production(Map<String, Object> map4Param);

    List<ContestPlayerExt> findListByCondition4Production(Map<String, Object> map4Param);

    Integer findCountByCondition4Vote(Map<String, Object> map4Param);

    List<ContestPlayerExt> findListByCondition4Vote(Map<String, Object> map4Param);

    Integer findCountByCondition4Score(Map<String, Object> map4Param);

    List<ContestPlayerExt> findListByCondition4Score(Map<String, Object> map4Param);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ContestPlayerExt> findListByCondition(Map<String, Object> map4Param);
}
