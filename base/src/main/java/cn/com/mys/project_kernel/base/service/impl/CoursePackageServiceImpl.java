package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CoursePackageMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CoursePackageMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CoursePackageExt;
import cn.com.mys.project_kernel.base.service.CoursePackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class CoursePackageServiceImpl implements CoursePackageService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CoursePackageMapper coursePackageMapper;

	@Autowired
	public CoursePackageMapperExt coursePackageMapperExt;


	@Override
	public CoursePackageExt findObjectByCoursePackageId(Map<String, Object> m) {
		return coursePackageMapperExt.findObjectByCoursePackageId(m);
	}

	@Override
	public List<CoursePackageExt> findListByCondition(Map<String, Object> map4Param) {
		return coursePackageMapperExt.findListByCondition(map4Param);
	}

	@Override
	public CoursePackageExt findObjectByCondition(Map<String, Object> mm) {
		return coursePackageMapperExt.findObjectByCondition(mm);
	}
}
