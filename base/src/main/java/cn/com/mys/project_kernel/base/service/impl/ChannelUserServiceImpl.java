package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ChannelUserMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ChannelUserMapperExt;
import cn.com.mys.project_kernel.base.entity.ChannelUser;
import cn.com.mys.project_kernel.base.entity.ext.ChannelUserExt;
import cn.com.mys.project_kernel.base.service.ChannelUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ChannelUserServiceImpl implements ChannelUserService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ChannelUserMapper channelUserMapper;

	@Autowired
	public ChannelUserMapperExt channelUserMapperExt;


	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return channelUserMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ChannelUserExt> findListByCondition(Map<String, Object> map4Param) {
		return channelUserMapperExt.findListByCondition(map4Param);
	}

	@Override
	public Integer insert(ChannelUser channelUser) {
		return channelUserMapper.insertSelective(channelUser);
	}

	@Override
	public Integer update(ChannelUser channelUser) {
		return channelUserMapper.updateByPrimaryKeySelective(channelUser);
	}

	@Override
	public ChannelUserExt findObjectByCondition(Map<String, Object> map4Param) {
		return channelUserMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer findDayUserCountByCondition(Map<String, Object> map4Param) {
		return channelUserMapperExt.findDayUserCountByCondition(map4Param);
	}
}
