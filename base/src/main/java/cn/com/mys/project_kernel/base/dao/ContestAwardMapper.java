package cn.com.mys.project_kernel.base.dao;

import cn.com.mys.project_kernel.base.entity.ContestAward;
import org.springframework.stereotype.Repository;

@Repository
public interface ContestAwardMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table contest_award
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table contest_award
     *
     * @mbggenerated
     */
    int insert(ContestAward record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table contest_award
     *
     * @mbggenerated
     */
    int insertSelective(ContestAward record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table contest_award
     *
     * @mbggenerated
     */
    ContestAward selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table contest_award
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(ContestAward record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table contest_award
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(ContestAward record);
}