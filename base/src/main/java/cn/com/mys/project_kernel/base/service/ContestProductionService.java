package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ContestProductionExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ContestProductionService {

    ContestProductionExt findObjectByCondition(Map<String, Object> m);

    ContestProductionExt findById(long id);

    Integer update(ContestProductionExt contestProductionExt);

    Integer insert(ContestProductionExt contestProductionExt);

    ContestProductionExt findLastObjectByCondition(Map<String, Object> m);

    List<ContestProductionExt> findListByContestId(Long contestId);
}
