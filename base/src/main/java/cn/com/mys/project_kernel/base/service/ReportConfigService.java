package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ReportConfigExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ReportConfigService {


    List<ReportConfigExt> findListByCondition(Map<String, Object> map);
}
