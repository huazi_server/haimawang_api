package cn.com.mys.project_kernel.base.controller.account.student;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.*;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.filter.wrapper.ParameterRequestWrapper;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.TypedStringValue;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 *
 * @author mingjun_T
 * 学生课程订单
 *
 */
@Controller
@RequestMapping(value = "/account/student/course/order")
public class StudentCourseOrderController {

	Log log = LogFactory.getLog(StudentCourseOrderController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentCourseOrderService studentCourseOrderService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private CourseLessonService courseLessonService;
	@Autowired
	private StudentCourseService studentCourseService;
	@Autowired
	private TeacherService teacherService;
	@Autowired
	private CourseGroupService courseGroupService;

	@Autowired
	private ChannelService channelService;
	@Autowired
	private ChannelStatisticsService channelStatisticsService;
	@Autowired
	private ChannelCourseService channelCourseService;


	@Autowired
	private ActivityTuanService activityTuanService;
	@Autowired
	private ActivityTuanOrderService activityTuanOrderService;

	@Autowired
	private StudentDiscountService studentDiscountService;


	//查询订单列表
	@RequestMapping(value = "list")
	public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("page", page);

		map4Param.put("state", "");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)

		map4Param.put("caseValue", "1");//所有订单1、拼团的个人订单2

		Integer count = studentCourseOrderService.findCountByCondition(map4Param);
		List<StudentCourseOrderExt> list = studentCourseOrderService.findListByCondition(map4Param);

		/*//不返回系列课的待付款订单
		CopyOnWriteArrayList<StudentCourseOrderExt> cowList = new CopyOnWriteArrayList<StudentCourseOrderExt>(list);
		for (StudentCourseOrderExt item : cowList) {
			Integer courseType = item.getCourseType();//课程类型(1单课程、2系列课)
			Integer state = item.getState();//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			if (courseType!=null && courseType==2 && state!=null && state==1) {
				cowList.remove(item);
				count--;
			}
		}
		list = cowList;*/


		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//申请退款
	@RequestMapping(value = "applyRefund")
	public ResponseEntity<ResponseJson> applyRefund(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String studentCourseOrderId = (String) paramMap.get("studentCourseOrderId");//学生课程订单id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(studentCourseOrderId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生课程订单id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		StudentCourseOrder studentCourseOrder = studentCourseOrderService.findById(Long.parseLong(studentCourseOrderId));

		if(studentCourseOrder!=null && studentCourseOrder.getCourseType()!=null && studentCourseOrder.getCourseType()==1){
			Course course = courseService.findById(Long.parseLong(studentCourseOrder.getCourseIds()));
			if(studentCourseOrder.getState()!=3 || course.getTeachStartAt().getTime() <= new Date().getTime()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"订单已支付并且未开课时才能申请退款",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(studentCourseOrder!=null && studentCourseOrder.getCourseType()!=null && studentCourseOrder.getCourseType()==2){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseGroupId", studentCourseOrder.getCourseGroupId());
			CourseGroupExt courseGroupExt = courseGroupService.findObjectByCondition(mm);
			List<CourseExt> courseExtList = courseGroupExt.getCourseInfoList();
			for(CourseExt course : courseExtList){
				Integer stage = course.getStage();//阶段
				if(stage!=null && stage==1){
					if(studentCourseOrder.getState()!=3 || course.getTeachStartAt().getTime() <= new Date().getTime()){
						ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"订单已支付并且第一阶段课程未开课时才能申请退款",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
						log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
						return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
					}
				}
			}
		}

		if(studentCourseOrder!=null && studentCourseOrder.getActivityTuanOrderId()!=null && studentCourseOrder.getActivityTuanOrderId()>0){
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("activityTuanOrderId", studentCourseOrder.getActivityTuanOrderId().toString());
			ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderService.findObjectByCondition(m);
			if(activityTuanOrderExt!=null && activityTuanOrderExt.getId()>0 && activityTuanOrderExt.getState()!=2){//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"拼团成功后才能申请退款",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}



		studentCourseOrder.setState(4);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		studentCourseOrder.setUpdatedAt(new Date());
		studentCourseOrderService.update(studentCourseOrder);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseOrder);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询订单详情
	@RequestMapping(value = "detail")
	public ResponseEntity<ResponseJson> detail(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String orderNo = (String) paramMap.get("orderNo");//订单编号

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(orderNo)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"订单编号不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//查询订单信息
		JSONObject obj = new JSONObject();
		obj.put("loginToken", loginToken);
		obj.put("tradeNo", orderNo);
		obj.put("loginProductId", Constant.loginProductId);
		obj.put("loginProductKey", Constant.loginProductKey);

		JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("detailUrl"), obj);
		log.info("--"+jo.toString());

		/**
		{
			"result": true,
			"duration": 391,
			"type": "Detail execute",
			"code": 200,
			"message": "Success",
			"data": {
				"payAmount": 50,
				"detail": "详情",
				"accountId": 20111345,
				"tradeNo": "114542864360275968",
				"refundTotalAmount": 0,
				"state": 0,
				"productId": 20111105,
				"updatedAt": 1557973135000,
				"id": 9,
				"payTotalAmount": 0,
				"title": "课程",
				"refundAmount": 0,
				"orderAmount": 100,
				"createdAt": 1557916908000,
				"payType": "weixin-web",
				"payExpireSecond": 60,
				"delFlag": 0,
				"payInfoListInfo": [{
					"id": 25,
					"tradeId": 9,
					"accountId": 20111345,
					"type": "weixin-web",
					"outTradeNo": "91557973134828",
					"body": "课程",
					"detail": "详情",
					"infos": null,
					"feeType": null,
					"feeAmount": 50,
					"feeSuccessAmount": 0,
					"refundTotalAmount": 0,
					"refundAmount": 0,
					"spbillCreateIp": null,
					"timeStartAt": null,
					"timeExpireSecond": null,
					"prepayId": null,
					"request": null,
					"result": null,
					"state": 0,
					"updatedAt": 1557973135000,
					"createdAt": 1557973135000,
					"delFlag": 0
		 		},{
					"id": 21,
					"tradeId": 9,
					"accountId": 20111345,
					"type": "weixin-web",
					"outTradeNo": "91557916908422",
					"body": "课程2",
					"detail": "课程2",
					"infos": null,
					"feeType": null,
					"feeAmount": 1000,
					"feeSuccessAmount": 0,
					"refundTotalAmount": 0,
					"refundAmount": 0,
					"spbillCreateIp": null,
					"timeStartAt": null,
					"timeExpireSecond": null,
					"prepayId": null,
					"request": null,
					"result": null,
					"state": 0,
					"updatedAt": 1557918176000,
					"createdAt": 1557916908000,
					"delFlag": 0
				}],
				"refundInfoListInfo": []
			},
			"error": null,
			"version": null,
			"extParam": null
		}
		*/



		String type = "1";//类型(1支付、2退款)

		//查询订单信息
		StudentCourseOrderExt order = studentCourseOrderService.findByOrderNo(orderNo);


		//类型(1支付、2退款)
		if(StringUtils.isNotEmpty(type) && "1".equals(type)){//支付

			if(order!=null && order.getState()==1){//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
				if(jo!=null && jo.getInt("code")>0 && jo.getInt("code")==200){
					JSONObject jo_data = jo.getJSONObject("data");
					int orderAmount = jo_data.getInt("orderAmount");//订单金额
					int payTotalAmount = jo_data.getInt("payTotalAmount");//支付总金额
					int refundTotalAmount = jo_data.getInt("refundTotalAmount");//退款总金额

					Double d = order.getOrderMoney().doubleValue()*100;//订单总金额,单位:分
					Double p = d-(order.getDiscountMoney()==null?0:order.getDiscountMoney().doubleValue()*100);//支付总金额,单位:分
					if(d.intValue()==orderAmount && p.intValue()==payTotalAmount){//判断订单是否支付成功

						if(order.getState()==1){
							order.setPayMoney(BigDecimal.valueOf(p/100.0));
							order.setPayAt(new Date());
							order.setState(3);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
							order.setUpdatedAt(new Date());
							order.setDelFlag(0);
							studentCourseOrderService.update(order);
						}
						//删除学生优惠券数据
						Map<String, Object> mm = new HashMap<>();
						mm.put("discountId", order.getDiscountId().toString());//优惠券id
						mm.put("studentId", order.getStudentId().toString());
						mm.put("state", "1");//状态(0未使用、1已使用)
						mm.put("delFlag", "1");
						studentDiscountService.update4Map(mm);



						if(order.getActivityTuanOrderId()==null || order.getActivityTuanOrderId()==0){
							//生成学生课程数据
							activityTuanOrderService.generateStudentCourseData(order);



							String channelId = order.getChannelId()==null?"":order.getChannelId().toString();
							studentId = order.getStudentId().toString();
							String courseType = order.getCourseType().toString();//课程类型(1单课程、2系列课)
							Long courseGroupId = order.getCourseGroupId();
							String courseIds = order.getCourseIds();
							String cId = "";
							String cgId = "";
							if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
								cId = courseIds;
							}
							if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
								cgId = courseGroupId.toString();
							}
							//根据渠道课程的优惠券的关系生成学生优惠券信息
							if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(courseType) && StringUtils.isNotEmpty(studentId)){
								studentDiscountService.insert(channelId, courseType, studentId, cId, cgId, order.getId().toString());
							}



							//更新购课人数限制
							courseService.updateCourseEnrollCount(cId);
						}



						Integer _sourceType = 0;//来源类型(购买单课程1、购买系列课2、活动领取失败3、活动领取成功4)
						Long _courseId = 0L;//单课程id或系列课id
						if(order.getCourseType()==1){
							_sourceType = 1;
							_courseId = StringUtils.isNotEmpty(order.getCourseIds())?Long.parseLong(order.getCourseIds()):_courseId;
						}
						if(order.getCourseType()==2){
							_sourceType = 2;
							_courseId = order.getCourseGroupId();
						}
						channelStatisticsService.generateChannelStatistics(order.getChannelId()!=null?order.getChannelId().toString():"", order.getStudentId().toString(), _sourceType.toString(), order.getId().toString(), order.getCourseType().toString(), _courseId.toString(), "2");

					}

				}

			}


			/*order.setUpdatedAt(new Date());
			order.setDelFlag(0);
			studentCourseOrderService.update(order);*/

		}

		if(StringUtils.isNotEmpty(type) && "2".equals(type)){//退款
			//do nothing
		}


		//查询订单信息
		StudentCourseOrderExt orderInfo = studentCourseOrderService.findByOrderNo(orderNo);


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),orderInfo);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课程关联的教师二维码信息
	@RequestMapping(value = "finish")
	public ResponseEntity<ResponseJson> finish(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");//课程id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> m = new HashMap<>();
		m.put("courseId", courseId);
		m.put("studentId", studentId);
		m.put("delFlag", "0");
		CourseExt courseExt = courseService.findObjectByCondition(m);

		Map<String, Object> mm = new HashMap<>();
		mm.put("courseId", courseId);
		mm.put("studentId", studentId);
		mm.put("delFlag", "0");
		StudentCourseExt studentCourseExt = studentCourseService.findObjectByCondition(mm);

		String qrCodeUrl4Teacher = "";
		if(studentCourseExt!=null && studentCourseExt.getId()>0 && studentCourseExt.getTeacherId()!=null && studentCourseExt.getTeacherId()>0){
			Teacher teacher = teacherService.findById(studentCourseExt.getTeacherId());
			if(teacher!=null && teacher.getId()>0){
				qrCodeUrl4Teacher = teacher.getQrCodeUrl();
			}
		}


		Map<String, Object> mmm = new LinkedHashMap<>();
		mmm.put("courseInfo", courseExt);
		mmm.put("qrCodeUrl4Official", Constant.qrCodeUrl4Official);
		mmm.put("qrCodeUrl4Teacher", qrCodeUrl4Teacher);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),mmm);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}






	//创建订单
	@RequestMapping(value = "create")
	public ResponseEntity<ResponseJson> create(HttpServletRequest request, HttpServletResponse response, String valuationFlag) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");//课程id
		String courseGroupId = (String) paramMap.get("courseGroupId");//课程分组id
		String payChannel = (String) paramMap.get("payChannel");//支付渠道(1微信、2支付宝)
		String payType = (String) paramMap.get("payType");//支付方式(weixin-web|weixin-app|weixin-jsapi)

		String channelId = (String) paramMap.get("channelId");//渠道id

		String discountId = (String) paramMap.get("discountId");//优惠券id

		String appId = (String) paramMap.get("appId");
		String appKey = (String) paramMap.get("appKey");

		//计价标识(1原价、2优惠价)
		valuationFlag = (StringUtils.isNotEmpty(valuationFlag) && "1".equals(valuationFlag))?"1":"2";


		int sourceType = 0;//来源类型(1WEB端、2小程序、3公众号)
		if(StringUtils.isNotEmpty(appId) && Constant.appId_web.equals(appId)){
			sourceType = 1;
		}
		if(StringUtils.isNotEmpty(appId) && Constant.appId_xiaochengxu.equals(appId)){
			sourceType = 2;
		}
		if(StringUtils.isNotEmpty(appId) && Constant.appId_gongzhonghao.equals(appId)){
			sourceType = 3;
		}



		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) && StringUtils.isEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id或年课id不能同时为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseId) && StringUtils.isNotEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id或年课id不能同时存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(payChannel) || StringUtils.isEmpty(payType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"支付渠道、支付方式不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//验证支付方式
		if("1".equals(payChannel) && !"weixin-web".equalsIgnoreCase(payType) && !"weixin-app".equalsIgnoreCase(payType) && !"weixin-jsapi".equalsIgnoreCase(payType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"支付方式有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Integer courseType = 0;//课程类型(1单课程、2系列课)
		String courseGroupIdStr = "0";
		String coursePackageIds = "";
		String courseIds = "";
		BigDecimal price = new BigDecimal(0);
		BigDecimal realPrice = new BigDecimal(0);

		CourseExt courseInfo = null;
		CourseGroupExt courseGroupInfo = null;

		if(StringUtils.isNotEmpty(courseId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseId", courseId);
			CourseExt course = courseService.findObjectById(mm);
			if(course.getState()!=3){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"报名中的课程才能购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			courseType = 1;
			courseGroupIdStr = "0";
			coursePackageIds = course.getCoursePackageId().toString();
			courseIds = course.getId().toString();
			price = course.getPrice();
			realPrice = course.getRealPrice();
			courseInfo = course;


			Map<String, Object> m = new HashMap<>();
			m.put("coursePackageId", coursePackageIds);
			m.put("studentId", studentId);
			m.put("delFlag","0");
			StudentCourseExt studentCourseExt = studentCourseService.findObjectByCondition(m);
			Integer buyFlag;//购买标识(1未购买、2已购买)
			if(studentCourseExt!=null && studentCourseExt.getId()>0){
				buyFlag = 2;
			} else{
				buyFlag = 1;
			}
			if(buyFlag == 2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			//检查是否存在符合条件的已付款的其他拼团订单
			Map<String, Object> pinTuanMap = new HashMap<>();
			pinTuanMap.put("delFlag", "0");
			pinTuanMap.put("state", "3");
			pinTuanMap.put("studentId", studentId);
			pinTuanMap.put("courseType", courseType.toString());
			pinTuanMap.put("coursePackageIds", coursePackageIds);
			List<StudentCourseOrderExt> scoList = studentCourseOrderService.findListByCondition4PinTuanOrder(pinTuanMap);
			if(scoList.size()>0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isNotEmpty(courseGroupId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseGroupId", courseGroupId);
			CourseGroupExt courseGroup = courseGroupService.findObjectByCondition(mm);
			if(courseGroup.getState()!=2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"上线中的年课才能购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			courseType = 2;
			courseGroupIdStr = courseGroupId;
			coursePackageIds = courseGroup.getCoursePackageIds();
			courseIds = courseGroup.getCourseIds();
			price = courseGroup.getPrice();
			realPrice = courseGroup.getRealPrice();
			courseGroupInfo = courseGroup;


			Map<String, Object> m = new HashMap<>();
			m.put("studentId", studentId);
			m.put("delFlag","0");
			List<StudentCourseExt> studentCourseInfoList = studentCourseService.findListByCondition4CourseGroup(m);
			Integer buyFlag;//购买标识(1未购买、2已购买)
			if(studentCourseInfoList!=null && studentCourseInfoList.size()>0){
				buyFlag = 2;
			} else{
				buyFlag = 1;
			}
			if(buyFlag == 2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			//检查是否存在符合条件的已付款的其他拼团订单
			Map<String, Object> pinTuanMap = new HashMap<>();
			pinTuanMap.put("delFlag", "0");
			pinTuanMap.put("state", "3");
			pinTuanMap.put("studentId", studentId);
			pinTuanMap.put("courseType", courseType.toString());
			List<StudentCourseOrderExt> scoList = studentCourseOrderService.findListByCondition4PinTuanOrder(pinTuanMap);
			if(scoList.size()>0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}


		ChannelCourseExt channelCourseExt = null;
		if(StringUtils.isNotEmpty(channelId)){
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("delFlag", "0");
			_map.put("channelId", channelId);
			_map.put("courseType", courseType);
			_map.put("courseId", ("2".equals(courseType.toString())?"0":courseId));
			_map.put("courseGroupId", ("1".equals(courseType.toString())?"0":courseGroupId));
			channelCourseExt = channelCourseService.findObjectByCondition(_map);
			if(channelCourseExt==null){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道课程不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(channelCourseExt.getPrice()==null || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格信息不完整",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}


		BigDecimal orderMoney = new BigDecimal(0);
		if("1".equals(valuationFlag)){//原价
			orderMoney = price;
		}
		if("2".equals(valuationFlag)){//优惠价
			orderMoney = realPrice;
		}


		if(StringUtils.isNotEmpty(channelId) && "2".equals(valuationFlag)){
			if(channelCourseExt!=null){
				if(channelCourseExt.getPrice()!=null){
					orderMoney = channelCourseExt.getPrice();
				}
			}
		}


		//验证优惠券
		BigDecimal discountMoney = new BigDecimal(0);
		if(StringUtils.isNotEmpty(discountId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("studentId", studentId);
			mm.put("discountId", discountId);
			StudentDiscountExt studentDiscountExt = studentDiscountService.findObjectByCondition(mm);
			if(studentDiscountExt==null || studentDiscountExt.getId()==0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券不属于你或已失效",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(studentDiscountExt.getValidityStart().getTime()>new Date().getTime() || studentDiscountExt.getValidityEnd().getTime()<new Date().getTime()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券不在使用期限内",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(courseType!=studentDiscountExt.getCourseType()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券的课程类型不匹配",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(orderMoney.compareTo(studentDiscountExt.getMoney()) == -1){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"未达到满减条件",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			Integer type = studentDiscountExt.getType();//类型(0抵用券、1折扣券)
			BigDecimal money = studentDiscountExt.getMoney();
			BigDecimal amount = studentDiscountExt.getAmount();
			if(type!=null && type==0){
				discountMoney = amount;
			}
			if(type!=null && type==1){
				discountMoney = orderMoney.multiply(amount).setScale(2, BigDecimal.ROUND_HALF_UP);//相乘(保留2位小数)
			}
			if(orderMoney.subtract(discountMoney).compareTo(BigDecimal.ZERO)==-1 || orderMoney.subtract(discountMoney).compareTo(BigDecimal.ZERO)==0){//当订单金额-优惠金额大于0时才可以
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券优惠价格要小于订单价格",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}else{
			discountId = "0";
		}



		//查询待付款的订单信息
		Map<String, Object> m = new HashMap<>();
		m.put("coursePackageIds", coursePackageIds);
		m.put("studentId", studentId);
		m.put("delFlag","0");
		m.put("state","1");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		m.put("courseType", courseType);
		StudentCourseOrderExt order = studentCourseOrderService.findObjectByCondition(m);
		Integer useExistFlag = Constant.useExistFlag;//是否使用已存在待付款订单的标识(1使用、2不使用)
		if(useExistFlag==1 && order!=null && order.getId()>0){

			order.setCourseType(courseType);
			order.setCourseGroupId(Long.parseLong(courseGroupIdStr));
			order.setCoursePackageIds(coursePackageIds);
			order.setCourseIds(courseIds);
			order.setStudentId(Long.parseLong(studentId));
			order.setPhone(studentExt.getPhone());
			if(StringUtils.isNotEmpty(channelId)){
				order.setChannelId(Long.parseLong(channelId));
			}
			order.setActivityTuanOrderId(0L);
			//order.setOrderNo(OrderUtils.getUniqueCode().toString());//订单编号
			order.setSourceType(sourceType);//来源类型(1WEB端、2小程序、3公众号)
			order.setOrderMoney(orderMoney);
			order.setDiscountId(Long.parseLong(discountId));
			order.setDiscountMoney(discountMoney);
			order.setPayChannel(Integer.parseInt(StringUtils.isEmpty(payChannel)?"0":payChannel));//支付渠道(1微信、2支付宝)
			order.setPayType(payType);
			order.setAppId(paramMap.get("appId")==null?"":(String)paramMap.get("appId"));
			order.setAppKey(paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));

			order.setRemark("");
			order.setState(1);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			order.setCreatedAt(new Date());
			order.setUpdatedAt(new Date());
			order.setDelFlag(0);

			studentCourseOrderService.update(order);
		}else{
			order = new StudentCourseOrderExt();

			order.setCourseType(courseType);
			order.setCourseGroupId(Long.parseLong(courseGroupIdStr));
			order.setCoursePackageIds(coursePackageIds);
			order.setCourseIds(courseIds);
			order.setStudentId(Long.parseLong(studentId));
			order.setPhone(studentExt.getPhone());
			if(StringUtils.isNotEmpty(channelId)){
				order.setChannelId(Long.parseLong(channelId));
			}
			order.setActivityTuanOrderId(0L);
			order.setOrderNo(OrderUtils.getUniqueCode().toString());//订单编号
			order.setSourceType(sourceType);//来源类型(1WEB端、2小程序、3公众号)
			order.setOrderMoney(orderMoney);
			order.setDiscountId(Long.parseLong(discountId));
			order.setDiscountMoney(discountMoney);
			order.setPayChannel(Integer.parseInt(StringUtils.isEmpty(payChannel)?"0":payChannel));//支付渠道(1微信、2支付宝)
			order.setPayType(payType);
			order.setAppId(paramMap.get("appId")==null?"":(String)paramMap.get("appId"));
			order.setAppKey(paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));

			order.setRemark("");
			order.setState(1);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			order.setCreatedAt(new Date());
			order.setUpdatedAt(new Date());
			order.setDelFlag(0);

			studentCourseOrderService.insert(order);
		}




		String title = "";
		String detail = "";


		if(courseType==1){
			order.setCourseInfo(courseInfo);
			title = courseInfo.getName();
			detail = courseInfo.getName();
		}
		if(courseType==2){
			order.setCourseGroupInfo(courseGroupInfo);
			title = courseGroupInfo.getName();
			detail = courseGroupInfo.getName();
		}



		//调用支付接口
		if("1".equals(payChannel)){
			//微信

			JSONObject jo = studentCourseOrderService.invokePay4Weixin(order, title, detail, loginToken, payType, paramMap.get("appId")==null?"":(String)paramMap.get("appId"), paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));
			String state = jo.getString("state");
			JSONObject result = jo.getJSONObject("result");
			JSONObject payResult = result.getJSONObject("payResult");

			if(StringUtils.isNotEmpty(state) && "SUCCESS".equals(state)){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),result);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}else{
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"调用支付失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),result);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}else{
			//支付宝

			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此方式暂未开放",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),null);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}



		/*ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),order);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);*/

	}



	//订单支付(继续支付)
	@RequestMapping(value = "pay")
	public ResponseEntity<ResponseJson> pay(HttpServletRequest request, HttpServletResponse response, String valuationFlag) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");//课程id
		String courseGroupId = (String) paramMap.get("courseGroupId");//课程分组id
		String payChannel = (String) paramMap.get("payChannel");//支付渠道(1微信、2支付宝)
		String payType = (String) paramMap.get("payType");//支付方式(weixin-web|weixin-app|weixin-jsapi)
		String orderNo = (String) paramMap.get("orderNo");//订单编号

		String channelId = (String) paramMap.get("channelId");//渠道id

		String discountId = (String) paramMap.get("discountId");//优惠券id

		String appId = (String) paramMap.get("appId");
		String appKey = (String) paramMap.get("appKey");

		//计价标识(1原价、2优惠价)
		valuationFlag = (StringUtils.isNotEmpty(valuationFlag) && "1".equals(valuationFlag))?"1":"2";


		int sourceType = 0;//来源类型(1WEB端、2小程序、3公众号)
		if(StringUtils.isNotEmpty(appId) && Constant.appId_web.equals(appId)){
			sourceType = 1;
		}
		if(StringUtils.isNotEmpty(appId) && Constant.appId_xiaochengxu.equals(appId)){
			sourceType = 2;
		}
		if(StringUtils.isNotEmpty(appId) && Constant.appId_gongzhonghao.equals(appId)){
			sourceType = 3;
		}



		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) && StringUtils.isEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id或年课id不能同时为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseId) && StringUtils.isNotEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id或年课id不能同时存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(payChannel) || StringUtils.isEmpty(payType) || StringUtils.isEmpty(orderNo)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"支付渠道、支付方式、订单编号不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//验证支付方式
		if("1".equals(payChannel) && !"weixin-web".equalsIgnoreCase(payType) && !"weixin-app".equalsIgnoreCase(payType) && !"weixin-jsapi".equalsIgnoreCase(payType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"支付方式有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}



		StudentCourseOrderExt order = studentCourseOrderService.findByOrderNo(orderNo);

		Integer courseType = 0;//课程类型(1单课程、2系列课)
		String courseGroupIdStr = "0";
		String coursePackageIds = "";
		String courseIds = "";
		BigDecimal price = new BigDecimal(0);
		BigDecimal realPrice = new BigDecimal(0);

		CourseExt courseInfo = null;
		CourseGroupExt courseGroupInfo = null;

		if(StringUtils.isNotEmpty(courseId)){
			Course c = courseService.findById(Long.parseLong(order.getCourseIds()));

			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseId", courseId);
			CourseExt course = courseService.findObjectById(mm);

			if(course==null){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(course.getCoursePackageId()!=c.getCoursePackageId()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"与原订单的课程包不一致，不能继续支付",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(course.getState()!=3){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"报名中的课程才能购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}


			courseType = 1;
			courseGroupIdStr = "0";
			coursePackageIds = course.getCoursePackageId().toString();
			courseIds = course.getId().toString();
			price = course.getPrice();
			realPrice = course.getRealPrice();
			courseInfo = course;


			Map<String, Object> m = new HashMap<>();
			m.put("coursePackageId", coursePackageIds);
			m.put("studentId", studentId);
			m.put("delFlag","0");
			StudentCourseExt studentCourseExt = studentCourseService.findObjectByCondition(m);
			Integer buyFlag;//购买标识(1未购买、2已购买)
			if(studentCourseExt!=null && studentCourseExt.getId()>0){
				buyFlag = 2;
			} else{
				buyFlag = 1;
			}
			if(buyFlag == 2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			//检查是否存在符合条件的已付款的其他拼团订单
			Map<String, Object> pinTuanMap = new HashMap<>();
			pinTuanMap.put("delFlag", "0");
			pinTuanMap.put("state", "3");
			pinTuanMap.put("studentId", studentId);
			pinTuanMap.put("courseType", courseType.toString());
			pinTuanMap.put("coursePackageIds", coursePackageIds);
			List<StudentCourseOrderExt> scoList = studentCourseOrderService.findListByCondition4PinTuanOrder(pinTuanMap);
			if(scoList.size()>0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		if(StringUtils.isNotEmpty(courseGroupId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseGroupId", courseGroupId);
			CourseGroupExt courseGroup = courseGroupService.findObjectByCondition(mm);
			if(courseGroup.getState()!=2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"上线中的年课才能购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			courseType = 2;
			courseGroupIdStr = courseGroupId;
			coursePackageIds = courseGroup.getCoursePackageIds();
			courseIds = courseGroup.getCourseIds();
			price = courseGroup.getPrice();
			realPrice = courseGroup.getRealPrice();
			courseGroupInfo = courseGroup;


			Map<String, Object> m = new HashMap<>();
			m.put("studentId", studentId);
			m.put("delFlag","0");
			List<StudentCourseExt> studentCourseInfoList = studentCourseService.findListByCondition4CourseGroup(m);
			Integer buyFlag;//购买标识(1未购买、2已购买)
			if(studentCourseInfoList!=null && studentCourseInfoList.size()>0){
				buyFlag = 2;
			} else{
				buyFlag = 1;
			}
			if(buyFlag == 2){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			//检查是否存在符合条件的已付款的其他拼团订单
			Map<String, Object> pinTuanMap = new HashMap<>();
			pinTuanMap.put("delFlag", "0");
			pinTuanMap.put("state", "3");
			pinTuanMap.put("studentId", studentId);
			pinTuanMap.put("courseType", courseType.toString());
			List<StudentCourseOrderExt> scoList = studentCourseOrderService.findListByCondition4PinTuanOrder(pinTuanMap);
			if(scoList.size()>0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"不能重复购买",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}


		if(order.getState()!=1){//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此订单不能继续支付",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		ChannelCourseExt channelCourseExt = null;
		if(StringUtils.isNotEmpty(channelId)){
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("delFlag", "0");
			_map.put("channelId", channelId);
			_map.put("courseType", courseType);
			_map.put("courseId", ("2".equals(courseType.toString())?"0":courseId));
			_map.put("courseGroupId", ("1".equals(courseType.toString())?"0":courseGroupId));
			channelCourseExt = channelCourseService.findObjectByCondition(_map);
			if(channelCourseExt==null){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道课程不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(channelCourseExt.getPrice()==null || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格信息不完整",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}


		BigDecimal orderMoney = new BigDecimal(0);
		if("1".equals(valuationFlag)){//原价
			orderMoney = price;
		}
		if("2".equals(valuationFlag)){//优惠价
			orderMoney = realPrice;
		}


		if(StringUtils.isNotEmpty(channelId) && "2".equals(valuationFlag)){
			if(channelCourseExt!=null){
				if(channelCourseExt.getPrice()!=null){
					orderMoney = channelCourseExt.getPrice();
				}
			}
		}


		//验证优惠券
		BigDecimal discountMoney = new BigDecimal(0);
		if(StringUtils.isNotEmpty(discountId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("studentId", studentId);
			mm.put("discountId", discountId);
			StudentDiscountExt studentDiscountExt = studentDiscountService.findObjectByCondition(mm);
			if(studentDiscountExt==null || studentDiscountExt.getId()==0){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券不属于你或已失效",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(studentDiscountExt.getValidityStart().getTime()>new Date().getTime() || studentDiscountExt.getValidityEnd().getTime()<new Date().getTime()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券不在使用期限内",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(courseType!=studentDiscountExt.getCourseType()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券的课程类型不匹配",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(orderMoney.compareTo(studentDiscountExt.getMoney()) == -1){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"未达到满减条件",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			Integer type = studentDiscountExt.getType();//类型(0抵用券、1折扣券)
			BigDecimal money = studentDiscountExt.getMoney();
			BigDecimal amount = studentDiscountExt.getAmount();
			if(type!=null && type==0){
				discountMoney = amount;
			}
			if(type!=null && type==1){
				discountMoney = orderMoney.multiply(amount).setScale(2, BigDecimal.ROUND_HALF_UP);//相乘(保留2位小数)
			}
			if(orderMoney.subtract(discountMoney).compareTo(BigDecimal.ZERO)==-1 || orderMoney.subtract(discountMoney).compareTo(BigDecimal.ZERO)==0){//当订单金额-优惠金额大于0时才可以
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"优惠券优惠价格要小于订单价格",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}else{
			discountId = "0";
		}



		Integer useExistFlag = Constant.useExistFlag;//是否使用已存在待付款订单的标识(1使用、2不使用)
		if(useExistFlag==1 && order!=null && order.getId()>0){

			order.setCourseType(courseType);
			order.setCourseGroupId(Long.parseLong(courseGroupIdStr));
			order.setCoursePackageIds(coursePackageIds);
			order.setCourseIds(courseIds);
			order.setStudentId(Long.parseLong(studentId));
			order.setPhone(studentExt.getPhone());
			if(StringUtils.isNotEmpty(channelId)){
				order.setChannelId(Long.parseLong(channelId));
			}
			order.setActivityTuanOrderId(0L);
			//order.setOrderNo(OrderUtils.getUniqueCode().toString());//订单编号
			order.setSourceType(sourceType);//来源类型(1WEB端、2小程序、3公众号)
			order.setOrderMoney(orderMoney);
			order.setDiscountId(Long.parseLong(discountId));
			order.setDiscountMoney(discountMoney);
			order.setPayChannel(Integer.parseInt(StringUtils.isEmpty(payChannel)?"0":payChannel));//支付渠道(1微信、2支付宝)
			order.setPayType(payType);
			order.setAppId(paramMap.get("appId")==null?"":(String)paramMap.get("appId"));
			order.setAppKey(paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));

			order.setRemark("");
			order.setState(1);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			order.setCreatedAt(new Date());
			order.setUpdatedAt(new Date());
			order.setDelFlag(0);

			studentCourseOrderService.update(order);
		}else{
			order = new StudentCourseOrderExt();

			order.setCourseType(courseType);
			order.setCourseGroupId(Long.parseLong(courseGroupIdStr));
			order.setCoursePackageIds(coursePackageIds);
			order.setCourseIds(courseIds);
			order.setStudentId(Long.parseLong(studentId));
			order.setPhone(studentExt.getPhone());
			if(StringUtils.isNotEmpty(channelId)){
				order.setChannelId(Long.parseLong(channelId));
			}
			order.setActivityTuanOrderId(0L);
			order.setOrderNo(OrderUtils.getUniqueCode().toString());//订单编号
			order.setSourceType(sourceType);//来源类型(1WEB端、2小程序、3公众号)
			order.setOrderMoney(orderMoney);
			order.setDiscountId(Long.parseLong(discountId));
			order.setDiscountMoney(discountMoney);
			order.setPayChannel(Integer.parseInt(StringUtils.isEmpty(payChannel)?"0":payChannel));//支付渠道(1微信、2支付宝)
			order.setPayType(payType);
			order.setAppId(paramMap.get("appId")==null?"":(String)paramMap.get("appId"));
			order.setAppKey(paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));

			order.setRemark("");
			order.setState(1);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			order.setCreatedAt(new Date());
			order.setUpdatedAt(new Date());
			order.setDelFlag(0);

			studentCourseOrderService.insert(order);
		}



		String title = "";
		String detail = "";


		if(courseType==1){
			order.setCourseInfo(courseInfo);
			title = courseInfo.getName();
			detail = courseInfo.getName();
		}
		if(courseType==2){
			order.setCourseGroupInfo(courseGroupInfo);
			title = courseGroupInfo.getName();
			detail = courseGroupInfo.getName();
		}



		//调用支付接口
		if("1".equals(payChannel)){
			//微信

			JSONObject jo = studentCourseOrderService.invokePay4Weixin(order, title, detail, loginToken, payType, paramMap.get("appId")==null?"":(String)paramMap.get("appId"), paramMap.get("appKey")==null?"":(String)paramMap.get("appKey"));
			String state = jo.getString("state");
			JSONObject result = jo.getJSONObject("result");
			JSONObject payResult = result.getJSONObject("payResult");

			if(StringUtils.isNotEmpty(state) && "SUCCESS".equals(state)){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),result);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}else{
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"调用支付失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),result);
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}else{
			//支付宝

			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此方式暂未开放",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),null);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}



	}



	/*************/



	//创建订单 - 原价
	@RequestMapping(value = "/original/create")
	public ResponseEntity<ResponseJson> create4Original(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		ResponseEntity<ResponseJson> result = this.create(request, response, "1");//计价标识(1原价、2优惠价)
		return result;
	}



	//订单支付(继续支付) - 原价
	@RequestMapping(value = "/original/pay")
	public ResponseEntity<ResponseJson> pay4Original(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		ResponseEntity<ResponseJson> result = this.pay(request, response, "1");//计价标识(1原价、2优惠价)
		return result;
	}









}
