package cn.com.mys.project_kernel.base.controller.account.teacher;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.Production;
import cn.com.mys.project_kernel.base.entity.ext.ProductionExt;
import cn.com.mys.project_kernel.base.entity.ext.TeacherExt;
import cn.com.mys.project_kernel.base.service.ProductionService;
import cn.com.mys.project_kernel.base.service.TeacherService;
import cn.com.mys.project_kernel.base.service.TbTestService;
import cn.com.mys.project_kernel.base.util.ParamUtils;
import cn.com.mys.project_kernel.base.util.PropertiesUtils;
import cn.com.mys.project_kernel.base.util.RequestUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 教师作品
 *
 */
@Controller
@RequestMapping(value = "/account/teacher/production")
public class TeacherProductionController {

	Log log = LogFactory.getLog(TeacherProductionController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private TeacherService teacherService;
	@Autowired
	private ProductionService productionService;



	//查询作品列表(个人)
	@RequestMapping(value = "list")
	public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String sourceType = (String) paramMap.get("sourceType");//来源类型(1系统、2教师、3学生)
		String state = (String) paramMap.get("state");//状态(1未发布、2已发布、3已下线)
		String name = (String) paramMap.get("name");


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("personId", teacherId);
		map4Param.put("teacherId", teacherId);
		map4Param.put("page", page);

		map4Param.put("sourceType", "2");
		map4Param.put("state", state);
		map4Param.put("name", name);


		Integer count = productionService.findCountByCondition(map4Param);
		List<ProductionExt> list = productionService.findListByCondition(map4Param);

		//查询不同状态(1未发布、2已发布、3已下线)的数量
		map4Param.put("state", "1");
		Integer c1 = productionService.findCountByCondition(map4Param);
		map4Param.put("state", "2");
		Integer c2 = productionService.findCountByCondition(map4Param);
		map4Param.put("state", "3");
		Integer c3 = productionService.findCountByCondition(map4Param);

		Map<String, Object> m1 = new HashMap<String, Object>();
		m1.put("t", "A");
		m1.put("n", "未发布");
		Map<String, Object> m2 = new HashMap<String, Object>();
		m2.put("t", "B");
		m2.put("n", "已发布");
		Map<String, Object> m3 = new HashMap<String, Object>();
		m3.put("t", "C");
		m3.put("n", "已下线");

		m1.put("c", c1);//未发布
		m2.put("c", c2);//已发布
		m3.put("c", c3);//已下线

		List<Object> l = new ArrayList<>();
		l.add(m1);l.add(m2);//t:A"未发布"、B"已发布"

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,l,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询作品详情
	@RequestMapping(value = "detail")
	public ResponseEntity<ResponseJson> detail(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("teacherId", teacherId);
		map4Param.put("productionId", productionId);

		ProductionExt productionExt = productionService.findObjectByCondition(map4Param);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//保存作品
	@RequestMapping(value = "save")
	public ResponseEntity<ResponseJson> save(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String sourceType = (String) paramMap.get("sourceType");//来源类型(1系统、2教师、3学生)
		String name = (String) paramMap.get("name");//名称
		String content = (String) paramMap.get("content");//内容地址
		String coverUrl = (String) paramMap.get("coverUrl");//封面图片url
		String intro = (String) paramMap.get("intro");//介绍
		String operateExplain = (String) paramMap.get("operateExplain");//操作说明
		String tagIds = (String) paramMap.get("tagIds");//标签id(多个","分隔)
		String openFlag = (String) paramMap.get("openFlag");//开放标记(1开放、2不开放)
		String category = (String) paramMap.get("category");//种类(1scratch)


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(sourceType) || StringUtils.isEmpty(category)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"来源类型、种类不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ProductionExt productionExt = new ProductionExt();
		productionExt.setPersonId(Long.parseLong(teacherId));
		productionExt.setSourceType(StringUtils.isEmpty(sourceType)?null:Integer.parseInt(sourceType));
		productionExt.setName(name);
		productionExt.setContent(content);
		productionExt.setCoverUrl(StringUtils.isEmpty(coverUrl)?Constant.coverUrl4Default:coverUrl);
		productionExt.setIntro(intro);
		productionExt.setOperateExplain(operateExplain);
		productionExt.setTagIds(tagIds);
		productionExt.setOpenFlag(StringUtils.isEmpty(openFlag)?null:Integer.parseInt(openFlag));
		productionExt.setCategory(StringUtils.isEmpty(category)?null:Integer.parseInt(category));

		productionExt.setLikeCount(0);
		productionExt.setLookCount(0);
		productionExt.setCommentCount(0);
		productionExt.setScore(0);

		productionExt.setState(1);//状态(1未发布、2已发布、3已下线)
		productionExt.setCreatedAt(new Date());
		productionExt.setUpdatedAt(new Date());
		productionExt.setDelFlag(0);
		productionService.insert(productionExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//更新作品
	@RequestMapping(value = "update")
	public ResponseEntity<ResponseJson> update(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		String sourceType = (String) paramMap.get("sourceType");//来源类型(1系统、2教师、3学生)
		String name = (String) paramMap.get("name");//名称
		String content = (String) paramMap.get("content");//内容地址
		String coverUrl = (String) paramMap.get("coverUrl");//封面图片url
		String intro = (String) paramMap.get("intro");//介绍
		String operateExplain = (String) paramMap.get("operateExplain");//操作说明
		String tagIds = (String) paramMap.get("tagIds");//标签id(多个","分隔)
		String openFlag = (String) paramMap.get("openFlag");//开放标记(1开放、2不开放)
		String category = (String) paramMap.get("category");//种类(1scratch)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Production production = productionService.findById(Long.parseLong(productionId));
		production.setPersonId(Long.parseLong(teacherId));
		production.setSourceType(StringUtils.isEmpty(sourceType)?null:Integer.parseInt(sourceType));
		production.setName(name);
		production.setContent(content);
		production.setCoverUrl(StringUtils.isEmpty(coverUrl)?Constant.coverUrl4Default:coverUrl);
		production.setIntro(intro);
		production.setOperateExplain(operateExplain);
		production.setTagIds(tagIds);
		production.setOpenFlag(StringUtils.isEmpty(openFlag)?null:Integer.parseInt(openFlag));
		production.setCategory(StringUtils.isEmpty(category)?null:Integer.parseInt(category));

		//production.setLikeCount(0);
		//production.setLookCount(0);
		//production.setCommentCount(0);
		//production.setScore(0);

		//production.setState(1);//状态(1未发布、2已发布、3已下线)
		production.setCreatedAt(new Date());
		production.setUpdatedAt(new Date());
		production.setDelFlag(0);
		productionService.update(production);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),production);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//删除作品
	@RequestMapping(value = "delete")
	public ResponseEntity<ResponseJson> delete(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Production production = productionService.findById(Long.parseLong(productionId));

		/*if(production!=null && production.getState()==2){//状态(1未发布、2已发布、3已下线)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"已发布作品不能直接删除",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		production.setDelFlag(1);
		productionService.update(production);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),production);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//发布作品
	@RequestMapping(value = "publish")
	public ResponseEntity<ResponseJson> publish(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		String sourceType = (String) paramMap.get("sourceType");//来源类型(1系统、2教师、3学生)
		String name = (String) paramMap.get("name");//名称
		String content = (String) paramMap.get("content");//内容地址
		String coverUrl = (String) paramMap.get("coverUrl");//封面图片url
		String intro = (String) paramMap.get("intro");//介绍
		String operateExplain = (String) paramMap.get("operateExplain");//操作说明
		String tagIds = (String) paramMap.get("tagIds");//标签id(多个","分隔)
		String openFlag = (String) paramMap.get("openFlag");//开放标记(1开放、2不开放)
		String category = (String) paramMap.get("category");//种类(1scratch)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(sourceType) || StringUtils.isEmpty(category)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品来源类型、种类不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(content) || StringUtils.isEmpty(openFlag)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"内容、开放标识不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(name) || StringUtils.isEmpty(intro) || StringUtils.isEmpty(operateExplain)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"名称、介绍、操作说明不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Production production = productionService.findById(Long.parseLong(productionId));
		if(production==null || production.getId()==0){
			ProductionExt productionExt = new ProductionExt();
			//新增
			productionExt.setPersonId(Long.parseLong(teacherId));
			productionExt.setSourceType(StringUtils.isEmpty(sourceType)?null:Integer.parseInt(sourceType));
			productionExt.setName(name);
			productionExt.setContent(content);
			productionExt.setCoverUrl(StringUtils.isEmpty(coverUrl)?Constant.coverUrl4Default:coverUrl);
			productionExt.setIntro(intro);
			productionExt.setOperateExplain(operateExplain);
			productionExt.setTagIds(tagIds);
			productionExt.setOpenFlag(StringUtils.isEmpty(openFlag)?null:Integer.parseInt(openFlag));
			productionExt.setCategory(StringUtils.isEmpty(category)?null:Integer.parseInt(category));

			productionExt.setLikeCount(0);
			productionExt.setLookCount(0);
			productionExt.setCommentCount(0);
			productionExt.setScore(0);

			productionExt.setState(2);//状态(1未发布、2已发布、3已下线)
			productionExt.setCreatedAt(new Date());
			productionExt.setUpdatedAt(new Date());
			productionExt.setDelFlag(0);
			productionService.insert(productionExt);

			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),productionExt);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//更新
		production.setPersonId(Long.parseLong(teacherId));
		production.setSourceType(StringUtils.isEmpty(sourceType)?null:Integer.parseInt(sourceType));
		production.setName(name);
		production.setContent(content);
		production.setCoverUrl(StringUtils.isEmpty(coverUrl)?Constant.coverUrl4Default:coverUrl);
		production.setIntro(intro);
		production.setOperateExplain(operateExplain);
		production.setTagIds(tagIds);
		production.setOpenFlag(StringUtils.isEmpty(openFlag)?null:Integer.parseInt(openFlag));
		production.setCategory(StringUtils.isEmpty(category)?null:Integer.parseInt(category));

		//production.setLikeCount(0);
		//production.setLookCount(0);
		//production.setCommentCount(0);
		//production.setScore(0);

		production.setState(2);//状态(1未发布、2已发布、3已下线)
		production.setCreatedAt(new Date());
		production.setUpdatedAt(new Date());
		production.setDelFlag(0);
		productionService.update(production);


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),production);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}











}
