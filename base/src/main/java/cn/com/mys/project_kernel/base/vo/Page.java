package cn.com.mys.project_kernel.base.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Page {
	
	private int pageNumber = 0;		// 当前页数
	private int pageSize = 10;		// 每页显示多少条记录
	private int totalSize;			// 一共多少记录

	/**
	 * 需要处理的属性
	 */
	private int pageTotal;			// 共有多少页
	@SuppressWarnings("unused")
	private boolean hasFirst;		// 是否有首页
	@SuppressWarnings("unused")
	private boolean hasPre;			// 是否有前一页
	@SuppressWarnings("unused")
	private boolean hasNext;		// 是否有下一页
	@SuppressWarnings("unused")
	private boolean hasLast;		// 是否有最后一页

	public Page() {}


	public Page(int pageNumber, int totalSize) {
		// 利用构造函数为变量赋值
		this.pageNumber = pageNumber;
		this.totalSize = totalSize;
	}

	public Page(int pageNumber, int pageSize, int totalSize) {
		// 利用构造函数为变量赋值
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.totalSize = totalSize;
	}

	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

	/**
	 * 需要处理的属性
	 */
	// 一共多少页
	public int getPageTotal() {
		if(getPageSize()==0) return 0;
		pageTotal = getTotalSize() / getPageSize();
		if (totalSize % pageSize != 0)
			pageTotal++;
		return pageTotal;
	}
	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}

	// 如果当前为第一页就没有首页
	@JsonIgnore
	public boolean isHasFirst() {
		return !(pageNumber==1);
	}
	public void setHasFirst(boolean hasFirst) {
		this.hasFirst = hasFirst;
	}

	// 如果不是最后一页就有尾页
	@JsonIgnore
	public boolean isHasLast() {
		return !(pageNumber == this.getPageTotal());
	}
	public void setHasLast(boolean hasLast) {
		this.hasLast = hasLast;
	}

	// 判断是否有前一页
	@JsonIgnore
	public boolean isHasPre() {
		return this.isHasFirst();
	}
	public void setHasPre(boolean hasPre) {
		this.hasPre = hasPre;
	}

	//判断是否有下一页
	@JsonIgnore
	public boolean isHasNext() {
		return isHasLast();
	}
	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	@Override
	public String toString() {
		return "Page{" +
				"pageNumber=" + pageNumber +
				", pageSize=" + pageSize +
				", totalSize=" + totalSize +
				", pageTotal=" + pageTotal +
				", hasFirst=" + hasFirst +
				", hasPre=" + hasPre +
				", hasNext=" + hasNext +
				", hasLast=" + hasLast +
				", startIndex=" + startIndex +
				'}';
	}



	/******************************************************************************/

	//查询的起始下标
	private int startIndex = 0;
	@JsonIgnore
	public int getStartIndex() {
		return (this.pageNumber)*this.pageSize;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
}
