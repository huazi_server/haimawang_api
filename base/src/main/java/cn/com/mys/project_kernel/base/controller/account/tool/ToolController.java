package cn.com.mys.project_kernel.base.controller.account.tool;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.entity.Teacher;
import cn.com.mys.project_kernel.base.entity.ext.StudentExt;
import cn.com.mys.project_kernel.base.entity.ext.TeacherExt;
import cn.com.mys.project_kernel.base.filter.wrapper.BodyReaderHttpServletRequestWrapper;
import cn.com.mys.project_kernel.base.service.StudentService;
import cn.com.mys.project_kernel.base.service.TbTestService;
import cn.com.mys.project_kernel.base.service.TeacherService;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import net.sf.json.JSONObject;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author mingjun_T
 * 工具
 *
 */
@Controller
@RequestMapping(value = "/account/tool")
public class ToolController {

	Log log = LogFactory.getLog(ToolController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private TeacherService teacherService;
	@Autowired
	private StudentService studentService;



	/***********************************************上传文件Start***************************************************/

	//上传文件
	@RequestMapping(value = "file/upload")
	public ResponseEntity<ResponseJson> file4Upload(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);


		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";


		if(StringUtils.isEmpty(teacherId) && StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"参数有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("loginToken", loginToken);
		map4Param.put("loginProductId", Constant.loginProductId);
		map4Param.put("loginProductKey", Constant.loginProductKey);

		if(StringUtils.isNotEmpty(teacherId)){
			map4Param.put("folderPath", "/teacher/file");//教师相关的文件保存的路径
		}
		if(StringUtils.isNotEmpty(studentId)){
			map4Param.put("folderPath", "/student/file");//学生相关的文件保存的路径
		}


		try {
			String result = "";

			BodyReaderHttpServletRequestWrapper bodyReaderRequest = (BodyReaderHttpServletRequestWrapper) request;
			MultipartHttpServletRequest multipartRequest = bodyReaderRequest.getMultipartRequest();
			String fileParamName = "upload";
			List<MultipartFile> files = multipartRequest.getFiles(fileParamName);//获取文件

			MultipartFile file = files.get(0);
			CommonsMultipartFile cf= (CommonsMultipartFile)file;
			DiskFileItem fi = (DiskFileItem)cf.getFileItem();

			//MultipartFile转换成File
			File f = fi.getStoreLocation();

			String uploadFileUrl = PropertiesUtils.getKeyValue("uploadFileUrl");

			result = FileUtils.post4Caller(uploadFileUrl,"upload",f,map4Param);

			//System.out.println(result);

			JSONObject jo = JSONObject.fromObject(result);
			boolean _result = false;
			Integer _code = 0;
			String _message = "";
			String _error = "";
			Object _data = null;

			if(jo.has("result")){
				_result = jo.getBoolean("result");
			}
			if(jo.has("code")){
				_code = jo.getInt("code");
			}
			if(jo.has("message")){
				_message = jo.getString("message");
			}
			if(jo.has("error")){
				_error = jo.getString("error");
			}
			if(jo.has("data")){
				_data = jo.getJSONObject("data");
			}

			ResponseJson responseJsonResult = new ResponseJson(_result,_code,_message,_error,ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_data);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		} catch (IOException e) {

			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"上传失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
	}






	/***********************************************上传文件End***************************************************/






}
