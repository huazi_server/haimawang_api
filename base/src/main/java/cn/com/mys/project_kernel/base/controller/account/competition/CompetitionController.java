package cn.com.mys.project_kernel.base.controller.account.competition;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.Competition;
import cn.com.mys.project_kernel.base.entity.CompetitionAward;
import cn.com.mys.project_kernel.base.entity.CompetitionProduction;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.filter.wrapper.BodyReaderHttpServletRequestWrapper;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 比赛
 *
 */
@Controller
@RequestMapping(value = "/account/competition")
public class CompetitionController {

	Log log = LogFactory.getLog(CompetitionController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private CompetitionService competitionService;
	@Autowired
	private CompetitionAwardService competitionAwardService;
	@Autowired
	private CompetitionUserService competitionUserService;
	@Autowired
	private CompetitionPlayerService competitionPlayerService;
	@Autowired
	private CompetitionProductionService competitionProductionService;
	@Autowired
	private CompetitionProductionVoteService competitionProductionVoteService;



	//获取用户信息
	@RequestMapping(value = "user/info")
	public ResponseEntity<ResponseJson> info4User(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");//比赛id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		Map<String, Object> m = VerifyLoginToken.verifyLoginToken4Map(loginToken);

		Integer accountId = (Integer) m.get("id");
		String phone = (String) m.get("phone");
		String nickname = (String) m.get("nickname");
		String avatarUrl = (String) m.get("avatarUrl");

		if(accountId==null || accountId==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		CompetitionUserExt competitionUserExt = competitionUserService.findByAccountId(accountId.toString());
		if(competitionUserExt==null || competitionUserExt.getId()==0){
			competitionUserExt = new CompetitionUserExt();

			competitionUserExt.setAccountId(accountId.longValue());
			competitionUserExt.setPhone(phone);
			competitionUserExt.setName(nickname);
			competitionUserExt.setAvatarUrl(avatarUrl);

			competitionUserExt.setState(0);
			competitionUserExt.setCreatedAt(new Date());
			competitionUserExt.setUpdatedAt(new Date());
			competitionUserExt.setDelFlag(0);
			competitionUserService.insert(competitionUserExt);
		}
		competitionUserExt.setAccountId(accountId.longValue());
		competitionUserExt.setPhone(phone);
		competitionUserExt.setName(nickname);
		competitionUserExt.setAvatarUrl(avatarUrl);

		competitionUserExt.setUpdatedAt(new Date());
		competitionUserService.update(competitionUserExt);


		if(StringUtils.isNotEmpty(competitionId)){
			Map<String, Object> map4Param = new HashMap<String, Object>();
			map4Param.put("delFlag", "0");
			map4Param.put("state", "1");//状态(0未完成、1已完成)
			map4Param.put("userId", competitionUserExt.getId().toString());
			map4Param.put("competitionId", competitionId);
			CompetitionPlayerExt competitionPlayerExt = competitionPlayerService.findObjectByCondition(map4Param);
			competitionUserExt.setCompetitionPlayerInfo(competitionPlayerExt);

			//指定比赛参加标识(1未参加、2已参加)
			Integer joinFlag = 1;
			//设置投票标识(1可以、2不可以)
			Integer voteFlag = 1;

			if(competitionPlayerExt!=null && competitionPlayerExt.getId()>0){
				joinFlag = 2;
			}

			map4Param = new HashMap<String, Object>();
			map4Param.put("delFlag", "0");
			map4Param.put("userId", competitionUserExt.getId().toString());
			map4Param.put("competitionId", competitionId);
			List<CompetitionProductionVoteExt> list = competitionProductionVoteService.findListByCondition4Today(map4Param);//查询当天投票列表
			if(list!=null && list.size()>0 && list.size()>=Constant.dayVoteCount4Competition.intValue()){
				voteFlag = 2;
			}

			competitionUserExt.setJoinFlag(joinFlag);
			competitionUserExt.setVoteFlag(voteFlag);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),competitionUserExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//上传文件
	@RequestMapping(value = "player/uploadFile",produces = "application/json;charset=utf-8")
	@ResponseBody
	public String uploadFile(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		CompetitionUserExt competitionUserExt = competitionUserService.findByAccountId(accountId);
		String userId = competitionUserExt!=null?String.valueOf(competitionUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",402);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","loginToken不存在或过期");

			return jo.toString();
		}
		if(StringUtils.isEmpty(userId)){
			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",100);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","用户不存在");
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("loginToken", loginToken);
		map4Param.put("folderPath", "/competition/user/file");//比赛用户相关的文件保存的路径

		map4Param.put("loginProductId", Constant.loginProductId);
		map4Param.put("loginProductKey", Constant.loginProductKey);

		try {
			String result = "";

			BodyReaderHttpServletRequestWrapper bodyReaderRequest = (BodyReaderHttpServletRequestWrapper) request;
			MultipartHttpServletRequest multipartRequest = bodyReaderRequest.getMultipartRequest();
			String fileParamName = "upload";
			List<MultipartFile> files = multipartRequest.getFiles(fileParamName);//获取文件

			MultipartFile file = files.get(0);

			//获取上传的文件的名称(包含后缀)
			String filename = file.getOriginalFilename();
			//获取名称(不含后缀)
			String fileName = filename.substring(0,filename.lastIndexOf("."));
			//获取后缀(扩展名)
			String contentType = filename.substring(filename.lastIndexOf(".")+1);
			map4Param.put("fileName", filename);
			map4Param.put("contentType", contentType);

			CommonsMultipartFile cf= (CommonsMultipartFile)file;
			DiskFileItem fi = (DiskFileItem)cf.getFileItem();

			//MultipartFile转换成File
			File f = fi.getStoreLocation();

			String uploadFileUrl = PropertiesUtils.getKeyValue("uploadFileUrl");

			result = FileUtils.post4Caller(uploadFileUrl,"upload",f,map4Param);

			//System.out.println(result);

			JSONObject jo = JSONObject.fromObject(result);

			//ResponseJson responseJsonResult = new ResponseJson(_result,_code,_message,_error,ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_data);
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			return jo.toString();
		} catch (IOException e) {

			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"上传失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code","100");
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","上传失败");

			return jo.toString();

		}
	}


	//验证
	@RequestMapping(value = "player/verify")
	public ResponseEntity<ResponseJson> verify4Player(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");//比赛id
		String phone = (String) paramMap.get("phone");//手机号

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		CompetitionUserExt competitionUserExt = competitionUserService.findByAccountId(accountId);
		String userId = competitionUserExt!=null?String.valueOf(competitionUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(userId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(competitionId) || StringUtils.isEmpty(phone)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、手机号不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(!ValidatorUtils.checkPhone(phone)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"手机号不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("userId", competitionUserExt.getId().toString());
		map4Param.put("competitionId", competitionId);
		CompetitionPlayerExt competitionPlayerExt = competitionPlayerService.findObjectByCondition(map4Param);
		if(competitionPlayerExt!=null && competitionPlayerExt.getId()>0){
			Map<String, Object> m = new HashMap<>();
			m.put("type", "1");//1已参加比赛、2验证码有误或已失效
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"您已经参赛",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		map4Param.put("userId", "");
		map4Param.put("phone", phone);
		competitionPlayerExt = competitionPlayerService.findObjectByCondition(map4Param);
		if(competitionPlayerExt!=null && competitionPlayerExt.getId()>0){
			Map<String, Object> m = new HashMap<>();
			m.put("type", "1");//1已参加比赛、2验证码有误或已失效
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"您已经参赛",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		map4Param.put("userId", competitionUserExt.getId().toString());
		map4Param.put("phone", "");

		//发送验证码
		String lastAuthCode = "";
		try{
			JSONObject obj = new JSONObject();
			obj.put("productId", Constant.loginProductId);
			obj.put("productKey", Constant.loginProductKey);
			obj.put("verifyType", "HuodongAction");
			obj.put("verifyPhone", phone);
			JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("verifyRequestUrl"), obj);
			log.debug("--"+jo.toString());
			if(jo!=null && jo.getBoolean("result") && jo.getJSONObject("data")!=null){
				String code = jo.getJSONObject("data").getString("code");//验证码
				Integer validity = jo.getJSONObject("data").getInt("validity");//有效时间(秒)
				if(StringUtils.isEmpty(code)){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"验证码获取有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
				lastAuthCode = code;
			}else{
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"验证码获取有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}catch (Exception e){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"验证码获取有误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		map4Param.put("state", "0");//状态(0未完成、1已完成)
		CompetitionPlayerExt playerExt = competitionPlayerService.findObjectByCondition(map4Param);
		if(playerExt==null || playerExt.getId()==0){
			playerExt = new CompetitionPlayerExt();
			playerExt.setCompetitionId(Long.parseLong(competitionId));
			playerExt.setUserId(competitionUserExt.getId());
			playerExt.setPhone(phone);
			playerExt.setLastAuthCode(lastAuthCode);
			playerExt.setLastAuthTime(new Date());

			playerExt.setState(0);
			playerExt.setCreatedAt(new Date());
			playerExt.setUpdatedAt(new Date());
			playerExt.setDelFlag(0);
			competitionPlayerService.insert(playerExt);
		}else{
			playerExt.setCompetitionId(Long.parseLong(competitionId));
			playerExt.setUserId(competitionUserExt.getId());
			playerExt.setPhone(phone);
			playerExt.setLastAuthCode(lastAuthCode);
			playerExt.setLastAuthTime(new Date());

			playerExt.setState(0);
			playerExt.setCreatedAt(new Date());
			playerExt.setUpdatedAt(new Date());
			playerExt.setDelFlag(0);
			competitionPlayerService.update(playerExt);
		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),playerExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//保存
	@RequestMapping(value = "player/save")
	public ResponseEntity<ResponseJson> save4Player(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		CompetitionUserExt competitionUserExt = competitionUserService.findByAccountId(accountId);
		String userId = competitionUserExt!=null?String.valueOf(competitionUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(userId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		String competitionId = (String) paramMap.get("competitionId");//比赛id
		String phone = (String) paramMap.get("phone");
		String authCode = (String) paramMap.get("authCode");//验证码
		String name = (String) paramMap.get("name");
		String age = (String) paramMap.get("age");
		String avatarUrl = (String) paramMap.get("avatarUrl");

		if(StringUtils.isEmpty(competitionId) || StringUtils.isEmpty(phone) || StringUtils.isEmpty(authCode)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、手机号、验证码不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(!ValidatorUtils.checkPhone(phone)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"手机号不正确",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(name) || StringUtils.isEmpty(age) || StringUtils.isEmpty(avatarUrl)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"姓名、年龄、头像不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("userId", competitionUserExt.getId().toString());
		map4Param.put("competitionId", competitionId);
		CompetitionPlayerExt competitionPlayerExt = competitionPlayerService.findObjectByCondition(map4Param);
		if(competitionPlayerExt!=null && competitionPlayerExt.getId()>0){
			Map<String, Object> m = new HashMap<>();
			m.put("type", "1");//1已参加比赛、2验证码有误或已失效
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"您已经参赛",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		map4Param.put("userId", "");
		map4Param.put("phone", phone);
		competitionPlayerExt = competitionPlayerService.findObjectByCondition(map4Param);
		if(competitionPlayerExt!=null && competitionPlayerExt.getId()>0){
			Map<String, Object> m = new HashMap<>();
			m.put("type", "1");//1已参加比赛、2验证码有误或已失效
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"您已经参赛",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		map4Param.put("userId", competitionUserExt.getId().toString());
		map4Param.put("phone", "");

		map4Param.put("state", "0");//状态(0未完成、1已完成)
		CompetitionPlayerExt playerExt = competitionPlayerService.findObjectByCondition(map4Param);
		if(playerExt==null || playerExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此手机号未曾获取验证码",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		//验证验证码
		String lastAuthCode = playerExt.getLastAuthCode();
		Date lastAuthTime = playerExt.getLastAuthTime();
		if(StringUtils.isEmpty(lastAuthCode) || DateUtils.getInterval(new Date(), lastAuthTime)>=Constant.authCodeValidity4Competition.intValue() || !authCode.equals(lastAuthCode)){
			Map<String, Object> m = new HashMap<>();
			m.put("type", "2");//1已参加比赛、2验证码有误或已失效
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"验证码有误或失效",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		playerExt.setPhone(phone);
		playerExt.setName(name);
		playerExt.setAge(Integer.parseInt(age));
		playerExt.setAvatarUrl(avatarUrl);
		playerExt.setCommitTime(new Date());
		playerExt.setState(1);//状态(0未完成、1已完成)
		playerExt.setCreatedAt(new Date());
		playerExt.setUpdatedAt(new Date());
		playerExt.setDelFlag(0);
		competitionPlayerService.update(playerExt);

		//设置编号
		Integer number = 0;
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("delFlag", "0");
		m.put("state", "1");//状态(0未完成、1已完成)
		m.put("competitionId", competitionId);
		m.put("playerId", playerExt.getId());
		CompetitionPlayerExt lastPlayer = competitionPlayerService.findLastObjectByCondition(m);
		Integer lastNumber = (lastPlayer!=null && lastPlayer.getNumber()!=null && lastPlayer.getNumber()>0)?lastPlayer.getNumber():0;
		number = lastNumber+1;
		playerExt.setNumber(number);
		competitionPlayerService.update(playerExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),playerExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//获取比赛的参赛者列表
	@RequestMapping(value = "player/list")
	public ResponseEntity<ResponseJson> list4Player(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");//比赛id
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		CompetitionUserExt competitionUserExt = competitionUserService.findByAccountId(accountId);
		String userId = competitionUserExt!=null?String.valueOf(competitionUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(userId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(competitionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("competitionId", competitionId);
		map4Param.put("page", page);
		Integer count = competitionPlayerService.findCountByCondition(map4Param);
		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}
		List<CompetitionPlayerExt> list = competitionPlayerService.findListByCondition(map4Param);
		for(CompetitionPlayerExt competitionPlayerExt : list){
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("competitionId", competitionId);
			m.put("playerId", competitionPlayerExt.getId().toString());
			CompetitionProductionExt competitionProductionExt = competitionProductionService.findObjectByCondition(m);
			competitionPlayerExt.setCompetitionProductionInfo(competitionProductionExt);
		}

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//获取比赛信息
	@RequestMapping(value = "info")
	public ResponseEntity<ResponseJson> info(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");//比赛id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		CompetitionUserExt competitionUserExt = competitionUserService.findByAccountId(accountId);
		String userId = competitionUserExt!=null?String.valueOf(competitionUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(userId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(competitionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),competitionExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//获取比赛的作品详情
	@RequestMapping(value = "production/info")
	public ResponseEntity<ResponseJson> info4Production(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");//比赛id
		String productionId = (String) paramMap.get("productionId");//作品id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		CompetitionUserExt competitionUserExt = competitionUserService.findByAccountId(accountId);
		String userId = competitionUserExt!=null?String.valueOf(competitionUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(userId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(competitionId) || StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		CompetitionProductionExt competitionProductionExt = competitionProductionService.findById(Long.parseLong(productionId));

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("userId", competitionUserExt.getId().toString());
		map4Param.put("competitionId", competitionId);
		CompetitionPlayerExt competitionPlayerExt = competitionPlayerService.findObjectByCondition(map4Param);

		//设置投票标识(1可以、2不可以)
		Integer voteFlag = 1;
		map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("userId", competitionUserExt.getId().toString());
		map4Param.put("competitionId", competitionId);
		List<CompetitionProductionVoteExt> list = competitionProductionVoteService.findListByCondition4Today(map4Param);//查询当天投票列表
		if(list!=null && list.size()>0 && list.size()>=Constant.dayVoteCount4Competition.intValue()){
			voteFlag = 2;
		}
		competitionUserExt.setVoteFlag(voteFlag);

		Map<String, Object> m = new HashMap<>();
		m.put("competitionUserInfo", competitionUserExt);
		m.put("competitionProductionInfo", competitionProductionExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//给作品投票
	@RequestMapping(value = "production/vote")
	public ResponseEntity<ResponseJson> vote4Production(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");//比赛id
		String productionId = (String) paramMap.get("productionId");//作品id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		CompetitionUserExt competitionUserExt = competitionUserService.findByAccountId(accountId);
		String userId = competitionUserExt!=null?String.valueOf(competitionUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(userId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(competitionId) || StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(competitionExt!=null && competitionExt.getState()!=4){//状态(0初始化、1报名中、2培训中、3进行中、4投票中、5投票截止、6公布中)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此比赛不能投票",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		CompetitionProductionExt competitionProductionExt = competitionProductionService.findById(Long.parseLong(productionId));
		if(competitionProductionExt==null || competitionProductionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		CompetitionPlayerExt competitionPlayerExt = competitionPlayerService.findById(competitionProductionExt.getPlayerId());

		//设置投票标识(1可以、2不可以)
		Integer voteFlag = 1;
		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("userId", competitionUserExt.getId().toString());
		map4Param.put("competitionId", competitionId);
		List<CompetitionProductionVoteExt> list = competitionProductionVoteService.findListByCondition4Today(map4Param);//查询当天投票列表
		if(list!=null && list.size()>0 && list.size()>=Constant.dayVoteCount4Competition.intValue()){
			voteFlag = 2;
		}
		competitionUserExt.setVoteFlag(voteFlag);

		if(voteFlag==2){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"当日投票机会用完",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		CompetitionProductionVoteExt competitionProductionVoteExt = new CompetitionProductionVoteExt();
		competitionProductionVoteExt.setCompetitionId(Long.parseLong(competitionId));
		competitionProductionVoteExt.setProductionId(Long.parseLong(productionId));
		competitionProductionVoteExt.setUserId(competitionUserExt.getId());

		competitionProductionVoteExt.setState(0);
		competitionProductionVoteExt.setCreatedAt(new Date());
		competitionProductionVoteExt.setUpdatedAt(new Date());
		competitionProductionVoteExt.setDelFlag(0);
		competitionProductionVoteService.insert(competitionProductionVoteExt);

		Integer voteCount = (competitionProductionExt.getVoteCount()==null||competitionProductionExt.getVoteCount()<=0)?0:competitionProductionExt.getVoteCount();
		competitionProductionExt.setVoteCount(voteCount+1);
		competitionProductionService.update(competitionProductionExt);

		//重新查询
		list = competitionProductionVoteService.findListByCondition4Today(map4Param);//查询当天投票列表
		if(list!=null && list.size()>0 && list.size()>=Constant.dayVoteCount4Competition.intValue()){
			voteFlag = 2;
		}
		competitionUserExt.setVoteFlag(voteFlag);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),competitionUserExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//获取比赛的投票排行榜列表
	@RequestMapping(value = "vote/list")
	public ResponseEntity<ResponseJson> list4Vote(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");//比赛id
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		CompetitionUserExt competitionUserExt = competitionUserService.findByAccountId(accountId);
		String userId = competitionUserExt!=null?String.valueOf(competitionUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(userId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(competitionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("competitionId", competitionId);
		map4Param.put("page", page);
		Integer count = competitionPlayerService.findCountByCondition4Vote(map4Param);
		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}
		List<CompetitionPlayerExt> list = competitionPlayerService.findListByCondition4Vote(map4Param);
		for(CompetitionPlayerExt competitionPlayerExt : list){
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("competitionId", competitionId);
			m.put("playerId", competitionPlayerExt.getId().toString());
			CompetitionProductionExt competitionProductionExt = competitionProductionService.findObjectByCondition(m);
			competitionPlayerExt.setCompetitionProductionInfo(competitionProductionExt);
		}

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//获取比赛的优胜者列表
	@RequestMapping(value = "winner/list")
	public ResponseEntity<ResponseJson> list4Winner(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String competitionId = (String) paramMap.get("competitionId");//比赛id
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		CompetitionUserExt competitionUserExt = competitionUserService.findByAccountId(accountId);
		String userId = competitionUserExt!=null?String.valueOf(competitionUserExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(userId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(competitionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		CompetitionExt competitionExt = competitionService.findById(Long.parseLong(competitionId));
		if(competitionExt==null || competitionExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		List<CompetitionPlayerExt> list = new ArrayList<>();

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("competitionId", competitionId);
		List<CompetitionAwardExt> awardList = competitionAwardService.findListByCondition(map4Param);
		Integer startIndex = 0;
		for(CompetitionAwardExt awardInfo : awardList){
			Integer userCount = (awardInfo.getUserCount()!=null&&awardInfo.getUserCount()>0)?awardInfo.getUserCount():0;
			if(userCount==null || userCount==0){
				continue;
			}
			page.setStartIndex(startIndex);
			page.setPageSize(userCount);

			map4Param = new HashMap<String, Object>();
			map4Param.put("delFlag", "0");
			map4Param.put("state", "1");//状态(0未完成、1已完成)
			map4Param.put("competitionId", competitionId);
			map4Param.put("page", page);
			Integer count = competitionPlayerService.findCountByCondition4Vote(map4Param);
			List<CompetitionPlayerExt> playerList = competitionPlayerService.findListByCondition4Vote(map4Param);

			for(int i=0;i<playerList.size();i++){
				CompetitionPlayerExt competitionPlayerExt = playerList.get(i);
				Map<String, Object> m = new HashMap<>();
				m.put("delFlag", "0");
				m.put("competitionId", competitionId);
				m.put("playerId", competitionPlayerExt.getId().toString());
				CompetitionProductionExt competitionProductionExt = competitionProductionService.findObjectByCondition(m);
				if(competitionProductionExt!=null && competitionProductionExt.getId()>0){
					competitionProductionExt.setAwardId(awardInfo.getId());//处理优胜者的奖励项
					competitionProductionExt.setCompetitionAwardInfo(awardInfo);
				}
				competitionPlayerExt.setCompetitionProductionInfo(competitionProductionExt);
			}

			startIndex = startIndex + userCount;//下一次查询，从前一次的userCount开始
			list.addAll(playerList);
		}

		page.setTotalSize(list.size());
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}













}
