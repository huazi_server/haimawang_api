package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.UserInfoGatherMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.UserInfoGatherMapperExt;
import cn.com.mys.project_kernel.base.entity.UserInfoGather;
import cn.com.mys.project_kernel.base.entity.ext.UserInfoGatherExt;
import cn.com.mys.project_kernel.base.service.UserInfoGatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class UserInfoGatherServiceImpl implements UserInfoGatherService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public UserInfoGatherMapper userInfoGatherMapper;

	@Autowired
	public UserInfoGatherMapperExt userInfoGatherMapperExt;


	@Override
	public Integer insert(UserInfoGather userInfoGather) {
		return userInfoGatherMapper.insertSelective(userInfoGather);
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return userInfoGatherMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<UserInfoGatherExt> findListByCondition(Map<String, Object> map4Param) {
		return userInfoGatherMapperExt.findListByCondition(map4Param);
	}
}
