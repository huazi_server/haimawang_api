package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.TbTest;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface TbTestMapperExt {

    int findTbTestListCountByCondition(Map<String, Object> map);

    List<TbTest> findTbTestListPageByCondition(Map<String, Object> map);


}