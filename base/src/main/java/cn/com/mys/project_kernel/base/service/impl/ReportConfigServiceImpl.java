package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ReportConfigMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ReportConfigMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ReportConfigExt;
import cn.com.mys.project_kernel.base.service.ReportConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ReportConfigServiceImpl implements ReportConfigService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ReportConfigMapper reportConfigMapper;

	@Autowired
	public ReportConfigMapperExt reportConfigMapperExt;


	@Override
	public List<ReportConfigExt> findListByCondition(Map<String, Object> map) {
		return reportConfigMapperExt.findListByCondition(map);
	}
}
