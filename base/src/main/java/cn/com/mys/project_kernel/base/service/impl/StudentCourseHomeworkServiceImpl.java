package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CourseLessonMapper;
import cn.com.mys.project_kernel.base.dao.CourseMapper;
import cn.com.mys.project_kernel.base.dao.StudentCourseHomeworkMapper;
import cn.com.mys.project_kernel.base.dao.StudentMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CourseLessonMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.CourseLessonVideoPointMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.StudentCourseHomeworkMapperExt;
import cn.com.mys.project_kernel.base.entity.*;
import cn.com.mys.project_kernel.base.entity.ext.CourseLessonExt;
import cn.com.mys.project_kernel.base.entity.ext.ProductionExt;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseHomeworkExt;
import cn.com.mys.project_kernel.base.service.StudentCourseHomeworkService;
import cn.com.mys.project_kernel.base.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class StudentCourseHomeworkServiceImpl implements StudentCourseHomeworkService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public StudentCourseHomeworkMapper studentCourseHomeworkMapper;

	@Autowired
	public StudentCourseHomeworkMapperExt studentCourseHomeworkMapperExt;

	@Autowired
	public CourseLessonMapper courseLessonMapper;
	@Autowired
	public CourseLessonMapperExt courseLessonMapperExt;
	@Autowired
	public StudentMapper studentMapper;
	@Autowired
	public CourseMapper courseMapper;
	@Autowired
	public CourseLessonVideoPointMapperExt courseLessonVideoPointMapperExt;


	//设置课时名称和锁标识
	private void setCourseLesson(StudentCourseHomeworkExt studentCourseHomeworkExt){
		Long courseLessonId = studentCourseHomeworkExt.getCourseLessonId();
		if(courseLessonId!=null && courseLessonId>0){
			CourseLesson courseLesson = courseLessonMapper.selectByPrimaryKey(courseLessonId);
			studentCourseHomeworkExt.setCourseLessonTitle(courseLesson.getTitle());
			studentCourseHomeworkExt.setCourseLessonName(courseLesson.getName());
			studentCourseHomeworkExt.setCourseLessonNumber(courseLesson.getNumber());
			String coverUrl = studentCourseHomeworkExt.getCoverUrl();
			studentCourseHomeworkExt.setCoverUrl(StringUtils.isEmpty(coverUrl)?courseLesson.getCoverUrl():coverUrl);

			Date startDate = studentCourseHomeworkExt.getStartAt();
			Date endDate = studentCourseHomeworkExt.getEndAt();
			Integer lockFlag = 1;//锁标识(1关闭、2打开)
			/*if(startDate.getTime()<=new Date().getTime() && new Date().getTime()<=endDate.getTime()){
				lockFlag = 2;
			}*/
			if(startDate.getTime()<=new Date().getTime()){
				lockFlag = 2;
			}
			studentCourseHomeworkExt.setCourseLessonLockFlag(lockFlag);

			Course course = courseMapper.selectByPrimaryKey(studentCourseHomeworkExt.getCourseId());
			studentCourseHomeworkExt.setCourseName((course!=null&&course.getId()>0&&StringUtils.isNotEmpty(course.getName()))?course.getName():"");

			Map<String, Object> m = new HashMap<>();
			m.put("courseLessonId", courseLessonId);
			CourseLessonExt courseLessonExt = courseLessonMapperExt.findObjectByCondition(m);
			studentCourseHomeworkExt.setCourseLessonInfo(courseLessonExt);
		}
	}
	//设置作业的学生的名称、头像
	private void setNameAndAvatarUrl(StudentCourseHomeworkExt studentCourseHomeworkExt){
		Long studentId = studentCourseHomeworkExt.getStudentId();//学生id
		Student student = studentMapper.selectByPrimaryKey(studentId);
		studentCourseHomeworkExt.setStudentName(student.getName());
		studentCourseHomeworkExt.setStudentAvatarUrl(student.getAvatarUrl());
	}
	//设置默认草稿
	private void setDefaultDraft(StudentCourseHomeworkExt studentCourseHomeworkExt){
		Integer state = studentCourseHomeworkExt.getState();//状态(1未保存、2未提交、3已提交)
		String draft = studentCourseHomeworkExt.getDraft();
		if(state==1 && StringUtils.isEmpty(draft)){
			Long courseLessonId = studentCourseHomeworkExt.getCourseLessonId();
			if(courseLessonId!=null && courseLessonId>0){
				CourseLesson courseLesson = courseLessonMapper.selectByPrimaryKey(courseLessonId);
				String sb3Url = courseLesson.getSb3Url();
				if(StringUtils.isNotEmpty(sb3Url)){
					studentCourseHomeworkExt.setDraft(sb3Url);
				}
			}
		}
	}



	@Override
	public StudentCourseHomeworkExt findObjectByCondition(Map<String, Object> map4Param) {
		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkMapperExt.findObjectByCondition(map4Param);
		if(studentCourseHomeworkExt!=null && studentCourseHomeworkExt.getId()>0){
			setCourseLesson(studentCourseHomeworkExt);
			setNameAndAvatarUrl(studentCourseHomeworkExt);
		}
		return studentCourseHomeworkExt;
	}

	@Override
	public Integer update(StudentCourseHomeworkExt studentCourseHomeworkExt) {
		return studentCourseHomeworkMapper.updateByPrimaryKeySelective(studentCourseHomeworkExt);
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return studentCourseHomeworkMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<StudentCourseHomeworkExt> findListByCondition(Map<String, Object> map4Param) {
		List<StudentCourseHomeworkExt> list = studentCourseHomeworkMapperExt.findListByCondition(map4Param);
		for(StudentCourseHomeworkExt studentCourseHomeworkExt : list){
			if(studentCourseHomeworkExt!=null && studentCourseHomeworkExt.getId()>0){
				setCourseLesson(studentCourseHomeworkExt);
				setNameAndAvatarUrl(studentCourseHomeworkExt);
			}
		}
		return list;
	}

	@Override
	public List<StudentCourseHomeworkExt> findListByCondition4CourseLesson(Map<String, Object> map4Param) {
		List<StudentCourseHomeworkExt> list = studentCourseHomeworkMapperExt.findListByCondition4CourseLesson(map4Param);
		for(StudentCourseHomeworkExt studentCourseHomeworkExt : list){
			if(studentCourseHomeworkExt!=null && studentCourseHomeworkExt.getId()>0){
				setCourseLesson(studentCourseHomeworkExt);
				setNameAndAvatarUrl(studentCourseHomeworkExt);
			}
		}
		return list;
	}


	@Override
	public Integer findAllCountByCondition(Map<String, Object> m) {
		return studentCourseHomeworkMapperExt.findAllCountByCondition(m);
	}

	@Override
	public Integer findCommitCountByCondition(Map<String, Object> m) {
		return studentCourseHomeworkMapperExt.findCommitCountByCondition(m);
	}

	@Override
	public Integer findOnlineCountByCondition(Map<String, Object> m) {
		return studentCourseHomeworkMapperExt.findOnlineCountByCondition(m);
	}

	@Override
	public Integer update4OnlineFlag(String onlineFlag) {
		return studentCourseHomeworkMapperExt.update4OnlineFlag(onlineFlag);
	}

	@Override
	public StudentCourseHomeworkExt findById(long id) {
		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkMapperExt.findById(id);
		if(studentCourseHomeworkExt!=null && studentCourseHomeworkExt.getId()>0){
			setCourseLesson(studentCourseHomeworkExt);
			setNameAndAvatarUrl(studentCourseHomeworkExt);
		}
		return studentCourseHomeworkExt;
	}

	@Override
	public Integer insert(StudentCourseHomeworkExt sch) {
		return studentCourseHomeworkMapper.insertSelective(sch);
	}




}
