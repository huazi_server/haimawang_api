package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.dao.ProductionMapper;
import cn.com.mys.project_kernel.base.dao.ReportMapper;
import cn.com.mys.project_kernel.base.dao.StudentMapper;
import cn.com.mys.project_kernel.base.dao.TeacherMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.*;
import cn.com.mys.project_kernel.base.entity.Production;
import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.Teacher;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.ProductionService;
import cn.com.mys.project_kernel.base.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ProductionServiceImpl implements ProductionService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ProductionMapper productionMapper;

	@Autowired
	public ProductionMapperExt productionMapperExt;
	@Autowired
	public StudentProductionCollectMapperExt studentProductionCollectMapperExt;
	@Autowired
	public StudentProductionLikeMapperExt studentProductionLikeMapperExt;

	@Autowired
	public ProductionTagMapperExt productionTagMapperExt;
	@Autowired
	public ReportMapperExt reportMapperExt;

	@Autowired
	public TeacherMapper teacherMapper;
	@Autowired
	public StudentMapper studentMapper;


	//设置收藏标识、点赞标识(1未收藏、2已收藏;1未点赞、2已点赞)
	private void setCollectFlagAndLikeFlag(ProductionExt productionExt, String studentId){
		Map<String, Object> m = new HashMap<>();
		m.put("productionId", productionExt.getId());
		m.put("studentId", studentId);
		m.put("delFlag","0");
		StudentProductionCollectExt studentProductionCollectExt = studentProductionCollectMapperExt.findObjectByCondition(m);
		if(studentProductionCollectExt!=null && studentProductionCollectExt.getId()>0){
			productionExt.setCollectFlag(2);
		}else{
			productionExt.setCollectFlag(1);
		}
		StudentProductionLikeExt studentProductionLikeExt = studentProductionLikeMapperExt.findObjectByCondition(m);
		if(studentProductionLikeExt!=null && studentProductionLikeExt.getId()>0){
			productionExt.setLikeFlag(2);
		}else{
			productionExt.setLikeFlag(1);
		}

	}

	//设置作品的作者的名称、头像
	private void setNameAndAvatarUrl(ProductionExt productionExt){
		Integer sourceType = productionExt.getSourceType();//来源类型(1系统、2教师、3学生)
		Long id = productionExt.getPersonId();//作者id
		if(sourceType==1){
			//do nothing
		}
		if(sourceType==2){
			Teacher teacher = teacherMapper.selectByPrimaryKey(id);
			productionExt.setAuthorName(teacher.getName());
			productionExt.setAuthorAvatarUrl(teacher.getAvatarUrl());
		}
		if(sourceType==3){
			Student student = studentMapper.selectByPrimaryKey(id);
			productionExt.setAuthorName(student.getName());
			productionExt.setAuthorAvatarUrl(student.getAvatarUrl());
		}


		if(StringUtils.isEmpty(productionExt.getAuthorName())){
			productionExt.setAuthorName(Constant.authorName4Production);
		}
		if(StringUtils.isEmpty(productionExt.getAuthorAvatarUrl())){
			productionExt.setAuthorAvatarUrl(Constant.authorAvatarUrl4Production);
		}

	}

	//设置作品的标签
	private void setTags(ProductionExt productionExt){
		String tagIds = productionExt.getTagIds();

		String tagNames = "";
		List<ProductionTagExt> productionTagInfoList = new ArrayList<ProductionTagExt>();
		if(StringUtils.isNotEmpty(tagIds)){
			productionTagInfoList = productionTagMapperExt.findListByTagIds(tagIds);//作品标签列表

			for(ProductionTagExt tag : productionTagInfoList){
				tagNames = tagNames + tag.getName() + ",";
			}
			tagNames = StringUtils.isNotEmpty(tagNames)?tagNames.substring(0,tagNames.length()-1):"";
		}
		productionExt.setProductionTagInfoList(productionTagInfoList==null?new ArrayList<ProductionTagExt>():productionTagInfoList);
		productionExt.setTagNames(StringUtils.isEmpty(tagNames)?"":tagNames);
	}


	//设置举报标识(1未被举报、2已被举报)
	private void setReportFlag(ProductionExt productionExt){
		Map<String, Object> m = new HashMap<>();
		m.put("defendantId", productionExt.getId().toString());//作品、评论、回复id
		m.put("type", "1");//类型(1作品、2评论、3回复)
		m.put("state","0,1");//状态(0待处理、1处理中 2已处理)
		m.put("delFlag","0");
		List<ReportExt> reportExtList = reportMapperExt.findListByCondition(m);
		if(reportExtList!=null && reportExtList.size()>0){
			productionExt.setReportFlag(2);
		}else{
			productionExt.setReportFlag(1);
		}

	}

	//设置所属标识(1不属于自己、2属于自己)
	private void setSelfFlag(ProductionExt productionExt, String studentId){
		Integer sourceType = productionExt.getSourceType();//来源类型(1系统、2教师、3学生)
		if(sourceType==3 && StringUtils.isNotEmpty(studentId) && Long.parseLong(studentId)==productionExt.getPersonId()){
			productionExt.setSelfFlag(2);
		}else{
			productionExt.setSelfFlag(1);
		}
	}





	@Override
	public Integer findCountByCondition4All(Map<String, Object> map4Param) {
		return productionMapperExt.findCountByCondition4All(map4Param);
	}

	@Override
	public List<ProductionExt> findListByCondition4All(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<ProductionExt> list = productionMapperExt.findListByCondition4All(map4Param);
		for(ProductionExt productionExt : list){
			/*if(StringUtils.isNotEmpty(studentId)){
				setCollectFlagAndLikeFlag(productionExt, studentId);
			}*/
			setCollectFlagAndLikeFlag(productionExt, studentId);
			setNameAndAvatarUrl(productionExt);
			setTags(productionExt);
			setReportFlag(productionExt);
			setSelfFlag(productionExt, studentId);
		}
		return list;
	}

	@Override
	public ProductionExt findObjectByCondition(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		ProductionExt productionExt = productionMapperExt.findObjectByCondition(map4Param);
		if(productionExt!=null){
			/*if(StringUtils.isNotEmpty(studentId)){
				setCollectFlagAndLikeFlag(productionExt, studentId);
			}*/
			setCollectFlagAndLikeFlag(productionExt, studentId);
			setNameAndAvatarUrl(productionExt);
			setTags(productionExt);
			setReportFlag(productionExt);
			setSelfFlag(productionExt, studentId);
		}
		return productionExt;
	}

	@Override
	public Integer insert(ProductionExt productionExt) {
		return productionMapper.insertSelective(productionExt);
	}

	@Override
	public Integer update(Production production) {
		return productionMapper.updateByPrimaryKeySelective(production);
	}

	@Override
	public Production findById(long id) {
		return productionMapper.selectByPrimaryKey(id);
	}


	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return productionMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ProductionExt> findListByCondition(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<ProductionExt> list = productionMapperExt.findListByCondition(map4Param);
		for(ProductionExt productionExt : list){
			/*if(StringUtils.isNotEmpty(studentId)){
				setCollectFlagAndLikeFlag(productionExt, studentId);
			}*/
			setCollectFlagAndLikeFlag(productionExt, studentId);
			setNameAndAvatarUrl(productionExt);
			setTags(productionExt);
			setReportFlag(productionExt);
			setSelfFlag(productionExt, studentId);
		}
		return list;
	}

	@Override
	public Integer findCountByCondition4Collect(Map<String, Object> map4Param) {
		return productionMapperExt.findCountByCondition4Collect(map4Param);
	}

	@Override
	public List<ProductionExt> findListByCondition4Collect(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<ProductionExt> list = productionMapperExt.findListByCondition4Collect(map4Param);
		for(ProductionExt productionExt : list){
			/*if(StringUtils.isNotEmpty(studentId)){
				setCollectFlagAndLikeFlag(productionExt, studentId);
			}*/
			setCollectFlagAndLikeFlag(productionExt, studentId);
			setNameAndAvatarUrl(productionExt);
			setTags(productionExt);
			setReportFlag(productionExt);
			setSelfFlag(productionExt, studentId);
		}
		return list;
	}

	@Override
	public Integer findCountByCondition4Like(Map<String, Object> map4Param) {
		return productionMapperExt.findCountByCondition4Like(map4Param);
	}

	@Override
	public List<ProductionExt> findListByCondition4Like(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<ProductionExt> list = productionMapperExt.findListByCondition4Like(map4Param);
		for(ProductionExt productionExt : list){
			/*if(StringUtils.isNotEmpty(studentId)){
				setCollectFlagAndLikeFlag(productionExt, studentId);
			}*/
			setCollectFlagAndLikeFlag(productionExt, studentId);
			setNameAndAvatarUrl(productionExt);
			setTags(productionExt);
			setReportFlag(productionExt);
			setSelfFlag(productionExt, studentId);
		}
		return list;
	}






}
