package cn.com.mys.project_kernel.base.controller;


import cn.com.mys.project_kernel.base.entity.TbTest;
import cn.com.mys.project_kernel.base.service.TbTestService;
import cn.com.mys.project_kernel.base.util.ParamUtils;
import cn.com.mys.project_kernel.base.util.ValidateCodeUtils;
import cn.com.mys.project_kernel.base.vo.ParamMap;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 *
 * @author mingjun_T
 * 测试
 *
 */
@Controller
@RequestMapping(value = "/main")
public class MainController {

	Log log = LogFactory.getLog(MainController.class);

	@Autowired
	private TbTestService tbTestService;

	//获取验证码
	@RequestMapping(value = "/validateCode", method = { RequestMethod.GET, RequestMethod.POST })
	public void validateCode(HttpServletRequest request, Model model, HttpServletResponse response) {

		// 设置响应的类型格式为图片格式
		response.setContentType("image/jpeg");
		//禁止图像缓存
		response.setCharacterEncoding("utf-8");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);

		HttpSession session = request.getSession();

		ValidateCodeUtils vCode = new ValidateCodeUtils(110,40,4,100);
		session.setAttribute("validateCode", vCode.getCode());
		try {
			vCode.write(response.getOutputStream());
			//刷新数据到页面
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//保存menuId
	@RequestMapping(value = "/saveMenuId", method = { RequestMethod.GET, RequestMethod.POST })
	public void saveMenuId(HttpServletRequest request, Model model, HttpServletResponse response) {
		log.debug("menuId========================================"+request.getParameter("menuId"));
		request.getSession().setAttribute("menuId",request.getParameter("menuId"));
	}

	//登录
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public String login(HttpServletRequest request, Model model) {

		JSONObject jo = new JSONObject();
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);

		String validateCode = (String)request.getSession().getAttribute("validateCode");
		if(map.get("validateCode")==null || !validateCode.equalsIgnoreCase((String)map.get("validateCode"))){
			jo.put("status", "FAIL");
			jo.put("message", "登录有误，请输入正确的验证码。");
		}else{
			TbTest tbTest = tbTestService.findTbTestByCondition(map);
			if(tbTest==null){
				jo.put("status", "FAIL");
				jo.put("message", "登录有误，请输入正确的用户名或密码。");
			}else{
				jo.put("status", "SUCCESS");
				jo.put("message", "登录成功。");
				request.getSession().setAttribute("isLogin", true);//设置用户已经登录
				request.getSession().setAttribute("tbTest", tbTest);
			}
		}

		return jo.toString();
	}

	//首页
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest request, Model model) {

		ModelAndView mav = new ModelAndView();
		mav.setViewName("main/home");
		return mav;
	}

	//退出登录
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request, Model model) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("main/login");
		return mav;
	}



	//index
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, Model model) {

		ModelAndView mav = new ModelAndView();
		mav.setViewName("main/index");
		return mav;
	}


}
