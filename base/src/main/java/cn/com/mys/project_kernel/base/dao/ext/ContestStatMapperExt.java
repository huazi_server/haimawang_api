package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestStatExt;
import org.springframework.stereotype.Repository;



@Repository
public interface ContestStatMapperExt {

    ContestStatExt findByContestId(Long contestId);
}