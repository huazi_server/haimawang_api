package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ActivityFreeMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ActivityFreeMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ActivityFreeExt;
import cn.com.mys.project_kernel.base.service.ActivityFreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ActivityFreeServiceImpl implements ActivityFreeService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ActivityFreeMapper activityFreeMapper;

	@Autowired
	public ActivityFreeMapperExt activityFreeMapperExt;


	@Override
	public ActivityFreeExt findObjectByCondition(Map<String, Object> map4Param) {
		return activityFreeMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer update(ActivityFreeExt activityFreeExt) {
		return activityFreeMapper.updateByPrimaryKeySelective(activityFreeExt);
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return activityFreeMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ActivityFreeExt> findListByCondition(Map<String, Object> map4Param) {
		return activityFreeMapperExt.findListByCondition(map4Param);
	}

	@Override
	public Integer insert(ActivityFreeExt activityFreeExt) {
		return activityFreeMapper.insertSelective(activityFreeExt);
	}
}
