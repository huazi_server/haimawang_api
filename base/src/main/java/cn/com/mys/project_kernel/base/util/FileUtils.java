package cn.com.mys.project_kernel.base.util;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class FileUtils {


    private static Log logger = LogFactory.getLog(FileUtils.class);

	
	//设置文件、文件夹的权限(所有用户均有可读可写权限)
	public static void setFilePrivilege(File file){
		
		//设置可读权限--针对所有用户
		file.setReadable(true, false);
		
		//设置可写权限--针对所有用户
		file.setWritable(true, false);
		
		//return true;
	}
	
	
	
	
	
	
    /**
     *  根据路径删除指定的目录或文件，无论存在与否
     *@param sPath  要删除的目录或文件
     *@return 删除成功返回 true，否则返回 false。
     */
    public static boolean DeleteFolder(String sPath) {
    	boolean flag = false;
    	File file = new File(sPath);
        // 判断目录或文件是否存在
        if (!file.exists()) {  // 不存在返回 false
            return false;
        } else {
            // 判断是否为文件
            if (file.isFile()) {  // 为文件时调用删除文件方法
                return deleteFile(sPath);
            } else {  // 为目录时调用删除目录方法
                return deleteDirectory(sPath);
                
                
            }
        }
    }
    
    
    /**
     * 删除单个文件
     * @param   sPath    被删除文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String sPath) {
    	boolean flag = false;
        File file = new File(sPath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            file.delete();
            flag = true;
        }
        return flag;
    }
    
    /**
     * 删除目录（文件夹）以及目录下的文件
     * @param   sPath 被删除目录的文件路径
     * @return  目录删除成功返回true，否则返回false
     */
    public static boolean deleteDirectory(String sPath) {
        //如果sPath不以文件分隔符结尾，自动添加文件分隔符
        if (!sPath.endsWith(File.separator)) {
            sPath = sPath + File.separator;
        }
        File dirFile = new File(sPath);
        //如果dir对应的文件不存在，或者不是一个目录，则退出
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            return false;
        }
        boolean flag = true;
        //删除文件夹下的所有文件(包括子目录)
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            //删除子文件
            if (files[i].isFile()) {
                flag = deleteFile(files[i].getAbsolutePath());
                if (!flag) break;
            } //删除子目录
            else {
                flag = deleteDirectory(files[i].getAbsolutePath());
                if (!flag) break;
            }
        }
        if (!flag) return false;
        //删除当前目录
        if (dirFile.delete()) {
            return true;
        } else {
            return false;
        }
    }
    
    
    
    //判断文件是否存在
    public static boolean isexists(String filePath){
    	
    	File file = new File(filePath);
    	
    	return file.exists();
    }
    
    
    
    
    /**
     * sourcePath	服务器文件路径
     * sourceName	目的地文件名称
     * @throws Exception 
     * 
     */
    //下载
    public static String download(HttpServletRequest request, HttpServletResponse response, String sourcePath, String sourceName){
    	
    	String msg = "";
        try {
            //创建要下载的文件的对象(参数为要下载的文件在服务器上的路径)
            //File serverFile = new File("E:/加班申请单.docx");
            File serverFile = new File(sourcePath);

            if (serverFile.isFile() && serverFile.exists()) {

                //设置要显示在保存窗口的文件名，如果文件名中有中文的话，则要设置字符集，否则会出现乱码。另外，要写上文件后缀名
                //String fileName = java.net.URLEncoder.encode("111.doc","utf-8");
                String fileName = java.net.URLEncoder.encode(sourceName,"utf-8");

                //该步是最关键的一步，使用setHeader()方法弹出"是否要保存"的对话框，打引号的部分都是固定的值，不要改变
                response.setHeader("Content-disposition","attachment;filename=" + fileName);

                /*
                //设置类型
                response.setContentType("application/msword");
                //定义下载文件的长度/字节
                long fileLength = serverFile.length();
                //把长整形的文件长度转换为字符串
                String length = String.valueOf(fileLength);
                //设置文件长度(如果是Post请求，则这步不可少)
                response.setHeader("content_Length",length);
                */


                /*
                 *以上内容仅是下载一个空文件
                 *以下内容用于将服务器中相应的文件内容以流的形式写入到该空文件中
                 */
                //获得一个 ServletOutputStream(向客户端发送二进制数据的输出流)对象
                OutputStream servletOutPutStream = response.getOutputStream();
                //获得一个从服务器上的文件myFile中获得输入字节的输入流对象
                FileInputStream fileInputStream = new FileInputStream(serverFile);


                byte bytes[] = new byte[1024];//设置缓冲区为1024个字节，即1KB
                int len = 0;
                //读取数据。返回值为读入缓冲区的字节总数,如果到达文件末尾，则返回-1
                while((len=fileInputStream.read(bytes))!=-1){

                    //将指定 byte数组中从下标 0 开始的 len个字节写入此文件输出流,(即读了多少就写入多少)
                    servletOutPutStream.write(bytes,0,len);
                }

                servletOutPutStream.close();
                fileInputStream.close();


                msg = "complete";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    	return msg;
    }
    
    
    //移动文件:传递文件原路径、新路径
    public static String moveFile(String oldPath, String newPath){
    	
    	File dir = new File(oldPath);
    	if (!dir.exists()) {// 判断源目录是否存在  
    		return null;// 不存在则直接return
        }
    	File moveDir = new File(newPath);// 创建目标目录  
        if (!moveDir.exists()) {// 判断目标目录是否存在  
            moveDir.mkdirs();// 不存在则创建  
        }
        
        /**/
        //将文件目录放入移动后的目录
        File moveFile = new File(moveDir.getPath() + "/"  + dir.getName());
        
        //System.out.println("moveDir.getPath()="+moveDir.getPath());
        //System.out.println("dir.getName()="+dir.getName());
        //File moveFile = new File(moveDir.getPath() + "\\"  + dir.getName());
        //File moveFile = new File(moveDir.getPath() + "\\"  + dir.getName());
        //File moveFile = new File(moveDir.getPath() + "\\"  + dir.getName());
        
        if (moveFile.exists()) {// 目标文件夹下存在的话，删除
            //moveFile.delete();
        	
        }
        
        dir.renameTo(moveFile);// 移动文件
        System.out.println(dir + " 移动成功");
    	
    	
    	return "succeed";
    }
    
    
    
    /**
     * 实现文件的简单处理,复制和移动文件、目录等
     */
    //移动指定文件夹内的全部文件
    public static void fileMove(String from, String to) throws Exception {  
        try {  
            File dir = new File(from);  
            File[] files = dir.listFiles();// 将文件或文件夹放入文件集  
            if (files == null)// 判断文件集是否为空  
                return;  
            File moveDir = new File(to);// 创建目标目录  
            if (!moveDir.exists()) {// 判断目标目录是否存在  
                moveDir.mkdirs();// 不存在则创建  
            }  
            for (int i = 0; i < files.length; i++) {// 遍历文件集  
                if (files[i].isDirectory()) {// 如果是文件夹或目录,则递归调用fileMove方法，直到获得目录下的文件  
                    fileMove(files[i].getPath(), to + "\\" + files[i].getName());// 递归移动文件  
                    files[i].delete();// 删除文件所在原目录  
                }  
                File moveFile = new File(moveDir.getPath() + "\\"// 将文件目录放入移动后的目录  
                        + files[i].getName());  
                if (moveFile.exists()) {// 目标文件夹下存在的话，删除  
                    moveFile.delete();  
                }  
                files[i].renameTo(moveFile);// 移动文件  
                System.out.println(files[i] + " 移动成功");  
            }  
        } catch (Exception e) {  
            throw e;  
        }  
    }  
    
    // 复制目录下的文件（不包括该目录）到指定目录，会连同子目录一起复制过去。  
    public static void copyFileFromDir(String toPath, String fromPath) {  
        File file = new File(fromPath);  
        createFile(toPath, false);// true:创建文件 false创建目录  
        if (file.isDirectory()) {// 如果是目录  
            copyFileToDir(toPath, listFile(file));  
        }  
    }  
    
    // 复制目录到指定目录,将目录以及目录下的文件和子目录全部复制到目标目录  
    public static void copyDir(String toPath, String fromPath) {  
        File targetFile = new File(toPath);// 创建文件  
        createFile(targetFile, false);// 创建目录  
        File file = new File(fromPath);// 创建文件  
        if (targetFile.isDirectory() && file.isDirectory()) {// 如果传入是目录  
            copyFileToDir(targetFile.getAbsolutePath() + "/" + file.getName(),  
                    listFile(file));// 复制文件到指定目录  
        }  
    }  
    
    // 复制一组文件到指定目录。targetDir是目标目录，filePath是需要复制的文件路径  
    public static void copyFileToDir(String toDir, String[] filePath) {  
        if (toDir == null || "".equals(toDir)) {// 目录路径为空  
            System.out.println("参数错误，目标路径不能为空");  
            return;  
        }  
        File targetFile = new File(toDir);  
        if (!targetFile.exists()) {// 如果指定目录不存在  
            targetFile.mkdir();// 新建目录  
        } else {  
            if (!targetFile.isDirectory()) {// 如果不是目录  
                System.out.println("参数错误，目标路径指向的不是一个目录！");  
                return;  
            }  
        }  
        for (int i = 0; i < filePath.length; i++) {// 遍历需要复制的文件路径  
            File file = new File(filePath[i]);// 创建文件  
            if (file.isDirectory()) {// 判断是否是目录  
                copyFileToDir(toDir + "/" + file.getName(), listFile(file));// 递归调用方法获得目录下的文件  
                System.out.println("复制文件 " + file);  
            } else {  
                copyFileToDir(toDir, file, "");// 复制文件到指定目录  
            }  
        }  
    }  
    
    //复制文件到指定目录
    public static void copyFileToDir(String toDir, File file, String newName) {  
        String newFile = "";  
        if (newName != null && !"".equals(newName)) {  
            newFile = toDir + "/" + newName;  
        } else {  
            newFile = toDir + "/" + file.getName();  
        }  
        File tFile = new File(newFile);  
        copyFile(tFile, file);// 调用方法复制文件  
    }  
    
    public static void copyFile(File toFile, File fromFile) {// 复制文件  
        if (toFile.exists()) {// 判断目标目录中文件是否存在  
            System.out.println("文件" + toFile.getAbsolutePath() + "已经存在，跳过该文件！");  
            return;  
        } else {  
            createFile(toFile, true);// 创建文件  
        }  
        System.out.println("复制文件" + fromFile.getAbsolutePath() + "到"  
                + toFile.getAbsolutePath());  
        try {  
            InputStream is = new FileInputStream(fromFile);// 创建文件输入流  
            FileOutputStream fos = new FileOutputStream(toFile);// 文件输出流  
            byte[] buffer = new byte[1024];// 字节数组  
            while (is.read(buffer) != -1) {// 将文件内容写到文件中  
                fos.write(buffer);  
            }  
            is.close();// 输入流关闭  
            fos.close();// 输出流关闭  
        } catch (FileNotFoundException e) {// 捕获文件不存在异常  
            e.printStackTrace();  
        } catch (IOException e) {// 捕获异常  
            e.printStackTrace();  
        }  
    }  
  
    public static String[] listFile(File dir) {// 获取文件绝对路径  
        String absolutPath = dir.getAbsolutePath();// 声获字符串赋值为路传入文件的路径  
        String[] paths = dir.list();// 文件名数组  
        String[] files = new String[paths.length];// 声明字符串数组，长度为传入文件的个数  
        for (int i = 0; i < paths.length; i++) {// 遍历显示文件绝对路径  
            files[i] = absolutPath + "/" + paths[i];  
        }  
        return files;  
    }  
  
    public static void createFile(String path, boolean isFile) {// 创建文件或目录  
        createFile(new File(path), isFile);// 调用方法创建新文件或目录  
    }  
    
    public static void createFile(File file, boolean isFile) {// 创建文件  
        if (!file.exists()) {// 如果文件不存在  
            if (!file.getParentFile().exists()) {// 如果文件父目录不存在  
                createFile(file.getParentFile(), false);  
            } else {// 存在文件父目录  
                if (isFile) {// 创建文件  
                    try {  
                        file.createNewFile();// 创建新文件  
                    } catch (IOException e) {  
                        e.printStackTrace();  
                    }  
                } else {  
                    file.mkdir();// 创建目录  
                }  
            }  
        }  
    }  
    
    //创建目录
    public static void createFolder(File file) {
        if (!file.exists()) {// 如果文件不存在 
        	file.mkdirs();// 创建目录
        } 
        
    }
    
    
    //删除目录
    public static void deleteFolder(File file) {
        if (file.exists()) {// 如果文件夹存在 
        	file.delete();
        }
        
    }
    
    //移动文件夹
    public static void moveFolder(String srcPath, String destPath){
    	
    	File srcFolder = new File(srcPath);
    	if(!srcFolder.exists()){
    		srcFolder.mkdirs();// 不存在则创建 
    	}
    	File destFolder = new File(destPath);
    	if(!destFolder.exists()){
    		destFolder.mkdirs();// 不存在则创建 
    	}
    	//File newFile = new File(destFolder.getAbsoluteFile() + "\\" + srcFolder.getName());
    	
    	File newFile = new File(destFolder.getAbsoluteFile()+"");
    	srcFolder.renameTo(newFile);
    	srcFolder.delete();//删除源文件夹
    }
    
    
    
    
    
    //获取某个目录下(包括子文件夹)的文件总大小
    public static double getDirsSize(File file) {     
        //判断文件是否存在     
        if (file.exists()) {     
            //如果是目录则递归计算其内容的总大小    
            if (file.isDirectory()) {     
                File[] children = file.listFiles();     
                double size = 0;     
                for (File f : children)     
                    size += getDirsSize(f);     
                return size;     
            } else {//如果是文件则直接返回其大小,以"兆"为单位   
                double size = (double) file.length() / 1024 / 1024;        
                return size;     
            }     
        } else {     
            System.out.println("文件或者文件夹不存在，请检查路径是否正确！");     
            return 0.0;     
        }     
    }
    
    
    
    //获取某个目录下(不包括子文件夹)的文件总大小
    public static double getDirSize(File file) {
        //判断文件是否存在
        if (file.exists()) {
            //如果是目录则计算其内容的总大小
            if (file.isDirectory()) {
                File[] children = file.listFiles();//获取目录下所有的文件、文件夹数组
                double size = 0;
                for (File f : children){
                	if(!f.isDirectory()){//判断是否是文件
                		size += f.length();
                	}
                    //size += getDirSize(f);
                }
                
                return size;//返回字节数
                //return size/1024/1024;//返回大小,以"兆"为单位
            } else {//如果是文件则直接返回其大小,以"兆"为单位
                double size = (double) file.length() / 1024 / 1024;
                return size;
            }
            
        } else {
            System.out.println("文件或者文件夹不存在，请检查路径是否正确！");
            return 0.0;
        }     
    }
    
    
    //获取指定目录下的指定类型的文件
    public static List<String> getFilePathList(String path, String fileType) {
    	
    	List<String> list = new ArrayList<String>(); 
    	
    	File file = new File(path);
        if (file.isDirectory()) {
            File[] dirFile = file.listFiles();
            for (File f : dirFile) {
                if (f.isFile() && f.getName().toLowerCase().endsWith(fileType)) {
                	
                	list.add(f.getAbsolutePath().replace("\\","/"));//获取全路径并添加到集合
                	
                }
            }
        }
    	//返回路径集合
    	return list;
    	
    }
    
    //获取文件大小
    public static long getFileSize(String filePath) {
    	
    	long fileLength = 0L;
    	
    	File f = new File(filePath);
    	if (f.exists() && f.isFile()){
    		fileLength = f.length();
    		return fileLength;
    	}
    	
    	return fileLength;
    }
    
    
    /**
     * 压缩文件夹
     * @param sourceDIR 文件夹名称（包含路径）
     * @param targetZipFile 生成zip文件名
     * @author liuxiangwei
     */
    public static void zipDIR(String sourceDIR, String targetZipFile) {
        try {
            FileOutputStream target = new FileOutputStream(targetZipFile);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(target));
            int BUFFER_SIZE = 1024;
            byte buff[] = new byte[BUFFER_SIZE];
            File dir = new File(sourceDIR);
            if (!dir.isDirectory()) {
                throw new IllegalArgumentException(sourceDIR+" is not a directory!");
            }
            File files[] = dir.listFiles();
      
            for (int i = 0; i < files.length; i++) {
                FileInputStream fi = new FileInputStream(files[i]);
                BufferedInputStream origin = new BufferedInputStream(fi);
                ZipEntry entry = new ZipEntry(files[i].getName());
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(buff)) != -1) {
                    out.write(buff, 0, count);
                }
                origin.close();
            }
            out.close();
      
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }
    
    //获取目录下的所有文件(指定类型)
    public static List<File> getFileListByDir(String dir, String uploadFileType){
    	
    	List<File> filelist = new ArrayList<File>();
    	
    	File dirFile = new File(dir);
    	
    	File[] files = dirFile.listFiles();
    	
    	
    	for(File f : files){
    		//判断是否是上传的文件类型
    		if(f.isFile() && uploadFileType.indexOf(f.getName().substring(f.getName().lastIndexOf(".")+1))!=-1){
    			
    			filelist.add(f);
    		}
    		
    	}
    	
    	return filelist;
    }
    
    
    //过滤路径中的特殊字符,返回替换后的路径
    public static String filterChar(String srcString) {
    	
    	String returnString = "";

    	//获取配置文件中定义的特殊字符
    	String filterChars = PropertiesUtils.getKeyValue("filterChar.properties", "filterChar");
    	try {
    		//转码
			filterChars = new String(filterChars.getBytes("ISO-8859-1"), "UTF-8");

	    	String[] array = filterChars.split(",");
	    	//转换为list
	        List<String> tempList = Arrays.asList(array);
	        
	    	String replace = "_";

	    	for(String s : tempList){
	        	
	            int r=srcString.indexOf(s);
	        	if(r!=-1){//存在就替换
	        		
	        		srcString = srcString.replace(s, replace);
	        	}
	    		
	    	}
	    	
		} catch (Exception e) {
			
			e.printStackTrace();
		}
    	returnString = srcString;
        
    	return returnString;
    }
    

    
    /** 
     * 将存放在sourceFilePath目录下的源文件，打包成fileName名称的zip文件，并存放到zipFilePath路径下 
     * @param sourceFilePath :待压缩的文件路径 
     * @param zipFilePath :压缩后存放路径 
     * @param fileName :压缩后文件的名称 
     * @return 
     */  
    public static boolean fileToZip(String sourceFilePath,String zipFilePath,String fileName){  
    	boolean flag = false;  
    	File sourceFile = new File(sourceFilePath);  
    	FileInputStream fis = null;  
    	BufferedInputStream bis = null;  
    	FileOutputStream fos = null;  
    	ZipOutputStream zos = null;  
    	
    	if(sourceFile.exists() == false){  
    		System.out.println("待压缩的文件目录："+sourceFilePath+"不存在.");  
    	}else{  
    		try {  
    			File zipFile = new File(zipFilePath + "/" + fileName +".zip");  
    			if(zipFile.exists()){  
    				System.out.println(zipFilePath + "目录下存在名字为:" + fileName +".zip" +"打包文件.");
    			}else{
    				File[] sourceFiles = sourceFile.listFiles();  
    				if(null == sourceFiles || sourceFiles.length<1){  
    					System.out.println("待压缩的文件目录：" + sourceFilePath + "里面不存在文件，无需压缩.");  
    				}else{  
    					fos = new FileOutputStream(zipFile);  
    					zos = new ZipOutputStream(new BufferedOutputStream(fos));  
    					byte[] bufs = new byte[1024*10];  
    					for(int i=0;i<sourceFiles.length;i++){  
    						//创建ZIP实体，并添加进压缩包
    						ZipEntry zipEntry = new ZipEntry(sourceFiles[i].getName());  
    						//ZipEntry zipEntry = new ZipEntry(zipEntry); 
    						zos.putNextEntry(zipEntry);  
    						//读取待压缩的文件并写进压缩包里
    						fis = new FileInputStream(sourceFiles[i]);  
    						bis = new BufferedInputStream(fis, 1024*10);  
    						int read = 0;  
    						while((read=bis.read(bufs, 0, 1024*10)) != -1){  
    							zos.write(bufs,0,read);  
    						}
    					}  
    					flag = true;  
    				}  
    			}  
    		} catch (FileNotFoundException e) {  
    			e.printStackTrace();  
    			throw new RuntimeException(e);  
    		} catch (IOException e) {  
    			e.printStackTrace();  
    			throw new RuntimeException(e);  
    		} finally{  
    			//关闭流
    			try {  
    				if(null != bis) bis.close();  
    				if(null != zos) zos.close();  
    			} catch (IOException e) {  
    				e.printStackTrace();  
    				throw new RuntimeException(e);  
    			}  
    		}  
    	}  
    	return flag;  
    }



    //读取网络文件内容
    public static String openFile(String fileNetUrl) {
        int HttpResult; // 服务器返回的状态
        String ee = new String();
        try
        {
            URL url =new URL(fileNetUrl); // 创建URL
            URLConnection urlconn = url.openConnection(); // 试图连接并取得返回状态码
            urlconn.connect();
            HttpURLConnection httpconn =(HttpURLConnection)urlconn;
            HttpResult = httpconn.getResponseCode();
            if(HttpResult != HttpURLConnection.HTTP_OK) {
                System.out.println("无法连接到");
            } else {
                int filesize = urlconn.getContentLength(); // 取数据长度
                InputStreamReader isReader = new InputStreamReader(urlconn.getInputStream(),"UTF-8");
                BufferedReader reader = new BufferedReader(isReader);
                StringBuffer buffer = new StringBuffer();
                String line; // 用来保存每行读取的内容
                line = reader.readLine(); // 读取第一行
                while (line != null) { // 如果 line 为空说明读完了
                    buffer.append(line); // 将读到的内容添加到 buffer 中
                    buffer.append(" "); // 添加换行符
                    line = reader.readLine(); // 读取下一行
                }
                System.out.println(buffer.toString());
                ee = buffer.toString();
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return  ee;
    }


    //读取本地文件内容
    public static String readFile(String filePath) {
        //filePath绝对路径或相对路径都可以，写入文件时演示相对路径,读取文件
        //防止文件建立或读取失败，用catch捕捉错误
        //不关闭文件会导致资源的泄露，读写文件都同理
        //Java7的try-with-resources可以优雅关闭文件，异常时自动关闭文件
        StringBuffer lineBuffer = new StringBuffer();
        try (//FileReader reader = new FileReader(filePath);
             //BufferedReader br = new BufferedReader(reader)
             InputStreamReader isr = new InputStreamReader(new FileInputStream(new File(filePath)), "UTF-8");
             BufferedReader br = new BufferedReader(isr)
        ) {
            String line;
            //网友推荐更加简洁的写法
            while ((line = br.readLine()) != null) {
                // 一次读入一行数据
                //System.out.println(line);
                lineBuffer.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //System.out.println("");
        //System.out.println("");
        //System.out.println("");
        //System.out.println(lineBuffer);
        //System.out.println("--------------");
        return lineBuffer.toString();
    }




    /**
     * 上传文件
     * serverUrl        接口地址
     * fileParamName    文件参数的名称
     * file             本地文件对象
     * params           其他参数
     *
     */
    public static String post4Caller(String serverUrl, String fileParamName, File file, Map<String, Object> params)
            throws ClientProtocolException, IOException {
        HttpPost httpPost = new HttpPost(serverUrl);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        // 上传的文件
        builder.addBinaryBody(fileParamName, file);
        // 设置其他参数
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            builder.addTextBody(entry.getKey(), entry.getValue().toString(), ContentType.TEXT_PLAIN.withCharset("UTF-8"));
        }
        HttpEntity httpEntity = builder.build();
        httpPost.setEntity(httpEntity);
        HttpClient httpClient = HttpClients.createDefault();
        HttpResponse response = httpClient.execute(httpPost);
        if (null == response || response.getStatusLine() == null) {
            logger.info("Post Request For Url[{}] is not ok. Response is null --->>>" + serverUrl);
            return null;
        } else if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            logger.info("Post Request For Url[{}] is not ok. Response Status Code is {} --->>>" + serverUrl + " statusCode=" +
                    response.getStatusLine().getStatusCode());
            return null;
        }
        return EntityUtils.toString(response.getEntity());
    }










    /**
    public static void main(String[] args) {// java程序主入口处  
        String fromPath = "E:/createFile";// 目录路径  
        String toPath = "F:/createFile";// 源路径  
        System.out.println("1.移动文件：从路径 " + fromPath + " 移动到路径 " + toPath);  
        try {  
            fileMove(fromPath, toPath);// 调用方法实现文件的移动  
        } catch (Exception e) {  
            System.out.println("移动文件出现问题" + e.getMessage());  
        }  
        System.out.println("2.复制目录 " + toPath + " 下的文件（不包括该目录）到指定目录" + fromPath  
                + " ，会连同子目录一起复制过去。");  
        copyFileFromDir(fromPath, toPath);// 调用方法实现目录复制  
        System.out.println("3.复制目录 " + fromPath + "到指定目录 " + toPath  
                + " ,将目录以及目录下的文件和子目录全部复制到目标目录");  
        copyDir(toPath, fromPath);// 调用方法实现目录以用目录下的文件和子目录全部复制  
    }  
    */
    
    
}
