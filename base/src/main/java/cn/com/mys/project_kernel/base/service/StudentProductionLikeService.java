package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.StudentProductionLikeExt;

import java.util.Map;

/**
 * Service的接口
 *
 */
public interface StudentProductionLikeService {


    StudentProductionLikeExt findObjectByCondition(Map<String, Object> map4Param);

    Integer insert(StudentProductionLikeExt studentProductionLikeExt);

    Integer delete(StudentProductionLikeExt studentProductionLikeExt);
}
