package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CompetitionPlayerMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CompetitionPlayerMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CompetitionPlayerExt;
import cn.com.mys.project_kernel.base.service.CompetitionPlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class CompetitionPlayerServiceImpl implements CompetitionPlayerService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CompetitionPlayerMapper competitionPlayerMapper;

	@Autowired
	public CompetitionPlayerMapperExt competitionPlayerMapperExt;


	@Override
	public CompetitionPlayerExt findObjectByCondition(Map<String, Object> map4Param) {
		return competitionPlayerMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return competitionPlayerMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<CompetitionPlayerExt> findListByCondition(Map<String, Object> map4Param) {
		return competitionPlayerMapperExt.findListByCondition(map4Param);
	}

	@Override
	public Integer findCountByCondition4Vote(Map<String, Object> map4Param) {
		return competitionPlayerMapperExt.findCountByCondition4Vote(map4Param);
	}

	@Override
	public List<CompetitionPlayerExt> findListByCondition4Vote(Map<String, Object> map4Param) {
		return competitionPlayerMapperExt.findListByCondition4Vote(map4Param);
	}

	@Override
	public CompetitionPlayerExt findById(Long id) {
		return competitionPlayerMapperExt.findById(id);
	}

	@Override
	public Integer insert(CompetitionPlayerExt playerExt) {
		return competitionPlayerMapper.insertSelective(playerExt);
	}

	@Override
	public Integer update(CompetitionPlayerExt playerExt) {
		return competitionPlayerMapper.updateByPrimaryKeySelective(playerExt);
	}

	@Override
	public CompetitionPlayerExt findLastObjectByCondition(Map<String, Object> m) {
		return competitionPlayerMapperExt.findLastObjectByCondition(m);
	}
}
