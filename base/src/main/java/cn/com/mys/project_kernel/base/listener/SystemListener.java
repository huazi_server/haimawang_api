package cn.com.mys.project_kernel.base.listener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;



/**
 * @author mingjun_T
 * 监听器
 * 
 */
public class SystemListener implements ServletContextListener {

	Log log = LogFactory.getLog(SystemListener.class);


	//监听器的销毁
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		log.debug("=============监听器销毁============");


	}

	
	//监听器的初始化
	@Override
	public void contextInitialized(ServletContextEvent event) {
		log.debug("=============监听器初始化============");



	}

	
}


