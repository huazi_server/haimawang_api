package cn.com.mys.project_kernel.base.controller.account.student;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.ext.StudentExt;
import cn.com.mys.project_kernel.base.filter.wrapper.BodyReaderHttpServletRequestWrapper;
import cn.com.mys.project_kernel.base.service.NotifyService;
import cn.com.mys.project_kernel.base.service.StudentCourseService;
import cn.com.mys.project_kernel.base.service.TbTestService;
import cn.com.mys.project_kernel.base.service.StudentService;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.util.FileUtils;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 学生
 *
 */
@Controller
@RequestMapping(value = "/account/student")
public class StudentController {

	Log log = LogFactory.getLog(StudentController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;

	@Autowired
	private NotifyService notifyService;

	@Autowired
	private StudentCourseService studentCourseService;


	//获取学生信息
	@RequestMapping(value = "info")
	public ResponseEntity<ResponseJson> info(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		Map<String, Object> m = VerifyLoginToken.verifyLoginToken4Map(loginToken);

		Integer accountId = (Integer) m.get("id");
		String phone = (String) m.get("phone");

		//请求体内的参数
		String useFlag = (String) paramMap.get("useFlag");//使用传递的名称和头像(1不使用、2使用)
		String name = (String) paramMap.get("name");
		String avatarUrl = (String) paramMap.get("avatarUrl");

		if(accountId==null || accountId==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		StudentExt studentExt = null;

		StudentExt se1 = studentService.findByAccountId(accountId.toString());
		studentExt = (studentExt==null||studentExt.getId()==0)?se1:studentExt;
		StudentExt se2 = studentService.findByPhone(phone);
		studentExt = (studentExt==null||studentExt.getId()==0)?se2:studentExt;
		if(studentExt==null || studentExt.getId()==0){
			//不存在则创建
			studentExt = new StudentExt();
			studentExt.setAccountId(accountId.longValue());
			studentExt.setType(0);//类型(0默认)
			studentExt.setPhone(phone);
			studentExt.setSourceType(1);//来源类型(1、WEB端)
			studentExt.setEnableFlag(1);//启用标记(1正常、2禁用)
			studentExt.setRegisterAt(new Date());
			studentExt.setRemark("");
			studentExt.setState(0);
			studentExt.setCreatedAt(new Date());
			studentExt.setUpdatedAt(new Date());
			studentExt.setDelFlag(0);
			studentService.insert4Ignore(studentExt);


			//插入操作可能由于并发而失败，所以重新查询数据
			studentExt = studentService.findByAccountId(accountId.toString());


			//不存在直接返回
			/*ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);*/
		}




		//更新accountId
		Long _accountId = studentExt.getAccountId();
		//if(accountId!=null && accountId>0 && (_accountId==null||_accountId==0)){}
		if(accountId!=null && accountId>0){
			studentExt.setAccountId(accountId.longValue());
			studentExt.setUpdatedAt(new Date());
			studentService.updateById(studentExt);
		}
		//更新手机号
		String _phone = studentExt.getPhone();
		//if(StringUtils.isNotEmpty(phone) && StringUtils.isEmpty(_phone)){}
		if(StringUtils.isNotEmpty(phone)){
			studentExt.setPhone(phone);
			studentExt.setUpdatedAt(new Date());
			studentService.updateById(studentExt);
		}


		if(StringUtils.isNotEmpty(useFlag) && "2".equals(useFlag)){
			studentExt.setName(name);
			studentExt.setAvatarUrl(avatarUrl);
			studentExt.setUpdatedAt(new Date());
			studentService.updateById(studentExt);
		}else{
			//do nothing
		}


		//更新名称和头像
		Object _nickname = m.get("nickname");
		Object _avatarUrl = m.get("avatarUrl");
		if(_nickname!=null && !"null".equalsIgnoreCase(_nickname.toString()) && StringUtils.isEmpty(studentExt.getName())){
			studentExt.setName(_nickname.toString());
		}
		if(_avatarUrl!=null && !"null".equalsIgnoreCase(_avatarUrl.toString()) && StringUtils.isEmpty(studentExt.getAvatarUrl())){
			studentExt.setAvatarUrl(_avatarUrl.toString());
		}
		studentService.updateById(studentExt);


		StudentExt student = studentService.findObjectById(studentExt.getId());
		Integer notifyCount = 0;
		//查询未读消息数量
		Map<String, Object> mm = new HashMap<String, Object>();
		mm.put("delFlag", "0");
		mm.put("ownerType", "2");//所属类型(1教师、2学生)
		mm.put("personId", student.getId().toString());
		mm.put("type", "");
		mm.put("state", "1");//状态(1未读、2已读)
		notifyCount = notifyService.findCountByCondition(mm);
		student.setNotifyCount(notifyCount);

		//设置当前课程阶段(课程进行中或结束)、最大课程阶段
		Map<String, Object> mmm = new HashMap<String, Object>();
		mmm.put("delFlag", "0");
		mmm.put("studentId", student.getId().toString());
		mmm.put("state", "5,6");//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
		Integer currentCourseStage = 0;
		currentCourseStage = studentCourseService.findMaxCourseStageByStudentId(mmm);
		student.setCurrentCourseStage(currentCourseStage);
		student.setMaxCourseStage(Constant.maxCourseStage);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),student);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//更新学生信息
	@RequestMapping(value = "update")
	public ResponseEntity<ResponseJson> update(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		String name = (String) paramMap.get("name");
		String nickName = (String) paramMap.get("nickName");
		String wxNickName = (String) paramMap.get("wxNickName");
		//String phone = (String) paramMap.get("phone");
		String avatarUrl = (String) paramMap.get("avatarUrl");//头像
		String email = (String) paramMap.get("email");
		String gender = (String) paramMap.get("gender");//性别(0未知、1男、2女)

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		StudentExt se = new StudentExt();
		se.setId(Long.parseLong(studentId));

		se.setName(name);
		se.setNickName(nickName);
		se.setWxNickName(wxNickName);
		//se.setPhone(phone);
		se.setAvatarUrl(avatarUrl);
		se.setEmail(email);
		se.setGender(StringUtils.isNotEmpty(gender)?Integer.parseInt(gender):null);

		se.setUpdatedAt(new Date());
		studentService.updateById(se);

		Student s = studentService.findById(Long.parseLong(studentId));


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),s);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//上传文件
	@RequestMapping(value = "uploadFile",produces = "application/json;charset=utf-8")
	@ResponseBody
	public String uploadFile(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",402);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","loginToken不存在或过期");

			return jo.toString();
		}
		if(StringUtils.isEmpty(studentId)){
			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",100);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","用户不存在");
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("loginToken", loginToken);
		map4Param.put("folderPath", "/student/file");//学生相关的文件保存的路径

		map4Param.put("loginProductId", Constant.loginProductId);
		map4Param.put("loginProductKey", Constant.loginProductKey);

		try {
			String result = "";

			BodyReaderHttpServletRequestWrapper bodyReaderRequest = (BodyReaderHttpServletRequestWrapper) request;
			MultipartHttpServletRequest multipartRequest = bodyReaderRequest.getMultipartRequest();
			String fileParamName = "upload";
			List<MultipartFile> files = multipartRequest.getFiles(fileParamName);//获取文件

			MultipartFile file = files.get(0);

			//获取上传的文件的名称(包含后缀)
			String filename = file.getOriginalFilename();
			//获取名称(不含后缀)
			String fileName = filename.substring(0,filename.lastIndexOf("."));
			//获取后缀(扩展名)
			String contentType = filename.substring(filename.lastIndexOf(".")+1);
			map4Param.put("fileName", filename);
			map4Param.put("contentType", contentType);

			CommonsMultipartFile cf= (CommonsMultipartFile)file;
			DiskFileItem fi = (DiskFileItem)cf.getFileItem();

			//MultipartFile转换成File
			File f = fi.getStoreLocation();

			String uploadFileUrl = PropertiesUtils.getKeyValue("uploadFileUrl");

			result = FileUtils.post4Caller(uploadFileUrl,"upload",f,map4Param);

			//System.out.println(result);

			JSONObject jo = JSONObject.fromObject(result);

			//ResponseJson responseJsonResult = new ResponseJson(_result,_code,_message,_error,ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_data);
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			return jo.toString();
		} catch (IOException e) {

			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"上传失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code","100");
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","上传失败");

			return jo.toString();

		}
	}












}
