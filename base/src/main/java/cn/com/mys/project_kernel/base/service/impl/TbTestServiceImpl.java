package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.TbTestMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.TbTestMapperExt;
import cn.com.mys.project_kernel.base.entity.TbTest;
import cn.com.mys.project_kernel.base.service.TbTestService;
import cn.com.mys.project_kernel.base.util.BeanConverterUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;


/**
 * TbTestService的实现类
 *
 */

@Service
public class TbTestServiceImpl implements TbTestService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public TbTestMapper tbTestMapper;

	@Autowired
	public TbTestMapperExt tbTestMapperExt;


	@Override
	public List<TbTest> findTbTestListByCondition(Map<String, Object> map) {
		//组织条件
		Example example = new Example(TbTest.class);
		example.createCriteria().andEqualTo("column1",map.get("column1"));
		return null;
	}

	@Override
	public int findTbTestListCountByCondition(Map<String, Object> map) {
		return tbTestMapperExt.findTbTestListCountByCondition(map);
	}

	@Override
	public List<TbTest> findTbTestListPageByCondition(Map<String, Object> map) {
		return tbTestMapperExt.findTbTestListPageByCondition(map);
	}

	@Override
	public List<TbTest> findTbTestListAll() {
		return null;
	}

	@Override
	public int insetTbTest(TbTest tbTest) {
		return tbTestMapper.insertSelective(tbTest);
	}

	@Override
	public int deleteTbTestById(String id) {
		return tbTestMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int updateTbTest(TbTest tbTest) {
		return tbTestMapper.updateByPrimaryKeySelective(tbTest);
	}
	
	@Override
	public int updateTbTest4Map(Map<String, Object> map) {
		TbTest tbTest = (TbTest)BeanConverterUtils.map2JavaBean(map, TbTest.class);
		return tbTestMapper.updateByPrimaryKeySelective(tbTest);
	}

	@Override
	public TbTest findTbTestById(String id) {
		return tbTestMapper.selectByPrimaryKey(id);
	}

	@Override
	public TbTest findTbTestByCondition(Map<String, Object> map) {
		//组织条件
		Example example = new Example(TbTest.class);
		example.createCriteria().andEqualTo("column1",map.get("column1"));
		return null;
	}
}
