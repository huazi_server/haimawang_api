package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.StudentCourse;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface StudentCourseService {


    StudentCourseExt findObjectByCondition(Map<String, Object> map4Param);

    Integer update(StudentCourseExt studentCourseExt);

    List<StudentCourseExt> findListByCondition(Map<String, Object> m);

    Integer insert(StudentCourseExt studentCourseExt);

    Integer delete(StudentCourseExt studentCourseExt);

    Integer findStudentCountByTeacherCourseId(Map<String, Object> map4Param);

    Integer findMaxCourseStageByStudentId(Map<String, Object> mmm);

    List<StudentCourseExt> findListByCourseId(Map<String, Object> m);

    List<StudentCourseExt> findListByCondition4CourseGroup(Map<String, Object> m);

    Integer update4Del(Map<String, Object> m);


}
