package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.Course;

import java.math.BigDecimal;
import java.util.List;

public class CourseExt extends Course {


    private Integer buyFlag;//购买标识(1未购买、2已购买)
    private Integer stopFlag;//结束标识(1未结束、2已结束)
    public Integer getBuyFlag() {
        return buyFlag;
    }
    public void setBuyFlag(Integer buyFlag) {
        this.buyFlag = buyFlag;
    }

    public Integer getStopFlag() {
        return stopFlag;
    }

    public void setStopFlag(Integer stopFlag) {
        this.stopFlag = stopFlag;
    }


    private Integer courseLessonCount;//课时数量
    private List<CourseLessonExt> courseLessonInfoList;//课时列表
    public Integer getCourseLessonCount() {
        return courseLessonCount;
    }
    public void setCourseLessonCount(Integer courseLessonCount) {
        this.courseLessonCount = courseLessonCount;
    }
    public List<CourseLessonExt> getCourseLessonInfoList() {
        return courseLessonInfoList;
    }
    public void setCourseLessonInfoList(List<CourseLessonExt> courseLessonInfoList) {
        this.courseLessonInfoList = courseLessonInfoList;
    }

    private Long teacherCourseId;//教师的课程id
    public Long getTeacherCourseId() {
        return teacherCourseId;
    }
    public void setTeacherCourseId(Long teacherCourseId) {
        this.teacherCourseId = teacherCourseId;
    }

    private Integer maxCourseStage;//最大课程阶段
    private String coursePackageName;//课程包名称
    public Integer getMaxCourseStage() {
        return maxCourseStage;
    }
    public void setMaxCourseStage(Integer maxCourseStage) {
        this.maxCourseStage = maxCourseStage;
    }
    public String getCoursePackageName() {
        return coursePackageName;
    }
    public void setCoursePackageName(String coursePackageName) {
        this.coursePackageName = coursePackageName;
    }


    private Integer studentScore;//学生的评分

    public Integer getStudentScore() {
        return studentScore;
    }

    public void setStudentScore(Integer studentScore) {
        this.studentScore = studentScore;
    }


    private StudentCourseOrderExt studentCourseOrderInfo;//学生课程订单信息
    public StudentCourseOrderExt getStudentCourseOrderInfo() {
        return studentCourseOrderInfo;
    }

    public void setStudentCourseOrderInfo(StudentCourseOrderExt studentCourseOrderInfo) {
        this.studentCourseOrderInfo = studentCourseOrderInfo;
    }


    private List<StudentExt> studentInfoList;//学生列表
    public List<StudentExt> getStudentInfoList() {
        return studentInfoList;
    }
    public void setStudentInfoList(List<StudentExt> studentInfoList) {
        this.studentInfoList = studentInfoList;
    }


    Integer unCommitCount;//未提交的课时的作业数量
    public Integer getUnCommitCount() {
        return unCommitCount;
    }
    public void setUnCommitCount(Integer unCommitCount) {
        this.unCommitCount = unCommitCount;
    }



    private CourseLessonExt courseLessonInfo4StayOpen;//待开放的第一个课时信息
    public CourseLessonExt getCourseLessonInfo4StayOpen() {
        return courseLessonInfo4StayOpen;
    }
    public void setCourseLessonInfo4StayOpen(CourseLessonExt courseLessonInfo4StayOpen) {
        this.courseLessonInfo4StayOpen = courseLessonInfo4StayOpen;
    }


    private StudentCourseExt StudentCourseInfo;//学生课程
    public StudentCourseExt getStudentCourseInfo() {
        return StudentCourseInfo;
    }
    public void setStudentCourseInfo(StudentCourseExt studentCourseInfo) {
        StudentCourseInfo = studentCourseInfo;
    }


    private String qrCodeUrl4Teacher;//教师的二维码图片地址
    public String getQrCodeUrl4Teacher() {
        return qrCodeUrl4Teacher;
    }
    public void setQrCodeUrl4Teacher(String qrCodeUrl4Teacher) {
        this.qrCodeUrl4Teacher = qrCodeUrl4Teacher;
    }


    private BigDecimal activityTuanPrice;//拼团活动价
    public BigDecimal getActivityTuanPrice() {
        return activityTuanPrice;
    }
    public void setActivityTuanPrice(BigDecimal activityTuanPrice) {
        this.activityTuanPrice = activityTuanPrice;
    }


    ChannelCourseExt ChannelCourseInfo;//渠道单课程或渠道系列课
    public ChannelCourseExt getChannelCourseInfo() {
        return ChannelCourseInfo;
    }
    public void setChannelCourseInfo(ChannelCourseExt channelCourseInfo) {
        ChannelCourseInfo = channelCourseInfo;
    }

}