package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ReportExt;

/**
 * Service的接口
 *
 */
public interface ReportService {


    Integer insert(ReportExt reportExt);
}
