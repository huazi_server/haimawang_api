package cn.com.mys.project_kernel.base.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
  * 拦截器(处理错误信息)
  * 自定义拦截器,处理所有的请求
  * 
  */

public class ExceptionOrErrorInterceptor implements HandlerInterceptor {

	
	/**
	  * log4j记录日志
	  */
	//获取log对象,通过加载action来获取log对象
	Log log = LogFactory.getLog(ExceptionOrErrorInterceptor.class);
	
	
	//该方法会在整个请求处理完成,也就是在视图返回并被渲染之后执行
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object obj, Exception ex)
			throws Exception {

		log.debug("=====afterCompletion====");

		//异常处理
		if(ex != null){
			/** 
			  * 处理Controller执行后发生的异常
			  */
			String errorMsg = "";//全部信息

			String message = "";//主要信息

			/**
			 * 换行
			 */
			String NEW_LINE = System.getProperty("line.separator");

			//通过instanceof判断到底是什么异常类型
			if (ex instanceof RuntimeException) {
				//未知的运行时异常,强转为运行时异常
				RuntimeException re = (RuntimeException) ex;
				//re.printStackTrace();
				String localizedMessage = re.getLocalizedMessage();
				Throwable throwable = re.getCause();
				String str = re.toString();

				message = re.getMessage();

				StackTraceElement[] stElement = re.getStackTrace();
				//重新赋值--异常信息
				for(StackTraceElement ste : stElement){
					//System.out.println(ste.toString());
					errorMsg = errorMsg + ste.toString() + NEW_LINE;
				}

				errorMsg = str +NEW_LINE+ errorMsg;
			}
			
			//自定义错误信息存放在request中
			//request.setAttribute("errorMsg", errorMsg);
			request.setAttribute("errorMsg", message);

			//记录error级别的信息--传递异常对象以及异常信息
			log.error(message, ex);
			
		}


		System.out.println("Completion");
		//System.out.println("-----------------------------------------------------------------------------------------");
	}
	
	//该方法将在请求处理之后,也就是在Controller方法调用之后被调用
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
			Object obj, ModelAndView mav) throws Exception {
		
		
		log.debug("==========postHandle=========");
		
		
		if(mav != null){  
            String viewName = mav.getViewName();
            log.debug("view name : " + viewName);
        }else{  
            log.debug("view is null");  
        }

		//System.out.println("postHandle");
	}
	
	//在业务处理器处理请求之前对该请求进行拦截处理
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object obj) throws Exception {
		
		//return false;
		log.debug("=======进入ExceptionOrErrorInterceptor拦截器=========");
		log.debug("=====preHandle====");

		System.out.println("-----------------------------------------------------------------------------------------");
		System.out.println("Start");
		return true;
	}
	
	
	
	
	
}
