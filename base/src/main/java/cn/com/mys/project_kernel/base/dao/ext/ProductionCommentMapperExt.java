package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ProductionCommentMapperExt {

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<ProductionCommentExt> findListByCondition(Map<String, Object> map4Param);

}