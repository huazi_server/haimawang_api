package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.NotifyMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.NotifyMapperExt;
import cn.com.mys.project_kernel.base.entity.Notify;
import cn.com.mys.project_kernel.base.entity.ext.NotifyExt;
import cn.com.mys.project_kernel.base.service.NotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class NotifyServiceImpl implements NotifyService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public NotifyMapper notifyMapper;

	@Autowired
	public NotifyMapperExt notifyMapperExt;


	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return notifyMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<NotifyExt> findListByCondition(Map<String, Object> map4Param) {
		return notifyMapperExt.findListByCondition(map4Param);
	}

	@Override
	public Notify findById(long id) {
		return notifyMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer update(NotifyExt notify) {
		return notifyMapper.updateByPrimaryKeySelective(notify);
	}

	@Override
	public Integer delete(NotifyExt notify) {
		return notifyMapper.deleteByPrimaryKey(notify.getId());
	}

	@Override
	public Integer updateByCondition(Map<String, Object> map4Param) {
		return notifyMapperExt.updateByCondition(map4Param);
	}

	@Override
	public Integer deleteByCondition(Map<String, Object> map4Param) {
		return notifyMapperExt.deleteByCondition(map4Param);
	}

	@Override
	public Integer insert(NotifyExt notify) {
		return notifyMapper.insertSelective(notify);
	}
}
