package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestProductionScoreExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ContestProductionScoreMapperExt {

    List<ContestProductionScoreExt> findListByCondition(Map<String, Object> m);
}