package cn.com.mys.project_kernel.base.exception;

/**
 * Created by mingjun_T on 2016-07-09.
 */
public class BusinessException extends Exception {

    public BusinessException(){}

    public BusinessException(String message){
        super(message);
    }

}
