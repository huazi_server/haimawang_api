package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CompetitionProductionVoteExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CompetitionProductionVoteMapperExt {

    List<CompetitionProductionVoteExt> findListByCondition4Today(Map<String, Object> map4Param);

    Integer findMaxVoteCountByCompetitionId(Long competitionId);
}