package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ChannelCourse;
import cn.com.mys.project_kernel.base.entity.ext.ChannelCourseExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ChannelCourseService {


    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ChannelCourseExt> findListByCondition(Map<String, Object> map4Param);

    Integer insert(ChannelCourse cc);

    ChannelCourse findById(long channelCourseId);

    Integer update(ChannelCourse cc);

    ChannelCourseExt findObjectByCondition(Map<String, Object> map4Param);
}
