package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ChannelStatisticsExt;

import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ChannelStatisticsService {

    Integer insert(ChannelStatisticsExt cs);

    Integer update(ChannelStatisticsExt cs);

    ChannelStatisticsExt findObjectByCondition(Map<String, Object> mm);


    void generateChannelStatistics(String channelId, String studentId, String sourceType, String sourceId, String courseType, String courseId, String type);

}
