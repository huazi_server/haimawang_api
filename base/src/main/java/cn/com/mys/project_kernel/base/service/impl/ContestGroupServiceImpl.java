package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestGroupMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestGroupMapperExt;
import cn.com.mys.project_kernel.base.entity.ContestGroup;
import cn.com.mys.project_kernel.base.service.ContestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service的实现类
 *
 */

@Service
public class ContestGroupServiceImpl implements ContestGroupService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestGroupMapper contestGroupMapper;

	@Autowired
	public ContestGroupMapperExt contestGroupMapperExt;

	@Override
	public ContestGroup findById(long id) {
		return contestGroupMapper.selectByPrimaryKey(id);
	}
}
