package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.StudentMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.StudentMapperExt;
import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.ext.StudentExt;
import cn.com.mys.project_kernel.base.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public StudentMapper studentMapper;

	@Autowired
	public StudentMapperExt studentMapperExt;


	@Override
	public StudentExt findByAccountId(String accountId) {
		return studentMapperExt.findByAccountId(accountId);
	}

	@Override
	public Integer updateById(StudentExt studentExt) {
		return studentMapperExt.updateById(studentExt);
	}

	@Override
	public Student findById(long id) {
		return studentMapper.selectByPrimaryKey(id);
	}

	@Override
	public StudentExt findObjectById(Long id) {
		return studentMapperExt.findObjectById(id);
	}

	@Override
	public Integer insert(StudentExt studentExt) {
		return studentMapper.insertSelective(studentExt);
	}

	@Override
	public StudentExt findByPhone(String phone) {
		return studentMapperExt.findByPhone(phone);
	}

	@Override
	public Integer insert4Ignore(StudentExt studentExt) {
		return studentMapperExt.insert4Ignore(studentExt);
	}

	@Override
	public Integer update(Student student) {
		return studentMapper.updateByPrimaryKeySelective(student);
	}


	@Override
	public Integer findCountByCondition4Homework(Map<String, Object> map4Param) {
		return studentMapperExt.findCountByCondition4Homework(map4Param);
	}

	@Override
	public List<StudentExt> findListByCondition4Homework(Map<String, Object> map4Param) {
		return studentMapperExt.findListByCondition4Homework(map4Param);
	}


	@Override
	public Integer findCountByCondition4Course(Map<String, Object> map4Param) {
		return studentMapperExt.findCountByCondition4Course(map4Param);
	}

	@Override
	public List<StudentExt> findListByCondition4Course(Map<String, Object> map4Param) {
		return studentMapperExt.findListByCondition4Course(map4Param);
	}
}
