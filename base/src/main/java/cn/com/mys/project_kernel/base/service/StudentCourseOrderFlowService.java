package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.StudentCourseOrderFlowExt;

/**
 * Service的接口
 *
 */
public interface StudentCourseOrderFlowService {


    Integer insert(StudentCourseOrderFlowExt studentCourseOrderFlowExt);
}
