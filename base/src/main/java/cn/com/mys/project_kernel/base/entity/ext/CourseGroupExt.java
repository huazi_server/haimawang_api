package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.CourseGroup;

import java.math.BigDecimal;
import java.util.List;


public class CourseGroupExt extends CourseGroup {

    private String coursePackageIds;//课程包id,多个","分隔
    private String courseIds;//课程id,多个","分隔

    public String getCoursePackageIds() {
        return coursePackageIds;
    }

    public void setCoursePackageIds(String coursePackageIds) {
        this.coursePackageIds = coursePackageIds;
    }

    public String getCourseIds() {
        return courseIds;
    }

    public void setCourseIds(String courseIds) {
        this.courseIds = courseIds;
    }

    private List<CourseExt> courseInfoList;//课程列表
    public List<CourseExt> getCourseInfoList() {
        return courseInfoList;
    }
    public void setCourseInfoList(List<CourseExt> courseInfoList) {
        this.courseInfoList = courseInfoList;
    }

    private Integer buyFlag;//购买标识(1未购买、2已购买)
    public Integer getBuyFlag() {
        return buyFlag;
    }
    public void setBuyFlag(Integer buyFlag) {
        this.buyFlag = buyFlag;
    }


    private Integer lessonCount;//总课时数量
    public Integer getLessonCount() {
        return lessonCount;
    }
    public void setLessonCount(Integer lessonCount) {
        this.lessonCount = lessonCount;
    }


    private BigDecimal activityTuanPrice;//拼团活动价
    public BigDecimal getActivityTuanPrice() {
        return activityTuanPrice;
    }
    public void setActivityTuanPrice(BigDecimal activityTuanPrice) {
        this.activityTuanPrice = activityTuanPrice;
    }


    ChannelCourseExt ChannelCourseInfo;//渠道单课程或渠道系列课
    public ChannelCourseExt getChannelCourseInfo() {
        return ChannelCourseInfo;
    }
    public void setChannelCourseInfo(ChannelCourseExt channelCourseInfo) {
        ChannelCourseInfo = channelCourseInfo;
    }

}