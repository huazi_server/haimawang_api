package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CompetitionPlayerExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CompetitionPlayerMapperExt {

    CompetitionPlayerExt findObjectByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<CompetitionPlayerExt> findListByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition4Vote(Map<String, Object> map4Param);

    List<CompetitionPlayerExt> findListByCondition4Vote(Map<String, Object> map4Param);

    CompetitionPlayerExt findById(Long id);

    CompetitionPlayerExt findLastObjectByCondition(Map<String, Object> m);
}