package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ActivityFreeExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ActivityFreeMapperExt {

    ActivityFreeExt findObjectByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ActivityFreeExt> findListByCondition(Map<String, Object> map4Param);
}