package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.CourseLesson;
import cn.com.mys.project_kernel.base.entity.StudentCourseHomework;

import java.util.Date;

public class StudentCourseHomeworkExt extends StudentCourseHomework {

    private CourseLessonExt courseLessonInfo;//课时
    private String courseName;//课程名称
    private Integer courseLessonNumber;//课时编号
    private String courseLessonTitle;//课时标题
    private String courseLessonName;//课时名称

    public CourseLessonExt getCourseLessonInfo() {
        return courseLessonInfo;
    }

    public void setCourseLessonInfo(CourseLessonExt courseLessonInfo) {
        this.courseLessonInfo = courseLessonInfo;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getCourseLessonNumber() {
        return courseLessonNumber;
    }

    public void setCourseLessonNumber(Integer courseLessonNumber) {
        this.courseLessonNumber = courseLessonNumber;
    }

    public String getCourseLessonTitle() {
        return courseLessonTitle;
    }

    public void setCourseLessonTitle(String courseLessonTitle) {
        this.courseLessonTitle = courseLessonTitle;
    }

    public String getCourseLessonName() {
        return courseLessonName;
    }
    public void setCourseLessonName(String courseLessonName) {
        this.courseLessonName = courseLessonName;
    }

    private Integer courseLessonLockFlag;//课时锁标识(1关闭、2打开)
    public Integer getCourseLessonLockFlag() {
        return courseLessonLockFlag;
    }

    public void setCourseLessonLockFlag(Integer courseLessonLockFlag) {
        this.courseLessonLockFlag = courseLessonLockFlag;
    }

    private String studentName;//学生名称
    private String studentAvatarUrl;//学生头像
    private Integer studentOnlineFlag;//在线标识(1离线、2在线)
    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentAvatarUrl() {
        return studentAvatarUrl;
    }

    public void setStudentAvatarUrl(String studentAvatarUrl) {
        this.studentAvatarUrl = studentAvatarUrl;
    }

    public Integer getStudentOnlineFlag() {
        return studentOnlineFlag;
    }

    public void setStudentOnlineFlag(Integer studentOnlineFlag) {
        this.studentOnlineFlag = studentOnlineFlag;
    }


    private Date startAt;//开始时间
    private Date endAt;//结束时间

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }
}