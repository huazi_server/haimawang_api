package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ProductionCoverMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ProductionCoverMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCoverExt;
import cn.com.mys.project_kernel.base.service.ProductionCoverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ProductionCoverServiceImpl implements ProductionCoverService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ProductionCoverMapper productionCoverMapper;

	@Autowired
	public ProductionCoverMapperExt productionCoverMapperExt;


	@Override
	public List<ProductionCoverExt> findListByCondition(Map<String, Object> map) {
		return productionCoverMapperExt.findListByCondition(map);
	}
}
