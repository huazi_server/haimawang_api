package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ProductionComment;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ProductionCommentService {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ProductionCommentExt> findListByCondition(Map<String, Object> map4Param);

    ProductionComment findById(long id);
    Integer update(ProductionComment productionComment);
    Integer insert(ProductionCommentExt productionCommentExt);
}
