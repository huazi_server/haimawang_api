package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ContestGroup;

/**
 * Service的接口
 *
 */
public interface ContestGroupService {

    ContestGroup findById(long id);
}
