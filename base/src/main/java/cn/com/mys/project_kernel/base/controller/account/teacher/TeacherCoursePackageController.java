package cn.com.mys.project_kernel.base.controller.account.teacher;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.StudentCourseHomework;
import cn.com.mys.project_kernel.base.entity.StudentCourseHomeworkMessage;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 教师课程包
 *
 */
@Controller
@RequestMapping(value = "/account/teacher/coursePackage")
public class TeacherCoursePackageController {

	Log log = LogFactory.getLog(TeacherCoursePackageController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private TeacherService teacherService;
	@Autowired
	private TeacherCourseService teacherCourseService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private CourseLessonService courseLessonService;
	@Autowired
	private StudentCourseHomeworkService studentCourseHomeworkService;
	@Autowired
	private StudentCourseHomeworkMessageService studentCourseHomeworkMessageService;
	@Autowired
	private TeacherCourseLessonService teacherCourseLessonService;
	@Autowired
	private StudentService studentService;
	@Autowired
	private CoursePackageService coursePackageService;
	@Autowired
	private CoursePackageLessonService coursePackageLessonService;


	//查询课程包列表
	@RequestMapping(value = "list")
	public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		List<CoursePackageExt> list = coursePackageService.findListByCondition(map4Param);
		Integer courseCount4Process = 0;//进行中的课程列表数量
		for(CoursePackageExt cp : list){
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("delFlag", "0");
			m.put("teacherId", teacherId);
			m.put("state", "5");
			m.put("coursePackageId", cp.getId().toString());

			courseCount4Process = courseService.findCountByCondition4Teacher(m);
			cp.setCourseCount4Process(courseCount4Process);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课时列表
	@RequestMapping(value = "lesson/list")
	public ResponseEntity<ResponseJson> list4Lesson(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String coursePackageId = (String) paramMap.get("coursePackageId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(coursePackageId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程包id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0初始化、1正常)
		map4Param.put("coursePackageId", coursePackageId);

		List<CoursePackageLessonExt> list = coursePackageLessonService.findListByCondition(map4Param);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课程列表
	@RequestMapping(value = "course/list")
	public ResponseEntity<ResponseJson> list4Course(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String coursePackageId = (String) paramMap.get("coursePackageId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(coursePackageId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程包id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("teacherId", teacherId);
		map4Param.put("page", page);

		map4Param.put("coursePackageId", coursePackageId);


		Integer count = courseService.findCountByCondition4TeacherCP(map4Param);
		List<CourseExt> list = courseService.findListByCondition4TeacherCP(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课时详情
	@RequestMapping(value = "course/lesson/detail")
	public ResponseEntity<ResponseJson> detail4CourseLesson(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(teacherCourseId) || StringUtils.isEmpty(courseLessonId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、教师课程id、课时id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("teacherId", teacherId);
		map4Param.put("courseId", courseId);
		map4Param.put("courseLessonId", courseLessonId);
		map4Param.put("teacherCourseId", teacherCourseId);

		CourseExt courseExt = courseService.findObjectByCondition(map4Param);

		CourseLessonExt courseLessonExt = courseLessonService.findObjectByCondition(map4Param);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),courseLessonExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课时下学生列表(包含作业)
	@RequestMapping(value = "course/student/homework")
	public ResponseEntity<ResponseJson> studentHomework4Course(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");

		String studyFlag = (String) paramMap.get("studyFlag");
		String onlineFlag = (String) paramMap.get("onlineFlag");
		String finishFlag = (String) paramMap.get("finishFlag");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(teacherCourseId) || StringUtils.isEmpty(courseLessonId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、教师课程id、课时id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("teacherId", teacherId);
		map4Param.put("courseId", courseId);
		map4Param.put("courseLessonId", courseLessonId);
		map4Param.put("teacherCourseId", teacherCourseId);
		CourseExt courseExt = courseService.findObjectByCondition(map4Param);
		CourseLessonExt courseLessonExt = courseLessonService.findObjectByCondition(map4Param);
		TeacherCourseLessonExt teacherCourseLessonExt = teacherCourseLessonService.findObjectByCondition(map4Param);


		map4Param.put("studyFlag", studyFlag);
		map4Param.put("onlineFlag", onlineFlag);
		map4Param.put("finishFlag", finishFlag);

		map4Param.put("page", page);

		Integer count = studentService.findCountByCondition4Course(map4Param);
		List<StudentExt> list = studentService.findListByCondition4Course(map4Param);
		//查询学生列表
		if(StringUtils.isNotEmpty(studyFlag) || StringUtils.isNotEmpty(onlineFlag) || StringUtils.isNotEmpty(finishFlag)){
			count = studentService.findCountByCondition4Homework(map4Param);
			list = studentService.findListByCondition4Homework(map4Param);
		}

		for(StudentExt s : list){
			s.setName(StringUtils.isNotEmpty(s.getName())?s.getName():Constant.studentName4Default);
			s.setAvatarUrl(StringUtils.isNotEmpty(s.getAvatarUrl())?s.getAvatarUrl():Constant.studentAvatarUrl4Default);


			Map<String, Object> m = new HashMap<String, Object>();
			m.put("delFlag", "0");
			m.put("courseId", courseId);
			m.put("courseLessonId", courseLessonId);
			m.put("teacherCourseId", teacherCourseId);
			m.put("studentId", s.getId().toString());
			StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findObjectByCondition(m);
			if(studentCourseHomeworkExt==null || studentCourseHomeworkExt.getId()==0){
				//模拟数据
				studentCourseHomeworkExt = new StudentCourseHomeworkExt();
				studentCourseHomeworkExt.setStudentId(s.getId());
				studentCourseHomeworkExt.setCourseId(Long.parseLong(courseId));
				studentCourseHomeworkExt.setCourseLessonId(Long.parseLong(courseLessonId));
				studentCourseHomeworkExt.setCoverUrl(courseLessonExt.getCoverUrl());
				studentCourseHomeworkExt.setStudyFlag(1);//学习标识(1未学习、2已学习)
				studentCourseHomeworkExt.setOnlineFlag(1);//在线标识(1离线、2在线)
				studentCourseHomeworkExt.setFinishFlag(0);//完成标识(0初始化、1待修改、2已完成)
				studentCourseHomeworkExt.setRemark("");
				studentCourseHomeworkExt.setState(1);//状态(1未保存、2未提交、3已提交)
				studentCourseHomeworkExt.setCreatedAt(new Date());
				studentCourseHomeworkExt.setUpdatedAt(new Date());
				studentCourseHomeworkExt.setDelFlag(0);

				studentCourseHomeworkExt.setStudentName(StringUtils.isNotEmpty(s.getName())?s.getName():Constant.studentName4Default);
				studentCourseHomeworkExt.setStudentAvatarUrl(StringUtils.isNotEmpty(s.getAvatarUrl())?s.getAvatarUrl():Constant.studentAvatarUrl4Default);


				studentCourseHomeworkExt.setCourseLessonTitle(courseLessonExt.getTitle());
				studentCourseHomeworkExt.setCourseLessonName(courseLessonExt.getName());
				studentCourseHomeworkExt.setCourseLessonNumber(courseLessonExt.getNumber());
				String coverUrl = studentCourseHomeworkExt.getCoverUrl();
				studentCourseHomeworkExt.setCoverUrl(StringUtils.isEmpty(coverUrl)?courseLessonExt.getCoverUrl():coverUrl);
				studentCourseHomeworkExt.setCourseName((courseExt!=null&&courseExt.getId()>0&&StringUtils.isNotEmpty(courseExt.getName()))?courseExt.getName():"");
			}
			s.setStudentCourseHomeworkInfo(studentCourseHomeworkExt);
		}

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询课程下学生列表
	@RequestMapping(value = "course/student/list")
	public ResponseEntity<ResponseJson> studentList4Course(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String courseId = (String) paramMap.get("courseId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");

		String fullStudyFlag = (String) paramMap.get("fullStudyFlag");//是否全部学习标识(1否、2是)
		String fullFinishFlag = (String) paramMap.get("fullFinishFlag");//是否全部完成标识(1否、2是)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		TeacherExt teacherExt = teacherService.findByAccountId(accountId);
		String teacherId = teacherExt!=null?String.valueOf(teacherExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(teacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(teacherCourseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、教师课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("teacherId", teacherId);
		map4Param.put("courseId", courseId);
		map4Param.put("teacherCourseId", teacherCourseId);
		CourseExt courseExt = courseService.findObjectByCondition(map4Param);

		map4Param.put("fullStudyFlag", fullStudyFlag);
		map4Param.put("fullFinishFlag", fullFinishFlag);

		map4Param.put("page", page);

		//查询学生列表
		Integer count = studentService.findCountByCondition4Course(map4Param);
		List<StudentExt> list = studentService.findListByCondition4Course(map4Param);

		for(StudentExt s : list){
			s.setName(StringUtils.isNotEmpty(s.getName())?s.getName():Constant.studentName4Default);
			s.setAvatarUrl(StringUtils.isNotEmpty(s.getAvatarUrl())?s.getAvatarUrl():Constant.studentAvatarUrl4Default);


			Map<String, Object> m = new HashMap<String, Object>();
			m.put("delFlag", "0");
			m.put("courseId", courseId);
			//m.put("teacherCourseId", teacherCourseId);
			m.put("studentId", s.getId().toString());
			List<StudentCourseHomeworkExt> homeworkList = studentCourseHomeworkService.findListByCondition(m);
			if(homeworkList==null || homeworkList.size()==0){
				homeworkList = new ArrayList<>();
				for(CourseLessonExt cl : courseExt.getCourseLessonInfoList()){
					StudentCourseHomeworkExt studentCourseHomeworkExt = new StudentCourseHomeworkExt();
					studentCourseHomeworkExt.setStudentId(s.getId());
					studentCourseHomeworkExt.setCourseId(Long.parseLong(courseId));
					studentCourseHomeworkExt.setCourseLessonId(cl.getId());
					studentCourseHomeworkExt.setCoverUrl(cl.getCoverUrl());
					studentCourseHomeworkExt.setStudyFlag(1);//学习标识(1未学习、2已学习)
					studentCourseHomeworkExt.setOnlineFlag(1);//在线标识(1离线、2在线)
					studentCourseHomeworkExt.setFinishFlag(0);//完成标识(0初始化、1待修改、2已完成)
					studentCourseHomeworkExt.setRemark("");
					studentCourseHomeworkExt.setState(1);//状态(1未保存、2未提交、3已提交)
					studentCourseHomeworkExt.setCreatedAt(new Date());
					studentCourseHomeworkExt.setUpdatedAt(new Date());
					studentCourseHomeworkExt.setDelFlag(0);

					studentCourseHomeworkExt.setStudentName(StringUtils.isNotEmpty(s.getName())?s.getName():Constant.studentName4Default);
					studentCourseHomeworkExt.setStudentAvatarUrl(StringUtils.isNotEmpty(s.getAvatarUrl())?s.getAvatarUrl():Constant.studentAvatarUrl4Default);


					studentCourseHomeworkExt.setCourseLessonTitle(cl.getTitle());
					studentCourseHomeworkExt.setCourseLessonName(cl.getName());
					studentCourseHomeworkExt.setCourseLessonNumber(cl.getNumber());
					String coverUrl = studentCourseHomeworkExt.getCoverUrl();
					studentCourseHomeworkExt.setCoverUrl(StringUtils.isEmpty(coverUrl)?cl.getCoverUrl():coverUrl);
					studentCourseHomeworkExt.setCourseName((courseExt!=null&&courseExt.getId()>0&&StringUtils.isNotEmpty(courseExt.getName()))?courseExt.getName():"");

					homeworkList.add(studentCourseHomeworkExt);
				}
			}
			s.setStudentCourseHomeworkInfoList(homeworkList);
		}

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}











}
