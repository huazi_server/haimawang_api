package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ChannelCourseMapper;
import cn.com.mys.project_kernel.base.dao.CourseGroupMapper;
import cn.com.mys.project_kernel.base.dao.CourseMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ChannelCourseMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.DiscountMapperExt;
import cn.com.mys.project_kernel.base.entity.ChannelCourse;
import cn.com.mys.project_kernel.base.entity.Course;
import cn.com.mys.project_kernel.base.entity.CourseGroup;
import cn.com.mys.project_kernel.base.entity.Discount;
import cn.com.mys.project_kernel.base.entity.ext.ChannelCourseExt;
import cn.com.mys.project_kernel.base.entity.ext.DiscountExt;
import cn.com.mys.project_kernel.base.service.ChannelCourseService;
import cn.com.mys.project_kernel.base.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ChannelCourseServiceImpl implements ChannelCourseService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ChannelCourseMapper channelCourseMapper;

	@Autowired
	public ChannelCourseMapperExt channelCourseMapperExt;

	@Autowired
	public CourseMapper courseMapper;
	@Autowired
	public CourseGroupMapper courseGroupMapper;
	@Autowired
	public DiscountMapperExt discountMapperExt;


	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return channelCourseMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ChannelCourseExt> findListByCondition(Map<String, Object> map4Param) {
		List<ChannelCourseExt> list = channelCourseMapperExt.findListByCondition(map4Param);

		for(ChannelCourseExt cc : list){
			Integer courseType = cc.getCourseType();//课程类型(1单课程、2系列课)
			if(courseType==1){
				Course course = courseMapper.selectByPrimaryKey(cc.getCourseId());
				if(course!=null && course.getId()>0){
					cc.setCourseName(course.getName());
					cc.setCourseIssue(course.getIssue());
				}
			}
			if(courseType==2){
				CourseGroup courseGroup = courseGroupMapper.selectByPrimaryKey(cc.getCourseGroupId());
				if(courseGroup!=null && courseGroup.getId()>0){
					cc.setCourseName(courseGroup.getName());
					cc.setCourseIssue(courseGroup.getIssue());
				}
			}

			String discountIds = cc.getDiscountIds();
			if(StringUtils.isNotEmpty(discountIds)){
				List<DiscountExt> discountInfoList = discountMapperExt.findByIds(discountIds);
				cc.setDiscountInfoList(discountInfoList);
			}

		}
		return list;
	}

	@Override
	public Integer insert(ChannelCourse cc) {
		return channelCourseMapper.insertSelective(cc);
	}

	@Override
	public ChannelCourse findById(long channelCourseId) {
		return channelCourseMapper.selectByPrimaryKey(channelCourseId);
	}

	@Override
	public Integer update(ChannelCourse cc) {
		return channelCourseMapper.updateByPrimaryKeySelective(cc);
	}

	@Override
	public ChannelCourseExt findObjectByCondition(Map<String, Object> map4Param) {
		return channelCourseMapperExt.findObjectByCondition(map4Param);
	}
}
