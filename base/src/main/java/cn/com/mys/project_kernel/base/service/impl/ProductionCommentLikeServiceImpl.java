package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ProductionCommentLikeMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ProductionCommentLikeMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentLikeExt;
import cn.com.mys.project_kernel.base.service.ProductionCommentLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ProductionCommentLikeServiceImpl implements ProductionCommentLikeService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ProductionCommentLikeMapper productionCommentLikeMapper;

	@Autowired
	public ProductionCommentLikeMapperExt productionCommentLikeMapperExt;


	@Override
	public ProductionCommentLikeExt findObjectByCondition(Map<String, Object> map4Param) {
		return productionCommentLikeMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer insert(ProductionCommentLikeExt productionCommentLikeExt) {
		return productionCommentLikeMapper.insertSelective(productionCommentLikeExt);
	}

	@Override
	public Integer delete(ProductionCommentLikeExt productionCommentLikeExt) {
		return productionCommentLikeMapper.deleteByPrimaryKey(productionCommentLikeExt.getId());
	}
}
