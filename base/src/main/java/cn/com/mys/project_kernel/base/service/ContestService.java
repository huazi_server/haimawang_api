package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ContestExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ContestService {

    ContestExt findById(long id);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ContestExt> findListByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition4Join(Map<String, Object> map4Param);

    List<ContestExt> findListByCondition4Join(Map<String, Object> map4Param);

    Integer findCountByCondition4Create(Map<String, Object> map4Param);

    List<ContestExt> findListByCondition4Create(Map<String, Object> map4Param);

    Integer insert(ContestExt contestExt);

    Integer update(ContestExt contestExt);

    List<ContestExt> findAllListByCondition(Map<String, Object> map);
}
