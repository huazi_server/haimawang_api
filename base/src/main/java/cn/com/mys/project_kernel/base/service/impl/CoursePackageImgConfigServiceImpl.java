package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CoursePackageImgConfigMapper;
import cn.com.mys.project_kernel.base.dao.ResourceFileMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CoursePackageImgConfigMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.ResourceFileMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CoursePackageImgConfigExt;
import cn.com.mys.project_kernel.base.service.CoursePackageImgConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class CoursePackageImgConfigServiceImpl implements CoursePackageImgConfigService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CoursePackageImgConfigMapper coursePackageImgConfigMapper;

	@Autowired
	public CoursePackageImgConfigMapperExt coursePackageImgConfigMapperExt;
	@Autowired
	public ResourceFileMapper resourceFileMapper;
	@Autowired
	public ResourceFileMapperExt resourceFileMapperExt;


	@Override
	public List<CoursePackageImgConfigExt> findListByCondition(Map<String, Object> map4Param) {
		return coursePackageImgConfigMapperExt.findListByCondition(map4Param);
	}

}
