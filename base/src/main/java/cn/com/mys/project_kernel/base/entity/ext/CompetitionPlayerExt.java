package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.CompetitionPlayer;

public class CompetitionPlayerExt extends CompetitionPlayer {

    private CompetitionProductionExt competitionProductionInfo;
    public CompetitionProductionExt getCompetitionProductionInfo() {
        return competitionProductionInfo;
    }
    public void setCompetitionProductionInfo(CompetitionProductionExt competitionProductionInfo) {
        this.competitionProductionInfo = competitionProductionInfo;
    }


}