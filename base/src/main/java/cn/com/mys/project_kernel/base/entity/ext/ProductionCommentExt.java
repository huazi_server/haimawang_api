package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ProductionComment;

import java.util.List;

public class ProductionCommentExt extends ProductionComment {

    private Integer productionCommentReplyCount;//评论的回复数量
    private List<ProductionCommentReplyExt> productionCommentReplyInfoList;//评论的回复列表
    private Integer likeFlag;//点赞标识(1未点赞、2已点赞)
    private Integer selfFlag;//所属标识(1不属于自己、2属于自己)

    public Integer getProductionCommentReplyCount() {
        return productionCommentReplyCount;
    }

    public void setProductionCommentReplyCount(Integer productionCommentReplyCount) {
        this.productionCommentReplyCount = productionCommentReplyCount;
    }

    public List<ProductionCommentReplyExt> getProductionCommentReplyInfoList() {
        return productionCommentReplyInfoList;
    }

    public void setProductionCommentReplyInfoList(List<ProductionCommentReplyExt> productionCommentReplyInfoList) {
        this.productionCommentReplyInfoList = productionCommentReplyInfoList;
    }

    public Integer getLikeFlag() {
        return likeFlag;
    }

    public void setLikeFlag(Integer likeFlag) {
        this.likeFlag = likeFlag;
    }

    public Integer getSelfFlag() {
        return selfFlag;
    }

    public void setSelfFlag(Integer selfFlag) {
        this.selfFlag = selfFlag;
    }
}