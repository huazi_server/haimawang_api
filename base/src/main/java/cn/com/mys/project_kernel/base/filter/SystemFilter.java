package cn.com.mys.project_kernel.base.filter;

import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.filter.wrapper.*;

import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;





public class SystemFilter implements Filter {

	Log log = LogFactory.getLog(SystemFilter.class);


	//loginToken验证接口地址
	String account4LoginTokenUrl = PropertiesUtils.getKeyValue("account4LoginTokenUrl");


	//验证loginToken
	public Map<String, Object> verifyLoginToken(String loginToken){

		Map<String, Object> map = new HashMap<String, Object>();

		String userId = "";
		ResponseJson responseJson = new ResponseJson();

		JSONObject obj = new JSONObject();
		obj.put("loginToken", loginToken);
		obj.put("loginProductId", Constant.loginProductId);
		obj.put("loginProductKey", Constant.loginProductKey);
		JSONObject jo = HttpUtils.sendPost4JSON(account4LoginTokenUrl, obj);
		log.info("--"+jo.toString());

		if(jo!=null && jo.getJSONObject("code")!=null && jo.getJSONObject("code").toString()!=null && "200".equals(jo.getJSONObject("code").toString())){
			JSONObject jo_data = jo.getJSONObject("data");
			String accountId = jo_data.getString("id");//accountId

			//查询相应的教师或学生的实际id
			userId = accountId;
			responseJson = null;

			map.put("userId",userId);
			map.put("responseJson",responseJson);
		}else{
			userId = "";
			responseJson = new ResponseJson(ResponseJson.FALSE,"401",ResponseJson.FAIL,"token失效或用户不存在",0,null);

			map.put("userId",userId);
			map.put("responseJson",responseJson);
		}
		return map;
	}




	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
		ResponseJson responseJsonResult = null;
		String userId = "";
		
		//获得在下面代码中要用的request,response,session对象
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpSession session = request.getSession();


		String encoding = request.getCharacterEncoding();
		Map mp = request.getParameterMap();

		//只对post请求方式有效(设置编码)
		request.setCharacterEncoding("utf-8");//解决request乱码
		response.setContentType("application/json;charset=UTF-8"); //解决response乱码

		//如果请求方式是get,就进行转码操作
		if("GET".equalsIgnoreCase(request.getMethod())){
			CharacterRequestWrapper characterRequestWrapper = null;
			if (request.getQueryString()!=null && !"".equals(request.getQueryString())) {

				log.debug("转码之前参数：--"+request.getParameterMap());
				//包装request对象
				characterRequestWrapper = new CharacterRequestWrapper(request); //使用包装的request
				log.debug("转码之后参数：--"+characterRequestWrapper.getParameterMap());
				//转码后重新设置request
				request = characterRequestWrapper;
			}

			//获取map(request)中的参数
			String loginToken = request.getParameter("loginToken");

			/*//验证token
			Map<String, Object> map = verifyLoginToken(loginToken);
			userId = (String) map.get("userId");
			responseJsonResult = (ResponseJson) map.get("responseJsonResult");*/
		}



		//如果请求方式是post,就进行InputStream的重新压入操作
		if("POST".equalsIgnoreCase(request.getMethod())){
			String contentType = request.getContentType();
			contentType = StringUtils.isEmpty(contentType)?"X-WWW-FORM-URLENCODED":contentType.toUpperCase();//如果是空就设置为普通表单形式

			//String contentType = request.getContentType().toUpperCase();
			if(contentType.indexOf("APPLICATION/JSON")>-1){
				// 防止流读取一次后就没有了, 所以需要将流继续缓存住
				request = new BodyReaderHttpServletRequestWrapper(request);
				//String body = HttpHelper.getBodyString(request);
			}
			else if(contentType.indexOf("X-WWW-FORM-URLENCODED")>-1){
				ModifyParametersWrapper mParametersWrapper = new ModifyParametersWrapper(request);
				mParametersWrapper.putHeader("contentType","application/json;charset=UTF-8");//属性为contentType
				request = mParametersWrapper;

				//处理参数
				Map<String, Object> m = new HashMap<String, Object>();
				String body = HttpHelper.getBodyString(request);
				if(body!=null && !"".equals(body)){
					String[] array = body.split("&");
					for(String a : array){
						String[] kv = a.split("=");
						if(kv.length==1){
							m.put(kv[0],"");
						}else{
							m.put(kv[0],kv[kv.length-1]);
						}

					}
				}

				body = JSONObject.fromObject(m).toString();
				// 防止流读取一次后就没有了, 所以需要将流继续缓存住
				request = new BodyReaderHttpServletRequestWrapper(request,body);
				//String body = HttpHelper.getBodyString(request);
			}
			else if(contentType.indexOf("MULTIPART/FORM-DATA")>-1){
				CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
				MultipartHttpServletRequest multipartRequest = multipartResolver.resolveMultipart(request);
				//Map<String, String[]> map = multipartRequest.getParameterMap();
				Map<String, Object> m = ParamUtils.requestParameter2Map(multipartRequest);
				String body = "";

				body = JSONObject.fromObject(m).toString();
				// 防止流读取一次后就没有了, 所以需要将流继续缓存住
				BodyReaderHttpServletRequestWrapper bodyReaderRequest = new BodyReaderHttpServletRequestWrapper(multipartRequest,body);

				//String body = HttpHelper.getBodyString(request);

				String fileParamName = "upload";//文件上传的参数名称
				List<MultipartFile> files = multipartRequest.getFiles(fileParamName);//获取文件
				if(files!=null && files.size()>0){
					bodyReaderRequest.setMultipartRequest(multipartRequest);
				}

				request = bodyReaderRequest;
			}


			//request的post请求中json数据转换为map(原始request中的InputStream只能读取一次,这里用的是包装后的request)
			Map<String, Object> paramMap = RequestUtils.parseRequest(request);
			if(paramMap==null || paramMap.size()==0){
				paramMap = ParamUtils.requestParameter2Map(request);
			}
			//获取map(request)中的参数
			String loginToken = (String) RequestUtils.getRequestBodyValue(paramMap, "loginToken");

			/*//验证token
			Map<String, Object> map = verifyLoginToken(loginToken);
			userId = (String) map.get("userId");
			responseJsonResult = (ResponseJson) map.get("responseJsonResult");*/
		}


		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);
		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}


		/*if(responseJsonResult!=null){
			try{
				PrintWriter out = response.getWriter();
				out.append(JSONObject.fromObject(responseJsonResult).toString());
				return;
			}catch (Exception e){
				e.printStackTrace();
				response.sendError(500);
				return;
			}
		}else{
			//验证通过,放行
			filterChain.doFilter(request, response);
			return;
		}*/


		/*if(paramMap!=null && paramMap.size()>0){
			String appId = (String) paramMap.get("appId");
			String appKey = (String) paramMap.get("appKey");
			if(StringUtils.isEmpty(appId) || StringUtils.isEmpty(appKey)){
				try{
					PrintWriter out = response.getWriter();
					ResponseJson rjResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"appId、appKey不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					out.append(JSONObject.fromObject(rjResult).toString());
					return;
				}catch (Exception e){
					e.printStackTrace();
					response.sendError(500);
					return;
				}
			}
		}*/


		long duration = System.currentTimeMillis();//controller执行的开始时间
		request.setAttribute("duration", duration);

		//验证通过,放行
		filterChain.doFilter(request, response);
		return;

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}



	
	
	
}
