package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CoursePackageImgConfigExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface CoursePackageImgConfigMapperExt {

    List<CoursePackageImgConfigExt> findListByCondition(Map<String, Object> map4Param);
}