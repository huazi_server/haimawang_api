package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.Notify;
import cn.com.mys.project_kernel.base.entity.ext.NotifyExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface NotifyService {


    Integer findCountByCondition(Map<String, Object> map4Param);
    List<NotifyExt> findListByCondition(Map<String, Object> map4Param);

    Notify findById(long id);
    Integer update(NotifyExt notify);
    Integer delete(NotifyExt notify);

    Integer updateByCondition(Map<String, Object> map4Param);
    Integer deleteByCondition(Map<String, Object> map4Param);

    Integer insert(NotifyExt notify);
}
