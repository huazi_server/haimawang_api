package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CompetitionMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CompetitionMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CompetitionExt;
import cn.com.mys.project_kernel.base.service.CompetitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class CompetitionServiceImpl implements CompetitionService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CompetitionMapper competitionMapper;

	@Autowired
	public CompetitionMapperExt competitionMapperExt;


	@Override
	public CompetitionExt findById(long id) {
		return competitionMapperExt.findById(id);
	}

	@Override
	public List<CompetitionExt> findAllListByCondition(Map<String, Object> map) {
		return competitionMapperExt.findAllListByCondition(map);
	}

	@Override
	public Integer update(CompetitionExt competition) {
		return competitionMapper.updateByPrimaryKeySelective(competition);
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return competitionMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<CompetitionExt> findListByCondition(Map<String, Object> map4Param) {
		return competitionMapperExt.findListByCondition(map4Param);
	}
}
