package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ChannelMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ChannelMapperExt;
import cn.com.mys.project_kernel.base.entity.Channel;
import cn.com.mys.project_kernel.base.entity.ext.ChannelExt;
import cn.com.mys.project_kernel.base.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ChannelServiceImpl implements ChannelService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ChannelMapper channelMapper;

	@Autowired
	public ChannelMapperExt channelMapperExt;


	@Override
	public Channel findById(long id) {
		return channelMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer update(Channel channel) {
		return channelMapper.updateByPrimaryKeySelective(channel);
	}

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return channelMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ChannelExt> findListByCondition(Map<String, Object> map4Param) {
		return channelMapperExt.findListByCondition(map4Param);
	}

	@Override
	public Integer insert(ChannelExt channelExt) {
		return channelMapper.insertSelective(channelExt);
	}

	@Override
	public ChannelExt findByAccountId(String accountId) {
		return channelMapperExt.findByAccountId(accountId);
	}

	@Override
	public ChannelExt findByPhone(String phone) {
		return channelMapperExt.findByPhone(phone);
	}
}
