package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ContestSignupConfig;

import java.util.List;

public class ContestSignupConfigExt extends ContestSignupConfig {

    private List<ContestGroupExt> contestGroupInfoList;

    public List<ContestGroupExt> getContestGroupInfoList() {
        return contestGroupInfoList;
    }

    public void setContestGroupInfoList(List<ContestGroupExt> contestGroupInfoList) {
        this.contestGroupInfoList = contestGroupInfoList;
    }
}