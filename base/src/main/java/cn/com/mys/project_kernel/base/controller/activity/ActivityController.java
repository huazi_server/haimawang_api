package cn.com.mys.project_kernel.base.controller.activity;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.ChannelActivity;
import cn.com.mys.project_kernel.base.entity.Course;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 *
 * @author mingjun_T
 * 活动
 *
 */
@Controller
@RequestMapping(value = "/activity")
public class ActivityController {

	Log log = LogFactory.getLog(ActivityController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private ActivityFreeService activityFreeService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private StudentCourseService studentCourseService;

	@Autowired
	private CourseGroupService courseGroupService;
	@Autowired
	private CoursePackageService coursePackageService;

	@Autowired
	private ActivityTuanService activityTuanService;
	@Autowired
	private ActivityTuanOrderService activityTuanOrderService;

	@Autowired
	private StudentCourseOrderService studentCourseOrderService;


	@Autowired
	private ChannelActivityService channelActivityService;


	//获取免费活动信息
	@RequestMapping(value = "free/info")
	public ResponseEntity<ResponseJson> info4Free(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String activityId = (String) paramMap.get("activityId");//活动id
		String addCountFlag = (String) paramMap.get("addCountFlag");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		/*if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);

		Integer gainFlag = 1;//1未领过、2已领过
		ActivityFreeExt activityFreeExt = activityFreeService.findObjectByCondition(map4Param);
		if(activityFreeExt!=null && activityFreeExt.getState()!=null && activityFreeExt.getState()!=2){//状态(1下线、2上线)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动未上线",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		if(activityFreeExt!=null && activityFreeExt.getCourseId()!=null){
			Map<String, Object> cm = new HashMap<String, Object>();
			cm.put("delFlag", "0");
			cm.put("courseId", activityFreeExt.getCourseId());
			CourseExt courseExt = courseService.findObjectByCondition(cm);
			activityFreeExt.setCourseInfo(courseExt);

			if(courseExt!=null && courseExt.getId()>0){
				map4Param = new HashMap<String, Object>();
				map4Param.put("studentId",studentId);
				map4Param.put("delFlag","0");
				//map4Param.put("courseId",courseId);
				map4Param.put("coursePackageId",courseExt.getCoursePackageId());
				StudentCourseExt sc = studentCourseService.findObjectByCondition(map4Param);
				if(sc!=null && sc.getId()>0){
					gainFlag = 2;
				}
			}
		}
		if(activityFreeExt!=null && activityFreeExt.getCourseGroupId()!=null){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("courseGroupId", activityFreeExt.getCourseGroupId());
			CourseGroupExt courseGroupExt = courseGroupService.findObjectByCondition(mm);
			activityFreeExt.setCourseGroupInfo(courseGroupExt);

			if(courseGroupExt!=null && courseGroupExt.getId()>0){
				Map<String, Object> m = new HashMap<>();
				m.put("studentId", studentId);
				m.put("delFlag","0");
				List<StudentCourseExt> studentCourseInfoList = studentCourseService.findListByCondition4CourseGroup(m);
				if(studentCourseInfoList!=null && studentCourseInfoList.size()>0){
					gainFlag = 2;
				}
			}
		}
		activityFreeExt.setGainFlag(gainFlag);



		//是否增加浏览量次数标识(1否、2是)
		if(StringUtils.isNotEmpty(addCountFlag) && Integer.parseInt(addCountFlag)==2){
			activityFreeExt.setLookCount(activityFreeExt.getLookCount()==null?0+1:activityFreeExt.getLookCount()+1);
			activityFreeService.update(activityFreeExt);
		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityFreeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//更新免费活动的浏览量
	@RequestMapping(value = "free/look")
	public ResponseEntity<ResponseJson> look4Free(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String activityId = (String) paramMap.get("activityId");//活动id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);

		ActivityFreeExt activityFreeExt = activityFreeService.findObjectByCondition(map4Param);
		if(activityFreeExt!=null){
			activityFreeExt.setLookCount(activityFreeExt.getLookCount()==null?0+1:activityFreeExt.getLookCount()+1);
			activityFreeService.update(activityFreeExt);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityFreeExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	/***************************************************************************************************************/
	/***************************************************************************************************************/
	/***************************************************************************************************************/



	//获取拼团活动信息
	@RequestMapping(value = "tuan/info")
	public ResponseEntity<ResponseJson> info4Tuan(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String activityId = (String) paramMap.get("activityId");//活动id
		String channelId = (String) paramMap.get("channelId");//渠道id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		if(StringUtils.isEmpty(activityId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("activityId", activityId);

		ActivityTuanExt activityTuanExt = activityTuanService.findObjectByCondition(map4Param);
		if(activityTuanExt==null || activityTuanExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ChannelActivityExt channelActivityExt = null;
		if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(activityId)){
			Map<String, Object> mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("state", "1");//状态(0初始化、1正常)
			mm.put("activityType", "1");//活动类型(0免费领、1拼团)
			mm.put("channelId", channelId);
			mm.put("activityId", activityId);
			channelActivityExt = channelActivityService.findObjectByCondition(mm);
			if(channelActivityExt==null){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"渠道活动不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
			if(channelActivityExt.getPrice()==null || channelActivityExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelActivityExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"价格信息不完整",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}


		if(activityTuanExt!=null && activityTuanExt.getState()!=null && activityTuanExt.getState()!=1){//状态(0未上线、1已上线)
			/*ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动未上线",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);*/



			//单课程id,多个","分隔
			String courseIds = "";
			//系列课id,多个","分隔
			String courseGroupIds = "";

			if(activityTuanExt.getCourseType()!=null && activityTuanExt.getCourseType()==1){//课程类型(1单课程、2系列课)
				//查询可以购买的单课程
				Map<String, Object> mp = new HashMap<String, Object>();
				mp.put("studentId",studentId);
				mp.put("delFlag","0");
				mp.put("enableFlag","1");//启用标记(1正常、2禁用)
				mp.put("state","3");//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
				mp.put("courseType", "1");//课程类型(1单课程、2系列课)
				mp.put("name","");
				List<CourseExt> list = courseService.findListByCondition4All(mp);
				//只保留正常数据
				CopyOnWriteArrayList<CourseExt> cowList = new CopyOnWriteArrayList<CourseExt>(list);
				for (CourseExt item : cowList) {
					if(item!=null && item.getCourseFlag()!=null && item.getCourseFlag()==2){//课程标识(1需购买课程、2非购买课程)
						cowList.remove(item);
					}
				}
				list = cowList;
				//按课程包分组
				Map<String, List> m = new HashMap<String, List>();
				for(CourseExt c : list){
					if(m.get(c.getCoursePackageId().toString())==null){
						List<CourseExt> cl = new ArrayList();
						cl.add(c);
						m.put(c.getCoursePackageId().toString(), cl);
					}else{
						m.get(c.getCoursePackageId().toString()).add(c);
					}
				}
				List<CoursePackageExt> _l = new ArrayList<>();
				for (Map.Entry<String, List> entry : m.entrySet()) {
					System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
					Map<String, Object> mm = new HashMap<String, Object>();
					mm.put("coursePackageId", entry.getKey());
					mm.put("delFlag","0");
					CoursePackageExt coursePackageExt = coursePackageService.findObjectByCoursePackageId(mm);
					coursePackageExt.setCourseInfoList(entry.getValue());
					_l.add(coursePackageExt);
				}

				//单课程id,多个","分隔
				for(CourseExt c : list){
					courseIds = StringUtils.isNotEmpty(courseIds)?(courseIds+","+c.getId()):courseIds+c.getId();
				}
			}
			if(activityTuanExt.getCourseType()!=null && activityTuanExt.getCourseType()==2){//课程类型(1单课程、2系列课)
				//查询可以购买的系列课
				Map<String, Object> mp = new HashMap<String, Object>();
				mp.put("studentId",studentId);
				mp.put("delFlag","0");
				mp.put("enableFlag","1");//启用标记(1正常、2禁用)
				mp.put("state","2");//状态,1待上线、2已上线
				mp.put("name","");
				List<CourseGroupExt> list = courseGroupService.findListByCondition4All(map4Param);
				//只保留正常数据
				CopyOnWriteArrayList<CourseGroupExt> cowList = new CopyOnWriteArrayList<CourseGroupExt>(list);
				for (CourseGroupExt item : cowList) {
					if(item!=null && item.getCourseGroupFlag()!=null && item.getCourseGroupFlag()==2){//课程分组标识(1正常课程分组、2活动课程分组)
						cowList.remove(item);
					}
				}
				list = cowList;

				//系列课id,多个","分隔
				for(CourseGroupExt cg : list){
					courseGroupIds = StringUtils.isNotEmpty(courseGroupIds)?(courseGroupIds+","+cg.getId()):courseGroupIds+cg.getId();
				}
			}

			activityTuanExt.setCourseIds(courseIds);
			activityTuanExt.setCourseGroupIds(courseGroupIds);
			activityTuanExt.setUpdatedAt(new Date());
			activityTuanService.update(activityTuanExt);
		}
		Date startAt = activityTuanExt.getStartAt();
		Date endAt = activityTuanExt.getEndAt();
		if(startAt!=null && startAt.getTime()>new Date().getTime()){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动未开始",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		/*if(endAt!=null && endAt.getTime()<new Date().getTime()){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动已结束",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		String courseIds = activityTuanExt.getCourseIds();
		String courseGroupIds = activityTuanExt.getCourseGroupIds();

		if(StringUtils.isNotEmpty(courseIds)){
			List<CourseExt> courseInfoList = new ArrayList<>();//单课程列表
			String[] cIds = courseIds.split(",");
			for(String cId : cIds){
				Map<String, Object> m = new HashMap<>();
				m.put("studentId",studentId);
				m.put("delFlag", "0");
				m.put("state", "3");//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
				m.put("enableFlag","1");//启用标记(1正常、2禁用)
				m.put("courseId", cId);
				CourseExt courseExt = courseService.findObjectByCondition(m);
				if(courseExt!=null && courseExt.getId()>0){
					courseExt.setActivityTuanPrice(activityTuanExt.getPrice());
					courseInfoList.add(courseExt);
				}
			}
			activityTuanExt.setCourseInfoList(courseInfoList);
		}

		if(StringUtils.isNotEmpty(courseGroupIds)){
			List<CourseGroupExt> courseGroupInfoList = new ArrayList<>();//系列课列表
			String[] cgIds = courseGroupIds.split(",");
			for(String cgId : cgIds){
				Map<String, Object> m = new HashMap<>();
				m.put("studentId",studentId);
				m.put("delFlag", "0");
				m.put("state", "2");//状态, 1待上线、2已上线
				m.put("enableFlag","1");//启用标记(1正常、2禁用)
				m.put("courseGroupId", cgId);
				CourseGroupExt courseGroupExt = courseGroupService.findObjectByCondition(m);
				if(courseGroupExt!=null && courseGroupExt.getId()>0){
					courseGroupExt.setActivityTuanPrice(activityTuanExt.getPrice());
					courseGroupInfoList.add(courseGroupExt);
				}
			}
			activityTuanExt.setCourseGroupInfoList(courseGroupInfoList);
		}


		//当没有单课程或系列课数据，活动自动下线
		if((activityTuanExt.getCourseInfoList()==null || activityTuanExt.getCourseInfoList().size()==0) && (activityTuanExt.getCourseGroupInfoList()==null || activityTuanExt.getCourseGroupInfoList().size()==0)){
			activityTuanExt.setState(0);//状态(0未上线、1已上线)
			activityTuanService.update(activityTuanExt);
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"活动未上线",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Integer stopFlag = 1;//结束标识(1未结束、2已结束)
		if(endAt!=null && endAt.getTime()<new Date().getTime()){
			stopFlag = 2;
		}
		activityTuanExt.setStopFlag(stopFlag);


		Integer joinFlag = 1;//参加标识(1未参加、已参加)
		if(StringUtils.isNotEmpty(studentId)){
			Map<String, Object> m4PinTuan = new HashMap<>();
			m4PinTuan.put("delFlag", "0");
			m4PinTuan.put("activityTuanId", activityTuanExt.getId().toString());
			m4PinTuan.put("studentId", studentId);
			m4PinTuan.put("state", "1,2");//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
			ActivityTuanOrderExt ato = activityTuanOrderService.findObjectByActivityId4Join(m4PinTuan);
			if(ato!=null && ato.getId()>0){
				joinFlag = 2;
			}
		}
		activityTuanExt.setJoinFlag(joinFlag);



		if(Constant.createValidFlag4PinTuan==2){
			Integer createPinTuanOrderFlag = 1;//能否新建拼团订单标识(1是、2否)
			if(activityTuanExt.getEndAt()!=null && DateUtils.addHours(activityTuanExt.getEndAt(), 0-Constant.createEndDate4PinTuan).getTime()<new Date().getTime()){
				createPinTuanOrderFlag = 2;//开团有效时间已过
			}
			activityTuanExt.setCreatePinTuanOrderFlag(createPinTuanOrderFlag);
		}


		activityTuanExt.setChannelActivityInfo(channelActivityExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),activityTuanExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	/**********************************************************************************************************/
	/**********************************************************************************************************/
	/**********************************************************************************************************/














}
