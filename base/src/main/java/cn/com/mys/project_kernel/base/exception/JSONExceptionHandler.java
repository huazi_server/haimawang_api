package cn.com.mys.project_kernel.base.exception;

import cn.com.mys.project_kernel.base.vo.ResponseJson;
import org.apache.log4j.spi.ErrorCode;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by admin on 2018/4/28.
 */

@ControllerAdvice
public class JSONExceptionHandler {


    /*----- INTERNAL SERVER ERROR -----*/

    //运行时异常
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseJson runtimeExceptionHandler(RuntimeException runtimeException) {
        return  new ResponseJson(ResponseJson.FALSE,HttpStatus.INTERNAL_SERVER_ERROR.value(),ResponseJson.FAIL,"SERVER: Runtime Exception",0,null);
    }

    //空指针异常
    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseJson nullPointerExceptionHandler(NullPointerException ex) {
        ex.printStackTrace();
        return  new ResponseJson(ResponseJson.FALSE,HttpStatus.INTERNAL_SERVER_ERROR.value(),ResponseJson.FAIL,"SERVER: Null Pointer Exception",0,null);
    }


    /*----- REQUEST ERROR -----*/

    //400错误
    @ExceptionHandler({HttpMessageNotReadableException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseJson requestNotReadable(HttpMessageNotReadableException ex){
        System.out.println("400..requestNotReadable");
        ex.printStackTrace();
        return  new ResponseJson(ResponseJson.FALSE,HttpStatus.BAD_REQUEST.value(),ResponseJson.FAIL,"Request Not Readable",0,null);
    }

    //400错误
    @ExceptionHandler({TypeMismatchException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseJson requestTypeMismatch(TypeMismatchException ex){
        System.out.println("400..TypeMismatchException");
        ex.printStackTrace();
        return  new ResponseJson(ResponseJson.FALSE,HttpStatus.BAD_REQUEST.value(),ResponseJson.FAIL,"Type Mismatch Exception",0,null);
    }

    //400错误
    @ExceptionHandler({MissingServletRequestParameterException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseJson requestMissingServletRequest(MissingServletRequestParameterException ex){
        System.out.println("400..MissingServletRequest");
        ex.printStackTrace();
        //return ErrorResponse.of("Missing Servlet Request", ErrorCode.BAD_REQUEST, HttpStatus.BAD_REQUEST);
        return  new ResponseJson(ResponseJson.FALSE,HttpStatus.BAD_REQUEST.value(),ResponseJson.FAIL,ex.getMessage(),0,null);
    }

    //405错误
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ResponseBody
    public ResponseJson request405(){
        System.out.println("405...");
        return  new ResponseJson(ResponseJson.FALSE,HttpStatus.METHOD_NOT_ALLOWED.value(),ResponseJson.FAIL,"Method Not Allowed",0,null);
    }


}
