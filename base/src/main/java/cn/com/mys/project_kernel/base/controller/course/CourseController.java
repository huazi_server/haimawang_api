package cn.com.mys.project_kernel.base.controller.course;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.Course;
import cn.com.mys.project_kernel.base.entity.CourseGroup;
import cn.com.mys.project_kernel.base.entity.CoursePackage;
import cn.com.mys.project_kernel.base.entity.TeacherCourse;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.ParamUtils;
import cn.com.mys.project_kernel.base.util.RequestUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.cglib.beans.BeanCopier;
import net.sf.cglib.core.Converter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 *
 * @author mingjun_T
 * 课程
 *
 */
@Controller
@RequestMapping(value = "/course")
public class CourseController {

	Log log = LogFactory.getLog(CourseController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentCourseService studentCourseService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private StudentCourseHomeworkService studentCourseHomeworkService;
	@Autowired
	private TeacherCourseService teacherCourseService;
	@Autowired
	private CourseLessonService courseLessonService;

	@Autowired
	private CoursePackageService coursePackageService;
	@Autowired
	private CoursePackageLessonService coursePackageLessonService;

	@Autowired
	private CourseGroupService courseGroupService;
	@Autowired
	private ChannelCourseService channelCourseService;
	@Autowired
	private DiscountService discountService;
	@Autowired
	private StudentDiscountService studentDiscountService;

	@Autowired
	private StudentCourseOrderService studentCourseOrderService;


	//查询可以报名购买的课程列表
	@RequestMapping(value = "sign")
	public ResponseEntity<ResponseJson> sign(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String channelId = (String) paramMap.get("channelId");//渠道id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/


		String courseType = "1";//课程类型(1单课程、2系列课)
		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		map4Param.put("state","3");//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
		map4Param.put("courseType", courseType);

		map4Param.put("name",name);

		List<CourseExt> list = courseService.findListByCondition4All(map4Param);


		//只保留正常数据
		CopyOnWriteArrayList<CourseExt> cowList = new CopyOnWriteArrayList<CourseExt>(list);
		for (CourseExt item : cowList) {
			if(item!=null && item.getCourseFlag()!=null && item.getCourseFlag()==2){//课程标识(1需购买课程、2非购买课程)
				cowList.remove(item);
			}
		}
		list = cowList;


		if(StringUtils.isNotEmpty(channelId)){
			cowList = new CopyOnWriteArrayList<CourseExt>(list);
			for (CourseExt item : cowList) {
				ChannelCourseExt channelCourseExt = null;
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("delFlag", "0");
				_map.put("channelId", channelId);
				_map.put("courseType", courseType);
				_map.put("courseId", item.getId().toString());
				_map.put("courseGroupId", "0");
				channelCourseExt = channelCourseService.findObjectByCondition(_map);
				item.setChannelCourseInfo(channelCourseExt);
				if(channelCourseExt==null){
					cowList.remove(item);
					continue;
				}
				if(channelCourseExt.getPrice()==null || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
					cowList.remove(item);
					continue;
				}
			}
			list = cowList;
		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询可以报名购买的课程包列表
	@RequestMapping(value = "sign4Package")
	public ResponseEntity<ResponseJson> sign4Package(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String channelId = (String) paramMap.get("channelId");//渠道id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/


		String courseType = "1";//课程类型(1单课程、2系列课)
		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		map4Param.put("state","3");//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
		map4Param.put("courseType", courseType);

		map4Param.put("name",name);

		List<CourseExt> list = courseService.findListByCondition4All(map4Param);


		//只保留正常数据
		CopyOnWriteArrayList<CourseExt> cowList = new CopyOnWriteArrayList<CourseExt>(list);
		for (CourseExt item : cowList) {
			if(item!=null && item.getCourseFlag()!=null && item.getCourseFlag()==2){//课程标识(1需购买课程、2非购买课程)
				cowList.remove(item);
			}
		}
		list = cowList;


		if(StringUtils.isNotEmpty(channelId)){
			cowList = new CopyOnWriteArrayList<CourseExt>(list);
			for (CourseExt item : cowList) {
				ChannelCourseExt channelCourseExt = null;
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("delFlag", "0");
				_map.put("channelId", channelId);
				_map.put("courseType", courseType);
				_map.put("courseId", item.getId().toString());
				_map.put("courseGroupId", "0");
				channelCourseExt = channelCourseService.findObjectByCondition(_map);
				item.setChannelCourseInfo(channelCourseExt);
				if(channelCourseExt==null){
					cowList.remove(item);
					continue;
				}
				if(channelCourseExt.getPrice()==null || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
					cowList.remove(item);
					continue;
				}
			}
			list = cowList;
		}


		//按课程包分组
		Map<String, List> m = new HashMap<String, List>();
		for(CourseExt c : list){
			if(m.get(c.getCoursePackageId().toString())==null){
				List<CourseExt> cl = new ArrayList();
				cl.add(c);
				m.put(c.getCoursePackageId().toString(), cl);
			}else{
				m.get(c.getCoursePackageId().toString()).add(c);

			}
		}

		List<CoursePackageExt> _l = new ArrayList<>();
		for (Map.Entry<String, List> entry : m.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

			Map<String, Object> mm = new HashMap<String, Object>();
			mm.put("coursePackageId", entry.getKey());
			mm.put("delFlag","0");
			CoursePackageExt coursePackageExt = coursePackageService.findObjectByCoursePackageId(mm);
			coursePackageExt.setCourseInfoList(entry.getValue());

			_l.add(coursePackageExt);
		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_l);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询可以报名购买的课程分组列表
	@RequestMapping(value = "sign4Group")
	public ResponseEntity<ResponseJson> sign4Group(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String channelId = (String) paramMap.get("channelId");//渠道id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		map4Param.put("state","2");//状态,1待上线、2已上线

		map4Param.put("name",name);

		List<CourseGroupExt> list = courseGroupService.findListByCondition4All(map4Param);


		//只保留正常数据
		CopyOnWriteArrayList<CourseGroupExt> cowList = new CopyOnWriteArrayList<CourseGroupExt>(list);
		for (CourseGroupExt item : cowList) {
			if(item!=null && item.getCourseGroupFlag()!=null && item.getCourseGroupFlag()==2){//课程分组标识(1正常课程分组、2活动课程分组)
				cowList.remove(item);
			}
		}
		list = cowList;


		String courseType = "2";//课程类型(1单课程、2系列课)
		if(StringUtils.isNotEmpty(channelId)){
			cowList = new CopyOnWriteArrayList<CourseGroupExt>(list);
			for (CourseGroupExt item : cowList) {
				ChannelCourseExt channelCourseExt = null;
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("delFlag", "0");
				_map.put("channelId", channelId);
				_map.put("courseType", courseType);
				_map.put("courseId", "0");
				_map.put("courseGroupId", item.getId().toString());
				channelCourseExt = channelCourseService.findObjectByCondition(_map);
				item.setChannelCourseInfo(channelCourseExt);
				if(channelCourseExt==null){
					cowList.remove(item);
					continue;
				}
				if(channelCourseExt.getPrice()==null || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
					cowList.remove(item);
					continue;
				}
			}
			list = cowList;
		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询课程详情(单课程)
	@RequestMapping(value = "detail")
	public ResponseEntity<ResponseJson> detail(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String channelId = (String) paramMap.get("channelId");//渠道id

		if(StringUtils.isEmpty(courseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Course course = courseService.findById(Long.parseLong(courseId));

		String courseType = "1";//课程类型(1单课程、2系列课)
		CourseExt courseExt = null;
		if(course!=null){
			courseExt = new CourseExt();
			org.springframework.beans.BeanUtils.copyProperties(course, courseExt);//属性拷贝

			if(StringUtils.isNotEmpty(channelId)){
				ChannelCourseExt channelCourseExt = null;
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("delFlag", "0");
				_map.put("channelId", channelId);
				_map.put("courseType", courseType);
				_map.put("courseId", course.getId().toString());
				_map.put("courseGroupId", "0");
				channelCourseExt = channelCourseService.findObjectByCondition(_map);
				if(channelCourseExt!=null){
					courseExt.setChannelCourseInfo(channelCourseExt);
				}else{
					courseExt = null;
				}
			}
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),courseExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询课程详情(系列课)
	@RequestMapping(value = "detail4Group")
	public ResponseEntity<ResponseJson> detail4Group(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseGroupId = (String) paramMap.get("courseGroupId");
		String channelId = (String) paramMap.get("channelId");//渠道id

		if(StringUtils.isEmpty(courseGroupId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"年课id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		CourseGroup courseGroup = courseGroupService.findById(Long.parseLong(courseGroupId));

		String courseType = "2";//课程类型(1单课程、2系列课)
		CourseGroupExt courseGroupExt = null;
		if(courseGroup!=null){
			courseGroupExt = new CourseGroupExt();
			org.springframework.beans.BeanUtils.copyProperties(courseGroup, courseGroupExt);//属性拷贝

			if(StringUtils.isNotEmpty(channelId)){
				ChannelCourseExt channelCourseExt = null;
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("delFlag", "0");
				_map.put("channelId", channelId);
				_map.put("courseType", courseType);
				_map.put("courseId", "0");
				_map.put("courseGroupId", courseGroup.getId().toString());
				channelCourseExt = channelCourseService.findObjectByCondition(_map);
				if(channelCourseExt!=null){
					courseGroupExt.setChannelCourseInfo(channelCourseExt);
				}else{
					courseGroupExt = null;
				}
			}
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),courseGroupExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//检查课程时间并更新状态
	@RequestMapping(value = "update")
	public ResponseEntity<ResponseJson> update(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数

		//更新所有的课程的状态
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("state", "2");//查询大于等于2的数据
		List<CourseExt> courseList = courseService.findAllListByCondition(m);
		Integer allCount = courseList.size();//总条目数
		Integer operateCount = 0;//操作条目数
		for(CourseExt c : courseList) {
			Integer state = c.getState();//原始状态
			Integer courseFlag = c.getCourseFlag();//课程标识(1需购买课程、2非购买课程)

			Date signStartAt = c.getSignStartAt();//报名开始时间
			Date signEndAt = c.getSignEndAt();//报名结束时间
			Date teachStartAt = c.getTeachStartAt();//课程开始时间
			Date teachEndAt = c.getTeachEndAt();//课程结束时间

			//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
			Date currentDate = new Date();//当前时间
			if (courseFlag != null && courseFlag == 1) {
				if (signStartAt.getTime() <= currentDate.getTime() && currentDate.getTime() <= signEndAt.getTime()) {
					c.setState(3);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
				if (signEndAt.getTime() <= currentDate.getTime() && currentDate.getTime() <= teachStartAt.getTime()) {
					c.setState(4);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
				if (teachStartAt.getTime() <= currentDate.getTime() && currentDate.getTime() <= teachEndAt.getTime()) {
					c.setState(5);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
				if (teachEndAt.getTime() <= currentDate.getTime()) {
					c.setState(6);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
			}

			if(courseFlag!=null && courseFlag==2){
				if(c.getState()!=null && c.getState().intValue()==2){
					c.setState(3);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
				if(teachStartAt.getTime()<=currentDate.getTime() && currentDate.getTime()<=teachEndAt.getTime()){
					c.setState(5);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
				if(teachEndAt.getTime()<=currentDate.getTime()){
					c.setState(6);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
			}


			//logger.info("-------");
		}




		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),null);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//给学生添加课程相关数据
	@RequestMapping(value = "/student/create")
	public ResponseEntity<ResponseJson> createStudentCourseData(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String phone = (String) paramMap.get("phone");
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)
		String cId = (String) paramMap.get("courseId");
		String cgId = (String) paramMap.get("courseGroupId");

		String channelId = (String) paramMap.get("channelId");

		if(StringUtils.isEmpty(phone) || StringUtils.isEmpty(courseType)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生手机号、课程类型不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType) && StringUtils.isEmpty(cId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"单课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType) && StringUtils.isEmpty(cgId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"系列课id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		String coursePackageIds = "";
		String courseIds = "";

		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
			cgId = "";
			Course course = courseService.findById(Long.parseLong(cId));
			coursePackageIds += course.getCoursePackageId();
			courseIds += course.getId();
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
			cId = "";
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("courseGroupId", cgId);
			CourseGroupExt courseGroupExt = courseGroupService.findObjectByCondition(m);
			coursePackageIds = courseGroupExt.getCoursePackageIds();
			courseIds = courseGroupExt.getCourseIds();
		}

		String[] cpIds = coursePackageIds.split(",");
		String[] cIds = courseIds.split(",");



		StudentExt studentExt = studentService.findByPhone(phone);
		if(studentExt==null || studentExt.getId()==0){
			//不存在则创建
			studentExt = new StudentExt();
			studentExt.setAccountId(null);
			studentExt.setType(0);//类型(0默认)
			studentExt.setPhone(phone);
			studentExt.setSourceType(1);//来源类型(1、WEB端)
			studentExt.setEnableFlag(1);//启用标记(1正常、2禁用)
			studentExt.setRegisterAt(new Date());
			studentExt.setRemark("");
			studentExt.setState(0);
			studentExt.setCreatedAt(new Date());
			studentExt.setUpdatedAt(new Date());
			studentExt.setDelFlag(0);
			studentService.insert4Ignore(studentExt);


			//插入操作可能由于并发而失败，所以重新查询数据
			studentExt = studentService.findByPhone(phone);
		}

		String studentId = studentExt.getId().toString();


		for(int i=0;i<cIds.length;i++){
			String courseId = cIds[i];
			String coursePackageId = cpIds[i];
			Course c = courseService.findById(Long.parseLong(courseId));
			coursePackageId = (c.getCoursePackageId()!=null&&c.getCoursePackageId()>0)?c.getCoursePackageId().toString():coursePackageId;

			StudentCourseExt studentCourseExt = new StudentCourseExt();
			studentCourseExt.setStudentId(Long.parseLong(studentId));
			studentCourseExt.setCourseId(Long.parseLong(courseId));
			studentCourseExt.setCoursePackageId(Long.parseLong(coursePackageId));
			studentCourseExt.setCourseGroupId(c.getCourseGroupId());
			studentCourseExt.setSourceType(4);//来源类型(正常购买1、免费领2、拼团购3、直接生成4)
			studentCourseExt.setSourceId(0L);//来源id(订单id或活动id)
			studentCourseExt.setCourseStartHintFlag(1);//课程开始提醒标识(1未提醒、2已提醒)
			studentCourseExt.setFullStudyFlag(1);//是否全部学习标识(1否、2是)
			studentCourseExt.setFullFinishFlag(1);//是否全部完成标识(1否、2是)

			studentCourseExt.setRemark("");
			studentCourseExt.setState(1);//状态(1未分配教师、2已分配教师)
			studentCourseExt.setCreatedAt(new Date());
			studentCourseExt.setUpdatedAt(new Date());
			studentCourseExt.setDelFlag(0);


			Map<String, Object> m = new HashMap<>();
			//m.put("courseId", courseId);
			m.put("coursePackageId", coursePackageId);
			m.put("studentId", studentId);
			m.put("delFlag", "0");
			StudentCourseExt sc = studentCourseService.findObjectByCondition(m);
			if (sc == null || sc.getId() == 0) {
				//生成学生课程信息
				studentCourseService.insert(studentCourseExt);
			}else{
				studentCourseExt = sc;
			}


			//do nothing
			log.info("generateTeacherCourseAndStudentCourseData");
			if(Constant.autoChooseTeacherFlag.intValue()==2) {//支付完自动分班标识(1否、2是)
				//生成教师课程、学生课程信息
				log.info("[generateTeacherCourseAndStudentCourseData]:"+"	"+"studentId="+studentCourseExt.getStudentId()+",coursePackageId="+studentCourseExt.getCoursePackageId()+",courseId="+studentCourseExt.getCourseId());
				studentCourseOrderService.generateTeacherCourseAndStudentCourseData(studentCourseExt.getStudentId().toString(), studentCourseExt.getCoursePackageId().toString(), studentCourseExt.getCourseId().toString());
			}
		}



		//根据渠道课程的优惠券的关系生成学生优惠券信息
		if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(courseType) && StringUtils.isNotEmpty(studentId)){
			studentDiscountService.insert(channelId, courseType, studentId, cId, cgId, "0");
		}



		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	/****************************************************************************************************/
	/****************************************************************************************************/
	/****************************************************************************************************/
	/****************************************************************************************************/



	//查询课程包详情
	@RequestMapping(value = "package/detail")
	public ResponseEntity<ResponseJson> detail4Package(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String stage = (String) paramMap.get("stage");//阶段为0

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag", "1");//启用标记(1正常、2禁用)
		map4Param.put("stage", "0");//阶段为0

		CoursePackageExt coursePackageExt = coursePackageService.findObjectByCondition(map4Param);
		if(coursePackageExt!=null && coursePackageExt.getId()>0){
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("delFlag","0");
			m.put("state", "1");//状态(0初始化、1正常)
			m.put("coursePackageId",coursePackageExt.getId().toString());
			List<CoursePackageLessonExt> coursePackageLessonInfoList = coursePackageLessonService.findListByCondition(m);//课程包的课时列表
			coursePackageExt.setCoursePackageLessonInfoList(coursePackageLessonInfoList);

			//查询学生课程信息
			if(StringUtils.isNotEmpty(studentId)){
				Map<String, Object> mm = new HashMap<String, Object>();
				mm.put("studentId",studentId);
				mm.put("delFlag","0");
				mm.put("coursePackageId",coursePackageExt.getId().toString());
				CourseExt courseExt = courseService.findObjectByCoursePackageIdAndStudentId(mm);
				coursePackageExt.setCourseInfo(courseExt);
			}
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),coursePackageExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	/****************************************************************************************************/
	/****************************************************************************************************/
	/****************************************************************************************************/
	/****************************************************************************************************/



	//查询可用的非购买的课程包列表
	@RequestMapping(value = "online/free")
	public ResponseEntity<ResponseJson> online_free(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String channelId = (String) paramMap.get("channelId");//渠道id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/


		String courseType = "1";//课程类型(1单课程、2系列课)
		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		map4Param.put("state","2,3");//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
		map4Param.put("courseType", courseType);

		map4Param.put("name",name);

		List<CourseExt> list = courseService.findListByCondition4Admin(map4Param);


		//只保留正常数据
		CopyOnWriteArrayList<CourseExt> cowList = new CopyOnWriteArrayList<CourseExt>(list);
		for (CourseExt item : cowList) {
			if(item!=null && item.getCourseFlag()!=null && item.getCourseFlag()!=2){//课程标识(1需购买课程、2非购买课程)
				cowList.remove(item);
			}
		}
		list = cowList;


		if(StringUtils.isNotEmpty(channelId)){
			cowList = new CopyOnWriteArrayList<CourseExt>(list);
			for (CourseExt item : cowList) {
				ChannelCourseExt channelCourseExt = null;
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("delFlag", "0");
				_map.put("channelId", channelId);
				_map.put("courseType", courseType);
				_map.put("courseId", item.getId().toString());
				_map.put("courseGroupId", "0");
				channelCourseExt = channelCourseService.findObjectByCondition(_map);
				item.setChannelCourseInfo(channelCourseExt);
				if(channelCourseExt==null){
					cowList.remove(item);
					continue;
				}
				if(channelCourseExt.getPrice()==null || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
					cowList.remove(item);
					continue;
				}
			}
			list = cowList;
		}


		//按课程包分组
		Map<String, List> m = new HashMap<String, List>();
		for(CourseExt c : list){
			if(m.get(c.getCoursePackageId().toString())==null){
				List<CourseExt> cl = new ArrayList();
				cl.add(c);
				m.put(c.getCoursePackageId().toString(), cl);
			}else{
				m.get(c.getCoursePackageId().toString()).add(c);

			}
		}

		List<CoursePackageExt> _l = new ArrayList<>();
		for (Map.Entry<String, List> entry : m.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

			Map<String, Object> mm = new HashMap<String, Object>();
			mm.put("coursePackageId", entry.getKey());
			mm.put("delFlag","0");
			CoursePackageExt coursePackageExt = coursePackageService.findObjectByCoursePackageId(mm);
			coursePackageExt.setCourseInfoList(entry.getValue());

			_l.add(coursePackageExt);
		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_l);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询可用的需购买的课程包列表
	@RequestMapping(value = "online/buy")
	public ResponseEntity<ResponseJson> online_buy(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String channelId = (String) paramMap.get("channelId");//渠道id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/


		String courseType = "1";//课程类型(1单课程、2系列课)
		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		map4Param.put("state","2,3");//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
		map4Param.put("courseType", courseType);

		map4Param.put("name",name);

		List<CourseExt> list = courseService.findListByCondition4Admin(map4Param);


		//只保留正常数据
		CopyOnWriteArrayList<CourseExt> cowList = new CopyOnWriteArrayList<CourseExt>(list);
		for (CourseExt item : cowList) {
			if(item!=null && item.getCourseFlag()!=null && item.getCourseFlag()==2){//课程标识(1需购买课程、2非购买课程)
				cowList.remove(item);
			}
		}
		list = cowList;


		if(StringUtils.isNotEmpty(channelId)){
			cowList = new CopyOnWriteArrayList<CourseExt>(list);
			for (CourseExt item : cowList) {
				ChannelCourseExt channelCourseExt = null;
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("delFlag", "0");
				_map.put("channelId", channelId);
				_map.put("courseType", courseType);
				_map.put("courseId", item.getId().toString());
				_map.put("courseGroupId", "0");
				channelCourseExt = channelCourseService.findObjectByCondition(_map);
				item.setChannelCourseInfo(channelCourseExt);
				if(channelCourseExt==null){
					cowList.remove(item);
					continue;
				}
				if(channelCourseExt.getPrice()==null || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
					cowList.remove(item);
					continue;
				}
			}
			list = cowList;
		}


		//按课程包分组
		Map<String, List> m = new HashMap<String, List>();
		for(CourseExt c : list){
			if(m.get(c.getCoursePackageId().toString())==null){
				List<CourseExt> cl = new ArrayList();
				cl.add(c);
				m.put(c.getCoursePackageId().toString(), cl);
			}else{
				m.get(c.getCoursePackageId().toString()).add(c);

			}
		}

		List<CoursePackageExt> _l = new ArrayList<>();
		for (Map.Entry<String, List> entry : m.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

			Map<String, Object> mm = new HashMap<String, Object>();
			mm.put("coursePackageId", entry.getKey());
			mm.put("delFlag","0");
			CoursePackageExt coursePackageExt = coursePackageService.findObjectByCoursePackageId(mm);
			coursePackageExt.setCourseInfoList(entry.getValue());

			_l.add(coursePackageExt);
		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_l);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询可用的需购买的课程分组列表
	@RequestMapping(value = "online/buy4Group")
	public ResponseEntity<ResponseJson> online_buy4Group(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String channelId = (String) paramMap.get("channelId");//渠道id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("enableFlag","1");//启用标记(1正常、2禁用)
		map4Param.put("state","2");//状态,1待上线、2已上线

		map4Param.put("name",name);

		List<CourseGroupExt> list = courseGroupService.findListByCondition4Admin(map4Param);


		//只保留正常数据
		CopyOnWriteArrayList<CourseGroupExt> cowList = new CopyOnWriteArrayList<CourseGroupExt>(list);
		for (CourseGroupExt item : cowList) {
			if(item!=null && item.getCourseGroupFlag()!=null && item.getCourseGroupFlag()==2){//课程分组标识(1正常课程分组、2活动课程分组)
				cowList.remove(item);
			}
		}
		list = cowList;


		String courseType = "2";//课程类型(1单课程、2系列课)
		if(StringUtils.isNotEmpty(channelId)){
			cowList = new CopyOnWriteArrayList<CourseGroupExt>(list);
			for (CourseGroupExt item : cowList) {
				ChannelCourseExt channelCourseExt = null;
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("delFlag", "0");
				_map.put("channelId", channelId);
				_map.put("courseType", courseType);
				_map.put("courseId", "0");
				_map.put("courseGroupId", item.getId().toString());
				channelCourseExt = channelCourseService.findObjectByCondition(_map);
				item.setChannelCourseInfo(channelCourseExt);
				if(channelCourseExt==null){
					cowList.remove(item);
					continue;
				}
				if(channelCourseExt.getPrice()==null || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==-1 || channelCourseExt.getPrice().compareTo(BigDecimal.ZERO)==0){//小于等于0
					cowList.remove(item);
					continue;
				}
			}
			list = cowList;
		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}





}
