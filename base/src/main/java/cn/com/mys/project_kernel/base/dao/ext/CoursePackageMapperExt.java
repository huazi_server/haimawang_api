package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CoursePackageExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface CoursePackageMapperExt {

    CoursePackageExt findObjectByCoursePackageId(Map<String, Object> m);

    List<CoursePackageExt> findListByCondition(Map<String, Object> map4Param);

    CoursePackageExt findObjectByCondition(Map<String, Object> mm);
}