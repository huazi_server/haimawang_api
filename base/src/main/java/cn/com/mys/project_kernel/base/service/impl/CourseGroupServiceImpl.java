package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CourseGroupMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.*;
import cn.com.mys.project_kernel.base.entity.CourseGroup;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.CourseGroupService;
import cn.com.mys.project_kernel.base.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class CourseGroupServiceImpl implements CourseGroupService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CourseGroupMapper courseGroupMapper;

	@Autowired
	public CourseGroupMapperExt courseGroupMapperExt;

	@Autowired
	public CourseMapperExt courseMapperExt;
	@Autowired
	public StudentCourseMapperExt studentCourseMapperExt;
	@Autowired
	public StudentCourseOrderMapperExt studentCourseOrderMapperExt;
	@Autowired
	public CourseLessonMapperExt courseLessonMapperExt;


	//设置课程包id,多个","分隔、课程id,多个","分隔
	private void setCoursePackageIdsAndCourseIds(CourseGroupExt courseGroupExt){
		Map<String, Object> map = new HashMap<>();
		map.put("delFlag", "0");
		map.put("courseType", "2");//课程类型(1单课程、2系列课)
		map.put("courseGroupId", courseGroupExt.getId().toString());
		List<CourseExt> courseInfoList = courseMapperExt.findListByCondition4CourseGroup(map);

		String coursePackageIds = "";
		String courseIds = "";
		Integer lessonCount = 0;//总课时数量

		for(CourseExt c : courseInfoList){
			coursePackageIds = coursePackageIds + c.getCoursePackageId().toString() + ",";
			courseIds = courseIds + c.getId().toString() + ",";

			if(c.getStage()>0){//统计
				Map<String, Object> m = new HashMap<>();
				m.put("delFlag","0");
				m.put("enableFlag","1");//启用标记(1正常、2禁用)
				m.put("courseId", c.getId().toString());
				List<CourseLessonExt> courseLessonInfoList = courseLessonMapperExt.findListByCondition(m);

				lessonCount = lessonCount+courseLessonInfoList.size();
			}
		}

		if(StringUtils.isNotEmpty(coursePackageIds)){
			coursePackageIds = coursePackageIds.substring(0,coursePackageIds.length()-1);
		}
		if(StringUtils.isNotEmpty(courseIds)){
			courseIds = courseIds.substring(0,courseIds.length()-1);
		}

		courseGroupExt.setCoursePackageIds(coursePackageIds);
		courseGroupExt.setCourseIds(courseIds);
		courseGroupExt.setCourseInfoList(courseInfoList);
		courseGroupExt.setLessonCount(lessonCount);
	}

	//设置购买标识(1未购买、2已购买)
	private void setBuyFlag(CourseGroupExt courseGroupExt, String studentId){
		Map<String, Object> m = new HashMap<>();
		m.put("studentId", studentId);
		m.put("delFlag","0");
		List<StudentCourseExt> studentCourseInfoList = studentCourseMapperExt.findListByCondition4CourseGroup(m);
		if(studentCourseInfoList!=null && studentCourseInfoList.size()>0){
			courseGroupExt.setBuyFlag(2);
		}else{
			courseGroupExt.setBuyFlag(1);
		}
	}


	@Override
	public CourseGroupExt findObjectByCondition(Map<String, Object> m) {
		String studentId = (String) m.get("studentId");
		CourseGroupExt courseGroupExt = courseGroupMapperExt.findObjectByCondition(m);
		if(courseGroupExt!=null && courseGroupExt.getId()>0){
			setCoursePackageIdsAndCourseIds(courseGroupExt);
			setBuyFlag(courseGroupExt, studentId);
		}
		return courseGroupExt;
	}

	@Override
	public List<CourseGroupExt> findListByCondition4All(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<CourseGroupExt> list = courseGroupMapperExt.findListByCondition4All(map4Param);
		for(CourseGroupExt courseGroupExt : list){
			setCoursePackageIdsAndCourseIds(courseGroupExt);
			setBuyFlag(courseGroupExt, studentId);
		}
		return list;
	}

	@Override
	public CourseGroup findById(long id) {
		return courseGroupMapper.selectByPrimaryKey(id);
	}


	@Override
	public List<CourseGroupExt> findListByCondition4Admin(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		List<CourseGroupExt> list = courseGroupMapperExt.findListByCondition4Admin(map4Param);
		for(CourseGroupExt courseGroupExt : list){
			setCoursePackageIdsAndCourseIds(courseGroupExt);
			setBuyFlag(courseGroupExt, studentId);
		}
		return list;
	}





}
