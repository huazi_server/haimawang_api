package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.CoursePackageExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface CoursePackageService {


    CoursePackageExt findObjectByCoursePackageId(Map<String, Object> m);

    List<CoursePackageExt> findListByCondition(Map<String, Object> map4Param);

    CoursePackageExt findObjectByCondition(Map<String, Object> mm);
}
