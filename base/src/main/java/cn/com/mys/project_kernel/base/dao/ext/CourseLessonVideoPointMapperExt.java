package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CourseLessonVideoPointExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface CourseLessonVideoPointMapperExt {

    List<CourseLessonVideoPointExt> findListByCondition(Map<String, Object> map4Point);
}