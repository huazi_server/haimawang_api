package cn.com.mys.project_kernel.base.util;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import net.sf.json.JSONObject;


public class HttpUtils {
	
	
	/**
	 * 向指定 URL 发送POST方法的请求
	 * 
	 * @param url
	 *            发送请求的 URL
	 * @param obj
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 */
	public static int sendPost_JSON(String url, JSONObject obj) {
        int  result = 0 ;
		BufferedReader in = null;
		OutputStream out = null;
		HttpURLConnection conn = null;
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			conn = (HttpURLConnection) realUrl.openConnection();
			// 设置允许输出
			conn.setDoOutput(true);
			conn.setDoInput(true);
			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "application/json");
			
			conn.connect();
			// 获取URLConnection对象对应的输出流
			out = conn.getOutputStream();
			// 发送请求参数
			out.write((obj.toString()).getBytes());
			// flush输出流的缓冲
			out.flush();
			result = conn.getResponseCode();
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	
	
	public static JSONObject sendPost4JSON(String url, JSONObject obj) {
        int  result = 0 ;
		BufferedReader in = null;
		OutputStream out = null;
		OutputStreamWriter osw = null;
		HttpURLConnection conn = null;
		String result4Json = "";
		JSONObject jsonObject = new JSONObject();
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			conn = (HttpURLConnection) realUrl.openConnection();
			// 设置允许输出
			conn.setDoOutput(true);
			conn.setDoInput(true);
			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Charset", "utf-8");
			
			conn.connect();
			// 获取URLConnection对象对应的输出流
			out = conn.getOutputStream();

			/*// 发送请求参数(方式一)
			out.write((obj.toString()).getBytes());
			// flush输出流的缓冲
			out.flush();*/

			// 发送请求参数(方式二)
			osw = new OutputStreamWriter(out, "utf-8");
			osw.write((obj.toString()));
			osw.flush();

			result = conn.getResponseCode();
			
			// 获取请求返回数据（设置返回数据编码为UTF-8）
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
            	result4Json += line;
            }
            jsonObject = JSONObject.fromObject(result4Json);
            //System.out.println(jsonObject);
			
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (osw != null) {
					osw.close();
				}
				if (in != null) {
					in.close();
				}

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return jsonObject;
	}








}
