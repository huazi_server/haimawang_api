package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ProductionCommentMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ProductionCommentLikeMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.ProductionCommentMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.ProductionCommentReplyLikeMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.ProductionCommentReplyMapperExt;
import cn.com.mys.project_kernel.base.entity.ProductionComment;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentExt;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentLikeExt;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentReplyExt;
import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentReplyLikeExt;
import cn.com.mys.project_kernel.base.service.ProductionCommentService;
import cn.com.mys.project_kernel.base.util.StringUtils;
import cn.com.mys.project_kernel.base.vo.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ProductionCommentServiceImpl implements ProductionCommentService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ProductionCommentMapper productionCommentMapper;

	@Autowired
	public ProductionCommentMapperExt productionCommentMapperExt;

	@Autowired
	public ProductionCommentLikeMapperExt productionCommentLikeMapperExt;
	@Autowired
	public ProductionCommentReplyMapperExt productionCommentReplyMapperExt;
	@Autowired
	public ProductionCommentReplyLikeMapperExt productionCommentReplyLikeMapperExt;




	//设置所属标识(1不属于自己、2属于自己)
	private void setSelfFlag(ProductionCommentExt productionCommentExt, String studentId){
		if(StringUtils.isNotEmpty(studentId) && Long.parseLong(studentId)>0 && productionCommentExt.getStudentId()!=null && productionCommentExt.getStudentId()==Long.parseLong(studentId)){
			productionCommentExt.setSelfFlag(2);
		}else{
			productionCommentExt.setSelfFlag(1);
		}
	}


	//设置点赞标识(1未点赞、2已点赞)
	private void setLikeFlag(ProductionCommentExt productionCommentExt, String studentId){
		Map<String, Object> map = new HashMap<>();
		map.put("studentId",studentId);
		map.put("commentId",productionCommentExt.getId().toString());
		map.put("delFlag","0");
		ProductionCommentLikeExt productionCommentLikeExt = productionCommentLikeMapperExt.findObjectByCondition(map);
		if(productionCommentLikeExt==null || productionCommentLikeExt.getId()==0){
			productionCommentExt.setLikeFlag(1);
		}else{
			productionCommentExt.setLikeFlag(2);
		}
	}

	//设置评论的回复列表
	private void setCommentReplyList(ProductionCommentExt productionCommentExt, String studentId, String replyCount){
		Map<String, Object> map = new HashMap<>();
		map.put("commentId",productionCommentExt.getId().toString());
		map.put("delFlag","0");

		Page page = new Page();
		page.setPageNumber(Integer.parseInt("0"));
		page.setPageSize(Integer.parseInt(StringUtils.isEmpty(replyCount)?"10":replyCount));
		map.put("page", page);

		Integer productionCommentReplyCount = 0;//评论的回复数量
		productionCommentReplyCount = productionCommentReplyMapperExt.findCountByCondition(map);
		List<ProductionCommentReplyExt> l = productionCommentReplyMapperExt.findListByCondition(map);

		for(ProductionCommentReplyExt pcr : l){
			Map<String, Object> m = new HashMap<>();
			m.put("studentId",studentId);
			m.put("replyId",pcr.getId().toString());
			m.put("delFlag","0");
			ProductionCommentReplyLikeExt productionCommentReplyLikeExt = productionCommentReplyLikeMapperExt.findObjectByCondition(m);
			if(productionCommentReplyLikeExt==null || productionCommentReplyLikeExt.getId()==0){
				pcr.setLikeFlag(1);
			}else{
				pcr.setLikeFlag(2);
			}


			if(StringUtils.isNotEmpty(studentId) && Long.parseLong(studentId)>0 && pcr.getStudentId()!=null && pcr.getStudentId()==Long.parseLong(studentId)){
				pcr.setSelfFlag(2);
			}else{
				pcr.setSelfFlag(1);
			}

		}

		productionCommentExt.setProductionCommentReplyCount(productionCommentReplyCount);
		productionCommentExt.setProductionCommentReplyInfoList(l);
	}



	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return productionCommentMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ProductionCommentExt> findListByCondition(Map<String, Object> map4Param) {
		String studentId = (String) map4Param.get("studentId");
		String replyCount = (String) map4Param.get("replyCount");
		List<ProductionCommentExt> list = productionCommentMapperExt.findListByCondition(map4Param);
		for(ProductionCommentExt pc : list){
			setSelfFlag(pc, studentId);
			setLikeFlag(pc, studentId);
			setCommentReplyList(pc, studentId, replyCount);
		}
		return list;
	}

	@Override
	public ProductionComment findById(long id) {
		return productionCommentMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer update(ProductionComment productionComment) {
		return productionCommentMapper.updateByPrimaryKeySelective(productionComment);
	}

	@Override
	public Integer insert(ProductionCommentExt productionCommentExt) {
		return productionCommentMapper.insertSelective(productionCommentExt);
	}
}
