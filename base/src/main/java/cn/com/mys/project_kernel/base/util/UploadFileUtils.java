package cn.com.mys.project_kernel.base.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;



public class UploadFileUtils {

	
	//本地上传
	public static String saveFileByLocal(MultipartFile file, HttpServletResponse response, String savePath, String filName) {
		String saveFilePath = "";

		if (file != null) {
			File f = new File(savePath);
			if (!f.exists()) {
				f.mkdirs();
			}

			//获取指定路径下的素材
			InputStream is;
			try {
				//文件读取与写入
				File uploadedFile = new File(savePath, filName);//fileName
				OutputStream os = new FileOutputStream(uploadedFile);
				is = file.getInputStream();

				byte buf[] = new byte[1024];
				int length = 0;
				while ((length = is.read(buf)) > 0) {
					os.write(buf, 0, length);
				}
				//关闭流
				os.flush();
				os.close();
				is.close();
				saveFilePath = savePath+"/"+filName;
			}catch (Exception e){

			}

		}
		//返回全路径
		return saveFilePath;
	}
	
	
	
	//网络上传,返回保存路径
	public static String saveFileByNet(String fileUrl, HttpServletResponse response, String savePath, String filName) {

		String saveFilePath = "";

		File f = new File(savePath);
		if (!f.exists()) {
			f.mkdirs();
		}
		
		//打开网络源
		URL url;
		try {
			url = new URL(fileUrl);
			URLConnection conn = url.openConnection();

			//文件读取与写入
			File uploadedFile = new File(savePath, filName);
			OutputStream os = new FileOutputStream(uploadedFile);
			
			//获取输入流对象
			InputStream is = conn.getInputStream();
			byte bytes[] = new byte[1024];//设置缓冲区为1024个字节，即1KB
			int len = 0;
			//读取数据。返回值为读入缓冲区的字节总数,如果到达文件末尾，则返回-1
			while((len=is.read(bytes))!=-1){
				
				//将指定 byte数组中从下标 0 开始的 len个字节写入此文件输出流,(即读了多少就写入多少)
				os.write(bytes,0,len);
			}
			os.flush();
			os.close();
			is.close();
			saveFilePath = savePath+"/"+filName;
		} catch (Exception e) {

		}
		//返回保存路径
		return saveFilePath;
	}
	
	
	
}
