package cn.com.mys.project_kernel.base.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AdminPage extends Page {

	private int pageTotal;			// 共有多少条目

	@Override
	public int getPageTotal() {
		return pageTotal;
	}

	@Override
	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}


	//转成AdminPage
	public static AdminPage getAdminPage(Page page){
		AdminPage adminPage = new AdminPage();
		//属性拷贝
		//BeanCopier beanCopier = BeanCopier.create(Page.class, AdminPage.class, false);//第三个参数表示是否使用转换器，false表示不使用，true表示使用
		//beanCopier.copy(page, adminPage, null);

		org.springframework.beans.BeanUtils.copyProperties(page.getClass(), adminPage.getClass());//属性拷贝
		adminPage.setPageNumber(page.getPageNumber());
		adminPage.setPageSize(page.getPageSize());
		adminPage.setTotalSize(page.getTotalSize());
		adminPage.setPageTotal(page.getTotalSize());//AdminPage重新设置此字段
		return adminPage;
	}



}
