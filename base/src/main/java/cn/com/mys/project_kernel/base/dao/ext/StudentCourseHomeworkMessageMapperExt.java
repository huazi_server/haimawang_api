package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.StudentCourseHomeworkMessageExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface StudentCourseHomeworkMessageMapperExt {

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<StudentCourseHomeworkMessageExt> findListByCondition(Map<String, Object> map4Param);
}