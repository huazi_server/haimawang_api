package cn.com.mys.project_kernel.base.filter.wrapper;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * 包装request(HttpServletRequestWrapper)
 * request请求处理
 * 乱码重新编码
 * 
 */

public class CharacterRequestWrapper extends HttpServletRequestWrapper{

	public CharacterRequestWrapper(HttpServletRequest request) {
		super(request);
	}

	
	//转码
	@SuppressWarnings("finally")
	private String convertCharacter(String value) {
		String resultValue = value;
		try {
			resultValue = new String(value.getBytes("ISO-8859-1"), "utf-8");//转换为utf-8
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return resultValue;
		}
	}

	//重写父类方法
	@Override
	public String getParameter(String name) {
		String value = super.getParameter(name);// 调用被包装对象的方法
		if (value != null){
			value = convertCharacter(value);//转换字符
		}
		return value;
	}

	//重写父类方法
	@Override
	public Map<String, String[]> getParameterMap() {
		Map<String, String[]> map = super.getParameterMap();
		if (map != null) {
			// 遍历转换字符
			for (String[] values : map.values()) {
				for (String value : values) {
					value = convertCharacter(value);
				}
			}
		}
		return map;
	}

	//重写父类方法
	@Override
	public String[] getParameterValues(String name) {
		
		String[] values = super.getParameterValues(name);
		if (values != null) {
			// 遍历转换字符
			for (String value : values) {
				value = convertCharacter(value);
			}
		}
		return values;
	}
	
	
	//重写父类方法
	@Override
	public String getQueryString() {
		//获取请求路径后的参数
		String query = super.getQueryString();
		
		query = convertCharacter(query);
		
		return query;
	}
	
	
	
	
	
	
	
	
	
	
	
}
