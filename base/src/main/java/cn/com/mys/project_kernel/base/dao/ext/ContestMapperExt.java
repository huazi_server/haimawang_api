package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ContestMapperExt {

    ContestExt findById(long id);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ContestExt> findListByCondition(Map<String, Object> map4Param);

    Integer findCountByCondition4Join(Map<String, Object> map4Param);

    List<ContestExt> findListByCondition4Join(Map<String, Object> map4Param);

    Integer findCountByCondition4Create(Map<String, Object> map4Param);

    List<ContestExt> findListByCondition4Create(Map<String, Object> map4Param);

    List<ContestExt> findAllListByCondition(Map<String, Object> map);
}