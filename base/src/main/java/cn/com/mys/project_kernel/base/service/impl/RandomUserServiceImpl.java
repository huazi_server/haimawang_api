package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.RandomUserMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.RandomUserMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.RandomUserExt;
import cn.com.mys.project_kernel.base.service.RandomUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class RandomUserServiceImpl implements RandomUserService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public RandomUserMapper randomUserMapper;

	@Autowired
	public RandomUserMapperExt randomUserMapperExt;


	@Override
	public List<RandomUserExt> findListByCondition(Map<String, Object> map4Param) {
		return randomUserMapperExt.findListByCondition(map4Param);
	}
}
