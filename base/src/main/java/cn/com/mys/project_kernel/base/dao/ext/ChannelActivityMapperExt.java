package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ChannelActivityExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ChannelActivityMapperExt {

    ChannelActivityExt findObjectByCondition(Map<String, Object> m);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ChannelActivityExt> findListByCondition(Map<String, Object> map4Param);

    Integer deleteByCondition(Map<String, Object> map4Param);
}