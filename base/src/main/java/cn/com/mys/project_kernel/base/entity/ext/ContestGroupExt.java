package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.dao.ext.ConfigGradeMapperExt;
import cn.com.mys.project_kernel.base.entity.ContestGroup;

import java.util.List;

public class ContestGroupExt extends ContestGroup {

    private List<ConfigGradeMapperExt> configGradeInfoList;

    public List<ConfigGradeMapperExt> getConfigGradeInfoList() {
        return configGradeInfoList;
    }

    public void setConfigGradeInfoList(List<ConfigGradeMapperExt> configGradeInfoList) {
        this.configGradeInfoList = configGradeInfoList;
    }
}