package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.TeacherCourseLessonExt;
import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public interface TeacherCourseLessonMapperExt {

    TeacherCourseLessonExt findObjectByCondition(Map<String, Object> m);

    Integer updateByTeacherCourseId(Map<String, Object> m);
}