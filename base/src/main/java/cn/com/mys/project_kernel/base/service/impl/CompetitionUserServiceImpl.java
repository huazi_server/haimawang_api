package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CompetitionUserMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CompetitionUserMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.CompetitionUserExt;
import cn.com.mys.project_kernel.base.service.CompetitionUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service的实现类
 *
 */

@Service
public class CompetitionUserServiceImpl implements CompetitionUserService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CompetitionUserMapper competitionUserMapper;

	@Autowired
	public CompetitionUserMapperExt competitionUserMapperExt;


	@Override
	public CompetitionUserExt findByAccountId(String accountId) {
		return competitionUserMapperExt.findByAccountId(accountId);
	}

	@Override
	public CompetitionUserExt findByOpenId(String openId) {
		return competitionUserMapperExt.findByOpenId(openId);
	}

	@Override
	public Integer insert(CompetitionUserExt competitionUserExt) {
		return competitionUserMapper.insertSelective(competitionUserExt);
	}

	@Override
	public Integer update(CompetitionUserExt competitionUserExt) {
		return competitionUserMapper.updateByPrimaryKeySelective(competitionUserExt);
	}
}
