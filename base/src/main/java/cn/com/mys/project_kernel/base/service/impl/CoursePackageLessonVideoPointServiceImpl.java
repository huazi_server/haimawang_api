package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CoursePackageLessonVideoPointMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CoursePackageLessonVideoPointMapperExt;
import cn.com.mys.project_kernel.base.service.CoursePackageLessonVideoPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service的实现类
 *
 */

@Service
public class CoursePackageLessonVideoPointServiceImpl implements CoursePackageLessonVideoPointService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CoursePackageLessonVideoPointMapper coursePackageLessonVideoPointMapper;

	@Autowired
	public CoursePackageLessonVideoPointMapperExt coursePackageLessonVideoPointMapperExt;





}
