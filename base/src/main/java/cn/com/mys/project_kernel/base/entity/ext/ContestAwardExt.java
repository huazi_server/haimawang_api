package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ContestAward;

import java.util.List;

public class ContestAwardExt extends ContestAward {

    private List<ContestPlayerExt> contestPlayerInfoList;

    public List<ContestPlayerExt> getContestPlayerInfoList() {
        return contestPlayerInfoList;
    }

    public void setContestPlayerInfoList(List<ContestPlayerExt> contestPlayerInfoList) {
        this.contestPlayerInfoList = contestPlayerInfoList;
    }
}