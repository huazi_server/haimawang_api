package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestReviewTeacherMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestReviewTeacherMapperExt;
import cn.com.mys.project_kernel.base.entity.ContestReviewTeacher;
import cn.com.mys.project_kernel.base.entity.ext.ContestReviewTeacherExt;
import cn.com.mys.project_kernel.base.service.ContestReviewTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ContestReviewTeacherServiceImpl implements ContestReviewTeacherService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestReviewTeacherMapper contestReviewTeacherMapper;

	@Autowired
	public ContestReviewTeacherMapperExt contestReviewTeacherMapperExt;

	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return contestReviewTeacherMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ContestReviewTeacherExt> findListByCondition(Map<String, Object> map4Param) {
		return contestReviewTeacherMapperExt.findListByCondition(map4Param);
	}

	@Override
	public ContestReviewTeacher findById(long id) {
		return contestReviewTeacherMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer insert(ContestReviewTeacher contestReviewTeacher) {
		return contestReviewTeacherMapper.insertSelective(contestReviewTeacher);
	}

	@Override
	public Integer update(ContestReviewTeacher contestReviewTeacher) {
		return contestReviewTeacherMapper.updateByPrimaryKeySelective(contestReviewTeacher);
	}

	@Override
	public ContestReviewTeacherExt findByPhone(String phone) {
		return contestReviewTeacherMapperExt.findByPhone(phone);
	}
}
