package cn.com.mys.project_kernel.base.util;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class ParamUtils {

	/**
	 * 从request中获得参数Map,并转为正常的Map
	 * @param request
	 * @return
     */
	public static Map requestParameter2Map(HttpServletRequest request) {
		//获取request中参数map
		Map parameter = request.getParameterMap();
		//定义返回map
		Map<String, Object> map = new HashMap<String, Object>();
		//获取迭代器
		Iterator entries = parameter.entrySet().iterator();
		Map.Entry entry;

		//根据迭代器遍历
		while (entries.hasNext()) {
			String name = "";
			String value = "";
			entry = (Map.Entry) entries.next();
			//获取key
			name = (String) entry.getKey();
			//获取value
			Object valueObj = entry.getValue();
			/*
			 * 处理value
			 */
			if(valueObj == null){
				value = "";
			}else if(valueObj instanceof String[]){
				String[] values = (String[])valueObj;
				for(int i=0;i<values.length;i++){
					value += values[i] + ",";
				}
				//如果有同名参数,就将数组转成字符串,多个","分隔
				value = value.substring(0, value.length()-1);
			}else{
				value = valueObj.toString();
			}
			//设置map
			map.put(name, value);
		}
		return map;
	}


	
}
