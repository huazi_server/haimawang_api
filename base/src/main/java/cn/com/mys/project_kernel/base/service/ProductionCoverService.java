package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ProductionCoverExt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ProductionCoverService {


    List<ProductionCoverExt> findListByCondition(Map<String, Object> map);
}
