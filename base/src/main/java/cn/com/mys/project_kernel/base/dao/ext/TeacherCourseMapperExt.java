package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.TeacherCourseExt;
import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public interface TeacherCourseMapperExt {

    TeacherCourseExt findObjectByCondition(Map<String, Object> m);
}