package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.AdminExt;

/**
 * Service的接口
 *
 */
public interface AdminService {

    AdminExt findByAccountId(String accountId);
}
