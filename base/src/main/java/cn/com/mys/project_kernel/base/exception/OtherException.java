package cn.com.mys.project_kernel.base.exception;

/**
 * Created by mingjun_T on 2016-07-09.
 */
public class OtherException extends Exception {

    public OtherException(){}

    public OtherException(String message){
        super(message);
    }

}
