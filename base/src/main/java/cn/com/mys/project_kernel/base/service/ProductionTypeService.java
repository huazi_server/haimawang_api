package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ProductionTypeExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ProductionTypeService {

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ProductionTypeExt> findListByCondition(Map<String, Object> map4Param);
}
