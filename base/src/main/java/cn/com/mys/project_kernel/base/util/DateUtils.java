package cn.com.mys.project_kernel.base.util;

/*
 * ALL RIGHTS RESERVED,
 * COPYRIGHT (C) 2014, MJ_T SOLUTIONS, Ltd.
 * 
 */

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * DateUtilsクラス
 */
public class DateUtils {
	
	
	
	/**
	* 該当日付の昨日を取得する
	* @param specifiedDay
	* @return
	* @throws Exception
	*/
	public static String getSpecifiedDayBefore(String specifiedDay){
		//SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		Calendar c = Calendar.getInstance();
		Date date=null;
		try {
			date = new SimpleDateFormat("yyyyMMdd").parse(specifiedDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.setTime(date);
		int day=c.get(Calendar.DATE);
		c.set(Calendar.DATE,day-1);
		
		String dayBefore=new SimpleDateFormat("yyyyMMdd").format(c.getTime());
		return dayBefore;
	}
	
	/**
	* 該当日付の明日を取得する
	* @param specifiedDay
	* @return
	*/
	public static String getSpecifiedDayAfter(String specifiedDay){
		Calendar c = Calendar.getInstance();
		Date date=null;
		try {
			date = new SimpleDateFormat("yyyyMMdd").parse(specifiedDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.setTime(date);
		int day=c.get(Calendar.DATE);
		c.set(Calendar.DATE,day+1);
		
		String dayAfter=new SimpleDateFormat("yyyyMMdd").format(c.getTime());
		return dayAfter;
	}
	
	
	
	/**
	 * 获取当前日期(时间)
	 * 返回Timestamp类型
	 * @return
	 */
	public static Timestamp getNowDateAndTimeByTimestamp(){
		
		Date d = new Date();
	    
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String dateNowStr = sdf.format(d);
	    Timestamp tp = Timestamp.valueOf(dateNowStr);
		return tp;
	}	
	
	public static Timestamp getNowDateAndTimeByTimestampFromDateTime(String dateTime){
		
		//Date d = new Date();
	    //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    //String dateNowStr = sdf.format(d);
		
	    Timestamp tp = Timestamp.valueOf(dateTime);
		return tp;
	}	
	
	
	/**
	 * 获取当前日期(时间)
	 * 返回String类型
	 * @return
	 */
	public static String getNowDateAndTimeFromTimestamp(Timestamp timestamp){
		
		//Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String tsStr = "";
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            //方法一
            tsStr = sdf.format(timestamp);
            System.out.println(tsStr);
            //方法二
            //tsStr = timestamp.toString();
            //System.out.println(tsStr);
        } catch (Exception e) {
        	tsStr = "";
        }
        
        return tsStr;
	}
	
	
	
	/**
	 * 获取当前日期(时间)
	 * @return
	 */
	public static String getNowDateTime(){
		
		Date d = new Date();
	    
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String dateNowStr = sdf.format(d);
	    
		return dateNowStr;
	}	
	
	
	/**
	 * 获取当前日期(时间)
	 * @return
	 */
	public static String getNowDate(){
		
		Date d = new Date();
	    
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    String dateNowStr = sdf.format(d);
	    
	    return dateNowStr;
	}
	
	
	/**
	 * 比较两个日期的前后关系
	 * 
	 * @param date1
	 *            比较日期1（格式为"yyyy-MM-dd"）
	 * @param date2
	 *            比较日期2（格式为"yyyy-MM-dd"）
	 * @param （格式为"yyyy-MM-dd"）
	 * @return true（date1<date2）、false（两者相等）、false（date1>date2）
	 */
	public static boolean compare(String date1, String date2, String dateFormat) {
		
		DateFormat df = new SimpleDateFormat(dateFormat);
		try {
			Date dt1 = df.parse(date1);
			Date dt2 = df.parse(date2);
			
			if (dt1.getTime() > dt2.getTime() || dt1.getTime() == dt2.getTime()) {
                //System.out.println("dt2<dt1或者dt2=dt1");
                return false;
            } else {
            	//date2>date1,符合条件
                return true;
            }
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
        
		return false;
	}
	
	//获取当前日期	yyyy/MM/dd
	public static String getDate(){
		
		Date d = new Date();
	    
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	    String dateNowStr = sdf.format(d);
	    return dateNowStr;
	}
	
	
	//获取指定天数的前后的日期(days可以为正数、负数)
	public static String addDays(int days) {

		
		Date date = new Date();
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");  //字符串转换
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, days); //days表示加减的天数
		c.getTime();
		String dateStr = formatDate.format(c.getTime());
		return dateStr;
	}
	

	/**
	 * 获取当前日期(时间)
	 * @return
	 */
	public static String getDate_Time(){
		
		Date d = new Date();
	    
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");
	    String dateNowStr = sdf.format(d);
	    
	    return dateNowStr;
	}

	//字符串转日期
	public static Date string2Date(String str) {
		if(StringUtils.isEmpty(str)){
			return null;
		}
		Date date = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = sdf.parse(str);
			SimpleDateFormat sdfAn = new SimpleDateFormat("yyyy-MM-dd");
			String dateTime = sdfAn.format(date);
		} catch (ParseException e) {

		}
		return date;
	}


	//字符串转日期
	public static Date string2Date4YMD(String str) {
		Date date = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			date = sdf.parse(str);
		} catch (ParseException e) {

		}
		return date;
	}

	//获取指定天数的前后的日期(days可以为正数、负数)
	public static Date addDays(Date date, int days) {
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");  //字符串转换
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, days); //days表示加减的天数
		date = c.getTime();
		return date;
	}


	//获取指定小时的前后的日期(days可以为正数、负数)
	public static Date addHours(Date date, int hours) {
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");  //字符串转换
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR_OF_DAY, hours); //hours表示加减的小时
		date = c.getTime();
		return date;
	}



	//日期转字符串
	public static String date2String(Date date) {
		String str = "";
		try {
			SimpleDateFormat sdfAn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			str = sdfAn.format(date);
		} catch (Exception e) {

		}
		return str;
	}


	//两个date相差的秒数
	public static long getInterval(Date date1, Date date2) {
		long interval = (date1.getTime() - date2.getTime())/1000;
		return interval;
	}



	//获取日期属于当年第几周
	public static int getYearWeek(Date date) {
		if(date==null){
			date = new Date();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.MONDAY);
		calendar.setTime(date);
		int yearWeekInt = calendar.get(Calendar.WEEK_OF_YEAR);
		//System.out.println(yearWeekInt);
		return yearWeekInt;
	}



	//获取年月日
	public static String getSysDate(Date date) {
		if(date==null){
			date = new Date();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = sdf.format(date);
		return dateStr;
	}

	//获取年份
	public static String getSysYear(Date date) {
		if(date==null){
			date = new Date();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		String year = sdf.format(date);
		return year;
	}

	//获取月日
	public static String getSysMonthAndDay(Date date) {
		if(date==null){
			date = new Date();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
		String md = sdf.format(date);
		return md;
	}

	//获取星期
	public static String getSysWeek(Date date) {
		String[] weekDays = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;
		return weekDays[w];
	}


	//获取星期
	public static int getSysWeekInt(Date date) {
		int[] weekDays = {7, 1, 2, 3, 4, 5, 6};
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;
		return weekDays[w];
	}



	/**
	 * 获取每周的第一天和最后一天
	 * @param date					日期
	 * @param dateFormat			日期字符串的格式
	 * @param resultDateFormat		日期字符串的格式
	 * @return
	 * @throws ParseException
	 */
	public static String getFirstAndLastOfWeek(Date date,String dateFormat,String resultDateFormat) {
		if(date==null){
			date = new Date();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = sdf.format(date);

		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(new SimpleDateFormat(dateFormat).parse(dateStr));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int d = 0;
		if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
			d = -6;
		} else {
			d = 2 - cal.get(Calendar.DAY_OF_WEEK);
		}
		cal.add(Calendar.DAY_OF_WEEK, d);
		// 所在周开始日期
		String data1 = new SimpleDateFormat(resultDateFormat).format(cal.getTime());
		cal.add(Calendar.DAY_OF_WEEK, 6);
		// 所在周结束日期
		String data2 = new SimpleDateFormat(resultDateFormat).format(cal.getTime());
		return data1 + "_" + data2;

	}


	/**
	 * 获取每周的第一天和最后一天
	 * @param dateStr				日期字符串
	 * @param dateFormat			日期字符串的格式
	 * @param resultDateFormat		日期字符串的格式
	 * @return
	 * @throws ParseException
	 */
	public static String getFirstAndLastOfWeek(String dateStr,String dateFormat,String resultDateFormat) {
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(new SimpleDateFormat(dateFormat).parse(dateStr));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int d = 0;
		if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
			d = -6;
		} else {
			d = 2 - cal.get(Calendar.DAY_OF_WEEK);
		}
		cal.add(Calendar.DAY_OF_WEEK, d);
		// 所在周开始日期
		String data1 = new SimpleDateFormat(resultDateFormat).format(cal.getTime());
		cal.add(Calendar.DAY_OF_WEEK, 6);
		// 所在周结束日期
		String data2 = new SimpleDateFormat(resultDateFormat).format(cal.getTime());
		return data1 + "_" + data2;

	}


	//获取指定日期字符串范围内的日期列表
	public static List<Date> getDates(Date dBegin, Date dEnd) {
		List lDate = new ArrayList();
		lDate.add(dBegin);
		Calendar calBegin = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		calBegin.setTime(dBegin);
		Calendar calEnd = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		calEnd.setTime(dEnd);
		// 测试此日期是否在指定日期之后
		while (dEnd.after(calBegin.getTime())) {
			// 根据日历的规则，为给定的日历字段添加或减去指定的时间量
			calBegin.add(Calendar.DAY_OF_MONTH, 1);
			lDate.add(calBegin.getTime());
		}
		return lDate;
	}







}