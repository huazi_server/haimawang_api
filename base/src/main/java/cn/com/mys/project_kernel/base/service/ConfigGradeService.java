package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ConfigGrade;
import cn.com.mys.project_kernel.base.entity.ext.ConfigGradeExt;

import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ConfigGradeService {

    ConfigGradeExt findObjectByCondition(Map<String, Object> map4Param);

    ConfigGrade findById(long id);
}
