package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.Contest;

import java.util.List;

public class ContestExt extends Contest {

    private ContestTemplateExt contestTemplateInfo;
    private ContestSignupConfigExt contestSignupConfigInfo;
    private ContestReviewConfigExt contestReviewConfigInfo;
    private List<ContestReviewTeacherExt> contestReviewTeacherInfoList;
    private List<ContestAwardExt> contestAwardInfoList;
    private ContestStatExt contestStatInfo;

    public ContestTemplateExt getContestTemplateInfo() {
        return contestTemplateInfo;
    }

    public void setContestTemplateInfo(ContestTemplateExt contestTemplateInfo) {
        this.contestTemplateInfo = contestTemplateInfo;
    }

    public ContestSignupConfigExt getContestSignupConfigInfo() {
        return contestSignupConfigInfo;
    }

    public void setContestSignupConfigInfo(ContestSignupConfigExt contestSignupConfigInfo) {
        this.contestSignupConfigInfo = contestSignupConfigInfo;
    }

    public ContestReviewConfigExt getContestReviewConfigInfo() {
        return contestReviewConfigInfo;
    }

    public void setContestReviewConfigInfo(ContestReviewConfigExt contestReviewConfigInfo) {
        this.contestReviewConfigInfo = contestReviewConfigInfo;
    }

    public List<ContestReviewTeacherExt> getContestReviewTeacherInfoList() {
        return contestReviewTeacherInfoList;
    }

    public void setContestReviewTeacherInfoList(List<ContestReviewTeacherExt> contestReviewTeacherInfoList) {
        this.contestReviewTeacherInfoList = contestReviewTeacherInfoList;
    }

    public List<ContestAwardExt> getContestAwardInfoList() {
        return contestAwardInfoList;
    }

    public void setContestAwardInfoList(List<ContestAwardExt> contestAwardInfoList) {
        this.contestAwardInfoList = contestAwardInfoList;
    }

    public ContestStatExt getContestStatInfo() {
        return contestStatInfo;
    }

    public void setContestStatInfo(ContestStatExt contestStatInfo) {
        this.contestStatInfo = contestStatInfo;
    }

    private Integer signupFlag;//指定比赛报名标识(1不可报、2可报)
    private Integer joinFlag;//指定比赛参加标识(1未参加、2已参加)
    public Integer getSignupFlag() {
        return signupFlag;
    }
    public void setSignupFlag(Integer signupFlag) {
        this.signupFlag = signupFlag;
    }
    public Integer getJoinFlag() {
        return joinFlag;
    }
    public void setJoinFlag(Integer joinFlag) {
        this.joinFlag = joinFlag;
    }


    private Integer signupCount;//报名人数
    private Integer commitCount;//提交人数
    public Integer getSignupCount() {
        return signupCount;
    }
    public void setSignupCount(Integer signupCount) {
        this.signupCount = signupCount;
    }
    public Integer getCommitCount() {
        return commitCount;
    }
    public void setCommitCount(Integer commitCount) {
        this.commitCount = commitCount;
    }
}