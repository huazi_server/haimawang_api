package cn.com.mys.project_kernel.base.controller.account.student;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.Student;
import cn.com.mys.project_kernel.base.entity.StudentCourseHomework;
import cn.com.mys.project_kernel.base.entity.Teacher;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseHomeworkMessageExt;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 *
 * @author mingjun_T
 * 学生课程作业
 *
 */
@Controller
@RequestMapping(value = "/account/student/course/homework")
public class StudentCourseHomeworkController {

	Log log = LogFactory.getLog(StudentCourseHomeworkController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentCourseHomeworkService studentCourseHomeworkService;
	@Autowired
	private StudentCourseHomeworkMessageService studentCourseHomeworkMessageService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private CourseLessonService courseLessonService;
	@Autowired
	private TeacherCourseLessonService teacherCourseLessonService;
	@Autowired
	private StudentCourseOrderService studentCourseOrderService;
	@Autowired
	private StudentCourseService studentCourseService;
	@Autowired
	private TeacherService teacherService;


	//心跳
	@RequestMapping(value = "heartbeat")
	public ResponseEntity<ResponseJson> heartbeat(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");
		String answerSecond = (String) paramMap.get("answerSecond");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(courseLessonId) || StringUtils.isEmpty(teacherCourseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、课时id、教师课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		map4Param.put("courseLessonId",courseLessonId);
		map4Param.put("teacherCourseId",teacherCourseId);
		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findObjectByCondition(map4Param);
		if(studentCourseHomeworkExt!=null && studentCourseHomeworkExt.getId()>0){
			studentCourseHomeworkExt.setAnswerSecond(StringUtils.isEmpty(answerSecond)?0:Integer.parseInt(answerSecond));

			Date startAt = studentCourseHomeworkExt.getStartAt();//开始时间
			Date endAt = studentCourseHomeworkExt.getEndAt();//结束时间
			if(startAt!=null && endAt!=null && startAt.getTime()<=new Date().getTime() && new Date().getTime()<=endAt.getTime()){
				Integer studyFlag = 2;//学习标识(1未学习、2已学习)
				studentCourseHomeworkExt.setStudyFlag(studyFlag);

				Integer onlineFlag = 2;//在线标识(1离线、2在线)
				studentCourseHomeworkExt.setOnlineFlag(onlineFlag);
				studentCourseHomeworkExt.setOnlineAt(new Date());//上线时间
			}

			studentCourseHomeworkService.update(studentCourseHomeworkExt);


			//检查并更新当前课程下的作业是否全部学习过
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("courseId", studentCourseHomeworkExt.getCourseId().toString());
			m.put("studentId", studentCourseHomeworkExt.getStudentId().toString());
			List<StudentCourseHomeworkExt> homeworkList = studentCourseHomeworkService.findListByCondition(m);
			int fullStudyFlag = 1;//是否全部学习标识(1否、2是)
			int studyCount = 0;
			for(StudentCourseHomeworkExt h : homeworkList){
				int _studyFlag = h.getStudyFlag();//学习标识(1未学习、2已学习)
				if(_studyFlag==2) studyCount++;
			}
			if(homeworkList.size()>0 && homeworkList.size()==studyCount){
				fullStudyFlag = 2;
			}

			m.put("teacherCourseId", studentCourseHomeworkExt.getTeacherCourseId().toString());
			StudentCourseExt sc = studentCourseService.findObjectByCondition(m);
			if(sc!=null && sc.getId()>0){
				sc.setFullStudyFlag(fullStudyFlag);
				studentCourseService.update(sc);
			}

		}


		//更新课时的作业上线人数
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("delFlag","0");
		m.put("courseId",courseId);
		m.put("courseLessonId",courseLessonId);
		m.put("teacherCourseId",teacherCourseId);
		m.put("onlineFlag","2");//在线标识(1离线、2在线)
		TeacherCourseLessonExt teacherCourseLessonExt = teacherCourseLessonService.findObjectByCondition(m);
		if(teacherCourseLessonExt!=null && teacherCourseLessonExt.getId()>0){
			Integer onlineCount = studentCourseHomeworkService.findOnlineCountByCondition(m);
			teacherCourseLessonExt.setOnlineCount(onlineCount);
			teacherCourseLessonService.update(teacherCourseLessonExt);
		}


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseHomeworkExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//查询作业详情
	@RequestMapping(value = "detail")
	public ResponseEntity<ResponseJson> detail(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(courseLessonId) || StringUtils.isEmpty(teacherCourseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、课时id、教师课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		map4Param.put("courseLessonId",courseLessonId);
		map4Param.put("teacherCourseId",teacherCourseId);
		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findObjectByCondition(map4Param);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseHomeworkExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//保存作业
	@RequestMapping(value = "save")
	public ResponseEntity<ResponseJson> save(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");
		String draft = (String) paramMap.get("draft");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(courseLessonId) || StringUtils.isEmpty(teacherCourseId) || StringUtils.isEmpty(draft)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、课时id、教师课程id、内容不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		map4Param.put("courseLessonId",courseLessonId);
		CourseLessonExt cl = courseLessonService.findObjectByCondition(map4Param);
		if(cl!=null && cl.getId()>0 && cl.getLockFlag()==1){//锁标识(1关闭、2打开)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课时还未开始",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		map4Param.put("courseLessonId",courseLessonId);
		map4Param.put("teacherCourseId",teacherCourseId);
		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findObjectByCondition(map4Param);
		studentCourseHomeworkExt.setDraft(draft);

		Integer state = studentCourseHomeworkExt.getState();
		if(state!=null && state==1){
			studentCourseHomeworkExt.setState(2);//状态(1未保存、2未提交、3已提交)
		}

		studentCourseHomeworkExt.setUpdatedAt(new Date());
		studentCourseHomeworkService.update(studentCourseHomeworkExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseHomeworkExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//更新作业
	@RequestMapping(value = "update")
	public ResponseEntity<ResponseJson> update(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");
		String draft = (String) paramMap.get("draft");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(courseLessonId) || StringUtils.isEmpty(teacherCourseId) || StringUtils.isEmpty(draft)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、课时id、教师课程id、内容不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		map4Param.put("courseLessonId",courseLessonId);
		CourseLessonExt cl = courseLessonService.findObjectByCondition(map4Param);
		if(cl!=null && cl.getId()>0 && cl.getLockFlag()==1){//锁标识(1关闭、2打开)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课时还未开始",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		map4Param.put("courseLessonId",courseLessonId);
		map4Param.put("teacherCourseId",teacherCourseId);
		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findObjectByCondition(map4Param);
		studentCourseHomeworkExt.setDraft(draft);

		studentCourseHomeworkExt.setUpdatedAt(new Date());
		studentCourseHomeworkService.update(studentCourseHomeworkExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseHomeworkExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//提交作业
	@RequestMapping(value = "commit")
	public ResponseEntity<ResponseJson> commit(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");
		String content = (String) paramMap.get("content");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(courseLessonId) || StringUtils.isEmpty(teacherCourseId) || StringUtils.isEmpty(content)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、课时id、内容不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		map4Param.put("courseLessonId",courseLessonId);
		CourseLessonExt cl = courseLessonService.findObjectByCondition(map4Param);
		if(cl!=null && cl.getId()>0 && cl.getLockFlag()==1){//锁标识(1关闭、2打开)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课时还未开始",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		map4Param = new HashMap<String, Object>();
		/*map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		CourseExt courseExt = courseService.findObjectByCourseId(map4Param);
		Date endAt = courseExt.getTeachEndAt();
		if(endAt!=null){
			//判断是否超过截止时间
			if(endAt.getTime() <= new Date().getTime()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此课程已结束，无需再提交作业。",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}*/

		map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		map4Param.put("courseLessonId",courseLessonId);
		map4Param.put("teacherCourseId",teacherCourseId);
		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findObjectByCondition(map4Param);
		/*Date endAt = studentCourseHomeworkExt.getEndAt();
		if(endAt!=null){
			//判断是否超过截止时间
			if(endAt.getTime() <= new Date().getTime()){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"此课时已结束，无需再提交作业。",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}*/




		studentCourseHomeworkExt.setContent(content);
		studentCourseHomeworkExt.setDraft("");//提交时清空临时保存的草稿

		studentCourseHomeworkExt.setCommitAt(new Date());
		studentCourseHomeworkExt.setCommitCount((studentCourseHomeworkExt.getCommitCount()==null)?0+1:studentCourseHomeworkExt.getCommitCount()+1);
		studentCourseHomeworkExt.setState(3);//状态(1未保存、2未提交、3已提交)
		studentCourseHomeworkExt.setUpdatedAt(new Date());
		studentCourseHomeworkService.update(studentCourseHomeworkExt);

		//更新课时的作业提交人数
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("delFlag","0");
		m.put("courseId",courseId);
		m.put("courseLessonId",courseLessonId);
		m.put("teacherCourseId",teacherCourseId);
		m.put("state","3");//状态(1未保存、2未提交、3已提交)
		TeacherCourseLessonExt teacherCourseLessonExt = teacherCourseLessonService.findObjectByCondition(m);
		Integer commitCount = studentCourseHomeworkService.findCommitCountByCondition(m);
		teacherCourseLessonExt.setCommitCount(commitCount);
		teacherCourseLessonService.update(teacherCourseLessonExt);



		//公众号消息发送
		if(Constant.msgSendFlag4Official==2){//是否进行公众号消息发送(1不发送、2发送)
			JSONObject obj = new JSONObject();
			obj.put("loginProductId", Constant.loginProductId);
			obj.put("loginProductKey", Constant.loginProductKey);
			obj.put("appId", Constant.appId4Official);
			obj.put("appKey", Constant.appKey4Official);
			obj.put("messageId", Constant.messageId2HomeworkCommit4Official);
			String wordData = Constant.wordData2HomeworkCommit4Official;
			wordData = wordData.replace("KEYWORD1",studentCourseHomeworkExt.getStudentName());//学生名称
			wordData = wordData.replace("KEYWORD2",studentCourseHomeworkExt.getCourseLessonTitle());//作业名称
			wordData = wordData.replace("KEYWORD3",DateUtils.date2String(studentCourseHomeworkExt.getCommitAt()));//完成时间
			obj.put("wordData", wordData);
			//Teacher t = teacherService.findById(studentCourseHomeworkExt.getTeacherId());
			//obj.put("accountId", t.getAccountId());
			obj.put("accountId", studentExt.getAccountId());
			obj.put("webUrl", "");
			obj.put("xcxAppId", "");
			obj.put("xcxPagepath", "");

			try{
				log.info("[HomeworkCommit send msg]--"+obj.toString());
				JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("msgSend4OfficialUrl"), obj);
				log.info("[HomeworkCommit send msg]--"+jo.toString());
			}catch (Exception e){
				log.info("[HomeworkCommit send msg fail]");
			}

		}



		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseHomeworkExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询作业列表(个人)
	@RequestMapping(value = "list")
	public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String courseId = (String) paramMap.get("courseId");//课程id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();

		map4Param = new HashMap<String, Object>();
		map4Param.put("studentId",studentId);
		map4Param.put("delFlag","0");
		map4Param.put("courseId",courseId);
		CourseExt courseExt = courseService.findObjectByCourseId(map4Param);
		if(courseExt==null || courseExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学生课程不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("studentId", studentId);
		map4Param.put("page", page);

		//当前时间(查询当前时间在课程的开始和结束时间之内的数据)
		//map4Param.put("currentDate", new Date());

		map4Param.put("courseId", courseId);

		//查询作业列表
		//Integer count = studentCourseHomeworkService.findCountByCondition(map4Param);
		//List<StudentCourseHomeworkExt> list = studentCourseHomeworkService.findListByCondition(map4Param);

		//查询课时列表(包含作业信息)
		Integer count = courseLessonService.findCountByCondition4CurrentDate(map4Param);
		List<CourseLessonExt> cl_list = courseLessonService.findListByCondition4CurrentDate(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),cl_list,courseExt,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	//提交留言
	@RequestMapping(value = "message/commit")
	public ResponseEntity<ResponseJson> commit4Message(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String courseId = (String) paramMap.get("courseId");
		String courseLessonId = (String) paramMap.get("courseLessonId");
		String teacherCourseId = (String) paramMap.get("teacherCourseId");
		String content = (String) paramMap.get("content");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(courseId) || StringUtils.isEmpty(courseLessonId) || StringUtils.isEmpty(teacherCourseId) || StringUtils.isEmpty(content)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课程id、课时id、教师课程id、内容不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag","0");
		map4Param.put("courseLessonId",courseLessonId);
		CourseLessonExt cl = courseLessonService.findObjectByCondition(map4Param);
		if(cl!=null && cl.getId()>0 && cl.getLockFlag()==1){//锁标识(1关闭、2打开)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"课时还未开始",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		Map<String, Object> m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("studentId", studentId);
		m.put("courseId", courseId);
		m.put("teacherCourseId", teacherCourseId);
		StudentCourseExt studentCourseExt = studentCourseService.findObjectByCondition(m);
		Long studentCourseId = 0L;
		if(studentCourseExt!=null && studentCourseExt.getId()>0){
			studentCourseId = studentCourseExt.getId();
		}

		StudentCourseHomeworkMessageExt studentCourseHomeworkMessageExt = new StudentCourseHomeworkMessageExt();
		studentCourseHomeworkMessageExt.setCourseId(Long.parseLong(courseId));
		studentCourseHomeworkMessageExt.setCourseLessonId(Long.parseLong(courseLessonId));
		studentCourseHomeworkMessageExt.setStudentId(Long.parseLong(studentId));
		studentCourseHomeworkMessageExt.setTeacherCourseId(Long.parseLong(teacherCourseId));
		studentCourseHomeworkMessageExt.setStudentCourseId(studentCourseId);
		studentCourseHomeworkMessageExt.setContent(content);

		studentCourseHomeworkMessageExt.setState(1);//状态(1未解决、2已解决)
		studentCourseHomeworkMessageExt.setCreatedAt(new Date());
		studentCourseHomeworkMessageExt.setUpdatedAt(new Date());
		studentCourseHomeworkMessageExt.setDelFlag(0);
		studentCourseHomeworkMessageService.insert(studentCourseHomeworkMessageExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseHomeworkMessageExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}




	//查询作业详情(通过id)
	@RequestMapping(value = "get")
	public ResponseEntity<ResponseJson> get(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String homeworkId = (String) paramMap.get("homeworkId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/


		if(StringUtils.isEmpty(homeworkId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作业id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		StudentCourseHomeworkExt studentCourseHomeworkExt = studentCourseHomeworkService.findById(Long.parseLong(homeworkId));


		if(StringUtils.isEmpty(accountId) || StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"非法访问",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(studentCourseHomeworkExt!=null && studentCourseHomeworkExt.getStudentId()!=null && StringUtils.isNotEmpty(studentId) && studentCourseHomeworkExt.getStudentId()!=Long.parseLong(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"非法访问",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}



		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),studentCourseHomeworkExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}








}
