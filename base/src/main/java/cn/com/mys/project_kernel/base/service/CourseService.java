package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.Course;
import cn.com.mys.project_kernel.base.entity.ext.CourseExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface CourseService {


    List<CourseExt> findListByCondition4All(Map<String, Object> map4Param);

    List<CourseExt> findListByCondition(Map<String, Object> map4Param);

    Course findById(Long courseId);

    Integer findCountByCondition4Teacher(Map<String, Object> map4Param);
    List<CourseExt> findListByCondition4Teacher(Map<String, Object> map4Param);

    CourseExt findObjectByCondition(Map<String, Object> map4Param);

    Integer update(CourseExt courseExt);

    List<CourseExt> findAllListByCondition(Map<String, Object> map);

    CourseExt findObjectByCourseId(Map<String, Object> map4Param);


    CourseExt findObjectByCondition4CoursePackageId(Map<String, Object> m);
    CourseExt findObjectByCoursePackageId(Map<String, Object> m);

    CourseExt findObjectById(Map<String, Object> mm);

    Integer findCountByCondition4TeacherCP(Map<String, Object> map4Param);
    List<CourseExt> findListByCondition4TeacherCP(Map<String, Object> map4Param);

    CourseExt findObjectByCoursePackageIdAndStudentId(Map<String, Object> mm);

    List<CourseExt> findListByCondition4Admin(Map<String, Object> map4Param);


    Integer updateCourseEnrollCount(String courseId);

}
