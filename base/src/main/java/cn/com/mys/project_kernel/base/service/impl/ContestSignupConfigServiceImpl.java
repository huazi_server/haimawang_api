package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestSignupConfigMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestSignupConfigMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestSignupConfigExt;
import cn.com.mys.project_kernel.base.service.ContestSignupConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ContestSignupConfigServiceImpl implements ContestSignupConfigService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestSignupConfigMapper contestSignupConfigMapper;

	@Autowired
	public ContestSignupConfigMapperExt contestSignupConfigMapperExt;

	@Override
	public ContestSignupConfigExt findObjectByCondition(Map<String, Object> map4Param) {
		return contestSignupConfigMapperExt.findObjectByCondition(map4Param);
	}

	@Override
	public Integer insert(ContestSignupConfigExt contestSignupConfigExt) {
		return contestSignupConfigMapper.insertSelective(contestSignupConfigExt);
	}

	@Override
	public Integer update(ContestSignupConfigExt contestSignupConfigExt) {
		return contestSignupConfigMapper.updateByPrimaryKeySelective(contestSignupConfigExt);
	}
}
