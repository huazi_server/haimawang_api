package cn.com.mys.project_kernel.base.job;


import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.JWindow;

import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.entity.*;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.DateUtils;
import cn.com.mys.project_kernel.base.util.HttpUtils;
import cn.com.mys.project_kernel.base.util.PropertiesUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import cn.com.mys.project_kernel.base.vo.Page;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.poi.util.IntegerField;
import org.apache.poi.util.SystemOutLogger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;




/**
 * 
 * 定时任务job
 * 
 */
@Service
public class QuartzJob {

	private static Logger logger = Logger.getLogger(QuartzJob.class);
	
	//这个属性如不是static,那么每次都要实例这个任务类,始终打印为: 1  
    //private static int _counter = 1;
	
	@Autowired
	public TbTestService tbTestService;

	@Autowired
	private StudentCourseHomeworkService studentCourseHomeworkService;
	@Autowired
	private TeacherCourseLessonService teacherCourseLessonService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private CourseLessonService courseLessonService;
	@Autowired
	private StudentCourseOrderService studentCourseOrderService;
	@Autowired
	private StudentCourseService studentCourseService;
	@Autowired
	private StudentCourseOrderFlowService studentCourseOrderFlowService;

	@Autowired
	private StudentService studentService;

	@Autowired
	private WeixinNotifyService weixinNotifyService;
	@Autowired
	private CourseGroupService courseGroupService;


	@Autowired
	private ChannelService channelService;
	@Autowired
	private ChannelStatisticsService channelStatisticsService;

	@Autowired
	private ActivityTuanService activityTuanService;
	@Autowired
	private ActivityTuanOrderService activityTuanOrderService;
	@Autowired
	private StudentDiscountService studentDiscountService;


	@Autowired
	private CompetitionService competitionService;
	@Autowired
	private CompetitionAwardService competitionAwardService;
	@Autowired
	private CompetitionProductionService competitionProductionService;
	@Autowired
	private CompetitionPlayerService competitionPlayerService;


	@Autowired
	private ContestService contestService;
	@Autowired
	private ContestAwardService contestAwardService;
	@Autowired
	private ContestProductionService contestProductionService;
	@Autowired
	private ContestProductionScoreService contestProductionScoreService;
	@Autowired
	private ContestPlayerService contestPlayerService;
	@Autowired
	private ContestWinService contestWinService;
	@Autowired
	private ContestStatService contestStatService;


	@Transactional
	public void execute4Test() {
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss"); 
		
		/*logger.debug("---------TaskOneJob----------->>>>sleep:"+df.format(new Date()));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
		
		
		//logger.debug("---------TaskOneJob----------->>>>time:"+df.format(new Date()));
		
		/*_counter++;
		logger.debug("---------TaskOneJob----------->>>>_counter:"+_counter);*/
		
		
		//logger.debug("-------------------------------------------");
	    
	}
	
	
	@Transactional
	public void execute4DataCheck() {
		/*SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		logger.debug("execute4DataCheck----------->>>>start----------->>>>time:"+df.format(new Date()));

		logger.debug("execute4DataCheck----------->>>>end----------->>>>time:"+df.format(new Date()));*/
	}




	/**********************************************************************************************************/
	/**********************************************************************************************************/
	/**********************************************************************************************************/




	@Transactional
	public void execute4CourseState() {
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		logger.debug("execute4CourseState----------->>>>start----------->>>>time:"+df.format(new Date()));

		//更新所有的课程的状态
		Map<String, Object> map = new HashMap<>();
		map.put("delFlag", "0");
		map.put("state", "2");//查询大于等于2的数据
		List<CourseExt> courseList = courseService.findAllListByCondition(map);
		Integer allCount = courseList.size();//总条目数
		Integer operateCount = 0;//操作条目数
		for(CourseExt c : courseList){
			Integer state = c.getState();//原始状态
			Integer courseFlag = c.getCourseFlag();//课程标识(1需购买课程、2非购买课程)

			Date signStartAt = c.getSignStartAt();//报名开始时间
			Date signEndAt = c.getSignEndAt();//报名结束时间
			Date teachStartAt = c.getTeachStartAt();//课程开始时间
			Date teachEndAt = c.getTeachEndAt();//课程结束时间

			//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
			Date currentDate = new Date();//当前时间
			if(courseFlag!=null && courseFlag==1){
				if(signStartAt.getTime()<=currentDate.getTime() && currentDate.getTime()<=signEndAt.getTime()){
					c.setState(3);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
				if(signEndAt.getTime()<=currentDate.getTime() && currentDate.getTime()<=teachStartAt.getTime()){
					c.setState(4);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
				if(teachStartAt.getTime()<=currentDate.getTime() && currentDate.getTime()<=teachEndAt.getTime()){
					c.setState(5);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
				if(teachEndAt.getTime()<=currentDate.getTime()){
					c.setState(6);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
			}

			if(courseFlag!=null && courseFlag==2){
				if(c.getState()!=null && c.getState().intValue()==2){
					c.setState(3);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
				if(teachStartAt.getTime()<=currentDate.getTime() && currentDate.getTime()<=teachEndAt.getTime()){
					c.setState(5);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
				if(teachEndAt.getTime()<=currentDate.getTime()){
					c.setState(6);
					operateCount++;
					//更新
					courseService.update(c);
					continue;
				}
			}
		}

		logger.debug("定时更新所有的课程的状态【完成】");

		logger.debug("execute4CourseState----------->>>>end----------->>>>time:"+df.format(new Date()));
	}



	@Transactional
	public void execute4OrderState() {
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		logger.debug("execute4OrderState----------->>>>start----------->>>>time:"+df.format(new Date()));

		//更新所有的订单的状态
		Integer allCount = 0;//总条目数
		Integer operateCount = 0;//操作条目数

		Map<String, Object> map = new HashMap<>();
		map.put("delFlag", "0");
		map.put("state", "2");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		map.put("newState", "7");
		//更新状态
		operateCount = studentCourseOrderService.updateByCondition(map);

		//查询订单支付详情,并更新订单信息
		operateCount = 0;//重新设置为0
		Map<String, Object> _map = new HashMap<>();
		_map.put("delFlag", "0");
		_map.put("state", "1");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		List<StudentCourseOrderExt> studentCourseOrderList = studentCourseOrderService.findAllListByCondition(_map);
		for(StudentCourseOrderExt order : studentCourseOrderList){

			//查询订单信息
			JSONObject obj = new JSONObject();
			obj.put("loginProductId", Constant.loginProductId);
			obj.put("loginProductKey", Constant.loginProductKey);
			String tradeNo = order.getOrderNo();//订单编号
			obj.put("tradeNo", tradeNo);

			JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("detailUrl"), obj);
			logger.debug("--"+jo.toString());

			/**
			{
				"result": true,
				"duration": 391,
				"type": "Detail execute",
				"code": 200,
				"message": "Success",
				"data": {
					"payAmount": 50,
					"detail": "详情",
					"accountId": 20111345,
					"tradeNo": "114542864360275968",
					"refundTotalAmount": 0,
					"state": 0,
					"productId": 20111105,
					"updatedAt": 1557973135000,
					"id": 9,
					"payTotalAmount": 0,
					"title": "课程",
					"refundAmount": 0,
					"orderAmount": 100,
					"createdAt": 1557916908000,
					"payType": "weixin-web",
					"payExpireSecond": 60,
					"delFlag": 0,
					"payInfoListInfo": [{
						"id": 25,
						"tradeId": 9,
						"accountId": 20111345,
						"type": "weixin-web",
						"outTradeNo": "91557973134828",
						"body": "课程",
						"detail": "详情",
						"infos": null,
						"feeType": null,
						"feeAmount": 50,
						"feeSuccessAmount": 0,
						"refundTotalAmount": 0,
						"refundAmount": 0,
						"spbillCreateIp": null,
						"timeStartAt": null,
						"timeExpireSecond": null,
						"prepayId": null,
						"request": null,
						"result": null,
						"state": 0,
						"updatedAt": 1557973135000,
						"createdAt": 1557973135000,
						"delFlag": 0
					},{
						"id": 21,
						"tradeId": 9,
						"accountId": 20111345,
						"type": "weixin-web",
						"outTradeNo": "91557916908422",
						"body": "课程2",
						"detail": "课程2",
						"infos": null,
						"feeType": null,
						"feeAmount": 1000,
						"feeSuccessAmount": 0,
						"refundTotalAmount": 0,
						"refundAmount": 0,
						"spbillCreateIp": null,
						"timeStartAt": null,
						"timeExpireSecond": null,
						"prepayId": null,
						"request": null,
						"result": null,
						"state": 0,
						"updatedAt": 1557918176000,
						"createdAt": 1557916908000,
						"delFlag": 0
					}],
			 		"refundInfoListInfo": []
			 	},
				"error": null,
				"version": null,
				"extParam": null
			}
			*/



			String type = "1";//类型(1支付、2退款)
			//类型(1支付、2退款)
			if(StringUtils.isNotEmpty(type) && "1".equals(type)){//支付

				if(order!=null && order.getState()==1){//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
					if(jo!=null && jo.getInt("code")>0 && jo.getInt("code")==200){
						JSONObject jo_data = jo.getJSONObject("data");
						int orderAmount = jo_data.getInt("orderAmount");//订单金额
						int payTotalAmount = jo_data.getInt("payTotalAmount");//支付总金额
						int refundTotalAmount = jo_data.getInt("refundTotalAmount");//退款总金额

						Double d = order.getOrderMoney().doubleValue()*100;//订单总金额,单位:分
						Double p = d-(order.getDiscountMoney()==null?0:order.getDiscountMoney().doubleValue()*100);//支付总金额,单位:分
						if(d.intValue()==orderAmount && p.intValue()==payTotalAmount){//判断订单是否支付成功

							if(order.getState()==1){
								order.setPayMoney(BigDecimal.valueOf(p/100.0));
								order.setPayAt(new Date());
								order.setState(3);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
								order.setUpdatedAt(new Date());
								order.setDelFlag(0);
								studentCourseOrderService.update(order);
							}
							//删除学生优惠券数据
							Map<String, Object> mm = new HashMap<>();
							mm.put("discountId", order.getDiscountId().toString());//优惠券id
							mm.put("studentId", order.getStudentId().toString());
							mm.put("state", "1");//状态(0未使用、1已使用)
							mm.put("delFlag", "1");
							studentDiscountService.update4Map(mm);



							if(order.getActivityTuanOrderId()==null || order.getActivityTuanOrderId()==0){
								//生成学生课程数据
								activityTuanOrderService.generateStudentCourseData(order);



								String channelId = order.getChannelId()==null?"":order.getChannelId().toString();
								String studentId = order.getStudentId().toString();
								String courseType = order.getCourseType().toString();//课程类型(1单课程、2系列课)
								Long courseGroupId = order.getCourseGroupId();
								String courseIds = order.getCourseIds();
								String cId = "";
								String cgId = "";
								if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
									cId = courseIds;
								}
								if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
									cgId = courseGroupId.toString();
								}
								//根据渠道课程的优惠券的关系生成学生优惠券信息
								if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(courseType) && StringUtils.isNotEmpty(studentId)){
									studentDiscountService.insert(channelId, courseType, studentId, cId, cgId, order.getId().toString());
								}



								//更新购课人数限制
								courseService.updateCourseEnrollCount(cId);
							}

							Integer _sourceType = 0;//来源类型(购买单课程1、购买系列课2、活动领取失败3、活动领取成功4)
							Long _courseId = 0L;//单课程id或系列课id
							if(order.getCourseType()==1){
								_sourceType = 1;
								_courseId = StringUtils.isNotEmpty(order.getCourseIds())?Long.parseLong(order.getCourseIds()):_courseId;
							}
							if(order.getCourseType()==2){
								_sourceType = 2;
								_courseId = order.getCourseGroupId();
							}
							channelStatisticsService.generateChannelStatistics(order.getChannelId()!=null?order.getChannelId().toString():"", order.getStudentId().toString(), _sourceType.toString(), order.getId().toString(), order.getCourseType().toString(), _courseId.toString(), "2");

						}

					}

				}


				/*order.setUpdatedAt(new Date());
				order.setDelFlag(0);
				studentCourseOrderService.update(order);*/

			}

			if(StringUtils.isNotEmpty(type) && "2".equals(type)){//退款
				//do nothing
			}


			operateCount++;
			logger.debug("更新订单orderNo=【"+order.getOrderNo()+"】");
		}

		//查询订单详情,并更新订单信息
		operateCount = 0;//重新设置为0
		Map<String, Object> __map = new HashMap<>();
		__map.put("delFlag", "0");
		__map.put("state", "5");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		List<StudentCourseOrderExt> __orderList = studentCourseOrderService.findAllListByCondition(__map);
		for(StudentCourseOrderExt order : __orderList){
			//查询订单信息
			JSONObject obj = new JSONObject();
			obj.put("loginProductId", Constant.loginProductId);
			obj.put("loginProductKey", Constant.loginProductKey);
			String tradeNo = order.getOrderNo();//订单编号
			obj.put("tradeNo", tradeNo);

			JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("detailUrl"), obj);
			logger.debug("--"+jo.toString());

			if(jo!=null && jo.getInt("code")>0 && jo.getInt("code")==200){
				JSONObject jo_data = jo.getJSONObject("data");
				int orderAmount = jo_data.getInt("orderAmount");//订单金额
				int payTotalAmount = jo_data.getInt("payTotalAmount");//支付总金额
				int refundTotalAmount = jo_data.getInt("refundTotalAmount");//退款总金额

				Double d = order.getOrderMoney().doubleValue()*100;//订单总金额,单位:分
				Double p = d-(order.getDiscountMoney()==null?0:order.getDiscountMoney().doubleValue()*100);//支付总金额,单位:分
				if(d.intValue()==orderAmount && p.intValue()==payTotalAmount && payTotalAmount==refundTotalAmount) {//判断订单是否退款成功
					order.setRefundMoney(new BigDecimal(refundTotalAmount/100.0));
					order.setPayAt(new Date());
					order.setState(6);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
					order.setUpdatedAt(new Date());
					order.setDelFlag(0);
					studentCourseOrderService.update(order);

					//删除学生课程数据
					Map<String, Object> m = new HashMap<>();
					m.put("sourceType", "1,3");//来源类型(正常购买1、免费领2、拼团购3、直接生成4)
					m.put("sourceId", order.getId().toString());//来源id(订单id或活动id)
					m.put("studentId", order.getStudentId().toString());
					m.put("updatedAt", new Date());
					m.put("delFlag", "1");
					studentCourseService.update4Del(m);

					//删除学生的订单关联的优惠券数据
					Map<String, Object> mm = new HashMap<>();
					mm.put("orderId", order.getId().toString());//订单id
					mm.put("studentId", order.getStudentId().toString());
					studentDiscountService.delete4Map(mm);
				}
			}

			operateCount++;
			logger.debug("更新订单orderNo=【"+order.getOrderNo()+"】");
		}

		//查询订单详情,并更新订单信息
		operateCount = 0;//重新设置为0
		Map<String, Object> ___map = new HashMap<>();
		___map.put("delFlag", "0");
		___map.put("state", "1");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		List<StudentCourseOrderExt> orderList = studentCourseOrderService.findAllListByCondition(___map);
		for(StudentCourseOrderExt order : orderList){
			if(order.getState()==1){
				Date updatedAt = order.getUpdatedAt();
				if((new Date().getTime() - updatedAt.getTime())/1000>Constant.payExpireSecond){
					order.setState(7);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
					order.setUpdatedAt(new Date());
					order.setDelFlag(0);
					studentCourseOrderService.update(order);
					continue;
				}


				if(Constant.closeOrderFlag4CourseState.intValue()==2){
					Integer courseType = order.getCourseType();//课程类型(1单课程、2系列课)
					if(courseType!=null && courseType==1){
						Course course = courseService.findById(Long.parseLong(order.getCourseIds()));
						if(course!=null && course.getState()>=4){//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
							order.setState(7);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
							order.setUpdatedAt(new Date());
							order.setDelFlag(0);
							studentCourseOrderService.update(order);
							continue;
						}
					}
					if(courseType!=null && courseType==2){
						Map<String, Object> mm = new HashMap<>();
						mm.put("delFlag", "0");
						mm.put("courseGroupId", order.getCourseGroupId());
						CourseGroupExt courseGroupExt = courseGroupService.findObjectByCondition(mm);
						List<CourseExt> courseExtList = courseGroupExt.getCourseInfoList();
						if(courseExtList!=null && courseExtList.size()>0){
							loop:for(CourseExt course : courseExtList){
								if(course.getStage()>0){
									if(course!=null && course.getState()>=4){//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
										order.setState(7);//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
										order.setUpdatedAt(new Date());
										order.setDelFlag(0);
										studentCourseOrderService.update(order);
										break loop;//退出loop循环
									}
								}
							}

						}
					}
				}
			}
		}
		logger.debug("定时更新所有的订单的状态【完成】");

		logger.debug("execute4OrderState----------->>>>end----------->>>>time:"+df.format(new Date()));
	}




	@Transactional
	public void execute4PinTuanOrder() {
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		logger.debug("execute4PinTuanOrder----------->>>>start----------->>>>time:"+df.format(new Date()));



		//查询已退款的拼团订单下的个人订单
		Map<String, Object> _map = new HashMap<>();
		_map.put("delFlag", "0");
		_map.put("state", "6");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
		List<StudentCourseOrderExt> _orderList = studentCourseOrderService.findListByCondition4ActivityTuanOrderId(_map);
		for(StudentCourseOrderExt order : _orderList){

			//更新拼团订单数据
			Long activityTuanOrderId = order.getActivityTuanOrderId();
			if(activityTuanOrderId!=null && activityTuanOrderId>0){
				Map<String, Object> mm = new HashMap<>();
				mm.put("delFlag", "0");
				mm.put("activityTuanOrderId", activityTuanOrderId);
				ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderService.findObjectByCondition(mm);
				ActivityTuan activityTuan = activityTuanService.findById(activityTuanOrderExt.getActivityTuanId());
				Integer restrictionCount = activityTuan.getRestrictionCount();
				Integer state = activityTuanOrderExt.getState();//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
				if(state==1){
					//如果是自己的拼团订单
					if(order.getStudentId()!=null && order.getStudentId()>0 && activityTuanOrderExt.getStudentId()!=null && activityTuanOrderExt.getStudentId()>0 && order.getStudentId().longValue()==activityTuanOrderExt.getStudentId().longValue()){
						Integer rpc = activityTuanOrderExt.getRealPeopleCount();
						if(rpc!=null && rpc<=1){
							//模拟随机用户
							if(Constant.mockFlag4PinTuan.intValue()==2) {//模拟标识(1不模拟、2模拟) - 拼团订单在截止时间时用户人数不足的情况，是否模拟用户
								activityTuanOrderService.mockRandomUser(activityTuanOrderExt, restrictionCount);
							}
							activityTuanOrderExt.setState(4);
							activityTuanOrderService.update(activityTuanOrderExt);
						}
						if(rpc!=null && rpc>1){
							//模拟随机用户
							if(Constant.mockFlag4PinTuan.intValue()==2) {//模拟标识(1不模拟、2模拟) - 拼团订单在截止时间时用户人数不足的情况，是否模拟用户
								activityTuanOrderService.mockRandomUser(activityTuanOrderExt, restrictionCount);
							}
							activityTuanOrderExt.setState(2);
							activityTuanOrderService.update(activityTuanOrderExt);
						}
					}
					//如果非自己的拼团订单
					if(order.getStudentId()!=null && order.getStudentId()>0 && activityTuanOrderExt.getStudentId()!=null && activityTuanOrderExt.getStudentId()>0 && order.getStudentId().longValue()!=activityTuanOrderExt.getStudentId().longValue()){
						//删除拼团订单中的个人信息
						String _studentIds = "";
						String _studentNames = "";
						String _studentAvatarUrls = "";
						String _studentOrderIds = "";
						Map<String, Object> mmm = new HashMap<>();
						mmm.put("delFlag", "0");
						mmm.put("activityTuanOrderId", activityTuanOrderExt.getId().toString());
						mmm.put("state", "3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
						List<StudentCourseOrderExt> oList = studentCourseOrderService.findListByActivityTuanOrderId(mmm);
						for(StudentCourseOrderExt o : oList){
							Long sId = o.getStudentId();
							Student s = studentService.findById(sId);
							_studentIds = _studentIds + "," + s.getId();
							_studentNames = _studentNames + "," + s.getName();
							_studentAvatarUrls = _studentAvatarUrls + "," + s.getAvatarUrl();
							_studentOrderIds = _studentOrderIds + "," + o.getId();
						}
						_studentIds = _studentIds.startsWith(",")?_studentIds.substring(1,_studentIds.length()):_studentIds;
						_studentNames = _studentNames.startsWith(",")?_studentNames.substring(1,_studentNames.length()):_studentNames;
						_studentAvatarUrls = _studentAvatarUrls.startsWith(",")?_studentAvatarUrls.substring(1,_studentAvatarUrls.length()):_studentAvatarUrls;
						_studentOrderIds = _studentOrderIds.startsWith(",")?_studentOrderIds.substring(1,_studentOrderIds.length()):_studentOrderIds;
						activityTuanOrderExt.setPeopleCount(oList.size());
						activityTuanOrderExt.setRealPeopleCount(oList.size());
						activityTuanOrderExt.setStudentIds(_studentIds);
						activityTuanOrderExt.setStudentNames(_studentNames);
						activityTuanOrderExt.setStudentAvatarUrls(_studentAvatarUrls);
						activityTuanOrderExt.setStudentOrderIds(_studentOrderIds);
						activityTuanOrderExt.setState(1);//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
						activityTuanOrderService.update(activityTuanOrderExt);
					}
				}
				if(state==2){
					//do nothing
				}
			}
		}



		//查询拼团订单
		Map<String, Object> pinTuanMap = new HashMap<>();
		pinTuanMap.put("delFlag", "0");
		pinTuanMap.put("state", "0,1");//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
		List<ActivityTuanOrderExt> ptoList = activityTuanOrderService.findListByCondition(pinTuanMap);
		for(ActivityTuanOrderExt activityTuanOrderExt : ptoList){
			ActivityTuan activityTuan = activityTuanService.findById(activityTuanOrderExt.getActivityTuanId());
			Integer restrictionCount = activityTuan.getRestrictionCount();


			if(activityTuanOrderExt.getState()==0){
				Map<String, Object> mm = new HashMap<>();
				mm = new HashMap<>();
				mm.put("delFlag", "0");
				mm.put("activityTuanOrderId", activityTuanOrderExt.getId().toString());
				mm.put("state", "3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
				mm.put("studentId", activityTuanOrderExt.getStudentId().toString());
				List<StudentCourseOrderExt> orderList = studentCourseOrderService.findListByActivityTuanOrderId(mm);
				StudentCourseOrderExt order = null;
				if(orderList!=null && orderList.size()>0){
					order = orderList.get(0);
					if(order!=null && order.getId()>0){
						Long sId = order.getStudentId();
						Student s = studentService.findById(sId);
						activityTuanOrderExt.setPeopleCount(1);
						activityTuanOrderExt.setRealPeopleCount(1);
						activityTuanOrderExt.setStudentIds(String.valueOf(s.getId()));
						activityTuanOrderExt.setStudentNames(s.getName());
						activityTuanOrderExt.setStudentAvatarUrls(s.getAvatarUrl());
						activityTuanOrderExt.setStudentOrderIds(String.valueOf(order.getId()));
						activityTuanOrderExt.setState(1);//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
						activityTuanOrderService.update(activityTuanOrderExt);
					}
				}
				continue;
			}

			if(activityTuanOrderExt.getState()==1){
				Map<String, Object> mm = new HashMap<>();
				mm.put("delFlag", "0");
				mm.put("activityTuanOrderId", activityTuanOrderExt.getId().toString());
				mm.put("state", "3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
				List<StudentCourseOrderExt> oList = studentCourseOrderService.findListByActivityTuanOrderId(mm);
				//支付成功，拼团订单关联的个人订单数大于规定人数，需创建新的拼团订单信息，并更新个人订单的拼团信息
				if(oList.size()>=restrictionCount.intValue()){
					//更新已有的拼团订单
					String _studentIds = "";
					String _studentNames = "";
					String _studentAvatarUrls = "";
					String _studentOrderIds = "";
					for(int i=0;i<restrictionCount.intValue();i++){
						StudentCourseOrderExt o = oList.get(i);
						o.setActivityTuanOrderId(activityTuanOrderExt.getId());
						studentCourseOrderService.update(o);

						Long sId = o.getStudentId();
						Student s = studentService.findById(sId);
						_studentIds = _studentIds + "," + s.getId();
						_studentNames = _studentNames + "," + s.getName();
						_studentAvatarUrls = _studentAvatarUrls + "," + s.getAvatarUrl();
						_studentOrderIds = _studentOrderIds + "," + o.getId();
					}
					_studentIds = _studentIds.startsWith(",")?_studentIds.substring(1,_studentIds.length()):_studentIds;
					_studentNames = _studentNames.startsWith(",")?_studentNames.substring(1,_studentNames.length()):_studentNames;
					_studentAvatarUrls = _studentAvatarUrls.startsWith(",")?_studentAvatarUrls.substring(1,_studentAvatarUrls.length()):_studentAvatarUrls;
					_studentOrderIds = _studentOrderIds.startsWith(",")?_studentOrderIds.substring(1,_studentOrderIds.length()):_studentOrderIds;
					activityTuanOrderExt.setPeopleCount(restrictionCount);
					activityTuanOrderExt.setRealPeopleCount(restrictionCount);
					activityTuanOrderExt.setStudentIds(_studentIds);
					activityTuanOrderExt.setStudentNames(_studentNames);
					activityTuanOrderExt.setStudentAvatarUrls(_studentAvatarUrls);
					activityTuanOrderExt.setStudentOrderIds(_studentOrderIds);
					activityTuanOrderExt.setState(2);//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
					activityTuanOrderService.update(activityTuanOrderExt);

					List<ActivityTuanOrderExt> atoList = new ArrayList<>();
					atoList.add(activityTuanOrderExt);

					//超出人数限制，超出的个人订单，需创建新的拼团订单信息
					Integer diff = oList.size()-restrictionCount;
					int activityTuanOrderCount = diff / restrictionCount;
					if (diff % restrictionCount != 0)
						activityTuanOrderCount++;

					for(int i=1;i<(activityTuanOrderCount+1);i++) {
						//创建新的拼团订单
						ActivityTuanOrderExt ato = new ActivityTuanOrderExt();
						ato.setActivityTuanId(activityTuan.getId());
						ato.setStudentId(0L);
						ato.setPeopleCount(0);
						ato.setRealPeopleCount(0);
						ato.setStartAt(new Date());
						ato.setEndAt(DateUtils.addHours(new Date(), Constant.createEndDate4PinTuan));
						ato.setRemark("");
						ato.setState(0);//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
						ato.setCreatedAt(new Date());
						ato.setUpdatedAt(new Date());
						ato.setDelFlag(0);
						activityTuanOrderService.insert(ato);

						//更新个人订单
						_studentIds = "";
						_studentNames = "";
						_studentAvatarUrls = "";
						_studentOrderIds = "";
						Integer rpc = 0;
						for(int j=0;j<restrictionCount.intValue();j++){
							StudentCourseOrderExt o = oList.get(restrictionCount*i+j);
							if(o!=null && o.getId()>0){
								o.setActivityTuanOrderId(ato.getId());
								studentCourseOrderService.update(o);

								Long sId = o.getStudentId();
								Student s = studentService.findById(sId);
								_studentIds = _studentIds + "," + s.getId();
								_studentNames = _studentNames + "," + s.getName();
								_studentAvatarUrls = _studentAvatarUrls + "," + s.getAvatarUrl();
								_studentOrderIds = _studentOrderIds + "," + o.getId();

								rpc++;
							}
						}

						//更新新的拼团订单
						_studentIds = _studentIds.startsWith(",")?_studentIds.substring(1,_studentIds.length()):_studentIds;
						_studentNames = _studentNames.startsWith(",")?_studentNames.substring(1,_studentNames.length()):_studentNames;
						_studentAvatarUrls = _studentAvatarUrls.startsWith(",")?_studentAvatarUrls.substring(1,_studentAvatarUrls.length()):_studentAvatarUrls;
						_studentOrderIds = _studentOrderIds.startsWith(",")?_studentOrderIds.substring(1,_studentOrderIds.length()):_studentOrderIds;
						ato.setStudentIds(_studentIds);
						ato.setStudentNames(_studentNames);
						ato.setStudentAvatarUrls(_studentAvatarUrls);
						ato.setStudentOrderIds(_studentOrderIds);
						ato.setPeopleCount(rpc);
						ato.setRealPeopleCount(rpc);
						ato.setState(1);//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
						activityTuanOrderService.update(ato);


						Integer rpCount = ato.getRealPeopleCount();
						if(restrictionCount.intValue()>rpCount){
							if(Constant.mockFlag4PinTuan.intValue()==2) {//模拟标识(1不模拟、2模拟) - 拼团订单在截止时间时用户人数不足的情况，是否模拟用户，使拼团订单能够成功
								activityTuanOrderService.mockRandomUser(ato, restrictionCount);
							}
						}
						Integer pCount = ato.getPeopleCount();
						if(restrictionCount.intValue()==pCount){
							ato.setState(2);//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
							activityTuanOrderService.update(ato);
						}

						atoList.add(ato);
					}
					for(ActivityTuanOrderExt ato : atoList){
						Integer pCount = ato.getPeopleCount();
						if(restrictionCount.intValue()==pCount.intValue() && ato.getState()==2){//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
							Map<String, Object> mmm = new HashMap<>();
							mmm = new HashMap<>();
							mmm.put("delFlag", "0");
							mmm.put("activityTuanOrderId", ato.getId().toString());
							mmm.put("state", "3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
							List<StudentCourseOrderExt> ol = studentCourseOrderService.findListByActivityTuanOrderId(mmm);
							//生成学生课程数据
							for(StudentCourseOrderExt order : ol){
								activityTuanOrderService.generateStudentCourseData(order);



								String channelId = order.getChannelId()==null?"":order.getChannelId().toString();
								String studentId = order.getStudentId().toString();
								String courseType = order.getCourseType().toString();//课程类型(1单课程、2系列课)
								Long courseGroupId = order.getCourseGroupId();
								String courseIds = order.getCourseIds();
								String cId = "";
								String cgId = "";
								if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
									cId = courseIds;
								}
								if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
									cgId = courseGroupId.toString();
								}
								//根据渠道课程的优惠券的关系生成学生优惠券信息
								if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(courseType) && StringUtils.isNotEmpty(studentId)){
									studentDiscountService.insert(channelId, courseType, studentId, cId, cgId, order.getId().toString());
								}



								//更新购课人数限制
								courseService.updateCourseEnrollCount(cId);



								//发送短信
								try{
									JSONObject obj = new JSONObject();
									obj.put("loginProductId", Constant.loginProductId);
									obj.put("loginProductKey", Constant.loginProductKey);
									obj.put("type", "notice-tuan-success");
									obj.put("phones", order.getPhone());//手机号,多个","分隔
									String title = "";
									if("1".equals(courseType)){
										Course courseInfo = courseService.findById(Long.parseLong(order.getCourseIds()));
										title = courseInfo.getName();
									}
									if("2".equals(courseType)){
										CourseGroup courseGroupInfo = courseGroupService.findById(order.getCourseGroupId());
										title = courseGroupInfo.getName();
									}
									String smsSendMsg4PinTuan = Constant.smsSendMsg4PinTuan;
									smsSendMsg4PinTuan = smsSendMsg4PinTuan.replace("COURSENAME", title);
									obj.put("param", smsSendMsg4PinTuan);
									JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("smsSendUrl"), obj);
									logger.debug("--"+jo.toString());
								}catch (Exception e){
									logger.error("[sms send fail]--"+"orderId="+order.getId()+"&phone="+order.getPhone());
								}
							}
						}
					}
					continue;
				}

				//查询拼团订单,拼团人数不够时间截止时需模拟个人信息
				if(oList.size()<restrictionCount.intValue()){
					//更新已有的拼团订单
					String _studentIds = "";
					String _studentNames = "";
					String _studentAvatarUrls = "";
					String _studentOrderIds = "";
					for(int i=0;i<oList.size();i++){
						StudentCourseOrderExt o = oList.get(i);
						o.setActivityTuanOrderId(activityTuanOrderExt.getId());
						studentCourseOrderService.update(o);

						Long sId = o.getStudentId();
						Student s = studentService.findById(sId);
						_studentIds = _studentIds + "," + s.getId();
						_studentNames = _studentNames + "," + s.getName();
						_studentAvatarUrls = _studentAvatarUrls + "," + s.getAvatarUrl();
						_studentOrderIds = _studentOrderIds + "," + o.getId();
					}
					_studentIds = _studentIds.startsWith(",")?_studentIds.substring(1,_studentIds.length()):_studentIds;
					_studentNames = _studentNames.startsWith(",")?_studentNames.substring(1,_studentNames.length()):_studentNames;
					_studentAvatarUrls = _studentAvatarUrls.startsWith(",")?_studentAvatarUrls.substring(1,_studentAvatarUrls.length()):_studentAvatarUrls;
					_studentOrderIds = _studentOrderIds.startsWith(",")?_studentOrderIds.substring(1,_studentOrderIds.length()):_studentOrderIds;
					activityTuanOrderExt.setPeopleCount(oList.size());
					activityTuanOrderExt.setRealPeopleCount(oList.size());
					activityTuanOrderExt.setStudentIds(_studentIds);
					activityTuanOrderExt.setStudentNames(_studentNames);
					activityTuanOrderExt.setStudentAvatarUrls(_studentAvatarUrls);
					activityTuanOrderExt.setStudentOrderIds(_studentOrderIds);
					activityTuanOrderExt.setState(1);//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
					activityTuanOrderService.update(activityTuanOrderExt);

					if(activityTuanOrderExt.getEndAt().getTime()<=new Date().getTime()){
						if(Constant.mockFlag4PinTuan.intValue()==2) {//模拟标识(1不模拟、2模拟) - 拼团订单在截止时间时用户人数不足的情况，是否模拟用户，使拼团订单能够成功
							activityTuanOrderService.mockRandomUser(activityTuanOrderExt, restrictionCount);
						}
						if(restrictionCount.intValue()==activityTuanOrderExt.getPeopleCount().intValue()){
							activityTuanOrderExt.setState(2);//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
							activityTuanOrderService.update(activityTuanOrderExt);
						}
						if(restrictionCount.intValue()==activityTuanOrderExt.getPeopleCount().intValue() && activityTuanOrderExt.getState()==2){//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
							Map<String, Object> mmm = new HashMap<>();
							mmm = new HashMap<>();
							mmm.put("delFlag", "0");
							mmm.put("activityTuanOrderId", activityTuanOrderExt.getId().toString());
							mmm.put("state", "3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
							List<StudentCourseOrderExt> ol = studentCourseOrderService.findListByActivityTuanOrderId(mmm);
							//生成学生课程数据
							for(StudentCourseOrderExt order : ol){
								activityTuanOrderService.generateStudentCourseData(order);



								String channelId = order.getChannelId()==null?"":order.getChannelId().toString();
								String studentId = order.getStudentId().toString();
								String courseType = order.getCourseType().toString();//课程类型(1单课程、2系列课)
								Long courseGroupId = order.getCourseGroupId();
								String courseIds = order.getCourseIds();
								String cId = "";
								String cgId = "";
								if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
									cId = courseIds;
								}
								if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
									cgId = courseGroupId.toString();
								}
								//根据渠道课程的优惠券的关系生成学生优惠券信息
								if(StringUtils.isNotEmpty(channelId) && StringUtils.isNotEmpty(courseType) && StringUtils.isNotEmpty(studentId)){
									studentDiscountService.insert(channelId, courseType, studentId, cId, cgId, order.getId().toString());
								}



								//更新购课人数限制
								courseService.updateCourseEnrollCount(cId);



								//发送短信
								try{
									JSONObject obj = new JSONObject();
									obj.put("loginProductId", Constant.loginProductId);
									obj.put("loginProductKey", Constant.loginProductKey);
									obj.put("type", "notice-tuan-success");
									obj.put("phones", order.getPhone());//手机号,多个","分隔
									String title = "";
									if("1".equals(courseType)){
										Course courseInfo = courseService.findById(Long.parseLong(order.getCourseIds()));
										title = courseInfo.getName();
									}
									if("2".equals(courseType)){
										CourseGroup courseGroupInfo = courseGroupService.findById(order.getCourseGroupId());
										title = courseGroupInfo.getName();
									}
									String smsSendMsg4PinTuan = Constant.smsSendMsg4PinTuan;
									smsSendMsg4PinTuan = smsSendMsg4PinTuan.replace("COURSENAME", title);
									obj.put("param", smsSendMsg4PinTuan);
									JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("smsSendUrl"), obj);
									logger.debug("--"+jo.toString());
								}catch (Exception e){
									logger.error("[sms send fail]--"+"orderId="+order.getId()+"&phone="+order.getPhone());
								}
							}
						}
					}
					continue;
				}
			}
		}






		//查询拼团订单详情,并更新订单信息
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("state", "1");//状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)
		List<ActivityTuanOrderExt> list = activityTuanOrderService.findListByCondition(m);
		for(ActivityTuanOrderExt ato : list){
			//查询拼团活动
			ActivityTuan activityTuan = activityTuanService.findById(ato.getActivityTuanId());
			//个人订单都退款成功，更新拼团订单为拼团结束
			Map<String, Object> mm = new HashMap<>();
			mm = new HashMap<>();
			mm.put("delFlag", "0");
			mm.put("activityTuanOrderId", ato.getId().toString());
			mm.put("state", "6");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
			List<StudentCourseOrderExt> oList = studentCourseOrderService.findListByActivityTuanOrderId(mm);
			if(activityTuan.getRestrictionCount().intValue()==oList.size()){
				ato.setState(4);
				ato.setUpdatedAt(new Date());
				activityTuanOrderService.update(ato);
				continue;
			}

			//拼团订单时间截止后人数不符，更新个人订单为申请退款，并更新拼团订单为拼团失败
			if(ato.getEndAt().getTime()<=new Date().getTime()){
				if(activityTuan.getRestrictionCount().intValue()>ato.getPeopleCount().intValue()){
					mm = new HashMap<>();
					mm.put("delFlag", "0");
					mm.put("activityTuanOrderId", ato.getId().toString());
					mm.put("state", "3");//状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
					oList = studentCourseOrderService.findListByActivityTuanOrderId(mm);
					for(StudentCourseOrderExt o : oList){
						o.setState(4);
						o.setUpdatedAt(new Date());
						studentCourseOrderService.update(o);
					}
					continue;
				}
			}
		}

		logger.debug("定时更新拼团订单【完成】");

		logger.debug("execute4PinTuanOrder----------->>>>end----------->>>>time:"+df.format(new Date()));
	}




	@Transactional
	public void execute4OnlineFlag() {
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		logger.debug("execute4OnlineFlag----------->>>>start----------->>>>time:"+df.format(new Date()));

		//更新所有的onlineFlag为离线
		String onlineFlag = "1";//在线标识(1离线、2在线)
		studentCourseHomeworkService.update4OnlineFlag(onlineFlag);

		logger.debug("定时更新所有的onlineFlag为离线【完成】");

		logger.debug("execute4OnlineFlag----------->>>>end----------->>>>time:"+df.format(new Date()));
	}





	@Transactional
	public void execute4CourseStart() {
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		logger.debug("execute4CourseStart----------->>>>start----------->>>>time:"+df.format(new Date()));

		//定时查询课程的状态并发送公众号消息
		Map<String, Object> map = new HashMap<>();
		map.put("delFlag", "0");
		map.put("state", "2");//查询大于等于2的数据
		List<CourseExt> courseList = courseService.findAllListByCondition(map);
		Integer allCount = courseList.size();//总条目数
		Integer operateCount = 0;//操作条目数
		for(CourseExt c : courseList){
			/*Integer state = c.getState();//原始状态

			Date signStartAt = c.getSignStartAt();//报名开始时间
			Date signEndAt = c.getSignEndAt();//报名结束时间
			Date teachStartAt = c.getTeachStartAt();//课程开始时间
			Date teachEndAt = c.getTeachEndAt();//课程结束时间

			//状态,1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束
			Date currentDate = new Date();//当前时间

			Map<String, Object> m = new HashMap<>();
			m.put("courseId", c.getId().toString());
			m.put("courseStartHintFlag", "1");//课程开始提醒标识(1未提醒、2已提醒)
			m.put("state", "2");//状态(1未分配教师、2已分配教师)
			m.put("delFlag", "0");
			List<StudentCourseExt> studentCourseList = studentCourseService.findListByCourseId(m);

			if((teachStartAt.getTime()-currentDate.getTime())/1000<=5400 && (teachStartAt.getTime()-currentDate.getTime())/1000>=1800){//提前5400-1800秒(1.5小时-0.5小时)
				//消息发送
				for(StudentCourseExt sc : studentCourseList){

					//公众号消息发送
					if(Constant.msgSendFlag4Official==2){//是否进行公众号消息发送(1不发送、2发送)
						JSONObject obj = new JSONObject();
						obj.put("loginProductId", Constant.loginProductId);
						obj.put("loginProductKey", Constant.loginProductKey);
						obj.put("appId", Constant.appId4Official);
						obj.put("appKey", Constant.appKey4Official);
						obj.put("messageId", Constant.messageId2CourseStart4Official);
						String wordData = Constant.wordData2CourseStart4Official;
						wordData = wordData.replace("KEYWORD1",c.getName());//课程名称
						wordData = wordData.replace("KEYWORD2",DateUtils.date2String(teachStartAt));//时间
						obj.put("wordData", wordData);
						Long studentId = sc.getStudentId();
						Student s = studentService.findById(studentId);
						obj.put("accountId", s.getAccountId());
						obj.put("webUrl", "");
						obj.put("xcxAppId", "");
						obj.put("xcxPagepath", "");

						try{
							logger.debug("[CourseStart send msg]--"+obj.toString());
							JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("msgSend4OfficialUrl"), obj);
							logger.debug("[CourseStart send msg]--"+jo.toString());

							sc.setCourseStartHintFlag(2);
							studentCourseService.update(sc);
							operateCount++;
						}catch (Exception e){
							logger.error("[CourseStart send msg fail]");
						}
					}
				}

			}*/




			//查询学生课程列表
			Map<String, Object> m = new HashMap<>();
			m.put("courseId", c.getId().toString());
			//m.put("courseStartHintFlag", "1");//课程开始提醒标识(1未提醒、2已提醒)
			m.put("state", "2");//状态(1未分配教师、2已分配教师)
			m.put("delFlag", "0");
			List<StudentCourseExt> studentCourseList = studentCourseService.findListByCourseId(m);

			//查询课时
			Map<String, Object> cl_m = new HashMap<>();
			cl_m.put("delFlag","0");
			cl_m.put("enableFlag","1");//启用标记(1正常、2禁用)
			cl_m.put("courseId", c.getId().toString());
			List<CourseLessonExt> courseLessonInfoList = courseLessonService.findListByCondition(cl_m);
			for(CourseLessonExt cl : courseLessonInfoList){
				Date currentDate = new Date();//当前时间
				Date teachStartAt = cl.getStartAt();//设置为第一课时的开始时间

				if(teachStartAt!=null && (teachStartAt.getTime()-currentDate.getTime())/1000<=5400 && (teachStartAt.getTime()-currentDate.getTime())/1000>=1800){//提前5400-1800秒(1.5小时-0.5小时)
					//消息发送
					for(StudentCourseExt sc : studentCourseList){

						Map<String, Object> mm = new HashMap<>();
						mm.put("courseId", c.getId().toString());
						mm.put("courseLessonId", cl.getId().toString());
						mm.put("delFlag", "0");
						mm.put("personId", sc.getStudentId().toString());
						WeixinNotifyExt weixinNotifyExt = weixinNotifyService.findObjectByCondition(mm);
						if(weixinNotifyExt==null || weixinNotifyExt.getId()==0){

							//公众号消息发送
							if(Constant.msgSendFlag4Official==2){//是否进行公众号消息发送(1不发送、2发送)
								JSONObject obj = new JSONObject();
								obj.put("loginProductId", Constant.loginProductId);
								obj.put("loginProductKey", Constant.loginProductKey);
								obj.put("appId", Constant.appId4Official);
								obj.put("appKey", Constant.appKey4Official);
								obj.put("messageId", Constant.messageId2CourseStart4Official);
								String wordData = Constant.wordData2CourseStart4Official;
								wordData = wordData.replace("KEYWORD1",c.getName());//课程名称
								//wordData = wordData.replace("KEYWORD2",cl.getTitle());//课时标题
								wordData = wordData.replace("KEYWORD2",DateUtils.date2String(teachStartAt));//时间
								obj.put("wordData", wordData);
								Long studentId = sc.getStudentId();
								Student s = studentService.findById(studentId);
								obj.put("accountId", s.getAccountId());
								obj.put("webUrl", "");
								obj.put("xcxAppId", "");
								obj.put("xcxPagepath", "");

								try{
									logger.debug("[CourseStart send msg]--"+obj.toString());
									JSONObject jo = HttpUtils.sendPost4JSON(PropertiesUtils.getKeyValue("msgSend4OfficialUrl"), obj);
									logger.debug("[CourseStart send msg]--"+jo.toString());

									weixinNotifyExt = new WeixinNotifyExt();
									weixinNotifyExt.setType(2);//类型(1作业批改完成提醒、2开课提醒、3作业完成提醒)
									weixinNotifyExt.setPersonId(studentId);
									weixinNotifyExt.setCourseId(c.getId());
									weixinNotifyExt.setCourseLessonId(cl.getId());
									weixinNotifyExt.setContent(wordData);
									weixinNotifyExt.setDetail(obj.toString());
									weixinNotifyExt.setRemark("");
									weixinNotifyExt.setState(0);
									weixinNotifyExt.setCreatedAt(new Date());
									weixinNotifyExt.setUpdatedAt(new Date());
									weixinNotifyExt.setDelFlag(0);
									weixinNotifyService.insert(weixinNotifyExt);

									operateCount++;
								}catch (Exception e){
									logger.error("[CourseStart send msg fail]");
								}
							}
						}
					}
				}
			}
		}
		logger.debug("定时查询课程的状态并发送公众号消息【完成】");

		logger.debug("execute4CourseStart----------->>>>end----------->>>>time:"+df.format(new Date()));
	}




	@Transactional
	public void execute4CompetitionState() {
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		logger.debug("execute4CompetitionState----------->>>>start----------->>>>time:"+df.format(new Date()));

		Date currentDate = new Date();//当前时间

		//更新所有的课程的状态
		Map<String, Object> map = new HashMap<>();
		map.put("delFlag", "0");
		List<CompetitionExt> competitionList = competitionService.findAllListByCondition(map);
		for(CompetitionExt competition : competitionList){
			Date signupTime = competition.getSignupTime();//报名时间
			Date trainTime = competition.getTrainTime();//培训时间
			Date processingTime = competition.getProcessingTime();//开始时间
			Date voteStartTime = competition.getVoteStartTime();//投票开始时间
			Date voteStopTime = competition.getVoteStopTime();//投票结束时间
			Date publishTime = competition.getPublishTime();//获奖公布时间
			Integer state = competition.getState();//状态(0初始化、1报名中、2培训中、3进行中、4投票中、5投票截止、6公布中)

			if(signupTime.getTime()<=currentDate.getTime() && currentDate.getTime()<trainTime.getTime()){
				state = 1;
			}
			if(trainTime.getTime()<=currentDate.getTime() && currentDate.getTime()<processingTime.getTime()){
				state = 2;
			}
			if(processingTime.getTime()<=currentDate.getTime() && currentDate.getTime()<voteStartTime.getTime()){
				state = 3;
			}
			if(voteStartTime.getTime()<=currentDate.getTime() && currentDate.getTime()<voteStopTime.getTime()){
				state = 4;
			}
			if(voteStopTime.getTime()<=currentDate.getTime() && currentDate.getTime()<publishTime.getTime()){
				state = 5;
			}
			if(publishTime.getTime()<=currentDate.getTime()){
				state = 6;
			}

			competition.setState(state);
			competition.setUpdatedAt(new Date());
			competitionService.update(competition);


			if(state>=5){
				//更新比赛参与者的奖励项
				Page page = new Page();
				Map<String, Object> map4Param = new HashMap<String, Object>();
				map4Param.put("delFlag", "0");
				map4Param.put("competitionId", competition.getId().toString());
				List<CompetitionAwardExt> awardList = competitionAwardService.findListByCondition(map4Param);

				Integer startIndex = 0;
				for(CompetitionAwardExt awardInfo : awardList){
					Integer userCount = (awardInfo.getUserCount()!=null&&awardInfo.getUserCount()>0)?awardInfo.getUserCount():0;
					if(userCount==null || userCount==0){
						continue;
					}
					page.setStartIndex(startIndex);
					page.setPageSize(userCount);

					map4Param = new HashMap<String, Object>();
					map4Param.put("delFlag", "0");
					map4Param.put("state", "1");//状态(0未完成、1已完成)
					map4Param.put("competitionId", competition.getId().toString());
					map4Param.put("page", page);
					Integer count = competitionPlayerService.findCountByCondition4Vote(map4Param);
					List<CompetitionPlayerExt> playerList = competitionPlayerService.findListByCondition4Vote(map4Param);

					for(int i=0;i<playerList.size();i++){
						CompetitionPlayerExt competitionPlayerExt = playerList.get(i);
						Map<String, Object> m = new HashMap<>();
						m.put("delFlag", "0");
						m.put("competitionId", competition.getId().toString());
						m.put("playerId", competitionPlayerExt.getId().toString());
						CompetitionProductionExt competitionProductionExt = competitionProductionService.findObjectByCondition(m);
						if(competitionProductionExt!=null && competitionProductionExt.getId()>0){
							competitionProductionExt.setAwardId(awardInfo.getId());//处理优胜者的奖励项
							competitionProductionExt.setCompetitionAwardInfo(awardInfo);
						}
						competitionPlayerExt.setCompetitionProductionInfo(competitionProductionExt);
					}

					startIndex = startIndex + userCount;//下一次查询，从前一次的userCount开始
				}
			}
		}


		logger.debug("定时更新所有的比赛的状态【完成】");

		logger.debug("execute4CompetitionState----------->>>>end----------->>>>time:"+df.format(new Date()));
	}


	@Transactional
	public void execute4ContestData() {
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		logger.debug("execute4ContestData----------->>>>start----------->>>>time:"+df.format(new Date()));

		Date currentDate = new Date();//当前时间

		//更新所有的课程的状态
		Map<String, Object> map = new HashMap<>();
		map.put("delFlag", "0");
		map.put("auditFlag", "3");//审核标识(1待审核、2审核不通过、3审核通过)
		map.put("state", "2,3,4,5,6");//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)
		List<ContestExt> contestList = contestService.findAllListByCondition(map);
		for(ContestExt contestExt : contestList){
			Date signupStartTime = contestExt.getSignupStartTime();//报名开始时间
			Date signupStopTime = contestExt.getSignupStopTime();//报名结束时间
			Date commitStopTime = contestExt.getCommitStopTime();//提交结束时间
			Date publishStartTime = contestExt.getPublishStartTime();//获奖公布开始时间
			Date publishStopTime = contestExt.getPublishStopTime();//获奖公布结束时间
			Integer state = contestExt.getState();//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)

			if(signupStopTime==null){
				if(signupStartTime.getTime()<=currentDate.getTime() && currentDate.getTime()<commitStopTime.getTime()){
					state = 3;
				}
				if(commitStopTime.getTime()<=currentDate.getTime() && currentDate.getTime()<publishStartTime.getTime()){
					state = 4;
				}
				if(publishStartTime.getTime()<=currentDate.getTime() && currentDate.getTime()<publishStopTime.getTime()){
					state = 5;
				}
				if(publishStopTime.getTime()<=currentDate.getTime()){
					state = 6;
				}
			}else{
				if(signupStopTime.getTime()<=currentDate.getTime() && currentDate.getTime()<commitStopTime.getTime()){
					state = 3;
				}
				if(commitStopTime.getTime()<=currentDate.getTime() && currentDate.getTime()<publishStartTime.getTime()){
					state = 4;
				}
				if(publishStartTime.getTime()<=currentDate.getTime() && currentDate.getTime()<publishStopTime.getTime()){
					state = 5;
				}
				if(publishStopTime.getTime()<=currentDate.getTime()){
					state = 6;
				}
			}

			contestExt.setState(state);
			contestExt.setUpdatedAt(new Date());
			contestService.update(contestExt);



			if(state>=4){
				//更新比赛参与者的奖励项
				Page page = new Page();
				Map<String, Object> mm = new HashMap<String, Object>();
				mm.put("delFlag", "0");
				mm.put("contestId", contestExt.getId().toString());
				List<ContestAwardExt> awardList = contestAwardService.findListByContestId(mm);

				//评审方式(1投票、2评审)
				Integer reviewWay = contestExt.getReviewWay();

				Integer startIndex = 0;
				for(ContestAwardExt awardInfo : awardList){
					Integer playerCount = (awardInfo.getPlayerCount()!=null&&awardInfo.getPlayerCount()>0)?awardInfo.getPlayerCount():0;
					if(playerCount==null || playerCount==0){
						continue;
					}
					page.setStartIndex(startIndex);
					page.setPageSize(playerCount);

					Map<String, Object> map4Param = new HashMap<String, Object>();
					map4Param.put("delFlag", "0");
					map4Param.put("state", "1");//状态(0未完成、1已完成)
					map4Param.put("contestId", contestExt.getId().toString());
					map4Param.put("page", page);
					Integer count = 0;
					List<ContestPlayerExt> playerList = new ArrayList<>();
					if(reviewWay!=null && reviewWay==1){
						count = contestPlayerService.findCountByCondition4Vote(map4Param);
						playerList = contestPlayerService.findListByCondition4Vote(map4Param);
					}
					if(reviewWay!=null && reviewWay==2){
						count = contestPlayerService.findCountByCondition4Score(map4Param);
						playerList = contestPlayerService.findListByCondition4Score(map4Param);
					}

					for(int i=0;i<playerList.size();i++){
						ContestPlayerExt contestPlayerExt = playerList.get(i);
						Map<String, Object> m = new HashMap<>();
						m.put("delFlag", "0");
						m.put("contestId", contestExt.getId().toString());
						m.put("playerId", contestPlayerExt.getId().toString());
						ContestProductionExt contestProductionExt = contestProductionService.findObjectByCondition(m);
						if(contestProductionExt!=null && contestProductionExt.getId()>0){
							contestProductionExt.setAwardId(awardInfo.getId());//处理优胜者的奖励项
						}
						contestPlayerExt.setContestProductionInfo(contestProductionExt);



						//更新获胜者信息
						ContestWinExt contestWinExt = new ContestWinExt();
						contestWinExt.setContestId(contestExt.getId());
						contestWinExt.setProductionId(contestProductionExt.getId());
						contestWinExt.setPlayerId(contestPlayerExt.getId());
						contestWinExt.setAwardId(awardInfo.getId());

						contestWinExt.setState(0);
						contestWinExt.setCreatedAt(new Date());
						contestWinExt.setUpdatedAt(new Date());
						contestWinExt.setDelFlag(0);
						contestWinService.insert(contestWinExt);
						contestProductionService.update(contestProductionExt);
					}
					awardInfo.setContestPlayerInfoList(playerList);

					startIndex = startIndex + playerCount;//下一次查询，从前一次的playerCount开始
				}


			}


			Integer reviewCount = 0;//已评审作品数量
			if(contestExt.getReviewWay()!=null && contestExt.getReviewWay()==2){
				ContestReviewConfigExt contestReviewConfigInfo = contestExt.getContestReviewConfigInfo();
				if(contestReviewConfigInfo!=null && contestReviewConfigInfo.getId()>0 && contestReviewConfigInfo.getProductionReviewCount()!=null && contestReviewConfigInfo.getProductionReviewCount()>0){
					//查询作品列表
					List<ContestProductionExt> contestProductionList = contestProductionService.findListByContestId(contestExt.getId());
					for(ContestProductionExt contestProductionExt : contestProductionList) {
						Map<String, Object> m = new HashMap<>();
						m.put("delFlag", "0");
						m.put("contestId", contestExt.getId().toString());
						m.put("productionId", contestProductionExt.getId().toString());
						List<ContestProductionScoreExt> l = contestProductionScoreService.findListByCondition(m);
						if (l.size() >= contestReviewConfigInfo.getProductionReviewCount().intValue()) {
							reviewCount++;
						}
					}
				}
			}
			ContestStatExt contestStatExt = contestStatService.findByContestId(contestExt.getId());
			if(contestStatExt==null || contestStatExt.getId()==0){
				contestStatExt = new ContestStatExt();
				contestStatExt.setContestId(contestExt.getId());
				contestStatExt.setSignupCount(0);
				contestStatExt.setCommitCount(0);
				contestStatExt.setReviewCount(0);
				contestStatExt.setState(0);
				contestStatExt.setCreatedAt(new Date());
				contestStatExt.setUpdatedAt(new Date());
				contestStatExt.setDelFlag(0);
				contestStatService.insert(contestStatExt);
			}
			contestStatExt.setSignupCount(contestExt.getSignupCount());
			contestStatExt.setCommitCount(contestExt.getCommitCount());
			contestStatExt.setReviewCount(reviewCount);
			contestStatExt.setUpdatedAt(new Date());
			contestStatService.update(contestStatExt);
		}


		logger.debug("定时更新比赛数据【完成】");

		logger.debug("execute4ContestData----------->>>>end----------->>>>time:"+df.format(new Date()));
	}







}
