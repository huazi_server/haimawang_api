package cn.com.mys.project_kernel.base.service;



import cn.com.mys.project_kernel.base.entity.ext.WeixinNotifyExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface WeixinNotifyService {


    Integer insert(WeixinNotifyExt weixinNotifyExt);

    WeixinNotifyExt findObjectByCondition(Map<String, Object> mm);
}
