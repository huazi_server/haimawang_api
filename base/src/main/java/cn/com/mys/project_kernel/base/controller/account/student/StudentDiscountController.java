package cn.com.mys.project_kernel.base.controller.account.student;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.StudentDiscount;
import cn.com.mys.project_kernel.base.entity.ext.NotifyExt;
import cn.com.mys.project_kernel.base.entity.ext.StudentDiscountExt;
import cn.com.mys.project_kernel.base.entity.ext.StudentExt;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.ParamUtils;
import cn.com.mys.project_kernel.base.util.RequestUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author mingjun_T
 * 学生优惠券
 *
 */
@Controller
@RequestMapping(value = "/account/student/discount")
public class StudentDiscountController {

	Log log = LogFactory.getLog(StudentDiscountController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private DiscountService discountService;
	@Autowired
	private StudentDiscountService studentDiscountService;



	//查询列表
	@RequestMapping(value = "list")
	public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String category = (String) paramMap.get("category");//种类(0通用、1scratch)
		String courseType = (String) paramMap.get("courseType");//课程类型(1单课程、2系列课)
		String type = (String) paramMap.get("type");//类型(0抵用券、1折扣券)


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();

		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("category", category);
		map4Param.put("courseType", courseType);
		map4Param.put("type", type);
		map4Param.put("page", page);
		map4Param.put("studentId", studentId);


		Integer count = studentDiscountService.findCountByCondition(map4Param);
		List<StudentDiscountExt> list = studentDiscountService.findListByCondition(map4Param);

		for(StudentDiscountExt sd : list){
			Integer enableFlag = 1;//可用标识(1可用、2不可用)

			Integer _category = sd.getCategory();
			Integer _courseType = sd.getCourseType();
			Integer _type = sd.getType();

			if(StringUtils.isNotEmpty(category) && _category!=null && !category.equals(_category.toString())){
				enableFlag = 2;
			}
			if(StringUtils.isNotEmpty(courseType) && _courseType!=null && !courseType.equals(_courseType.toString())){
				enableFlag = 2;
			}
			if(StringUtils.isNotEmpty(type) && _type!=null && !type.equals(_type.toString())){
				enableFlag = 2;
			}

			sd.setEnableFlag(enableFlag);
		}



		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}









}
