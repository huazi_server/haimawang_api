package cn.com.mys.project_kernel.base.util;



import java.io.*;
import java.util.List;

/**
 * Created by admin on 2018/9/18.
 */
public class ListUtils {

    public static <T> List<T> deepCopy(List<T> src) {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(byteOut);
            out.writeObject(src);

            ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
            ObjectInputStream in = new ObjectInputStream(byteIn);
            @SuppressWarnings("unchecked")
            List<T> dest = (List<T>) in.readObject();
            return dest;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static <T> List<T> merge(List<T> list, List<T> intoList) {
        list.addAll(intoList);
        return list;
    }


    //获取无重复并集
    public static <T> List<T> union(List<T> list1, List<T> list2) {
        list2.removeAll(list1);
        list1.addAll(list2);
        return list1;
    }


    //获取差集
    public static <T> List<T> subtract(List<T> list1, List<T> list2) {
        list1.removeAll(list2);
        return list1;
    }








}
