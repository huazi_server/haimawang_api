package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.StudentCourseOrder;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseOrderExt;
import net.sf.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface StudentCourseOrderService {


    Integer findCountByCondition(Map<String, Object> map4Param);

    List<StudentCourseOrderExt> findListByCondition(Map<String, Object> map4Param);

    StudentCourseOrder findById(long id);

    Integer update(StudentCourseOrder studentCourseOrder);

    StudentCourseOrderExt findObjectByCondition(Map<String, Object> m);

    Integer insert(StudentCourseOrderExt order);

    StudentCourseOrderExt findByOrderNo(String orderNo);

    List<StudentCourseOrderExt> findAllListByCondition(Map<String, Object> map);

    Integer updateByCondition(Map<String, Object> map);

    void generateTeacherCourseAndStudentCourseData(String studentId, String coursePackageId, String courseId);

    JSONObject invokePay4Weixin(StudentCourseOrderExt order, String title, String detail, String loginToken, String payType, String appId, String appKey);

    List<StudentCourseOrderExt> findListByActivityTuanOrderId(Map<String, Object> mm);

    List<StudentCourseOrderExt> findListByCondition4PinTuanOrder(Map<String, Object> pinTuanMap);

    List<StudentCourseOrderExt> findListByCondition4ActivityTuanOrderId(Map<String, Object> map);

    Integer findCountByCondition4Channel(Map<String, Object> map4Param);

    List<StudentCourseOrderExt> findListByCondition4Channel(Map<String, Object> map4Param);
}
