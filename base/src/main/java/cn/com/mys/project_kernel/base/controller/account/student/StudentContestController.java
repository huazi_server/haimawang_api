package cn.com.mys.project_kernel.base.controller.account.student;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.ConfigGrade;
import cn.com.mys.project_kernel.base.entity.ContestGroup;
import cn.com.mys.project_kernel.base.entity.ContestSignupConfig;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 *
 * @author mingjun_T
 * 比赛
 *
 */
@Controller
@RequestMapping(value = "/account/student/contest")
public class StudentContestController {

	Log log = LogFactory.getLog(StudentContestController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;
	@Autowired
	private StudentService studentService;
    @Autowired
    private ConfigGradeService configGradeService;

    @Autowired
    private ContestService contestService;
    @Autowired
    private ContestGroupService contestGroupService;
    @Autowired
    private ContestAwardService contestAwardService;
    @Autowired
    private ContestUserService contestUserService;
    @Autowired
    private ContestPlayerService contestPlayerService;
    @Autowired
    private ContestProductionService contestProductionService;
    @Autowired
    private ContestProductionVoteService contestProductionVoteService;
    @Autowired
    private ContestProductionScoreService contestProductionScoreService;
    @Autowired
    private ContestSignupConfigService contestSignupConfigService;
    @Autowired
    private ContestReviewConfigService contestReviewConfigService;
    @Autowired
    private ContestReviewTeacherService contestReviewTeacherService;
    @Autowired
    private ContestWinService contestWinService;
    @Autowired
    private ContestTemplateService contestTemplateService;


	//保存
	@RequestMapping(value = "player/save")
	public ResponseEntity<ResponseJson> save4Player(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		String contestId = (String) paramMap.get("contestId");//比赛id
		String joinCode = (String) paramMap.get("joinCode");
		String name = (String) paramMap.get("name");
		String age = (String) paramMap.get("age");
		String school = (String) paramMap.get("school");
		String groupId = (String) paramMap.get("groupId");
		String gradeId = (String) paramMap.get("gradeId");

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
        if(contestExt!=null && contestExt.getAuditFlag()!=3){//审核标识(1待审核、2审核不通过、3审核通过)
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }
        if(contestExt!=null && contestExt.getState()!=2){//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"当前比赛不可报名",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("contestId", contestId);
		ContestSignupConfigExt contestSignupConfigExt = contestSignupConfigService.findObjectByCondition(map4Param);
		if(contestSignupConfigExt!=null && contestSignupConfigExt.getId()>0){
			Integer joinCodeInputFlag = contestSignupConfigExt.getJoinCodeInputFlag();//参赛码输入标识(1否、2是)
			Integer nameInputFlag = contestSignupConfigExt.getNameInputFlag();//姓名输入标识(1否、2是)
			Integer ageInputFlag = contestSignupConfigExt.getAgeInputFlag();//年龄输入标识(1否、2是)
			Integer schoolInputFlag = contestSignupConfigExt.getSchoolInputFlag();//学校输入标识(1否、2是)
			String groupIds = contestSignupConfigExt.getGroupIds();//组别id(多个","分隔)

			if(joinCodeInputFlag!=null && joinCodeInputFlag==2){
				if(StringUtils.isEmpty(joinCode)){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"参赛码不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
			if(nameInputFlag!=null && nameInputFlag==2){
				if(StringUtils.isEmpty(name)){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"姓名不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
			if(ageInputFlag!=null && ageInputFlag==2){
				if(StringUtils.isEmpty(age)){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"年龄不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
			if(schoolInputFlag!=null && schoolInputFlag==2){
				if(StringUtils.isEmpty(school)){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"学校不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
			if(StringUtils.isNotEmpty(groupIds)){
				if(StringUtils.isEmpty(groupId) || StringUtils.isEmpty(gradeId)){
					ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"组别、年级不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
					log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
					return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
				}
			}
		}


		Long _groupId = 0L;
        Long _gradeId = 0L;
		String _groupName = "";
		String _gradeName = "";
        if(StringUtils.isNotEmpty(groupId)){
            ContestGroup contestGroup = contestGroupService.findById(Long.parseLong(gradeId));
            if(contestGroup!=null && contestGroup.getId()>0){
                _groupId = contestGroup.getId();
                _groupName = contestGroup.getName();
            }
        }
        if(StringUtils.isNotEmpty(gradeId)){
            ConfigGrade configGrade = configGradeService.findById(Long.parseLong(gradeId));
            if(configGrade!=null && configGrade.getId()>0){
                _gradeId = configGrade.getId();
                _gradeName = configGrade.getName();
            }
        }

		map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("studentId", studentId);
		map4Param.put("contestId", contestId);
		ContestPlayerExt contestPlayerExt = contestPlayerService.findObjectByCondition(map4Param);
		if(contestPlayerExt!=null && contestPlayerExt.getId()>0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"您已经参赛",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		map4Param.put("studentId", "");
		map4Param.put("phone", studentExt.getPhone());
		contestPlayerExt = contestPlayerService.findObjectByCondition(map4Param);
		if(contestPlayerExt!=null && contestPlayerExt.getId()>0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"您已经参赛",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		map4Param.put("studentId", studentId);
		map4Param.put("phone", "");

		map4Param.put("state", "0");//状态(0未完成、1已完成)
		ContestPlayerExt playerExt = contestPlayerService.findObjectByCondition(map4Param);
		if(playerExt==null || playerExt.getId()==0){
			playerExt = new ContestPlayerExt();
			playerExt.setContestId(Long.parseLong(contestId));
			playerExt.setStudentId(Long.parseLong(studentId));
			playerExt.setPhone(studentExt.getPhone());
			playerExt.setAvatarUrl(studentExt.getAvatarUrl());
			playerExt.setName(name);
			playerExt.setAge(Integer.parseInt(age));
			playerExt.setSchool(school);
			playerExt.setGroupId(_groupId);
			playerExt.setGradeId(_gradeId);
            playerExt.setGroupName(_groupName);
            playerExt.setGradeName(_gradeName);

			playerExt.setCommitTime(new Date());
			playerExt.setState(1);//状态(0未完成、1已完成)
			playerExt.setCreatedAt(new Date());
			playerExt.setUpdatedAt(new Date());
			playerExt.setDelFlag(0);
			contestPlayerService.insert(playerExt);
		}else{
			playerExt.setContestId(Long.parseLong(contestId));
			playerExt.setStudentId(Long.parseLong(studentId));
			playerExt.setPhone(studentExt.getPhone());
			playerExt.setAvatarUrl(studentExt.getAvatarUrl());
			playerExt.setName(name);
			playerExt.setAge(Integer.parseInt(age));
			playerExt.setSchool(school);
            playerExt.setGroupId(_groupId);
            playerExt.setGradeId(_gradeId);
            playerExt.setGroupName(_groupName);
            playerExt.setGradeName(_gradeName);

			playerExt.setCommitTime(new Date());
			playerExt.setState(1);//状态(0未完成、1已完成)
			playerExt.setCreatedAt(new Date());
			playerExt.setUpdatedAt(new Date());
			playerExt.setDelFlag(0);
			contestPlayerService.update(playerExt);
		}


		//设置编号
		Integer number = 0;
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("delFlag", "0");
		m.put("state", "1");//状态(0未完成、1已完成)
		m.put("contestId", contestId);
		m.put("playerId", playerExt.getId());
		CompetitionPlayerExt lastPlayer = contestPlayerService.findLastObjectByCondition(m);
		Integer lastNumber = (lastPlayer!=null && lastPlayer.getNumber()!=null && lastPlayer.getNumber()>0)?lastPlayer.getNumber():0;
		number = lastNumber+1;
		playerExt.setNumber(number);
		contestPlayerService.update(playerExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),playerExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//获取比赛列表
	@RequestMapping(value = "list")
	public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
        String pageNumber = (String) paramMap.get("pageNumber");//页数
        String pageSize = (String) paramMap.get("pageSize");//查询条目数

        String type = (String) paramMap.get("type");//类型(1我参加的、2我创建的)

		//验证token
        String loginToken = (String) paramMap.get("loginToken");//token
        String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
        StudentExt studentExt = studentService.findByAccountId(accountId);
        String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

        Page page = new Page();
        if(StringUtils.isNotEmpty(pageNumber)){
            page.setPageNumber(Integer.parseInt(pageNumber));
        }
        if(StringUtils.isNotEmpty(pageSize)){
            page.setPageSize(Integer.parseInt(pageSize));
        }


        Map<String, Object> map4Param = new HashMap<String, Object>();
        map4Param.put("delFlag", "0");
        map4Param.put("studentId", studentId);
        map4Param.put("page", page);

        Integer count = 0;
        List<ContestExt> list = new ArrayList<>();
        if(StringUtils.isEmpty(type)){
            map4Param.put("auditFlag", "3");//审核标识(1待审核、2审核不通过、3审核通过)
            map4Param.put("state", "2,3,4,5,6");//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)
            count = contestService.findCountByCondition(map4Param);
            list = contestService.findListByCondition(map4Param);
        }
        if(StringUtils.isNotEmpty(type) && "1".equals(type)){
            map4Param.put("auditFlag", "3");//审核标识(1待审核、2审核不通过、3审核通过)
            map4Param.put("state", "2,3,4,5,6");//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)
            count = contestService.findCountByCondition4Join(map4Param);
            list = contestService.findListByCondition4Join(map4Param);
        }
        if(StringUtils.isNotEmpty(type) && "2".equals(type)){
            map4Param.put("auditFlag", "");//审核标识(1待审核、2审核不通过、3审核通过)
            map4Param.put("state", "");//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)
            count = contestService.findCountByCondition4Create(map4Param);
            list = contestService.findListByCondition4Create(map4Param);
        }

        for(ContestExt contestExt : list){
            Map<String, Object> m = new HashMap<String, Object>();
            m.put("delFlag", "0");
            m.put("state", "1");//状态(0未完成、1已完成)
            m.put("studentId", studentId);
            m.put("contestId", contestExt.getId().toString());
            ContestPlayerExt contestPlayerExt = contestPlayerService.findObjectByCondition(m);
            Integer joinFlag = 1;//指定比赛参加标识(1未参加、2已参加)
            if(contestPlayerExt!=null && contestPlayerExt.getId()>0){
                joinFlag = 2;
            }
            contestExt.setJoinFlag(joinFlag);
        }


        page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


    //获取比赛模板信息
    @RequestMapping(value = "template/info")
    public ResponseEntity<ResponseJson> info4Template(HttpServletRequest request, HttpServletResponse response) {

        //参数转换
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);

        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }

        log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

        //url地址后的参数处理

        //请求体内的参数
        String contestId = (String) paramMap.get("contestId");//比赛id

        //验证token
        String loginToken = (String) paramMap.get("loginToken");//token
        String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
        StudentExt studentExt = studentService.findByAccountId(accountId);
        String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

        /*if(StringUtils.isEmpty(accountId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }
        if(StringUtils.isEmpty(studentId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }*/

        if(StringUtils.isEmpty(contestId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
        if(contestExt==null || contestExt.getId()==0){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        Long templateId = contestExt.getTemplateId();
        if(templateId==null || templateId<=0){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛的模板不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        ContestTemplateExt contestTemplateExt = contestTemplateService.findById(templateId);

        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestTemplateExt);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
    }


    //获取比赛信息
    @RequestMapping(value = "info")
    public ResponseEntity<ResponseJson> info(HttpServletRequest request, HttpServletResponse response) {

        //参数转换
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);

        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }

        log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

        //url地址后的参数处理

        //请求体内的参数
        String contestId = (String) paramMap.get("contestId");//比赛id

        //验证token
        String loginToken = (String) paramMap.get("loginToken");//token
        String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
        StudentExt studentExt = studentService.findByAccountId(accountId);
        String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

        /*if(StringUtils.isEmpty(accountId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }
        if(StringUtils.isEmpty(studentId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }*/

        if(StringUtils.isEmpty(contestId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
        if(contestExt==null || contestExt.getId()==0){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        Map<String, Object> map4Param = new HashMap<String, Object>();
        map4Param.put("delFlag", "0");
        map4Param.put("state", "1");//状态(0未完成、1已完成)
        map4Param.put("studentId", studentId);
        map4Param.put("contestId", contestId);
        ContestPlayerExt contestPlayerExt = contestPlayerService.findObjectByCondition(map4Param);
        Integer joinFlag = 1;//指定比赛参加标识(1未参加、2已参加)
        if(contestPlayerExt!=null && contestPlayerExt.getId()>0){
            joinFlag = 2;
        }
        contestExt.setJoinFlag(joinFlag);

        Integer signupFlag = 1;//指定比赛报名标识(1不可报、2可报)
        Date currentDate = new Date();//当前时间
        Date signupStartTime = contestExt.getSignupStartTime();//报名开始时间
        Date signupStopTime = contestExt.getSignupStopTime();//报名结束时间
        Date commitStopTime = contestExt.getCommitStopTime();//提交结束时间
        if(signupStopTime==null){
            if(signupStartTime.getTime()<=currentDate.getTime() && currentDate.getTime()<commitStopTime.getTime()){
                signupFlag = 2;
            }
        }else{
            if(signupStartTime.getTime()<=currentDate.getTime() && currentDate.getTime()<signupStopTime.getTime()){
                signupFlag = 2;
            }
        }
        contestExt.setSignupFlag(signupFlag);


        //查询作品
        if(contestPlayerExt!=null && contestPlayerExt.getId()>0) {
            map4Param = new HashMap<String, Object>();
            map4Param.put("delFlag", "0");
            //map4Param.put("state", "1");//状态(0未完成、1已完成)
            map4Param.put("playerId", contestPlayerExt.getId().toString());
            map4Param.put("contestId", contestId);
            ContestProductionExt contestProductionExt = contestProductionService.findObjectByCondition(map4Param);
            contestPlayerExt.setContestProductionInfo(contestProductionExt);
        }

        Map<String, Object> m = new HashMap<String, Object>();
        m.put("contestInfo", contestExt);
        m.put("contestPlayerInfo", contestPlayerExt);

        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),m);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
    }

    //保存比赛信息
    @RequestMapping(value = "save")
    public ResponseEntity<ResponseJson> save(HttpServletRequest request, HttpServletResponse response) {

        //参数转换
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);

        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }

        log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

        //url地址后的参数处理

        //请求体内的参数
        String name = (String) paramMap.get("name");
        String sponsor = (String) paramMap.get("sponsor");
        String theme = (String) paramMap.get("theme");
        String reviewWay = (String) paramMap.get("reviewWay");
        String contactName = (String) paramMap.get("contactName");
        String contactPhone = (String) paramMap.get("contactPhone");

        String joinCodeInputFlag = (String) paramMap.get("joinCodeInputFlag");//参赛码输入标识(1否、2是)


        //验证token
        String loginToken = (String) paramMap.get("loginToken");//token
        String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
        StudentExt studentExt = studentService.findByAccountId(accountId);
        String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

        if(StringUtils.isEmpty(accountId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }
        if(StringUtils.isEmpty(studentId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        if(StringUtils.isEmpty(name) || StringUtils.isEmpty(sponsor) || StringUtils.isEmpty(theme) || StringUtils.isEmpty(reviewWay)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"名称、主办者、主题、评审方式不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }
        if(StringUtils.isEmpty(contactName) || StringUtils.isEmpty(contactPhone)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"联系姓名、电话不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        if(StringUtils.isEmpty(joinCodeInputFlag)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"参赛码标识不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        ContestExt contestExt = new ContestExt();
        contestExt.setCreateType(1);//创建者类型(0默认、1学生)
        contestExt.setCreateId(Long.parseLong(studentId));
        contestExt.setName(name);
        contestExt.setSponsor(sponsor);
        contestExt.setTheme(theme);
        contestExt.setReviewWay(Integer.parseInt(reviewWay));
        contestExt.setContactName(contactName);
        contestExt.setContactPhone(contactPhone);
        contestExt.setAuditFlag(1);//审核标识(1待审核、2审核不通过、3审核通过)
        contestExt.setState(0);//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)
        contestExt.setCreatedAt(new Date());
        contestExt.setUpdatedAt(new Date());
        contestExt.setDelFlag(0);
        contestService.insert(contestExt);

        ContestSignupConfigExt contestSignupConfigExt = new ContestSignupConfigExt();
        contestSignupConfigExt.setContestId(contestExt.getId());
        contestSignupConfigExt.setJoinCodeInputFlag(Integer.parseInt(joinCodeInputFlag));
        contestSignupConfigExt.setNameInputFlag(1);
        contestSignupConfigExt.setAgeInputFlag(1);
        contestSignupConfigExt.setSchoolInputFlag(1);
        contestSignupConfigExt.setGroupIds("");

        contestSignupConfigExt.setState(0);
        contestSignupConfigExt.setCreatedAt(new Date());
        contestSignupConfigExt.setUpdatedAt(new Date());
        contestSignupConfigExt.setDelFlag(0);
        contestSignupConfigService.insert(contestSignupConfigExt);

        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestExt);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
    }


    //获取比赛的作品列表
    @RequestMapping(value = "production/list")
    public ResponseEntity<ResponseJson> list4Production(HttpServletRequest request, HttpServletResponse response) {

        //参数转换
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);

        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }

        log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

        //url地址后的参数处理

        //请求体内的参数
        String contestId = (String) paramMap.get("contestId");//比赛id
        String pageNumber = (String) paramMap.get("pageNumber");//页数
        String pageSize = (String) paramMap.get("pageSize");//查询条目数

        String type = (String) paramMap.get("type");

        //验证token
        String loginToken = (String) paramMap.get("loginToken");//token
        String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
        StudentExt studentExt = studentService.findByAccountId(accountId);
        String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

        /*if(StringUtils.isEmpty(accountId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }
        if(StringUtils.isEmpty(studentId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }*/

        if(StringUtils.isEmpty(contestId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        Page page = new Page();
        if(StringUtils.isNotEmpty(pageNumber)){
            page.setPageNumber(Integer.parseInt(pageNumber));
        }
        if(StringUtils.isNotEmpty(pageSize)){
            page.setPageSize(Integer.parseInt(pageSize));
        }

        ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
        if(contestExt==null || contestExt.getId()==0){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        type = StringUtils.isEmpty(type)?"1":type;//1时间排序、2投票排序、3评分排序

        Map<String, Object> map4Param = new HashMap<String, Object>();
        map4Param.put("delFlag", "0");
        map4Param.put("state", "1");//状态(0未完成、1已完成)
        map4Param.put("contestId", contestId);
        map4Param.put("page", page);

        map4Param.put("type", type);

        Integer count = contestPlayerService.findCountByCondition4Production(map4Param);
        List<ContestPlayerExt> list = contestPlayerService.findListByCondition4Production(map4Param);

        for(ContestPlayerExt contestPlayerExt : list){
            Map<String, Object> m = new HashMap<>();
            m.put("delFlag", "0");
            m.put("contestId", contestId);
            m.put("playerId", contestPlayerExt.getId().toString());
            ContestProductionExt contestProductionExt = contestProductionService.findObjectByCondition(m);
            contestPlayerExt.setContestProductionInfo(contestProductionExt);
        }

        page.setTotalSize(count);
        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
    }

	//获取比赛的作品详情
	@RequestMapping(value = "production/info")
	public ResponseEntity<ResponseJson> info4Production(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id
		String productionId = (String) paramMap.get("productionId");//作品id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		StudentExt studentExt = studentService.findByAccountId(accountId);
		String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		if(StringUtils.isEmpty(contestId) || StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		ContestProductionExt contestProductionExt = contestProductionService.findById(Long.parseLong(productionId));

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestProductionExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//给作品投票
	@RequestMapping(value = "production/vote")
	public ResponseEntity<ResponseJson> vote4Production(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id
		String productionId = (String) paramMap.get("productionId");//作品id

		//验证token
        String loginToken = (String) paramMap.get("loginToken");//token
        String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
        StudentExt studentExt = studentService.findByAccountId(accountId);
        String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId) || StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

        ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
        if(contestExt==null || contestExt.getId()==0){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }
        ContestProductionExt contestProductionExt = contestProductionService.findById(Long.parseLong(productionId));
        if(contestProductionExt==null || contestProductionExt.getId()==0){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        ContestUserExt contestUserExt = contestUserService.findByAccountId(accountId);//TODO

        Integer voteFlag = 1;//指定作品投票标识(1未投、2已投)
        if(StringUtils.isNotEmpty(contestId) && StringUtils.isNotEmpty(productionId)){
            Map<String, Object> map4Param = new HashMap<String, Object>();
            map4Param.put("delFlag", "0");
            map4Param.put("userId", contestUserExt.getId().toString());
            map4Param.put("contestId", contestId);
            map4Param.put("productionId", productionId);
            List<CompetitionProductionVoteExt> list = contestProductionVoteService.findListByCondition4Production(map4Param);//查询投票列表
            if(list!=null && list.size()>0){
                voteFlag = 2;
            }
        }
        contestUserExt.setVoteFlag(voteFlag);

        if(voteFlag==2){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"您已经投票",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        ContestProductionVoteExt contestProductionVoteExt = new ContestProductionVoteExt();
        contestProductionVoteExt.setContestId(Long.parseLong(contestId));
        contestProductionVoteExt.setProductionId(Long.parseLong(productionId));
        contestProductionVoteExt.setPlayerId(contestProductionExt.getPlayerId());
        contestProductionVoteExt.setUserId(contestUserExt.getId());

        contestProductionVoteExt.setState(0);
        contestProductionVoteExt.setCreatedAt(new Date());
        contestProductionVoteExt.setUpdatedAt(new Date());
        contestProductionVoteExt.setDelFlag(0);
        contestProductionVoteService.insert(contestProductionVoteExt);

        Integer vote = (contestProductionExt.getVote()==null||contestProductionExt.getVote()<=0)?0:contestProductionExt.getVote();
        contestProductionExt.setVote(vote+1);
        contestProductionService.update(contestProductionExt);



        //重新查询
        voteFlag = 1;//指定作品投票标识(1未投、2已投)
        if(StringUtils.isNotEmpty(contestId) && StringUtils.isNotEmpty(productionId)){
            Map<String, Object> map4Param = new HashMap<String, Object>();
            map4Param.put("delFlag", "0");
            map4Param.put("userId", contestUserExt.getId().toString());
            map4Param.put("contestId", contestId);
            map4Param.put("productionId", productionId);
            List<CompetitionProductionVoteExt> list = contestProductionVoteService.findListByCondition4Production(map4Param);//查询投票列表
            if(list!=null && list.size()>0){
                voteFlag = 2;
            }
        }
        contestUserExt.setVoteFlag(voteFlag);

        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestUserExt);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

    //保存作品
    @RequestMapping(value = "production/save")
    public ResponseEntity<ResponseJson> save4Production(HttpServletRequest request, HttpServletResponse response) {

        //参数转换
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);

        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }

        log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

        //url地址后的参数处理

        //请求体内的参数
        String contestId = (String) paramMap.get("contestId");//比赛id
        String name = (String) paramMap.get("name");
        String content = (String) paramMap.get("content");
        String coverUrl = (String) paramMap.get("coverUrl");
        String intro = (String) paramMap.get("intro");

        //验证token
        String loginToken = (String) paramMap.get("loginToken");//token
        String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
        StudentExt studentExt = studentService.findByAccountId(accountId);
        String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

        if(StringUtils.isEmpty(accountId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }
        if(StringUtils.isEmpty(studentId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        if(StringUtils.isEmpty(contestId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }
        if(StringUtils.isEmpty(name) || StringUtils.isEmpty(content) || StringUtils.isEmpty(coverUrl) || StringUtils.isEmpty(intro)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"名称、内容、封面、介绍不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
        if(contestExt==null || contestExt.getId()==0){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        Map<String, Object> map4Param = new HashMap<String, Object>();
        map4Param.put("delFlag", "0");
        map4Param.put("state", "1");//状态(0未完成、1已完成)
        map4Param.put("studentId", studentId);
        map4Param.put("contestId", contestId);
        ContestPlayerExt contestPlayerExt = contestPlayerService.findObjectByCondition(map4Param);

        ContestProductionExt contestProductionExt = new ContestProductionExt();
        contestProductionExt.setContestId(contestExt.getId());
        contestProductionExt.setPlayerId(contestPlayerExt.getId());
        contestProductionExt.setName(name);
        contestProductionExt.setContent(content);
        contestProductionExt.setCoverUrl(coverUrl);
        contestProductionExt.setIntro(intro);

        contestProductionExt.setVote(0);
        contestProductionExt.setScore(0);

        contestProductionExt.setCommitTime(new Date());

        contestProductionExt.setState(1);//状态(0未完成、1已完成)
        contestProductionExt.setCreatedAt(new Date());
        contestProductionExt.setUpdatedAt(new Date());
        contestProductionExt.setDelFlag(0);
        contestProductionService.insert(contestProductionExt);


        //设置编号
        Integer number = 0;
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("delFlag", "0");
        m.put("state", "1");//状态(0未完成、1已完成)
        m.put("contestId", contestId);
        m.put("productionId", contestProductionExt.getId());
        ContestProductionExt lastProduction = contestProductionService.findLastObjectByCondition(m);
        Integer lastNumber = (lastProduction!=null && lastProduction.getNumber()!=null && lastProduction.getNumber()>0)?lastProduction.getNumber():0;
        number = lastNumber+1;
        contestProductionExt.setNumber(number);
        contestProductionService.update(contestProductionExt);


        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestProductionExt);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
    }

	//获取比赛的优胜者列表
	@RequestMapping(value = "winner/list")
	public ResponseEntity<ResponseJson> list4Winner(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		//验证token
        String loginToken = (String) paramMap.get("loginToken");//token
        String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
        StudentExt studentExt = studentService.findByAccountId(accountId);
        String studentId = studentExt!=null?String.valueOf(studentExt.getId()):"";

		/*if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(studentId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}*/

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

        ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
        if(contestExt==null || contestExt.getId()==0){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        //评审方式(1投票、2评审)
        Integer reviewWay = contestExt.getReviewWay();

		List<ContestAwardExt> list = contestExt.getContestAwardInfoList();
		Integer startIndex = 0;
		for(ContestAwardExt awardInfo : list){
			Integer playerCount = (awardInfo.getPlayerCount()!=null&&awardInfo.getPlayerCount()>0)?awardInfo.getPlayerCount():0;
			if(playerCount==null || playerCount==0){
				continue;
			}
			page.setStartIndex(startIndex);
			page.setPageSize(playerCount);

            Map<String, Object> map4Param = new HashMap<String, Object>();
			map4Param.put("delFlag", "0");
			map4Param.put("state", "1");//状态(0未完成、1已完成)
			map4Param.put("contestId", contestId);
			map4Param.put("page", page);
			Integer count = 0;
			List<ContestPlayerExt> playerList = new ArrayList<>();
			if(reviewWay!=null && reviewWay==1){
                count = contestPlayerService.findCountByCondition4Vote(map4Param);
                playerList = contestPlayerService.findListByCondition4Vote(map4Param);
            }
            if(reviewWay!=null && reviewWay==2){
                count = contestPlayerService.findCountByCondition4Score(map4Param);
                playerList = contestPlayerService.findListByCondition4Score(map4Param);
            }

			for(int i=0;i<playerList.size();i++){
                ContestPlayerExt contestPlayerExt = playerList.get(i);
				Map<String, Object> m = new HashMap<>();
				m.put("delFlag", "0");
				m.put("contestId", contestId);
				m.put("playerId", contestPlayerExt.getId().toString());
                ContestProductionExt contestProductionExt = contestProductionService.findObjectByCondition(m);
				if(contestProductionExt!=null && contestProductionExt.getId()>0){
                    contestProductionExt.setAwardId(awardInfo.getId());//处理优胜者的奖励项
				}
                contestPlayerExt.setContestProductionInfo(contestProductionExt);
			}
            awardInfo.setContestPlayerInfoList(playerList);

			startIndex = startIndex + playerCount;//下一次查询，从前一次的playerCount开始
		}

		page.setTotalSize(list.size());
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}




}
