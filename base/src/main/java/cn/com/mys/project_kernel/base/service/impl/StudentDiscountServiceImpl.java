package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.DiscountMapper;
import cn.com.mys.project_kernel.base.dao.StudentDiscountMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ChannelCourseMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.DiscountMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.StudentDiscountMapperExt;
import cn.com.mys.project_kernel.base.entity.Discount;
import cn.com.mys.project_kernel.base.entity.StudentDiscount;
import cn.com.mys.project_kernel.base.entity.ext.ChannelCourseExt;
import cn.com.mys.project_kernel.base.entity.ext.DiscountExt;
import cn.com.mys.project_kernel.base.entity.ext.StudentDiscountExt;
import cn.com.mys.project_kernel.base.service.StudentDiscountService;
import cn.com.mys.project_kernel.base.util.DateUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class StudentDiscountServiceImpl implements StudentDiscountService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public StudentDiscountMapper studentDiscountMapper;

	@Autowired
	public StudentDiscountMapperExt studentDiscountMapperExt;

	@Autowired
	public ChannelCourseMapperExt channelCourseMapperExt;

	@Autowired
	public DiscountMapper discountMapper;
	@Autowired
	public DiscountMapperExt discountMapperExt;




	@Override
	public Integer insert(String channelId, String courseType, String studentId, String courseId, String courseGroupId, String orderId) {
		if(StringUtils.isNotEmpty(courseType) && "1".equals(courseType)){
			courseGroupId = "";
		}
		if(StringUtils.isNotEmpty(courseType) && "2".equals(courseType)){
			courseId = "";
		}
		//根据渠道课程的优惠券的关系生成学生优惠券信息
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("channelId", channelId);
		m.put("courseType", courseType);
		m.put("courseId", courseId);
		m.put("courseGroupId", courseGroupId);
		List<ChannelCourseExt> channelCourseList = channelCourseMapperExt.findListByCondition(m);
		for(ChannelCourseExt cc : channelCourseList){
			String discountIds = cc.getDiscountIds();
			if(StringUtils.isNotEmpty(discountIds)){
				List<DiscountExt> list = discountMapperExt.findByIds(discountIds);
				for(DiscountExt d : list){
					Map<String, Object> mm = new HashMap<>();
					mm.put("delFlag", "0");
					mm.put("studentId", studentId);
					mm.put("discountId", d.getId().toString());
					StudentDiscountExt studentDiscountExt = studentDiscountMapperExt.findObjectByCondition(mm);
					if(studentDiscountExt==null || studentDiscountExt.getId()==0){
						studentDiscountExt = new StudentDiscountExt();
						studentDiscountExt.setStudentId(Long.parseLong(studentId));
						studentDiscountExt.setDiscountId(d.getId());
						studentDiscountExt.setOrderId(StringUtils.isEmpty(orderId)?0L:Long.parseLong(orderId));
						studentDiscountExt.setValidityStart(new Date());
						studentDiscountExt.setValidityEnd(DateUtils.addDays(new Date(), d.getValidityDays()));
						studentDiscountExt.setState(0);
						studentDiscountExt.setCreatedAt(new Date());
						studentDiscountExt.setUpdatedAt(new Date());
						studentDiscountExt.setDelFlag(0);

						studentDiscountMapper.insertSelective(studentDiscountExt);
					}
					//修改优惠券状态
					d.setState(1);//状态(0未发放过、1已发放过)
					discountMapper.updateByPrimaryKeySelective(d);
				}
			}
		}

		return 0;
	}




	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return studentDiscountMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<StudentDiscountExt> findListByCondition(Map<String, Object> map4Param) {
		return studentDiscountMapperExt.findListByCondition(map4Param);
	}

	@Override
	public StudentDiscountExt findObjectByCondition(Map<String, Object> mm) {
		return studentDiscountMapperExt.findObjectByCondition(mm);
	}

	@Override
	public Integer delete4Map(Map<String, Object> mm) {
		return studentDiscountMapperExt.delete4Map(mm);
	}

	@Override
	public Integer update4Map(Map<String, Object> mm) {
		return studentDiscountMapperExt.update4Map(mm);
	}
}


