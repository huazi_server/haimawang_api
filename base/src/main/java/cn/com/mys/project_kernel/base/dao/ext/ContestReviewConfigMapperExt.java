package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ContestReviewConfigExt;
import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public interface ContestReviewConfigMapperExt {

    ContestReviewConfigExt findObjectByCondition(Map<String, Object> map);
}