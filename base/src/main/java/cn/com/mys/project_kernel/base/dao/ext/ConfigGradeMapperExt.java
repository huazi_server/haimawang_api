package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.ConfigGradeExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface ConfigGradeMapperExt {

    ConfigGradeExt findObjectByCondition(Map<String, Object> map4Param);

    List<ConfigGradeMapperExt> findListByIds(String gradeIds);
}