package cn.com.mys.project_kernel.base.controller;


import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.ParamUtils;
import cn.com.mys.project_kernel.base.util.PropertiesUtils;
import cn.com.mys.project_kernel.base.util.RequestUtils;
import cn.com.mys.project_kernel.base.util.StringUtils;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author mingjun_T
 * 异常统一处理
 *
 */
@Controller
@RequestMapping(value = "/error")
public class ErrorController {


	//400
	@RequestMapping(value = "error_400")
	public ResponseEntity<ResponseJson> error_400(HttpServletRequest request, HttpServletResponse response) {
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"400",ResponseJson.FAIL,"错误的请求",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//403
	@RequestMapping(value = "error_403")
	public ResponseEntity<ResponseJson> error_403(HttpServletRequest request, HttpServletResponse response) {
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"403",ResponseJson.FAIL,"无效的访问",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//404
	@RequestMapping(value = "error_404")
	public ResponseEntity<ResponseJson> error_404(HttpServletRequest request, HttpServletResponse response) {
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"404",ResponseJson.FAIL,"目标不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//405
	@RequestMapping(value = "error_405")
	public ResponseEntity<ResponseJson> error_405(HttpServletRequest request, HttpServletResponse response) {
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"405",ResponseJson.FAIL,"非法访问",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//500
	@RequestMapping(value = "error_500")
	public ResponseEntity<ResponseJson> error_500(HttpServletRequest request, HttpServletResponse response) {
		String errorMsg = (String) request.getAttribute("errorMsg");//自定义拦截器中获取的错误信息
		//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"500",ResponseJson.FAIL,"服务器内部错误",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
		//TODO
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"500",ResponseJson.FAIL,"服务器内部错误:"+errorMsg,ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}




}
