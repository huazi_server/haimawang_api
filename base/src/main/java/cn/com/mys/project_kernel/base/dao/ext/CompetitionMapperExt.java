package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CompetitionExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CompetitionMapperExt {

    CompetitionExt findById(long id);

    List<CompetitionExt> findAllListByCondition(Map<String, Object> map);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<CompetitionExt> findListByCondition(Map<String, Object> map4Param);
}