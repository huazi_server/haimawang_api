package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.DiscountMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.DiscountMapperExt;
import cn.com.mys.project_kernel.base.entity.Discount;
import cn.com.mys.project_kernel.base.entity.ext.DiscountExt;
import cn.com.mys.project_kernel.base.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class DiscountServiceImpl implements DiscountService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public DiscountMapper discountMapper;

	@Autowired
	public DiscountMapperExt discountMapperExt;


	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return discountMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<DiscountExt> findListByCondition(Map<String, Object> map4Param) {
		return discountMapperExt.findListByCondition(map4Param);
	}

	@Override
	public Integer insert(Discount discount) {
		return discountMapper.insertSelective(discount);
	}

	@Override
	public Discount findById(long id) {
		return discountMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer update(Discount d) {
		return discountMapper.updateByPrimaryKeySelective(d);
	}

	@Override
	public Integer update4Map(Map<String, Object> map4Param) {
		return discountMapperExt.update4Map(map4Param);
	}
}
