package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ActivityTuanOrder;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class ActivityTuanOrderExt extends ActivityTuanOrder {

    private List<Map<String, Object>> studentInfoList;//学生列表
    public List<Map<String, Object>> getStudentInfoList() {
        return studentInfoList;
    }
    public void setStudentInfoList(List<Map<String, Object>> studentInfoList) {
        this.studentInfoList = studentInfoList;
    }

    private Integer restrictionCount;//成团限制人数
    public Integer getRestrictionCount() {
        return restrictionCount;
    }
    public void setRestrictionCount(Integer restrictionCount) {
        this.restrictionCount = restrictionCount;
    }


    private long remnantTime;//剩余时间(秒)
    public long getRemnantTime() {
        return remnantTime;
    }
    public void setRemnantTime(long remnantTime) {
        this.remnantTime = remnantTime;
    }
}