package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.StudentDiscount;
import cn.com.mys.project_kernel.base.util.PropertiesUtils;

import java.math.BigDecimal;

public class StudentDiscountExt extends StudentDiscount {


    private String name;//名称
    private Integer category;//种类(1scratch)
    private Integer courseType;//课程类型(1单课程、2系列课)
    private Integer type;//类型(0抵用券、1折扣券)
    private BigDecimal money;//满减金额(小数位保留2位)
    private BigDecimal amount;//优惠额度-优惠金额或折减百分率(小数位保留2位)

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getCourseType() {
        return courseType;
    }

    public void setCourseType(Integer courseType) {
        this.courseType = courseType;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    private Integer enableFlag;//可用标识(1可用、2不可用)

    public Integer getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(Integer enableFlag) {
        this.enableFlag = enableFlag;
    }
}