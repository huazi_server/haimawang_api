package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ResourceConfigMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ResourceConfigMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ResourceConfigExt;
import cn.com.mys.project_kernel.base.service.ResourceConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ResourceConfigServiceImpl implements ResourceConfigService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ResourceConfigMapper resourceConfigMapper;

	@Autowired
	public ResourceConfigMapperExt resourceConfigMapperExt;


	@Override
	public List<ResourceConfigExt> findListByCondition(Map<String, Object> map4Param) {
		return resourceConfigMapperExt.findListByCondition(map4Param);
	}
}
