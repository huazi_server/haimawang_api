package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.AdminExt;
import org.springframework.stereotype.Repository;



@Repository
public interface AdminMapperExt {

    AdminExt findByAccountId(String accountId);
}