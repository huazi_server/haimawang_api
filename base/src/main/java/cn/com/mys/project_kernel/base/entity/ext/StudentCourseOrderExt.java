package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.Course;
import cn.com.mys.project_kernel.base.entity.StudentCourseOrder;

public class StudentCourseOrderExt extends StudentCourseOrder {

    private CourseExt courseInfo;//课程
    public CourseExt getCourseInfo() {
        return courseInfo;
    }
    public void setCourseInfo(CourseExt courseInfo) {
        this.courseInfo = courseInfo;
    }

    private CourseGroupExt courseGroupInfo;//课程分组
    public CourseGroupExt getCourseGroupInfo() {
        return courseGroupInfo;
    }
    public void setCourseGroupInfo(CourseGroupExt courseGroupInfo) {
        this.courseGroupInfo = courseGroupInfo;
    }


    private ActivityTuanOrderExt activityTuanOrderInfo;//拼团订单
    public ActivityTuanOrderExt getActivityTuanOrderInfo() {
        return activityTuanOrderInfo;
    }
    public void setActivityTuanOrderInfo(ActivityTuanOrderExt activityTuanOrderInfo) {
        this.activityTuanOrderInfo = activityTuanOrderInfo;
    }



    private Integer pinTuanOrderUpdateFlag;//个人订单支付完，拼团订单是否更新标识(1否、2是)
    public Integer getPinTuanOrderUpdateFlag() {
        return pinTuanOrderUpdateFlag;
    }
    public void setPinTuanOrderUpdateFlag(Integer pinTuanOrderUpdateFlag) {
        this.pinTuanOrderUpdateFlag = pinTuanOrderUpdateFlag;
    }
}