package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.CourseLesson;
import cn.com.mys.project_kernel.base.entity.ext.CourseLessonExt;
import cn.com.mys.project_kernel.base.entity.ext.StudentCourseHomeworkExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface CourseLessonService {

    CourseLessonExt findObjectByCondition(Map<String, Object> map4Param);

    Integer update(CourseLessonExt courseLessonExt);

    Integer findCountByCondition4CurrentDate(Map<String, Object> map4Param);
    List<CourseLessonExt> findListByCondition4CurrentDate(Map<String, Object> map4Param);

    List<CourseLessonExt> findListByCondition(Map<String, Object> m);
}
