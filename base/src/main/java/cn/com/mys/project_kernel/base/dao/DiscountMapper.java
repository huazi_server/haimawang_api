package cn.com.mys.project_kernel.base.dao;

import cn.com.mys.project_kernel.base.entity.Discount;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscountMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbggenerated
     */
    int insert(Discount record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbggenerated
     */
    int insertSelective(Discount record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbggenerated
     */
    Discount selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(Discount record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(Discount record);
}