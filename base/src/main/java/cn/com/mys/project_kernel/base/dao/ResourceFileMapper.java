package cn.com.mys.project_kernel.base.dao;

import cn.com.mys.project_kernel.base.entity.ResourceFile;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceFileMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_file
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_file
     *
     * @mbggenerated
     */
    int insert(ResourceFile record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_file
     *
     * @mbggenerated
     */
    int insertSelective(ResourceFile record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_file
     *
     * @mbggenerated
     */
    ResourceFile selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_file
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(ResourceFile record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_file
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(ResourceFile record);
}