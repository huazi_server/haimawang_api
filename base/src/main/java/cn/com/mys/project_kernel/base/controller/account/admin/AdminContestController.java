package cn.com.mys.project_kernel.base.controller.account.admin;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.controller.account.tool.VerifyLoginToken;
import cn.com.mys.project_kernel.base.entity.*;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.filter.wrapper.BodyReaderHttpServletRequestWrapper;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.AdminPage;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import com.alibaba.fastjson.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import sun.awt.geom.AreaOp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;


/**
 *
 * @author mingjun_T
 *
 *
 */
@Controller
@RequestMapping(value = "/account/admin/contest")
public class AdminContestController {

	Log log = LogFactory.getLog(AdminContestController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private AdminService adminService;

	@Autowired
	private ProductionService productionService;

	@Autowired
	private ConfigGradeService configGradeService;

	@Autowired
	private ContestService contestService;
	@Autowired
	private ContestGroupService contestGroupService;
	@Autowired
	private ContestAwardService contestAwardService;
	@Autowired
	private ContestUserService contestUserService;
	@Autowired
	private ContestPlayerService contestPlayerService;
	@Autowired
	private ContestProductionService contestProductionService;
	@Autowired
	private ContestProductionVoteService contestProductionVoteService;
	@Autowired
	private ContestProductionScoreService contestProductionScoreService;
	@Autowired
	private ContestSignupConfigService contestSignupConfigService;
	@Autowired
	private ContestReviewConfigService contestReviewConfigService;
	@Autowired
	private ContestReviewTeacherService contestReviewTeacherService;
	@Autowired
	private ContestWinService contestWinService;
	@Autowired
	private ContestTemplateService contestTemplateService;


	//查询比赛列表
	@RequestMapping(value = "/list")
	public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		String auditFlag = (String) paramMap.get("auditFlag");//审核标识(1待审核、2审核不通过、3审核通过)
		String state = (String) paramMap.get("state");//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("page", page);
		map4Param.put("auditFlag", auditFlag);
		map4Param.put("state", state);
		Integer count = contestService.findCountByCondition(map4Param);
		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}
		List<ContestExt> list = contestService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//获取比赛信息
	@RequestMapping(value = "info")
	public ResponseEntity<ResponseJson> info(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//保存比赛信息
	@RequestMapping(value = "update")
	public ResponseEntity<ResponseJson> update(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id

		String name = (String) paramMap.get("name");
		String sponsor = (String) paramMap.get("sponsor");
		String contractor = (String) paramMap.get("contractor");
		String support = (String) paramMap.get("support");
		String theme = (String) paramMap.get("theme");
		String coverUrl = (String) paramMap.get("coverUrl");
		String templateId = (String) paramMap.get("templateId");

		String signupStartTime = (String) paramMap.get("signupStartTime");
		String signupStopTime = (String) paramMap.get("signupStopTime");
		String commitStopTime = (String) paramMap.get("commitStopTime");
		String publishStartTime = (String) paramMap.get("publishStartTime");
		String publishStopTime = (String) paramMap.get("publishStopTime");
		String reviewWay = (String) paramMap.get("reviewWay");
		String joinCodeInputFlag = (String) paramMap.get("joinCodeInputFlag");//参赛码输入标识(1否、2是)

		String productionCategory = (String) paramMap.get("productionCategory");
		String productionDemand = (String) paramMap.get("productionDemand");
		String awardIntro = (String) paramMap.get("awardIntro");

		String contactName = (String) paramMap.get("contactName");
		String contactPhone = (String) paramMap.get("contactPhone");


		String awardJson = (String) paramMap.get("awardJson");//奖励项json数组


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		contestExt.setName(name);
		contestExt.setSponsor(sponsor);
		contestExt.setContractor(contractor);
		contestExt.setSupport(support);
		contestExt.setTheme(theme);
		contestExt.setCoverUrl(coverUrl);
		if(StringUtils.isNotEmpty(templateId)){
			contestExt.setTemplateId(Long.parseLong(templateId));
		}

		if(StringUtils.isNotEmpty(signupStartTime)){
			contestExt.setSignupStartTime(DateUtils.string2Date(signupStartTime));
		}
		if(StringUtils.isNotEmpty(signupStopTime)){
			contestExt.setSignupStopTime(DateUtils.string2Date(signupStopTime));
		}
		if(StringUtils.isNotEmpty(commitStopTime)){
			contestExt.setCommitStopTime(DateUtils.string2Date(commitStopTime));
		}
		if(StringUtils.isNotEmpty(publishStartTime)){
			contestExt.setPublishStartTime(DateUtils.string2Date(publishStartTime));
		}
		if(StringUtils.isNotEmpty(publishStopTime)){
			contestExt.setPublishStopTime(DateUtils.string2Date(publishStopTime));
		}
		if(StringUtils.isNotEmpty(reviewWay)){
			contestExt.setReviewWay(Integer.parseInt(reviewWay));
		}

		if(StringUtils.isNotEmpty(joinCodeInputFlag)){
			ContestSignupConfigExt contestSignupConfigExt = contestExt.getContestSignupConfigInfo();
			if(contestSignupConfigExt==null || contestSignupConfigExt.getId()==0){
				contestSignupConfigExt = new ContestSignupConfigExt();
				contestSignupConfigExt.setContestId(contestExt.getId());

				contestSignupConfigExt.setNameInputFlag(1);
				contestSignupConfigExt.setAgeInputFlag(1);
				contestSignupConfigExt.setSchoolInputFlag(1);
				contestSignupConfigExt.setGroupIds("");

				contestSignupConfigExt.setState(0);
				contestSignupConfigExt.setCreatedAt(new Date());
				contestSignupConfigExt.setUpdatedAt(new Date());
				contestSignupConfigExt.setDelFlag(0);
				contestSignupConfigService.insert(contestSignupConfigExt);
			}
			contestSignupConfigExt.setJoinCodeInputFlag(Integer.parseInt(joinCodeInputFlag));

			contestSignupConfigExt.setUpdatedAt(new Date());
			contestSignupConfigService.update(contestSignupConfigExt);
		}

		contestExt.setProductionCategory(productionCategory);
		contestExt.setProductionDemand(productionDemand);
		contestExt.setAwardIntro(awardIntro);
		contestExt.setContactName(contactName);
		contestExt.setContactPhone(contactPhone);


		contestExt.setState(1);//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)
		contestExt.setUpdatedAt(new Date());
		contestService.update(contestExt);



		//设置奖励项信息
		if(StringUtils.isNotEmpty(awardJson)){
			JSONArray ja = JSONArray.parseArray(awardJson);
			List<ContestAwardExt> list = new ArrayList<>();
			for(int i=0;i<ja.size();i++){
				com.alibaba.fastjson.JSONObject jo = ja.getJSONObject(i);
				try {
					String _number = jo.get("number").toString();
					String _name = jo.get("name").toString();
					String _playerCount = jo.get("playerCount").toString();
					String _awardName = jo.get("awardName").toString();
					String _awardImgUrl = jo.get("awardImgUrl").toString();
					ContestAwardExt contestAwardExt = new ContestAwardExt();
					contestAwardExt.setContestId(contestExt.getId());
					contestAwardExt.setNumber(Integer.parseInt(_number));
					contestAwardExt.setName(_name);
					contestAwardExt.setPlayerCount(Integer.parseInt(_playerCount));
					contestAwardExt.setAwardName(_awardName);
					contestAwardExt.setAwardImgUrl(_awardImgUrl);

					contestAwardExt.setState(0);
					contestAwardExt.setCreatedAt(new Date());
					contestAwardExt.setUpdatedAt(new Date());
					contestAwardExt.setDelFlag(0);
					list.add(contestAwardExt);
				}catch (Exception e) {
					continue;
				}
			}

			if(list.size()>0) {
				//删除旧数据
				contestAwardService.deleteByContestId(contestExt.getId());
				for (ContestAwardExt contestAwardExt : list) {
					contestAwardService.insert(contestAwardExt);
				}
			}
		}



		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//修改审核标识
	@RequestMapping(value = "audit")
	public ResponseEntity<ResponseJson> audit(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id

		String auditFlag = (String) paramMap.get("auditFlag");


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId) || StringUtils.isEmpty(auditFlag)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、审核标识不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(Integer.parseInt(auditFlag)==3){
			contestExt.setJoinCode(StringUtils.getRandomString(6).toUpperCase());//设置随机字符串
		}

		contestExt.setAuditFlag(Integer.parseInt(auditFlag));//审核标识(1待审核、2审核不通过、3审核通过)
		contestExt.setUpdatedAt(new Date());
		contestService.update(contestExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//修改状态
	@RequestMapping(value = "state")
	public ResponseEntity<ResponseJson> state(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id

		String state = (String) paramMap.get("state");


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId) || StringUtils.isEmpty(state)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、状态不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(contestExt.getAuditFlag()!=3){//审核标识(1待审核、2审核不通过、3审核通过)
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"审核通过才能进行此操作",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestSignupConfigExt contestSignupConfigExt = contestExt.getContestSignupConfigInfo();
		if(contestSignupConfigExt==null || contestSignupConfigExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"设置报名配置后才能进行此操作",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		contestExt.setState(Integer.parseInt(state));//状态(0初始化、1未上线、2已上线、3申报中、4待公布、5公布中、6已结束)
		contestExt.setUpdatedAt(new Date());
		contestService.update(contestExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//上传文件
	@RequestMapping(value = "uploadFile",produces = "application/json;charset=utf-8")
	@ResponseBody
	public String uploadFile(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",402);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","loginToken不存在或过期");

			return jo.toString();
		}
		if(StringUtils.isEmpty(adminId)){
			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code",100);
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","用户不存在");
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("loginToken", loginToken);
		map4Param.put("folderPath", "/contest/file");//比赛相关的文件保存的路径

		map4Param.put("loginProductId", Constant.loginProductId);
		map4Param.put("loginProductKey", Constant.loginProductKey);

		try {
			String result = "";

			BodyReaderHttpServletRequestWrapper bodyReaderRequest = (BodyReaderHttpServletRequestWrapper) request;
			MultipartHttpServletRequest multipartRequest = bodyReaderRequest.getMultipartRequest();
			String fileParamName = "upload";
			List<MultipartFile> files = multipartRequest.getFiles(fileParamName);//获取文件

			MultipartFile file = files.get(0);

			//获取上传的文件的名称(包含后缀)
			String filename = file.getOriginalFilename();
			//获取名称(不含后缀)
			String fileName = filename.substring(0,filename.lastIndexOf("."));
			//获取后缀(扩展名)
			String contentType = filename.substring(filename.lastIndexOf(".")+1);
			map4Param.put("fileName", filename);
			map4Param.put("contentType", contentType);

			CommonsMultipartFile cf= (CommonsMultipartFile)file;
			DiskFileItem fi = (DiskFileItem)cf.getFileItem();

			//MultipartFile转换成File
			File f = fi.getStoreLocation();

			String uploadFileUrl = PropertiesUtils.getKeyValue("uploadFileUrl");

			result = FileUtils.post4Caller(uploadFileUrl,"upload",f,map4Param);

			//System.out.println(result);

			JSONObject jo = JSONObject.fromObject(result);

			//ResponseJson responseJsonResult = new ResponseJson(_result,_code,_message,_error,ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),_data);
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			return jo.toString();
		} catch (IOException e) {

			//ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"上传失败",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			//log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			//return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);

			JSONObject jo = new JSONObject();
			jo.put("result",ResponseJson.FALSE);
			jo.put("code","100");
			jo.put("message",ResponseJson.FAIL);
			jo.put("error","上传失败");

			return jo.toString();

		}
	}
	//获取比赛奖励列表
	@RequestMapping(value = "award/list")
	public ResponseEntity<ResponseJson> list4Award(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		List<ContestAwardExt> list = contestExt.getContestAwardInfoList();

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//获取比赛奖励信息
	@RequestMapping(value = "award/info")
	public ResponseEntity<ResponseJson> info4Award(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String awardId = (String) paramMap.get("awardId");//奖励项id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(awardId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"奖励项id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestAward contestAward = contestAwardService.findById(Long.parseLong(awardId));

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestAward);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//保存比赛奖励信息
	@RequestMapping(value = "award/save")
	public ResponseEntity<ResponseJson> save4Award(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");
		String number = (String) paramMap.get("number");
		String name = (String) paramMap.get("name");
		String playerCount = (String) paramMap.get("playerCount");
		String awardName = (String) paramMap.get("awardName");
		String awardImgUrl = (String) paramMap.get("awardImgUrl");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(number) || StringUtils.isEmpty(name) || StringUtils.isEmpty(playerCount)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"编号、名称、人数不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestAwardExt contestAwardExt = new ContestAwardExt();
		contestAwardExt.setContestId(Long.parseLong(contestId));
		contestAwardExt.setName(name);
		contestAwardExt.setNumber(Integer.parseInt(number));
		contestAwardExt.setPlayerCount(Integer.parseInt(playerCount));
		contestAwardExt.setAwardName(awardName);
		contestAwardExt.setAwardImgUrl(awardImgUrl);

		contestAwardExt.setState(0);
		contestAwardExt.setCreatedAt(new Date());
		contestAwardExt.setUpdatedAt(new Date());
		contestAwardExt.setDelFlag(0);
		contestAwardService.insert(contestAwardExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestAwardExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//更新比赛奖励信息
	@RequestMapping(value = "award/update")
	public ResponseEntity<ResponseJson> update4Award(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String awardId = (String) paramMap.get("awardId");//奖励项id

		String number = (String) paramMap.get("number");
		String name = (String) paramMap.get("name");
		String playerCount = (String) paramMap.get("playerCount");
		String awardName = (String) paramMap.get("awardName");
		String awardImgUrl = (String) paramMap.get("awardImgUrl");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(awardId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"奖励项id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestAward contestAward = contestAwardService.findById(Long.parseLong(awardId));
		contestAward.setName(name);
		contestAward.setNumber(Integer.parseInt(number));
		contestAward.setPlayerCount(Integer.parseInt(playerCount));
		contestAward.setAwardName(awardName);
		contestAward.setAwardImgUrl(awardImgUrl);
		contestAwardService.update(contestAward);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestAward);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	/************************************************************************************************/
	/************************************************************************************************/
	/************************************************************************************************/



	//获取比赛的报名配置
	@RequestMapping(value = "signup/config/info")
	public ResponseEntity<ResponseJson> info4SignupConfig(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestSignupConfigExt contestSignupConfigExt = contestExt.getContestSignupConfigInfo();

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestSignupConfigExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//保存比赛的报名配置
	@RequestMapping(value = "signup/config/save")
	public ResponseEntity<ResponseJson> save4SignupConfig(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id

		String joinCodeInputFlag = (String) paramMap.get("joinCodeInputFlag");//参赛码输入标识(1否、2是)
		String nameInputFlag = (String) paramMap.get("nameInputFlag");//姓名输入标识(1否、2是)
		String ageInputFlag = (String) paramMap.get("ageInputFlag");//年龄输入标识(1否、2是)
		String schoolInputFlag = (String) paramMap.get("schoolInputFlag");//学校输入标识(1否、2是)
		String groupIds = (String) paramMap.get("groupIds");//组别id(多个","分隔)

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(joinCodeInputFlag) || StringUtils.isEmpty(nameInputFlag) || StringUtils.isEmpty(ageInputFlag) || StringUtils.isEmpty(schoolInputFlag) || StringUtils.isEmpty(groupIds)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"输入标识或组别id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestSignupConfigExt contestSignupConfigExt = contestExt.getContestSignupConfigInfo();
		if(contestSignupConfigExt==null || contestSignupConfigExt.getId()==0){
			contestSignupConfigExt = new ContestSignupConfigExt();
			contestSignupConfigExt.setContestId(contestExt.getId());
			contestSignupConfigExt.setJoinCodeInputFlag(1);
			contestSignupConfigExt.setNameInputFlag(1);
			contestSignupConfigExt.setAgeInputFlag(1);
			contestSignupConfigExt.setSchoolInputFlag(1);
			contestSignupConfigExt.setGroupIds("");

			contestSignupConfigExt.setState(0);
			contestSignupConfigExt.setCreatedAt(new Date());
			contestSignupConfigExt.setUpdatedAt(new Date());
			contestSignupConfigExt.setDelFlag(0);
			contestSignupConfigService.insert(contestSignupConfigExt);
		}

		contestSignupConfigExt.setContestId(contestExt.getId());
		contestSignupConfigExt.setJoinCodeInputFlag(Integer.parseInt(joinCodeInputFlag));
		contestSignupConfigExt.setNameInputFlag(Integer.parseInt(nameInputFlag));
		contestSignupConfigExt.setAgeInputFlag(Integer.parseInt(ageInputFlag));
		contestSignupConfigExt.setSchoolInputFlag(Integer.parseInt(schoolInputFlag));
		contestSignupConfigExt.setGroupIds(groupIds);

		contestSignupConfigExt.setState(0);
		contestSignupConfigExt.setCreatedAt(new Date());
		contestSignupConfigExt.setUpdatedAt(new Date());
		contestSignupConfigExt.setDelFlag(0);
		contestSignupConfigService.update(contestSignupConfigExt);


		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestSignupConfigExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}

	//获取比赛的评审配置
	@RequestMapping(value = "review/config/info")
	public ResponseEntity<ResponseJson> info4ReviewConfig(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//保存比赛的评审配置
	@RequestMapping(value = "review/config/save")
	public ResponseEntity<ResponseJson> save4ReviewConfig(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id

		String reviewWay = (String) paramMap.get("reviewWay");//评审方式(1投票、2评审)

		String productionReviewCount = (String) paramMap.get("productionReviewCount");
		String reviewTeacherCount = (String) paramMap.get("reviewTeacherCount");
		String reviewTeacherIds = (String) paramMap.get("reviewTeacherIds");
		String reviewStopTime = (String) paramMap.get("reviewStopTime");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId) || StringUtils.isEmpty(reviewWay)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id、评审方式不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isNotEmpty(reviewWay) && "2".equals(reviewWay)){
			if(StringUtils.isEmpty(productionReviewCount) || StringUtils.isEmpty(reviewTeacherCount) || StringUtils.isEmpty(reviewTeacherIds) || StringUtils.isEmpty(reviewStopTime)){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品评审次数、评审老师数量、评审老师id、评审结束时间不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}

			String[] reviewTeacherIds_array = reviewTeacherIds.split(",");
			if(reviewTeacherIds_array.length!=Integer.parseInt(reviewTeacherCount)){
				ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"评审老师数量和评审老师id不匹配",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
				log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
				return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
			}
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		if(StringUtils.isNotEmpty(reviewWay) && "2".equals(reviewWay)){
			ContestReviewConfigExt contestReviewConfigExt = contestExt.getContestReviewConfigInfo();
			if(contestReviewConfigExt==null || contestReviewConfigExt.getId()==0){
				contestReviewConfigExt = new ContestReviewConfigExt();
				contestReviewConfigExt.setContestId(Long.parseLong(contestId));

				contestReviewConfigExt.setState(0);
				contestReviewConfigExt.setCreatedAt(new Date());
				contestReviewConfigExt.setUpdatedAt(new Date());
				contestReviewConfigExt.setDelFlag(0);
				contestReviewConfigService.insert(contestReviewConfigExt);
			}

			contestReviewConfigExt.setContestId(Long.parseLong(contestId));
			contestReviewConfigExt.setProductionReviewCount(Integer.parseInt(productionReviewCount));
			contestReviewConfigExt.setReviewTeacherCount(Integer.parseInt(reviewTeacherCount));
			contestReviewConfigExt.setReviewTeacherIds(reviewTeacherIds);
			contestReviewConfigExt.setReviewStopTime(DateUtils.string2Date(reviewStopTime));

			contestReviewConfigExt.setUpdatedAt(new Date());
			contestReviewConfigService.update(contestReviewConfigExt);

			contestExt.setContestReviewConfigInfo(contestReviewConfigExt);
		}

		contestExt.setReviewWay(Integer.parseInt(reviewWay));

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}



	/************************************************************************************************/
	/************************************************************************************************/
	/************************************************************************************************/



	//获取比赛评委老师列表
	@RequestMapping(value = "review/teacher/list")
	public ResponseEntity<ResponseJson> list4ReviewTeacher(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("page", page);

		Integer count = contestReviewTeacherService.findCountByCondition(map4Param);

		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}

		List<ContestReviewTeacherExt> list = contestReviewTeacherService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//获取比赛评委老师信息
	@RequestMapping(value = "review/teacher/info")
	public ResponseEntity<ResponseJson> info4ReviewTeacher(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String reviewTeacherId = (String) paramMap.get("reviewTeacherId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(reviewTeacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"评审老师id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestReviewTeacher contestReviewTeacher = contestReviewTeacherService.findById(Long.parseLong(reviewTeacherId));

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestReviewTeacher);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//保存比赛评委老师信息
	@RequestMapping(value = "review/teacher/save")
	public ResponseEntity<ResponseJson> save4ReviewTeacher(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String name = (String) paramMap.get("name");
		String phone = (String) paramMap.get("phone");
		String avatarUrl = (String) paramMap.get("avatarUrl");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(name) || StringUtils.isEmpty(phone)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"名称、手机号不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestReviewTeacher contestReviewTeacher = new ContestReviewTeacher();
		contestReviewTeacher.setName(name);
		contestReviewTeacher.setPhone(phone);
		contestReviewTeacher.setAvatarUrl(avatarUrl);

		contestReviewTeacher.setState(0);
		contestReviewTeacher.setCreatedAt(new Date());
		contestReviewTeacher.setUpdatedAt(new Date());
		contestReviewTeacher.setDelFlag(0);
		contestReviewTeacherService.insert(contestReviewTeacher);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestReviewTeacher);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//更新比赛评委老师信息
	@RequestMapping(value = "review/teacher/update")
	public ResponseEntity<ResponseJson> update4ReviewTeacher(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String reviewTeacherId = (String) paramMap.get("reviewTeacherId");

		String name = (String) paramMap.get("name");
		String phone = (String) paramMap.get("phone");
		String avatarUrl = (String) paramMap.get("avatarUrl");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(reviewTeacherId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"评审老师id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestReviewTeacher contestReviewTeacher = contestReviewTeacherService.findById(Long.parseLong(reviewTeacherId));
		contestReviewTeacher.setName(name);
		contestReviewTeacher.setPhone(phone);
		contestReviewTeacher.setAvatarUrl(avatarUrl);
		contestReviewTeacher.setUpdatedAt(new Date());
		contestReviewTeacherService.update(contestReviewTeacher);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestReviewTeacher);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
    //删除比赛评委老师信息
    @RequestMapping(value = "review/teacher/delete")
    public ResponseEntity<ResponseJson> delete4ReviewTeacher(HttpServletRequest request, HttpServletResponse response) {

        //参数转换
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);

        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }

        log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

        //url地址后的参数处理

        //请求体内的参数
        String reviewTeacherId = (String) paramMap.get("reviewTeacherId");

        //验证token
        String loginToken = (String) paramMap.get("loginToken");//token
        String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
        AdminExt adminExt = adminService.findByAccountId(accountId);
        String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

        if(StringUtils.isEmpty(accountId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }
        if(StringUtils.isEmpty(adminId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        if(StringUtils.isEmpty(reviewTeacherId)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"评审老师id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        ContestReviewTeacher contestReviewTeacher = contestReviewTeacherService.findById(Long.parseLong(reviewTeacherId));
        contestReviewTeacher.setDelFlag(1);
        contestReviewTeacher.setUpdatedAt(new Date());
        contestReviewTeacherService.update(contestReviewTeacher);

        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestReviewTeacher);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
    }



	//获取模板列表
	@RequestMapping(value = "template/list")
	public ResponseEntity<ResponseJson> list4Template(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}


		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0初始化、1正常)
		map4Param.put("page", page);

		Integer count = contestTemplateService.findCountByCondition(map4Param);

		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}

		List<ContestReviewTeacherExt> list = contestTemplateService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//获取模板信息
	@RequestMapping(value = "template/info")
	public ResponseEntity<ResponseJson> info4Template(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String templateId = (String) paramMap.get("templateId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(templateId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"模板id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestTemplateExt contestTemplateExt = contestTemplateService.findById(Long.parseLong(templateId));

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestTemplateExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//保存模板信息
	@RequestMapping(value = "template/save")
	public ResponseEntity<ResponseJson> save4Template(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String bgColor = (String) paramMap.get("bgColor");
		String bannerImg = (String) paramMap.get("bannerImg");
		String navbarBgColor = (String) paramMap.get("navbarBgColor");
		String navbarSelcolor = (String) paramMap.get("navbarSelcolor");
		String titleImg = (String) paramMap.get("titleImg");
		String bottomBgColor = (String) paramMap.get("bottomBgColor");
		String buttonColor = (String) paramMap.get("buttonColor");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(bgColor) || StringUtils.isEmpty(bannerImg)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"背景色、banner图不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(navbarBgColor) || StringUtils.isEmpty(navbarSelcolor)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"导航栏背景色、选中色不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(titleImg) || StringUtils.isEmpty(bottomBgColor) || StringUtils.isEmpty(buttonColor)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"title图、底部背景色、按钮颜色不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestTemplateExt contestTemplateExt = new ContestTemplateExt();
		contestTemplateExt.setBgColor(bgColor);
		contestTemplateExt.setBannerImg(bannerImg);
		contestTemplateExt.setNavbarBgColor(navbarBgColor);
		contestTemplateExt.setNavbarSelcolor(navbarSelcolor);
		contestTemplateExt.setTitleImg(titleImg);
		contestTemplateExt.setBottomBgColor(bottomBgColor);
		contestTemplateExt.setButtonColor(buttonColor);

		contestTemplateExt.setState(0);
		contestTemplateExt.setCreatedAt(new Date());
		contestTemplateExt.setUpdatedAt(new Date());
		contestTemplateExt.setDelFlag(0);
		contestTemplateService.insert(contestTemplateExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestTemplateExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//更新模板信息
	@RequestMapping(value = "template/update")
	public ResponseEntity<ResponseJson> update4Template(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String templateId = (String) paramMap.get("templateId");

		String bgColor = (String) paramMap.get("bgColor");
		String bannerImg = (String) paramMap.get("bannerImg");
		String navbarBgColor = (String) paramMap.get("navbarBgColor");
		String navbarSelcolor = (String) paramMap.get("navbarSelcolor");
		String titleImg = (String) paramMap.get("titleImg");
		String bottomBgColor = (String) paramMap.get("bottomBgColor");
		String buttonColor = (String) paramMap.get("buttonColor");
		String state = (String) paramMap.get("state");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(templateId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"模板id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestTemplateExt contestTemplateExt = contestTemplateService.findById(Long.parseLong(templateId));
		contestTemplateExt.setBgColor(bgColor);
		contestTemplateExt.setBannerImg(bannerImg);
		contestTemplateExt.setNavbarBgColor(navbarBgColor);
		contestTemplateExt.setNavbarSelcolor(navbarSelcolor);
		contestTemplateExt.setTitleImg(titleImg);
		contestTemplateExt.setBottomBgColor(bottomBgColor);
		contestTemplateExt.setButtonColor(buttonColor);

		if(StringUtils.isNotEmpty(state)){
			contestTemplateExt.setState(Integer.parseInt(state));
		}
		contestTemplateExt.setUpdatedAt(new Date());
		contestTemplateService.update(contestTemplateExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestTemplateExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//删除模板信息
	@RequestMapping(value = "template/delete")
	public ResponseEntity<ResponseJson> delete4Template(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String templateId = (String) paramMap.get("templateId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(templateId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"模板id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestTemplateExt contestTemplateExt = contestTemplateService.findById(Long.parseLong(templateId));
		contestTemplateExt.setDelFlag(1);
		contestTemplateExt.setUpdatedAt(new Date());
		contestTemplateService.update(contestTemplateExt);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestTemplateExt);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	//查询参赛者列表
	@RequestMapping(value = "player/list")
	public ResponseEntity<ResponseJson> list4Player(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);


		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";
		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}


		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("state", "1");//状态(0未完成、1已完成)
		map4Param.put("contestId", contestId);
		map4Param.put("page", page);
		Integer count = contestPlayerService.findCountByCondition(map4Param);
		try {
			if(Integer.parseInt(pageSize)==-1){
				page.setPageNumber(0);
				page.setPageSize(count);
				map4Param.put("page", page);
			}
		}catch (Exception e){

		}
		List<ContestPlayerExt> list = contestPlayerService.findListByCondition(map4Param);
		for(ContestPlayerExt contestPlayerExt : list){
			Map<String, Object> m = new HashMap<>();
			m.put("delFlag", "0");
			m.put("contestId", contestId);
			m.put("playerId", contestPlayerExt.getId().toString());
			ContestProductionExt contestProductionExt = contestProductionService.findObjectByCondition(m);
			if(contestProductionExt!=null && contestProductionExt.getId()>0){
				m.put("productionId", contestProductionExt.getId().toString());
				//查询评分
				List<ContestProductionScoreExt> contestProductionScoreList = contestProductionScoreService.findListByCondition(m);
				Integer score = 0;
				if(contestProductionScoreList.size()>0){
					for(ContestProductionScoreExt ps : contestProductionScoreList){
						if(ps.getScore()!=null && ps.getScore()>0){
							score += ps.getScore();
						}
					}
					score = score/contestProductionScoreList.size();
				}

				contestProductionExt.setScore(score);
				contestProductionExt.setContestProductionScoreInfoList(contestProductionScoreList);

				String reviewProgress = "";//评审进度
				Integer productionReviewCount = 0;
				if(contestExt.getReviewWay()!=null && contestExt.getReviewWay()==2 && contestExt.getContestReviewConfigInfo()!=null && contestExt.getContestReviewConfigInfo().getId()>0 && contestExt.getContestReviewConfigInfo().getProductionReviewCount()!=null && contestExt.getContestReviewConfigInfo().getProductionReviewCount()>0){
					productionReviewCount = contestExt.getContestReviewConfigInfo().getProductionReviewCount();
				}
				reviewProgress = contestProductionScoreList.size()+"/"+productionReviewCount;
				contestProductionExt.setReviewProgress(reviewProgress);
			}
			contestPlayerExt.setContestProductionInfo(contestProductionExt);
		}

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//获取比赛的优胜者列表
	@RequestMapping(value = "winner/list")
	public ResponseEntity<ResponseJson> list4Winner(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String contestId = (String) paramMap.get("contestId");//比赛id
		String pageNumber = (String) paramMap.get("pageNumber");//页数
		String pageSize = (String) paramMap.get("pageSize");//查询条目数

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(contestId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Page page = new Page();
		if(StringUtils.isNotEmpty(pageNumber)){
			page.setPageNumber(Integer.parseInt(pageNumber));
		}
		if(StringUtils.isNotEmpty(pageSize)){
			page.setPageSize(Integer.parseInt(pageSize));
		}

		ContestExt contestExt = contestService.findById(Long.parseLong(contestId));
		if(contestExt==null || contestExt.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"比赛不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		Map<String, Object> map4Param = new HashMap<String, Object>();
		map4Param.put("delFlag", "0");
		map4Param.put("contestId", contestId);
		map4Param.put("page", page);
		Integer count = contestWinService.findCountByCondition(map4Param);
		List<ContestWinExt> list = contestWinService.findListByCondition(map4Param);

		page.setTotalSize(count);
		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,AdminPage.getAdminPage(page));
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}
	//查询作品详情
	@RequestMapping(value = "production/info")
	public ResponseEntity<ResponseJson> info4Production(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String productionId = (String) paramMap.get("productionId");

		//验证token
		String loginToken = (String) paramMap.get("loginToken");//token
		String accountId = VerifyLoginToken.verifyLoginToken(loginToken);
		AdminExt adminExt = adminService.findByAccountId(accountId);
		String adminId = adminExt!=null?String.valueOf(adminExt.getId()):"";

		if(StringUtils.isEmpty(accountId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"402",ResponseJson.FAIL,"loginToken不存在或过期",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}
		if(StringUtils.isEmpty(adminId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"用户不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		if(StringUtils.isEmpty(productionId)){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品id不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ContestProduction contestProduction = contestProductionService.findById(Long.parseLong(productionId));
		if(contestProduction==null || contestProduction.getId()==0){
			ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"作品不存在",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
			log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
			return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
		}

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),contestProduction);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}








}
