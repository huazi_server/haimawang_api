package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ChannelActivityExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ChannelActivityService {

    ChannelActivityExt findObjectByCondition(Map<String, Object> m);

    Integer findCountByCondition(Map<String, Object> map4Param);

    List<ChannelActivityExt> findListByCondition(Map<String, Object> map4Param);

    Integer deleteByCondition(Map<String, Object> map4Param);

    Integer insert(ChannelActivityExt channelActivityExt);
}
