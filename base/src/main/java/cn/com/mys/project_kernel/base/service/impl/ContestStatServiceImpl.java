package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestStatMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.*;
import cn.com.mys.project_kernel.base.entity.ext.ContestStatExt;
import cn.com.mys.project_kernel.base.service.ContestStatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * Service的实现类
 *
 */

@Service
public class ContestStatServiceImpl implements ContestStatService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestStatMapper contestStatMapper;

	@Autowired
	public ContestStatMapperExt contestStatMapperExt;


	@Override
	public ContestStatExt findByContestId(Long contestId) {
		return contestStatMapperExt.findByContestId(contestId);
	}

	@Override
	public Integer insert(ContestStatExt contestStatExt) {
		return contestStatMapper.insertSelective(contestStatExt);
	}

	@Override
	public Integer update(ContestStatExt contestStatExt) {
		return contestStatMapper.updateByPrimaryKeySelective(contestStatExt);
	}
}
