package cn.com.mys.project_kernel.base.controller.config;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.util.Base64Utils;
import cn.com.mys.project_kernel.base.util.ParamUtils;
import cn.com.mys.project_kernel.base.util.PropertiesUtils;
import cn.com.mys.project_kernel.base.util.RequestUtils;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


/**
 * 
 * @author mingjun_T
 * base64
 *
 */
@Controller
@RequestMapping(value = "/config")
public class Base64Controller {

	Log log = LogFactory.getLog(Base64Controller.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@RequestMapping(value = "base64ToUrl")
	public ResponseEntity<ResponseJson> base64ToUrl(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String base64 = (String) paramMap.get("base64");

		String data = "";//TODO

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),data);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}


	@RequestMapping(value = "urlToBase64")
	public ResponseEntity<ResponseJson> urlToBase64(HttpServletRequest request, HttpServletResponse response) {

		//参数转换
		Map<String, Object> map = ParamUtils.requestParameter2Map(request);
		Map<String, Object> paramMap = RequestUtils.parseRequest(request);

		if(paramMap==null || paramMap.size()==0){
			paramMap = map;
		}

		log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

		//url地址后的参数处理

		//请求体内的参数
		String imgUrl = (String) paramMap.get("imgUrl");

		//String data = "data:image/png;base64,"+Base64Utils.ImageToBase64ByOnline(imgUrl);
		String data = Base64Utils.ImageToBase64ByOnline(imgUrl);

		System.out.println("data="+data);

		ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),data);
		log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
		return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
	}




}
