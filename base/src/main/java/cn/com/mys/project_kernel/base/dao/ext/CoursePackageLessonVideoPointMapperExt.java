package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.CoursePackageLessonVideoPointExt;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface CoursePackageLessonVideoPointMapperExt {

    List<CoursePackageLessonVideoPointExt> findListByCondition(Map<String, Object> map4Point);
}