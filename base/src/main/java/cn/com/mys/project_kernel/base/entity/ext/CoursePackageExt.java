package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.CoursePackage;

import java.util.List;

public class CoursePackageExt extends CoursePackage {

    private List<CourseExt> courseInfoList;//课程列表
    public List<CourseExt> getCourseInfoList() {
        return courseInfoList;
    }
    public void setCourseInfoList(List<CourseExt> courseInfoList) {
        this.courseInfoList = courseInfoList;
    }


    private CourseExt courseInfo;//课程
    public CourseExt getCourseInfo() {
        return courseInfo;
    }
    public void setCourseInfo(CourseExt courseInfo) {
        this.courseInfo = courseInfo;
    }


    private Integer buyFlag;//购买标识(1未购买、2已购买)
    public Integer getBuyFlag() {
        return buyFlag;
    }
    public void setBuyFlag(Integer buyFlag) {
        this.buyFlag = buyFlag;
    }


    private Integer courseCount4Process;//进行中的课程列表数量
    public Integer getCourseCount4Process() {
        return courseCount4Process;
    }
    public void setCourseCount4Process(Integer courseCount4Process) {
        this.courseCount4Process = courseCount4Process;
    }


    List<CoursePackageLessonExt> coursePackageLessonInfoList;//课程包的课时列表
    public List<CoursePackageLessonExt> getCoursePackageLessonInfoList() {
        return coursePackageLessonInfoList;
    }
    public void setCoursePackageLessonInfoList(List<CoursePackageLessonExt> coursePackageLessonInfoList) {
        this.coursePackageLessonInfoList = coursePackageLessonInfoList;
    }
}