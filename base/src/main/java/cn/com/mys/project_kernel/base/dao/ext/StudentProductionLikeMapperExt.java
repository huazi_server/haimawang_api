package cn.com.mys.project_kernel.base.dao.ext;

import cn.com.mys.project_kernel.base.entity.ext.StudentProductionLikeExt;
import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public interface StudentProductionLikeMapperExt {

    StudentProductionLikeExt findObjectByCondition(Map<String, Object> m);
}