package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ProductionTagMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ProductionTagMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ProductionTagExt;
import cn.com.mys.project_kernel.base.service.ProductionTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Service的实现类
 *
 */

@Service
public class ProductionTagServiceImpl implements ProductionTagService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ProductionTagMapper productionTagMapper;

	@Autowired
	public ProductionTagMapperExt productionTagMapperExt;


	@Override
	public List<ProductionTagExt> findListByCondition(Map<String, Object> map) {
		return productionTagMapperExt.findListByCondition(map);
	}
}
