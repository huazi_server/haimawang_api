package cn.com.mys.project_kernel.base.controller.userinfo;


import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.entity.UserInfoGather;
import cn.com.mys.project_kernel.base.entity.ext.UserInfoGatherExt;
import cn.com.mys.project_kernel.base.service.TbTestService;
import cn.com.mys.project_kernel.base.service.UserInfoGatherService;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.Page;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author mingjun_T
 * 用户信息收集
 *
 */
@Controller
@RequestMapping(value = "/userInfoGather")
public class UserInfoGatherController {

	Log log = LogFactory.getLog(UserInfoGatherController.class);

	//接口api的每页数量
	private static Integer apiPageSize = Constant.apiPageSize;


	@Autowired
	private TbTestService tbTestService;

	@Autowired
	private UserInfoGatherService userInfoGatherService;



    //列表
    @RequestMapping(value = "list")
    public ResponseEntity<ResponseJson> list(HttpServletRequest request, HttpServletResponse response) {

        //参数转换
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);

        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }

        log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

        //url地址后的参数处理

        //请求体内的参数

        String name = (String) paramMap.get("name");
        String phone = (String) paramMap.get("phone");
        String gender = (String) paramMap.get("gender");//性别(0未知、1男、2女)

        String pageNumber = (String) paramMap.get("pageNumber");//页数
        String pageSize = (String) paramMap.get("pageSize");//查询条目数

        Page page = new Page();

        if(StringUtils.isNotEmpty(pageNumber)){
            page.setPageNumber(Integer.parseInt(pageNumber));
        }
        if(StringUtils.isNotEmpty(pageSize)){
            page.setPageSize(Integer.parseInt(pageSize));
        }

        Map<String, Object> map4Param = new HashMap<String, Object>();
        map4Param.put("delFlag", "0");
        map4Param.put("name", name);
        map4Param.put("phone", phone);
        map4Param.put("gender", gender);
        map4Param.put("page", page);

        Integer count = userInfoGatherService.findCountByCondition(map4Param);
        List<UserInfoGatherExt> list = userInfoGatherService.findListByCondition(map4Param);

        page.setTotalSize(count);
        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),list,page);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
    }


    //保存
    @RequestMapping(value = "save")
    public ResponseEntity<ResponseJson> save(HttpServletRequest request, HttpServletResponse response) {

        //参数转换
        Map<String, Object> map = ParamUtils.requestParameter2Map(request);
        Map<String, Object> paramMap = RequestUtils.parseRequest(request);

        if(paramMap==null || paramMap.size()==0){
            paramMap = map;
        }

        log.info(RequestUtils.getLog4Prefix(request) + "params:"+paramMap);

        //url地址后的参数处理

        //请求体内的参数

        String name = (String) paramMap.get("name");
        String phone = (String) paramMap.get("phone");
        String age = (String) paramMap.get("age");
        String gender = (String) paramMap.get("gender");//性别(0未知、1男、2女)

        if(StringUtils.isEmpty(name) || StringUtils.isEmpty(phone) || StringUtils.isEmpty(age)){
            ResponseJson responseJsonResult = new ResponseJson(ResponseJson.FALSE,"100",ResponseJson.FAIL,"姓名、手机号、年龄不能为空",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request));
            log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
            return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
        }

        UserInfoGather userInfoGather = new UserInfoGather();
        userInfoGather.setName(name);
        userInfoGather.setAge(age);
        userInfoGather.setPhone(phone);
        userInfoGather.setGender(StringUtils.isNotEmpty(gender)?Integer.parseInt(gender):0);
        userInfoGather.setState(0);
        userInfoGather.setCreatedAt(new Date());
        userInfoGather.setUpdatedAt(new Date());
        userInfoGather.setDelFlag(0);
        userInfoGatherService.insert(userInfoGather);

        ResponseJson responseJsonResult = new ResponseJson(ResponseJson.TRUE,"200",ResponseJson.SUCCESS,"",ResponseJson.getExecuteTime(request),ResponseJson.getExtParam(request),userInfoGather);
        log.info(RequestUtils.getLog4Prefix(request) + "result:"+responseJsonResult);
        return new ResponseEntity<ResponseJson>(responseJsonResult, HttpStatus.OK);
    }






}
