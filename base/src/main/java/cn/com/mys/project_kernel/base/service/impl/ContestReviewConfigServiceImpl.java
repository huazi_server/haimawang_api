package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.ContestReviewConfigMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ContestReviewConfigMapperExt;
import cn.com.mys.project_kernel.base.entity.ext.ContestReviewConfigExt;
import cn.com.mys.project_kernel.base.service.ContestReviewConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service的实现类
 *
 */

@Service
public class ContestReviewConfigServiceImpl implements ContestReviewConfigService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ContestReviewConfigMapper contestReviewConfigMapper;

	@Autowired
	public ContestReviewConfigMapperExt contestReviewConfigMapperExt;

	@Override
	public Integer insert(ContestReviewConfigExt contestReviewConfigExt) {
		return contestReviewConfigMapper.insertSelective(contestReviewConfigExt);
	}

	@Override
	public Integer update(ContestReviewConfigExt contestReviewConfigExt) {
		return contestReviewConfigMapper.updateByPrimaryKeySelective(contestReviewConfigExt);
	}
}
