package cn.com.mys.project_kernel.base.util;

/*
 * 保存xml文件
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class XmlUtils {
	
	/*
	 * doc		xml文档对象
	 * xmlname	路径(名称)
	 * 
	 */
	//保存xml文件的方法
	public static void saveDoc(Document doc,String xmlname) throws IOException{
		
		//创建格式器,\t是缩进使用的字符为制表符--true是是否换行
		OutputFormat of=new OutputFormat("\t",true);
		//把文档中已经存在的空白处去掉
		of.setTrimText(true);
		
		//创建输出流
		OutputStream os=new FileOutputStream(xmlname);
		//创建字符输出流--把字节流转换为字符流并指定编码
		Writer wr=new OutputStreamWriter(os,"UTF-8");
		//使用输出流和格式化器来创建xml输出流
		XMLWriter xw=new XMLWriter(wr,of);
		//把Document对象写入xml文件中
		xw.write(doc);
		xw.close();
	}
	
	
	//获取xml文件中指定节点的值(指定位置)
	@SuppressWarnings("unchecked")
	public static String getXMLNodeValue(URL filePath, String nodePath, int index,InputStream is) throws IOException{
		/*BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line = "";
		String[] arrs = null;
		List<String> jsons = new ArrayList<String>();
		String returnjson = "[";
		int count = 0;
		while ((line = br.readLine()) != null) {
			if (count++ == 0)
				continue;
			arrs = line.split(" ");
			jsons.add("{name:'" + arrs[0] + "',start:'" + arrs[1] + "',end:'"
					+ arrs[2] + "'}");
			// System.out.println("{name:'" + arrs[0] + "',start:'" + arrs[1] +
			// "',end:'" + arrs[2] + "'}");
		}*/
		       //创建解析器对象
				SAXReader sr=new SAXReader();
				//获取Document对象
				Document doc;
				String eleText=null;
				try {
					doc = sr.read(filePath);
					//获取根元素
					Element root=doc.getRootElement();
					//获取指定的元素
					List<Element> elements=root.selectNodes(nodePath);//元素层次
					
					eleText = elements.get(index).getText();//获取内容
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}//filePath：xml路径	注意sr.read(filePath):路径不能含有中文,所有需要以文件方式来读取
				
				
				return eleText;

	}
	
	
    public static List<String> getXMLNodeValueList(String filePath, String nodePath) throws Exception {
		
		//创建解析器对象
		SAXReader sr=new SAXReader();
		//获取Document对象
		URL ppturl = new URL(filePath);
		Document doc=sr.read(ppturl);//filePath
		//获取根元素
		Element root=doc.getRootElement();
		//获取指定的元素
		List<Element> elements=root.selectNodes(nodePath);
		
		List<String> videoNodelist = new ArrayList<String>();
		
		for(Element e:elements){
			
			String node = e.getText();
			
			node = node.substring(0, node.lastIndexOf("."));
			
			videoNodelist.add(node);
		}
		
		//字符串排序(默认排序)
	    Collections.sort(videoNodelist);
	    //System.out.println(videoNodes.toString());
	    //字符串排序(从小到大)
	    Collections.sort(videoNodelist, new Comparator() {
	    	@Override
	    	public int compare(Object o1, Object o2) {
	    		return new Integer((String) o1).compareTo(new Integer((String) o2));
	    	}
	    });
	
		return videoNodelist;
		
	}

    
	
}
