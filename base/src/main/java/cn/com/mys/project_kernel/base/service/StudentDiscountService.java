package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.StudentDiscountExt;

import java.util.List;
import java.util.Map;

/**
 * Service的接口
 *
 */
public interface StudentDiscountService {


    Integer insert(String channelId, String courseType, String studentId, String courseId, String courseGroupId, String orderId);

    Integer findCountByCondition(Map<String, Object> map4Param);
    List<StudentDiscountExt> findListByCondition(Map<String, Object> map4Param);

    StudentDiscountExt findObjectByCondition(Map<String, Object> mm);

    Integer delete4Map(Map<String, Object> mm);

    Integer update4Map(Map<String, Object> mm);
}
