package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.StudentProductionCollectExt;

import java.util.Map;

/**
 * Service的接口
 *
 */
public interface StudentProductionCollectService {


    StudentProductionCollectExt findObjectByCondition(Map<String, Object> map4Param);

    Integer insert(StudentProductionCollectExt studentProductionCollectExt);

    Integer delete(StudentProductionCollectExt studentProductionCollectExt);
}
