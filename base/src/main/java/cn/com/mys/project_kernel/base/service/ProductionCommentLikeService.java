package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentLikeExt;

import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ProductionCommentLikeService {

    ProductionCommentLikeExt findObjectByCondition(Map<String, Object> map4Param);

    Integer insert(ProductionCommentLikeExt productionCommentLikeExt);

    Integer delete(ProductionCommentLikeExt productionCommentLikeExt);
}
