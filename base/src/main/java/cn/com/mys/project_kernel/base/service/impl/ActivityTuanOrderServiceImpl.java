package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.dao.*;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.ActivityTuanMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.ActivityTuanOrderMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.CourseMapperExt;
import cn.com.mys.project_kernel.base.dao.ext.RandomUserMapperExt;
import cn.com.mys.project_kernel.base.entity.*;
import cn.com.mys.project_kernel.base.entity.ext.*;
import cn.com.mys.project_kernel.base.service.*;
import cn.com.mys.project_kernel.base.util.*;
import cn.com.mys.project_kernel.base.vo.ResponseJson;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.beans.Transient;
import java.util.*;


/**
 * Service的实现类
 *
 */

@Service
public class ActivityTuanOrderServiceImpl implements ActivityTuanOrderService {

	Log logger = LogFactory.getLog(ActivityTuanOrderServiceImpl.class);


	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public ActivityTuanOrderMapper activityTuanOrderMapper;

	@Autowired
	public ActivityTuanOrderMapperExt activityTuanOrderMapperExt;

	@Autowired
	public CourseMapper courseMapper;
	@Autowired
	public CourseMapperExt courseMapperExt;

	@Autowired
	public StudentCourseOrderMapper studentCourseOrderMapper;

	@Autowired
	public ActivityTuanMapper activityTuanMapper;

	@Autowired
	public ActivityTuanMapperExt activityTuanMapperExt;

	@Autowired
	public RandomUserMapperExt randomUserMapperExt;

	@Autowired
	public StudentMapper studentMapper;



	//特殊注入
	@Autowired
	public CourseService courseService;
	@Autowired
	public CourseGroupService courseGroupService;
	@Autowired
	public StudentCourseService studentCourseService;
	@Autowired
	public StudentCourseOrderService studentCourseOrderService;




	//设置拼团订单信息
	private void setStudentInfoList(ActivityTuanOrderExt activityTuanOrderExt){
		if(activityTuanOrderExt!=null && activityTuanOrderExt.getId()>0){
			String studentIds = activityTuanOrderExt.getStudentIds();
			String studentNames = activityTuanOrderExt.getStudentNames();
			String studentAvatarUrls = activityTuanOrderExt.getStudentAvatarUrls();
			String studentOrderIds = activityTuanOrderExt.getStudentOrderIds();
			if(StringUtils.isNotEmpty(studentIds) && StringUtils.isNotEmpty(studentNames) && StringUtils.isNotEmpty(studentAvatarUrls) && StringUtils.isNotEmpty(studentOrderIds)) {
				String[] sIds = studentIds.split(",");
				String[] sNames = studentNames.split(",");
				String[] sAvatarUrls = studentAvatarUrls.split(",");
				String[] sOrderIds = studentOrderIds.split(",");
				List<Map<String, Object>> studentInfoList = new ArrayList<>();
				if(sIds.length==sNames.length && sNames.length==sAvatarUrls.length && sAvatarUrls.length==sOrderIds.length){
					for(int i=0;i<sIds.length;i++){
						Map<String, Object> m = new HashMap<>();
						m.put("studentId", sIds[i]);
						m.put("studentName", sNames[i]);
						m.put("studentAvatarUrl", sAvatarUrls[i]);
						m.put("studentOrderId", sOrderIds[i]);
						studentInfoList.add(m);
					}
				}
				activityTuanOrderExt.setStudentInfoList(studentInfoList);
			}
		}
	}

	//设置成团限制人数、剩余时间
	private void setRestrictionCountAndRemnantTime(ActivityTuanOrderExt activityTuanOrderExt){
		if(activityTuanOrderExt!=null && activityTuanOrderExt.getId()>0) {
			if(activityTuanOrderExt.getActivityTuanId()!=null && activityTuanOrderExt.getActivityTuanId()>0){
				ActivityTuan activityTuan = activityTuanMapper.selectByPrimaryKey(activityTuanOrderExt.getActivityTuanId());
				if(activityTuan!=null && activityTuan.getId()>0){
					activityTuanOrderExt.setRestrictionCount(activityTuan.getRestrictionCount());
				}
			}
			if(activityTuanOrderExt.getEndAt()!=null){
				if(activityTuanOrderExt.getEndAt().getTime()>=new Date().getTime()){
					activityTuanOrderExt.setRemnantTime(DateUtils.getInterval(activityTuanOrderExt.getEndAt(), new Date()));
				}else{
					activityTuanOrderExt.setRemnantTime(0L);
				}
			}
		}
	}


	@Override
	public ActivityTuanOrder findById(Long tuanOrderId) {
		return activityTuanOrderMapper.selectByPrimaryKey(tuanOrderId);
	}

	@Override
	public ActivityTuanOrderExt findObjectByCondition(Map<String, Object> map) {
		ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderMapperExt.findObjectByCondition(map);
		setStudentInfoList(activityTuanOrderExt);
		setRestrictionCountAndRemnantTime(activityTuanOrderExt);
		return activityTuanOrderExt;
	}


	@Override
	public Integer findCountByCondition(Map<String, Object> map4Param) {
		return activityTuanOrderMapperExt.findCountByCondition(map4Param);
	}

	@Override
	public List<ActivityTuanOrderExt> findListByCondition(Map<String, Object> map4Param) {
		List<ActivityTuanOrderExt> list = activityTuanOrderMapperExt.findListByCondition(map4Param);
		for(ActivityTuanOrderExt activityTuanOrderExt : list){
			setStudentInfoList(activityTuanOrderExt);
			setRestrictionCountAndRemnantTime(activityTuanOrderExt);
		}
		return list;
	}

	@Override
	public ActivityTuanOrderExt findObjectByActivityId(Map<String, Object> map) {
		ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderMapperExt.findObjectByActivityId(map);
		setStudentInfoList(activityTuanOrderExt);
		return activityTuanOrderExt;
	}

	@Override
	public ActivityTuanOrderExt findObjectByActivityId4Join(Map<String, Object> map) {
		ActivityTuanOrderExt activityTuanOrderExt = activityTuanOrderMapperExt.findObjectByActivityId4Join(map);
		setStudentInfoList(activityTuanOrderExt);
		setRestrictionCountAndRemnantTime(activityTuanOrderExt);
		return activityTuanOrderExt;
	}


	@Override
	public Integer insert(ActivityTuanOrderExt activityTuanOrderExt) {
		return activityTuanOrderMapper.insertSelective(activityTuanOrderExt);
	}

	@Override
	public Integer update(ActivityTuanOrderExt activityTuanOrderExt) {
		return activityTuanOrderMapper.updateByPrimaryKeySelective(activityTuanOrderExt);
	}



	//模拟随机用户
	@Override
	public void mockRandomUser(ActivityTuanOrderExt ato, Integer restrictionCount) {
		Integer rpCount = ato.getRealPeopleCount();
		String _studentIds = ato.getStudentIds();
		String _studentNames = ato.getStudentNames();
		String _studentAvatarUrls = ato.getStudentAvatarUrls();
		String _studentOrderIds = ato.getStudentOrderIds();
		//查询模拟用户
		Map<String, Object> m = new HashMap<>();
		m.put("delFlag", "0");
		m.put("queryCount", restrictionCount.intValue()-rpCount.intValue());
		List<RandomUserExt> randomUserList = randomUserMapperExt.findListByCondition(m);
		for(RandomUserExt ru : randomUserList){
			_studentIds = _studentIds + "," + "0";
			_studentNames = _studentNames + "," + ru.getName();
			_studentAvatarUrls = _studentAvatarUrls + "," + ru.getAvatarUrl();
			_studentOrderIds = _studentOrderIds + "," + "0";
		}
		_studentIds = _studentIds.startsWith(",")?_studentIds.substring(1,_studentIds.length()):_studentIds;
		_studentNames = _studentNames.startsWith(",")?_studentNames.substring(1,_studentNames.length()):_studentNames;
		_studentAvatarUrls = _studentAvatarUrls.startsWith(",")?_studentAvatarUrls.substring(1,_studentAvatarUrls.length()):_studentAvatarUrls;
		_studentOrderIds = _studentOrderIds.startsWith(",")?_studentOrderIds.substring(1,_studentOrderIds.length()):_studentOrderIds;
		ato.setStudentIds(_studentIds);
		ato.setStudentNames(_studentNames);
		ato.setStudentAvatarUrls(_studentAvatarUrls);
		ato.setStudentOrderIds(_studentOrderIds);
		ato.setPeopleCount(restrictionCount);
		ato.setRealPeopleCount(rpCount);
		activityTuanOrderMapper.updateByPrimaryKeySelective(ato);
	}


	//生成学生课程信息
	@Override
	public void generateStudentCourseData(StudentCourseOrderExt order){

		//个人订单
		String coursePackageIds = order.getCoursePackageIds();
		String courseIds = order.getCourseIds();

		String[] cpIds = coursePackageIds.split(",");
		String[] cIds = courseIds.split(",");
		for(int i=0;i<cIds.length;i++){
			String courseId = cIds[i];
			String coursePackageId = cpIds[i];
			Course c = courseService.findById(Long.parseLong(courseId));
			coursePackageId = (c.getCoursePackageId()!=null&&c.getCoursePackageId()>0)?c.getCoursePackageId().toString():coursePackageId;

			StudentCourseExt studentCourseExt = new StudentCourseExt();
			studentCourseExt.setStudentId(order.getStudentId());
			studentCourseExt.setCourseId(Long.parseLong(courseId));
			studentCourseExt.setCoursePackageId(Long.parseLong(coursePackageId));
			studentCourseExt.setCourseGroupId(order.getCourseGroupId());
			studentCourseExt.setSourceType(1);//来源类型(正常购买1、免费领2、拼团购3、直接生成4)
			studentCourseExt.setSourceId(order.getId());//来源id(订单id或活动id)
			studentCourseExt.setCourseStartHintFlag(1);//课程开始提醒标识(1未提醒、2已提醒)
			studentCourseExt.setFullStudyFlag(1);//是否全部学习标识(1否、2是)
			studentCourseExt.setFullFinishFlag(1);//是否全部完成标识(1否、2是)

			studentCourseExt.setRemark("");
			studentCourseExt.setState(1);//状态(1未分配教师、2已分配教师)
			studentCourseExt.setCreatedAt(new Date());
			studentCourseExt.setUpdatedAt(new Date());
			studentCourseExt.setDelFlag(0);


			Map<String, Object> m = new HashMap<>();
			//m.put("courseId", courseId);
			m.put("coursePackageId", coursePackageId);
			m.put("studentId", order.getStudentId().toString());
			m.put("delFlag", "0");
			StudentCourseExt sc = studentCourseService.findObjectByCondition(m);
			if (sc == null || sc.getId() == 0) {
				//生成学生课程信息
				studentCourseService.insert(studentCourseExt);
			}else{
				studentCourseExt = sc;
			}


			//do nothing
			logger.info("generateTeacherCourseAndStudentCourseData");
			if(Constant.autoChooseTeacherFlag.intValue()==2) {//支付完自动分班标识(1否、2是)
				//生成教师课程、学生课程信息
				logger.info("[generateTeacherCourseAndStudentCourseData]:"+"	"+"studentId="+studentCourseExt.getStudentId()+",coursePackageId="+studentCourseExt.getCoursePackageId()+",courseId="+studentCourseExt.getCourseId());
				studentCourseOrderService.generateTeacherCourseAndStudentCourseData(studentCourseExt.getStudentId().toString(), studentCourseExt.getCoursePackageId().toString(), studentCourseExt.getCourseId().toString());
			}
		}


	}







}
