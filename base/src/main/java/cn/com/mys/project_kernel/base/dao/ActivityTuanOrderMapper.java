package cn.com.mys.project_kernel.base.dao;

import cn.com.mys.project_kernel.base.entity.ActivityTuanOrder;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityTuanOrderMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table activity_tuan_order
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table activity_tuan_order
     *
     * @mbggenerated
     */
    int insert(ActivityTuanOrder record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table activity_tuan_order
     *
     * @mbggenerated
     */
    int insertSelective(ActivityTuanOrder record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table activity_tuan_order
     *
     * @mbggenerated
     */
    ActivityTuanOrder selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table activity_tuan_order
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(ActivityTuanOrder record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table activity_tuan_order
     *
     * @mbggenerated
     */
    int updateByPrimaryKeyWithBLOBs(ActivityTuanOrder record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table activity_tuan_order
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(ActivityTuanOrder record);
}