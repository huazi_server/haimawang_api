package cn.com.mys.project_kernel.base.service.impl;

import cn.com.mys.project_kernel.base.dao.CourseLessonVideoPointMapper;
import cn.com.mys.project_kernel.base.dao.base.BaseMapper;
import cn.com.mys.project_kernel.base.dao.ext.CourseLessonVideoPointMapperExt;
import cn.com.mys.project_kernel.base.service.CourseLessonVideoPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service的实现类
 *
 */

@Service
public class CourseLessonVideoPointServiceImpl implements CourseLessonVideoPointService {

	@Autowired
	public BaseMapper baseMapper;

	@Autowired
	public CourseLessonVideoPointMapper courseLessonVideoPointMapper;

	@Autowired
	public CourseLessonVideoPointMapperExt courseLessonVideoPointMapperExt;





}
