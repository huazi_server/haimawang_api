package cn.com.mys.project_kernel.base.service;


import cn.com.mys.project_kernel.base.entity.ext.ProductionCommentReplyLikeExt;

import java.util.Map;

/**
 * Service的接口
 *
 */
public interface ProductionCommentReplyLikeService {

    ProductionCommentReplyLikeExt findObjectByCondition(Map<String, Object> map4Param);

    Integer insert(ProductionCommentReplyLikeExt productionCommentReplyLikeExt);

    Integer delete(ProductionCommentReplyLikeExt productionCommentReplyLikeExt);
}
