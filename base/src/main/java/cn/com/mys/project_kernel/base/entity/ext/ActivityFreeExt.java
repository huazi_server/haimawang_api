package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.ActivityFree;
import cn.com.mys.project_kernel.base.entity.Course;

public class ActivityFreeExt extends ActivityFree {

    private CourseExt courseInfo;//课程
    public CourseExt getCourseInfo() {
        return courseInfo;
    }
    public void setCourseInfo(CourseExt courseInfo) {
        this.courseInfo = courseInfo;
    }
    private CourseGroupExt courseGroupInfo;//课程分组
    public CourseGroupExt getCourseGroupInfo() {
        return courseGroupInfo;
    }
    public void setCourseGroupInfo(CourseGroupExt courseGroupInfo) {
        this.courseGroupInfo = courseGroupInfo;
    }


    private Integer gainFlag;//1未领过、2已领过
    public Integer getGainFlag() {
        return gainFlag;
    }
    public void setGainFlag(Integer gainFlag) {
        this.gainFlag = gainFlag;
    }
}