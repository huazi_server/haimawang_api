package cn.com.mys.project_kernel.base.entity.ext;


import cn.com.mys.project_kernel.base.entity.CompetitionPlayer;
import cn.com.mys.project_kernel.base.entity.CompetitionProduction;

public class CompetitionProductionExt extends CompetitionProduction {

    private CompetitionAwardExt competitionAwardInfo;//奖励项
    public CompetitionAwardExt getCompetitionAwardInfo() {
        return competitionAwardInfo;
    }
    public void setCompetitionAwardInfo(CompetitionAwardExt competitionAwardInfo) {
        this.competitionAwardInfo = competitionAwardInfo;
    }

    private CompetitionPlayerExt competitionPlayerInfo;//作者信息
    public CompetitionPlayerExt getCompetitionPlayerInfo() {
        return competitionPlayerInfo;
    }
    public void setCompetitionPlayerInfo(CompetitionPlayerExt competitionPlayerInfo) {
        this.competitionPlayerInfo = competitionPlayerInfo;
    }

    private Integer maxVoteCount;//比赛最高票
    public Integer getMaxVoteCount() {
        return maxVoteCount;
    }
    public void setMaxVoteCount(Integer maxVoteCount) {
        this.maxVoteCount = maxVoteCount;
    }
}