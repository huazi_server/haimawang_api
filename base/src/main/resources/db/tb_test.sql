

## 测试表

CREATE TABLE tb_test(
	test_id VARCHAR(32) NOT NULL,			-- 主键id
	column_1 VARCHAR(32) DEFAULT NULL,		-- 字段一
	column_2 VARCHAR(32) DEFAULT NULL,		-- 字段二
	column_3 VARCHAR(32) DEFAULT NULL,		-- 字段三
	PRIMARY KEY (test_id)

)ENGINE=INNODB DEFAULT CHARSET=utf8;

