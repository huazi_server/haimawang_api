





-- ----------------------------
-- Table structure for activity_free
-- ----------------------------
DROP TABLE IF EXISTS `activity_free`;
CREATE TABLE `activity_free` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '活动名称',
  `dest` varchar(100) DEFAULT NULL COMMENT '活动目的',
  `intro` varchar(1000) DEFAULT NULL COMMENT '活动介绍',
  `quotaCount` int(11) DEFAULT NULL COMMENT '名额人数',
  `gainCount` int(11) DEFAULT NULL COMMENT '已领人数',
  `lookCount` int(11) DEFAULT NULL COMMENT '浏览量',
  `courseType` int(11) DEFAULT NULL COMMENT '课程类型(1单课程、2系列课)',
  `courseGroupId` bigint(20) DEFAULT NULL COMMENT '课程分组id',
  `courseId` bigint(20) DEFAULT NULL COMMENT '课程id',
  `imgUrl` varchar(1000) DEFAULT NULL COMMENT '图片地址(多个","分隔)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(1下线、2上线)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10003 DEFAULT CHARSET=utf8 COMMENT='活动(免费领)表';



-- ----------------------------
-- Table structure for activity_tuan
-- ----------------------------
DROP TABLE IF EXISTS `activity_tuan`;
CREATE TABLE `activity_tuan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '活动名称',
  `dest` varchar(100) DEFAULT NULL COMMENT '活动目的',
  `intro` varchar(1000) DEFAULT NULL COMMENT '活动介绍',
  `restrictionCount` int(11) DEFAULT NULL COMMENT '成团限制人数',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '价格(小数位保留2位)',
  `startAt` datetime DEFAULT NULL COMMENT '开始时间',
  `endAt` datetime DEFAULT NULL COMMENT '结束时间',
  `courseType` int(11) DEFAULT NULL COMMENT '课程类型(1单课程、2系列课)',
  `courseGroupIds` varchar(1000) DEFAULT NULL COMMENT '课程分组id(多个","分隔)',
  `courseIds` varchar(1000) DEFAULT NULL COMMENT '课程id(多个","分隔)',
  `imgUrl` varchar(1000) DEFAULT NULL COMMENT '图片地址(多个","分隔)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(0未上线、1已上线)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8 COMMENT='活动(拼团)表';



-- ----------------------------
-- Table structure for activity_tuan_order
-- ----------------------------
DROP TABLE IF EXISTS `activity_tuan_order`;
CREATE TABLE `activity_tuan_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `activityTuanId` bigint(20) NOT NULL COMMENT '拼团活动id',
  `studentId` bigint(20) DEFAULT NULL COMMENT '学生id(团长)',
  `peopleCount` int(11) DEFAULT NULL COMMENT '已有人数',
  `realPeopleCount` int(11) DEFAULT NULL COMMENT '实际已有人数',
  `startAt` datetime DEFAULT NULL COMMENT '开始时间',
  `endAt` datetime DEFAULT NULL COMMENT '结束时间',
  `studentIds` varchar(1000) DEFAULT NULL COMMENT '学生id,多个","分隔',
  `studentOrderIds` varchar(1000) DEFAULT NULL COMMENT '学生订单id,多个","分隔',
  `studentNames` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '学生名称,多个","分隔',
  `studentAvatarUrls` varchar(1000) DEFAULT NULL COMMENT '学生头像,多个","分隔',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(0初始化、1拼团中、2拼团成功、3拼团失败、4拼团结束)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1048 DEFAULT CHARSET=utf8 COMMENT='拼团订单表';



-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` bigint(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `avatarUrl` varchar(255) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `delFlag` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20111250 DEFAULT CHARSET=utf8;



-- ----------------------------
-- Table structure for channel
-- ----------------------------
DROP TABLE IF EXISTS `channel`;
CREATE TABLE `channel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `info` varchar(1000) DEFAULT NULL COMMENT '介绍',
  `address` varchar(1000) DEFAULT NULL COMMENT '地址',
  `number` varchar(20) DEFAULT NULL COMMENT '编号',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10003 DEFAULT CHARSET=utf8 COMMENT='渠道表';



-- ----------------------------
-- Table structure for channel_activity
-- ----------------------------
DROP TABLE IF EXISTS `channel_activity`;
CREATE TABLE `channel_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `channelId` bigint(20) DEFAULT NULL COMMENT '名称',
  `activityType` int(11) DEFAULT NULL COMMENT '介绍',
  `activityId` bigint(20) DEFAULT NULL COMMENT '地址',
  `number` varchar(20) DEFAULT NULL COMMENT '编号',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10004 DEFAULT CHARSET=utf8 COMMENT='渠道表';



-- ----------------------------
-- Table structure for channel_statistics
-- ----------------------------
DROP TABLE IF EXISTS `channel_statistics`;
CREATE TABLE `channel_statistics` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `channelId` bigint(20) NOT NULL COMMENT '渠道id',
  `studentId` bigint(20) NOT NULL COMMENT '学生id',
  `phone` varchar(32) DEFAULT NULL COMMENT '手机号',
  `sourceType` int(11) DEFAULT NULL COMMENT '来源类型(购买单课程1、购买系列课2、免费领活动领取失败3、免费领活动领取成功4)',
  `sourceId` bigint(20) DEFAULT NULL COMMENT '来源id(订单id或活动id)',
  `courseType` int(11) DEFAULT NULL,
  `courseId` bigint(20) DEFAULT NULL COMMENT '课程id或年课id',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COMMENT='渠道统计表';



-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `personId` bigint(20) DEFAULT NULL COMMENT '教师或学生id',
  `type` int(11) DEFAULT NULL COMMENT '类型(1教师、2学生)',
  `title` varchar(100) DEFAULT NULL COMMENT '标题',
  `content` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '内容',
  `imgUrl` varchar(1000) DEFAULT NULL COMMENT '截图地址(多个","分隔)',
  `contact` varchar(1000) DEFAULT NULL COMMENT '联系方式',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(0待处理、1处理中 2已处理)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='意见反馈表';



-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `coursePackageId` bigint(20) NOT NULL COMMENT '课程包id',
  `courseGroupId` bigint(20) DEFAULT NULL COMMENT '课程分组id',
  `name` varchar(1000) DEFAULT NULL COMMENT '名称',
  `coverUrl` varchar(1000) DEFAULT NULL COMMENT '封面图片url',
  `courseType` int(11) DEFAULT NULL COMMENT '课程类型(1单课程、2系列课)',
  `courseFlag` int(11) DEFAULT NULL COMMENT '课程标识(1需购买课程、2非购买课程)',
  `issue` varchar(32) DEFAULT NULL COMMENT '课程期次',
  `signStartAt` datetime DEFAULT NULL COMMENT '报名开始时间',
  `signEndAt` datetime DEFAULT NULL COMMENT '报名结束时间',
  `teachStartAt` datetime DEFAULT NULL COMMENT '课程开始时间',
  `teachEndAt` datetime DEFAULT NULL COMMENT '课程结束时间',
  `stage` int(11) DEFAULT NULL COMMENT '阶段(0-6)',
  `category` int(11) DEFAULT NULL COMMENT '种类(1scratch)',
  `enrollCount` int(11) DEFAULT NULL COMMENT '招生人数',
  `enableFlag` int(11) DEFAULT NULL COMMENT '启用标记(1正常、2禁用)',
  `planTeacherIds` varchar(1000) DEFAULT NULL COMMENT '计划授课教师id(多个","分隔)',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '价格(小数位保留2位)',
  `realPrice` decimal(10,2) DEFAULT '0.00' COMMENT '实际价格(小数位保留2位)',
  `operateUserId` bigint(20) DEFAULT NULL COMMENT '操作人id',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态,0编辑中 1待上线、2已上线、3报名中、4报名截止、5课程进行中、6课程结束',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10024 DEFAULT CHARSET=utf8 COMMENT='课程表';



-- ----------------------------
-- Table structure for course_group
-- ----------------------------
DROP TABLE IF EXISTS `course_group`;
CREATE TABLE `course_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(1000) DEFAULT NULL COMMENT '名称',
  `issue` varchar(32) DEFAULT NULL COMMENT '期次',
  `category` int(11) DEFAULT NULL COMMENT '种类(1scratch)',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '价格(小数位保留2位)',
  `realPrice` decimal(10,2) DEFAULT '0.00' COMMENT '实际价格(小数位保留2位)',
  `enableFlag` int(11) DEFAULT NULL COMMENT '启用标记(1正常、2禁用)',
  `courseGroupFlag` int(11) DEFAULT NULL COMMENT '课程分组标识(1正常课程分组、2活动课程分组)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态, 1待上线、2已上线 3\\禁用',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='课程分组表';



-- ----------------------------
-- Table structure for course_lesson
-- ----------------------------
DROP TABLE IF EXISTS `course_lesson`;
CREATE TABLE `course_lesson` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `courseId` bigint(20) NOT NULL COMMENT '课程id',
  `coursePackageId` bigint(20) NOT NULL COMMENT '课程包id',
  `coursePackageLessonId` bigint(20) NOT NULL COMMENT '课程包对应的课时id',
  `startAt` datetime DEFAULT NULL COMMENT '开始时间',
  `endAt` datetime DEFAULT NULL COMMENT '结束时间',
  `timeMinute` int(11) DEFAULT NULL COMMENT '时长(单位:分钟)',
  `title` varchar(1000) DEFAULT NULL COMMENT '标题',
  `name` varchar(1000) DEFAULT NULL COMMENT '名称',
  `knowledges` varchar(1000) DEFAULT NULL COMMENT '知识点(多个","分隔)',
  `shortKnowledges` varchar(1000) DEFAULT NULL COMMENT '简化知识点(多个","分隔)',
  `number` int(11) DEFAULT NULL COMMENT '课时序号(由小到大)',
  `enableFlag` int(11) DEFAULT NULL COMMENT '启用标记(1正常、2禁用)',
  `coverUrl` varchar(1000) DEFAULT NULL COMMENT '封面图片url',
  `videoUrl` varchar(1000) DEFAULT NULL COMMENT '视频地址',
  `docUrl` varchar(1000) DEFAULT NULL COMMENT '课件地址',
  `imgUrl` text COMMENT '图片地址(多个","分隔)',
  `textbookUrl` varchar(1000) DEFAULT NULL COMMENT '讲义地址',
  `sb3Url` varchar(1000) DEFAULT NULL COMMENT 'scratch地址(作业参考地址)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态 0 正常 , 1 已过',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=349 DEFAULT CHARSET=utf8 COMMENT='课程对应的课时表';



-- ----------------------------
-- Table structure for course_lesson_video_point
-- ----------------------------
DROP TABLE IF EXISTS `course_lesson_video_point`;
CREATE TABLE `course_lesson_video_point` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `coursePackageId` bigint(20) DEFAULT NULL,
  `coursePackageLessonId` bigint(20) DEFAULT NULL,
  `courseId` bigint(20) NOT NULL COMMENT '课程id',
  `courseLessonId` bigint(20) NOT NULL COMMENT '课时id',
  `number` int(11) DEFAULT NULL COMMENT '序号(由小到大)',
  `startTime` int(11) DEFAULT NULL COMMENT '开始时间(秒)',
  `endTime` int(11) DEFAULT NULL COMMENT '结束时间(秒)',
  `title` varchar(1000) DEFAULT NULL COMMENT '标题',
  `imgUrl` varchar(1000) DEFAULT NULL COMMENT '图片url',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(0禁用、1启用)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程对应的课时的视频片段表';



-- ----------------------------
-- Table structure for course_package
-- ----------------------------
DROP TABLE IF EXISTS `course_package`;
CREATE TABLE `course_package` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(32) DEFAULT NULL COMMENT '名称',
  `lessonCount` int(11) DEFAULT NULL COMMENT '课时数量',
  `coverUrl` varchar(1000) DEFAULT NULL COMMENT '封面图片url',
  `stage` int(11) DEFAULT NULL COMMENT '阶段(0-6)',
  `category` int(11) DEFAULT NULL COMMENT '种类(1scratch)',
  `enableFlag` int(11) DEFAULT NULL COMMENT '启用标记(1正常、2禁用)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='课程包表';



-- ----------------------------
-- Table structure for course_package_img_config
-- ----------------------------
DROP TABLE IF EXISTS `course_package_img_config`;
CREATE TABLE `course_package_img_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `stage` int(11) DEFAULT NULL COMMENT '阶段(0-6)',
  `name` varchar(1000) DEFAULT NULL COMMENT '名称',
  `coverUrl` varchar(1000) DEFAULT NULL COMMENT '封面图片url',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='课程包图片配置表';



-- ----------------------------
-- Table structure for course_package_lesson
-- ----------------------------
DROP TABLE IF EXISTS `course_package_lesson`;
CREATE TABLE `course_package_lesson` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `coursePackageId` bigint(20) NOT NULL COMMENT '课程包id',
  `title` varchar(1000) DEFAULT NULL COMMENT '标题',
  `name` varchar(1000) DEFAULT NULL COMMENT '名称',
  `knowledges` varchar(1000) DEFAULT NULL COMMENT '知识点(多个","分隔)',
  `shortKnowledges` varchar(1000) DEFAULT NULL COMMENT '简化知识点(多个","分隔)',
  `number` int(11) DEFAULT NULL COMMENT '课时序号(由小到大)',
  `enableFlag` int(11) DEFAULT NULL COMMENT '启用标记(1正常、2禁用)',
  `coverUrl` varchar(1000) DEFAULT NULL COMMENT '封面图片url',
  `videoUrl` varchar(1000) DEFAULT NULL COMMENT '视频地址',
  `docUrl` varchar(1000) DEFAULT NULL COMMENT '课件地址',
  `imgUrl` text COMMENT '图片地址(多个","分隔)',
  `textbookUrl` varchar(1000) DEFAULT NULL COMMENT '讲义地址',
  `sb3Url` varchar(1000) DEFAULT NULL COMMENT 'scratch地址(作业参考地址)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8 COMMENT='课程包对应的课时表';



-- ----------------------------
-- Table structure for course_package_lesson_video_point
-- ----------------------------
DROP TABLE IF EXISTS `course_package_lesson_video_point`;
CREATE TABLE `course_package_lesson_video_point` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `coursePackageId` bigint(20) NOT NULL COMMENT '课程包id',
  `coursePackageLessonId` bigint(20) NOT NULL COMMENT '课时包的课时id',
  `number` int(11) DEFAULT NULL COMMENT '序号(由小到大)',
  `startTime` int(11) DEFAULT NULL COMMENT '开始时间(秒)',
  `endTime` int(11) DEFAULT NULL COMMENT '结束时间(秒)',
  `title` varchar(1000) DEFAULT NULL COMMENT '标题',
  `imgUrl` varchar(1000) DEFAULT NULL COMMENT '图片url',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(0禁用、1启用)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='课程对应的课时的视频片段表';



-- ----------------------------
-- Table structure for notify
-- ----------------------------
DROP TABLE IF EXISTS `notify`;
CREATE TABLE `notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `ownerType` int(11) DEFAULT NULL COMMENT '所属类型(1教师、2学生)',
  `personId` bigint(20) DEFAULT NULL COMMENT '教师或学生id(消息所属)',
  `type` int(11) DEFAULT NULL COMMENT '类型(1系统、2收到的点赞、3收到的评论、4发出的评论)',
  `fromType` int(11) DEFAULT NULL COMMENT '来自类型(1教师、2学生)',
  `fromId` bigint(20) DEFAULT NULL COMMENT '教师或学生id(来自)',
  `fromName` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '教师或学生名称(来自)',
  `fromAvatarUrl` varchar(1000) DEFAULT NULL COMMENT '教师或学生头像(来自)',
  `toType` int(11) DEFAULT NULL COMMENT '接收类型(1教师、2学生)',
  `toId` bigint(20) DEFAULT NULL COMMENT '教师或学生id(接收)',
  `toName` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '教师或学生名称(接收)',
  `toAvatarUrl` varchar(1000) DEFAULT NULL COMMENT '教师或学生头像(接收)',
  `content` varchar(1000) DEFAULT NULL COMMENT '内容',
  `productionId` bigint(20) DEFAULT NULL COMMENT '关联的作品id',
  `productionName` varchar(1000) DEFAULT NULL COMMENT '关联的作品名称',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(1未读、2已读)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COMMENT='通知表';



-- ----------------------------
-- Table structure for production
-- ----------------------------
DROP TABLE IF EXISTS `production`;
CREATE TABLE `production` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `personId` bigint(20) DEFAULT NULL COMMENT '教师或学生id',
  `authorName` varchar(32) DEFAULT NULL COMMENT '作者名称',
  `authorAvatarUrl` varchar(1000) DEFAULT NULL COMMENT '作者头像',
  `sourceType` int(11) NOT NULL COMMENT '来源类型(1系统、2教师、3学生)',
  `name` varchar(1000) DEFAULT NULL COMMENT '名称',
  `content` varchar(1000) DEFAULT NULL COMMENT '内容地址',
  `coverUrl` varchar(1000) DEFAULT NULL COMMENT '封面图片url',
  `lookCount` int(11) DEFAULT NULL COMMENT '浏览数',
  `likeCount` int(11) DEFAULT NULL COMMENT '点赞数',
  `commentCount` int(11) DEFAULT NULL COMMENT '评论数',
  `intro` varchar(1000) DEFAULT NULL COMMENT '介绍',
  `operateExplain` varchar(1000) DEFAULT NULL COMMENT '操作说明',
  `score` int(11) DEFAULT NULL COMMENT '评分',
  `tagIds` varchar(1000) DEFAULT NULL COMMENT '标签id(多个","分隔)',
  `tagNames` varchar(1000) DEFAULT NULL COMMENT '标签名称(多个","分隔)',
  `openFlag` int(11) DEFAULT NULL COMMENT '开放标记(1开放、2不开放)',
  `category` int(11) DEFAULT NULL COMMENT '种类(1scratch)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(1未发布、2已发布、3已下线)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8 COMMENT='作品表';



-- ----------------------------
-- Table structure for production_comment
-- ----------------------------
DROP TABLE IF EXISTS `production_comment`;
CREATE TABLE `production_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `studentId` bigint(20) NOT NULL COMMENT '学生id',
  `code` varchar(32) DEFAULT NULL COMMENT '编码',
  `type` int(11) DEFAULT NULL COMMENT '类型(0:默认)',
  `productionId` bigint(20) DEFAULT NULL COMMENT '作品id',
  `fromId` bigint(20) DEFAULT NULL COMMENT '评论者id',
  `fromName` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评论者名称',
  `fromAvatarUrl` varchar(1000) DEFAULT NULL COMMENT '评论者头像',
  `content` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '内容',
  `likeCount` int(11) DEFAULT NULL COMMENT '点赞次数',
  `replyCount` int(11) DEFAULT NULL COMMENT '回复次数',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='作品评论表';



-- ----------------------------
-- Table structure for production_comment_like
-- ----------------------------
DROP TABLE IF EXISTS `production_comment_like`;
CREATE TABLE `production_comment_like` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `commentId` bigint(20) DEFAULT NULL COMMENT '评论id',
  `studentId` bigint(20) DEFAULT NULL COMMENT '学生id',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='作品评论点赞表';



-- ----------------------------
-- Table structure for production_comment_reply
-- ----------------------------
DROP TABLE IF EXISTS `production_comment_reply`;
CREATE TABLE `production_comment_reply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `studentId` bigint(20) NOT NULL COMMENT '学生id',
  `type` int(11) DEFAULT NULL COMMENT '类型(0:默认,1:针对评论的回复,2:针对回复的回复)',
  `productionId` bigint(20) DEFAULT NULL COMMENT '所属作品id',
  `commentId` bigint(20) DEFAULT NULL COMMENT '所属评论id',
  `targetId` bigint(20) DEFAULT NULL COMMENT '目标id(评论id或回复id)',
  `fromId` bigint(20) DEFAULT NULL COMMENT '回复者id',
  `fromName` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '回复者名称',
  `fromAvatarUrl` varchar(1000) DEFAULT NULL COMMENT '回复者头像',
  `toId` bigint(20) DEFAULT NULL COMMENT '目标者id',
  `toName` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '目标者名称',
  `toAvatarUrl` varchar(1000) DEFAULT NULL COMMENT '目标者头像',
  `content` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '内容',
  `likeCount` int(11) DEFAULT NULL COMMENT '点赞次数',
  `replyCount` int(11) DEFAULT NULL COMMENT '回复次数',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='作品评论回复表';



-- ----------------------------
-- Table structure for production_comment_reply_like
-- ----------------------------
DROP TABLE IF EXISTS `production_comment_reply_like`;
CREATE TABLE `production_comment_reply_like` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `replyId` bigint(20) DEFAULT NULL COMMENT '回复id',
  `studentId` bigint(20) DEFAULT NULL COMMENT '学生id',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='作品评论回复点赞表';



-- ----------------------------
-- Table structure for production_cover
-- ----------------------------
DROP TABLE IF EXISTS `production_cover`;
CREATE TABLE `production_cover` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(1000) DEFAULT NULL COMMENT '名称',
  `imgUrl` varchar(1000) DEFAULT NULL COMMENT '图片url地址',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='作品封面表';



-- ----------------------------
-- Table structure for production_tag
-- ----------------------------
DROP TABLE IF EXISTS `production_tag`;
CREATE TABLE `production_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(1000) DEFAULT NULL COMMENT '名称',
  `imgUrl` varchar(1000) DEFAULT NULL COMMENT '图片url地址',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='作品标签表';



-- ----------------------------
-- Table structure for random_user
-- ----------------------------
DROP TABLE IF EXISTS `random_user`;
CREATE TABLE `random_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `nickname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatarUrl` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `city` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `info` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `delFlag` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8mb4 COMMENT='随机用户';



-- ----------------------------
-- Table structure for report
-- ----------------------------
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `reportConfigId` bigint(20) DEFAULT NULL COMMENT '配置id',
  `type` int(11) DEFAULT NULL COMMENT '类型(1作品、2评论、3回复)',
  `defendantId` bigint(20) DEFAULT NULL COMMENT '作品、评论、回复id',
  `plaintiffId` bigint(20) NOT NULL COMMENT '举报人id',
  `title` varchar(100) DEFAULT NULL COMMENT '标题(关键词)',
  `content` varchar(1000) DEFAULT NULL COMMENT '内容',
  `imgUrl` varchar(1000) DEFAULT NULL COMMENT '截图地址(多个","分隔)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(0待处理、1处理中 2已处理)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='举报信息表';



-- ----------------------------
-- Table structure for report_config
-- ----------------------------
DROP TABLE IF EXISTS `report_config`;
CREATE TABLE `report_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `type` int(11) DEFAULT NULL COMMENT '类型(1作品、2评论、3回复)',
  `content` varchar(1000) DEFAULT NULL COMMENT '内容',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='举报配置表';



-- ----------------------------
-- Table structure for resource_config
-- ----------------------------
DROP TABLE IF EXISTS `resource_config`;
CREATE TABLE `resource_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `type` int(11) DEFAULT NULL COMMENT '类型(1背景、2造型、3声音、4角色)',
  `title` varchar(1000) DEFAULT NULL COMMENT '标题',
  `name` varchar(1000) DEFAULT NULL COMMENT '名称',
  `fileUrl` varchar(1000) DEFAULT NULL COMMENT '文件url地址',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='资源配置表';



-- ----------------------------
-- Table structure for resource_file
-- ----------------------------
DROP TABLE IF EXISTS `resource_file`;
CREATE TABLE `resource_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `type` int(11) DEFAULT NULL COMMENT '类型(1背景、2造型、3声音)',
  `name` varchar(1000) DEFAULT NULL COMMENT '名称',
  `fileName` varchar(1000) DEFAULT NULL COMMENT '文件名称',
  `fileName4New` varchar(1000) DEFAULT NULL COMMENT '文件名称(新)',
  `filePath4New` varchar(1000) DEFAULT NULL COMMENT '文件路径(新)',
  `fileUrl` varchar(1000) DEFAULT NULL COMMENT '文件url地址',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(1未完成、2已完成)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1359 DEFAULT CHARSET=utf8 COMMENT='资源表';



-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `accountId` bigint(20) DEFAULT NULL COMMENT 'accountId',
  `code` varchar(32) DEFAULT NULL COMMENT '代码',
  `type` int(11) DEFAULT NULL COMMENT '类型(0默认)',
  `name` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '名称',
  `nickName` varchar(100) DEFAULT NULL COMMENT '昵称',
  `wxNickName` varchar(100) DEFAULT NULL COMMENT '微信昵称',
  `openId` varchar(1000) DEFAULT NULL COMMENT '微信openId',
  `phone` varchar(32) DEFAULT NULL COMMENT '手机号',
  `sourceType` int(11) DEFAULT NULL COMMENT '来源类型(1、WEB端)',
  `registerAt` datetime DEFAULT NULL COMMENT '注册时间',
  `enableFlag` int(11) DEFAULT NULL COMMENT '启用标记(1正常、2禁用)',
  `avatarUrl` varchar(1000) DEFAULT NULL COMMENT '头像',
  `qrCodeUrl` varchar(1000) DEFAULT NULL COMMENT '微信二维码图片地址',
  `email` varchar(1000) DEFAULT NULL COMMENT '邮箱',
  `info` varchar(1000) DEFAULT NULL COMMENT '介绍',
  `gender` int(11) DEFAULT NULL COMMENT '性别(0未知、1男、2女)',
  `channelIds` varchar(1000) DEFAULT NULL COMMENT '渠道id,多个","分隔',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `accountId` (`accountId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COMMENT='学生表';



-- ----------------------------
-- Table structure for student_course
-- ----------------------------
DROP TABLE IF EXISTS `student_course`;
CREATE TABLE `student_course` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `studentId` bigint(20) NOT NULL COMMENT '学生id',
  `courseId` bigint(20) NOT NULL COMMENT '课程id',
  `coursePackageId` bigint(20) DEFAULT NULL COMMENT '课程包id',
  `teacherCourseId` bigint(20) DEFAULT NULL COMMENT '教师课程id',
  `courseGroupId` bigint(20) DEFAULT NULL COMMENT '课程分组id',
  `teacherId` bigint(20) DEFAULT NULL COMMENT '教师id',
  `score` int(11) DEFAULT NULL COMMENT '给教师评分',
  `sourceType` int(11) DEFAULT NULL COMMENT '来源类型(正常购买1、免费领2、拼团购3、直接生成4)',
  `sourceId` bigint(20) DEFAULT NULL COMMENT '来源id(订单id或活动id)',
  `courseStartHintFlag` int(11) DEFAULT NULL COMMENT '课程开始提醒标识(1未提醒、2已提醒)',
  `fullStudyFlag` int(11) DEFAULT NULL COMMENT '是否全部学习标识(1否、2是)',
  `fullFinishFlag` int(11) DEFAULT NULL COMMENT '是否全部完成标识(1否、2是)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(1未分配教师、2已分配教师)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=298 DEFAULT CHARSET=utf8 COMMENT='学生课程表';



-- ----------------------------
-- Table structure for student_course_homework
-- ----------------------------
DROP TABLE IF EXISTS `student_course_homework`;
CREATE TABLE `student_course_homework` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `studentId` bigint(20) NOT NULL COMMENT '学生id',
  `courseId` bigint(20) NOT NULL COMMENT '课程id(冗余)',
  `courseLessonId` bigint(20) DEFAULT NULL COMMENT '课程对应的课时id(冗余)',
  `studentCourseId` bigint(20) NOT NULL COMMENT '学生课程id(冗余)',
  `teacherId` bigint(20) DEFAULT NULL COMMENT '教师id(分配的教师)(冗余)',
  `teacherCourseId` bigint(20) DEFAULT NULL COMMENT '教师课程id',
  `teacherCourseLessonId` bigint(20) DEFAULT NULL COMMENT '教师课程对应的课时id',
  `coverUrl` varchar(1000) DEFAULT NULL COMMENT '封面图片url',
  `content` varchar(1000) DEFAULT NULL COMMENT '内容(提交后)',
  `draft` varchar(1000) DEFAULT NULL COMMENT '内容(提交前的草稿)',
  `answerSecond` int(11) DEFAULT NULL COMMENT '作答时间,单位秒',
  `commitAt` datetime DEFAULT NULL COMMENT '提交时间',
  `commitCount` int(11) DEFAULT NULL COMMENT '提交次数',
  `comment` varchar(1000) DEFAULT NULL COMMENT '评语',
  `commentAt` datetime DEFAULT NULL COMMENT '评语时间',
  `onlineAt` datetime DEFAULT NULL COMMENT '上线时间',
  `onlineFlag` int(11) DEFAULT NULL COMMENT '在线标识(1离线、2在线)',
  `studyFlag` int(11) DEFAULT NULL COMMENT '学习标识(1未学习、2已学习)',
  `finishFlag` int(11) DEFAULT NULL COMMENT '完成标识(0初始化、1待修改、2已完成)',
  `checkFlag` int(11) DEFAULT NULL COMMENT '批改标识(1未批改、2已批改)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(1未保存、2未提交、3已提交)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8 COMMENT='学生课程-作业表';



-- ----------------------------
-- Table structure for student_course_homework_message
-- ----------------------------
DROP TABLE IF EXISTS `student_course_homework_message`;
CREATE TABLE `student_course_homework_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `studentId` bigint(20) NOT NULL COMMENT '学生id',
  `courseId` bigint(20) NOT NULL COMMENT '课程id',
  `courseLessonId` bigint(20) DEFAULT NULL COMMENT '课程对应的课时id',
  `studentCourseId` bigint(20) DEFAULT NULL COMMENT '学生课程id',
  `teacherCourseId` bigint(20) DEFAULT NULL COMMENT '教师课程id',
  `content` varchar(1000) DEFAULT NULL COMMENT '内容',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(1未解决、2已解决)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='学生课程-作业留言表';



-- ----------------------------
-- Table structure for student_course_order
-- ----------------------------
DROP TABLE IF EXISTS `student_course_order`;
CREATE TABLE `student_course_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `studentId` bigint(20) NOT NULL COMMENT '学生id',
  `phone` varchar(32) DEFAULT NULL COMMENT '手机号',
  `channelId` bigint(20) DEFAULT NULL COMMENT '渠道id',
  `courseType` int(11) DEFAULT NULL COMMENT '课程类型(1单课程、2系列课)',
  `courseGroupId` bigint(20) DEFAULT NULL COMMENT '课程分组id',
  `coursePackageIds` varchar(1000) NOT NULL COMMENT '课程包id,多个","分隔',
  `courseIds` varchar(1000) NOT NULL COMMENT '课程id,多个","分隔',
  `orderNo` varchar(20) NOT NULL COMMENT '系统订单编号',
  `activityTuanOrderId` bigint(20) DEFAULT NULL COMMENT '拼团订单id',
  `sourceType` int(11) NOT NULL COMMENT '来源类型(1WEB端、2小程序、3公众号)',
  `orderMoney` decimal(10,2) DEFAULT '0.00' COMMENT '订单金额(小数位保留2位)',
  `payMoney` decimal(10,2) DEFAULT '0.00' COMMENT '支付金额(小数位保留2位)',
  `refundMoney` decimal(10,2) DEFAULT NULL COMMENT '退款金额(小数位保留2位)',
  `payAt` datetime DEFAULT NULL COMMENT '支付时间',
  `payChannel` int(11) DEFAULT NULL COMMENT '支付渠道(1微信、2支付宝)',
  `payType` varchar(32) DEFAULT NULL COMMENT '支付方式(weixin-web|weixin-app|weixin-jsapi)',
  `appId` varchar(1000) DEFAULT NULL COMMENT 'WEB端:20111113,小程序:20111114,公众号:20111115',
  `appKey` varchar(1000) DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=utf8 COMMENT='课程订单表';



-- ----------------------------
-- Table structure for student_course_order_flow
-- ----------------------------
DROP TABLE IF EXISTS `student_course_order_flow`;
CREATE TABLE `student_course_order_flow` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `orderNo` varchar(20) NOT NULL COMMENT '系统订单编号',
  `payMoney` decimal(10,2) DEFAULT '0.00' COMMENT '支付金额(小数位保留2位)',
  `payAt` datetime DEFAULT NULL COMMENT '支付时间',
  `payChannel` int(11) DEFAULT NULL COMMENT '支付渠道(1微信、2支付宝)',
  `payType` varchar(32) DEFAULT NULL COMMENT '支付方式(weixin-web|weixin-app|weixin-jsapi)',
  `channelOrderNo` bigint(20) DEFAULT NULL COMMENT '通道订单编号(第三方)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态(1支付成功、2支付失败)',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程订单流水表';



-- ----------------------------
-- Table structure for student_production_collect
-- ----------------------------
DROP TABLE IF EXISTS `student_production_collect`;
CREATE TABLE `student_production_collect` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `studentId` bigint(20) DEFAULT NULL COMMENT '学生id',
  `productionId` bigint(20) DEFAULT NULL COMMENT '作品id',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='学生收藏表';



-- ----------------------------
-- Table structure for student_production_like
-- ----------------------------
DROP TABLE IF EXISTS `student_production_like`;
CREATE TABLE `student_production_like` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `studentId` bigint(20) DEFAULT NULL COMMENT '学生id',
  `productionId` bigint(20) DEFAULT NULL COMMENT '作品id',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='学生点赞表';



-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `accountId` bigint(20) DEFAULT NULL COMMENT 'accountId',
  `code` varchar(32) DEFAULT NULL COMMENT '代码',
  `type` int(11) DEFAULT NULL COMMENT '类型(1名师、2普通)',
  `name` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '名称',
  `nickName` varchar(100) DEFAULT NULL COMMENT '昵称',
  `wxNickName` varchar(100) DEFAULT NULL COMMENT '微信昵称',
  `openId` varchar(1000) DEFAULT NULL COMMENT '微信openId',
  `phone` varchar(32) DEFAULT NULL COMMENT '手机号',
  `sourceType` int(11) DEFAULT NULL COMMENT '来源类型(1、WEB端)',
  `registerAt` datetime DEFAULT NULL COMMENT '注册时间',
  `enableFlag` int(11) DEFAULT NULL COMMENT '启用标记(1正常、2禁用)',
  `avatarUrl` varchar(1000) DEFAULT NULL COMMENT '头像',
  `qrCodeUrl` varchar(1000) DEFAULT NULL COMMENT '微信二维码图片地址',
  `email` varchar(1000) DEFAULT NULL COMMENT '邮箱',
  `info` varchar(1000) DEFAULT NULL COMMENT '介绍',
  `gender` int(11) DEFAULT NULL COMMENT '性别(0未知、1男、2女)',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='教师表';



-- ----------------------------
-- Table structure for teacher_course
-- ----------------------------
DROP TABLE IF EXISTS `teacher_course`;
CREATE TABLE `teacher_course` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `teacherId` bigint(20) NOT NULL COMMENT '教师id',
  `courseId` bigint(20) NOT NULL COMMENT '课程id',
  `studentCount` int(11) DEFAULT NULL COMMENT '学生人数',
  `score` int(11) DEFAULT NULL COMMENT '评分',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='教师课程表';



-- ----------------------------
-- Table structure for teacher_course_lesson
-- ----------------------------
DROP TABLE IF EXISTS `teacher_course_lesson`;
CREATE TABLE `teacher_course_lesson` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `teacherId` bigint(20) NOT NULL COMMENT '教师id',
  `courseId` bigint(20) NOT NULL COMMENT '课程id',
  `teacherCourseId` bigint(20) DEFAULT NULL COMMENT '教师的课程id',
  `courseLessonId` bigint(20) NOT NULL COMMENT '课时id',
  `allCount` int(11) DEFAULT NULL COMMENT '总人数',
  `commitCount` int(11) DEFAULT NULL COMMENT '提交人数',
  `onlineCount` int(11) DEFAULT NULL COMMENT '上线人数',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8 COMMENT='教师课程对应的课时表';



-- ----------------------------
-- Table structure for weixin_notify
-- ----------------------------
DROP TABLE IF EXISTS `weixin_notify`;
CREATE TABLE `weixin_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `type` int(11) DEFAULT NULL COMMENT '类型(1作业批改完成提醒、2开课提醒、3作业完成提醒)',
  `personId` bigint(20) DEFAULT NULL COMMENT '教师或学生id',
  `courseId` bigint(20) NOT NULL COMMENT '课程id',
  `courseLessonId` bigint(20) DEFAULT NULL COMMENT '课时id',
  `content` text COMMENT '通知内容',
  `detail` text COMMENT '通知详情',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `createdAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新时间',
  `delFlag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记(0:显示;1:隐藏)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COMMENT='微信通知表';


