package cn.com.mys.project_kernel.base.test;

import cn.com.mys.project_kernel.base.constant.Constant;
import cn.com.mys.project_kernel.base.exception.OtherException;
import cn.com.mys.project_kernel.base.util.*;

import com.alibaba.fastjson.JSONArray;
import net.sf.json.JSONObject;
import net.sourceforge.groboutils.junit.v1.MultiThreadedTestRunner;
import net.sourceforge.groboutils.junit.v1.TestRunnable;
import org.apache.http.client.ClientProtocolException;
import org.apache.poi.util.SystemOutLogger;
import org.junit.Test;
import sun.security.jgss.HttpCaller;

import java.io.*;
import java.math.BigDecimal;
import java.net.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by mingjun_T on 2016-07-16.
 * 测试类
 *
 */
public class TestCass {


    @Test
    public void md5Test() {
        String md5 = MD5Utils.getMD5String("1");
        System.out.println(md5);//202CB962AC59075B964B07152D234B70
        md5 = MD5Utils.getMD5String("2");
        System.out.println(md5);//202CB962AC59075B964B07152D234B70
        md5 = MD5Utils.getMD5String("3");
        System.out.println(md5);//202CB962AC59075B964B07152D234B70
        md5 = MD5Utils.getMD5String("4");
        System.out.println(md5);//202CB962AC59075B964B07152D234B70
        md5 = MD5Utils.getMD5String("5");
        System.out.println(md5);//202CB962AC59075B964B07152D234B70
        md5 = MD5Utils.getMD5String("6");
        System.out.println(md5);//202CB962AC59075B964B07152D234B70
        md5 = MD5Utils.getMD5String("7");
        System.out.println(md5);//202CB962AC59075B964B07152D234B70
    }


    @Test
    public void DateTest() {
        Date dt1 = new Date();
        Date dt2 = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        Date t1 = DateUtils.string2Date(dt1.toString());
        System.out.println(t1);//
    }


    @Test
    public void fun(){
        Long l1= 1L;
        Long l2= 2L;

        System.out.println(l1==l2);//false



        int a = 65;
        System.out.println((char)a);
        char b = 'A';
        System.out.println((int)b);

    }



    public static String getEncoding(String str) {
        String encode = "UTF-8";
        try {
            if (str.equals(new String(str.getBytes(encode), encode))) {
                String s = encode;
                return s;
            }
        } catch (Exception exception) {
        }
        encode = "ISO-8859-1";
        try {
            if (str.equals(new String(str.getBytes(encode), encode))) {
                String s1 = encode;
                return s1;
            }
        } catch (Exception exception1) {
        }
        encode = "GB2312";
        try {
            if (str.equals(new String(str.getBytes(encode), encode))) {
                String s2 = encode;
                return s2;
            }
        } catch (Exception exception2) {
        }
        encode = "GBK";
        try {
            if (str.equals(new String(str.getBytes(encode), encode))) {
                String s3 = encode;
                return s3;
            }
        } catch (Exception exception3) {
        }
        return "";
    }

    public static boolean isUTF8(String key){
        try {
            key.getBytes("utf-8");
            return true;
        }catch (UnsupportedEncodingException e){
            return false;
        }
    }


    @Test
    public void testEncoding(){
        String str = "%E8%8B%B1%E8%AF%AD";
        String result = getEncoding(str);
        System.out.println(result);

        boolean b = isUTF8(str);
        System.out.println(b);

    }


    public static void main(String[] args) {
        String url = "http://test.huazilive.com/api/service/account/privacy/folder/file/upload";
        String fileUrl = "C://Users/admin/Desktop/1.png";
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("loginToken", "b78ee9a21174b8102e9825d96d832d6b");
        map.put("folderPath", "/teacher/avatar");
        map.put("loginProductId", Constant.loginProductId);
        map.put("loginProductKey", Constant.loginProductKey);
        try {
            System.out.println(FileUtils.post4Caller(url, "upload", new File(fileUrl), map));
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void line(){
        String str = "java.lang.ArithmeticException: / by zero\r\ncn.com.mys.project_kernel.base.controller.account.student.StudentHomeworkController.result(StudentHomeworkController.java:627)\r\nsun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\nsun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)\r\nsun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\njava.lang.reflect.Method.invoke(Method.java:601)\r\norg.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:222)\r\norg.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:137)\r\norg.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:110)\r\norg.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:814)\r\norg.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:737)\r\norg.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:85)\r\norg.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:959)\r\norg.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:893)\r\norg.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:969)\r\norg.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:871)\r\njavax.servlet.http.HttpServlet.service(HttpServlet.java:641)\r\norg.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:845)\r\njavax.servlet.http.HttpServlet.service(HttpServlet.java:722)\r\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:305)\r\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:210)\r\ncn.com.mys.project_kernel.base.filter.SystemFilter.doFilter(SystemFilter.java:217)\r\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:243)\r\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:210)\r\norg.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:121)\r\norg.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:243)\r\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:210)\r\ncom.thetransactioncompany.cors.CORSFilter.doFilter(CORSFilter.java:209)\r\ncom.thetransactioncompany.cors.CORSFilter.doFilter(CORSFilter.java:244)\r\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:243)\r\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:210)\r\norg.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:225)\r\norg.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:169)\r\norg.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:472)\r\norg.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:168)\r\norg.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:98)\r\norg.apache.catalina.valves.AccessLogValve.invoke(AccessLogValve.java:927)\r\norg.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:118)\r\norg.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:407)\r\norg.apache.coyote.http11.AbstractHttp11Processor.process(AbstractHttp11Processor.java:999)\r\norg.apache.coyote.AbstractProtocol$AbstractConnectionHandler.process(AbstractProtocol.java:565)\r\norg.apache.tomcat.util.net.JIoEndpoint$SocketProcessor.run(JIoEndpoint.java:307)\r\njava.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1110)\r\njava.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:603)\r\njava.lang.Thread.run(Thread.java:722)\r\n";
        System.out.println(str);
    }

    @Test
    public void fff(){

        double rightCount = 20;
        BigDecimal bd = new BigDecimal((rightCount/95)*100).setScale(0, BigDecimal.ROUND_HALF_UP);
        int accuracy = Integer.parseInt(bd.toString());
        System.out.println(accuracy);



        BigDecimal bd4SH = new BigDecimal((9.0/10)*100).setScale(0, BigDecimal.ROUND_HALF_UP);
        System.out.println(bd4SH.toString());

    }

    @Test
    public void ffff(){
        JSONObject jo = new JSONObject();
        jo.put("subject","111");
        System.out.println(jo.toString());

    }


    @Test
    public void fifi(){

        String ss = PropertiesUtils.getKeyValue("studentName4Default");

        System.out.println(ss);
    }


    @Test
    public void fun13(){
        String s = "111111119<img src=\"https://huazi-api-test.oss-cn-beijing.aliyuncs.com/storage/4036/24484/Wuk1Hb73uQA7HmpniPc0fa6FfjsY5h.png\" style=\"vertical-align:middle;FLOAT:right;\">如图，将硬纸片沿虚线折起来，便可做成一个正方体，这个正方体的2号面的对面是<div class=\"quizPutTag\"> </div>号面．";

        String img = "";

        int i = s.indexOf("img src=");
        String ss = s.substring(i+9,s.length());
        System.out.println(ss);
        String jj = ss.substring(0,ss.indexOf(".png")+4);
        System.out.println(jj);



    }

    @Test
    public void fun14() throws SocketException {
        String localip = null;// 本地IP，如果没有配置外网IP则返回它
        String netip = null;// 外网IP

        Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
        InetAddress ip = null;
        boolean finded = false;// 是否找到外网IP
        while (netInterfaces.hasMoreElements() && !finded) {
            NetworkInterface ni = netInterfaces.nextElement();
            Enumeration<InetAddress> address = ni.getInetAddresses();
            while (address.hasMoreElements()) {
                ip = address.nextElement();
                if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress() && ip.getHostAddress().indexOf(":") == -1) {// 外网IP
                    netip = ip.getHostAddress();
                    finded = true;
                    break;
                } else if (ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
                        && ip.getHostAddress().indexOf(":") == -1) {// 内网IP
                    localip = ip.getHostAddress();
                }
            }
        }
        System.out.println("localip----"+localip);
        System.out.println("netip----"+netip);

    }


    @Test
    public void fun15(){
        String str = "17,18,12,1,11,16,13,14,15,2,3,4,5,6,10,9,7,8";
        String sss = StringUtils.sortNumberString(str,",","#");
        System.out.println(sss);
    }


    @Test
    public void fun16(){
        Object o = 1;
        int i =  (Integer)o+1;
        System.out.println(i);

    }

    @Test
    public void fun17(){
        for(;;){
            System.out.println("555555");
        }
    }

    @Test
    public void fun18(){
        List<String> list1 = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();

        list1.add("1");
        list1.add("2");
        list1.add("3");

        list2.add("11");
        list2.add("22");
        list2.add("33");

        List list = ListUtils.merge(list1,list2);

        for(Object s : list){
            System.out.println(s);
        }
    }


    @Test
    public void fun19(){
        String ids1 = "1,2,3,4,5,11";
        String ids2 = "11,12,13,14,15";

        List<String> list = ListUtils.subtract(new ArrayList(Arrays.asList(ids1.split(","))),new ArrayList(Arrays.asList(ids2.split(","))));
        for(String s : list){
            System.out.println(s);
        }


    }

    @Test
    public void fun20(){
        double i1 = 17;
        Integer i2 = 28;
        BigDecimal bd = new BigDecimal((i1/i2)*100).setScale(0, BigDecimal.ROUND_HALF_UP);
        System.out.println(bd.toString());
        int i = Integer.parseInt(bd.toString());
        System.out.println(i);


    }


    @Test
    public void fun21(){

        String c = "<div>\n" +
                "\t<video controls=\"controls\" height=\"297\" id=\"video20181026234013\" poster=\"https://www.kaimengkeji.com/file/userfiles/3490b499c2d8422e95d0969c9b5fcc42/images/info/video/2018/11/%E6%B5%8B%E8%AF%95.png\" width=\"419\">\n" +
                "\t\t<source src=\"https://www.kaimengkeji.com/file/userfiles/3490b499c2d8422e95d0969c9b5fcc42/files/info/video/2018/11/234.mp4\" type=\"video/mp4\" />你的浏览器不支持此播放器.<br />\n" +
                "\t\t请直接下载文件: <a href=\"https://www.kaimengkeji.com/file/userfiles/3490b499c2d8422e95d0969c9b5fcc42/files/info/video/2018/11/234.mp4\">video/mp4</a></video>\n" +
                "</div>\n" +
                "<p>\n" +
                "\t&nbsp;</p>";

        int i1 = c.indexOf("<video",0);
        int i2 = c.indexOf("<source",i1);
        int i3 = c.indexOf("</video>",i2);

        String s1 = c.substring(0,i1);
        String s2 = c.substring(i1,i2);
        String s3 = c.substring(i2,i3);//处理并去掉多余内容
        String s4 = c.substring(i3,c.length());

        String src_mp4 = s3.substring(s3.indexOf(" src=",0),s3.indexOf(" type=",0));
        System.out.println("src_mp4--->>>"+src_mp4);
        s2 = s2.replace(">",src_mp4+">");

        String content = s1+s2+s4;
        System.out.println("content--->>>"+content);


    }


    @Test
    public void fun22(){
        //获取日期属于当年第几周
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);

        System.out.println(calendar.get(Calendar.WEEK_OF_YEAR));
    }

    @Test
    public void fun23(){
        //获取日期属于当年第几周
        String year = DateUtils.getSysYear(new Date());
        System.out.println(year);
    }


    @Test
    public void fun24(){
        //获取每周的第一天和最后一天
        String days = DateUtils.getFirstAndLastOfWeek(new Date(),"yyyy-MM-dd","yyyy-MM-dd");
        System.out.println(days);


        //获取每周的第一天和最后一天
        String days2 = DateUtils.getFirstAndLastOfWeek("2018-12-28","yyyy-MM-dd","yyyy-MM-dd");
        System.out.println(days2);

    }


    @Test
    public void fun25(){
        Date firstOfWeek4Date = null;
        Date lastOfWeek4Date = null;
        try {
            firstOfWeek4Date = new SimpleDateFormat("yyyy-MM-dd").parse("2018-12-31");
            lastOfWeek4Date = new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-06");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<Date> list = DateUtils.getDates(firstOfWeek4Date,lastOfWeek4Date);
        for(Date date : list){

            System.out.println(date);
        }

    }


    @Test
    public void fun26(){
        String md = DateUtils.getSysMonthAndDay(new Date());
        System.out.println(md);

        String week = DateUtils.getSysWeek(new Date());
        System.out.println(week);


        String startDate = DateUtils.getSysDate(new Date());
        String endDate = DateUtils.getSysDate(DateUtils.addDays(new Date(),1));
        System.out.println(startDate);
        System.out.println(endDate);


        Random r = new Random();
        int s = r.nextInt(10 - 0 + 1) + 0;
        System.out.println(s);
    }


    @Test
    public void fun27(){

        List<Map<String, Object>> knowledgeStatInfoList = new ArrayList<Map<String, Object>>();

        Map<String, Object> mmmm1 = new HashMap<String, Object>();
        mmmm1.put("knowledgeId", 1);
        mmmm1.put("knowledgeName", "11");
        mmmm1.put("questionCount", 1);
        mmmm1.put("avgRightRate", 1);
        mmmm1.put("mean", "");

        Map<String, Object> mmmm2 = new HashMap<String, Object>();
        mmmm2.put("knowledgeId", 2);
        mmmm2.put("knowledgeName", "22");
        mmmm2.put("questionCount", 2);
        mmmm2.put("avgRightRate", 1);
        mmmm2.put("mean", "");

        Map<String, Object> mmmm3 = new HashMap<String, Object>();
        mmmm3.put("knowledgeId", 3);
        mmmm3.put("knowledgeName", "33");
        mmmm3.put("questionCount", 5);
        mmmm3.put("avgRightRate", 1);
        mmmm3.put("mean", "");

        Map<String, Object> mmmm4 = new HashMap<String, Object>();
        mmmm4.put("knowledgeId", 4);
        mmmm4.put("knowledgeName", "44");
        mmmm4.put("questionCount", 3);
        mmmm4.put("avgRightRate", 1);
        mmmm4.put("mean", "");

        Map<String, Object> mmmm5 = new HashMap<String, Object>();
        mmmm5.put("knowledgeId", 5);
        mmmm5.put("knowledgeName", "55");
        mmmm5.put("questionCount", 1);
        mmmm5.put("avgRightRate", 1);
        mmmm5.put("mean", "");

        knowledgeStatInfoList.add(mmmm1);
        knowledgeStatInfoList.add(mmmm2);
        knowledgeStatInfoList.add(mmmm3);
        knowledgeStatInfoList.add(mmmm4);
        knowledgeStatInfoList.add(mmmm5);


        Collections.sort(knowledgeStatInfoList, new Comparator<Map<String, Object>>() {
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                //return o1.get("questionCount").toString().compareTo(o2.get("questionCount").toString());//从小到大排序
                return o2.get("questionCount").toString().compareTo(o1.get("questionCount").toString());//从大到小排序
            }
        });

        List<Map<String, Object>> knowledgeResultList = new ArrayList<Map<String, Object>>();
        for(int i=0;i<knowledgeStatInfoList.size();i++){
            Map<String, Object> map = knowledgeStatInfoList.get(i);
            System.out.println(map.toString());
            if(i<3){
                knowledgeResultList.add(knowledgeStatInfoList.get(i));
            }
        }

        if(knowledgeStatInfoList.size()>3){
            knowledgeStatInfoList = knowledgeStatInfoList.subList(3,knowledgeStatInfoList.size());
            Long knowledgeId = 0L;
            String knowledgeName = "其他";
            Integer questionCount = 0;
            Integer avgRightRate = 0;
            String mean = "";

            for(Map<String, Object> m : knowledgeStatInfoList){
                System.out.println(m.toString());
                questionCount = questionCount+Integer.parseInt(m.get("questionCount").toString());
                avgRightRate = avgRightRate+Integer.parseInt(m.get("avgRightRate").toString());
            }

            avgRightRate = avgRightRate/knowledgeStatInfoList.size();
            System.out.println("questionCount="+questionCount);
            System.out.println("avgRightRate="+avgRightRate);

            Map<String, Object> map = new HashMap<>();
            map.put("knowledgeId", knowledgeId);
            map.put("knowledgeName", knowledgeName);
            map.put("questionCount", questionCount);
            map.put("avgRightRate", avgRightRate);
            map.put("mean", mean);

            knowledgeResultList.add(map);
        }


        for(Map<String, Object> mm : knowledgeResultList){
            System.out.println(mm.toString());
        }



    }


    @Test
    public void fun28(){
        List<Long> list = new ArrayList<>();
        list.add(1L);
        list.add(2L);
        list.add(1L);
        list.add(2L);
        list.add(5L);
        list.add(6L);

        List<Long> l = new ArrayList<>(new HashSet<>(list));//去重
        for(Long n : l){
            System.out.println("n="+n);
        }

    }


    @Test
    public void fun29(){
        for(int i=0;i<5;i++){
            for(int j=0;j<5;j++){
                if(i==j){
                    System.out.println("i="+i+"--j="+j);
                    break;
                }

            }
        }

    }



    @Test
    public void fun30(){
        OtherException o = new OtherException();
        String clazz = o.getClass().getName();
        System.out.println(clazz);

    }


    @Test
    public void fun31(){

        String s = OrderUtils.getUniqueCode().toString();
        System.out.println(s);
        System.out.println(s.length());
    }


    @Test
    public void fun32(){

        String filePath = "F://backdrops.json";
        String content = FileUtils.readFile(filePath);
        System.out.println(content);

    }



    @Test
    public void fun33(){
        String fileUrl = "https://huazi-api-test.oss-cn-beijing.aliyuncs.com/storage/4038/282842/XOPQ9kZRxAnwNmPiarmQ4Xgvasc7ch3.png";
        String filePath4New = fileUrl.substring("https://huazi-api-test.oss-cn-beijing.aliyuncs.com".length());
        System.out.println(filePath4New);

    }



    @Test
    public void fun34(){
        BigDecimal bd = new BigDecimal(0.01);
        Double d = bd.doubleValue()*100;
        int payAmount = d.intValue();
        System.out.println(payAmount);
    }


    @Test
    public void fun35(){
        System.out.println(System.getProperty("user.dir"));
        System.out.println(System.getProperty("user.home"));
        System.out.println("------------------");

        //获取所有的属性
        Properties properties = System.getProperties();
        //遍历所有的属性
        for (String key : properties.stringPropertyNames()) {
            //输出对应的键和值
            System.out.println(key + "=" + properties.getProperty(key));
        }

    }


    @Test
    public void fun36(){
        // 构造一个Runner
        TestRunnable runner = new TestRunnable() {
            @Override
            public void runTest() throws Throwable {
                // 测试内容
                // 可以在这里直接调用Assert的各种方法,因为TestRunnable类是继承自Assert的
                System.out.println("------");


            }
        };
        int runnerCount = 2;
        //Rnner数组，想当于并发多少个
        TestRunnable[] trs = new TestRunnable[runnerCount];
        for (int i = 0; i < runnerCount; i++) {
            trs[i] = runner;
        }
        // 用于执行多线程测试用例的Runner，将前面定义的单个Runner组成的数组传入
        MultiThreadedTestRunner mttr = new MultiThreadedTestRunner(trs);
        try {
            // 并发执行数组里定义的内容
            mttr.runTestRunnables();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Test
    public void fun37(){
        // 构造一个Runner
        TestRunnable runner = new TestRunnable() {
            @Override
            public void runTest() throws Throwable {
                // 测试内容
                // 可以在这里直接调用Assert的各种方法,因为TestRunnable类是继承自Assert的

                String loginToken = "c185887dba17df6eb9682e59659256e7";
                String url = "https://www.haimacode.com/test/api/haimawang/account/student/course/order/detail";//测试

                JSONObject obj = new JSONObject();
                obj.put("loginToken", loginToken);
                obj.put("orderNo", "146775941908004864");

                JSONObject jo = HttpUtils.sendPost4JSON(url, obj);

                System.out.println(jo.toString());
            }
        };
        int runnerCount = 10;
        //Rnner数组，想当于并发多少个
        TestRunnable[] trs = new TestRunnable[runnerCount];
        for (int i = 0; i < runnerCount; i++) {
            trs[i] = runner;
        }
        // 用于执行多线程测试用例的Runner，将前面定义的单个Runner组成的数组传入
        MultiThreadedTestRunner mttr = new MultiThreadedTestRunner(trs);
        try {
            // 并发执行数组里定义的内容
            mttr.runTestRunnables();
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }



    @Test
    public void fun38(){

        for(int i=0;i<5;i++){
            System.out.println("111");
            loop:for(int j=0;j<5;j++){
                System.out.println("222");
                if(j==3){
                    System.out.println("333");
                    break loop;
                }
                System.out.println("444");
            }
            System.out.println("555");
        }

        System.out.println("666");


    }


    @Test
    public void fun39(){
        int i = 0___00___1;
        System.out.println(i+1);

        //int j = 0/0;
        try{
            int j = 0/0;
        }catch(ArithmeticException|NullPointerException e){
            System.out.println(e.getMessage());
        }


    }


    @Test
    public void fun40() {

        Date date = DateUtils.addHours(new Date(), -25);
        System.out.println(DateUtils.date2String(date));

        int r = 0-Constant.createEndDate4PinTuan;
        System.out.println("r="+r);



        String s = StringUtils.unique("1,2,3,4,5"+","+"1,3,5,7,9");
        System.out.println("s="+s);


        String ss = StringUtils.join("1,2", "3,4", "");
        System.out.println("ss="+ss);

        String str = "1,2,3,4,5,1,3,5,7,9";
        String[] arr = str.split(",");
        List<String> list = new ArrayList<>(new LinkedHashSet(new ArrayList<>(Arrays.asList(arr))));
        String sss = org.apache.commons.lang.StringUtils.join(list, ",");
        System.out.println("sss="+sss);
    }



    @Test
    public void fun41(){
        double p = 5.365;
        BigDecimal bd = BigDecimal.valueOf(p/100);
        System.out.println(bd);


        String s = StringUtils.getRandomString(6).toUpperCase();
        System.out.println(s);
    }



    @Test
    public void fun42(){

        String _value = "[{\"epf\":1,\"epb\":581,\"eqf\":208,\"eqb\":1210,\"dmdp\":1090,\"dmdq\":437},{\"epf\":2,\"epb\":581,\"eqf\":208,\"eqb\":1210,\"dmdp\":1090,\"dmdq\":437}]";
        com.alibaba.fastjson.JSONArray ja = new com.alibaba.fastjson.JSONArray();
        if(StringUtils.isNotEmpty(_value)){
            ja = JSONArray.parseArray(_value);
        }
        com.alibaba.fastjson.JSONObject jo = new com.alibaba.fastjson.JSONObject();
        jo.put("a", 1);
        ja.add(0, jo);
        System.out.println(ja.toJSONString());

    }


    @Test
    public void fun43(){




    }












}
